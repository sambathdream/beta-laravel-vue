<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger('team_id')->nullable();
            $table->unsignedInteger('open_id')->nullable();
            
            $table->string('item_id')->nullable();
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->decimal('qty', 13, 2)->default(0);

            $table->decimal('buy_price', 13, 2)->default(0);
            $table->decimal('sell_price', 13, 2)->default(0);

            $table->string('category')->nullable();
            $table->string('type')->nullable();
            $table->string('image')->nullable();

            $table->unsignedInteger('debit_gl')->nullable();
            $table->unsignedInteger('credit_gl')->nullable();

            $table->unsignedInteger('created_by_id')->nullable();
            $table->unsignedInteger('modified_by_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });

        Schema::create('item_categories', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('team_id')->nullable();

            $table->string('text')->nullable();

        });

        Schema::create('item_types', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('team_id')->nullable();

            $table->string('text')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');

        Schema::dropIfExists('item_categories');
        Schema::dropIfExists('item_types');
    }
}
