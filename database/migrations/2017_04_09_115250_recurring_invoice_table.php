<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecurringInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_recurring_inv', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('team_id')->nullable();
            $table->unsignedInteger('open_id')->nullable();

            $table->unsignedInteger('invoice_id')->nullable(); //stored in the invoice table with status 'recurring'
            $table->unsignedInteger('status_id')->default(1);  // 1: active 2: disabled ?

            $table->integer('freq')->nullable(); //frequency - number of days, weeks or months
            $table->unsignedInteger('freq_type')->default(1); //1: days, 2: weeks, 3: months

            $table->boolean('reminder_to_cust')->default(true);
            $table->boolean('reminder_to_user')->default(true);
            $table->boolean('invoice_to_cust')->default(true);
            $table->boolean('invoice_to_user')->default(true);

            $table->unsignedInteger('ends')->default(1);//1: never, 2: specific date, 3:after no # of days
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->integer('end_after_no_inv')->nullable();

            $table->integer('invoices_created')->default(0); //invoices created so far
            $table->timestamp('last_sent_date')->nullable();
            
            $table->unsignedInteger('created_by_id')->nullable();
            $table->unsignedInteger('modified_by_id')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_recurring_inv');
    }
}
