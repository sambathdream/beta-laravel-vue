<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TeamDetailEmail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('team_details', function (Blueprint $table) {
            //

            $table->string('email')->nullable();
            $table->string('inv_footer')->nullable();
            $table->string('inv_notes')->nullable();
            $table->binary('imageSrc')->nullable();

            $table->string('bank_detail')->nullable();
            $table->string('billing_address')->nullable();
            $table->string('billing_address_line_2')->nullable();
            $table->string('billing_city')->nullable();
            $table->string('billing_state')->nullable();
            $table->string('billing_zip', 25)->nullable();
            $table->string('billing_country', 2)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('team_details', function (Blueprint $table) {
            //
            $table->dropColumn('email');
            $table->dropColumn('inv_footer');
            $table->dropColumn('inv_notes');
            $table->dropColumn('imageSrc');

            $table->dropColumn('bank_detail');
            $table->dropColumn('billing_address');
            $table->dropColumn('billing_address_line_2');
            $table->dropColumn('billing_city');
            $table->dropColumn('billing_state');
            $table->dropColumn('billing_zip');
            $table->dropColumn('billing_country');


        });
    }
}
