<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBalanceToTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            //
            $table->decimal('balance', 13, 2)->default(0);
            $table->unsignedInteger('txn_ref')->nullable();
            $table->unsignedInteger('inv_ref')->nullable();
            $table->unsignedInteger('pay_ref')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            //
            $table->dropColumn('balance');
            $table->dropColumn('txn_ref');
            $table->dropColumn('inv_ref');
            $table->dropColumn('pay_ref');

        });
    }
}
