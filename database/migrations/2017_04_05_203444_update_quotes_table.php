<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_quotes', function (Blueprint $table) {
            //
            $table->string('currency_id_local',3)->nullable();
            $table->decimal('tax_amount_local', 13, 2)->default(0);
        });

        Schema::table('user_invoices', function (Blueprint $table) {
            //
            $table->decimal('tax_amount_local', 13, 2)->default(0);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_quotes', function (Blueprint $table) {
            //
            $table->dropColumn('currency_id_local');
            $table->dropColumn('tax_amount_local');
        });

        Schema::table('user_invoices', function (Blueprint $table) {
            //
            $table->dropColumn('tax_amount_local');
        });

    }
}
