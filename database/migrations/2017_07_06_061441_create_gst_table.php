<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGstTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('gst_hsn_sac', function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger('team_id')->nullable(); //renamed to team from company
            $table->unsignedInteger('open_id')->nullable(); //

            $table->string('type',3)->nullable(); //
            $table->string('category')->nullable(); //organization name
            $table->string('sub_category')->nullable();
            $table->string('service')->nullable();
            $table->string('code')->nullable();
            $table->string('description')->nullable();

            $table->unsignedInteger('created_by_id')->nullable();
            $table->unsignedInteger('modified_by_id')->nullable();

            $table->timestamps();
            $table->softDeletes();

        });


        Schema::create('inv_item_taxes', function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger('team_id')->nullable();  //from the company table (these are customer of ours - pi.team)
            $table->unsignedInteger('item_num')->nullable(); //invoice no. not visible to user

            $table->unsignedInteger('open_id')->nullable();

            $table->string('tax_id')->nullable();
            $table->string('name')->nullable();
            $table->decimal('rate', 13, 3)->default(0);
            $table->decimal('amount', 13, 2)->default(0);

            $table->unsignedInteger('gl_account_id')->nullable(); //gl_account id

            $table->boolean('tax_display')->default(false);
            $table->boolean('recoverable')->default(false);
            $table->boolean('compound')->default(false);

            $table->timestamps();
            $table->softDeletes();

            $table->string('type',3)->nullable(); //TAX, DED
            $table->boolean('exclude_tax')->default(false);

        });

        Schema::table('items', function (Blueprint $table) {
            //
            $table->unsignedInteger('gst_hsnsac_id')->nullable(); //gl_account id

        });

        //user_invoice_taxes

        Schema::table('user_invoice_taxes', function (Blueprint $table) {
            //
            $table->unsignedInteger('item_num')->nullable();

        });

        Schema::table('user_quote_taxes', function (Blueprint $table) {
            //
            $table->unsignedInteger('item_num')->nullable();

        });

        Schema::table('user_credit_taxes', function (Blueprint $table) {
            //
            $table->unsignedInteger('item_num')->nullable();

        });

        //hsnsac_code

        Schema::table('user_invoice_items', function (Blueprint $table) {
            //
            $table->string('hsnsac_code')->nullable();

        });

        Schema::table('user_quote_items', function (Blueprint $table) {
            //
            $table->string('hsnsac_code')->nullable();

        });

        Schema::table('user_credit_items', function (Blueprint $table) {
            //
            $table->string('hsnsac_code')->nullable();

        });

        //contact
        //team_details

        /*
        Schema::table('user_invoice_settings', function (Blueprint $table) {
            //
            $table->integer('inv_first_rem')->nullable();
            $table->unsignedInteger('inv_first_rem_type')->nullable(); //1 = days 2 = months
            $table->unsignedInteger('inv_first_rem_from')->nullable(); //1 = due date 2 = post/invoice date

        });
        */


        Schema::table('team_details', function (Blueprint $table) {
            //
            $table->string('invoice_prefix',20)->nullable();
            $table->string('quote_prefix',20)->nullable();
            $table->string('rec_invoice_prefix',20)->nullable();

            $table->boolean('line_tax')->default(false); //tax calculated in the invoice line items
            $table->boolean('gst_enabled')->default(false); //enable gst
            $table->string('gst_code')->nullable(); //gstn number
        });

        Schema::table('contacts', function (Blueprint $table) {
            //
            $table->string('gst_code')->nullable(); //client gstn number

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gst_hsn_sac');
        Schema::dropIfExists('inv_item_taxes');

        Schema::table('items', function (Blueprint $table) {
            //
            $table->dropColumn('gst_hsnsac_id');
        });

        Schema::table('user_invoice_taxes', function (Blueprint $table) {
            //
            $table->dropColumn('item_num');
        });
        Schema::table('user_quote_taxes', function (Blueprint $table) {
            //
            $table->dropColumn('item_num');
        });
        Schema::table('user_credit_taxes', function (Blueprint $table) {
            //
            $table->dropColumn('item_num');
        });

        //

        Schema::table('user_invoice_items', function (Blueprint $table) {
            //
            $table->dropColumn('hsnsac_code');
        });
        Schema::table('user_quote_items', function (Blueprint $table) {
            //
            $table->dropColumn('hsnsac_code');
        });
        Schema::table('user_credit_items', function (Blueprint $table) {
            //
            $table->dropColumn('hsnsac_code');
        });

        //contacts and team_details

        Schema::table('contacts', function (Blueprint $table) {
            //
            $table->dropColumn('gst_code');
        });

        Schema::table('team_details', function (Blueprint $table) {
            //
            $table->dropColumn('invoice_prefix');
            $table->dropColumn('quote_prefix');
            $table->dropColumn('rec_invoice_prefix');
            $table->dropColumn('line_tax');
            $table->dropColumn('gst_enabled');
            $table->dropColumn('gst_code');

        });

    }
}
