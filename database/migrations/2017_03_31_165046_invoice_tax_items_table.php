<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvoiceTaxItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_invoice_taxes', function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger('team_id')->nullable();  //from the company table (these are customer of ours - pi.team)
            $table->unsignedInteger('invoice_num')->nullable(); //invoice no. not visible to user

            $table->unsignedInteger('tax_open_id')->nullable();

            $table->string('tax_id')->nullable();
            $table->string('tax_name')->nullable();
            $table->decimal('tax_rate', 13, 3)->default(0);
            $table->decimal('tax_amount', 13, 2)->default(0);

            $table->unsignedInteger('tax_gl')->nullable(); //gl_account id

            $table->boolean('tax_display')->default(false);
            $table->boolean('recoverable')->default(false);
            $table->boolean('compound')->default(false);

            $table->timestamps();
            $table->softDeletes();

        });

        Schema::create('user_quote_taxes', function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger('team_id')->nullable();  //from the company table (these are customer of ours - pi.team)
            $table->unsignedInteger('quote_num')->nullable(); //invoice no. not visible to user

            $table->unsignedInteger('tax_open_id')->nullable();

            $table->string('tax_id')->nullable();
            $table->string('tax_name')->nullable();
            $table->decimal('tax_rate', 13, 3)->default(0);
            $table->decimal('tax_amount', 13, 2)->default(0);

            //$table->unsignedInteger('tax_gl')->nullable(); //to be deleted

            $table->boolean('tax_display')->default(false);
            $table->boolean('recoverable')->default(false);
            $table->boolean('compound')->default(false);

            $table->timestamps();
            $table->softDeletes();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_invoice_taxes');
        Schema::dropIfExists('user_quote_taxes');
    }
}
