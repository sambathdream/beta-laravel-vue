<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ManualIdToInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_invoices', function (Blueprint $table) {
            //
            $table->string('manual_id')->nullable(); //to be deleted
        });

        Schema::table('user_quotes', function (Blueprint $table) {
            //
            $table->string('manual_id')->nullable(); //to be deleted
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_invoices', function (Blueprint $table) {
            //
            $table->dropColumn('manual_id');
        });

        Schema::table('user_quotes', function (Blueprint $table) {
            //
            $table->dropColumn('manual_id');
        });

    }
}
