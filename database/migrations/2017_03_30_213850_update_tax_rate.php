<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTaxRate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tax_rates', function (Blueprint $table) {
            //
            $table->string('tax_id', 20)->nullable();
            $table->unsignedInteger('gl_account_id')->nullable();
            $table->boolean('tax_display')->default(false);
            $table->boolean('recoverable')->default(false);
            $table->boolean('compound')->default(false);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tax_rates', function (Blueprint $table) {
            //
            $table->dropColumn('tax_id');
            $table->dropColumn('gl_account_id');
            $table->dropColumn('tax_display');
            $table->dropColumn('recoverable');
            $table->dropColumn('compound');

        });
    }
}
