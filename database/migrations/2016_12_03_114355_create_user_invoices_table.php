<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_invoices', function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger('team_id')->nullable();  //from the team table (these are customer of ours - pi.team)
            $table->unsignedInteger('open_id')->nullable();

            $table->unsignedInteger('contact_id')->nullable(); //from the contacts table

            $table->unsignedInteger('status_id')->default(1);

            $table->string('logo')->nullable();         //to be deleted
            $table->string('from')->nullable();         //to be deleted
            $table->string('title')->nullable();
            $table->string('summary')->nullable();
            $table->string('from_email')->nullable();   //to be deleted

            $table->string('customer_name')->nullable();
            $table->string('customer_email')->nullable();
            $table->string('poso_number')->nullable();
            $table->date('post_date')->nullable();
            $table->date('due_date')->nullable();
            $table->string('ship_to_addr')->nullable();//to be deleted
            $table->string('bill_to_addr')->nullable();//to be deleted

            $table->string('phone')->nullable();          //to be deleted
            $table->string('ship_phone')->nullable();   ////to be deleted

            $table->boolean('is_recurring')->default(false);
            $table->unsignedInteger('recurring_id')->nullable();
            $table->unsignedInteger('frequency_id')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->timestamp('last_sent_date')->nullable();

            $table->boolean('discount_type')->default(false);   //false means % and true means amount
            $table->double('discount_rate1', 8, 2)->default(0);

            $table->string('tax_name1')->nullable();          //to be deleted
            $table->double('tax_rate1', 8, 2)->default(0);    //to be deleted

            $table->decimal('sub_total', 13, 2)->default(0);
            $table->decimal('discount_amount', 13, 2)->default(0);
            $table->decimal('tax_amount', 13, 2)->default(0);

            $table->decimal('total_amount', 13, 2)->default(0);
            $table->decimal('amount_paid', 13, 2)->default(0);
            $table->decimal('balance', 13, 2)->default(0);

            $table->decimal('total_amount_local', 13, 2)->default(0);
            $table->decimal('amount_paid_local', 13, 2)->default(0);
            $table->decimal('balance_local', 13, 2)->default(0);

            $table->string('currency_id',3)->nullable();
            $table->string('currency_id_local',3)->nullable();
            $table->decimal('currency_rate', 13, 3)->default(0); //rate during invoice create

            $table->unsignedInteger('debit_gl')->nullable(); //gl_account id
            $table->boolean('credit_gl_cb')->default(false);

            $table->text('terms')->nullable();
            $table->text('notes')->nullable();
            $table->text('footer')->nullable();

            $table->unsignedInteger('created_by_id')->nullable();
            $table->unsignedInteger('modified_by_id')->nullable();

            $table->timestamps();
            $table->softDeletes();


        });


        Schema::create('user_invoice_items', function (Blueprint $table)
        {
            $table->increments('id');

            $table->unsignedInteger('team_id')->nullable();  //from the company table (these are customer of ours - pi.team)
            $table->unsignedInteger('invoice_num')->nullable(); //invoice no. not visible to user

            //$table->unsignedInteger('contact_id')->nullable(); //this field is not needed

            $table->string('item_id')->nullable();
            $table->text('item_name')->nullable();
            $table->decimal('item_price', 13, 2)->default(0);
            $table->decimal('item_qty', 13, 2)->default(0);
            $table->decimal('item_total', 13, 2)->default(0);

            $table->boolean('item_taxable')->default(true);

            $table->unsignedInteger('credit_gl')->nullable(); //gl_account id

            $table->timestamps();
            $table->softDeletes();

        });


        Schema::create('user_quotes', function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger('team_id')->nullable();  //from the team table (these are customer of ours - pi.team)
            $table->unsignedInteger('open_id')->nullable();

            $table->unsignedInteger('contact_id')->nullable(); //from the contacts table

            $table->unsignedInteger('status_id')->default(1);

            $table->string('logo')->nullable();//to be deleted
            $table->string('from')->nullable();//to be deleted
            $table->string('title')->nullable();
            $table->string('summary')->nullable();
            $table->string('from_email')->nullable();

            $table->string('customer_name')->nullable();
            $table->string('customer_email')->nullable();
            $table->string('poso_number')->nullable();
            $table->date('post_date')->nullable();
            $table->date('due_date')->nullable();
            $table->string('ship_to_addr')->nullable();//to be deleted
            $table->string('bill_to_addr')->nullable();//to be deleted

            $table->string('phone')->nullable();    //to be deleted
            $table->string('ship_phone')->nullable();  //to be deleted

            $table->boolean('discount_type')->default(false);   //false means % and true means amount
            $table->double('discount_rate1', 8, 2)->default(0);

            $table->string('tax_name1')->nullable();//to be deleted
            $table->double('tax_rate1', 8, 2)->default(0);//to be deleted

            $table->decimal('sub_total', 13, 2)->default(0);
            $table->decimal('discount_amount', 13, 2)->default(0);
            $table->decimal('tax_amount', 13, 2)->default(0);

            $table->decimal('total_amount', 13, 2)->default(0);
            $table->decimal('amount_paid', 13, 2)->default(0);
            $table->decimal('balance', 13, 2)->default(0);

            $table->decimal('total_amount_local', 13, 2)->default(0);
            $table->decimal('amount_paid_local', 13, 2)->default(0);
            $table->decimal('balance_local', 13, 2)->default(0);
            
            $table->string('currency_id',3)->nullable();
            $table->decimal('currency_rate', 13, 2)->default(0); //rate during invoice create

            $table->text('terms')->nullable();
            $table->text('notes')->nullable();
            $table->text('footer')->nullable();

            $table->unsignedInteger('created_by_id')->nullable();
            $table->unsignedInteger('modified_by_id')->nullable();

            $table->timestamps();
            $table->softDeletes();


        });


        Schema::create('user_quote_items', function (Blueprint $table)
        {
            $table->increments('id');

            $table->unsignedInteger('team_id')->nullable();  //from the company table (these are customer of ours - pi.team)
            $table->unsignedInteger('quote_num')->nullable(); //invoice no. not visible to user

            //$table->unsignedInteger('contact_id')->nullable(); //this field is not needed

            $table->string('item_id')->nullable();
            $table->text('item_name')->nullable();
            $table->decimal('item_price', 13, 2)->default(0);
            $table->decimal('item_qty', 13, 2)->default(0);
            $table->decimal('item_total', 13, 2)->default(0);

            $table->boolean('item_taxable')->default(true);

            $table->timestamps();
            $table->softDeletes();

        });

        Schema::create('user_inv_invitations', function($table)
        {
            $table->increments('id');

            $table->unsignedInteger('team_id')->nullable();
            $table->unsignedInteger('open_id')->nullable();

            $table->unsignedInteger('contact_id')->nullable();

            $table->string('invitation_key')->nullable(); //encrypted user entered invoice number (not the system invoice id)

            $table->timestamp('sent_date')->nullable();
            $table->timestamp('viewed_date')->nullable();
            $table->timestamp('opened_date')->nullable();

            $table->unsignedInteger('created_by_id')->nullable();
            $table->unsignedInteger('modified_by_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });

        Schema::create('user_invoice_terms', function (Blueprint $table) {

            $table->unsignedInteger('team_id')->nullable();

            $table->unsignedInteger('days')->nullable();
            $table->string('text')->nullable();

        });

        Schema::create('user_invoice_status', function (Blueprint $table) {

            $table->unsignedInteger('team_id')->nullable();

            $table->unsignedInteger('id')->nullable();
            $table->string('text')->nullable();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_invoices');
        Schema::dropIfExists('user_invoice_items');
        Schema::dropIfExists('user_inv_invitations');

        Schema::dropIfExists('user_invoice_terms');
        Schema::dropIfExists('user_invoice_status');

        Schema::dropIfExists('user_quotes');
        Schema::dropIfExists('user_quote_items');

    }

}
