<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailsToStripeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stripe_billing', function (Blueprint $table) {
            //
            $table->boolean('auto_email')->default(false);
            $table->string('from_email')->nullable();
            $table->string('cc_email')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stripe_billing', function (Blueprint $table) {
            //
            $table->dropColumn('auto_email');
            $table->dropColumn('from_email');
            $table->dropColumn('cc_email');
        });
    }
}
