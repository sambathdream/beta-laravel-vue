<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateQuoteAndInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_invoices', function (Blueprint $table) {
            //ded_amount
            $table->decimal('ded_amount', 13, 2)->default(0);
            $table->string('encrypted_id',50)->nullable();

        });

        Schema::table('user_quotes', function (Blueprint $table) {
            //
            $table->unsignedInteger('debit_gl')->nullable(); //gl_account id
            $table->boolean('credit_gl_cb')->default(false);
            $table->decimal('ded_amount', 13, 2)->default(0);
            $table->string('encrypted_id',50)->nullable();

        });

        Schema::table('user_quote_items', function (Blueprint $table) {
            //
            $table->unsignedInteger('credit_gl')->nullable(); //gl_account id

        });

        Schema::table('team_details', function (Blueprint $table) {
            //
            $table->text('inv_header')->nullable();
            $table->text('inv_left_content')->nullable();
            $table->text('inv_notes')->nullable()->change();
            $table->text('inv_footer')->nullable()->change();

            $table->text('quote_footer')->nullable();
            $table->text('quote_notes')->nullable();
            $table->text('quote_header')->nullable();
            $table->text('quote_left_content')->nullable();

            $table->string('email_subject')->nullable();
        });

        Schema::table('tax_rates', function (Blueprint $table) {
            //
            $table->string('type',3)->nullable(); //TAX, DED
            $table->boolean('exclude_tax')->default(false);

        });

        Schema::table('user_quote_taxes', function (Blueprint $table) {
            //
            $table->string('type',3)->nullable(); //TAX, DED
            $table->boolean('exclude_tax')->default(false);

        });

        Schema::table('user_invoice_taxes', function (Blueprint $table) {
            //
            $table->string('type',3)->nullable(); //TAX, DED
            $table->boolean('exclude_tax')->default(false);

        });

        Schema::table('contacts', function (Blueprint $table) {
            //
            $table->string('emails',600)->nullable(); //multiple emails

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_invoices', function (Blueprint $table) {
            //
            $table->dropColumn('ded_amount');
            $table->dropColumn('encrypted_id');
        });

        Schema::table('user_quotes', function (Blueprint $table) {
            //            $table->decimal('ded_amount', 13, 2)->default(0);
            $table->dropColumn('debit_gl');
            $table->dropColumn('credit_gl_cb');
            $table->dropColumn('ded_amount');
            $table->dropColumn('encrypted_id');
        });


        Schema::table('user_quote_items', function (Blueprint $table) {
            //
            $table->dropColumn('credit_gl');
        });

        Schema::table('team_details', function (Blueprint $table) {
            //
            $table->dropColumn('inv_header');
            $table->dropColumn('inv_left_content');

            $table->dropColumn('quote_footer');
            $table->dropColumn('quote_notes');
            $table->dropColumn('quote_header');
            $table->dropColumn('quote_left_content');

            $table->dropColumn('email_subject');
        });

        Schema::table('tax_rates', function (Blueprint $table) {
            //
            $table->dropColumn('type');
            $table->dropColumn('exclude_tax');
            //
        });

        Schema::table('user_quote_taxes', function (Blueprint $table) {
            //
            $table->dropColumn('type');
            $table->dropColumn('exclude_tax');
        });

        Schema::table('user_invoice_taxes', function (Blueprint $table) {
            //
            $table->dropColumn('type');
            $table->dropColumn('exclude_tax');
        });

        Schema::table('contacts', function (Blueprint $table) {
            //
            $table->dropColumn('emails');
        });

    }

}
