<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('team_details', function (Blueprint $table)
        {
            $table->increments('id');

            $table->unsignedInteger('team_id')->nullable();  //from the team table (these are customer of ours - pi.team)

            $table->string('id_number')->nullable();
            $table->string('vat_number')->nullable();
            $table->string('website')->nullable();
            $table->string('phone')->nullable();
            $table->unsignedInteger('company_size')->default(1);
            $table->unsignedInteger('industry')->default(1);

            $table->unsignedInteger('created_by_id')->nullable();
            $table->unsignedInteger('modified_by_id')->nullable();

            $table->timestamps();
        });

        Schema::create('localization', function (Blueprint $table)
        {
            $table->increments('id');

            $table->unsignedInteger('team_id')->nullable();  //from the team table (these are customer of ours - pi.team)

            $table->string('currency_id',3)->nullable();
            $table->string('language_id',3)->nullable();

            $table->string('timezone')->nullable();

            $table->unsignedInteger('date_time_format')->default(1);
            $table->unsignedInteger('week_first_day')->default(1);    //"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday",
            $table->unsignedInteger('year_first_month')->default(1);    //'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december',

            $table->unsignedInteger('created_by_id')->nullable();
            $table->unsignedInteger('modified_by_id')->nullable();

            $table->timestamps();
        });

        Schema::create('datetimelist', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('carbon')->nullable();
            $table->string('name')->nullable();

        });

        Schema::create('tax_rates', function($table)
        {
            $table->increments('id');

            $table->unsignedInteger('team_id')->nullable();  //from the team table (these are customer of ours - pi.team)
            $table->unsignedInteger('open_id')->nullable();

            $table->string('name')->nullable();
            $table->decimal('rate', 13, 3)->nullable();

            $table->unsignedInteger('created_by_id')->nullable();
            $table->unsignedInteger('modified_by_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });

        Schema::create('user_invoice_settings', function (Blueprint $table)
        {
            $table->increments('id');

            $table->unsignedInteger('team_id')->nullable();  //from the company table (these are customer of ours - pi.team)

            $table->string('invoice_title')->nullable();
            $table->string('invoice_summary')->nullable();    //
            $table->boolean('debit_gl_cb')->default(true);
            $table->boolean('credit_gl_cb')->default(true);
            $table->unsignedInteger('inv_pay_term')->default(6); //due in 15 days

            $table->timestamps();
            $table->softDeletes();

        });

        Schema::create('accounting', function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger('team_id')->nullable();  //from the team table (these are customer of ours - pi.team)

            $table->unsignedInteger('paymentcr')->nullable(); //credit
            $table->unsignedInteger('paymentdb')->nullable(); //debit

            $table->unsignedInteger('processfeecr')->nullable(); //credit
            $table->unsignedInteger('processfeedb')->nullable(); //debit

            $table->unsignedInteger('invoicecr')->nullable(); //credit
            $table->unsignedInteger('invoicedb')->nullable(); //debit

            $table->unsignedInteger('taxcr')->nullable(); //credit
            $table->unsignedInteger('taxdb')->nullable(); //debit

            $table->unsignedInteger('productcr')->nullable(); //credit
            $table->unsignedInteger('productdb')->nullable(); //debit

            $table->unsignedInteger('manualcr')->nullable(); //credit
            $table->unsignedInteger('manualdb')->nullable(); //debit

            $table->unsignedInteger('discountcr')->nullable(); //credit
            $table->unsignedInteger('discountdb')->nullable(); //debit

            $table->unsignedInteger('created_by_id')->nullable();
            $table->unsignedInteger('modified_by_id')->nullable();

            $table->timestamps();
            $table->softDeletes();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_details');
        Schema::dropIfExists('localization');
        Schema::dropIfExists('datetimelist');
        Schema::dropIfExists('tax_rates');
        Schema::dropIfExists('user_invoice_settings');
        Schema::dropIfExists('accounting');
    }
}
