<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StipeBillingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stripe_billing', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('team_id')->nullable();  //
            $table->unsignedInteger('frequency')->default(3); //3 = monthly

            $table->string('signingsecret')->nullable();
            $table->string('webhook')->nullable();

            $table->unsignedInteger('created_by_id')->nullable();
            $table->unsignedInteger('modified_by_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stripe_billing');
    }
}
