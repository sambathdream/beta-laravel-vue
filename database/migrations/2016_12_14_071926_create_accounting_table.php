<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger('team_id')->nullable();
            $table->unsignedInteger('open_id')->nullable();

            $table->string('category')->nullable();
            $table->string('txn_type', 1)->nullable();      //D: debit C: credit
            $table->date('txn_date')->nullable();

            $table->unsignedInteger('gl_account_id')->nullable(); //gl_account id
            $table->decimal('txn_amount', 13, 2)->default(0);//

            $table->string('description')->nullable();
            $table->string('ref')->nullable();  //do we need this now as we specific ref fields?
            $table->string('flag', 1)->default('N');  //P: partial F: Full N: Not cleared

            $table->unsignedInteger('created_by_id')->nullable();
            $table->unsignedInteger('modified_by_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });

        Schema::create('gl_accounts', function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger('team_id')->nullable();
            $table->unsignedInteger('open_id')->nullable();

            $table->unsignedInteger('type')->nullable(); //rename this to type
            
            $table->string('name')->nullable();

            $table->decimal('int_balance', 13, 2)->default(0); //internal balance
            $table->decimal('ext_balance', 13, 2)->default(0); //external balance

            $table->string('description')->nullable();

            $table->unsignedInteger('created_by_id')->nullable();
            $table->unsignedInteger('modified_by_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });

        Schema::create('gl_types', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('team_id')->nullable();

            $table->string('primary')->nullable();
            $table->string('sub')->nullable();

        });

        Schema::create('txn_categories', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('team_id')->nullable();

            $table->string('gl_type_id')->nullable();
            $table->string('text')->nullable();

        });

        Schema::create('txn_types', function (Blueprint $table) {

            $table->string('type', 1)->nullable(); //C or D
            $table->string('text')->nullable();

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
        Schema::dropIfExists('gl_accounts');
        Schema::dropIfExists('gl_types');

        Schema::dropIfExists('txn_categories');
        Schema::dropIfExists('txn_types');
    }
}
