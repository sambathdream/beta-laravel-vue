<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditnoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_credits', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('team_id')->nullable();  //from the team table (these are customer of ours - pi.team)
            $table->unsignedInteger('open_id')->nullable();

            $table->unsignedInteger('contact_id')->nullable(); //from the contacts table

            $table->unsignedInteger('status_id')->default(1);

            $table->string('title')->nullable();
            $table->string('summary')->nullable();

            $table->string('customer_name')->nullable();
            $table->string('customer_email')->nullable();
            $table->string('poso_number')->nullable();
            $table->date('post_date')->nullable();
            $table->date('due_date')->nullable();

            $table->boolean('is_recurring')->default(false);
            $table->unsignedInteger('recurring_id')->nullable();
            $table->unsignedInteger('frequency_id')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->timestamp('last_sent_date')->nullable();

            $table->boolean('discount_type')->default(false);   //false means % and true means amount
            $table->double('discount_rate1', 8, 2)->default(0);

            $table->decimal('sub_total', 13, 2)->default(0);
            $table->decimal('discount_amount', 13, 2)->default(0);
            $table->decimal('tax_amount', 13, 2)->default(0);

            $table->decimal('total_amount', 13, 2)->default(0);
            $table->decimal('amount_paid', 13, 2)->default(0);
            $table->decimal('balance', 13, 2)->default(0);

            $table->decimal('total_amount_local', 13, 2)->default(0);
            $table->decimal('amount_paid_local', 13, 2)->default(0);
            $table->decimal('balance_local', 13, 2)->default(0);

            $table->string('currency_id',3)->nullable();
            $table->string('currency_id_local',3)->nullable();
            $table->decimal('currency_rate', 13, 3)->default(0); //rate during invoice create

            $table->unsignedInteger('credit_gl')->nullable(); //gl_account id
            $table->boolean('debit_gl_cb')->default(false);

            $table->text('terms')->nullable();
            $table->text('notes')->nullable();
            $table->text('footer')->nullable();

            $table->unsignedInteger('created_by_id')->nullable();
            $table->unsignedInteger('modified_by_id')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->decimal('tax_amount_local', 13, 2)->default(0);
            $table->string('stripe_id',50)->nullable();
            $table->decimal('ded_amount', 13, 2)->default(0);
            $table->string('encrypted_id',50)->nullable();

        });

        Schema::create('user_credit_items', function (Blueprint $table)
        {
            $table->increments('id');

            $table->unsignedInteger('team_id')->nullable();  //from the company table (these are customer of ours - pi.team)
            $table->unsignedInteger('creditnote_num')->nullable(); //invoice no. not visible to user

            $table->string('item_id')->nullable();
            $table->text('item_name')->nullable();
            $table->decimal('item_price', 13, 2)->default(0);
            $table->decimal('item_qty', 13, 2)->default(0);
            $table->decimal('item_total', 13, 2)->default(0);

            $table->boolean('item_taxable')->default(true);

            $table->unsignedInteger('debit_gl')->nullable(); //gl_account id

            $table->timestamps();
            $table->softDeletes();

        });

        Schema::create('user_credit_taxes', function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger('team_id')->nullable();  //from the company table (these are customer of ours - pi.team)
            $table->unsignedInteger('creditnote_num')->nullable(); //invoice no. not visible to user

            $table->unsignedInteger('tax_open_id')->nullable();

            $table->string('tax_id')->nullable();
            $table->string('tax_name')->nullable();
            $table->decimal('tax_rate', 13, 3)->default(0);
            $table->decimal('tax_amount', 13, 2)->default(0);

            $table->unsignedInteger('tax_gl')->nullable(); //gl_account id

            $table->boolean('tax_display')->default(false);
            $table->boolean('recoverable')->default(false);
            $table->boolean('compound')->default(false);

            $table->timestamps();
            $table->softDeletes();

            $table->string('type',3)->nullable(); //TAX, DED
            $table->boolean('exclude_tax')->default(false);

        });

        Schema::table('team_details', function (Blueprint $table) {
            //
            $table->text('credit_footer')->nullable();
            $table->text('credit_notes')->nullable();
            $table->text('credit_header')->nullable();
            $table->text('credit_left_content')->nullable();

        });

        Schema::create('user_refunds', function($table)
        {
            $table->increments('id');

            $table->unsignedInteger('team_id')->nullable();  //from the team table (these are customer of ours - pi.team)
            $table->unsignedInteger('open_id')->nullable();

            $table->unsignedInteger('contact_id')->nullable(); //from the contacts table

            $table->unsignedInteger('creditnote_num')->index(); //id from the invoice table and not the invoice number

            $table->unsignedInteger('user_gateway_id')->nullable();
            $table->unsignedInteger('payment_type_id')->nullable();

            $table->decimal('amount', 13, 2)->default(0);
            $table->decimal('amount_local', 13, 2)->default(0);

            $table->string('transaction_reference')->nullable();
            $table->string('payer_id')->nullable();

            $table->date('payment_date')->nullable();

            $table->unsignedInteger('created_by_id')->nullable();
            $table->unsignedInteger('modified_by_id')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->string('stripe_charge_id',50)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_credits');
        Schema::dropIfExists('user_credit_items');
        Schema::dropIfExists('user_credit_taxes');

        Schema::table('team_details', function (Blueprint $table) {
            //
            $table->dropColumn('credit_footer');
            $table->dropColumn('credit_notes');
            $table->dropColumn('credit_header');
            $table->dropColumn('credit_left_content');

        });

        Schema::dropIfExists('user_refunds');
    }
}
