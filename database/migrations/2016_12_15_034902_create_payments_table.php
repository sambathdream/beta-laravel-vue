<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('gateways', function($table)
        {
            $table->increments('id');

            $table->string('name')->nullable();
            $table->string('provider')->nullable();
            $table->boolean('visible')->default(true)->nullable();

            $table->timestamps();
            $table->softDeletes();

        });

        Schema::create('user_gateways', function($table)
        {
            $table->increments('id');

            $table->unsignedInteger('team_id')->nullable();
            $table->unsignedInteger('open_id')->nullable();
            
            $table->unsignedInteger('gateway_id')->nullable();  //id from the gateway table

            $table->text('gate_data')->nullable();

            $table->boolean('status')->default(false)->nullable();

            $table->unsignedInteger('created_by_id')->nullable();
            $table->unsignedInteger('modified_by_id')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
        
        Schema::create('user_payments', function($table)
        {
            $table->increments('id');

            $table->unsignedInteger('team_id')->nullable();  //from the team table (these are customer of ours - pi.team)
            $table->unsignedInteger('open_id')->nullable();

            $table->unsignedInteger('contact_id')->nullable(); //from the contacts table

            $table->unsignedInteger('invoice_id')->index(); //id from the invoice table and not the invoice number

            $table->unsignedInteger('user_gateway_id')->nullable();
            $table->unsignedInteger('payment_type_id')->nullable();

            $table->decimal('amount', 13, 2)->default(0);
            $table->decimal('amount_local', 13, 2)->default(0);

            $table->string('transaction_reference')->nullable();
            $table->string('payer_id')->nullable();

            $table->date('payment_date')->nullable();

            $table->unsignedInteger('created_by_id')->nullable();
            $table->unsignedInteger('modified_by_id')->nullable();

            $table->timestamps();
            $table->softDeletes();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gateways');
        Schema::dropIfExists('user_gateways');
        Schema::dropIfExists('user_payments');
    }
}
