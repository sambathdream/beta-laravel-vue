<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $this->call('CountriesSeeder');
        $this->command->info('Seeded the countries!');

        DB::table('datetimelist')->insert(['carbon' => '', 'name' => '31/Dec/2017']);
        DB::table('datetimelist')->insert(['carbon' => '', 'name' => '31-Dec-2017']);
        DB::table('datetimelist')->insert(['carbon' => '', 'name' => '31/December/2017']);
        DB::table('datetimelist')->insert(['carbon' => '', 'name' => '31-December-2017']);
        DB::table('datetimelist')->insert(['carbon' => '', 'name' => 'Dec 31, 2017']);
        DB::table('datetimelist')->insert(['carbon' => '', 'name' => 'December 31, 2017']);
        DB::table('datetimelist')->insert(['carbon' => '', 'name' => 'Sun Dec 31st, 2017']);
        DB::table('datetimelist')->insert(['carbon' => '', 'name' => '2017-12-31']);
        DB::table('datetimelist')->insert(['carbon' => '', 'name' => '31-12-2017']);
        DB::table('datetimelist')->insert(['carbon' => '', 'name' => '12/31/2017']);
        DB::table('datetimelist')->insert(['carbon' => '', 'name' => '31.12.2017']);
        DB::table('datetimelist')->insert(['carbon' => '', 'name' => '31. Dec. 2017']);
        DB::table('datetimelist')->insert(['carbon' => '', 'name' => '31. December 2017']);

        $this->command->info('Seeded the date time list!');

        DB::table('gl_types')->insert(['primary' => 'Assets', 'sub' => 'Accounts Receivable']);
        DB::table('gl_types')->insert(['primary' => 'Assets', 'sub' => 'Inventory']);
        DB::table('gl_types')->insert(['primary' => 'Assets', 'sub' => 'Other Current Asset']);
        DB::table('gl_types')->insert(['primary' => 'Assets', 'sub' => 'Fixed Asset']);
        DB::table('gl_types')->insert(['primary' => 'Assets', 'sub' => 'Other Asset']);
        DB::table('gl_types')->insert(['primary' => 'Liabilities', 'sub' => 'Long Term Liability']);
        DB::table('gl_types')->insert(['primary' => 'Liabilities', 'sub' => 'Other Current Liability']);
        DB::table('gl_types')->insert(['primary' => 'Liabilities', 'sub' => 'Credit Card']);
        DB::table('gl_types')->insert(['primary' => 'Liabilities', 'sub' => 'Accounts Payable']);
        DB::table('gl_types')->insert(['primary' => 'Equity', 'sub' => 'Equity']);
        DB::table('gl_types')->insert(['primary' => 'Income', 'sub' => 'Income']);
        DB::table('gl_types')->insert(['primary' => 'Expense', 'sub' => 'Cost of Goods']);
        DB::table('gl_types')->insert(['primary' => 'Expense', 'sub' => 'Expenses']);
        DB::table('gl_types')->insert(['primary' => 'Other Income and Expense', 'sub' => 'Other Income']);
        DB::table('gl_types')->insert(['primary' => 'Other Income and Expense', 'sub' => 'Other Expense']);

        /*
        DB::table('gl_types')->insert(['primary' => 'Revenue', 'sub' => 'Sales']);
        DB::table('gl_types')->insert(['primary' => 'Revenue', 'sub' => 'Service']);
        */

        $this->command->info('Seeded the gl type list!');

        DB::table('txn_types')->insert(['type' => 'C', 'text' => 'Credit']);
        DB::table('txn_types')->insert(['type' => 'D', 'text' => 'Debit']);

        $this->command->info('Seeded the txn type list!');


        DB::table('txn_categories')->insert(['text' => 'Application Product']);
        DB::table('txn_categories')->insert(['text' => 'Hardware Product']);
        DB::table('txn_categories')->insert(['text' => 'Software Product']);
        DB::table('txn_categories')->insert(['text' => 'Other Product']);

        DB::table('txn_categories')->insert(['text' => 'Application Service']);
        DB::table('txn_categories')->insert(['text' => 'Hardware Service']);
        DB::table('txn_categories')->insert(['text' => 'Software Service']);
        DB::table('txn_categories')->insert(['text' => 'Other Service']);

        DB::table('txn_categories')->insert(['text' => 'Application Asset']);
        DB::table('txn_categories')->insert(['text' => 'Hardware Asset']);
        DB::table('txn_categories')->insert(['text' => 'Software Asset']);
        DB::table('txn_categories')->insert(['text' => 'Other Asset']);

        DB::table('txn_categories')->insert(['text' => 'Other']);

        DB::table('txn_categories')->insert(['text' => 'Create bill payment']);
        DB::table('txn_categories')->insert(['text' => 'Accounting Fees']);
        DB::table('txn_categories')->insert(['text' => 'Advertising & Promotion']);
        DB::table('txn_categories')->insert(['text' => 'Bank Service Charges']);
        DB::table('txn_categories')->insert(['text' => 'Computer - Hardware']);
        DB::table('txn_categories')->insert(['text' => 'Computer - Hosting']);
        DB::table('txn_categories')->insert(['text' => 'Computer - Internet']);
        DB::table('txn_categories')->insert(['text' => 'Computer - Software']);
        DB::table('txn_categories')->insert(['text' => 'Dues & Subscriptions']);
        DB::table('txn_categories')->insert(['text' => 'Education & Training']);
        DB::table('txn_categories')->insert(['text' => 'Insurance - General Liability']);
        DB::table('txn_categories')->insert(['text' => 'Insurance - Vehicles']);
        DB::table('txn_categories')->insert(['text' => 'Interest Expense']);
        DB::table('txn_categories')->insert(['text' => 'Meals & Entertainment']);
        DB::table('txn_categories')->insert(['text' => 'Office Supplies']);
        DB::table('txn_categories')->insert(['text' => 'Payroll - Employee Expenses Paid']);
        DB::table('txn_categories')->insert(['text' => 'Payroll Services']);
        DB::table('txn_categories')->insert(['text' => 'Payroll - Employee Benefits']);
        DB::table('txn_categories')->insert(['text' => 'Payroll - Employers Share of Benefits']);
        DB::table('txn_categories')->insert(['text' => 'Payroll - Salary & Wages']);
        DB::table('txn_categories')->insert(['text' => 'Professional Fees']);
        DB::table('txn_categories')->insert(['text' => 'Rent Expense']);
        DB::table('txn_categories')->insert(['text' => 'Repairs & Maintenance']);
        DB::table('txn_categories')->insert(['text' => 'Telephone - Land Line']);
        DB::table('txn_categories')->insert(['text' => 'Telephone - Wireless']);
        DB::table('txn_categories')->insert(['text' => 'Travel Expense']);
        DB::table('txn_categories')->insert(['text' => 'Uncategorized Expense']);
        DB::table('txn_categories')->insert(['text' => 'Utilities']);
        DB::table('txn_categories')->insert(['text' => 'Vehicle - Fule']);
        DB::table('txn_categories')->insert(['text' => 'Vehicle - Repairs & Maintenance']);
        DB::table('txn_categories')->insert(['text' => 'Consulting Income']);
        DB::table('txn_categories')->insert(['text' => 'Payroll - Employee']);
        DB::table('txn_categories')->insert(['text' => 'Payments Received']);
        DB::table('txn_categories')->insert(['text' => 'Sales']);
        DB::table('txn_categories')->insert(['text' => 'Uncategorized Income']);
        DB::table('txn_categories')->insert(['text' => 'Owner Investment / Drawings']);
        DB::table('txn_categories')->insert(['text' => 'Owners Equity']);
        DB::table('txn_categories')->insert(['text' => 'Payroll Holding Account']);
        DB::table('txn_categories')->insert(['text' => 'Payroll Liabilities']);
        /**/

        $this->command->info('Seeded the txn category list!');

        DB::table('item_types')->insert(['text' => 'Product']);
        DB::table('item_types')->insert(['text' => 'Service']);
        DB::table('item_types')->insert(['text' => 'Asset']);
        DB::table('item_types')->insert(['text' => 'Other']);

        $this->command->info('Seeded the item_types list!');

        DB::table('item_categories')->insert(['text' => 'Application']);
        DB::table('item_categories')->insert(['text' => 'Hardware']);
        DB::table('item_categories')->insert(['text' => 'Software']);
        DB::table('item_categories')->insert(['text' => 'Other']);

        $this->command->info('Seeded the item_categories list!');

        DB::table('user_invoice_terms')->insert([ 'days' => '0', 'text' => 'Due Immediately']);
        DB::table('user_invoice_terms')->insert([ 'days' => '3', 'text' => 'Due in 3 Days']);
        DB::table('user_invoice_terms')->insert([ 'days' => '5', 'text' => 'Due in 5 Days']);
        DB::table('user_invoice_terms')->insert([ 'days' => '7', 'text' => 'Due in 7 Days']);
        DB::table('user_invoice_terms')->insert([ 'days' => '10', 'text' => 'Due in 10 Days']);
        DB::table('user_invoice_terms')->insert([ 'days' => '15', 'text' => 'Due in 15 Days']);
        DB::table('user_invoice_terms')->insert([ 'days' => '30', 'text' => 'Due in 30 Days']);
        DB::table('user_invoice_terms')->insert([ 'days' => '45', 'text' => 'Due in 45 Days']);
        DB::table('user_invoice_terms')->insert([ 'days' => '60', 'text' => 'Due in 60 Days']);

        $this->command->info('Seeded the user_invoice_terms list!');

        DB::table('user_invoice_status')->insert([ 'id' => '0','text' => 'Draft']);
        DB::table('user_invoice_status')->insert([ 'id' => '1','text' => 'Created']);
        DB::table('user_invoice_status')->insert([ 'id' => '2','text' => 'Sent']);
        DB::table('user_invoice_status')->insert([ 'id' => '3','text' => 'Overdue']);
        DB::table('user_invoice_status')->insert([ 'id' => '4','text' => 'Paid']);
        DB::table('user_invoice_status')->insert([ 'id' => '5','text' => 'Recurring']);
        DB::table('user_invoice_status')->insert([ 'id' => '6','text' => 'Open Bill']); //stripe
        DB::table('user_invoice_status')->insert([ 'id' => '7','text' => 'Partially Paid']); //stripe
        DB::table('user_invoice_status')->insert([ 'id' => '8','text' => 'Refunded']); //stripe
        //

        $this->command->info('Seeded the user_invoice_status list!');

    }
}
