<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //      factory(App\Company::class,15)->create();
        //      factory(App\User::class,50)->create();
        factory(App\Contact::class,200)->create();

        //Country seeder
        //$this->call('CountriesSeeder');
        //$this->command->info('Seeded the countries!');
    }
}
