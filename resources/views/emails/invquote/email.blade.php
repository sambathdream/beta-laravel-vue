<!doctype html>
<html>
<head>
    <title>HTML Editor - Full Version</title>
</head>
<body>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
    <tr>
        <td bgcolor="#f8f8f8" valign="top" width="100%">
            <table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" style="border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1;border-top:1px solid #e1e1e1;margin-top:50px" width="460">
                <tbody>
                <tr>
                    <td width="460">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="460">
                            <tbody>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>

                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="460">
                            <tbody>
                            <tr>
                                <td width="40">&nbsp;</td>
                                <td style="font-size:16px;color:#b8b9c1;font-weight:normal;text-align:center;font-family:Helvetica,Arial,sans-serif;line-height:24px;vertical-align:top" width="460">&nbsp;</td>
                                <td width="40">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="40">&nbsp;</td>
                                <td style="font-size:16px;color:#b8b9c1;font-weight:normal;text-align:center;font-family:Helvetica,Arial,sans-serif;line-height:24px;vertical-align:top" width="460"><span style="text-decoration:none;color:#2f2f36;font-weight:bold;font-size:32px;line-height:32px">Invoice #3</span></td>
                                <td width="40">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="40">&nbsp;</td>
                                <td style="font-size:16px;color:#a0a0a5;font-weight:normal;text-align:center;font-family:Helvetica,Arial,sans-serif;line-height:24px;vertical-align:top" width="460">for test customer<br />
                                    issued on Dec. 9, 2016<br />
                                    from <b>test business</b></td>
                                <td width="40">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>



            <table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" style="border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1" width="460">
                <tbody>
                <tr>
                    <td width="460">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="460">
                            <tbody>
                            <tr>
                                <td height="20">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>

                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="460">
                            <tbody>
                            <tr>
                                <td width="40">&nbsp;</td>
                                <td class="" style="font-size:14px;color:#444;font-weight:normal;text-align:left;font-family:Helvetica,Arial,sans-serif;line-height:24px;vertical-align:top" width="510">
                                    <p style="font-size:18px;border-top:1px solid #e1e1e1;border-bottom:1px solid #e1e1e1;padding:10px 0;background:#fefefe;text-align:center;margin:5px 0">Total Due: <span style="white-space:nowrap;font-weight:bold;font-size:18px">$100.00 USD</span></p>
                                </td>
                                <td width="40">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>

            <table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="" style="border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1" width="460">
                <tbody>
                <tr>
                    <td class="" width="460">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="460">
                            <tbody>
                            <tr>
                                <td width="40">&nbsp;</td>
                                <td class="" style="font-size:14px;color:#a0a0a5;font-weight:normal;text-align:center;font-family:Helvetica,Arial,sans-serif;line-height:24px;vertical-align:top" width="510">
                                    <div><a href="https://finance.pi.team/browser"
                                            style="background-color:#5fbeaa;border-radius:4px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:16px;font-weight:bold;line-height:40px;text-align:center;text-decoration:none;width:200px"
                                            target="_blank">View in browser </a></div>

                                    <p style="margin-top:3px;color:#444">Due by: <span style="white-space:nowrap;font-weight:bold;font-size:14px">Dec 09, 2016</span></p>
                                </td>
                                <td width="40">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="40">&nbsp;</td>
                                <td class="" height="10" width="512">&nbsp;</td>
                                <td width="40">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>

            <table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" style="border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1" width="460">
                <tbody>
                <tr>
                    <td width="460">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="460">
                            <tbody>
                            <tr>
                                <td height="10" width="460">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>

            <table align="center" bgcolor="#f9f9f9" border="0" cellpadding="0" cellspacing="0" class="" style="border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1;border-top:1px solid #e1e1e1" width="460">
                <tbody>
                <tr>
                    <td class="" width="460">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="460">
                            <tbody>
                            <tr>
                                <td class="" height="10">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>

                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="460">
                            <tbody>
                            <tr>
                                <td width="40">&nbsp;</td>
                                <td class="" style="font-size:16px;color:#b8b9c1;font-weight:normal;text-align:center;font-family:Helvetica,Arial,sans-serif;line-height:24px;vertical-align:top" width="460">&nbsp;</td>
                                <td width="40">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="40">&nbsp;</td>
                                <td class="" style="font-size:14px;color:#959599;font-weight:normal;font-family:Helvetica,Arial,sans-serif;line-height:20px;text-align:center" width="460">
                                    <p class="">Thanks for your business. If this invoice was sent in error, please contact <a href="mailto:fasterfene86@gmail.com" style="text-decoration:none;color:#008f9b;font-weight:bold" target="_blank">fasterfene86@gmail.<span class="il">com</span></a></p>
                                </td>
                                <td width="40">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="40">&nbsp;</td>
                                <td class="" height="10" width="512">&nbsp;</td>
                                <td width="40">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>

            <table align="center" bgcolor="#f0f0f0" border="0" cellpadding="0" cellspacing="0" style="border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1;border-top:1px solid #e1e1e1" width="462">
                <tbody>
                <tr>
                    <td height="10" width="462">&nbsp;</td>
                </tr>
                </tbody>
            </table>

            <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1" width="460">
                <tbody>
                <tr>
                    <td bgcolor="#f0f0f0" class="" width="460">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="460">
                            <tbody>
                            <tr>
                                <td width="460">
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="460">
                                        <tbody>
                                        <tr>
                                            <td width="30">&nbsp;</td>
                                            <td width="530">
                                                <table align="center" border="0" cellpadding="0" cellspacing="0" class="">
                                                    <tbody>
                                                    <tr>
                                                        <td class="" height="0" style="font-size:14px;color:#b8b9c1;font-weight:normal;font-family:Helvetica,Arial,sans-serif;line-height:24px"><a
                                                                href="https://finance.pi.team/invoice/?"
                                                                style="text-decoration:none;color:#00929f" target="_blank">Print</a> &nbsp;|&nbsp;
                                                            <a href="https://finance.pi.team/invoice/2/pdf"
                                                               style="text-decoration:none;color:#00929f" target="_blank">PDF</a></td>
                                                    </tr>
                                                    </tbody>
                                                </table>

                                                <table align="left" border="0" cellpadding="0" cellspacing="0" class="" width="140">
                                                    <tbody>
                                                    <tr>
                                                        <td width="140">&nbsp;</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td width="30">&nbsp;</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>

            <table align="center" border="0" cellpadding="0" cellspacing="0" class="" width="460">
                <tbody>
                <tr>
                    <td class="" style="border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1;border-bottom:1px solid #e1e1e1;border-radius:0 0 10px 10px;background:#f0f0f0" width="460">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="460">
                            <tbody>
                            <tr>
                                <td height="14">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>

            <table align="center" border="0" cellpadding="0" cellspacing="0" width="460">
                <tbody>
                <tr>
                    <td height="20">&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align:center"><strong><span style="font-size:12px;"><span style="font-family:arial,helvetica,sans-serif;"><a href="https://finance.pi.team" target="_blank">Powered by Pi.TEAM</span></span></strong></td>
                </tr>
                <tr>
                    <td height="40" style="line-height:1px">&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
