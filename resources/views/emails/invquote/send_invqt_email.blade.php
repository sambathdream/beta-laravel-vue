
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <title></title>
    <!-- <style> - font -->
    <style type="text/css">
        @import url('https://fonts.googleapis.com/css?family=Montserrat:400,600');
    </style>


    <!-- <style> - main -->
    <style type="text/css">

        .btn-orange:hover {
            background : #de553a !important;

        }
        .footer-btns a:hover {
            background: #c9d0d6 !important;
        }

        .social-icons a:hover {
            opacity:  1 !important;
        }
        .clearfix:after {
            visibility: hidden;
            display: block;
            font-size: 0;
            content: " ";
            clear: both;
            height: 0;
        }
        .clearfix { display: inline-block; }
        /* start commented backslash hack \*/
        * html .clearfix { height: 1%; }
        .clearfix { display: block; }
    </style>

</head>
<body style="font-family: 'Montserrat', sans-serif, Arial;background: #f5f8fa;padding: 30px;margin: 0;">
<div class="white-wrap" style="background: #fff;padding: 50px 25px;max-width: 700px;margin: auto;color: #868686;">

    <div style="text-align:center">

        <h1 style="font-size: 40px;color: #414042;font-weight: 400;margin: 0;">{{ $type }} # {{ $invoice->open_id }}</h1>
        <p class="big" style="font-size: 20px;line-height: 28px;">For {{ $invoice->contactTab->name }} issued
            on {{ (Carbon\Carbon::parse($invoice->post_date))->formatLocalized('%B %d, %Y') }}<br>
            from <b style="color: #414042;font-weight: bold;">{{ $team_name }}</b></p>

        <div class="invoice-total" style="background: #e9f0f5;display: block;max-width: 400px;margin: 20px auto 30px auto;color: #414042;font-size: 18px;border: 1px solid #e1e7ec;line-height: 24px;padding: 20px;font-weight: bold;">
            Total @if($type == 'Invoice' and $invoice->status_id != 8)
                Due
            @elseif($type == 'Quote')
                Quote
            @elseif($type == 'CreditNote')
                Credit Note
            @endif
            Amount: {{ $inv_country->currency_symbol }}
            @if($type == 'CreditNote')
                {{ number_format($invoice->total_amount, 2, '.', ',') }}
            @else
                {{ number_format($invoice->balance, 2, '.', ',') }}
            @endif
        </div>


        <a class="btn-orange" href="{{ $share_url }}" title="View in Browser" target="_blank" style="display: inline-block;background: #ed583b;color: #fff;font-size: 16px;line-height: 30px;padding: 12px 10px;min-width: 150px;text-decoration: none;border-radius: 4px;">View in Browser</a>


        <p class="bigger" style="font-size: 22px;line-height: 34px;margin-top: 30px;">Thanks for your business. If this was sent in error,<br>
            please contact <b style="color: #414042;font-weight: bold;"><a href="mailto:{{ $from_email }}">{{ $from_email }}</a></b></p>

        <div class="divider" style="padding: 5px;height: 1px;"></div>

        @if($email_message)
            <div class="usr-messagebox clearfix" style="display: block;margin: 15px;text-align: left;">
                <div class="usr-details" style="background: #f7f7f7;border: 1px solid #f7f7f7;padding: 10px;line-height: 30px;min-height: 60px;">
                    <img src="http://via.placeholder.com/50x50" class="usr-pic" alt="" style="border-radius: 50%;float: left;margin: 5px 10px;">
                    <h4 style="font-size: 19px;color: #414042;margin: 5px 0;display: inline-block;float: left;line-height: 20px;">{{ $team_name }}</h4><br>
                    <div class="usr-date" style="color: #868686;display: inline-block;float: left;line-height: 20px;font-size: 19px;">{{ Carbon\Carbon::now()->formatLocalized('%B %d, %Y') }}</div>
                </div>
                <div class="usr-msg" style="color: #868686;font-size: 14px;line-height: 24px;border: 1px solid #dfdfdf;border-top: 0;float: left;width: 100%;">
                    <p style="padding:  0 30px;"><span class="bigp" style="display: inline-block;font-size: 22px;padding: 20px 0;color: #414042;">Hi there,</span><br>
                        {{ $email_message }}
                    </p>
                    <p style="padding:  0 30px;">
                        <span class="bigp" style="display: inline-block;font-size: 22px;padding: 20px 0;color: #414042;">Have a great day!</span></p>
                </div>
            </div>
        @endif

        <!-- commented--
            <a class="btn-orange btn-big" href="http://golubic.co" title="Reply" target="_blank">Reply on Pi.Team</a>
        commented-- -->
        <div class="footer-btns">
            <a href="{{ $share_url }}" target="_blank" style="display: inline-block;border-radius: 3px;background: #aab1b7;color: #fff;font-size: 13px;text-decoration: none;margin-top: 10px;margin-right: 4px;margin-left: 4px;line-height: 38px;min-width: 140px;font-weight: bold;">PRINT</a>
            <a href="{{ $download_url }}" target="_blank" style="display: inline-block;border-radius: 3px;background: #aab1b7;color: #fff;font-size: 13px;text-decoration: none;margin-top: 10px;margin-right: 4px;margin-left: 4px;line-height: 38px;min-width: 140px;font-weight: bold;">PDF</a>
            @if($type == 'Invoice' and $invoice->status_id != 8)
                <a href="{{ $share_url }}" target="_blank" style="display: inline-block;border-radius: 3px;background: #aab1b7;color: #fff;font-size: 13px;text-decoration: none;margin-top: 10px;margin-right: 4px;margin-left: 4px;line-height: 38px;min-width: 140px;font-weight: bold;">MAKE PAYMENT</a>
            @endif
        </div>
    </div>
</div>
<div class="footer" style="text-align: center;padding: 35px 10px;">
    <div class="footer-hdr" style="color: #1a1a1a;letter-spacing: .14em;font-size: 17px;text-transform: uppercase;">Powered by</div>
    <div style="text-align:center">
        <div class="logo" style="margin-bottom: 10px;margin-top: 10px;">
            <a href="https://www.pi.team" target="_blank">
                <img width="175" height="40" src="https://www.pi.team/img/piteam_logo.min.png" alt="">
            </a>
        </div>
    </div>
    <p class="footer-par" style="color: #868686;font-size: 15px;">Get Pi.TEAM updates, small business tips and stay in touch with us.</p>
    <div class="social-icons">
        <a href="https://www.facebook.com/Pi.TeamOfficial/" target="_blank" style="display: inline-block;margin: 10px 12px;opacity: .8;">
            <img width="22" height="21" src="http://i.imgur.com/f0YZZv3.jpg" alt="">
        </a>
        <a href="https://www.linkedin.com/company/pi.team" target="_blank" style="display: inline-block;margin: 10px 12px;opacity: .8;">
            <img width="22" height="21" src="http://i.imgur.com/fdP8DIY.jpg" alt="">
        </a>
        <a href="https://twitter.com/piteamofficial" target="_blank" style="display: inline-block;margin: 10px 12px;opacity: .8;">
            <img width="22" height="21" src="http://i.imgur.com/9SMTdt0.jpg" alt="">
        </a>
    </div>
    <div class="footer-tiny" style="background:  #e8ecef;color: #9ba2aa;padding:  20px 40px;text-align: center;font-size: 12px;max-width:  670px;margin: auto;margin-top:  10px;">
        This email is not for marketing purposes; it
        contains information about your transactions with {{ $team_name }}. If you previously opted out of marketing
        communications from us, those preferences do not apply to this email. We are located at AvanSaber Inc 2035
        Sunset Lake
        Road Suite B-2 Newark, DE 19702 USA &amp; AvanSaber Technologies Pvt Ltd C1001 Kool Home Bavdhan Pune, MH
        411 021 INDIA. You can still <a href="<%asm_group_unsubscribe_raw_url%>" target="_blank">unsubscribe</a>.
        You can also update your email <a href="<%asm_preferences_raw_url%>">preference here.</a>
    </div>
</div>

</body>
</html>