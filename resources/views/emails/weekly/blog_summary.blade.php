<h3 style="font-size: 26px;line-height: 28px;color: #414042;font-weight: 400;margin: 0;padding: 10px 0;">Do you really need a CFO for your small business?</h3>
<p class="big" style="margin: 10px 0;font-size: 20px;line-height: 28px;">There are certain roles that a CFO plays in the business as the owner of a small business cannot be present nor handle each and everything ...</p>
<!--
<p class="author" style="margin: 10px 0;color: #414042;padding: 10px 0;font-weight: bold;">Chris Dixon, Co-Founder Hunch</p>
-->
<a class="btn-orange" href="https://www.pi.team/blog/2017/07/21/really-need-cfo-small-business/" title="Read More" target="_blank" style="display: inline-block;background: #ed583b;color: #fff;font-size: 16px;line-height: 30px;padding: 12px 10px;min-width: 150px;text-decoration: none;border-radius: 4px;">Read More</a>