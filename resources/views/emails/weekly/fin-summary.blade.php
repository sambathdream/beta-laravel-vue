
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <title></title>
    <!-- <style> - font -->
    <style type="text/css">
        @import url('https://fonts.googleapis.com/css?family=Montserrat:400,600');
    </style>
    <!-- <style> - main -->
    <style type="text/css">

        .btn-orange:hover {
            background : #de553a !important;

        }
        .footer-btns a:hover {
            background: #e6e8ea !important;
        }

        .social-icons a:hover {
            opacity:  1 !important;
        }
    </style>

</head>
<body style="font-family: 'Montserrat', sans-serif, Arial;background: #f5f8fa;padding: 30px;margin: 0;">
<div class="white-wrap" style="background: #fff;padding: 50px 25px;max-width: 700px;margin: auto;color: #868686;">
    <div style="text-align:center">
        <div class="logo" style="margin-bottom: 20px;">
            <img width="175" height="40" src="http://i.imgur.com/3fI7F4E.png" " alt="">
        </div>
        <h1 style="font-size: 40px;color: #414042;font-weight: 400;margin: 0;">Your week in numbers</h1>
        <h5 style="color: #e25438;font-size: 18px;margin: 10px 0;font-weight: 400;">{{ $lastweek_monday_human }} - {{ $lastweek_sunday_human }}</h5>

        <hr style="display: block;height: 1px;border: 0;border-top: 1px solid #f5f3f6;margin: 20px 0;padding: 0;">

        @include('emails.weekly.blog_summary')

        <div class="table-wrap" style="background: #fff;margin: 30px 0 30px 0;max-width: 100%;overflow: auto;box-shadow: 0px 10px 25px 0px rgba(0, 0, 0, 0.2);">
            <div class="table-hdr" style="background: #414042;color: #fff;padding: 20px 30px;line-height: 20px;"><a href="https://finance.pi.team/login" target="_blank" style="background:#414042;color:#fff;">{{ $team_name }}</a> - your business at a glance</div>
            <table style="table-layout: fixed;min-width: 100%;border-collapse: collapse;font-size: 14px;text-align: left;">
                <tr><td style="border: 1px solid #dfdfdf;padding: 20px 30px;line-height: 20px;background: #edf3f7;">INFO</td><td style="border: 1px solid #dfdfdf;padding: 20px 30px;line-height: 20px;background:#f5f8fa;">Last Week</td><td style="border: 1px solid #dfdfdf;padding: 20px 30px;line-height: 20px;background:#f5f8fa;">Month to date</td><td style="border: 1px solid #dfdfdf;padding: 20px 30px;line-height: 20px;background:#f5f8fa;">Year to date</td></tr>
                <tr><td style="border: 1px solid #dfdfdf;padding: 20px 30px;line-height: 20px;background: #edf3f7;">Invoices</td><td style="border: 1px solid #dfdfdf;padding: 20px 30px;line-height: 20px;">{{ $comp_country->currency_symbol }} {{ number_format($sales_last_week,2) }}</td><td style="border: 1px solid #dfdfdf;padding: 20px 30px;line-height: 20px;">{{ $comp_country->currency_symbol }} {{ number_format($sales_this_month,2) }}</td><td style="border: 1px solid #dfdfdf;padding: 20px 30px;line-height: 20px;">{{ $comp_country->currency_symbol }} {{ number_format($sales_this_yr,2) }}</td></tr>
                <tr><td style="border: 1px solid #dfdfdf;padding: 20px 30px;line-height: 20px;background: #edf3f7;">Payment</td><td style="border: 1px solid #dfdfdf;padding: 20px 30px;line-height: 20px;background:#f5f8fa;">{{ $comp_country->currency_symbol }} {{ number_format($pay_last_week,2) }}</td><td style="border: 1px solid #dfdfdf;padding: 20px 30px;line-height: 20px;background:#f5f8fa;">{{ $comp_country->currency_symbol }} {{ number_format($pay_this_month,2) }}</td><td style="border: 1px solid #dfdfdf;padding: 20px 30px;line-height: 20px;background:#f5f8fa;">{{ $comp_country->currency_symbol }} {{ number_format($pay_this_yr,2) }}</td></tr>
                <tr><td style="border: 1px solid #dfdfdf;padding: 20px 30px;line-height: 20px;background: #edf3f7;">Due</td><td style="border: 1px solid #dfdfdf;padding: 20px 30px;line-height: 20px;">{{ $comp_country->currency_symbol }} {{ number_format($sales_last_week - $pay_last_week,2) }}</td><td style="border: 1px solid #dfdfdf;padding: 20px 30px;line-height: 20px;">{{ $comp_country->currency_symbol }} {{ number_format($sales_this_month - $pay_this_month,2) }}</td><td style="border: 1px solid #dfdfdf;padding: 20px 30px;line-height: 20px;">{{ $comp_country->currency_symbol }} {{ number_format($sales_this_yr - $pay_this_yr,2) }}</td></tr>
            </table>
        </div>


        <h3 style="font-size: 26px;line-height: 28px;color: #414042;font-weight: 400;margin: 0;padding: 10px 0;">Need help? We've got you!</h3>

        <p class="small" style="margin: 10px 0;font-size: 18px;line-height: 27px;">Visit Pi.TEAM's community space to see our FAQs, get advice from other<br>
            small business owners, or ask an accountant a question.</p>
        <a class="btn-orange" href="https://help.pi.team" title="Take me there!" target="_blank" style="display: inline-block;background: #ed583b;color: #fff;font-size: 16px;line-height: 30px;padding: 12px 10px;min-width: 150px;text-decoration: none;border-radius: 4px;">Take me there!</a>


    </div>
</div>
<div class="footer" style="text-align: center;padding: 35px 10px;">
    <div class="footer-hdr" style="color: #1a1a1a;letter-spacing: .14em;font-size: 17px;text-transform: uppercase;font-weight: bold;">Connect with us</div>
    <p class="footer-par" style="margin: 10px 0;color: #868686;font-size: 15px;">Get Pi.TEAM updates, small business tips and stay in touch with us.</p>
    <div class="social-icons">
        <a href="https://www.facebook.com/Pi.TeamOfficial/" target="_blank" style="display: inline-block;margin: 10px 12px;opacity: .8;">
            <img width="22" height="21" src="http://i.imgur.com/f0YZZv3.jpg" alt="">
        </a>
        <a href="https://www.linkedin.com/company/pi.team" target="_blank" style="display: inline-block;margin: 10px 12px;opacity: .8;">
            <img width="22" height="21" src="http://i.imgur.com/fdP8DIY.jpg" alt="">
        </a>
        <a href="https://twitter.com/piteamofficial" target="_blank" style="display: inline-block;margin: 10px 12px;opacity: .8;">
            <img width="22" height="21" src="http://i.imgur.com/9SMTdt0.jpg" alt="">
        </a>
    </div>
    <div class="footer-tiny" style="background: #e8ecef;color: #9ba2aa;padding: 20px 40px;text-align: center;font-size: 12px;max-width: 670px;margin: auto;margin-top: 10px;">
        This email is not for marketing purposes; it
        contains information about your transactions. If you previously opted out of marketing
        communications from us, those preferences do not apply to this email. We are located at AvanSaber Inc 2035
        Sunset Lake
        Road Suite B-2 Newark, DE 19702 USA &amp; AvanSaber Technologies Pvt Ltd C1001 Kool Home Bavdhan Pune, MH
        411 021 INDIA. You can still <a href="<%asm_group_unsubscribe_raw_url%>" target="_blank">unsubscribe</a>.
        You can also update your email <a href="<%asm_preferences_raw_url%>">preference here.</a>
    </div>
</div>

</body>
</html>