<!doctype html>
<title>Functionality Maintenance</title>
<style>
    body { text-align: center; padding: 150px; }
    h1 { font-size: 50px; }
    body { font: 20px Helvetica, sans-serif; color: #333; }
    article { display: block; text-align: left; width: 650px; margin: 0 auto; }
    a { color: #dc8100; text-decoration: none; }
    a:hover { color: #333; text-decoration: none; }
</style>

<article>
    <h1><a onclick="window.history.go(-1); return false;">Click To Go Back To Previous Page</a></h1>
    
    <h1>We&rsquo;ll be back soon!</h1>
    <div>
        <p>Sorry for the inconvenience but we&rsquo;re performing some maintenance for this specific functionality at the moment. If you need to you can always <a href="mailto:support@pi.team">contact us</a>, otherwise we&rsquo;ll be back online shortly! Meanwhile please feel free to press BACK to explore other functions.</p>
        <p>&mdash; The Team</p>
    </div>
</article>


