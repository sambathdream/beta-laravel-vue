@extends('spark::layouts.app')

@section('content')
    <div class="clearfix wrapper-page">
    

            <div class="card-box">
                <div class="panel-heading"><h3 class="text-center">Login</h3></div>

                <div class="panel-body">
                    @include('spark::shared.errors')

                    <form class="form-horizontal" role="form" method="POST" action="/login">
                        {{ csrf_field() }}

                        <!-- E-Mail Address -->
                        <div class="form-group">


                            <div class="col-xs-12">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus>
                            </div>
                        </div>

                        <!-- Password -->
                        <div class="form-group">


                            <div class="col-xs-12">
                                <input type="password" class="form-control" name="password">
                            </div>
                        </div>

                        <!-- Remember Me -->
                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="checkbox checkbox-primary">

                                        <input type="checkbox" name="remember"><label> Remember Me</label>
                                </div>
                            </div>
                        </div>

                        <!-- Login Button -->
                        <div class="form-group">
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-primary btn-block text-uppercase waves-effect waves-light">
                                    <i class="fa m-r-xs fa-sign-in"></i>Login
                                </button>


                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <a class="text-dark" href="{{ url('/password/reset') }}"><i class="fa fa-lock m-r-5"></i> Forgot Your Password?</a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>


</div>
@endsection
