<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta Information -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title', config('app.name'))</title>

    <link rel="icon" href="/img/cropped-pi-logo-32x32.png">
    <meta name="theme-color" content="#5fbeaa">

    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600' rel='stylesheet' type='text/css'>
    <link href='//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css' rel='stylesheet'
          type='text/css'>

    <!-- Loader by Nik -->
    <link href="/custom/css/loader.css" rel="stylesheet" type="text/css"/>
    <script src="/custom/js/pace.min.js"></script>

    <!-- CSS -->
    <link href="/css/sweetalert.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">

    <!-- custom CSS - added by Nik -->
    <link href="/assets/plugins/custombox/css/custombox.css" rel="stylesheet">
    <link href="/assets/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />

    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>

    <link href="/assets/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />

    <link href="/assets/css/core.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/css/components.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/css/icons.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/css/pages.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/css/responsive.css" rel="stylesheet" type="text/css"/>

    <script src="/assets/js/modernizr.min.js"></script>
    <!-- jQuery  - added by Nik -->
    <script src="/assets/js/jquery.min.js"></script>

    <!-- Tour css here -->

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tour/0.11.0/css/bootstrap-tour.min.css" rel="stylesheet" type="text/css" />

@yield('header') <!-- custom header - added by Nik -->

    <!-- Scripts -->
@yield('scripts', '')

<!-- Global Spark Object -->
    <script>
        window.Spark = <?php echo json_encode(array_merge(
            Spark::scriptVariables(), []
        )); ?>;
    </script>
</head>
<body class="fixed-left">
<!-- Begin page -->
<div id="wrapper">

    <div id="spark-app" v-cloak>
        <!-- Navigation -->

        <!-- Top Bar Start -->
        <div class="topbar">

            <!-- LOGO -->
            <div class="topbar-left">
                <div class="text-center">
                    <a href="/home" class="logo"><i class="icon-magnet icon-c-logo"></i><span>Pi<i
                                    class="md md-album"></i>TEAM</span></a>
                    <!-- Image Logo here -->
                    <!--<a href="index.html" class="logo">-->
                    <!--<i class="icon-c-logo"> <img src="assets/images/logo_sm.png" height="42"/> </i>-->
                    <!--<span><img src="assets/images/logo_light.png" height="20"/></span>-->
                    <!--</a>-->
                </div>
            </div>
            @if (Auth::check())
                @include('spark::nav.user')
            @else
                @include('spark::nav.guest')
            @endif
        </div>

    @if (Auth::check())
        <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        <ul>

                            <li class="text-muted menu-title">Navigation</li>

                            <li class="has_sub">
                                <a href="/dashboard" class="waves-effect"><i class="ti-dashboard"></i>
                                    <span> Dashboard </span></a>
                            </li>

                            <li class="has_sub" id="contacts-menu-tour">
                                <a href="/contacts" class="waves-effect"><i class="ti-user"></i>
                                    <span class="label label-primary">{{ App\Contact::where('team_id', Auth::user()->currentTeam->id)->count() }}</span>
                                    <span> Contacts </span></a>
                            </li>

                            <li class="has_sub" id="sales-menu-tour">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-layout-media-overlay-alt-2"></i>
                                    <span class="label label-danger">{{ App\UserInvoice::where('team_id', Auth::user()->currentTeam->id)->count() }}</span>
                                    <span> Sales & Invoices </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">

                                    <li><a href="/invoices">
                                            <span class="label label-danger">{{ App\UserInvoice::where('team_id', Auth::user()->currentTeam->id)->count() }}</span>
                                            Invoices</a></li>
                                    <li><a href="/invoices/create">
                                            Create Invoice</a></li>
                                    <li><a href="/payments">
                                            <span class="label label-warning">{{ App\UserPayment::where('team_id', Auth::user()->currentTeam->id)->count() }}</span>
                                            Payments</a></li>
                                    <li><a href="/invoices/recurring">
                                            <span class="label label-success">{{ App\UserRecInvoice::where('team_id', Auth::user()->currentTeam->id)->count() }}</span>
                                            Recurring Invoices</a></li>

                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-write"></i>
                                    <span class="label label-info">{{ App\UserQuote::where('team_id', Auth::user()->currentTeam->id)->count() }}</span>
                                    <span> Quotations </span>
                                    <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">

                                    <li><a href="/quotations">
                                            <span class="label label-info">{{ App\UserQuote::where('team_id', Auth::user()->currentTeam->id)->count() }}</span>
                                            Quotations</a></li>
                                    <li><a href="/quotations/create">Create Quotation</a></li>
                                </ul>
                            </li>

                            <!--

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-file"></i><span> Purchases </span>
                                    <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">

                                    <li><a href="extra-invoice.html">Bills</a></li>
                                    <li><a href="extra-maintenance.html">Receipts</a></li>
                                    <li><a href="extra-maintenance.html">Vendors</a></li>
                                    <li><a href="extra-maintenance.html">Products & Services</a></li>
                                </ul>
                            </li>

                            -->

                            <li class="has_sub" id="inv-menu-tour">
                                <a href="/inventory" class="waves-effect"><i class="ti-truck"></i>
                                    <span class="label label-default">{{ App\Item::where('team_id', Auth::user()->currentTeam->id)->count() }}</span>
                                    <span> Products & Services </span></a>
                            </li>

                            <li class="has_sub" id="accounting-menu-tour">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-wallet"></i><span> Accounting </span>
                                    <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">

                                    <li><a href="/transactions">Transactions</a></li>
                                    <li><a href="/glaccounts">Chart of Accounts</a></li>
                                </ul>
                            </li>

                            <!--
                            <li class="has_sub">
                                <a href="/reports" class="waves-effect"><i class="ti-pulse"></i>
                                    <span> Reports </span></a>
                            </li>
                            -->

                            <li class="has_sub" id="reports-menu-tour">
                                <a href="/reports" class="waves-effect"><i class="ti-pulse"></i>
                                    <span class="label label-primary"></span>
                                    <span> Reports </span></a>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-settings"></i><span> Configuration </span>
                                    <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="/gateways">Payment Gateway</a></li>
                                    <li><a href="/config">Configuration</a></li>
                                </ul>

                            </li>

                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- Left Sidebar End -->
    @endif

    <!-- Main Content -->
        <!-- ============================================================== -->
        <!-- Start Content here by Nik-->
        <!-- ============================================================== -->


        @if (Auth::check())

            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    @yield('content')

                </div>
            </div>
        @else

            <div class="content-page" style="margin-left: 0px;">
                <!-- Start content -->
                <div class="content">

                    @yield('content')

                </div>
            </div>
        @endif


    <!-- Application Level Modals -->
        @if (Auth::check())
            @include('spark::modals.notifications')
            @include('spark::modals.support')
            @include('spark::modals.session-expired')
        @endif

    </div>
</div>

<!-- JavaScript - added by Nik -->
<script>
    var resizefunc = [];
</script>

<!-- Chat Script -->
@if (Auth::check())

    <!-- Start of HubSpot Embed Code -->
    <!--
    <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/3020395.js"></script>
    -->
    <!-- End of HubSpot Embed Code -->

    <!-- Start of Async Drift Code -->
    <script>
        !function() {
            var t;
            if (t = window.driftt = window.drift = window.driftt || [], !t.init) return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0,
                t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ],
                t.factory = function(e) {
                    return function() {
                        var n;
                        return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
                    };
                }, t.methods.forEach(function(e) {
                t[e] = t.factory(e);
            }), t.load = function(t) {
                var e, n, o, i;
                e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"),
                    o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js",
                    n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
            });
        }();
        drift.SNIPPET_VERSION = '0.3.1';
        drift.load('4azm3ri88dns');
    </script>
    <!-- End of Async Drift Code -->

    <!--
    <script type="text/javascript" src="//www.pi.team/livechat/php/app.php?widget-init.js"></script>
    -->

    <!-- Hotjar Tracking Code for https://www.pi.team -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:495598,hjsv:5};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
    <!-- Hotjar ends -->
    
@endif
<!-- End Chat Script -->

<script src="/assets/js/detect.js"></script>
<script src="/assets/js/fastclick.js"></script>
<script src="/assets/js/jquery.slimscroll.js"></script>
<script src="/assets/js/jquery.blockUI.js"></script>
<script src="/assets/js/waves.js"></script>
<script src="/assets/js/wow.min.js"></script>
<script src="/assets/js/jquery.nicescroll.js"></script>
<script src="/assets/js/jquery.scrollTo.min.js"></script>

<!-- Form Related -->
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript" src="/assets/plugins/multiselect/js/jquery.multi-select.js"></script>
<script src="/assets/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
<script src="/assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
<script src="/assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
<script src="/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/assets/pages/jquery.form-advanced.init.js"></script>

@yield('footer') <!-- footer added by Nik -->

<!-- Spark JavaScript -->
<script src="/js/config.js"></script>
<script src="/js/app.js"></script>
<script src="/js/sweetalert.min.js"></script>

<script src="/assets/js/jquery.core.js"></script>
<script src="/assets/js/jquery.app.js"></script>

<!-- Modal-Effect -->
<script src="/assets/plugins/custombox/js/custombox.min.js"></script>
<script src="/assets/plugins/custombox/js/legacy.min.js"></script>

<script>
    Pace.stop;
    <!-- loading animation - nik -->
</script>

<!-- tour script -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tour/0.11.0/js/bootstrap-tour.min.js"></script>
<!-- end of tour script -->

@yield('after-footer') <!-- after-footer added by Nik -->

<!-- Google -->

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-93355542-1', 'auto');
    ga('send', 'pageview');

</script>

    
<!-- End of Google -->

</body>
</html>
