<!-- Left Side Of Navbar -->
<!--
<li><a href="/">Test</a></li>
<li><a href="/">Test One</a></li>
-->
<li class="dropdown">
    <a href="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown"
       role="button" aria-haspopup="true" aria-expanded="false"><i class="ti-settings"></i>
        &nbsp;<span class="caret"></span></a>
    <ul class="dropdown-menu">
        <li><a href="/settings#/profile">User Profile</a></li>
        <li><a href="/settings#/security">Security</a></li>
        <li><a href="/settings#/companies">All Companies</a></li>
        <li><a href="/settings/companies/{{ Auth::user()->current_team_id }}#/owner">Company Profile</a></li>
        <li><a href="/settings/companies/{{ Auth::user()->current_team_id }}#/membership">Team Management</a></li>
        <li><a href="/settings/companies/{{ Auth::user()->current_team_id }}#/subscription">Pi Subscription</a></li>
        <li><a href="/settings/companies/{{ Auth::user()->current_team_id }}#/payment-method">Payment Method</a></li>
        <li><a href="/settings/companies/{{ Auth::user()->current_team_id }}#/invoices">Pi Invoices</a></li>
    </ul>
</li>

<li class="dropdown">
    <a href="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown"
       role="button" aria-haspopup="true" aria-expanded="false"><i class="ti-wand"></i>
        &nbsp;<span class="caret"></span></a>
    <ul class="dropdown-menu">
        <li><a href="/invoices/create">Create Invoice</a></li>
        <li><a href="/quotations/create">Create Quotation</a></li>
        <li><a href="/payments">View Payments</a></li>
    </ul>
</li>



