<!-- NavBar For Authenticated Users -->
<spark-navbar
        :user="user"
        :teams="teams"
        :current-team="currentTeam"
        :has-unread-notifications="hasUnreadNotifications"
        :has-unread-announcements="hasUnreadAnnouncements"
        inline-template>

    <nav class="navbar navbar-default">
        <div class="container" v-if="user">

            <div id="navigation-button-tour" class="pull-left">
                <button class="button-menu-mobile open-left waves-effect waves-light">
                    <i class="md md-menu"></i>
                </button>
                <span class="clearfix"></span>
            </div> <!-- this div added by Nikhil -->

            <div class="collapse navbar-collapse" id="spark-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav" id="shortcut-button-tour">
                    @includeIf('spark::nav.user-left')
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right pull-right">
                @includeIf('spark::nav.user-right')

                <!-- Notifications -->
                    <li>
                        <a @click="showNotifications" class="waves-effect waves-light">
                        <div class="m-t-5">
                            <span class="badge badge-xs badge-danger" v-if="hasUnreadNotifications || hasUnreadAnnouncements">*</span>
                            <i class="icon-bell"></i>
                        </div>
                        </a>
                    </li>
                    <li class="hidden-xs m-t-1">
                        <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>
                    </li>

                    <li class="dropdown top-menu-item-xs">
                        <!-- User Photo / Name -->
                        <a href="#" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" role="button" aria-expanded="false">
                            <img :src="user.photo_url" class="img-circle">
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <!-- Impersonation -->
                            @if (session('spark:impersonator'))
                                <li class="dropdown-header">Impersonation</li>

                                <!-- Stop Impersonating -->
                                <li>
                                    <a href="/spark/kiosk/users/stop-impersonating">
                                        <i class="fa fa-fw fa-btn fa-user-secret"></i>Back To My Account
                                    </a>
                                </li>

                                <li class="divider"></li>
                            @endif

                        <!-- Developer -->
                            @if (Spark::developer(Auth::user()->email))
                                @include('spark::nav.developer')
                            @endif

                        <!-- Subscription Reminders -->
                            @include('spark::nav.subscriptions')

                        <!-- Settings -->
                            <li class="dropdown-header">Settings</li>

                            <!-- Your Settings -->
                            <li>
                                <a href="/settings">
                                    <i class="fa fa-fw fa-btn fa-cog"></i>Your Settings
                                </a>
                            </li>

                            @if (Spark::usesTeams() && (Spark::createsAdditionalTeams() || Spark::showsTeamSwitcher()))
                            <!-- Team Settings -->
                                @include('spark::nav.teams')
                            @endif

                            <li class="divider"></li>

                            <!-- Support -->
                            <li class="dropdown-header">Support</li>

                            <li>
                                <a @click.prevent="showSupportForm" style="cursor: pointer;">
                                    <i class="fa fa-fw fa-btn fa-paper-plane"></i>Email Us
                                </a>
                            </li>

                            <li class="divider"></li>

                            <!-- Logout -->
                            <li>
                                <a href="/logout">
                                    <i class="fa fa-fw fa-btn fa-sign-out"></i>Logout
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</spark-navbar>
