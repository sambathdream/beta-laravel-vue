
<nav class="navbar navbar-default" role="navigation">
    <div class="container">

        <div class="collapse navbar-collapse" id="spark-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                &nbsp;
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right pull-right">
                <li class="top-menu-item-xs"><a href="/login" class="navbar-link">Login</a></li>
                <li class="top-menu-item-xs"><a href="/register" class="navbar-link">Register</a></li>
            </ul>
        </div>
    </div>
</nav>