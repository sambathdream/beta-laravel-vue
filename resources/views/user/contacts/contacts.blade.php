@extends('spark::layouts.app')

@section('header')

    <script>

        var command = "{{ isset($command) ? $command : 'Invoice' }}";

    </script>

@endsection

@section('content')
    <contact-api :user="user" inline-template>
        <div class="container">
            <!-- Contact related code starts -->

            <div class="row">
                <div class="col-sm-12">
                    <div class="btn-group pull-right m-b-15">

                        <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light"
                                @click="showContactModal">
                            <i class="md md-add"></i> Add Contact
                        </button>

                    </div>
                    <h4 class="page-title">Contacts</h4>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="card-box">

                        <div class="row">

                            <div class="table-responsive">

                                <!-- table code starts here -->
                                <contact-table></contact-table>
                                <!-- table code ends here -->

                            </div>

                        </div>

                    </div>

                </div> <!-- end col -->




            </div>

            <!-- Modal -->

        @include('user.contacts.add_contact_modal')


        <!-- ends nik - contact related code ends here -->
        </div>
    </contact-api>
@endsection

@section('footer')

    <!--
    <script src="/assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
    -->

    <script type="text/x-template" id="contact-table">

        <div id="content">

            <div class="row m-b-10">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-1" style="padding-top: 5px;">Search</label>
                        <div class="col-md-6" style="padding-left: 24px;">
                            <input type="text" style="height: 34px;" class="form-control" v-model="searchFor"
                                   @keyup.enter="setFilter" readonly></div>
                        <div class="col-md-4" style="padding-left: 0px;">
                            <button type="button" class="btn btn-white waves-effect" @click="setFilter">Go</button>
                            <button type="button" class="btn btn-white waves-effect" @click="resetFilter">Reset</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">

                        <div class="pull-right">

                            <div class="btn-group vertical-middle">
                                <button type="button" class="btn btn-white waves-effect" id="settingsBtn" @click="
                                showSettingsModal" disabled>
                                Settings</button>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

            <div :class="[{'vuetable-wrapper ui basic segment': true}, loading]">

                <vuetable ref="vuetable"
                          api-url="https://finance.pi.team/api/contacts"
                          :fields="fields"
                          pagination-path="pagination"
                          :sort-order="sortOrder"
                          :multi-sort="multiSort"
                          :per-page="perPage"
                          :append-params="moreParams"
                          detail-row-component="contact-detail-row"
                          detail-row-id="id"
                          detail-row-transition="expand"
                          row-class-callback="rowClassCB"
                          @vuetable:pagination-data="onPaginationData"
                          @vuetable:load-success="onLoadSuccess"
                          @vuetable:loading="showLoader"
                          @vuetable:loaded="hideLoader"
                          @vuetable:cell-clicked="onCellClicked"
                ></vuetable>
                <div class="vuetable-pagination ui bottom attached segment grid">
                    <vuetable-pagination-info ref="paginationInfo"
                                              :pagination-info-template="paginationInfoTemplate"
                    ></vuetable-pagination-info>
                    <component :is="paginationComponent" ref="pagination"
                               @vuetable-pagination:change-page="onChangePage"
                    ></component>
                </div>

            </div><!-- vuetable-wrapper -->

            <div class="modal fade" id="settingsModal">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Settings</h4>
                        </div>

                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="checkbox" v-model="multiSort">
                                        <label>Multisort (use Alt+Click)</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-md-2 m-t-5">
                                            <label>Pagination:</label>
                                        </div>
                                        <div class="col-md-10">
                                            <select class="form-control select2" v-model="paginationComponent">
                                                <option value="vuetable-pagination">vuetable-pagination</option>
                                                <option value="vuetable-pagination-dropdown">
                                                    vuetable-pagination-dropdown
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row m-t-10">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-md-2 m-t-5">
                                            <label>Per Page:</label>
                                        </div>
                                        <div class="col-md-10">
                                            <select class="form-control select2" v-model="perPage">
                                                <option :value="10">10</option>
                                                <option :value="15">15</option>
                                                <option :value="20">20</option>
                                                <option :value="25">25</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row m-t-10">
                                <div class="col-md-12">
                                    <div class="panel panel-default panel-border">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Visible fields</h3>
                                        </div>
                                        <div class="panel-body">
                                            <div v-for="field in fields">
                                                <div class="checkbox">
                                                    <input type="checkbox" v-model="field.visible">
                                                    <label>@{{ getFieldTitle(field) }}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close
                            </button>
                        </div>


                    </div>
                </div>

            </div>

        </div><!-- content -->

    </script>


    <script type="text/javascript">


        /*

        $(function(){

            $('#bill_country_id').select2().on("change", function(e) {
                // mostly used event, fired to the original element when the value changes
                //log("change val=" + e.val);
                //var id = $('#currency_id').val();
                document.getElementById('bill_country').value = $('#bill_country_id').val();
                //$('#bill_country_id').val(id).trigger("change");
                alert(document.getElementById('bill_country').value);

            });

        });


        $(document).ready(function () {
            function onchange() {
                document.getElementById("ship_address1").value    = document.getElementById("bill_address1").value;
            }
            $('#bill_address1').on('change', onchange);

        });
        */

        /*
         //Select value without triggering change event
         $(destino).val('x');

         //Select value and trigger change event
         $(destino).val("x").trigger("change")
         */
    </script>

@endsection

@section('after-footer')




@endsection
