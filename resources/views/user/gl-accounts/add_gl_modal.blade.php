
<div id="add-gl-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Create a General Ledger Account</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">

                            <select class="form-control selectpicker show-tick" data-live-search="true" name="type" id="type"
                                    v-model="new_glacc.type">
                                @foreach ( App\GL_Type::all() as $gl_type)
                                    <option value="{{ $gl_type->id }}" data-subtext="{{ $gl_type->primary }}">{{ $gl_type->sub }}</option>
                                @endforeach

                            </select>

                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" id="name"
                                   placeholder="Name" v-model="new_glacc.name"
                                   v-validate="'required'">
                                <i v-show="errors.has('name')" class="fa fa-warning"></i>
                                <span v-show="errors.has('name')">The 'GL Name' field is mandatory.</span>
                        </div>
                    </div>
                </div>

                <h4>Description</h4>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" name="description" class="form-control"
                                   id="description"
                                   placeholder="Description" v-model="new_glacc.description">
                        </div>
                    </div>
                </div>

                <h4>GL Account</h4>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <h5 class="m-t-0">Internal Balance</h5>
                            <input type="text" name="int_balance" class="form-control"
                                   id="int_balance"
                                   placeholder="Internal Balance" v-model="new_glacc.int_balance"
                                   v-validate data-vv-rules="decimal:2">
                            <i v-show="errors.has('int_balance')" class="fa fa-warning"></i>
                            <span v-show="errors.has('int_balance')" class="help is-danger">This field must be numeric and may contain 2 decimal points.</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <h5 class="m-t-0">External or Bank Ref Balance</h5>
                            <input type="text" name="ext_balance" class="form-control"
                                   id="ext_balance"
                                   placeholder="External Balance" v-model="new_glacc.ext_balance"
                                   v-validate data-vv-rules="decimal:2">
                            <i v-show="errors.has('ext_balance')" class="fa fa-warning"></i>
                            <span v-show="errors.has('ext_balance')" class="help is-danger">This field must be numeric and may contain 2 decimal points.</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group no-margin">
                            <button type="button" class="btn btn-white waves-effect"
                                    data-dismiss="modal">Close
                            </button>

                            <a v-if="new_glacc.id" href="#" class="btn btn-primary waves-effect submit-new-edit"
                               @click.prevent="updateGL">Update Changes
                            </a>
                            <a v-else href="#" class="btn btn-primary waves-effect submit-new-edit"
                               @click.prevent="saveNewGL">Save Changes
                            </a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- /.modal -->