@extends('spark::layouts.app')

@section('header')

    <!-- Plugin Css-->
    <style>
        .datepicker {
            padding-left: 0px;
            padding-right: 0px;
            padding-top: 0px;
            padding-bottom: 0px;
        }


    </style>

    <link href="/assets/plugins/bootstrap-table/css/bootstrap-table.min.css" rel="stylesheet" type="text/css"/>

    <script>

        var command = "{{ isset($command) ? $command : 'Invoice' }}";

        jQuery(document).ready(function () {

            var token = '{{ $encrypted_csrf_token }}';
            Bus.$emit('getToken', token); //need to be replaced with open_id

        });

    </script>

    <style>
        fieldset[disabled] .multiselect{pointer-events:none}.multiselect__spinner{position:absolute;right:1px;top:1px;width:48px;height:35px;background:#fff;display:block}.multiselect__spinner:after,.multiselect__spinner:before{position:absolute;content:"";top:50%;left:50%;margin:-8px 0 0 -8px;width:16px;height:16px;border-radius:100%;border-color:#41b883 transparent transparent;border-style:solid;border-width:2px;box-shadow:0 0 0 1px transparent}.multiselect__spinner:before{animation:a 2.4s cubic-bezier(.41,.26,.2,.62);animation-iteration-count:infinite}.multiselect__spinner:after{animation:a 2.4s cubic-bezier(.51,.09,.21,.8);animation-iteration-count:infinite}.multiselect__loading-enter-active,.multiselect__loading-leave-active{transition:opacity .4s ease-in-out;opacity:1}.multiselect__loading-enter,.multiselect__loading-leave-active{opacity:0}.multiselect,.multiselect__input,.multiselect__single{font-family:inherit;font-size:14px;-ms-touch-action:manipulation;touch-action:manipulation}.multiselect{box-sizing:content-box;display:block;position:relative;width:100%;min-height:40px;text-align:left;color:#35495e}.multiselect *{box-sizing:border-box}.multiselect:focus{outline:none}.multiselect--disabled{pointer-events:none;opacity:.6}.multiselect--active{z-index:1}.multiselect--active .multiselect__current,.multiselect--active .multiselect__input,.multiselect--active .multiselect__tags{border-bottom-left-radius:0;border-bottom-right-radius:0}.multiselect--active .multiselect__select{transform:rotate(180deg)}.multiselect--above.multiselect--active .multiselect__current,.multiselect--above.multiselect--active .multiselect__input,.multiselect--above.multiselect--active .multiselect__tags{border-top-left-radius:0;border-top-right-radius:0}.multiselect__input,.multiselect__single{position:relative;display:inline-block;min-height:20px;line-height:20px;border:none;border-radius:5px;background:#fff;padding:1px 0 0 5px;width:100%;transition:border .1s ease;box-sizing:border-box;margin-bottom:8px}.multiselect__tag~.multiselect__input,.multiselect__tag~.multiselect__single{width:auto}.multiselect__input:hover,.multiselect__single:hover{border-color:#cfcfcf}.multiselect__input:focus,.multiselect__single:focus{border-color:#a8a8a8;outline:none}.multiselect__single{padding-left:6px;margin-bottom:8px}.multiselect__tags{min-height:40px;display:block;padding:8px 40px 0 8px;border-radius:5px;border:1px solid #e8e8e8;background:#fff}.multiselect__tag{position:relative;display:inline-block;padding:4px 26px 4px 10px;border-radius:5px;margin-right:10px;color:#fff;line-height:1;background:#41b883;margin-bottom:8px;white-space:nowrap}.multiselect__tag-icon{cursor:pointer;margin-left:7px;position:absolute;right:0;top:0;bottom:0;font-weight:700;font-style:normal;width:22px;text-align:center;line-height:22px;transition:all .2s ease;border-radius:5px}.multiselect__tag-icon:after{content:"\D7";color:#266d4d;font-size:14px}.multiselect__tag-icon:focus,.multiselect__tag-icon:hover{background:#369a6e}.multiselect__tag-icon:focus:after,.multiselect__tag-icon:hover:after{color:#fff}.multiselect__current{min-height:40px;overflow:visible !important;padding:8px 12px 0;padding-right:30px;white-space:nowrap;border-radius:5px;border:1px solid #e8e8e8}.multiselect__current,.multiselect__select{line-height:16px;box-sizing:border-box;display:block;margin:0;text-decoration:none;cursor:pointer}.multiselect__select{position:absolute;width:40px;height:38px;right:1px;top:1px;padding:4px 8px;text-align:center;transition:transform .2s ease}.multiselect__select:before{position:relative;right:0;top:65%;color:#999;margin-top:4px;border-style:solid;border-width:5px 5px 0;border-color:#999 transparent transparent;content:""}.multiselect__placeholder{color:#adadad;display:inline-block;margin-bottom:10px;padding-top:2px}.multiselect--active .multiselect__placeholder{display:none}.multiselect__content{position:absolute;list-style:none;display:block;background:#fff;width:100%;max-height:240px;overflow:visible !important;padding:0;margin:0;border:1px solid #e8e8e8;border-top:none;border-bottom-left-radius:5px;border-bottom-right-radius:5px;z-index:1}.multiselect--above .multiselect__content{bottom:100%;border-bottom-left-radius:0;border-bottom-right-radius:0;border-top-left-radius:5px;border-top-right-radius:5px;border-bottom:none;border-top:1px solid #e8e8e8}.multiselect__content::webkit-scrollbar{display:none}.multiselect__element{display:block}.multiselect__option{display:block;padding:12px;min-height:40px;line-height:16px;text-decoration:none;text-transform:none;vertical-align:middle;position:relative;cursor:pointer;white-space:nowrap}.multiselect__option:after{top:0;right:0;position:absolute;line-height:40px;padding-right:12px;padding-left:20px}.multiselect__option--highlight{background:#41b883;outline:none;color:#fff}.multiselect__option--highlight:after{content:attr(data-select);background:#41b883;color:#fff}.multiselect__option--selected{background:#f3f3f3;color:#35495e;font-weight:700}.multiselect__option--selected:after{content:attr(data-selected);color:silver}.multiselect__option--selected.multiselect__option--highlight{background:#ff6a6a;color:#fff}.multiselect__option--selected.multiselect__option--highlight:after{background:#ff6a6a;content:attr(data-deselect);color:#fff}.multiselect--disabled{background:#ededed;pointer-events:none}.multiselect--disabled .multiselect__current,.multiselect--disabled .multiselect__select,.multiselect__option--disabled{background:#ededed;color:#a6a6a6}.multiselect__option--disabled{cursor:text;pointer-events:none}.multiselect__option--disabled.multiselect__option--highlight{background:#dedede!important}.multiselect-enter-active,.multiselect-leave-active{transition:all .15s ease}.multiselect-enter,.multiselect-leave-active{opacity:0}@keyframes a{0%{transform:rotate(0)}to{transform:rotate(2turn)}}
    </style>

    <style>

        .multiselect__element{
            z-index: 200 !important;
        }

        .multiselect{
            z-index: 200 !important;
        }
    </style>

@endsection

@section('content')

    <invoice-create-api :user="user" inline-template>
        <div class="container">
            <!-- Contact related code starts -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <form id="vue-invoice" class="form-horizontal" name="vue-invoice" enctype="multipart/form-data">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">

                        <div class="btn-group dropdown pull-right m-b-15">
                            <a class="btn btn-default waves-effect waves-light" @click.prevent="save{{ isset($command) ? $command : 'Invoice' }}" href="#">Save
                                {{ isset($command) ? $command : 'Invoice' }}</a>
                        </div>

                        <h4 class="page-title">New {{ isset($command) ? $command : 'Invoice' }}</h4>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">

                                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" id="business-from-tour">
                                        <div class="form-group">
                                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                                <address class="m-b-0">
                                                    <strong>@{{ invoice_from.name }}</strong><br>
                                                    @{{ invoice_from.billing_address }}
                                                    @{{ invoice_from.billing_address_line_2 }}<br>
                                                    @{{ invoice_from.billing_city }} @{{ invoice_from.bill_state }}
                                                    @{{ invoice_from.billing_zip }} @{{ invoice_from.billing_country }}<br>
                                                    @{{ invoice_from.phone }} @{{ invoice_from.email }}
                                                    @{{ invoice_from.website }}

                                                </address>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                                <a class="btn btn-white btn-custom waves-effect waves-light btn-xs"
                                                   href="/settings/companies/{{ Auth::user()->current_team_id }}#/payment-method">
                                                    Edit Business Details</a>
                                                <a href="/config"
                                                   class="btn btn-white btn-custom waves-effect waves-light btn-xs">
                                                    Invoice Logo</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">{{ isset($command) ? $command : 'Invoice' }} Title</label>
                                        <div class="col-sm-8">

                                            <input type="text" class="form-control text-capitalize" id="invoice_title"
                                                   v-model="invoice_form.invoice_title"
                                                   name="invoice_title" placeholder="{{ isset($command) ? $command : 'Invoice' }}"
                                                   v-validate="'required'" data-vv-scope="invoice_form">
                                            <i v-show="errors.has('invoice_title', 'invoice_form')"
                                               class="fa fa-warning text-danger"></i>
                                            <span v-show="errors.has('invoice_title', 'invoice_form')"
                                                  class="help text-danger">{{ isset($command) ? $command : 'Invoice' }} title field is required.</span>

                                        </div>


                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Summary</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="invoice_summary"
                                                   v-model="invoice_form.invoice_summary"
                                                   name="invoice_summary" placeholder="{{ isset($command) ? $command : 'Invoice' }} Summary"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">

                                    <div class="form-group">

                                        <label class="col-md-4 control-label">Credit GL</label>
                                        <div class="col-md-8">

                                            <select class="form-control selectpicker show-tick" data-live-search="true" name="credit_gl"
                                                    id="credit_gl" v-model="invoice_form.credit_gl">
                                                @foreach ($gl_accounts as $gl_account)
                                                    <option value="{{ $gl_account->open_id }}">{{ $gl_account->name }}</option>
                                                @endforeach

                                            </select>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Debit GL</label>
                                        <div class="col-md-8">
                                            <div class="checkbox checkbox-primary">
                                                <input id="debit_gl_cb" type="checkbox"
                                                       v-model="invoice_form.debit_gl_cb">
                                                <label for="debit_gl_cb">Post Debit</label>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                        <div class="card-box">
                            <div class="row">

                                <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                    <div class="row" v-if="!invoice_form.contact_id">
                                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                            <div class="form-group">
                                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" id="customer-name-tour">

                                                    <typeahead style="bottom: 12px;"></typeahead>

                                                    <input type="hidden" class="form-control" id="contact_id"
                                                           v-model="invoice_form.contact_id"
                                                           name="contact_id"
                                                           v-validate="'required'" data-vv-scope="invoice_form">
                                                    <i v-show="errors.has('contact_id', 'invoice_form')"
                                                       class="fa fa-warning text-danger"></i>
                                                    <span v-show="errors.has('contact_id', 'invoice_form')"
                                                          class="help text-danger">Valid customer is required.</span>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" v-show="invoice_form.contact_id">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                                            <address class="m-b-0">
                                                <strong>BILL TO:</strong><br>
                                                <strong>@{{ new_contact.name }}</strong><br>
                                                <div v-show="new_contact.bill_address1"> @{{ new_contact.bill_address1 }}<br></div>
                                                <div v-show="new_contact.bill_address2">@{{ new_contact.bill_address2 }}<br></div>
                                                <div>@{{ new_contact.bill_city }} @{{ new_contact.bill_state }}</div>
                                                <div v-show="new_contact.bill_country_id">@{{ new_contact.bill_country_id }}<br></div>
                                                <div v-show="new_contact.phone"><abbr title="Phone">P:</abbr> @{{ new_contact.phone }}</div>
                                                <div v-show="invoice_form.customer_email"><abbr title="Email">@:</abbr> @{{ invoice_form.customer_email }}</div>
                                            </address>

                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                                            <address class="m-b-0" v-if="new_contact.ship_address1">
                                                <strong>SHIP TO:</strong><br>
                                                <strong v-show="new_contact.ship_contact">@{{ new_contact.ship_contact }}</strong><br>
                                                <div v-show="new_contact.ship_address1"> @{{ new_contact.ship_address1 }}<br></div>
                                                <div v-show="new_contact.ship_address2">@{{ new_contact.ship_address2 }}<br></div>
                                                <div>@{{ new_contact.ship_city }} @{{ new_contact.ship_state }}</div>
                                                <div v-show="new_contact.ship_country_id">@{{ new_contact.ship_country_id }}</div>
                                                <div v-show="new_contact.ship_phone"><abbr title="Phone">P:</abbr> @{{ new_contact.ship_phone }}</div>
                                            </address>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                            <div class="form-group">
                                                <div class="col-md-12" id="customer-add-tour"
                                                     style="margin-top: 8px;">
                                                    <button type="button"
                                                            class="btn btn-inverse btn-custom waves-effect waves-light btn-xs"
                                                            @click="createInvoiceAddContact">
                                                        <i class="md md-add"></i> Add New Customer
                                                    </button>
                                                    <a v-show="invoice_form.contact_id"
                                                       class="btn btn-primary btn-custom waves-effect waves-light btn-xs"
                                                       @click="createInvoiceEditContact"> Edit / View </a>
                                                    <a v-show="invoice_form.contact_id"
                                                       class="btn btn-default btn-custom waves-effect waves-light btn-xs"
                                                       @click="selectDiffCustomer"> Select Different Customer </a>

                                                    <!-- dynamic visibility for these buttons -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{ isset($command) ? $command : 'Invoice' }} #</label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" id="open_id" name="open_id"
                                                       v-model="invoice_form.open_id"
                                                       placeholder="{{ $command }} ID"  />
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control" id="poso_number" name="poso_number"
                                                       placeholder="PO/SO #" v-model="invoice_form.poso_number" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{ isset($command) ? $command : 'Invoice' }} Date</label>
                                            <div class="col-md-7">
                                                <datepicker v-model="invoice_form.invoice_date" input-class="form-control"
                                                            monday-first>
                                                </datepicker>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{ isset($command) ? $command : 'Invoice' }} Due</label>
                                            <div class="col-md-7">
                                                <datepicker v-model="invoice_form.due_date" input-class="form-control"
                                                            monday-first>
                                                </datepicker>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                            <div class='row'>
                                <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'
                                     id="invoice-item-tour">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>

                                            <th width="15%">ID/Sr.No</th>
                                            <th width="38%">Product/Item Name or Description</th>
                                            <th width="15%">Price</th>
                                            <th width="15%">Quantity</th>
                                            <th width="15%">Total</th>
                                            <th width="2%">&nbsp;</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr v-for="(item, index) in invoice_form.items" v-if="invoice_form.item_options">
                                            <!-- <tr v-for="(item, index) in invoice_form.items" v-if="invoice_form.itemOptions.length !== 0"> -->

                                            <td>
                                                <input type="text" name="item_id[]" id="item_id[]" class="form-control" v-model="item.item_id">
                                            </td>

                                            <td>

                                                <autocomplete
                                                        :index="index"
                                                        anchor="name"
                                                        label="buy_price"
                                                        :options="invoice_form.item_options"
                                                        :on-select="onAutocompleteSelect"
                                                        :on-input="onAutocompleteInput">
                                                </autocomplete>

                                                <input type="hidden" name="credit_gl[]" id="credit_gl[]"
                                                       v-model="item.credit_gl">

                                            </td>

                                            <td>
                                                <input type="text" name="item_price[]" id="item_price[]"
                                                       class="form-control" v-model="item.price"
                                                       v-validate data-vv-scope="invoice_form" data-vv-rules="decimal:2" data-vv-delay="1000">
                                                <i v-show="errors.has('item_price[]', 'invoice_form')"
                                                   class="fa fa-warning text-danger"></i>
                                                <span v-show="errors.has('item_price[]', 'invoice_form')" class="help text-danger">Invalid Input.</span>
                                            </td>
                                            <td><input type="text" name="item_qty[]" id="item_qty[]"
                                                       class="form-control" v-model="item.qty"
                                                       v-validate data-vv-rules="decimal:2" data-vv-delay="1000" data-vv-scope="invoice_form">
                                                <i v-show="errors.has('item_qty[]', 'invoice_form')"
                                                   class="fa fa-warning text-danger"></i>
                                                <span v-show="errors.has('item_qty[]', 'invoice_form')" class="help text-danger">Invalid Input.</span>


                                            </td>
                                            <td><input type="text" name="item_total[]" id="item_total[]"
                                                       class="form-control" readonly
                                                       v-bind:value="item.total = parseFloat(item.qty) * parseFloat(item.price)">
                                            </td>
                                            <td>
                                                <button class="btn btn-xs btn-danger delete m-t-5" type="button" @click=
                                                "removeLine(index, 1)"><i class="fa fa-remove"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                    <button class="btn-xs btn-success" type="button" @click="addLine"
                                            id="item-row-tour">
                                        <i class="fa fa-plus"></i> ROW
                                    </button>
                                    <button class="btn-xs btn-default" type="button" id="add-product-tour"
                                            data-toggle="modal" data-target="#add-product-modal">
                                        <i class="fa fa-cart-plus"></i> PRODUCT
                                    </button>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">

                                    <div class="row">
                                        <div class="form-group">

                                            <div class="col-sm-9">
                                                <multiselect v-model="invoice_form.ded_items" :options="ded_rates" :multiple="true"
                                                             :close-on-select="true" :clear-on-select="false"
                                                             :hide-selected="true" placeholder="Deductibles/Discounts" label="name"
                                                             track-by="name" @close="selectedDedItems">
                                                </multiselect>
                                            </div>
                                            <div class="col-sm-3">
                                                <button class="btn btn-icon waves-effect waves-light btn-primary" type="button" id="add-product-tour"
                                                        data-toggle="modal" @click="showTaxDedModal(2)">
                                                    <i class="fa fa-plus-circle"></i> Add
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">

                                            <div class="col-sm-9">
                                                <multiselect v-model="invoice_form.tax_items" :options="tax_rates" :multiple="true"
                                                             :close-on-select="true" :clear-on-select="false"
                                                             :hide-selected="true" placeholder="Taxes" label="name"
                                                             track-by="name" @close="selectedTaxItems">
                                                </multiselect>
                                            </div>
                                            <div class="col-sm-3">
                                                <button class="btn btn-icon waves-effect waves-light btn-primary" type="button" id="add-product-tour"
                                                        data-toggle="modal" @click="showTaxDedModal(1)">
                                                    <i class="fa fa-plus-circle"></i> Add
                                                </button>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <div class="input-group col-sm-9">
                                            <select class="form-control selectpicker show-tick" data-live-search="true"
                                                    name="currency_id" id="inv_currency_id"
                                                    v-model="invoice_form.currency_id" v-on:change="convertCurrency">
                                                @foreach (App\Country::all() as $country)
                                                    <option value="{{ $country->id }}">Currency: {{ $country->name }}
                                                        / {{ $country->currency_code }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>


                                </div>
                                <div class="col-sm-offset-0 col-md-offset-0 col-lg-offset-0 col-xs-12 col-sm-5 col-md-5 col-lg-5"
                                     style="padding-right: 20px">

                                    <p class="text-right"><b>Sub-total:</b>  @{{ this.sub_total }}</p>

                                    <p v-for="tax_row in invoice_form.ded_items" class="text-right">
                                        @{{ tax_row.name + ' (' + tax_row.rate + '%)' + ': '}}
                                        @{{ tax_row.amount = (( sub_total ) * tax_row.rate / 100) }}
                                    </p>

                                    <p v-for="tax_row in invoice_form.tax_items" class="text-right">
                                        @{{ tax_row.name + ' (' + tax_row.rate + '%)' + ': '}}
                                        @{{ tax_row.amount = (( sub_total - ded_amount_inc_tax ) * tax_row.rate / 100) }}</p>

                                    <p v-if="tax_amount" class="text-right small">Total Tax: @{{ tax_amount }}</p>

                                    <hr>
                                    <h3 class="text-right">Total: @{{ total_amount }}</h3>

                                </div>
                                <div class="row" v-show="this.invoice_form.currency_id != this.invoice_form.company_currency_id">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-right: 20px;">
                                        <div class="pull-right">
                                            Local Currency: @{{ currencyConvert.company_country }} - Invoice total after conversion: @{{ converted_amount }} at @{{ currencyConvert.rate }}
                                            <button type="button"
                                                    class="btn btn-xs btn-white" @click="convertCurrency">
                                                <i class="md md-add"></i> Update Rate &nbsp;
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end Panel -->
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion-test-3">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion-test-3" href="#collapseOne-3"
                                           aria-expanded="false" class="collapsed">
                                            Notes
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne-3" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <textarea name="invoice_notes" id="invoice_notes" class="form-control"
                                                  v-model="invoice_form.notes"
                                                  v-validate data-vv-rules="max:250" data-vv-scope="invoice_form"></textarea>
                                        <i v-show="errors.has('invoice_notes', 'invoice_form')" class="fa fa-warning text-danger"></i>
                                        <span v-show="errors.has('invoice_notes', 'invoice_form')" class="help text-danger">
                                            Maximum 250 characters allowed.</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion-test-1">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion-test-1" href="#collapseOne-1"
                                           aria-expanded="false" class="collapsed">
                                            Footer
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne-1" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <textarea name="invoice_footer" id="invoice_footer" class="form-control"
                                                  v-model="invoice_form.footer"
                                                  v-validate data-vv-rules="max:250" data-vv-scope="invoice_form"></textarea>
                                        <i v-show="errors.has('invoice_footer', 'invoice_form')" class="fa fa-warning text-danger"></i>
                                        <span v-show="errors.has('invoice_footer', 'invoice_form')" class="help text-danger">
                                            Maximum 250 characters allowed.</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-12">

                        <div class="btn-group dropup pull-right m-b-15"
                             id="save-invoice-tour">
                            <a class="btn btn-default waves-effect waves-light" @click.prevent="save{{ isset($command) ? $command : 'Invoice' }}" href="#">Save
                                {{ isset($command) ? $command : 'Invoice' }}</a>
                        </div>

                    </div>
                </div>

            </form>

            <!-- Modal -->
            @include('user.inventory.add_item_modal')
            @include('user.contacts.add_contact_modal')
            @include('user.config.update_tax_ded_modal')

        </div>

    </invoice-create-api>

@endsection

@section('footer')


@endsection


@section('after-footer')

    <!-- Tour script -->

    @if(!$boot_tour->create_invoice)

        <script>

            $(document).ready(function () {

                // Instance the tour
                var tour = new Tour({
                    steps: [
                        {
                            element: "#business-from-tour",
                            title: "Invoice From?",
                            content: "Usually these are your business details. You can click on 'Edit Business Details' to change it permanently. ",
                            placement: "bottom",
                            backdrop: true,
                            backdropContainer: 'body'
                        },
                        {
                            element: '#customer-add-tour',
                            title: "Add New Customer",
                            content: "You can add new customer and it will be reflected here immediately.",
                            placement: "bottom",
                            backdrop: true,
                            backdropContainer: 'body'
                        },
                        {
                            element: "#customer-name-tour",
                            title: "Search Customer",
                            content: "Just enter customer name and we will search if already created.",
                            placement: "bottom",
                            backdrop: true,
                            backdropContainer: 'body'
                        },
                        {
                            element: "#invoice-item-tour",
                            title: "Add Invoice Items",
                            content: "You can enter item no, item name, price etc or you can add a new product then search it in the item name.",
                            placement: "top",
                            backdrop: true,
                            backdropContainer: 'body'
                        },
                        {
                            element: "#add-product-tour",
                            title: "New Product",
                            content: "You can add new product and search using item name field.",
                            placement: "bottom",
                            backdrop: true,
                            backdropContainer: 'body'
                        },
                        {
                            element: "#item-row-tour",
                            title: "Add Invoice Item",
                            content: "Add new item row on the invoice. You can add unlimited rows.",
                            placement: "bottom",
                            backdrop: true,
                            backdropContainer: 'body'
                        },
                        {
                            element: "#save-invoice-tour",
                            title: "Save Invoice",
                            content: "Once ready just click this button and invoice will be saved.",
                            placement: "left",
                            backdrop: true,
                            backdropContainer: 'body'
                        }

                    ],

                    smartPlacement: true,

                    onEnd: function (tour) {
                        Bus.$emit('closeTour', 3); //0 = dashboard

                    },

                });

                // Initialize the tour
                tour.init();
                tour.start();

            });


        </script>

    @endif

    <!-- End of Tour script -->

@endsection
