@extends('spark::layouts.app')

@section('header')

    <script>

        var command = "{{ isset($command) ? $command : 'Invoice' }}";

    </script>



@endsection

@section('content')

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <invoice-api :user="user" inline-template>
        <!-- Page-Title -->
        <div class="container">

            <div class="row hidden-print">
                <div
                        @if (Auth::check())
                        class="col-md-12"
                        @else
                        class="col-md-8 col-md-offset-2"
                        @endif
                >

                    <div class="pull-right m-b-15">

                        @if($command == 'Invoice')
                            <a type="button" href="{{ url('/public/invoices/' . $invitation_key . '/download') }}"
                               class="btn btn-info waves-effect waves-light" target="_blank">
                                PDF
                            </a>
                        @else
                            <a type="button" href="{{ url('/public/quotations/' . $invitation_key . '/download') }}"
                               class="btn btn-info waves-effect waves-light" target="_blank">
                                PDF
                            </a>
                        @endif

                        @if (Auth::check())

                            @if($invoice->status_id != 6)

                                <button type="button" class="btn btn-default waves-effect waves-light"
                                        v-on:click.prevent="send{{ isset($command) ? $command : 'Invoice' }}('{{ $invoice->from_email }}','{{ $invoice->contactTab->email }}','{{ $invoice->contactTab->emails }}','{{ $team->name }}','{{ $invoice->contactTab->name }}','{{ $invoice->open_id }}','{{ $team_details->email_subject }}')">
                                    Email {{ isset($command) ? $command : 'Invoice' }}
                                </button>

                            @endif
                        <!--
                            <button type="button" class="btn btn-primary waves-effect waves-light">Mark as Sent
                            </button>
                            -->
                            @if($command == 'Invoice')

                                @if($invoice->status_id == 6)
                                    <button type="button" class="btn btn-danger waves-effect waves-light" @click="stripeBillToInvoice({{ $invoice }})">
                                        <i class="fa fa-fw fa-rocket fa-btn"></i> Close & Invoice
                                    </button>
                                @endif

                                @if($invoice->status_id != 4 and $invoice->status_id != 7) <!-- need to add more conditions -->
                                    <a type="button" class="btn btn-primary waves-effect waves-light" href="{{ url('/invoices/' . $invoice->open_id . '/edit') }}">
                                        <i class="fa fa-fw fa-pencil-square-o fa-btn"></i> Edit
                                    </a>
                                @endif

                                @if($invoice->balance > 0 and $invoice->status_id != 6)
                                    <button type="button" class="btn btn-danger waves-effect waves-light" @click="showPayModal({{ $invoice }},{{ $invitation_key }})">
                                        Manual Payment
                                    </button>
                                @endif
                            @else
                                <button type="button" class="btn btn-danger waves-effect waves-light" @click="displayConvertToInvoice()">Convert to Invoice
                                </button>
                            @endif

                        @endif

                        @if ($gateway_id == 3 and ($invoice->balance * 100) >= 1 and $invoice->status_id != 6)
                            <button type="button" class="btn btn-default" id="rzp-button1" onclick="clicked()">
                                <i class="fa fa-fw fa-inr fa-btn"></i> Make Payment</button>
                        @elseif ($gateway_id == 1 and ($invoice->balance * 100) >= 1 and $invoice->status_id != 6)
                            <button id="customButton" class="btn btn-danger waves-effect waves-light" onclick="stripeClicked()">
                                <i class="fa fa-fw fa-cc-stripe fa-btn"></i> Make Payment</button>
                        @endif
                    </div>

                </div>
            </div>

            @if (Auth::check())

                <div class="row hidden-print">
                    <div class="col-sm-12">
                        <div class="card-box widget-inline" style="margin-bottom: 10px;">
                            <div class="row">
                                <div class="col-lg-3 col-sm-6">
                                    <div class="widget-inline-box text-center">
                                        <h3>
                                            <i class="text-primary md md-add-shopping-cart"></i> {{ $invoice->statusTab->text }}
                                        </h3>
                                        <h4 class="text-muted">Status</h4>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-sm-6">
                                    <div class="widget-inline-box text-center">
                                        <h3><i class="text-pink md md-account-child"></i>{{ $invoice->contactTab->name }}</h3>
                                        <h4 class="text-muted">Customer</h4>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-sm-6">
                                    <div class="widget-inline-box text-center">
                                        <h3>{{ $inv_country->currency_symbol }} {{ number_format($invoice->balance, 2, '.', ',') }}
                                        </h3>
                                        <h4 class="text-muted">Amount Due</h4>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-sm-6">
                                    <div class="widget-inline-box text-center b-0">
                                        <h4>
                                            <i class="text-purple md md-visibility"></i> {{ (Carbon\Carbon::parse($invoice->due_date))->formatLocalized('%B %d, %Y') }}
                                        </h4>
                                        <h4 class="text-muted" style="margin-top: 17px;">Due On</h4>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            @else


            @endif

            <div class="row">
                <div
                        @if (Auth::check())
                        class="col-md-12"
                        @else
                        class="col-md-8 col-md-offset-2"
                        @endif
                >

                    <div class="container card-box" style="margin-bottom: 15px;">

                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="">
                                    @if($team_details->imageSrc)
                                        <img style="vertical-align: middle;padding-top: 2%;" alt="{{ isset($team->name) ? $team->name : 'Missing Name' }}"
                                             width="70%" src="{{ $team_details->imageSrc }}">
                                    @else
                                        <h1>{{ $command }}</h1>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                                <div class="pull-right">
                                    <address>
                                        <strong>{{ isset($team->name) ? $team->name : 'Missing Name' }}</strong><br>
                                        {{ isset($team->billing_address) ? $team->billing_address : '' }}@if(strlen($team->billing_address) > 0)<br>@endif
                                        {{ isset($team->billing_address_line_2) ? $team->billing_address_line_2 : '' }}@if(strlen($team->billing_address_line_2) > 0)<br>@endif
                                        {{ isset($team->billing_city) ? $team->billing_city : '' }}{{ isset($team->billing_state) ? ' ' . $team->billing_state : '' }}@if((strlen($team->billing_city) > 0) or (strlen($team->billing_state) > 0))<br>@endif
                                        {{ isset($team->billing_zip) ? $team->billing_zip : '' }}{{ isset($team->billing_country) ? ' ' . $team->billing_country : '' }}@if((strlen($team->billing_zip) > 0) or (strlen($team->billing_country) > 0))<br>@endif
                                        {{ isset($team_details->email) ? 'Email: ' . $team_details->email : '' }}@if((strlen($team_details->email) > 0))<br>@endif
                                        {{ isset($team_details->website) ? 'Web: ' . $team_details->website : '' }}@if((strlen($team_details->website) > 0))<br>@endif
                                        @if($team_details->gst_code)GST Code: {{ $team_details->gst_code }}<br>@endif
                                    </address>

                                </div>

                            </div>
                        </div>

                        <hr style="margin-top: 0px;margin-bottom: 0px;">

                        <div class="row">

                            <h4 align="center"><strong>{{ $invoice->title }}</strong></h4>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                                <address>
                                    <strong>BILL TO:</strong><br>
                                    <strong>{{ isset($invoice->contactTab->name) ? $invoice->contactTab->name : '' }}</strong><br>
                                    {{ isset($invoice->contactTab->bill_address1) ? $invoice->contactTab->bill_address1 : '' }}@if(strlen($invoice->contactTab->bill_address1) > 0)<br>@endif
                                    {{ isset($invoice->contactTab->bill_address2) ? $invoice->contactTab->bill_address2 : '' }}@if(strlen($invoice->contactTab->bill_address2) > 0)<br>@endif
                                    {{ isset($invoice->contactTab->bill_city) ? $invoice->contactTab->bill_city : '' }}{{ isset($invoice->contactTab->bill_state) ? ' ' . $invoice->contactTab->bill_state : '' }}@if((strlen($invoice->contactTab->bill_city) > 0) or (strlen($invoice->contactTab->bill_state) > 0))<br>@endif
                                    {{ isset($invoice->contactTab->bill_postal_code) ? $invoice->contactTab->bill_postal_code : '' }}{{ isset($invoice->contactTab->bill_country_id) ? ' ' . $invoice->contactTab->bill_country_id : '' }}@if((strlen($invoice->contactTab->bill_postal_code) > 0) or (strlen($invoice->contactTab->bill_country_id) > 0))<br>@endif
                                    {{ isset($invoice->contactTab->email) ? $invoice->contactTab->email : '' }}@if(strlen($invoice->contactTab->email) > 0)<br>@endif
                                    {{ isset($invoice->contactTab->contact1) ? $invoice->contactTab->contact1 : '' }}{{ isset($invoice->contactTab->phone) ? ' ' . $invoice->contactTab->phone : '' }}@if((strlen($invoice->contactTab->contact1) > 0) or (strlen($invoice->contactTab->phone) > 0))<br>@endif

                                </address>
                                @if($invoice->contactTab->gst_code)
                                <p>GST/GSTIN: {{ $invoice->contactTab->gst_code }}</p>
                                @endif
                                @if($invoice->summary)
                                <p>{{ $invoice->summary }}</p>
                                @endif
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                                <address>

                                    @if(strlen($invoice->contactTab->ship_address1)>0)

                                        <strong>SHIP TO:</strong><br>
                                        <strong>{{ isset($invoice->contactTab->ship_contact) ? $invoice->contactTab->ship_contact : $invoice->contactTab->name }}</strong><br>

                                        {{ isset($invoice->contactTab->ship_address1) ? $invoice->contactTab->ship_address1 : '' }}@if(strlen($invoice->contactTab->ship_address1) > 0)<br>@endif
                                        {{ isset($invoice->contactTab->ship_address2) ? $invoice->contactTab->ship_address2 : '' }}@if(strlen($invoice->contactTab->ship_address2) > 0)<br>@endif
                                        {{ isset($invoice->contactTab->ship_city) ? $invoice->contactTab->ship_city : '' }}{{ isset($invoice->contactTab->ship_state) ? ' ' . $invoice->contactTab->ship_state : '' }}@if((strlen($invoice->contactTab->ship_city > 0)) or (strlen($invoice->contactTab->ship_state > 0)))<br>@endif
                                        {{ isset($invoice->contactTab->ship_postal_code) ? $invoice->contactTab->ship_postal_code : '' }}{{ isset($invoice->contactTab->ship_country_id) ? ' ' . $invoice->contactTab->ship_country_id : '' }}@if((strlen($invoice->contactTab->ship_postal_code) > 0) or (strlen($invoice->contactTab->ship_country_id) > 0))<br>@endif
                                        {{ isset($invoice->contactTab->ship_phone) ? ' ' . $invoice->contactTab->ship_phone : '' }}@if(strlen($invoice->contactTab->ship_phone) > 0)<br>@endif

                                    @else
                                        &nbsp;
                                    @endif
                                </address>

                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="pull-right">
                                    <p><strong>{{ $command }} Number: </strong> {{ $invoice->open_id }}</p>
                                    @if($invoice->poso_number)
                                        <p><strong class="">PO/SO Number:</strong>{{ $invoice->poso_number }}</p>
                                    @endif
                                    <p><strong>{{ $command }} Date: </strong>{{ (Carbon\Carbon::parse($invoice->post_date))->formatLocalized('%B %d, %Y') }}</p>

                                    @if($invoice->status_id != 8 and $invoice->team_id != 61)
                                    <p><strong>Payment Due: </strong>{{ (Carbon\Carbon::parse($invoice->due_date))->formatLocalized('%B %d, %Y') }}</p>
                                    <p><strong>Amount Due ({{ $inv_country->currency_symbol }}): </strong>{{ $inv_country->currency_symbol }} {{ number_format($invoice->balance, 2, '.', ',') }}</p>
                                    @endif

                                    @if($team_details->id_number)
                                        <p>{{ $team_details->id_number }}</p>
                                    @endif
                                    @if($team_details->vat_number)
                                        <p>VAT: {{ $team_details->vat_number }}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="margin-top: 20px">
                                    <table class="table ">
                                        <thead style="background-color: #333;">
                                        <tr>
                                            <th style="color: #FFFFFF;text-align: left;">ID/Sr.No</th>
                                            <th style="color: #FFFFFF;text-align: left;">Product/Item</th>
                                            <th style="color: #FFFFFF;text-align: right;">Price</th>
                                            <th style="color: #FFFFFF;text-align: right;">Quantity</th>
                                            <th style="color: #FFFFFF;text-align: right;">Amount</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if($command == 'Invoice')
                                            @foreach($invoice->invItemsTab as $item)
                                                <tr>
                                                    <td style="text-align: left;"><strong>{{ $item->item_id }}</strong></td>
                                                    <td style="text-align: left;"><span style="color: #777;">
                                                            {{ $item->item_name }}
                                                            @if($item->hsnsac_code)<br>HSN/SAC: {{ $item->hsnsac_code }}@endif
                                                        </span></td>
                                                    <td style="text-align: right;">{{ number_format($item->item_price, 2, '.', ',') }}</td>
                                                    <td style="text-align: right;">{{ number_format($item->item_qty, 2, '.', ',') }}</td>
                                                    <td style="text-align: right;">{{ number_format($item->item_total, 2, '.', ',') }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            @foreach($invoice->quoteItemsTab as $item)
                                                <tr>
                                                    <td style="text-align: left;"><strong>{{ $item->item_id }}</strong></td>
                                                    <td style="text-align: left;"><span style="color: #777;">{{ $item->item_name }}<br>{{ $item->hsnsac_code }}</span></td>

                                                    <td style="text-align: right;">{{ number_format($item->item_price, 2, '.', ',') }}</td>
                                                    <td style="text-align: right;">{{ number_format($item->item_qty, 2, '.', ',') }}</td>

                                                    <td style="text-align: right;">{{ number_format($item->item_total, 2, '.', ',') }}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <hr/>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6 col-md-offset-6 col-lg-offset-6 col-sm-offset-6 col-xs-offset-6">
                                <p class="text-right"><b>Sub-total: </b>{{ $inv_country->currency_symbol }} {{ number_format($invoice->sub_total, 2, '.', ',') }}</p>

                                @if($invoice->discount_rate1 != 0)
                                    <p class="text-right">Discount ({{ number_format($invoice->discount_rate1, 2, '.', ',') }}%): {{ $inv_country->currency_symbol }} {{ number_format($invoice->discount_amount, 2, '.', ',') }}</p>
                                @endif

                                <p class="text-right">
                                    @if($command == 'Invoice')
                                        @foreach($invoice->invTaxTab as $tax)
                                            @if($tax->type == 'DED')
                                                {{ $tax->tax_name }} @if($tax->tax_display) ({{ $tax->tax_id }}) @endif
                                                ({{ number_format($tax->tax_rate, 2, '.', ',') }}%): {{ $inv_country->currency_symbol }} {{ number_format($tax->tax_amount, 2, '.', ',') }}
                                                <br>
                                            @endif
                                        @endforeach
                                    @else
                                        @foreach($invoice->quoteTaxTab as $tax)
                                            @if($tax->type == 'DED')
                                                {{ $tax->tax_name }} @if($tax->tax_display) ({{ $tax->tax_id }}) @endif
                                                ({{ number_format($tax->tax_rate, 2, '.', ',') }}%): {{ $inv_country->currency_symbol }} {{ number_format($tax->tax_amount, 2, '.', ',') }}
                                                <br>
                                            @endif
                                        @endforeach
                                    @endif
                                </p>
                                
                                <p class="text-right">
                                    @if($command == 'Invoice')
                                        @foreach($invoice->invTaxTab as $tax)
                                            @if($tax->type == 'TAX')
                                            {{ $tax->tax_name }} @if($tax->tax_display) ({{ $tax->tax_id }}) @endif
                                                ({{ number_format($tax->tax_rate, 2, '.', ',') }}%): {{ $inv_country->currency_symbol }} {{ number_format($tax->tax_amount, 2, '.', ',') }}
                                            <br>
                                            @endif
                                        @endforeach
                                    @else
                                        @foreach($invoice->quoteTaxTab as $tax)
                                            @if($tax->type == 'TAX')
                                            {{ $tax->tax_name }} @if($tax->tax_display) ({{ $tax->tax_id }}) @endif
                                                ({{ number_format($tax->tax_rate, 2, '.', ',') }}%): {{ $inv_country->currency_symbol }} {{ number_format($tax->tax_amount, 2, '.', ',') }}
                                            <br>
                                            @endif
                                        @endforeach
                                    @endif

                                </p>
                                <p class="text-right">Total Tax: {{ $inv_country->currency_symbol }} {{ number_format($invoice->tax_amount, 2, '.', ',') }}</p>
                                <hr>
                                <h3 class="text-right">
                                    @if($invoice->status_id == 8)
                                    Refund
                                    @endif
                                    {{ $command }} Amount: {{ $inv_country->currency_symbol }} {{ number_format($invoice->total_amount, 2, '.', ',') }}</h3>
                                <hr>

                                @if($invoice->status_id != 8 and $invoice->team_id != 61) <!-- only for starthub account team = 61 -->
                                <p class="text-right">Amount Paid: {{ $inv_country->currency_symbol }} {{ number_format($invoice->amount_paid, 2, '.', ',') }}</p>
                                <p class="text-right"><b>Amount Due:</b> {{ $inv_country->currency_symbol }} {{ number_format($invoice->balance, 2, '.', ',') }}</p>
                                @endif
                            </div>

                        </div>
                        <br>
                        @if($invoice->notes)
                            <p><strong>Notes :</strong><br>{!! nl2br(e($invoice->notes)) !!}</p><br>
                        @endif
                        <hr>
                        <div style='text-align:center;'>{!! nl2br(e($invoice->footer)) !!}</div>


                    </div>

                </div>

            </div>


            <div class="row hidden-print">
                <div
                        @if (Auth::check())
                        class="col-md-12"
                        @else
                        class="col-md-8 col-md-offset-2"
                        @endif
                >

                    <div class="pull-right m-b-15">

                        @if($command == 'Invoice')
                            <a type="button" href="{{ url('/public/invoices/' . $invitation_key . '/download') }}"
                               class="btn btn-info waves-effect waves-light">
                                PDF
                            </a>
                        @else
                            <a type="button" href="{{ url('/public/quotations/' . $invitation_key . '/download') }}"
                               class="btn btn-info waves-effect waves-light">
                                PDF
                            </a>
                        @endif

                        @if (Auth::check())

                            @if($invoice->status_id != 6)

                                <button type="button" class="btn btn-default waves-effect waves-light"
                                        v-on:click.prevent="send{{ isset($command) ? $command : 'Invoice' }}('{{ $invoice->from_email }}','{{ $invoice->contactTab->email }}','{{ $invoice->contactTab->emails }}','{{ $team->name }}','{{ $invoice->contactTab->name }}','{{ $invoice->open_id }}','{{ $team_details->email_subject }}')">
                                    Email {{ isset($command) ? $command : 'Invoice' }}
                                </button>

                            @endif

                            @if($command == 'Invoice')

                                @if($invoice->status_id == 6)
                                    <button type="button" class="btn btn-danger waves-effect waves-light" @click="stripeBillToInvoice({{ $invoice }})">
                                        <i class="fa fa-fw fa-rocket fa-btn"></i> Close & Invoice
                                    </button>
                                @endif

                                @if($invoice->status_id != 4 and $invoice->status_id != 7) <!-- need to add more conditions -->
                                <a type="button" class="btn btn-primary waves-effect waves-light" href="{{ url('/invoices/' . $invoice->open_id . '/edit') }}">
                                    <i class="fa fa-fw fa-pencil-square-o fa-btn"></i> Edit
                                </a>
                                @endif

                                @if($invoice->balance > 0 and $invoice->status_id != 6)
                                    <button type="button" class="btn btn-danger waves-effect waves-light" @click="showPayModal({{ $invoice }},{{ $invitation_key }})">
                                        Manual Payment
                                    </button>
                                @endif
                            @else
                                <button type="button" class="btn btn-danger waves-effect waves-light" @click="displayConvertToInvoice()">Convert to Invoice
                                </button>
                            @endif

                        @endif

                        @if ($gateway_id == 3 and ($invoice->balance * 100) >= 1 and $invoice->status_id != 6)
                            <button type="button" class="btn btn-default" id="rzp-button1" onclick="clicked()">
                                <i class="fa fa-fw fa-inr fa-btn"></i> Make Payment</button>
                        @elseif ($gateway_id == 1 and ($invoice->balance * 100) >= 1 and $invoice->status_id != 6)
                            <button id="customButton" class="btn btn-danger waves-effect waves-light" onclick="stripeClicked()">
                                <i class="fa fa-fw fa-cc-stripe fa-btn"></i> Make Payment</button>
                        @endif
                    </div>

                </div>
            </div>

            @if (Auth::check() and $command == 'Invoice')
                @include('user.payments.add_pymt_modal')
            @endif
            @if (Auth::check())
                @include('user.common.invquote.send_email_modal')

                @if ($command == 'Quotation')
                    @include('user.common.invquote.convert_to_invoice')
                @endif

            @endif

        </div>

    </invoice-api>

    @if (!Auth::check())
        <div class="row hidden-print">
            <div class="footer" style="left: 0px;">
                <div style='text-align:center;'>Created using <a href="https://www.pi.team" target="_blank">Pi.TEAM</a> - Finance Simplified </div>
            </div>
        </div>
    @endif

@endsection

@section('footer')

    @if($gateway_id == 3 and ($invoice->balance * 100) >= 1)

        <script src="https://checkout.razorpay.com/v1/checkout.js"></script>

        <script>
            var options = {
                "key": "{{ $public_key }}", //rzp_test_qqId1PvSrrDoKq
                "amount": "{{ $invoice->balance * 100 }}", // 2000 paise = INR 20
                "name": "{{ str_limit($invoice->from,20) }}",
                "description": "{{ str_limit($invoice->title,10) }} - {{ str_limit($invoice->summary,10) }}",
                "image": "/img/cropped-pi-logo-32x32.png",
                "handler": function (response){

                    //postRazor('/public/invoices/167503046/payment', { gateway_id: 3, razorpay_payment_id: response.razorpay_payment_id });

                    Bus.$emit('postPayment', { invoice_id: {{ $invitation_key }}, gateway_id: 3, razorpay_payment_id: response.razorpay_payment_id }); //need to be replaced with open_id
                    //gateway_id = 3
                },
                "theme": {
                    "color": "#5fbeaa"
                }
            };
            var rzp1 = new Razorpay(options);

            document.getElementById('rzp-button1').onclick = function(e){

                rzp1.open();
                e.preventDefault();

            }

            function clicked(){

                rzp1.open();
                //payment-form.preventDefault();
            }

            function postRazor(path, params, method) {
                method = method || "post"; // Set method to post by default if not specified.

                // The rest of this code assumes you are not using a library.
                // It can be made less wordy if you use one.
                var form = document.createElement("form");
                form.setAttribute("method", method);
                form.setAttribute("action", path);

                for(var key in params) {
                    if(params.hasOwnProperty(key)) {
                        var hiddenField = document.createElement("input");
                        hiddenField.setAttribute("type", "hidden");
                        hiddenField.setAttribute("name", key);
                        hiddenField.setAttribute("value", params[key]);

                        form.appendChild(hiddenField);

                        var hiddenField = document.createElement("input");
                        hiddenField.setAttribute("type", "hidden");
                        hiddenField.setAttribute("name", "_token");
                        hiddenField.setAttribute("value", "{{ csrf_token() }}");

                        form.appendChild(hiddenField);

                    }
                }

                document.body.appendChild(form);
                form.submit();
            }

        </script>

    @elseif($gateway_id == 1 and ($invoice->balance * 100) >= 1)

        <script src="https://checkout.stripe.com/checkout.js"></script>

        <script>

            var handler = StripeCheckout.configure({
                key: '{{ $public_key }}', //pk_test_iIvVDbNPrQFJ7CNDXqkBJUPd
                image: 'https://s3.amazonaws.com/stripe-uploads/acct_18qnTOGpIhOelKUymerchant-icon-1473240896932-Untitled%20design.png',
                locale: 'auto',
                currency: '{{ $inv_country->currency_code }}',
                token: function(token) {
                    // You can access the token ID with `token.id`.
                    // Get the token ID to your server-side code for use.
                    //postRazor('/public/invoices//payment', { stripe_token: token.id });
                    //console.log(token);

                    Bus.$emit('postPayment', { invoice_id: {{ $invitation_key }}, gateway_id: 1, stripe_token: token.id }); //need to be replaced with open_id

                }
            });


            // Close Checkout on page navigation:
            window.addEventListener('popstate', function() {
                handler.close();
            });

            function stripeClicked(){
                //alert('by function');
                //alert('test2');
                // Open Checkout with further options:
                handler.open({
                    name: '{{ str_limit($invoice->from,20) }}',
                    description: '{{ str_limit($invoice->title,10) }} - {{ str_limit($invoice->summary,10) }}',
                    amount: {{ $invoice->balance * 100 }}
                });
                //e.preventDefault();

            }

        </script>

    @endif

@endsection

@section('after-footer')



@endsection
