
<div id="convert-to-invoice-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Confirm Invoice Date and Due Date</h4>
            </div>
            <div class="modal-body">

                <div class="row">

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Invoice Title</label>
                        <div class="col-sm-8">

                            <input type="text" class="form-control text-capitalize" id="inv_title"
                                   v-model="convert_invoice.invoice_title"
                                   name="inv_title" placeholder="Invoice Title"
                                   v-validate="'required'" data-vv-scope="convert_to_invoice_form">
                            <i v-show="errors.has('inv_title', 'convert_to_invoice_form')"
                               class="fa fa-warning text-danger"></i>
                            <span v-show="errors.has('inv_title', 'convert_to_invoice_form')"
                                  class="help text-danger">Invoice title field is required.</span>

                        </div>


                    </div>

                </div>

                <div class="row">
                    <div class="form-group">

                        <label class="col-md-3 control-label">{{ isset($command) ? $command : 'Invoice' }} Date</label>

                        <div class="col-md-7">
                            <datepicker v-model="convert_invoice.invoice_date" input-class="form-control"
                                        monday-first>
                            </datepicker>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-3 control-label">{{ isset($command) ? $command : 'Invoice' }} Due</label>
                        <div class="col-md-7">
                            <datepicker v-model="convert_invoice.due_date" input-class="form-control"
                                        monday-first>
                            </datepicker>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group no-margin">
                            <button type="button" class="btn btn-white waves-effect"
                                    data-dismiss="modal">Close
                            </button>

                            <button href="#" class="btn btn-primary waves-effect submit-new-edit"
                                    v-on:click.prevent="convertToInvoice({{ $invitation_key }})">
                                Convert To Invoice
                            </button>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- /.modal -->