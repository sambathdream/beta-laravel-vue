
<div id="send-email-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Confirm Email Addresses</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group" style="margin-bottom: 0px;">

                            <div class="input-group">
                            <span class="input-group-btn">
                                    <button type="button" class="btn waves-effect waves-light btn-default">
                                        From </button>
                            </span>
                                <input type="text" class="form-control" id="from_email"
                                       value="{{ Auth::user()->email }}"
                                       name="from_email" placeholder="Email"
                                       v-validate="'email'" data-vv-scope="email_form" readonly>
                            </div>
                            <i v-show="errors.has('from_email', 'email_form')"
                               class="fa fa-warning text-danger"></i>
                            <span v-show="errors.has('from_email', 'email_form')"
                                  class="help text-danger">The email field must be a valid email.</span>
                        </div>
                    </div>
                </div>

                <hr>

                <!-- added by nik -->

                <div class="row">
                    <div class="col-md-10">
                        <div class="row" v-for="(row_to, index) in new_email.to_emails">
                            <div class="col-md-12">
                                <div class="form-group">

                                    <div class="input-group">
                            <span class="input-group-btn">
                                    <button type="button" class="btn waves-effect waves-light btn-white">
                                        To </button>
                            </span>
                                        <input type="text" class="form-control" id="to_emails[]"
                                               v-model="row_to.id"
                                               name="to_emails[]" placeholder="Email"
                                               v-validate="'email'" data-vv-scope="email_form">
                                    </div>
                                    <i v-show="errors.has('to_emails[]', 'email_form')"
                                       class="fa fa-warning text-danger"></i>
                                    <span v-show="errors.has('to_emails[]', 'email_form')"
                                          class="help text-danger">The email field must be a valid email.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">

                        <button class="btn btn-icon waves-effect waves-light btn-default" @click="addTo()">
                            <i class="fa  fa-plus-circle"></i> </button>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-10">
                        <div class="row" v-for="(row_cc, index) in new_email.cc_emails">
                            <div class="col-md-12">
                                <div class="form-group">

                                    <div class="input-group">
                        <span class="input-group-btn">
                                <button type="button" class="btn waves-effect waves-light btn-white">
                                    Cc </button>
                        </span>
                                        <input type="text" class="form-control" id="cc_emails[]"
                                               v-model="row_cc.id"
                                               name="cc_emails[]" placeholder="Email"
                                               v-validate="'email'" data-vv-scope="email_form">
                                    </div>
                                    <i v-show="errors.has('cc_emails[]', 'email_form')"
                                       class="fa fa-warning text-danger"></i>
                                    <span v-show="errors.has('cc_emails[]', 'email_form')"
                                          class="help text-danger">The email field must be a valid email.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">

                        <button class="btn btn-icon waves-effect waves-light btn-default" @click="addCc()">
                            <i class="fa  fa-plus-circle"></i> </button>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-12">
                        <div class="form-group">

                            <div class="input-group">
                            <span class="input-group-btn">
                                    <button type="button" class="btn waves-effect waves-light btn-white">
                                        Subject </button>
                            </span>
                                <input type="text" class="form-control" id="subject"
                                       v-model="new_email.subject"
                                       name="subject" placeholder="Subject"
                                       v-validate="'required'" data-vv-scope="email_form">
                            </div>
                            <i v-show="errors.has('subject', 'email_form')"
                               class="fa fa-warning text-danger"></i>
                            <span v-show="errors.has('subject', 'email_form')"
                                  class="help text-danger">Subject is mandatory.</span>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-md-12">
                        <div class="form-group">
                            <textarea v-model="new_email.message" id="message"
                                      class="form-control" rows="5"
                                      name="message" placeholder="Enter your message (optional)"
                                      v-validate data-vv-rules="max:250" data-vv-scope="email_form">
                            </textarea>
                            <i v-show="errors.has('message', 'email_form')" class="fa fa-warning text-danger"></i>
                            <span v-show="errors.has('message', 'email_form')" class="help text-danger">
                                            Maximum 250 characters allowed.</span>
                        </div>
                    </div>

                </div>
                <div class="row">

                    <div class="form-group">
                        <div class="col-sm-12">
                            <div class="checkbox checkbox-default">
                                <input id="copy_myself" type="checkbox" data-parsley-multiple="groups"
                                       v-model="new_email.copy_myself" disabled>
                                <label for="copy_myself"> Send a copy to myself <small>(not recommended - some email providers identify this as a spam)</small> </label>
                            </div>
                            <div class="checkbox checkbox-default">
                                <input id="attach_pdf" type="checkbox" data-parsley-multiple="groups"
                                       v-model="new_email.attach_pdf" disabled>
                                <label for="attach_pdf"> Attach the invoice as a PDF </label>
                            </div>

                        </div>
                    </div>

                </div>


                <!-- end by nik -->


            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group no-margin">
                            <button type="button" class="btn btn-white waves-effect"
                                    data-dismiss="modal">Close
                            </button>

                            <button href="#" class="btn btn-primary waves-effect submit-new-edit"
                                    v-on:click.prevent="send{{ isset($command) ? $command : 'Invoice' }}Api({{ $invitation_key }})">
                                Send Email
                            </button>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- /.modal -->