@extends('spark::layouts.app')

@section('header')

    <!-- Plugin Css-->
    <style>
        .datepicker {
            padding-left: 0px;
            padding-right: 0px;
            padding-top: 0px;
            padding-bottom: 0px;
        }


    </style>

    <link href="/assets/plugins/bootstrap-table/css/bootstrap-table.min.css" rel="stylesheet" type="text/css"/>

    <script>

        var command = "{{ isset($command) ? $command : 'Invoice' }}";

        jQuery(document).ready(function () {

            var token = '{{ $encrypted_csrf_token }}';
            Bus.$emit('getToken', token); //need to be replaced with open_id

        });

    </script>

    <link href="/css/multiselect.min.css" rel="stylesheet" type="text/css"/>

    <style>

        .multiselect__element {
            z-index: 200 !important;
        }

        .multiselect {
            z-index: 200 !important;
        }
    </style>

@endsection

@section('content')

    <invoice-gst-api :user="user" inline-template>
        <div class="container">
            <!-- Contact related code starts -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <form id="vue-invoice" class="form-horizontal" name="vue-invoice" enctype="multipart/form-data">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">

                        <div class="btn-group dropdown pull-right m-b-15">
                            <a class="btn btn-default waves-effect waves-light"
                               @click.prevent="save{{ isset($command) ? $command : 'Invoice' }}" href="#">Save
                                {{ isset($command) ? $command : 'Invoice' }}</a>
                        </div>

                        <h4 class="page-title">New {{ isset($command) ? $command : 'Invoice' }}</h4>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">

                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" id="business-from-tour">
                                            <div class="form-group">
                                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                                    <address class="m-b-0">
                                                        <strong>@{{ invoice_from.name }}</strong><br>
                                                        @{{ invoice_from.billing_address }}
                                                        @{{ invoice_from.billing_address_line_2 }}<br>
                                                        @{{ invoice_from.billing_city }} @{{ invoice_from.bill_state }}
                                                        @{{ invoice_from.billing_zip }} @{{ invoice_from.billing_country }}
                                                        <br>
                                                        @{{ invoice_from.phone }} @{{ invoice_from.email }}
                                                        @{{ invoice_from.website }}

                                                    </address>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" id="business-from-tour">
                                            <div class="form-group">
                                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                                    <a class="btn btn-white btn-custom waves-effect waves-light btn-xs"
                                                       href="/settings/companies/{{ Auth::user()->current_team_id }}#/payment-method">
                                                        Edit Business Details</a>
                                                    <a href="/config"
                                                       class="btn btn-white btn-custom waves-effect waves-light btn-xs">
                                                        Invoice Logo</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">

                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">{{ isset($command) ? $command : 'Invoice' }}
                                                Title</label>
                                            <div class="col-sm-8">

                                                <input type="text" class="form-control text-capitalize"
                                                       id="invoice_title"
                                                       v-model="invoice_form.invoice_title"
                                                       name="invoice_title"
                                                       placeholder="{{ isset($command) ? $command : 'Invoice' }}"
                                                       v-validate="'required'" data-vv-scope="invoice_form">
                                                <i v-show="errors.has('invoice_title', 'invoice_form')"
                                                   class="fa fa-warning text-danger"></i>
                                                <span v-show="errors.has('invoice_title', 'invoice_form')"
                                                      class="help text-danger">{{ isset($command) ? $command : 'Invoice' }}
                                                    title field is required.</span>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Summary</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="invoice_summary"
                                                       v-model="invoice_form.invoice_summary"
                                                       name="invoice_summary"
                                                       placeholder="{{ isset($command) ? $command : 'Invoice' }} Summary"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Currency</label>
                                            <div class="col-md-8">
                                                <div class="input-group">
                                                    <select class="form-control"
                                                            name="currency_id" id="inv_currency_id"
                                                            v-model="invoice_form.currency_id"
                                                            v-on:change="convertCurrency">
                                                        @foreach (App\Country::all() as $country)
                                                            <option value="{{ $country->id }}">
                                                                {{ $country->name }}
                                                                / Currency: {{ $country->currency_code }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">

                                    <div class="row">
                                        <div class="form-group">

                                            <label class="col-md-4 control-label">Debit GL</label>
                                            <div class="col-md-8">

                                                <select class="form-control selectpicker show-tick"
                                                        data-live-search="true"
                                                        name="debit_gl"
                                                        id="debit_gl" v-model="invoice_form.debit_gl">
                                                    @foreach ($gl_accounts as $gl_account)
                                                        <option value="{{ $gl_account->open_id }}">{{ $gl_account->name }}</option>
                                                    @endforeach

                                                </select>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Credit GL</label>
                                            <div class="col-md-8">
                                                <div class="checkbox checkbox-primary">
                                                    <input id="credit_gl_cb" type="checkbox"
                                                           v-model="invoice_form.credit_gl_cb">
                                                    <label for="credit_gl_cb">Post Credit</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">GST Setting</label>
                                            <div class="col-md-8">
                                                <div class="checkbox checkbox-primary">
                                                    <input id="line_tax" type="checkbox"
                                                           v-model="company_details.line_tax">
                                                    <label for="line_tax"> Enable Line Item Tax</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                        <div class="card-box">
                            <div class="row">

                                <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6">
                                    <div class="row" v-if="!invoice_form.contact_id">
                                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                            <div class="form-group">
                                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12"
                                                     id="customer-name-tour">

                                                    <typeahead style="bottom: 12px;"></typeahead>

                                                    <input type="hidden" class="form-control" id="contact_id"
                                                           v-model="invoice_form.contact_id"
                                                           name="contact_id"
                                                           v-validate="'required'" data-vv-scope="invoice_form">
                                                    <i v-show="errors.has('contact_id', 'invoice_form')"
                                                       class="fa fa-warning text-danger"></i>
                                                    <span v-show="errors.has('contact_id', 'invoice_form')"
                                                          class="help text-danger">Valid customer is required.</span>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" v-show="invoice_form.contact_id">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                                            <address class="m-b-0">
                                                <strong>BILL TO:</strong><br>
                                                <strong>@{{ new_contact.name }}</strong><br>
                                                <div v-show="new_contact.bill_address1"> @{{ new_contact.bill_address1 }}
                                                    <br></div>
                                                <div v-show="new_contact.bill_address2">@{{ new_contact.bill_address2 }}
                                                    <br></div>
                                                <div>@{{ new_contact.bill_city }} @{{ new_contact.bill_state }}</div>
                                                <div v-show="new_contact.bill_country_id">@{{ new_contact.bill_country_id }}
                                                    <br></div>
                                                <div v-show="new_contact.phone"><abbr
                                                            title="Phone">P:</abbr> @{{ new_contact.phone }}</div>
                                                <div v-show="invoice_form.customer_email"><abbr title="Email">@
                                                        :</abbr> @{{ invoice_form.customer_email }}</div>
                                            </address>

                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                                            <address class="m-b-0" v-if="new_contact.ship_address1">
                                                <strong>SHIP TO:</strong><br>
                                                <strong v-show="new_contact.ship_contact">@{{ new_contact.ship_contact }}</strong><br>
                                                <div v-show="new_contact.ship_address1"> @{{ new_contact.ship_address1 }}
                                                    <br></div>
                                                <div v-show="new_contact.ship_address2">@{{ new_contact.ship_address2 }}
                                                    <br></div>
                                                <div>@{{ new_contact.ship_city }} @{{ new_contact.ship_state }}</div>
                                                <div v-show="new_contact.ship_country_id">@{{ new_contact.ship_country_id }}</div>
                                                <div v-show="new_contact.ship_phone"><abbr
                                                            title="Phone">P:</abbr> @{{ new_contact.ship_phone }}</div>
                                            </address>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                            <div class="form-group">
                                                <div class="col-md-12" id="customer-add-tour"
                                                     style="margin-top: 8px;">
                                                    <button type="button"
                                                            class="btn btn-inverse btn-custom waves-effect waves-light btn-xs"
                                                            @click="createInvoiceAddContact">
                                                        <i class="md md-add"></i> Add New Customer
                                                    </button>
                                                    <a v-show="invoice_form.contact_id"
                                                       class="btn btn-primary btn-custom waves-effect waves-light btn-xs"
                                                       @click="createInvoiceEditContact"> Edit / View </a>
                                                    <a v-show="invoice_form.contact_id"
                                                       class="btn btn-default btn-custom waves-effect waves-light btn-xs"
                                                       @click="selectDiffCustomer"> Select Different Customer </a>

                                                    <!-- dynamic visibility for these buttons -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{ isset($command) ? $command : 'Invoice' }}
                                                #</label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control form-label" id="open_id"
                                                       name="open_id"
                                                       v-model="invoice_form.open_id" placeholder="Invoice ID"
                                                       v-validate data-vv-rules="required|numeric|max:10"
                                                       data-vv-scope="invoice_form"
                                                />

                                                <i v-show="errors.has('open_id', 'invoice_form')"
                                                   class="fa fa-warning text-danger"></i>
                                                <span v-show="errors.has('open_id', 'invoice_form')"
                                                      class="help text-danger">Invalid Input. Only upto 10 digits allowed.</span>

                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control" id="poso_number"
                                                       name="poso_number"
                                                       placeholder="PO/SO #" v-model="invoice_form.poso_number"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{ isset($command) ? $command : 'Invoice' }}
                                                Date</label>
                                            <div class="col-md-7">
                                                <datepicker v-model="invoice_form.invoice_date"
                                                            input-class="form-control"
                                                            monday-first>
                                                </datepicker>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{ isset($command) ? $command : 'Invoice' }}
                                                Due</label>
                                            <div class="col-md-7">
                                                <datepicker v-model="invoice_form.due_date" input-class="form-control"
                                                            monday-first>
                                                </datepicker>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                            <div class='row'>
                                <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'
                                     id="invoice-item-tour">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>

                                            <th width="15%">ID/Sr.No</th>
                                            <th width="38%">Product/Item Name or Description</th>
                                            <th width="15%">Price</th>
                                            <th width="15%">Quantity</th>
                                            <th width="15%">Total</th>
                                            <th width="2%">&nbsp;</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr v-for="(item, index) in invoice_form.items">
                                            <!-- <tr v-for="(item, index) in invoice_form.items" v-if="invoice_form.itemOptions.length !== 0"> -->

                                            <td>
                                                <input type="text" name="item_id[]" id="item_id[]" class="form-control"
                                                       v-model="item.item_id">
                                            </td>

                                            <td>

                                                <autocomplete
                                                        :index="index"
                                                        anchor="name"
                                                        label="buy_price"
                                                        :options="invoice_form.item_options"
                                                        :on-select="onAutocompleteSelect"
                                                        :on-input="onAutocompleteInput"
                                                        placeholder="Name or Description"
                                                >
                                                </autocomplete>

                                                <input type="hidden" name="credit_gl[]" id="credit_gl[]"
                                                       v-model="item.credit_gl">

                                                <div class="row" style="margin-top: 10px;">
                                                    <!--v-for="hsnsac in item.hsn_sac_tab" -->
                                                    <div class="col-md-12">
                                                        <input type="text" name="hsnsac_code[]" id="hsnsac_code[]"
                                                               class="form-control" v-model="item.hsnsac_code"
                                                               placeholder="HSN/SAC Code or Comments">

                                                    </div>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="text" name="item_price[]" id="item_price[]"
                                                               class="form-control" v-model="item.price"
                                                               v-validate data-vv-scope="invoice_form"
                                                               data-vv-rules="decimal:2" data-vv-delay="1000">
                                                        <i v-show="errors.has('item_price[]', 'invoice_form')"
                                                           class="fa fa-warning text-danger"></i>
                                                        <span v-show="errors.has('item_price[]', 'invoice_form')"
                                                              class="help text-danger">Invalid Input.</span>
                                                    </div>
                                                </div>

                                                <div class="row" style="margin-top: 5px;"
                                                     v-show="company_details.line_tax">
                                                    <div class="col-md-12">

                                                        <multiselect v-model="item.ded_items"
                                                                     :options="ded_rates"
                                                                     :multiple="true"
                                                                     :close-on-select="true"
                                                                     :clear-on-select="false"
                                                                     :hide-selected="true"
                                                                     placeholder="Deductibles/Discounts"
                                                                     label="name"
                                                                     track-by="name"
                                                                     :show-labels="false"
                                                                     :block-keys="['Tab', 'Enter']"
                                                        @close="selectedDedItems">
                                                        </multiselect>

                                                    </div>
                                                </div>

                                                <div class="row" style="margin-top: 1px;" v-show="item.ded_items">
                                                    <div class="col-md-12">
                                                        <p v-for="ded_row in item.ded_items" class="text-left"
                                                           style="margin-bottom: 5px;">
                                                            @{{ ded_row.name + ' (' + ded_row.rate + '%)' + ': '}}
                                                            @{{ ded_row.amount = (( item.qty * item.price ) * ded_row.rate / 100) }}
                                                        </p>
                                                    </div>
                                                </div>

                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="text" name="item_qty[]" id="item_qty[]"
                                                               class="form-control" v-model="item.qty"
                                                               v-validate data-vv-rules="decimal:2" data-vv-delay="1000"
                                                               data-vv-scope="invoice_form">
                                                        <i v-show="errors.has('item_qty[]', 'invoice_form')"
                                                           class="fa fa-warning text-danger"></i>
                                                        <span v-show="errors.has('item_qty[]', 'invoice_form')"
                                                              class="help text-danger">Invalid Input.</span>
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top: 5px;"
                                                     v-show="company_details.line_tax">
                                                    <div class="col-md-12">
                                                        <multiselect
                                                                v-model="item.tax_items"
                                                                :options="tax_rates"
                                                                :multiple="true"
                                                                :close-on-select="true"
                                                                :clear-on-select="false"
                                                                :hide-selected="true"
                                                                placeholder="Taxes"
                                                                label="name"
                                                                track-by="name"
                                                                :show-labels="false"
                                                                :block-keys="['Tab', 'Enter']"
                                                        @close="selectedTaxItems">
                                                        </multiselect>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="text" name="item_total[]" id="item_total[]"
                                                               class="form-control" readonly
                                                               v-bind:value="item.total">
                                                    </div>
                                                </div>

                                                <div class="row" style="margin-top: 1px;" v-show="item.tax_items">
                                                    <div class="col-md-12">
                                                        <p v-for="tax_row in item.tax_items" class="text-left"
                                                           style="margin-bottom: 5px;">
                                                            @{{ tax_row.name + ' (' + tax_row.rate + '%)' + ': '}}
                                                            @{{ tax_row.amount = (( item.total ) * tax_row.rate / 100) }}
                                                        </p>
                                                    </div>
                                                </div>

                                            </td>
                                            <td>
                                                <button class="btn btn-xs btn-danger delete m-t-5" type="button" @click=
                                                "removeLine(item)"><i class="fa fa-remove"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <button class="btn-xs waves-effect btn-success" type="button"
                                                        @click="addLine"
                                                        id="item-row-tour">
                                                    <i class="fa fa-plus"></i> NEW ROW
                                                </button>
                                                <button class="btn-xs waves-effect btn-default" type="button"
                                                        id="add-product-tour"
                                                        data-toggle="modal" data-target="#add-product-modal">
                                                    <i class="fa fa-cart-plus"></i> NEW PRODUCT
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">

                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <button class="btn-xs waves-effect waves-light btn-primary"
                                                        type="button" id="add-product-tour"
                                                        data-toggle="modal" @click="showTaxDedModal(1)">
                                                    <i class="fa fa-plus-circle"></i> NEW TAX
                                                </button>

                                                <button class="btn-xs waves-effect waves-light btn-primary"
                                                        type="button" id="add-product-tour"
                                                        data-toggle="modal" @click="showTaxDedModal(2)">
                                                    <i class="fa fa-plus-circle"></i> NEW DEDUCTIONS
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="form-group">
                                            <div class="col-sm-9">
                                                <multiselect v-model="invoice_form.ded_items" :options="ded_rates"
                                                             :multiple="true"
                                                             :close-on-select="true" :clear-on-select="false"
                                                             :hide-selected="true" placeholder="Deductibles/Discounts"
                                                             label="name"
                                                             track-by="name" @close="selectedDedItems">
                                                </multiselect>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">

                                        <div class="form-group">
                                            <div class="col-sm-9">
                                                <multiselect v-model="invoice_form.tax_items" :options="tax_rates"
                                                             :multiple="true"
                                                             :close-on-select="true" :clear-on-select="false"
                                                             :hide-selected="true" placeholder="Taxes" label="name"
                                                             track-by="name" @close="selectedTaxItems">
                                                </multiselect>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-offset-0 col-md-offset-0 col-lg-offset-0 col-xs-12 col-sm-5 col-md-5 col-lg-5"
                                     style="padding-right: 20px">

                                    <div v-if="this.products_ded_total">
                                        <p class="text-right" v-if="this.products_ded_total[1]"><b>Total
                                                Deductions:</b> @{{this.products_ded_total[1]}}</p>
                                    </div>

                                    <p class="text-right"><b>Sub-total:</b> @{{ this.sub_total }}</p>

                                    <p v-for="tax_row in invoice_form.ded_items" class="text-right">
                                        @{{ tax_row.name + ' (' + tax_row.rate + '%)' + ': '}}
                                        @{{ tax_row.amount = (( sub_total ) * tax_row.rate / 100) }}
                                    </p>

                                    <div v-if="this.products_tax_total">
                                        <p class="text-right" v-if="this.products_tax_total[1]"><b>Total
                                                Line Taxes:</b> @{{this.products_tax_total[1]}}</p>
                                    </div>

                                    <p v-for="tax_row in invoice_form.tax_items" class="text-right">
                                        @{{ tax_row.name + ' (' + tax_row.rate + '%)' + ': '}}
                                        @{{ tax_row.amount = (( sub_total - ded_amount_inc_tax ) * tax_row.rate / 100) }}
                                    </p>

                                    <hr>
                                    <h3 class="text-right">Total: @{{ total_amount }}</h3>

                                </div>
                                <div class="row"
                                     v-show="this.invoice_form.currency_id != this.invoice_form.company_currency_id">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-right: 20px;">
                                        <div class="pull-right">
                                            Local Currency: @{{ currencyConvert.company_country }} - Invoice total after
                                            conversion: @{{ converted_amount }} at @{{ currencyConvert.rate }}
                                            <button type="button"
                                                    class="btn btn-xs btn-white" @click="convertCurrency">
                                                <i class="md md-add"></i> Update Rate &nbsp;
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end Panel -->
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion-test-3">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion-test-3" href="#collapseOne-3"
                                           aria-expanded="false" class="collapsed">
                                            Notes
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne-3" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <textarea name="invoice_notes" id="invoice_notes" class="form-control"
                                                  v-model="invoice_form.notes"
                                                  v-validate data-vv-rules="max:250"
                                                  data-vv-scope="invoice_form"></textarea>
                                        <i v-show="errors.has('invoice_notes', 'invoice_form')"
                                           class="fa fa-warning text-danger"></i>
                                        <span v-show="errors.has('invoice_notes', 'invoice_form')"
                                              class="help text-danger">
                                            Maximum 250 characters allowed.</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion-test-1">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion-test-1" href="#collapseOne-1"
                                           aria-expanded="false" class="collapsed">
                                            Footer
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne-1" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <textarea name="invoice_footer" id="invoice_footer" class="form-control"
                                                  v-model="invoice_form.footer"
                                                  v-validate data-vv-rules="max:250"
                                                  data-vv-scope="invoice_form"></textarea>
                                        <i v-show="errors.has('invoice_footer', 'invoice_form')"
                                           class="fa fa-warning text-danger"></i>
                                        <span v-show="errors.has('invoice_footer', 'invoice_form')"
                                              class="help text-danger">
                                            Maximum 250 characters allowed.</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-12">

                        <div class="btn-group dropup pull-right m-b-15"
                             id="save-invoice-tour">
                            <a class="btn btn-default waves-effect waves-light"
                               @click.prevent="save{{ isset($command) ? $command : 'Invoice' }}" href="#">Save
                                {{ isset($command) ? $command : 'Invoice' }}</a>
                        </div>

                    </div>
                </div>

            </form>

            <!-- Modal -->
            @include('user.inventory.add_item_modal')
            @include('user.contacts.add_contact_modal')
            @include('user.config.update_tax_ded_modal')

        </div>

    </invoice-gst-api>

@endsection

@section('footer')


@endsection


@section('after-footer')

    <!-- Tour script -->

    @if(!$boot_tour->create_invoice)

        <script>

            $(document).ready(function () {

                // Instance the tour
                var tour = new Tour({
                    steps: [
                        {
                            element: "#business-from-tour",
                            title: "Invoice From?",
                            content: "Usually these are your business details. You can click on 'Edit Business Details' to change it permanently. ",
                            placement: "bottom",
                            backdrop: true,
                            backdropContainer: 'body'
                        },
                        {
                            element: '#customer-add-tour',
                            title: "Add New Customer",
                            content: "You can add new customer and it will be reflected here immediately.",
                            placement: "bottom",
                            backdrop: true,
                            backdropContainer: 'body'
                        },
                        {
                            element: "#customer-name-tour",
                            title: "Search Customer",
                            content: "Just enter customer name and we will search if already created.",
                            placement: "bottom",
                            backdrop: true,
                            backdropContainer: 'body'
                        },
                        {
                            element: "#invoice-item-tour",
                            title: "Add Invoice Items",
                            content: "You can enter item no, item name, price etc or you can add a new product then search it in the item name.",
                            placement: "top",
                            backdrop: true,
                            backdropContainer: 'body'
                        },
                        {
                            element: "#add-product-tour",
                            title: "New Product",
                            content: "You can add new product and search using item name field.",
                            placement: "bottom",
                            backdrop: true,
                            backdropContainer: 'body'
                        },
                        {
                            element: "#item-row-tour",
                            title: "Add Invoice Item",
                            content: "Add new item row on the invoice. You can add unlimited rows.",
                            placement: "bottom",
                            backdrop: true,
                            backdropContainer: 'body'
                        },
                        {
                            element: "#save-invoice-tour",
                            title: "Save Invoice",
                            content: "Once ready just click this button and invoice will be saved.",
                            placement: "left",
                            backdrop: true,
                            backdropContainer: 'body'
                        }

                    ],

                    smartPlacement: true,

                    onEnd: function (tour) {
                        Bus.$emit('closeTour', 3); //0 = dashboard

                    },

                });

                // Initialize the tour
                tour.init();
                tour.start();

            });


        </script>

    @endif

    <!-- End of Tour script -->

@endsection
