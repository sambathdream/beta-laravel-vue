<!DOCTYPE html>
<html lang="en">
<head>
    <style>
        <?php include(public_path() . '/assets/css/bootstrap.min.css');?>
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>{{ $command }}</title>

</head>
<body>
<div class="wrapper">
    <div class="row">
        <div class="col-md-12">

            <div class="col-md-12">

                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="">
                            @if($team_details->imageSrc)
                                <img style="vertical-align: middle;padding-top: 2%;" alt="{{ isset($team->name) ? $team->name : 'Missing Name' }}"
                                     width="80%" src="{{ $team_details->imageSrc }}">
                            @else
                                <h1>{{ $command }}</h1>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                        <div class="pull-right">
                            <address>
                                <strong>{{ isset($team->name) ? $team->name : 'Missing Name' }}</strong><br>
                                {{ isset($team->billing_address) ? $team->billing_address : '' }}@if(strlen($team->billing_address) > 0)<br>@endif
                                {{ isset($team->billing_address_line_2) ? $team->billing_address_line_2 : '' }}@if(strlen($team->billing_address_line_2) > 0)<br>@endif
                                {{ isset($team->billing_city) ? $team->billing_city : '' }}{{ isset($team->billing_state) ? ' ' . $team->billing_state : '' }}@if((strlen($team->billing_city) > 0) or (strlen($team->billing_state) > 0))<br>@endif
                                {{ isset($team->billing_zip) ? $team->billing_zip : '' }}{{ isset($team->billing_country) ? ' ' . $team->billing_country : '' }}@if((strlen($team->billing_zip) > 0) or (strlen($team->billing_country) > 0))<br>@endif
                                {{ isset($team_details->email) ? 'Email: ' . $team_details->email : '' }}@if((strlen($team_details->email) > 0))<br>@endif
                                {{ isset($team_details->website) ? 'Web: ' . $team_details->website : '' }}@if((strlen($team_details->website) > 0))<br>@endif
                                @if($team_details->gst_code)GST Code: {{ $team_details->gst_code }}<br>@endif
                            </address>

                        </div>

                    </div>
                </div>

                <hr style="margin-top: 0px;margin-bottom: 0px;">

                <div class="row">

                    <h4 align="center"><strong>{{ $invoice->title }}</strong></h4>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                        <address>
                            <strong>BILL TO:</strong><br>
                            <strong>{{ isset($invoice->contactTab->name) ? $invoice->contactTab->name : '' }}</strong><br>
                            {{ isset($invoice->contactTab->bill_address1) ? $invoice->contactTab->bill_address1 : '' }}@if(strlen($invoice->contactTab->bill_address1) > 0)<br>@endif
                            {{ isset($invoice->contactTab->bill_address2) ? $invoice->contactTab->bill_address2 : '' }}@if(strlen($invoice->contactTab->bill_address2) > 0)<br>@endif
                            {{ isset($invoice->contactTab->bill_city) ? $invoice->contactTab->bill_city : '' }}{{ isset($invoice->contactTab->bill_state) ? ' ' . $invoice->contactTab->bill_state : '' }}@if((strlen($invoice->contactTab->bill_city) > 0) or (strlen($invoice->contactTab->bill_state) > 0))<br>@endif
                            {{ isset($invoice->contactTab->bill_postal_code) ? $invoice->contactTab->bill_postal_code : '' }}{{ isset($invoice->contactTab->bill_country_id) ? ' ' . $invoice->contactTab->bill_country_id : '' }}@if((strlen($invoice->contactTab->bill_postal_code) > 0) or (strlen($invoice->contactTab->bill_country_id) > 0))<br>@endif
                            {{ isset($invoice->contactTab->email) ? $invoice->contactTab->email : '' }}@if(strlen($invoice->contactTab->email) > 0)<br>@endif
                            {{ isset($invoice->contactTab->contact1) ? $invoice->contactTab->contact1 : '' }}{{ isset($invoice->contactTab->phone) ? ' ' . $invoice->contactTab->phone : '' }}@if((strlen($invoice->contactTab->contact1) > 0) or (strlen($invoice->contactTab->phone) > 0))<br>@endif
                        </address>
                        @if($invoice->contactTab->gst_code)
                            <p>GST/GSTIN: {{ $invoice->contactTab->gst_code }}</p>
                        @endif
                        @if($invoice->summary)
                            <p>{{ $invoice->summary }}</p>
                        @endif

                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                        <address>

                            @if(strlen($invoice->contactTab->ship_address1)>0)

                                <strong>SHIP TO:</strong><br>
                                <strong>{{ isset($invoice->contactTab->ship_contact) ? $invoice->contactTab->ship_contact : $invoice->contactTab->name }}</strong><br>

                                {{ isset($invoice->contactTab->ship_address1) ? $invoice->contactTab->ship_address1 : '' }}@if(strlen($invoice->contactTab->ship_address1) > 0)<br>@endif
                                {{ isset($invoice->contactTab->ship_address2) ? $invoice->contactTab->ship_address2 : '' }}@if(strlen($invoice->contactTab->ship_address2) > 0)<br>@endif
                                {{ isset($invoice->contactTab->ship_city) ? $invoice->contactTab->ship_city : '' }}{{ isset($invoice->contactTab->ship_state) ? ' ' . $invoice->contactTab->ship_state : '' }}@if((strlen($invoice->contactTab->ship_city > 0)) or (strlen($invoice->contactTab->ship_state > 0)))<br>@endif
                                {{ isset($invoice->contactTab->ship_postal_code) ? $invoice->contactTab->ship_postal_code : '' }}{{ isset($invoice->contactTab->ship_country_id) ? ' ' . $invoice->contactTab->ship_country_id : '' }}@if((strlen($invoice->contactTab->ship_postal_code) > 0) or (strlen($invoice->contactTab->ship_country_id) > 0))<br>@endif
                                {{ isset($invoice->contactTab->ship_phone) ? ' ' . $invoice->contactTab->ship_phone : '' }}@if(strlen($invoice->contactTab->ship_phone) > 0)<br>@endif

                            @else
                                &nbsp;
                            @endif
                        </address>

                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <div class="pull-right">
                            <p><strong>{{ $command }} Number: </strong> {{ $invoice->open_id }}</p>
                            @if($invoice->poso_number)
                                <p><strong class="">PO/SO Number:</strong>{{ $invoice->poso_number }}</p>
                            @endif
                            <p><strong>{{ $command }} Date: </strong>{{ (Carbon\Carbon::parse($invoice->post_date))->formatLocalized('%B %d, %Y') }}</p>
                            @if($invoice->status_id != 8 and $invoice->team_id != 61)
                            <p><strong>Payment Due: </strong>{{ (Carbon\Carbon::parse($invoice->due_date))->formatLocalized('%B %d, %Y') }}</p>
                            <p><strong>Amount Due ({{ $inv_country->currency_symbol }}): </strong>{{ $inv_country->currency_symbol }} {{ number_format($invoice->balance, 2, '.', ',') }}</p>
                            @endif
                            @if($team_details->id_number)
                                <p>{{ $team_details->id_number }}</p>
                            @endif
                            @if($team_details->vat_number)
                                <p>VAT: {{ $team_details->vat_number }}</p>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="table" style="page-break-after:always; margin-top: 20px">
                            <table class="table ">
                                <thead style="background-color: #333;">
                                <tr>
                                    <th style="color: #FFFFFF;text-align: left;">ID/Sr.No</th>
                                    <th style="color: #FFFFFF;text-align: left;">Product/Item</th>
                                    <th style="color: #FFFFFF;text-align: right;">Price</th>
                                    <th style="color: #FFFFFF;text-align: right;">Quantity</th>
                                    <th style="color: #FFFFFF;text-align: right;">Amount</th>
                                </tr>
                                </thead>
                                <tbody>

                                @if($command == 'Invoice')
                                    @foreach($invoice->invItemsTab as $indexKey => $item)

                                        <tr>
                                            <td style="text-align: left;"><strong>{{ $item->item_id }}</strong></td>
                                            <td style="text-align: left;"><span style="color: #777;">{{ $item->item_name }}
                                                    @if($item->hsnsac_code)<br>HSN/SAC: {{ $item->hsnsac_code }}@endif
                                                </span></td>
                                            <td style="text-align: right;">{{ number_format($item->item_price, 2, '.', ',') }}</td>
                                            <td style="text-align: right;">{{ number_format($item->item_qty, 2, '.', ',') }}</td>
                                            <td style="text-align: right;">{{ number_format($item->item_total, 2, '.', ',') }}</td>
                                        </tr>

                                    @endforeach
                                @else
                                    @foreach($invoice->quoteItemsTab as $item)
                                        <tr>
                                            <td style="text-align: left;"><strong>{{ $item->item_id }}</strong></td>
                                            <td style="text-align: left;"><span style="color: #777;">{{ $item->item_name }}<br>{{ $item->hsnsac_code }}</span></td>
                                            <td style="text-align: right;">{{ number_format($item->item_price, 2, '.', ',') }}</td>
                                            <td style="text-align: right;">{{ number_format($item->item_qty, 2, '.', ',') }}</td>
                                            <td style="text-align: right;">{{ number_format($item->item_total, 2, '.', ',') }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>



                        <hr/>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7 col-sm-7 col-xs-7 col-lg-7 col-md-offset-5 col-lg-offset-5 col-sm-offset-5 col-xs-offset-5">
                        <p class="text-right"><b>Sub-total: </b>{{ $inv_country->currency_symbol }} {{ number_format($invoice->sub_total, 2, '.', ',') }}</p>

                        @if($invoice->discount_rate1 != 0)
                            <p class="text-right">Discount ({{ number_format($invoice->discount_rate1, 2, '.', ',') }}%): {{ $inv_country->currency_symbol }} {{ number_format($invoice->discount_amount, 2, '.', ',') }}</p>
                        @endif

                        <p class="text-right">
                            @if($command == 'Invoice')
                                @foreach($invoice->invTaxTab as $tax)
                                    @if($tax->type == 'DED')
                                        {{ $tax->tax_name }} ({{ number_format($tax->tax_rate, 2, '.', ',') }}%): {{ $inv_country->currency_symbol }} {{ number_format($tax->tax_amount, 2, '.', ',') }}
                                        <br>
                                    @endif
                                @endforeach
                            @else
                                @foreach($invoice->quoteTaxTab as $tax)
                                    @if($tax->type == 'DED')
                                        {{ $tax->tax_name }} ({{ number_format($tax->tax_rate, 2, '.', ',') }}%): {{ $inv_country->currency_symbol }} {{ number_format($tax->tax_amount, 2, '.', ',') }}
                                        <br>
                                    @endif
                                @endforeach
                            @endif
                        </p>
                        <p class="text-right">
                            @if($command == 'Invoice')
                                @foreach($invoice->invTaxTab as $tax)
                                    @if($tax->type == 'TAX')
                                        {{ $tax->tax_name }} ({{ number_format($tax->tax_rate, 2, '.', ',') }}%): {{ $inv_country->currency_symbol }} {{ number_format($tax->tax_amount, 2, '.', ',') }}
                                        <br>
                                    @endif
                                @endforeach
                            @else
                                @foreach($invoice->quoteTaxTab as $tax)
                                    @if($tax->type == 'TAX')
                                        {{ $tax->tax_name }} ({{ number_format($tax->tax_rate, 2, '.', ',') }}%): {{ $inv_country->currency_symbol }} {{ number_format($tax->tax_amount, 2, '.', ',') }}
                                        <br>
                                    @endif
                                @endforeach
                            @endif

                        </p>

                        <p class="text-right">Total Tax: {{ $inv_country->currency_symbol }} {{ number_format($invoice->tax_amount, 2, '.', ',') }}</p>
                        <hr>
                        <h3 class="text-right">
                            @if($invoice->status_id == 8)
                                Refund
                            @endif
                                {{ $command }} Amount: {{ $inv_country->currency_symbol }} {{ number_format($invoice->total_amount, 2, '.', ',') }}</h3>
                        <hr>
                        @if($invoice->status_id != 8 and $invoice->team_id != 61) <!-- only for starthub account team = 61 -->
                        <p class="text-right">Amount Paid: {{ $inv_country->currency_symbol }} {{ number_format($invoice->amount_paid, 2, '.', ',') }}</p>
                        <p class="text-right"><b>Amount Due:</b> {{ $inv_country->currency_symbol }} {{ number_format($invoice->balance, 2, '.', ',') }}</p>
                        @endif
                    </div>

                </div>
                <br>
                @if($invoice->notes)
                    <p><strong>Notes :</strong><br>{!! nl2br(e($invoice->notes)) !!}</p><br>
                @endif
                <hr>
                <div style='text-align:center;'>{!! nl2br(e($invoice->footer)) !!}</div>

            </div>
        </div>
    </div>
</div>

</body>
</html>