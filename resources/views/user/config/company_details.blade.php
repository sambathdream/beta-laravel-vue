<form role="form" class="form-horizontal">

    <h2><i class="fa fa-btn fa-map-marker"></i>Basic</h2>
    <div class="form-group">
        <label class="col-md-4 control-label">Name</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="name"
                   id="name" readonly v-model="company_details.name">
        </div>

        <a class="btn btn-icon waves-effect btn-default waves-light" href="/settings/companies/{{ Auth::user()->current_team_id }}"> <i class="fa fa-edit"></i> </a>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">ID Number</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="id_number"
                   id="id_number" v-model="company_details.id_number"
                   placeholder="ID Number (displayed on the invoice)">

        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">VAT Number</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="vat_number"
                   id="vat_number" v-model="company_details.vat_number"
                   placeholder="VAT Number (displayed on the invoice)">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Company GST Code / GSTN</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="gst_code"
                   id="gst_code" v-model="company_details.gst_code"
                   placeholder="GST Number (will be displayed on the invoice)">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">&nbsp;</label>

        <div class="col-md-6">
            <div class="checkbox checkbox-default">
                <input id="checkbox1" type="checkbox" data-parsley-multiple="groups" data-parsley-mincheck="2"
                       v-model="company_details.line_tax">
                <label for="checkbox1"> Enable Line Item Tax</label>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Website</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="url"
                   id="url" v-model="company_details.website"
                   placeholder="Website (displayed on the invoice)"
                   >

        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Email</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="email"
                   id="email" v-model="company_details.email"
                   placeholder="Email (displayed on the invoice)"
                   >

        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Default Email Subject</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="email_subject"
                   id="email_subject" v-model="company_details.email_subject"
                   placeholder="Default Email Subject (Used For Auto Emails)"
            >

        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Phone</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="phone"
                   placeholder="Phone Number (displayed on the invoice)"
                   id="phone" v-model="company_details.phone">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Invoice Notes</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="inv_notes"
                   id="inv_notes" v-model="company_details.inv_notes"
                   placeholder="Invoice Notes (displayed on the invoice)">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Invoice Footer</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="inv_footer"
                   id="inv_footer" v-model="company_details.inv_footer"
                   placeholder="Invoice Footer (displayed on the invoice)">
        </div>
    </div>

    <!-- quotes -->
    <div class="form-group">
        <label class="col-md-4 control-label">Quote Notes</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="quote_notes"
                   id="quote_notes" v-model="company_details.quote_notes"
                   placeholder="Quote Notes (displayed on the quote)">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Quote Footer</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="quote_footer"
                   id="quote_footer" v-model="company_details.quote_footer"
                   placeholder="Quote Footer (displayed on the quote)">
        </div>
    </div>
    <!-- end quotes -->

    <div class="Image-input">
        <div class="Image-input__image-wrapper">
            <i v-show="! company_details.imageSrc" class="icon fa fa-picture-o"></i>
            <img v-show="company_details.imageSrc" class="Image-input__image" :src="company_details.imageSrc">
        </div>
        <input @change="previewThumbnail" class="Image-input__input" name="thumbnail" type="file">
    </div>
    <br>

    <div class="form-group">
        <label class="col-md-4 control-label">Company Size</label>

        <div class="col-md-6">
            <select class="form-control select2" data-live-search="true" id="company_size"
                    name="company_size" v-model="company_details.company_size">
                @foreach(StaticArray::$company_size as $key => $size)
                    <option value="{{ $key }}">{{ $size }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Industry</label>

        <div class="col-md-6">
            <select class="form-control select2" data-live-search="true" id="industry" name="industry"
                    v-model="company_details.industry">
                @foreach(StaticArray::$industry as $key => $ind)
                    <option value="{{ $key }}">{{ $ind }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <!-- Update Button -->
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="ladda-button ladda-button-demo btn btn-primary" data-style="zoom-in"
                    @click.prevent="updateConfig(1)">
                <span>Update</span>
            </button>
        </div>
    </div>

    <h2>
        <i class="fa fa-btn fa-map-marker"></i>
        Address
    </h2>

    <div class="form-group">
        <label class="col-md-4 control-label">Address</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" v-model="company_address.billing_address">
            <span class="help-block" style="display: none;"></span></div>
    </div>
    <div class="form-group"><label class="col-md-4 control-label">Address Line
            2</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" v-model="company_address.billing_address_line_2">
            <span class="help-block" style="display: none;"></span></div>
    </div>
    <div class="form-group"><label class="col-md-4 control-label">City</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" v-model="company_address.billing_city">
            <span class="help-block" style="display: none;"></span></div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label">State &amp; ZIP / Postal Code</label>
        <div class="col-sm-3">
            <input type="text" placeholder="State" class="form-control" v-model="company_address.billing_state">
            <span class="help-block" style="display: none;"></span></div>
        <div class="col-sm-3">
            <input type="text" placeholder="Postal Code" class="form-control" v-model="company_address.billing_zip">
            <span class="help-block" style="display: none;"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label">Country</label>
        <div class="col-sm-6">
            <select class="form-control" v-model="company_address.billing_country">
                @foreach (app(Laravel\Spark\Repositories\Geography\CountryRepository::class)->all() as $key => $country)
                    <option value="{{ $key }}">{{ $country }}</option>
                @endforeach
            </select>
            <span class="help-block" style="display: none;">
        </span>
        </div>
    </div>

    <!-- Update Button -->
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <a type="submit" class="ladda-button ladda-button-demo btn btn-primary" data-style="zoom-in"
               @click.prevent="updateConfig(5)">
                <span>Update</span>
            </a>
        </div>
    </div>

    <div class="alert alert-info">
        <strong>Please subscribe to update your address.</strong> This same address gets reflected on the invoice.
    </div>

</form>