<form role="form" class="form-horizontal">

    <div class="form-group">
        <label class="col-md-4 control-label">Currency</label>

        <div class="col-md-6">
            <select class="form-control select2" name="currency_id" id="currency_id"
                    v-model="localization.currency_id">
                @foreach ($countries as $country)
                    <option value="{{ $country->id }}">{{ $country->name }} / {{ $country->currency_code }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Language</label>

        <div class="col-md-6">
            <select class="form-control select2" name="language_id" id="language_id"
                    v-model="localization.language_id" disabled>
                <option value="840">English / United States</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">TimeZone</label>

        <div class="col-md-6">

            {!! Timezone::selectForm('', null, array('class' => 'select2 form-control', 'id' => 'timezone', 'name' => 'timezone', 'v-model' => 'localization.timezone')) !!}

        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Date Format</label>

        <div class="col-md-6">
            <select class="select2 form-control" id="date_time_format" name="date_time_format" v-model="localization.date_time_format">
                @foreach(App\DateTimeList::all() as $datetime)
                    <option value="{{ $datetime->id }}">{{ $datetime->name }}</option>
                @endforeach
            </select>

        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">First Day of the Week</label>

        <div class="col-md-6">

            <select class="select2 form-control" id="week_first_day" name="week_first_day" v-model="localization.week_first_day">
                @foreach(StaticArray::$week_days as $key => $day)
                    <option value="{{ $key }}">{{ $day }}</option>
                @endforeach
            </select>

        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">First Month of the Financial Year</label>

        <div class="col-md-6">
            <select class="select2 form-control" id="year_first_month" name="year_first_month" v-model="localization.year_first_month">
                @foreach(StaticArray::$year_months as $key => $month)
                    <option value="{{ $key }}">{{ $month }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <!-- Update Button -->
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="ladda-button ladda-button-demo btn btn-primary" data-style="zoom-in" @click.prevent="updateConfig(2)">
                <span>Update</span>
            </button>
        </div>
    </div>
</form>