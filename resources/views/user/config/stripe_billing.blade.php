<form role="form" class="form-horizontal">

    <div class="form-group">
        <label class="col-md-4 control-label">Frequency</label>

        <div class="col-md-6">
            <select class="form-control" name="stripe_bill_freq" id="stripe_bill_freq"
                    v-model="stripe_billing.frequency">
                <option value="1">Daily</option>
                <option value="2">Weekly</option>
                <option value="3">Monthly</option>
                <option value="4">Bi-Monthly</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Stripe Signing Secret</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="signingsecret"
                   id="signingsecret" placeholder="Signing Secret" v-model="stripe_billing.signingsecret">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">WebHook</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="webhook"
                   id="webhook" placeholder="WebHook" v-model="stripe_billing.webhook" disabled
                   v-validate="'url'" data-vv-scope="company_details" data-vv-delay="1000">

            <i v-show="errors.has('webhook', 'company_details')" class="fa fa-warning text-danger"></i>
            <span v-show="errors.has('webhook', 'company_details')"
                  class="help text-danger">Invalid URL</span>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Email</label>

        <div class="col-md-6">
            <div class="checkbox checkbox-custom">
                <input id="auto_email" type="checkbox" checked="" v-model="stripe_billing.auto_email">
                <label for="auto_email">
                    Auto email all paid invoices
                </label>
            </div>
        </div>

    </div>

    <div class="form-group" v-show="stripe_billing.auto_email">

        <label class="col-md-4 control-label">From</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="from_email"
                   id="from_email" placeholder="example@domain.com" v-model="stripe_billing.from_email"
                   v-validate="'required|email'" data-vv-scope="stripe_billing" data-vv-delay="500">

            <i v-show="errors.has('from_email', 'stripe_billing')" class="fa fa-warning text-danger"></i>
            <span v-show="errors.has('from_email', 'stripe_billing')"
                  class="help text-danger">Invalid Email</span>
        </div>

    </div>

    <div class="form-group" v-show="stripe_billing.auto_email">

        <label class="col-md-4 control-label">CC (optional)</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="cc_email"
                   id="cc_email" placeholder="example@domain.com" v-model="stripe_billing.cc_email"
                   v-validate="'email'" data-vv-scope="stripe_billing" data-vv-delay="500">

            <i v-show="errors.has('cc_email', 'stripe_billing')" class="fa fa-warning text-danger"></i>
            <span v-show="errors.has('cc_email', 'stripe_billing')"
                  class="help text-danger">Invalid Email</span>
        </div>

    </div>


    <!-- Update Button -->
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="ladda-button ladda-button-demo btn btn-primary" data-style="zoom-in" @click.prevent="updateConfig(6)">
                <span>Update</span>
            </button>
        </div>
    </div>
</form>