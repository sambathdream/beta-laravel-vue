<div class="row">
    <div class="col-md-12">
        <div class="form-group">

            <input type="text" class="form-control" name="tax_name" id="tax_name" v-model="tax_rate.name"
                   v-validate="'required'" data-vv-scope="tax_rate" placeholder="Tax Name e.g. Service Tax or TDS">
            <i v-show="errors.has('tax_name', 'tax_rate')"
               class="fa fa-warning text-danger"></i>
            <span v-show="errors.has('tax_name', 'tax_rate')"
                  class="help text-danger">Name field is required.</span>

        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">

            <input type="text" class="form-control" name="tax_rate" id="tax_rate" v-model="tax_rate.rate"
                   v-validate="'required|decimal:3'" data-vv-scope="tax_rate" placeholder="Tax Rate in % e.g. 5 or 10">
            <i v-show="errors.has('tax_rate', 'tax_rate')"
               class="fa fa-warning text-danger"></i>
            <span v-show="errors.has('tax_rate', 'tax_rate')"
                  class="help text-danger">Please enter valid rate e.g. 5.000 .</span>

        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">

            <input type="text" class="form-control" name="tax_id" id="tax_id" v-model="tax_rate.tax_id"
                   placeholder="Tax ID/Number e.g. Service Tax: 1234ABC">

        </div>
    </div>
</div>
<div class="row" v-show="tax_rate.tax_id">
    <div class="col-md-12">
        <div class="form-group">

            <div class="checkbox checkbox-default">
                <input id="checkbox1" type="checkbox" data-parsley-multiple="groups" data-parsley-mincheck="2"
                       v-model="tax_rate.tax_display">
                <label for="checkbox1"> Show tax number on invoice / quote</label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">

            <select class="form-control" name="gl_account_open_id" id="gl_account_open_id"
                    v-model="tax_rate.gl_account_open_id">
                @foreach ($gl_accounts as $gl_account)
                    <option value="{{ $gl_account->open_id }}">{{ $gl_account->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="checkbox checkbox-default">
                <input id="checkbox2" type="checkbox" data-parsley-multiple="groups" data-parsley-mincheck="2" disabled
                       v-model="tax_rate.recoverable">
                <label for="checkbox2"> This is a recoverable tax </label>
            </div>
            <div class="checkbox checkbox-default">
                <input id="checkbox3" type="checkbox" data-parsley-multiple="groups" data-parsley-mincheck="2" required=""
                       v-model="tax_rate.compound">
                <label for="checkbox3"> This is a compound tax </label>
            </div>

        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">


        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12" style="padding-right: 5%;">
        <div class="form-group no-margin">
            <button type="button" class="btn btn-white waves-effect"
                    data-dismiss="modal">Close
            </button>
            <a class="btn btn-primary waves-effect submit-new-edit"
                    @click="addTaxRate(3)">Save Tax
            </a>

        </div>
    </div>
</div>