<div class="card-box">
    <div class="row">
        <div class="col-md-8 col-md-offset-4">
            <!--
            <h4 class="m-t-0 header-title"><b>Tax Configurations</b></h4>
            -->
            <p class="text-muted font-13 m-b-30">
                Use below configuration to enable or disable invoice / quote deductibles.
            </p>
            <div class="checkbox checkbox-custom">
                <input id="checkbox0" type="checkbox" checked="" disabled>
                <label for="checkbox0">
                    Enable Invoice / Quote Deductibles
                </label>
            </div>

            <div class="checkbox checkbox-custom">
                <input id="checkbox11" type="checkbox" disabled>
                <label for="checkbox11">
                    Enable Line Item Deductibles
                </label>
            </div>

        </div>

    </div>
</div>


<form role="form" class="form-horizontal">

    <div class="form-group">
        <label class="col-md-4 control-label">Deductible Name <span style="color:#FF0000;">*</span></label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="tax_name" id="tax_name" v-model="tax_rate.name"
                   v-validate="'required'" data-vv-scope="tax_rate">
            <i v-show="errors.has('tax_name', 'tax_rate')"
               class="fa fa-warning text-danger"></i>
            <span v-show="errors.has('tax_name', 'tax_rate')"
                  class="help text-danger">Name field is required.</span>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Deductible Rate <span style="color:#FF0000;">*</span></label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="tax_rate" id="tax_rate" v-model="tax_rate.rate"
                   v-validate="'required|decimal:3'" data-vv-scope="tax_rate">
            <i v-show="errors.has('tax_rate', 'tax_rate')"
               class="fa fa-warning text-danger"></i>
            <span v-show="errors.has('tax_rate', 'tax_rate')"
                  class="help text-danger">Please enter valid rate e.g. 5.000 .</span>

        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Deductible ID/Number <span style="color:#FF0000;">&nbsp;</span><br><small>e.g. TAN 9427583067</small></label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="tax_id" id="tax_id" v-model="tax_rate.tax_id">

        </div>
    </div>

    <div class="form-group" v-show="tax_rate.tax_id">
        <label class="col-sm-4 control-label">Invoice/Quote Display <span style="color:#FF0000;">&nbsp;</span></label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-default">
                <input id="checkbox1" type="checkbox" data-parsley-multiple="groups" data-parsley-mincheck="2"
                       v-model="tax_rate.tax_display">
                <label for="checkbox1"> Show deductible number on invoice </label>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Deductible Account <span style="color:#FF0000;">*</span></label>

        <div class="col-md-6">
            <select class="form-control" name="gl_account_open_id" id="gl_account_open_id"
                    v-model="tax_rate.gl_account_open_id">
                @foreach ($gl_accounts as $gl_account)
                    <option value="{{ $gl_account->open_id }}">{{ $gl_account->name }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <!-- Update Button -->
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary" @click.prevent="addTaxRate(7)">
                <span>Add</span>
            </button>
        </div>
    </div>

</form>


<div class="row">
    <div class="col-sm-12">

        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>Deductible Name</th>
                <th>Deductible %</th>
                <th>Deductible ID</th>
                <th>GL Account</th>
                <th>Display</th>
                <th>Action</th>
            </tr>
            </thead>

            <tbody>
            <tr v-for="(taxrate, index) in tax_rates_ded">
                <td>@{{ taxrate.name }}</td>
                <td>@{{ taxrate.rate }}</td>
                <td>@{{ taxrate.tax_id }}</td>
                <td>
                    @{{ taxrate.gl_account_name }}
                </td>
                <td>
                    <div v-if="taxrate.tax_display">
                        <span class="label label-success">Yes</span>
                    </div>
                    <div v-else>
                        <span>No</span>
                    </div>
                </td>
                <td>
                    <button class="btn-xs btn-icon waves-effect waves-light btn-danger" @click="removeDedRate(taxrate.id,index)"><i class="fa fa-remove"></i>
                    </button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>