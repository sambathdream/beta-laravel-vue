<form role="form" class="form-horizontal">

    <div class="form-group">
        <label class="col-md-4 control-label">Payment Credit</label>

        <div class="col-md-6">
            <select class="form-control select2" name="paymentcr" id="paymentcr"
                    v-model="accounting.paymentcr">
                @foreach ($gl_accounts as $gl_account)
                    <option value="{{ $gl_account->open_id }}">{{ $gl_account->name }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Payment Debit</label>

        <div class="col-md-6">
            <select class="form-control select2" name="paymentdb" id="paymentdb"
                    v-model="accounting.paymentdb">
                @foreach ($gl_accounts as $gl_account)
                    <option value="{{ $gl_account->open_id }}">{{ $gl_account->name }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Process Fee Credit</label>

        <div class="col-md-6">
            <select class="form-control select2" name="processfeecr" id="processfeecr"
                    v-model="accounting.processfeecr">
                @foreach ($gl_accounts as $gl_account)
                    <option value="{{ $gl_account->open_id }}">{{ $gl_account->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label">Process Fee Debit</label>

        <div class="col-md-6">
            <select class="form-control select2" name="processfeedb" id="processfeedb"
                    v-model="accounting.processfeedb">
                @foreach ($gl_accounts as $gl_account)
                    <option value="{{ $gl_account->open_id }}">{{ $gl_account->name }}</option>
                @endforeach
            </select>
        </div>
    </div>


    <div class="form-group">
        <label class="col-md-4 control-label">Invoice Credit</label>

        <div class="col-md-6">
            <select class="form-control select2" name="invoicecr" id="invoicecr"
                    v-model="accounting.invoicecr">
                @foreach ($gl_accounts as $gl_account)
                    <option value="{{ $gl_account->open_id }}">{{ $gl_account->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label">Invoice Debit</label>

        <div class="col-md-6">
            <select class="form-control select2" name="invoicedb" id="invoicedb"
                    v-model="accounting.invoicedb">
                @foreach ($gl_accounts as $gl_account)
                    <option value="{{ $gl_account->open_id }}">{{ $gl_account->name }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <!--
    <div class="form-group">
        <label class="col-md-4 control-label">Tax Credit</label>

        <div class="col-md-6">
            <select class="form-control select2" name="taxcr" id="taxcr"
                    v-model="accounting.taxcr">
                @foreach ($gl_accounts as $gl_account)
                    <option value="{{ $gl_account->open_id }}">{{ $gl_account->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label">Tax Debit</label>

        <div class="col-md-6">
            <select class="form-control select2" name="taxdb" id="taxdb"
                    v-model="accounting.taxdb">
                @foreach ($gl_accounts as $gl_account)
                    <option value="{{ $gl_account->open_id }}">{{ $gl_account->name }}</option>
                @endforeach
            </select>
        </div>
    </div>

    -->

    <div class="form-group">
        <label class="col-md-4 control-label">Product Credit</label>

        <div class="col-md-6">
            <select class="form-control select2" name="productcr" id="productcr"
                    v-model="accounting.productcr">
                @foreach ($gl_accounts as $gl_account)
                    <option value="{{ $gl_account->open_id }}">{{ $gl_account->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label">Product Debit</label>

        <div class="col-md-6">
            <select class="form-control select2" name="productdb" id="productdb"
                    v-model="accounting.productdb">
                @foreach ($gl_accounts as $gl_account)
                    <option value="{{ $gl_account->open_id }}">{{ $gl_account->name }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Manual Credit</label>

        <div class="col-md-6">
            <select class="form-control select2" name="manualcr" id="manualcr"
                    v-model="accounting.manualcr">
                @foreach ($gl_accounts as $gl_account)
                    <option value="{{ $gl_account->open_id }}">{{ $gl_account->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label">Manual Debit</label>

        <div class="col-md-6">
            <select class="form-control select2" name="manualdb" id="manualdb"
                    v-model="accounting.manualdb">
                @foreach ($gl_accounts as $gl_account)
                    <option value="{{ $gl_account->open_id }}">{{ $gl_account->name }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <!-- Update Button -->
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="ladda-button ladda-button-demo btn btn-primary"  data-style="zoom-in" @click.prevent="updateConfig(4)">
                <span>Update</span>
            </button>
        </div>
    </div>
</form>