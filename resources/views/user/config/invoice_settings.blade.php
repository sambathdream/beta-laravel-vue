<form role="form" class="form-horizontal">

    <div class="form-group">
        <label class="col-md-4 control-label">Website</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="url"
                   id="url" v-model="company_details.website"
                   placeholder="Website (displayed on the invoice)"
                   v-validate="'url'" data-vv-scope="company_details" data-vv-delay="1000">

            <i v-show="errors.has('url', 'company_details')" class="fa fa-warning text-danger"></i>
            <span v-show="errors.has('url', 'company_details')"
                  class="help text-danger">Invalid URL</span>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Email</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="email"
                   placeholder="Email (displayed on the invoice)"
                   id="email" v-model="company_details.email">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Phone</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="phone"
                   placeholder="Phone Number (displayed on the invoice)"
                   id="phone" v-model="company_details.phone">
        </div>
    </div>

    <!-- Update Button -->
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary" @click.prevent="update">
                <span>Update</span>
            </button>
        </div>
    </div>

</form>