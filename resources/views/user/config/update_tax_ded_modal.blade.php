<div id="update_tax_ded_modal" name="update_tax_ded_modal" class="modal fade" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content p-0">

                <ul class="nav nav-tabs navtab-bg nav-justified">
                    <li v-bind:class="{ active: tax_modal_flag }">
                        <a href="#tax_update-tab-1" data-toggle="tab" aria-expanded="true">
                            <span class="visible-xs"><i class="fa fa-home">Tax Rates</i></span>
                            <span class="hidden-xs">Tax Rates</span>
                        </a>
                    </li>

                    <li v-bind:class="{ active: ded_modal_flag }">
                        <a href="#ded_update-tab-2" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs"><i class="fa fa-user">Deductibles</i></span>
                            <span class="hidden-xs">Deductibles</span>
                        </a>
                    </li>

                </ul>
                <div class="tab-content">

                    <form class="tab-pane" v-bind:class="{ active: tax_modal_flag }" id="tax_update-tab-1">
                        
                        @include('user.config.update_tax_rate_form')

                    </form>
                    <form class="tab-pane" v-bind:class="{ active: ded_modal_flag }" id="ded_update-tab-2">

                        @include('user.config.update_ded_rate_form')
                    </form>
                </div>

            </div>


        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->