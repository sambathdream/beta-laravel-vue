@extends('spark::layouts.app')

@section('header')

    <!-- Ladda buttons css -->
    <link href="/assets/plugins/ladda-buttons/css/ladda-themeless.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/bootstrap-table/css/bootstrap-table.min.css" rel="stylesheet" type="text/css" />

    <style>
        .Image-input {
            display: flex;
        }

        .Image-input__image-wrapper {
            flex-basis: 80%;
            height: 150px;
            flex: 2.5;
            border-radius: 1px;
            margin-right: 10px;
            overflow-y: hidden;
            border-radius: 1px;
            background: #eee;
            justify-content: center;
            align-items: center;
            display: flex;
        }

        .Image-input__image-wrapper > .icon {
            color: #ccc;
            font-size: 50px;
            cursor: default;
        }

        .Image-input__image {
            max-width: 100%;
            border-radius: 1px;
        }

        .Image-input__input-wrapper {
            overflow: hidden;
            position: relative;
            background: #eee;
            border-radius: 1px;
            float: left;
            flex: 1;
            display: flex;
            justify-content: center;
            align-items: center;
            color: rgba(0, 0, 0, 0.2);
            transition: 0.4s background;
        }

        .Image-input__input-wrapper:hover {
            background: #e0e0e0;
        }

        .Image-input__input {
            cursor: inherit;
            display: block;
            font-size: 999px;
            min-height: 100%;
            opacity: 0;
            position: absolute;
            right: 0;
            text-align: right;
            top: 0;
            cursor: pointer;
        }
    </style>
    

@endsection

@section('content')

    <config-api :user="user" :teams="teams" inline-template>
        <div class="spark-screen container">
            <div class="row">
                <!-- Tabs -->
                <div class="col-md-4">
                    <div class="panel panel-color panel-primary panel-flush">
                        <div class="panel-heading">
                            Basic
                        </div>

                        <div class="panel-body">
                            <div class="spark-settings-tabs">
                                <ul class="nav spark-settings-stacked-tabs" role="tablist">
                                    <!-- Profile Link -->
                                    <li class="active" role="presentation">
                                        <a href="#company_details" aria-controls="company_details" role="tab" data-toggle="tab" v-on:click="showConfig(1)">
                                            <i class="fa fa-fw fa-btn fa-building-o"></i>Company Details
                                        </a>
                                    </li>

                                    <li role="presentation">
                                        <a href="#localization" aria-controls="localization" role="tab" data-toggle="tab" v-on:click="showConfig(2)">
                                            <i class="fa fa-fw fa-btn fa-globe"></i>Localization
                                        </a>
                                    </li>

                                    <li role="presentation">
                                        <a href="#tax_rate" aria-controls="tax_rate" role="tab" data-toggle="tab" v-on:click="showConfig(3)">
                                            <i class="fa fa-fw fa-btn fa-university"></i>Tax Rates
                                        </a>
                                    </li>

                                    <li role="presentation">
                                        <a href="#deductible" aria-controls="deductible" role="tab" data-toggle="tab" v-on:click="showConfig(7)">
                                            <i class="fa fa-fw fa-btn fa-scissors"></i>Deductible
                                        </a>
                                    </li>

                                    <li role="presentation">
                                        <a href="#accounting" aria-controls="accounting" role="tab" data-toggle="tab" v-on:click="showConfig(4)">
                                            <i class="fa fa-fw fa-btn fa-money"></i>Accounting
                                        </a>
                                    </li>

                                    <li role="presentation">
                                        <a href="#stripe_billing" aria-controls="stripe_billing" role="tab" data-toggle="tab" v-on:click="showConfig(6)">
                                            <i class="fa fa-fw fa-btn fa-cc-stripe"></i>Stripe Billing
                                        </a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>

                </div>

                <!-- Tab Panels -->
                <div class="col-md-8">
                    <div class="tab-content">

                        <!-- Company Details -->
                        <div role="tabpanel" class="tab-pane active" id="company_details">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        Company Details
                                    </div>
                                    <div class="pull-right"><!----></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body"><!---->  <!---->
                                    @include('user.config.company_details')
                                </div>
                            </div>


                        </div>

                        <!-- Localization -->
                        <div role="tabpanel" class="tab-pane" id="localization">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        Localization
                                    </div>
                                    <div class="pull-right"><!----></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body"><!---->  <!---->
                                    @include('user.config.localization')
                                </div>
                            </div>
                        </div>

                        <!-- Tax Rate -->
                        <div role="tabpanel" class="tab-pane" id="tax_rate">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        Tax Rates
                                    </div>
                                    <div class="pull-right"><!----></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body"><!---->  <!---->
                                    @include('user.config.tax_rate')
                                </div>
                            </div>
                        </div>

                        <!-- Tax Rate -->
                        <div role="tabpanel" class="tab-pane" id="deductible">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        Deductible
                                    </div>
                                    <div class="pull-right"><!----></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body"><!---->  <!---->
                                    @include('user.config.deductible')
                                </div>
                            </div>
                        </div>

                        <!-- Accounting -->
                        <div role="tabpanel" class="tab-pane" id="accounting">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        Accounting
                                    </div>
                                    <div class="pull-right"><!----></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body"><!---->  <!---->
                                    @include('user.config.accounting')
                                </div>
                            </div>
                        </div>

                        <!-- Invoice Settings -->
                        <div role="tabpanel" class="tab-pane" id="invoice_settings">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        Localization
                                    </div>
                                    <div class="pull-right"><!----></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body"><!---->  <!---->
                                    @include('user.config.invoice_settings')
                                </div>
                            </div>


                        </div>

                        <!-- Invoice Settings -->
                        <div role="tabpanel" class="tab-pane" id="stripe_billing">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        Stripe Billing
                                    </div>
                                    <div class="pull-right"><!----></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body"><!---->  <!---->
                                    @include('user.config.stripe_billing')
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </config-api>

@endsection

@section('footer')


@endsection

@section('after-footer')

    <script src="/assets/plugins/ladda-buttons/js/spin.min.js"></script>
    <script src="/assets/plugins/ladda-buttons/js/ladda.min.js"></script>
    <script src="/assets/plugins/ladda-buttons/js/ladda.jquery.min.js"></script>

    <script>

        $(document).ready(function () {

            // Bind normal buttons
            $('.ladda-button').ladda('bind', {timeout: 2000});

            // Bind progress buttons and simulate loading progress
            Ladda.bind('.progress-demo .ladda-button', {
                callback: function (instance) {
                    var progress = 0;
                    var interval = setInterval(function () {
                        progress = Math.min(progress + Math.random() * 0.1, 1);
                        instance.setProgress(progress);

                        if (progress === 1) {
                            instance.stop();
                            clearInterval(interval);
                        }
                    }, 200);
                }
            });


            var l = $('.ladda-button-demo').ladda();

            l.click(function () {
                // Start loading
                l.ladda('start');

                // Timeout example
                // Do something in backend and then stop ladda
                setTimeout(function () {
                    l.ladda('stop');
                }, 12000)


            });

        });

    </script>


@endsection
