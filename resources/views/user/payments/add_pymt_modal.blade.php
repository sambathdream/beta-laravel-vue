
<div id="add-pay-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Post Manual Payment</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button type="button" class="btn waves-effect waves-light btn-primary">
                                        Amount {{ $inv_country->currency_symbol }} </button>
                                </span>
                                <input type="text" name="amount" class="form-control" id="amount"
                                       placeholder="Amount" v-model="new_payment.amount"
                                       v-validate:amount.initial="'required|decimal:2'" data-vv-scope="pay_form">
                            </div>

                            <i v-show="errors.has('amount', 'pay_form')" class="fa fa-warning text-danger"></i>
                            <span v-show="errors.has('amount', 'pay_form')" class="help text-danger">
                                This field must be numeric and may contain 2 decimal points.
                            </span>

                        </div>

                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <select class="form-control selectpicker show-tick" data-live-search="true"
                                    id="payment_type_id" name="payment_type_id"
                                    v-model="new_payment.payment_type_id">
                                <option value="0">Payment Type</option>
                                @foreach(StaticArray::$pay_type as $key => $type)
                                    <option value="{{ $key }}">{{ $type }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <datepicker v-model="new_payment.payment_date" input-class="form-control"
                                        monday-first>
                            </datepicker>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">

                            <input type="text" name="name" class="form-control" id="transaction_reference"
                                   placeholder="Transaction Reference" v-model="new_payment.transaction_reference"
                                   v-validate="'required'">
                            <i v-show="errors.has('pay_form', 'transaction_reference')" class="fa fa-warning"></i>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group no-margin">
                            <button type="button" class="btn btn-white waves-effect"
                                    data-dismiss="modal">Close
                            </button>

                            <button href="#" class="btn btn-primary waves-effect submit-new-edit"
                                    v-on:click.prevent="postPaymentFun">
                                Post
                            </button>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- /.modal -->