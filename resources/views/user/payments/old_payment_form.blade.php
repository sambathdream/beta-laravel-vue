<form id="payment-form"  class="form-horizontal" name="payment-form" novalidate role="form" method="POST"
      action="{{ url('/public/invoices/' . $id . '/payment') }}">

    {{ csrf_field() }}

    <h3>Contact Information</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">

                <div class="col-md-12">
                    <input class="form-control" placeholder="Full Name" id="full_name" type="text"
                           name="full_name" value="{{ $invoice->contactTab->name }}">

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="col-md-12">
                    <input class="form-control" placeholder="Email"
                           autocomplete="email" id="email" type="email"
                           name="email" value="{{ $invoice->contactTab->email }}">
                </div>
            </div>
        </div>
    </div>

    <h3>Billing Address</h3>
    <h5><span class="help">*Billing address must match address associated with credit card.</span></h5>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="col-md-12">
                    <input class="form-control" autocomplete="address-line1" placeholder="Street"
                           id="address1" type="text" name="address1" value="">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="col-md-12">
                    <input class="form-control" autocomplete="address-line2" placeholder="Apt/Suite"
                           id="address2" type="text" name="address2" value="">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="col-md-12">
                    <input class="form-control" autocomplete="address-level2" placeholder="City"
                           id="city" type="text" name="city" value="">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="col-md-12">
                    <input class="form-control" autocomplete="address-level1"
                           placeholder="State/Province" id="state" type="text" name="state" value="">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="col-md-12">
                    <input class="form-control" autocomplete="postal-code" placeholder="Postal Code"
                           id="postal_code" type="text" name="postal_code" value="">
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <div class="col-md-12">
                    <input class="form-control" autocomplete="country" placeholder="Country"
                           id="country_id" type="text" name="country_id" value="">
                </div>
            </div>
        </div>
    </div>

    <h3>Billing Method</h3>

    <div class="row">
        <div class="col-md-9">
            <div class="form-group">
                <div class="col-md-12">
                    <input class="form-control" id="card_number" placeholder="Card Number"
                           autocomplete="cc-number" type="text" name="">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="col-md-12">
                    <input class="form-control" id="cvv" placeholder="CVV"
                           autocomplete="off" type="text" name="">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="col-md-12">
                    <select class="form-control" id="expiration_month" autocomplete="cc-exp-month" name="">
                        <option value="" disabled="disabled" selected="selected">Expiration Month</option>
                        <option value="1">01 - January</option>
                        <option value="2">02 - February</option>
                        <option value="3">03 - March</option>
                        <option value="4">04 - April</option>
                        <option value="5">05 - May</option>
                        <option value="6">06 - June</option>
                        <option value="7">07 - July</option>
                        <option value="8">08 - August</option>
                        <option value="9">09 - September</option>
                        <option value="10">10 - October</option>
                        <option value="11">11 - November</option>
                        <option value="12">12 - December</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="col-md-12">
                    <select class="form-control" id="expiration_year"
                            autocomplete="cc-exp-year" name="">
                        <option value="" disabled="disabled" selected="selected">Expiration Year</option>
                        <option value="2016">2016</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                        <option value="2024">2024</option>
                        <option value="2025">2025</option>
                        <option value="2026">2026</option>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="padding-top:18px">
        <div class="col-md-6">
            <button type="submit" class="btn btn-success btn-lg">PAY NOW - {{ $invoice->balance }} USD</button>



        </div>

        <div class="col-md-6">
            <div class="pull-right">
                <img src="/img/Visa-Icon.png"
                     alt="Visa"
                     style="width: 70px; display: inline; margin-right: 6px;"/>
                <img src="/img/MasterCard-Icon.png"
                     alt="Master Card" style="width: 70px; display: inline; margin-right: 6px;"/>
                <img src="/img/AmericanExpress-Icon.png"
                     alt="American Express" style="width: 70px; display: inline; margin-right: 6px;"/>
            </div>
        </div>
    </div>

</form>

<button id="rzp-button1" onclick="clicked()">RazorPay</button>