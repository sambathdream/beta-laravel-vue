@extends('spark::layouts.app')

@section('header')


    <link href="assets/plugins/bootstrap-table/css/bootstrap-table.min.css" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

    <!-- DataTables -->
    <!--
    <link href="/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>

    <style>
        .calendar {
            position: relative !important;
        }

    </style>

@endsection

@section('content')

    <report-api :user="user" inline-template>
        <div class="container">

            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <h4 class="m-t-0 header-title"><b>Reports</b></h4>

                        <div class="row">
                            &nbsp;
                        </div>
                        <div class="row">
                            <div class="col-lg-9">

                                <form class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-lg-4 control-label">Report Selection: </label>
                                    <div class="col-lg-8">
                                    <select class="form-control" name="report_type" id="report_type"
                                            v-model="report_type">
                                        <option value="1">Customers</option>
                                        <option value="2">Invoices</option>
                                        <option value="3">Payments</option>
                                        <option value="4">Products/Services/Assets</option>
                                        <option value="5">Tax</option>
                                        <option value="7">Refunds</option>
                                    </select>
                                    </div>
                                </div>
                                </form>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <div class="form-group text-left m-b-0">
                                            <button class="btn btn-primary waves-effect waves-light" onclick="loadTable()">
                                                Run
                                            </button>
                                            <button type="button" class="btn btn-inverse waves-effect waves-light"
                                            @click="downloadReport()">
                                                <i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;Export
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" v-show="report_type">

                            <div class="col-lg-9">

                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">Invoice Date Range: </label>
                                        <div class="col-lg-8">

                                            <div id="reportrange" class="pull-left"
                                                 style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                                <span></span> <b class="caret"></b>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>

                    <div class="card-box table-responsive" v-show="report_type == 1">
                        <div class="row">

                            <table id="datatable-contact" class="table table-striped table-bordered dt-responsive nowrap">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Total Amount</th>
                                    <th>Amount Paid</th>
                                    <th>Balance</th>
                                </tr>
                                </thead>
                            </table>

                        </div>

                    </div>

                    <div class="card-box table-responsive" v-show="report_type == 2">
                        <div class="row">

                            <table id="datatable-invoice" class="table table-striped table-bordered dt-responsive nowrap">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Invoice Id</th>
                                    <th>Invoice Date</th>
                                    <th>Status</th>
                                    <th>Total Amount</th>
                                    <th>Amount Paid</th>
                                    <th>Balance</th>
                                    <th>Payment Date</th>
                                </tr>
                                </thead>
                            </table>

                        </div>

                    </div>

                    <div class="card-box table-responsive" v-show="report_type == 3">
                        <div class="row">

                            <table id="datatable-payment" class="table table-striped table-bordered dt-responsive nowrap">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Invoice Id</th>
                                    <th>Invoice Date</th>
                                    <th>Payment Date</th>
                                    <th>Total Amount</th>
                                    <th>Payment Amount</th>
                                </tr>
                                </thead>
                            </table>

                        </div>

                    </div>

                    <div class="card-box table-responsive" v-show="report_type == 4">
                        <div class="row">

                            <table id="datatable-invent" class="table table-striped table-bordered dt-responsive nowrap">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Invoice Id</th>
                                    <th>Invoice Date</th>
                                    <th>Status</th>
                                    <th>Total Amount</th>
                                    <th>Item ID</th>
                                    <th>Item Name</th>
                                    <th>Price</th>
                                    <th>Qty</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                            </table>

                        </div>

                    </div>


                    <div class="card-box table-responsive" v-show="report_type == 5">
                        <div class="row">

                            <table id="datatable-tax" class="table table-striped table-bordered dt-responsive nowrap">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Invoice Id</th>
                                    <th>Invoice Date</th>
                                    <th>Status</th>
                                    <th>Total Amount</th>
                                    <th>Tax Name</th>
                                    <th>Tax Rate</th>
                                    <th>Tax Amount</th>
                                </tr>
                                </thead>
                            </table>

                        </div>

                    </div>

                    <div class="card-box table-responsive" v-show="report_type == 7">
                        <div class="row">

                            <table id="datatable-refund" class="table table-striped table-bordered dt-responsive nowrap">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Credit Id</th>
                                    <th>Credit Date</th>
                                    <th>Refund Date</th>
                                    <th>Total Amount</th>
                                    <th>Refund Amount</th>
                                </tr>
                                </thead>
                            </table>

                        </div>

                    </div>

                </div>
            </div>

        </div>

    </report-api>



@endsection

@section('footer')

    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

    <script>

        function loadTable() {

            //alert('start: ' + );

            if($("#report_type").val() == '1') {

                $("#datatable-contact").dataTable().fnDestroy();

                var oTable = $('#datatable-contact').DataTable({
                    processing: true,
                    serverSide: true,
                    //"bDestroy": true
                    ajax: {
                        'url': 'https://finance.pi.team/api/displayreport',
                        'type': 'POST',
                        'headers': {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        },
                        'data': {
                            'cmd': 'display',
                            'type': '1', //customer
                            'from_date': $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD'),
                            'to_date': $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD')
                        }
                    },
                    /*
                     data: function (d) {
                     d.name = $('input[name=name]').val();
                     d.operator = $('select[name=operator]').val();
                     d.user_invoices = $('input[name=user_invoices]').val();
                     },
                     */
                    columns: [

                        {data: 'name', name: 'name'},
                        {data: 'email', name: 'email'},
                        {data: 'total_amount', name: 'total_amount'},
                        {data: 'amount_paid', name: 'amount_paid'},
                        {data: 'balance', name: 'balance'}
                    ]
                });

            }

            if($("#report_type").val() == '2') {

                $("#datatable-invoice").dataTable().fnDestroy();

                var oTable = $('#datatable-invoice').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        'url': 'https://finance.pi.team/api/displayreport',
                        'type': 'POST',
                        'headers': {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        },
                        'data': {
                            'cmd': 'display',
                            'type': '2', //customer
                            'from_date': $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD'),
                            'to_date': $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD')
                        }
                    },
                    columns: [
                        {data: 'name', name: 'name'},
                        {data: 'email', name: 'email'},
                        {data: 'open_id', name: 'invoice_id'},
                        {data: 'post_date', name: 'invoice_date'},
                        {data: 'status', name: 'status'},
                        {data: 'total_amount_local', name: 'total_amount'},
                        {data: 'amount_paid_local', name: 'amount_paid'},
                        {data: 'balance_local', name: 'balance'},
                        {data: 'payment_date', name: 'payment_date'}
                    ]
                });

            }

            if($("#report_type").val() == '3') {

                $("#datatable-payment").dataTable().fnDestroy();

                var oTable = $('#datatable-payment').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        'url': 'https://finance.pi.team/api/displayreport',
                        'type': 'POST',
                        'headers': {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        },
                        'data': {
                            'cmd': 'display',
                            'type': '3', //customer
                            'from_date': $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD'),
                            'to_date': $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD')
                        }
                    },
                    columns: [
                        {data: 'name', name: 'name'},
                        {data: 'open_id', name: 'invoice_id'},
                        {data: 'post_date', name: 'invoice_date'},
                        {data: 'payment_date', name: 'payment_date'},
                        {data: 'total_amount_local', name: 'total_amount'},
                        {data: 'payment_amount', name: 'payment_amount'}
                    ]
                });

            }

            if($("#report_type").val() == '4') {

                $("#datatable-invent").dataTable().fnDestroy();

                var oTable = $('#datatable-invent').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        'url': 'https://finance.pi.team/api/displayreport',
                        'type': 'POST',
                        'headers': {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        },
                        'data': {
                            'cmd': 'display',
                            'type': '4', //customer
                            'from_date': $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD'),
                            'to_date': $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD')
                        }
                    },
                    columns: [
                        {data: 'name', name: 'name'},
                        {data: 'open_id', name: 'invoice_id'},
                        {data: 'post_date', name: 'invoice_date'},
                        {data: 'status', name: 'status'},
                        {data: 'total_amount_local', name: 'total_amount'},
                        {data: 'item_id', name: 'item_id'},
                        {data: 'item_name', name: 'item_name'},
                        {data: 'item_price', name: 'item_price'},
                        {data: 'item_qty', name: 'item_qty'},
                        {data: 'item_total', name: 'item_total'}
                    ]
                });

            }

            if($("#report_type").val() == '5') {

                $("#datatable-tax").dataTable().fnDestroy();

                var oTable = $('#datatable-tax').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        'url': 'https://finance.pi.team/api/displayreport',
                        'type': 'POST',
                        'headers': {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        },
                        'data': {
                            'cmd': 'display',
                            'type': '5', //customer
                            'from_date': $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD'),
                            'to_date': $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD')
                        }
                    },
                    columns: [
                        {data: 'name', name: 'name'},
                        {data: 'open_id', name: 'invoice_id'},
                        {data: 'post_date', name: 'invoice_date'},
                        {data: 'status', name: 'status'},
                        {data: 'total_amount_local', name: 'total_amount'},
                        {data: 'tax_name', name: 'tax_name'},
                        {data: 'tax_rate', name: 'tax_rate'},
                        {data: 'tax_amount', name: 'tax_amount'}
                    ]
                });

            }

            if($("#report_type").val() == '7') {

                $("#datatable-refund").dataTable().fnDestroy();

                var oTable = $('#datatable-refund').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        'url': 'https://finance.pi.team/api/displayreport',
                        'type': 'POST',
                        'headers': {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        },
                        'data': {
                            'cmd': 'display',
                            'type': '7', //customer
                            'from_date': $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD'),
                            'to_date': $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD')
                        }
                    },
                    columns: [
                        {data: 'name', name: 'name'},
                        {data: 'open_id', name: 'credit_id'},
                        {data: 'post_date', name: 'credit_date'},
                        {data: 'refund_date', name: 'refund_date'},
                        {data: 'total_amount_local', name: 'total_amount'},
                        {data: 'refund_amount', name: 'refund_amount'}
                    ]
                });

            }

        }


    </script>


@endsection

@section('after-footer')

    <script type="text/javascript">
        $(function () {

            var start = moment().subtract(30, 'days');
            var end = moment();

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);

        });
    </script>


@endsection