

@extends('spark::layouts.app')

@section('header')


    <link href="assets/plugins/bootstrap-table/css/bootstrap-table.min.css" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

    <!-- DataTables -->
    <!--
    <link href="/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    -->

@endsection

@section('content')

<report-api :user="user" inline-template>
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <h4 class="m-t-0 header-title"><b>Customer Report</b></h4>
                    <p class="text-muted font-13 m-b-30">
                        &nbsp;<button onclick="loadTable()">Click me</button>
                        <input type="date" class="form-control" id="from_date" name="from_date">
                        <input type="date" class="form-control" id="to_date" name="to_date">
                    </p>

                    <table id="datatable-contact" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                        <tr>
                            <th>name</th>
                            <th>email</th>
                            <th>total amount</th>
                            <th>amount paid</th>
                            <th>balance</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

    </div>

</report-api>

@endsection

@section('footer')

<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>


<!--
<script src="/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/assets/plugins/datatables/responsive.bootstrap.min.js"></script>
-->

<script>
    //jQuery(document).ready(function () {

    function loadTable() {



        $("#datatable-contact").dataTable().fnDestroy();
        //}

        var oTable = $('#datatable-contact').DataTable({
            processing: true,
            serverSide: true,
            //"bDestroy": true
            ajax: {
                'url': 'https://finance.pi.team/api/displayreport',
                'type': 'POST',
                'headers': {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                'data' : {
                    'cmd' : "refresh",
                    'from_date': $("#from_date").val(),
                    'to_date'  : $("#to_date").val()
                }
            },
            /*
             data: function (d) {
             d.name = $('input[name=name]').val();
             d.operator = $('select[name=operator]').val();
             d.user_invoices = $('input[name=user_invoices]').val();
             },
             */

            columns: [

                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'total_amount', name: 'total_amount'},
                {data: 'amount_paid', name: 'amount_paid'},
                {data: 'balance', name: 'balance'}
            ]
        });
    }
    //});
</script>

@endsection

@section('after-footer')

    <script type="text/javascript">



    </script>

@endsection