<form class="form-horizontal" role="form">

    <div class="form-group">
        <label class="col-md-4 control-label">API Username</label>

        <div class="col-md-6">
            <input type="text" class="form-control" id="paypal_api_username"
                   name="paypal_api_username" v-model="paypal_api_username">

        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">API Password</label>

        <div class="col-md-6">
            <input type="text" class="form-control" id="paypal_api_password"
                   name="paypal_api_password" v-model="paypal_api_password">

        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Signature</label>

        <div class="col-md-6">
            <input type="text" class="form-control" id="paypal_signature"
                   name="paypal_signature" v-model="paypal_signature">

        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">Test Mode</label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-default">
                <input id="paypal_test_mode" name="paypal_test_mode"
                       type="checkbox" v-model="paypal_test_mode">
                <label for="paypal_test_mode"> Enable </label>
            </div>
        </div>
    </div>

    <!-- Update Button -->
    <div class="form-group">
        <div class="col-md-offset-4 col-md-6">
            <button type="submit" v-on:click.prevent="updateGateway(2)" class="btn btn-primary">
                Update
            </button>
        </div>
    </div>
</form>