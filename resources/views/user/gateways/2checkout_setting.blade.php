<form class="form-horizontal" role="form">

    <div class="form-group">
        <label class="col-md-4 control-label">Account Number</label>

        <div class="col-md-6">
            <input type="text" class="form-control" id="twocheck_acc_number"
                   name="twocheck_acc_number" v-model="twocheck.acc_number">

        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Secret Word</label>

        <div class="col-md-6">
            <input type="text" class="form-control" id="twocheck_secret_word"
                   name="twocheck_secret_word" v-model="twocheck.secret_word">

        </div>
    </div>

    <div class="form-group" v-if="twocheck.man_bill_addr">
        <label class="col-sm-4 control-label">Billing Address</label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-default">
                <input id="twocheck_man_bill_addr" name="twocheck_man_bill_addr" v-model="twocheck.man_bill_addr" type="checkbox">
                <label for="twocheck_man_bill_addr"> Mandatory billing address </label>
            </div>
            <div class="checkbox checkbox-default">
                <input id="twocheck_update_bill_addr" name="twocheck_update_bill_addr" v-model="twocheck.update_bill_addr" type="checkbox">
                <label for="twocheck_update_bill_addr"> Update contact's billing address if different </label>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">Test Mode</label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-default">
                <input id="twocheck_test_mode" name="twocheck_test_mode"
                       v-model="twocheck.test_mode" type="checkbox">
                <label for="2check_test_mode"> Enable </label>
            </div>
        </div>
    </div>

    <!-- Update Button -->
    <div class="form-group">
        <div class="col-md-offset-4 col-md-6">
            <button type="submit" v-on:click.prevent="updateGateway(5)" class="btn btn-primary">
                Update
            </button>
        </div>
    </div>
</form>