<form class="form-horizontal" role="form">

    <div class="form-group">
        <label class="col-md-4 control-label">Secret Key</label>

        <div class="col-md-6">
            <input type="text" class="form-control" id="razor_secret_key"
                   name="razor_secret_key" v-model="razor.secret_key"
                   v-validate="'required'" data-vv-scope="razor_form">

            <i v-show="errors.has('razor_secret_key', 'razor_form')" class="fa fa-warning text-danger"></i>
            <span v-show="errors.has('razor_secret_key', 'razor_form')"
                  class="help text-danger">Razor secret key is required.</span>

        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Public Key</label>

        <div class="col-md-6">
            <input type="text" class="form-control" id="razor_pub_key"
                   name="razor_pub_key" v-model="razor.pub_key"
                   v-validate="'required'" data-vv-scope="razor_form">

            <i v-show="errors.has('razor_pub_key', 'razor_form')" class="fa fa-warning text-danger"></i>
            <span v-show="errors.has('razor_pub_key', 'razor_form')"
                  class="help text-danger">Razor public key is required.</span>

        </div>
    </div>

    <div class="form-group" v-if="razor.man_bill_addr">
        <label class="col-sm-4 control-label">Billing Address</label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-default">
                <input id="razor_man_bill_addr" name="razor_man_bill_addr" v-model="razor.man_bill_addr" type="checkbox">
                <label for="razor_man_bill_addr"> Mandatory billing address </label>
            </div>
            <div class="checkbox checkbox-default">
                <input id="razor_update_bill_addr" name="razor_update_bill_addr" v-model="razor.update_bill_addr" type="checkbox">
                <label for="razor_update_bill_addr"> Update contact's billing address if different </label>
            </div>
        </div>
    </div>

    <div class="form-group" v-if="razor.test_mode">
        <label class="col-sm-4 control-label">Test Mode</label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-default">
                <input id="razor_test_mode" name="razor_test_mode"
                       v-model="razor.test_mode" type="checkbox">
                <label for="razor_test_mode"> Enable </label>
            </div>
        </div>
    </div>

    <!-- Update Button -->
    <div class="form-group">
        <div class="col-md-offset-4 col-md-6">
            <button v-if="!razor.status" type="submit" v-on:click.prevent="updateGateway(3)" class="btn btn-primary">
                Activate
            </button>
            <button v-if="razor.status" type="submit" v-on:click.prevent="updateGateway(3)" class="btn btn-primary">
                Update
            </button>
            <button v-if="razor.status" type="submit" v-on:click.prevent="removeGateway(3)" class="btn btn-danger">
                Delete
            </button>
        </div>
    </div>
</form>