@extends('spark::layouts.app')

@section('scripts')

@endsection

@section('content')
    <gateway-api :user="user" :teams="teams" inline-template>
        <div class="spark-screen container">
            <div class="row">
                <!-- Tabs -->
                <div class="col-md-4">
                    <div class="panel panel-color panel-primary">
                        <div class="panel-heading">
                            Payment Gateways
                        </div>

                        <div class="panel-body">
                            <div class="spark-settings-tabs">
                                <ul class="nav spark-settings-stacked-tabs" role="tablist">
                                    <!-- Profile Link -->
                                    <li v-bind:class="{ active: stripe.status }" role="presentation">
                                        <a href="#stripe" aria-controls="profile" role="tab" data-toggle="tab" v-on:click="showGateway(1)">
                                            <i class="fa fa-fw fa-cc-stripe fa-btn"></i>Stripe
                                        </a>
                                    </li>

                                    <li v-bind:class="{ active: razor.status }" role="presentation">
                                        <a href="#razorpay" aria-controls="api" role="tab" data-toggle="tab" v-on:click="showGateway(3)">
                                            <i class="fa fa-fw fa-btn fa-inr"></i>RazorPay
                                        </a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>


                </div>

                <!-- Tab Panels -->
                <div class="col-md-8">
                    <div class="tab-content">
                        <!-- Profile -->
                        <div role="tabpanel" class="tab-pane" v-bind:class="{ active: stripe.status }" id="stripe">

                            <div class="panel panel-default">
                                <div class="panel-heading">Stripe</div>

                                <div class="panel-body">

                                    @include('user.gateways.stripe_setting')

                                </div>
                            </div>


                        </div>

                        <div role="tabpanel" class="tab-pane" v-bind:class="{ active: razor.status }" id="razorpay">

                            <div class="panel panel-default">
                                <div class="panel-heading">RazorPay</div>

                                <div class="panel-body">

                                    @include('user.gateways.razorpay_setting')

                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </gateway-api>
@endsection
