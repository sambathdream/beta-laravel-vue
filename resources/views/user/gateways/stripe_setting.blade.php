<form class="form-horizontal" role="form">

    <div class="form-group">
        <label class="col-md-4 control-label">Secret Key</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="stripe_secret_key" id="stripe_secret_key" v-model="stripe.secret_key"
               v-validate="'required'" data-vv-scope="stripe_form">

            <i v-show="errors.has('stripe_secret_key', 'stripe_form')" class="fa fa-warning text-danger"></i>
            <span v-show="errors.has('stripe_secret_key', 'stripe_form')"
                  class="help text-danger">Stripe secret key is required.</span>

        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Publishable Key</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="stripe_pub_key" id="stripe_pub_key" v-model="stripe.pub_key"
                   v-validate="'required'" data-vv-scope="stripe_form">

            <i v-show="errors.has('stripe_pub_key', 'stripe_form')" class="fa fa-warning text-danger"></i>
            <span v-show="errors.has('stripe_pub_key', 'stripe_form')"
                  class="help text-danger">Stripe secret key is required.</span>

        </div>
    </div>

    <div class="form-group" v-if="stripe.man_bill_addr">
        <label class="col-md-4 control-label">Webhook URL</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="stripe_webhook" id="stripe.webhook" value="{{ $enc_team_id }}">
            <input type="hidden" v-model="stripe.webhook">
            <strong>You must add this URL as an <a target="_blank" href="https://dashboard.stripe.com/account/webhooks">endpoint at Stripe.</a></strong>
        </div>
    </div>

    <div class="form-group" v-if="stripe.man_bill_addr">
        <label class="col-sm-4 control-label">Billing Address</label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-default">
                <input id="stripe_man_bill_addr" name="stripe_man_bill_addr" v-model="stripe.man_bill_addr" type="checkbox">
                <label for="stripe_man_bill_addr"> Mandatory billing address </label>
            </div>
            <div class="checkbox checkbox-default">
                <input id="stripe_update_bill_addr" name="stripe_update_bill_addr" v-model="stripe.update_bill_addr" type="checkbox">
                <label for="stripe_update_bill_addr"> Update contact's billing address if different </label>
            </div>
        </div>
    </div>

    <!-- Update Button -->
    <div class="form-group">
        <div class="col-md-offset-4 col-md-6">
            <button v-if="!stripe.status" type="submit" v-on:click.prevent="updateGateway(1)" class="btn btn-primary">
                Activate
            </button>
            <button v-if="stripe.status" type="submit" v-on:click.prevent="updateGateway(1)" class="btn btn-primary">
                Update
            </button>
            <button v-if="stripe.status" type="submit" v-on:click.prevent="removeGateway(1)" class="btn btn-danger">
                Delete
            </button>
        </div>
    </div>
</form>