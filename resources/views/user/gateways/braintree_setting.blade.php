<form class="form-horizontal" role="form">

    <div class="form-group">
        <label class="col-md-4 control-label">Merchant ID</label>

        <div class="col-md-6">
            <input type="text" class="form-control" id="braintree_merchant_id"
                   name="braintree_merchant_id" v-model="braintree_merchant_id">

        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Private Key</label>

        <div class="col-md-6">
            <input type="text" class="form-control" id="braintree_pvt_key"
                   name="braintree_pvt_key" v-model="braintree_pvt_key">

        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Public Key</label>

        <div class="col-md-6">
            <input type="text" class="form-control" id="braintree_pub_key"
                   name="braintree_pub_key" v-model="braintree_pub_key">

        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">Test Mode</label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-default">
                <input id="braintree_test_mode" v-model="braintree_test_mode"
                       name="braintree_test_mode" type="checkbox">
                <label for="braintree_test_mode"> Enable </label>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">Billing Address</label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-default">
                <input id="braintree_man_bill_addr" name="braintree_man_bill_addr"
                       v-model="braintree_man_bill_addr" type="checkbox">
                <label for="braintree_man_bill_addr"> Mandatory billing address </label>
            </div>
            <div class="checkbox checkbox-default">
                <input id="braintree_update_bill_addr" name="braintree_update_bill_addr"
                       v-model="braintree_update_bill_addr" type="checkbox">
                <label for="braintree_update_bill_addr"> Update contact's billing address if different </label>
            </div>
        </div>
    </div>

    <!-- Update Button -->
    <div class="form-group">
        <div class="col-md-offset-4 col-md-6">
            <button type="submit" v-on:click.prevent="updateGateway(4)" class="btn btn-primary">
                Update
            </button>
        </div>
    </div>
</form>