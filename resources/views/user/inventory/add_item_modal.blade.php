<div id="add-product-modal" name="add-product-modal" class="modal fade" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content p-0">

            <form enctype="multipart/form-data" id="item_form">

                <ul class="nav nav-tabs navtab-bg nav-justified">
                    <li class="active">
                        <a href="#item-tab-1" data-toggle="tab" aria-expanded="true">
                            <span class="visible-xs"><i class="fa fa-home">Item Properties</i></span>
                            <span class="hidden-xs">Item Properties</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="#item-tab-2" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs"><i class="fa fa-user">Extras</i></span>
                            <span class="hidden-xs">Extras</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="item-tab-1">

                        <h4>Basic</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" name="item_id" class="form-control"
                                           id="item_id"
                                           placeholder="Item ID/Code (Displayed on the invoice/quote)" v-model="new_item.item_id"
                                           v-validate="'required'" data-vv-scope="item_form">
                                    <i v-show="errors.has('item_id', 'item_form')" class="fa fa-warning text-danger"></i>
                                    <span v-show="errors.has('item_id', 'item_form')" class="help text-danger">Item Code field is required.</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" id="name"
                                           placeholder="Item Name (Displayed on the invoice/quote)" v-model="new_item.name"
                                           v-validate="'required'" data-vv-scope="item_form">
                                    <i v-show="errors.has('name', 'item_form')" class="fa fa-warning text-danger"></i>
                                    <span v-show="errors.has('name', 'item_form')" class="help text-danger">@{{ errors.first('name', 'item_form') }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" name="new_item.description" class="form-control"
                                           id="new_item.description"
                                           placeholder="Item Description (Optional - not displayed anywhere)" v-model="new_item.description">
                                </div>
                            </div>
                        </div>

                        <h4>Quantity</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" name="qty" class="form-control" id="qty"
                                           placeholder="Item Quantity" v-model="new_item.qty"
                                           v-validate data-vv-rules="decimal:2" data-vv-scope="item_form">
                                    <i v-show="errors.has('qty', 'item_form')" class="fa fa-warning text-danger"></i>
                                    <span v-show="errors.has('qty', 'item_form')" class="help text-danger">This field must be numeric and may contain 2 decimal points.</span>
                                </div>
                            </div>
                        </div>

                        <h4>Price</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5 class="m-t-0">Buy Price</h5>
                                    <input type="text" name="buy_price" id="buy_price" class="form-control"
                                           placeholder="Buy Price" v-model="new_item.buy_price"
                                           v-validate data-vv-rules="decimal:2" data-vv-scope="item_form">

                                    <i v-show="errors.has('buy_price', 'item_form')" class="fa fa-warning text-danger"></i>
                                    <span v-show="errors.has('buy_price', 'item_form')" class="help text-danger">This field must be numeric and may contain 2 decimal points.</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5 class="m-t-0">Sell Price</h5>
                                    <input type="text" name="sell_price" id="sell_price" class="form-control"
                                           placeholder="Sell Price" v-model="new_item.sell_price" data-vv-rules="decimal:2"
                                           v-validate data-vv-scope="item_form">

                                    <i v-show="errors.has('sell_price', 'item_form')" class="fa fa-warning text-danger"></i>
                                    <span v-show="errors.has('sell_price', 'item_form')" class="help text-danger">This field must be numeric and may contain 2 decimal points.</span>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="tab-pane" id="item-tab-2">

                        <h4>Category & Type</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select class="form-control" name="category" id="category"
                                            v-model="new_item.category">
                                        @foreach (App\Item_Category::all() as $item_cat)
                                            <option value="{{ $item_cat->id }}">{{ $item_cat->text }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">

                                    <select class="form-control" name="type" id="type"
                                            v-model="new_item.type">
                                        @foreach (App\Item_Type::all() as $item_type)
                                            <option value="{{ $item_type->id }}">{{ $item_type->text }}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                        </div>

                        <h4>HSN & SAC</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <hsntypeahead style="bottom: 12px;"></hsntypeahead>
                                </div>
                            </div>
                        </div>

                        <div class="row" v-show="this.gst_hsnsac.type">
                            <div class="col-md-12">
                                <div class="panel panel-primary panel-border">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">@{{ this.gst_hsnsac.type }}: @{{  this.gst_hsnsac.category }}</h3>
                                    </div>
                                    <div class="panel-body" style="padding-top: 0px;">
                                        <p class="text-muted m-t-0 m-b-0 font-15">@{{ this.gst_hsnsac.sub_category }}</p>
                                        <p class="text-muted m-t-0 m-b-0 font-15">@{{ this.gst_hsnsac.service }}</p>
                                        <p><strong>@{{ this.gst_hsnsac.type }} Code: @{{ this.gst_hsnsac.code }}</strong></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if(\Auth::user()->currentTeam->id == 1)
                            <div class="row" v-show="this.gst_hsnsac.type">
                                <div class="col-md-12">
                                    <multiselecttaxinv
                                            v-model="new_item.tax_items"
                                            :options="tax_rates"
                                            :multiple="true"
                                            :close-on-select="true"
                                            :clear-on-select="false"
                                            :hide-selected="true"
                                            placeholder="Taxes"
                                            label="name"
                                            track-by="name"
                                    @close="selectedTaxItems">

                                    </multiselecttaxinv>
                                </div>
                            </div>
                        @endif

                        <h4>Accounting</h4>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Debit Account <small>(posted immediately)</small></label>
                                    <select class="form-control" name="debit_gl" id="debit_gl"
                                            v-model="new_item.debit_gl" placeholder="Debit GL">

                                        @foreach ($gl_accounts as $gl_account)
                                            <option value="{{ $gl_account->open_id }}">{{ $gl_account->name }}</option>
                                        @endforeach

                                    </select>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Credit Account <small>(posted in invoicing)</small></label>
                                    <select class="form-control" name="credit_gl"
                                            id="credit_gl" v-model="new_item.credit_gl" placeholder="Credit GL">
                                        @foreach ($gl_accounts as $gl_account)
                                            <option value="{{ $gl_account->open_id }}" data-subtext="{{ $gl_account->category }}">{{ $gl_account->name }}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal-footer">

                    <div class="row">
                        <div class="col-md-12" style="padding-right: 5%;">
                            <div class="form-group no-margin">
                                <button type="button" class="btn btn-white waves-effect"
                                        data-dismiss="modal">Close
                                </button>

                                <a v-if="new_item.id" href="#" class="btn btn-primary waves-effect submit-new-edit"
                                   @click.prevent="updateItem">Update Changes
                                </a>
                                <a v-else href="#" class="btn btn-primary waves-effect submit-new-edit"
                                   @click.prevent="saveNewItem">Save Changes
                                </a>

                            </div>
                        </div>
                    </div>

                </div>

            </form>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->