<form role="form" class="form-horizontal">
    <div class="form-group">
        <label class="col-md-4 control-label">Address</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" v-model="company_details.billing_address">
            <span class="help-block" style="display: none;"></span></div>
    </div>
    <div class="form-group"><label class="col-md-4 control-label">Address Line
            2</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" v-model="company_details.billing_address_line_2">
            <span class="help-block" style="display: none;"></span></div>
    </div>
    <div class="form-group"><label class="col-md-4 control-label">City</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" v-model="company_details.billing_city">
            <span class="help-block" style="display: none;"></span></div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label">State &amp; ZIP / Postal Code</label>
        <div class="col-sm-3">
            <input type="text" placeholder="State" class="form-control" v-model="company_details.billing_state">
            <span class="help-block" style="display: none;"></span></div>
        <div class="col-sm-3">
            <input type="text" placeholder="Postal Code" class="form-control" v-model="company_details.billing_zip">
            <span class="help-block" style="display: none;"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label">Country</label>
        <div class="col-sm-6">
            <select class="form-control" v-model="company_details.billing_country">
                @foreach (app(Laravel\Spark\Repositories\Geography\CountryRepository::class)->all() as $key => $country)
                    <option value="{{ $key }}">{{ $country }}</option>
                @endforeach
            </select>
            <span class="help-block" style="display: none;">
        </span>
        </div>
    </div>

</form>