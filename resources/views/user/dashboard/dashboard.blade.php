@extends('spark::layouts.app')

@section('header')

<script>

    var first_time = {{ $first_time }};

</script>

<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1770171759665747'); // Insert your pixel ID here.
    fbq('track', 'PageView');
</script>
<noscript>
    <img height="1" width="1" style="display:none"
         src="https://www.facebook.com/tr?id=1770171759665747&ev=PageView&noscript=1"/>
</noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<script>
    fbq('track', 'CompleteRegistration', {
        value: 5.00,
        currency: 'USD'
    });
</script>
    
@endsection

@section('content')
    <dashboard-api :user="user" inline-template>
        <div class="container">
            <!-- Contact related code starts -->
            <!--
            <div class="row">
                <div class="col-sm-12">
                    <div class="btn-group pull-right m-t-15">
                        <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light"
                                data-toggle="dropdown" aria-expanded="false">Settings <span class="m-l-5"><i
                                        class="fa fa-cog"></i></span></button>
                        <ul class="dropdown-menu drop-menu-right" role="menu">
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>

                    <h4 class="page-title">Dashboard</h4>
                    <p class="text-muted page-title-alt">Welcome to Ubold admin panel !</p>
                </div>
            </div>
            -->

            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <div class="widget-bg-color-icon card-box fadeInDown animated">
                        <div class="bg-icon bg-icon-info pull-left">
                            <i class="md md-attach-money text-info"></i>
                        </div>
                        <div class="text-right">
                            <h3 class="text-dark">{{ $inv_country->currency_symbol }} <b class="counter">{{ number_format($total_revenue, 2, '.', ',') }}</b></h3>
                            <p class="text-muted">Total Revenue</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="widget-bg-color-icon card-box">
                        <div class="bg-icon bg-icon-success pull-left">
                            <i class="md md-remove-red-eye text-success"></i>
                        </div>
                        <div class="text-right">
                            <h3 class="text-dark">{{ $inv_country->currency_symbol }} <b class="counter">{{ number_format($total_receivable->int_balance, 2, '.', ',') }}</b></h3>
                            <p class="text-muted">Receivable</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="widget-bg-color-icon card-box">
                        <div class="bg-icon bg-icon-pink pull-left">
                            <i class="md md-add-shopping-cart text-pink"></i>
                        </div>
                        <div class="text-right">
                            <h3 class="text-dark"><b class="counter">{{ $total_sales }}</b></h3>
                            <p class="text-muted">Sales Today</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="widget-bg-color-icon card-box">
                        <div class="bg-icon bg-icon-purple pull-left">
                            <i class="md md-account-balance-wallet text-purple"></i>
                        </div>
                        <div class="text-right">
                            <h3 class="text-dark"><b class="counter">{{ $total_payments }}</b></h3>
                            <p class="text-muted">Payments Today</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-lg-4">
                    <div class="card-box">
                        <!--
                        <h4 class="text-dark header-title m-t-0 m-b-30">Total Revenue</h4>
                        -->
                        <div class="widget-chart text-center">

                            {!! $chart3->render() !!}

                                <ul class="list-inline m-t-15">
                                    <li>
                                        <h5 class="text-muted m-t-20">Target</h5>
                                        <h4 class="m-b-0">{{ $inv_country->currency_symbol }}{{ $target_this_week }}</h4>
                                    </li>
                                    <li>
                                        <h5 class="text-muted m-t-20">Last week</h5>
                                        <h4 class="m-b-0">{{ $inv_country->currency_symbol }}{{ $sales_last_week }}</h4>
                                    </li>
                                    <li>
                                        <h5 class="text-muted m-t-20">Last Month</h5>
                                        <h4 class="m-b-0">{{ $inv_country->currency_symbol }}{{ $sales_last_month }}</h4>
                                    </li>
                                </ul>

                        </div>
                    </div>

                </div>

                <div class="col-lg-8">
                    <div class="card-box">
                        <h4 class="text-dark header-title m-t-0">Sales Analytics</h4>
                        <div class="text-center">
                            {!! $chart1->render() !!}
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-lg-6">
                    <div class="card-box">
                        <h4 class="text-dark header-title m-t-0">Total Sales</h4>

                        <div class="text-center">
                            {!! $chart2->render() !!}
                        </div>


                    </div>

                </div>

                <!-- col -->

                <div class="col-lg-6">
                    
                    <div class="row">
                    <div class="card-box" id="card-box-tour">
                        <a href="/invoices" class="pull-right btn btn-default btn-sm waves-effect waves-light">View All</a>
                        <h4 class="text-dark header-title m-t-0">Recent Invoices</h4>
                        <p class="text-muted m-b-30 font-13">
                            List of all recent invoices.

                        </p>

                        <div class="table-responsive">
                            <table class="table table-actions-bar m-b-0">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Amount</th>
                                    <th style="min-width: 80px;">Manage</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($recent_invoices as $invoice)
                                    <tr>
                                        <td>{{ $invoice->open_id }}</td>
                                        <td>{{ $invoice->contactTab->name }}</td>
                                        <td>{{ $invoice->total_amount }}</td>
                                        <td>
                                            <a href="/invoices/{{ $invoice->open_id }}" class="table-action-btn"><i class="ti ti-eye"></i></a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>

                    </div>
                    </div>
                </div>
                <!-- end col -->

            </div>
            <!-- end row -->


            @if($first_time)
            <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Default Initial Configurations</h4>
                        </div>
                        <div class="modal-body">

                            @include('user.dashboard.default_config')

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" @click.prevent="updateConfig(2)"
                                    id="init-config-tour">
                                <span>Next</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal -->


            <div id="con-addr-modal" class="modal fade" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Default Address For Invoicing</h4>
                        </div>
                        <div class="modal-body">

                            <div class="alert alert-danger">
                                This address will be printed on the invoice.
                            </div>
                            @include('user.dashboard.address_config')

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" @click.prevent="updateConfig(5)"
                                    id="init-config-tour">
                                <span>Save</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal -->
            @endif


            <!-- ends nik -  related code ends here -->
        </div>

    </dashboard-api>

@endsection

@section('footer')

    <script src="assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
    <script src="assets/plugins/counterup/jquery.counterup.min.js"></script>

    {!! Charts::assets(['highcharts']) !!}

    {!! Charts::assets(['justgage']) !!}

    {!! Charts::assets(['progressbarjs']) !!}
@endsection

@section('after-footer')

    @if($first_time)
    <script>

        $('#con-close-modal').modal({
            show: 'true'
        });

    </script>

    <script type="text/javascript">
        var capterra_vkey = '02e8f14a5db8b64b06f668eed5a64f45',
            capterra_vid = '2112421',
            capterra_prefix = (('https:' == document.location.protocol) ? 'https://ct.capterra.com' : 'http://ct.capterra.com');

        (function() {
            var ct = document.createElement('script'); ct.type = 'text/javascript'; ct.async = true;
            ct.src = capterra_prefix + '/capterra_tracker.js?vid=' + capterra_vid + '&vkey=' + capterra_vkey;
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ct, s);
        })();
    </script>
    
    @endif

    <!-- Tour script -->
    @if(!$boot_tour->dashboard)

    <script>

        $(document).ready(function () {

            // Instance the tour
            var tour = new Tour({
                steps: [
                    {
                        element: '#navigation-button-tour',
                        title: "Navigation Menu Hide/Display",
                        content: "Click on the button and make sidebar navigation small or big.",
                        placement: "bottom",
                        backdrop: false
                    },
                    {
                        element: '#shortcut-button-tour',
                        title: "Shortcuts",
                        content: "You can access your settings as well as create invoice, quote or view payments.",
                        placement: "bottom",
                        backdrop: false
                    },
                    {
                        element: "#contacts-menu-tour",
                        title: "Create Customer",
                        content: "Click on the Contacts to create or manage your customers."
                    },
                    {
                        element: "#sales-menu-tour",
                        title: "Create Invoice",
                        content: "Click on the Sales to create your invoice or view existing invoices. You can also view all payments posted so far."
                    },
                    {
                        element: "#inv-menu-tour",
                        title: "Create Products/Service/Asset",
                        content: "Click on the 'Products & Service' to create new products, services or assets or view existing inventory."

                    }
                ],

                smartPlacement: true,

                onEnd: function (tour) {
                    Bus.$emit('closeTour', 1); //0 = dashboard

                },

            });

            // Initialize the tour
            tour.init();

            @if($first_time)
            Bus.$on('startTour',  () => {

                // Start the tour
                console.log('capture');
                tour.start();

            });
            @else
                tour.start();
            @endif

        });


    </script>
    @endif

    <!-- End of Tour script -->

@endsection
