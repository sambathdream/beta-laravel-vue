<form role="form" class="form-horizontal">

    <div class="form-group">
        <label class="col-md-4 control-label">Currency</label>

        <div class="col-md-6">
            <select class="form-control" data-live-search="true" name="currency_id" id="currency_id"
                    v-model="localization.currency_id">
                @foreach ($countries as $country)
                    <option value="{{ $country->id }}">{{ $country->name }} / {{ $country->currency_code }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Language</label>

        <div class="col-md-6">
            <select class="form-control" data-live-search="true" name="language_id" id="language_id"
                    v-model="localization.language_id">
                <option value="840">English / United States</option>
            </select>
        </div>
    </div>


    <div class="form-group">
        <label class="col-md-4 control-label">TimeZone</label>

        <div class="col-md-6">

            {!! Timezone::selectForm('', null, array('class' => 'form-control', 'id' => 'timezone', 'name' => 'timezone', 'v-model' => 'localization.timezone', 'data-live-search' => 'true')) !!}

        </div>
    </div>


    <div class="form-group">
        <label class="col-md-4 control-label">Date Format</label>

        <div class="col-md-6">
            <select class="form-control" data-live-search="true"
                    id="date_time_format" name="date_time_format" v-model="localization.date_time_format">
                @foreach(App\DateTimeList::all() as $datetime)
                    <option value="{{ $datetime->id }}">{{ $datetime->name }}</option>
                @endforeach
            </select>

        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">First Day of the Week</label>

        <div class="col-md-6">

            <select class="form-control" data-live-search="true"
                    id="week_first_day" name="week_first_day" v-model="localization.week_first_day">
                @foreach(StaticArray::$week_days as $key => $day)
                    <option value="{{ $key }}">{{ $day }}</option>
                @endforeach
            </select>

        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">First Month of the Financial Year</label>

        <div class="col-md-6">
            <select class="form-control" data-live-search="true"
                    id="year_first_month" name="year_first_month" v-model="localization.year_first_month">
                @foreach(StaticArray::$year_months as $key => $month)
                    <option value="{{ $key }}">{{ $month }}</option>
                @endforeach
            </select>
        </div>
    </div>

</form>