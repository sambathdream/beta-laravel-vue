<div id="add-txn-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Create Transaction</h4>
            </div>
            <div class="modal-body">

                <h4>Basic</h4>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">

                            <h5 class="m-t-0">Category</h5>
                            <select class="form-control selectpicker show-tick" data-live-search="true" name="category" id="category"
                                    v-model="new_txn.category">

                                @foreach (App\Txn_Category::get() as $category)
                                    <option value="{{ $category->id }}">{{ $category->text }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <h5 class="m-t-0">Type</h5>
                            <select class="form-control selectpicker show-tick" data-live-search="true" name="type" id="type"
                                    v-model="new_txn.type">
                                @foreach (App\Txn_Type::get() as $txn_status)
                                    <option value="{{ $txn_status->type }}">{{ $txn_status->text }}</option>
                                @endforeach
                                    <option value="B">Both</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">

                            <datepicker v-model="new_txn.txn_date" input-class="form-control" monday-first></datepicker>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" name="new_txn.description" class="form-control"
                                   id="new_txn.description"
                                   placeholder="Description" v-model="new_txn.description">
                        </div>
                    </div>
                </div>

                <h4>Price</h4>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" name="new_txn.txn_amount" class="form-control"
                                   id="new_txn.txn_amount"
                                   placeholder="Transaction Amount" v-model="new_txn.txn_amount">
                        </div>
                    </div>
                </div>

                <h4>GL Account</h4>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group" v-show="new_txn.type != 'C'">
                            <h5 class="m-t-0">Debit</h5>
                            <select class="form-control selectpicker show-tick" data-live-search="true" name="debit_gl" id="debit_gl"
                                    v-model="new_txn.debit_gl">
                                <option value="">None Selected</option>
                                @foreach ($gl_accounts as $gl_account)
                                    <option value="{{ $gl_account->open_id }}">{{ $gl_account->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" v-show="new_txn.type != 'D'">
                            <h5 class="m-t-0">Credit</h5>
                            <select class="form-control selectpicker show-tick" data-live-search="true" name="credit_gl" id="credit_gl"
                                    v-model="new_txn.credit_gl">
                                <option value="">None Selected</option>
                                @foreach ($gl_accounts as $gl_account)
                                    <option value="{{ $gl_account->open_id }}">{{ $gl_account->name }}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group no-margin">

                            <button type="button" class="btn btn-white waves-effect"
                                    data-dismiss="modal">Close
                            </button>

                            <a href="#" data-dismiss="modal"
                               class="btn btn-primary waves-effect submit-new-edit"
                               v-on:click.prevent="saveNewItem">Save Changes
                            </a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- /.modal -->