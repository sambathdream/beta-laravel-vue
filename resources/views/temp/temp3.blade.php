@extends('spark::layouts.app')

@section('header')

    <!-- Plugin Css-->

    <link href="/assets/plugins/bootstrap-table/css/bootstrap-table.min.css" rel="stylesheet" type="text/css"/>

    <link href="/assets/plugins/summernote/summernote.css" rel="stylesheet"/>

    <link href="/js/awesomplete/awesomplete.css" rel="stylesheet"/>
    <script src="/js/awesomplete/awesomplete.js" async></script>

@endsection

@section('content')

    <select id="customer_select" name="customer_select" class="form-control select2" >
        <option value="3620194" selected="selected">select2/select2</option>
    </select>

    <input type="text" name="username" id="username">
    <div id="resultarea"></div>

@endsection

@section('footer')

    <!--form validation init-->
    <script src="/assets/plugins/summernote/summernote.min.js"></script>

    <script>

        jQuery(document).ready(function () {

            $.ajaxPrefilter(function (options, originalOptions, xhr) {
                var token =
                'eyJpdiI6IkxhYUhuT2NoN3VKbkhtNDJnWVNWYmc9PSIsInZhbHVlIjoiYWxzbTRcL25neGVkNld3RkR5a2NLZmc9PSIsIm1hYyI6ImFiMDczMzIxYmIxZTFkMzQ0NGJjNDkxNzk1NDdlMzI1OWU2MDU5ZTczOWZmODhiNWY0ZDkzODlhMzhiMThjYmIifQ==';
                    //'{{ $encrypted_csrf_token }}';
                // $('meta[name="csrf_token"]').attr('content');

                if (token) {
                    return xhr.setRequestHeader('X-XSRF-TOKEN', token);
                }
            });


            //var myusername = $("#username").val();
            $.ajax({
                type: "GET",
                url: "/api/contacts/1",
                //data: myusername,
                cache: false,
                success: function(data){
                    $("#resultarea").text(data);
                }
            });

            //console.log("init select2");
            var $ajax = $(".customer_select");
            //console.log($ajax);
            $ajax.select2({
                ajax: {
                    url: "/api/contacts/list",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        //console.log("select2 processing results");
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                },
                minimumInputLength: 1,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });

            $('.summernote').summernote({
                height: 350,                 // set editor height
                minHeight: null,             // set minimum height of editor
                maxHeight: null,             // set maximum height of editor
                focus: false                 // set focus to editable area after initializing summernote
            });

            $('.inline-editor').summernote({
                airMode: true
            });
        });
    </script>

    <script>
        function formatRepo(repo) {
            if (repo.loading) return repo.name;

            var markup = "<div class='select2-result-repository clearfix'>";
            //console.log("cp 1");
            if (repo.image) {
                markup += "<div class='col-md-2 select2-result-repository__avatar'><img width='30' src='" + repo.image + "' /></div>";
            }
            else {
                markup += "<div class='col-md-2 select2-result-repository__avatar'><img width='30' src='/custom/images/nobody.jpg' /></div>";
            }
            //console.log("cp 2");
            markup += "<div class='col-md-10'><div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'>" + repo.name + "</div>";

            if (repo.email) {
                markup += "<div class='select2-result-repository__description'>" + repo.email + "</div>";
            }
            markup += "</div>";

            return markup;
        }

        function formatRepoSelection(repo) {

            //
            document.getElementById('customer_email').value = repo.email;

            document.getElementById('bill_to_addr').value = repo.name + ', '
                + repo.bill_address1 + ', ' + repo.bill_address2 + ', '
                + repo.bill_city + ', ' + repo.bill_state + ', '
                + repo.bill_country_id;

            document.getElementById('ship_to_addr').value = repo.ship_contact + ', '
                + repo.ship_address1 + ', ' + repo.ship_address2 + ', '
                + repo.ship_city + ', ' + repo.ship_state + ', '
                + repo.ship_country_id;

            return repo.name;
        }

    </script>

@endsection


@section('after-footer')




@endsection
