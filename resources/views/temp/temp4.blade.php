<!doctype html>
<html lang="en">
<head>
    <style>
        <?php include(public_path() . '/assets/css/bootstrap.min.css');?>
    </style>

    <meta charset="UTF-8">
    <title>Invoice</title>
    <style>
        .wrapper {
            position: relative;
        }

        .wrapper .header {
            height: 200px;
        }



        table.outline-table {
            border: 1px solid;
            border-spacing: 0;
        }
        .borderless td, .borderless th {
            border: hidden;
        }

        table thead {
            background:grey;
        }

        .table thead th,.table thead td{
            color:#fff !important;
        }


        tr.border-bottom td, td.border-bottom {
            border-bottom: 1px solid;
        }
        tr.border-top td, td.border-top {
            border-top: 1px solid;
        }
        tr.border-right td, td.border-right {
            border-right: 1px solid;
        }
        tr.border-right td:last-child {
            border-right: 0px;
        }
        tr.center td, td.center {
            text-align: center;
            vertical-align: text-top;
        }
        td.pad-left {
            padding-left: 5px;
        }
        tr.right-center td, td.right-center {
            text-align: right;
            padding-right: 50px;
        }
        tr.right td, td.right {
            text-align: right;
        }
        .logo {
            background-image:url("img/color-logo.png");
            background-repeat:no-repeat;
            width:700px;
            height:342px;
            position:absolute;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <div class="col-md-12">
        <div class="row">

            <div class="header col-md-12">
                <div class="pull-left col-md-7 col-xs-7">
                    <img width="200" height="45" src="{{ $image_uri }}" alt="">
                </div>
                <div class="pull-right col-md-5 col-xs-5"><h4 class="page-title">Invoice #</h4></div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <hr>
            <div class="col-md-12">

                <div class="row">

                    <div class="col-xs-4">
                        <strong>Bill To</strong>
                        <address>
                            {{ $invoice->bill_to_addr }}<br>
                            <abbr title="Phone">P:</abbr> (123) 456-7890 {{ $invoice->phone }}
                            <br><abbr title="Email">@:</abbr> {{ $invoice->customer_email }}
                        </address>
                    </div>
                    <div class="column col-xs-4">
                        <strong>Ship To</strong>
                        <address>
                            {{ $invoice->ship_to_addr }}<br>
                            <abbr title="Phone">P:</abbr> (123) 456-7890 {{ $invoice->ship_phone }}
                        </address>
                    </div>
                    <div class="column col-md-4">
                        <p><strong>Invoice
                                Date:</strong><br>
                            {{ (Carbon\Carbon::parse($invoice->invoice_date))->formatLocalized('%B %d, %Y') }}
                        </p>

                        <p><strong>Due
                                Date:</strong> <br>{{ (Carbon\Carbon::parse($invoice->due_date))->formatLocalized('%B %d, %Y') }}
                        </p>

                        <p class="m-t-10"><strong>Invoice Status:</strong> <span
                                    class="label label-pink"> </span></p>

                        <p class="m-t-10"><strong>PO/SO Number:</strong> {{ $invoice->poso_number }}</p>
                    </div>
                    <div class="clearfix" style="clear: both"></div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table m-t-0">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($invoiceitems as $item)
                            <tr>
                                <td>{{ $item->item_id }}</td>
                                <td>{{ $item->item_name }}</td>
                                <td>{{ $item->item_price }}</td>
                                <td>{{ $item->item_qty }}</td>
                                <td>{{ $item->item_total }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="3"></td>
                            <td><b>Sub-total:</b></td>
                            <td> {{ $invoice->sub_total }}</td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                            <td><b>Discout:</b></td>
                            <td> {{ $invoice->sub_total }}</td>
                        </tr>
                        <tr>
                            <td class="borderless" colspan="3"></td>
                            <td><b>{{ $invoice->tax_name1 }}</b></td>
                            <td> {{ $invoice->tax_rate1 }}
                                %: {{ $invoice->tax_amount }}</td>
                        </tr>
                        <tr class="borderless">
                            <td class="borderless" colspan="3"></td>
                            <td><b>Amount Due (USD):</b></td>
                            <td> {{ $invoice->total_amount }}</td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>
</div>

</body>
</html>