const TestProjectStatusMap = {
  "new": 1,
  "accepted": 2,
  "submitted": 3,
  "under review": 4,
  "test passed": 5,
  "test failed": 5
};

export function testProjectStatusWeight(status){
  if(!status){
    return false;
  }
  if(status == "Submitted"){
    status = "under review";
  }
  let statusWeight = TestProjectStatusMap[status.toLowerCase()];
  return statusWeight;
}