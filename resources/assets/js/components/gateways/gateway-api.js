const gateApiUrl = base_api_path + "api/gateways"; //don't add '/' in the end

Vue.component('gateway-api', {
    props: ['user', 'teams'],

    data() {
        return {

            stripe: {
                secret_key: "",
                pub_key: "",
                webhook: "",
                man_bill_addr: false,
                update_bill_addr: false,
                test_mode: false,
                status: false
            },

            razor: {
                secret_key: "",
                pub_key: "",
                man_bill_addr: false,
                update_bill_addr: false,
                test_mode: false,
                status: false
            },

            twocheck: {
                acc_number: "",
                secret_word: "",
                man_bill_addr: false,
                update_bill_addr: false,
                test_mode: false
            },

            paypal_api_username: "",
            paypal_api_password: "",
            paypal_signature: "",
            paypal_test_mode:false,

            braintree_merchant_id: "",
            braintree_pvt_key: "",
            braintree_pub_key: "",
            braintree_test_mode: false,
            braintree_man_bill_addr: false,
            braintree_update_bill_addr: false,

            gateway_id: '',
            gate_data: '',

            gate_data_stripe: '',
            gate_data_paypal: '',
            gate_data_razorpay: '',
            gate_data_brain_tree: '',
            gate_data_2check: '',

            user_gateways: []

        }
    },

    methods: {

        showGateway (gateway_id) {

            this.$http.get(gateApiUrl + '/' + gateway_id).then((response) => {

                console.log(response.data);

                var gateway_id      = response.data.gateway_id;
                var gateway_string  = response.data.gate_data.split('#');
                var flag = false;

                switch(gateway_id) {

                    case 1:

                        this.stripe.secret_key       = gateway_string[0];
                        this.stripe.pub_key          = gateway_string[1];
                        this.stripe.webhook          = gateway_string[2];

                        if(gateway_string[3] === "true"){ flag = true } else { flag = false }
                        this.stripe.man_bill_addr    = flag;

                        if(gateway_string[4] === "true"){ flag = true } else { flag = false }
                        this.stripe.update_bill_addr = flag;

                        this.stripe.status           = response.data.status;

                        break;

                    case 2:

                        this.paypal_api_username = gateway_string[0];
                        this.paypal_api_password = gateway_string[1];
                        this.paypal_signature = gateway_string[2];
                        this.paypal_test_mode = gateway_string[3];

                        break;

                    case 3:

                        this.razor.secret_key = gateway_string[0];
                        this.razor.pub_key = gateway_string[1];

                        if(gateway_string[2] === "true"){ flag = true } else { flag = false }
                        this.razor.man_bill_addr = flag;

                        if(gateway_string[3] === "true"){ flag = true } else { flag = false }
                        this.razor.update_bill_addr = flag;

                        if(gateway_string[4] === "true"){ flag = true } else { flag = false }
                        this.razor.test_mode = flag;

                        this.razor.status           = response.data.status;

                        break;

                    case 4:

                        this.braintree_merchant_id   = gateway_string[0];
                        this.braintree_pvt_key       = gateway_string[1];
                        this.braintree_pub_key       = gateway_string[2];
                        this.braintree_test_mode     = gateway_string[3];
                        this.braintree_man_bill_addr = gateway_string[4];
                        this.braintree_update_bill_addr = gateway_string[5];

                        break;

                    case 5:

                        this.twocheck.acc_number = gateway_string[0];
                        this.twocheck.secret_word = gateway_string[1];

                        if(gateway_string[2] === "true"){ flag = true } else { flag = false }
                        this.twocheck.man_bill_addr = flag;

                        if(gateway_string[3] === "true"){ flag = true } else { flag = false }
                        this.twocheck.update_bill_addr = flag;

                        if(gateway_string[4] === "true"){ flag = true } else { flag = false }
                        this.twocheck.test_mode = flag;

                        break;
                }


            });
        },

        callGatewayUpdateApi (body,id) {

            console.log(body);

            this.$http.put(gateApiUrl + '/' + id, body).then((response) => {

                //console.log(response.data);

            }, (errorResponse) => {

                console.error("Error Posting to the server " + errorResponse);

            })

        },


        updateGateway(id) {

            let postBody;

            switch(id) {

                case 1:

                    this.$validator.validateAll('stripe_form').then(success => {

                        if (!success) {
                            // handle error
                            return;

                        } else {

                            postBody = {
                                gateway_id: id,  //stripe
                                gate_data: this.gate_data_stripe,
                                status: this.stripe.status = true
                            }

                            this.razor.status = false;
                            this.callGatewayUpdateApi(postBody,id);
                        }
                    });

                break;

                case 2:
                    postBody = {
                        gateway_id: id,  //paypal
                        gate_data: this.gate_data_paypal
                    }
                break;

                case 3:

                    this.$validator.validateAll('razor_form').then(success => {

                        if (!success) {
                            // handle error
                            return;

                        } else {

                            postBody = {
                                gateway_id: id,  //razor
                                gate_data: this.gate_data_razorpay,
                                status: this.razor.status = true
                            }

                            this.stripe.status = false;
                            this.callGatewayUpdateApi(postBody,id);

                        }
                    });

                break;

                case 4:
                    postBody = {
                        gateway_id: id,  //braintree
                        gate_data: this.gate_data_brain_tree
                    }
                break;

                case 5:
                    postBody = {
                        gateway_id: id,  //2check
                        gate_data: this.gate_data_2check
                    }
                break;

            }

        },
        removeGateway (id) {

            //console.log(body);

            this.$http.delete(gateApiUrl + '/' + id).then((response) => {

                console.log(response.data);

                switch(id) {
                    case 1:

                        this.stripe.secret_key = "";
                        this.stripe.pub_key = "";
                        this.stripe.webhook = "";
                        this.stripe.man_bill_addr = false;
                        this.stripe.update_bill_addr = false;
                        this.stripe.test_mode = false;
                        this.stripe.status = false;

                        break;

                    case 3:

                        this.razor.secret_key = "";
                        this.razor.pub_key = "";
                        this.razor.man_bill_addr = false;
                        this.razor.update_bill_addr = false;
                        this.razor.test_mode = false;
                        this.razor.status = false;

                        break;
                }

            }, (errorResponse) => {

                console.error("Error Posting to the server " + errorResponse);

            })

        },

    },

    computed: {

        gate_data_stripe: function () {
            return ( this.stripe.secret_key + "#" +
                this.stripe.pub_key + "#" +
                this.stripe.webhook + "#" +
                this.stripe.man_bill_addr + "#" +
                this.stripe.update_bill_addr );
        },

        gate_data_paypal: function () {
            return ( this.paypal_api_username + "#" +
            this.paypal_api_password + "#" +
            this.paypal_signature + "#" +
            this.paypal_test_mode);

        },

        gate_data_razorpay: function () {
            return ( this.razor.secret_key + "#" +
                    this.razor.pub_key + "#" +
                    this.razor.man_bill_addr + "#" +
                    this.razor.update_bill_addr + "#" +
                    this.razor.test_mode);

        },

        gate_data_brain_tree: function () {
            return ( this.braintree_merchant_id + "#" +
            this.braintree_pvt_key + "#" +
            this.braintree_pub_key + "#" +
            this.braintree_test_mode + "#" +
            this.braintree_man_bill_addr + "#" +
            this.braintree_update_bill_addr);

        },

        gate_data_2check: function () {
            return ( this.twocheck.acc_number + "#" +
                    this.twocheck.secret_word + "#" +
                    this.twocheck.man_bill_addr + "#" +
                    this.twocheck.update_bill_addr + "#" +
                    this.twocheck.test_mode);

        }
    },

    mounted() {
        //console.log(Spark.state.currentTeam.name);
        this.showGateway(1);    //stripe
        this.showGateway(3);    //razor
    }
});
