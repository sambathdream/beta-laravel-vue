import Datepicker from 'vuejs-datepicker';

const apiStripeBillConvert  = base_api_path + "api/invoices/stripebillconvert"; //don't add '/' in the end
const apiConvertToInvoice   = base_api_path + "api/quotations/convert";

Vue.component('invoice-api', {
    props: ['user', 'team'],
    components: {
        Datepicker
    },
    data() {
        return {

            new_txn: {
                category: 47, //sales
                type: '', //D: debit C: credit
                txn_date: '',//moment(date, 'YYYY-MM-DD').format('MM/DD/YYYY'),
                txn_amount: 0,
                txn_amount_local: 0,
                description: 'Invoice',
                debit_gl: '',
                credit_gl: ''
            },

            new_payment: {
                //contact_id: 0,
                open_id: 0,
                invoice_key: 0,
                amount: 0,
                amount_local: 0,
                payment_type_id: 0,
                payment_date: moment().format('YYYY-MM-DD'),
                transaction_reference: ''
            },

            new_email: {
                from_email: '',
                to_email1: '',
                to_email2: '',
                cc_email1: '',

                to_emails: [{
                    id: ''
                }],
                cc_emails: [{
                    id: ''
                }],

                subject: 'Invoice',
                message: '',
                copy_myself: false,
                attach_pdf: true
            },

            currencyConvert: {
                contact_country: '',
                company_country: '',
                currency_id: 840,
                company_currency_id: 356,
                rate: 0
            },

            convert_invoice: {
                invoice_key: '',
                invoice_title: '',
                invoice_date: moment().format('YYYY-MM-DD'),
                due_date: moment().add(15, 'days').format('YYYY-MM-DD'),

                flag: false
            }

        }
    },
    mounted: function () {

        //this.new_payment.amount = parseFloat(js_amount).toFixed(2);

    },

    created () {

        Bus.$on('postPayment',  (data) => {

            //console.log(data);

            var postPaymentApiUrl = base_api_path + "public/invoices/" + data.invoice_id + "/payment";

            this.$http.post(postPaymentApiUrl, data).then((response) => {

                //console.log(response);

                if(response.data === 'false') {
                    sweetAlert({
                        title: 'Payment Failed',
                        type: 'error',
                        html: 'Payment failed. Please try again later or contact vendor.',
                        showCloseButton: true
                    });
                }
                else{

                    sweetAlert({
                        title: 'Payment Posted',
                        type: 'success',
                        html: 'Payment <b># ' + response.data.balance + '</b> successfully posted. ',
                        showCloseButton: true
                    });

                    window.location.reload();
                    //href = base_api_path + "public/invoices/" + data.invoice_id;

                }

            }, (errorResponse) => {
                console.error("Error Posting to the server " + errorResponse);
            });

        });
    },

    methods: {

        stripeBillToInvoice: function (invoice) {

            //console.log(invoice);

            this.$http.post(apiStripeBillConvert + '/' + invoice.open_id, invoice.open_id).then((response) => {

                //console.log(response);

                sweetAlert({
                    title: 'Invoice Created',
                    type: 'success',
                    html: 'All open bills were successfully converted to invoice. Balance updated. ',
                    showCloseButton: true
                });

                window.location.href = base_api_path + "invoices/" + invoice.open_id;

            }, (errorResponse) => {
                console.error("Error Posting to the server " + errorResponse);
            })

        },

        showPayModal: function (invoice,invoice_key) {

            //console.log(invoice);
            this.new_payment.open_id         = invoice.open_id;
            this.new_payment.amount          = invoice.balance;
            //this.new_payment.contact_id      = invoice.contact_id;
            this.new_payment.invoice_key     = invoice_key;

            this.currencyConvert.currency_id         = invoice.currency_id;
            this.currencyConvert.company_currency_id = invoice.currency_id_local;
            this.currencyConvert.rate                = invoice.currency_rate;

            $('#add-pay-modal').modal('show');
        },

        postPaymentFun: function () {

            //console.log(this.new_payment);

            this.new_payment.payment_date = moment(this.new_payment.payment_date).format('YYYY-MM-DD');

            this.new_payment.amount_local = this.new_payment.amount * this.currencyConvert.rate;

            var apiPaymentsUrl = base_api_path + "api/payments";

            this.$http.post(apiPaymentsUrl, this.new_payment).then((response) => {

                //this.contacts.push(postBody);
                //this.postPayTxn();
                //console.log(response);
                sweetAlert({
                    title: 'Payment Posted',
                    type: 'success',
                    html: 'Payment successfully posted. Invoice balance updated. ',
                    showCloseButton: true
                });

                $('#add-pay-modal').modal('hide');

                window.location.href = base_api_path + "invoices/" + this.new_payment.open_id;

            }, (errorResponse) => {
                console.error("Error Posting to the server " + errorResponse);
            })

        },

        postPayTxn: function () {

            //this.convertCurrency();

            this.new_txn.category           = 47;
            this.new_txn.txn_date           = this.new_payment.payment_date;
            this.new_txn.txn_amount         = this.new_payment.amount;
            this.new_txn.txn_amount_local   = this.new_payment.amount_local;
            //this.currencyConvert.rate * this.new_payment.amount;

            this.new_txn.description    = "Invoice";
            //this.new_txn.debit_gl       = paymentdb; //js variable value by laravel
            //this.new_txn.credit_gl      = invoice_debit_gl;

            this.$http.post(base_api_path + "public/invoices/" + this.new_payment.invoice_key + "/transaction", this.new_txn).then((response) => {


            }, (errorResponse) => {
                console.error("Error Posting to the server " + errorResponse);
            });

        },


        addTo: function () {
            this.new_email.to_emails.push({ id: '' });
        },

        addCc: function () {
            this.new_email.cc_emails.push({ id: '' });
        },

        sendInvoice: function (from_email,to_email, emails, company_name, customer_name, open_id, inv_subject) {

            //console.log(quoteNumber);

            this.new_email.from_email       = from_email;
            this.new_email.to_email1        = to_email;

            if(to_email) {
                this.new_email.to_emails[0].id = to_email;
            }
            //this.new_email.to_emails        = emails;
            if(emails) {
                emails = JSON.parse(emails);
            }

            if(emails[0]) {
                this.new_email.to_emails[1] = emails[0];
            }
            if(emails[1]) {
                this.new_email.to_emails[2] = emails[1];
            }
            if(emails[2]) {
                this.new_email.to_emails[3] = emails[2];
            }
            if(emails[3]) {
                this.new_email.to_emails[4] = emails[3];
            }
            if(emails[4]) {
                this.new_email.to_emails[5] = emails[4];
            }

            if(inv_subject){
                this.new_email.subject = inv_subject;
            }
            else {
                this.new_email.subject = 'Invoice ' + '# ' + open_id + ' for ' + customer_name + ' from ' + company_name;
            }

            $('#send-email-modal').modal('show');

            //this.sendQuotationApi();

        },

        sendInvoiceApi: function (invoiceNumber) {

            var apiUrl = base_api_path + "api/invoices/" + invoiceNumber + "/send";

            this.$http.post(apiUrl,this.new_email).then((response) => {

                //console.log(response.data);

                $('#send-email-modal').modal('hide');

                sweetAlert({
                    title: 'Invoice Sent',
                    type: 'success',
                    html: 'Email/s successfully sent. ',
                    showCloseButton: true
                });
            }, (errorResponse) => {
                console.error("Error Posting to the server " + errorResponse);
            })

        },

        sendQuotation: function (from_email,to_email, emails, company_name, customer_name, open_id, inv_subject) {

            //console.log(quoteNumber);

            this.new_email.from_email       = from_email;
            this.new_email.to_email1        = to_email;

            if(to_email) {
                this.new_email.to_emails[0].id = to_email;
            }

            if(emails) {
                emails = JSON.parse(emails);
            }

            if(emails[0]) {
                this.new_email.to_emails[1] = emails[0];
            }
            if(emails[1]) {
                this.new_email.to_emails[2] = emails[1];
            }
            if(emails[2]) {
                this.new_email.to_emails[3] = emails[2];
            }
            if(emails[3]) {
                this.new_email.to_emails[4] = emails[3];
            }
            if(emails[4]) {
                this.new_email.to_emails[5] = emails[4];
            }

            if(inv_subject){
                this.new_email.subject = inv_subject;
            }
            else {
                this.new_email.subject = 'Quote ' + '# ' + open_id + ' for ' + customer_name + ' from ' + company_name;
            }

            $('#send-email-modal').modal('show');

            //this.sendQuotationApi();
        },

        sendQuotationApi: function (quoteNumber) {

            var apiUrl = base_api_path + "api/quotations/" + quoteNumber + "/send";

            this.$http.post(apiUrl,this.new_email).then((response) => {

                $('#send-email-modal').modal('hide');

                sweetAlert({
                    title: 'Quotation Sent',
                    type: 'success',
                    html: 'Email/s successfully sent. ',
                    showCloseButton: true
                });
            }, (errorResponse) => {
                console.error("Error Posting to the server " + errorResponse);
            })

        },

        sendCreditNote: function (from_email,to_email, emails, company_name, customer_name, open_id, inv_subject) {

            //console.log(quoteNumber);

            this.new_email.from_email       = from_email;
            this.new_email.to_email1        = to_email;

            if(to_email) {
                this.new_email.to_emails[0].id = to_email;
            }
            //this.new_email.to_emails        = emails;
            if(emails) {
                emails = JSON.parse(emails);
            }

            if(emails[0]) {
                this.new_email.to_emails[1] = emails[0];
            }
            if(emails[1]) {
                this.new_email.to_emails[2] = emails[1];
            }
            if(emails[2]) {
                this.new_email.to_emails[3] = emails[2];
            }
            if(emails[3]) {
                this.new_email.to_emails[4] = emails[3];
            }
            if(emails[4]) {
                this.new_email.to_emails[5] = emails[4];
            }

            if(inv_subject){
                this.new_email.subject = inv_subject;
            }
            else {
                this.new_email.subject = 'Credit Note ' + '# ' + open_id + ' for ' + customer_name + ' from ' + company_name;
            }

            $('#send-email-modal').modal('show');

            //this.sendQuotationApi();

        },

        sendCreditNoteApi: function (invoiceNumber) {

            var apiUrl = base_api_path + "api/creditnotes/" + invoiceNumber + "/send";

            this.$http.post(apiUrl,this.new_email).then((response) => {

                //console.log(response.data);

                $('#send-email-modal').modal('hide');

                sweetAlert({
                    title: 'Credit Note Sent',
                    type: 'success',
                    html: 'Email/s successfully sent. ',
                    showCloseButton: true
                });
            }, (errorResponse) => {
                console.error("Error Posting to the server " + errorResponse);
            })

        },

        displayConvertToInvoice: function () {

            //this.new_email.from_email = from_email;
            //this.new_email.to_email1 = to_email;
            this.convert_invoice.invoice_title = 'Invoice';

            $('#convert-to-invoice-modal').modal('show');

        },

        convertToInvoice: function (invoice_key) {

            this.convert_invoice.invoice_key = invoice_key;

            this.convert_invoice.invoice_date    = moment(this.convert_invoice.invoice_date).format('YYYY-MM-DD');
            this.convert_invoice.due_date        = moment(this.convert_invoice.due_date).format('YYYY-MM-DD');

            this.$http.post(apiConvertToInvoice,this.convert_invoice).then((response) => {

                console.log(response.data);

                $('#convert-to-invoice-modal').modal('hide');

                sweetAlert({
                    title: 'Invoice Converted',
                    type: 'success',
                    html:
                    'Converted successfully. ' +
                    '<a href="/invoices/' + response.data.open_id + '">Click Here To View Invoice</a>',
                    showCloseButton: true
                });


            }, (errorResponse) => {
                console.error("Error Posting to the server " + errorResponse);
            })

        }


        /*
         convertCurrency: function () {

         this.$http.post(paymentsApiUrl + '/convert', this.currencyConvert).then((response) => {

         //console.log(response.data);
         var rate = parseFloat(response.data.rate);
         this.currencyConvert.rate = rate;
         this.currencyConvert.contact_country = response.data.contact_country;
         this.currencyConvert.company_country = response.data.company_country;

         //this.currency_code = this.currencyConvert.contact_country;

         },
         (errorResponse) =>{
         console.error("Error Posting to the server " + errorResponse);
         });

         if(this.currencyConvert.contact_country != this.currencyConvert.company_country)
         {
         this.diff_curr = true;
         }
         else {
         this.diff_curr = false;
         }

         }
         */

    },
    computed: {


    }


});
