import Datepicker from 'vuejs-datepicker';
import Autocomplete from './vue-autocomplete.vue';
import objectAssign from 'object-assign';

import typeahead from './Typeahead.vue';
import Multiselect from 'vue-multiselect';
import multiselecttaxinv from 'vue-multiselect';

import hsntypeahead from '../inventory/Typeahead-Gst.vue';

const contactApiUrl = base_api_path + "api/contacts"; //don't add '/' in the end
const invoiceApiUrl = base_api_path + "api/invoices";
const recInvApiUrl = base_api_path + "api/invoices/recurring";

const quoteApiUrl = base_api_path + "api/quotations";
const inventoryApiUrl = base_api_path + "api/inventory"; //don't add '/' in the end
const configApiUrl = base_api_path + "api/config"; //don't add '/' in the end
const paymentsApiUrl = base_api_path + "api/payments";
const creditNoteApiUrl = base_api_path + "api/creditnotes";

const invoiceUrl = base_api_path + "/invoices";
const quoteUrl = base_api_path + "/quotations";
const creditNoteUrl = base_api_path + "/creditnotes";
const recInvUrl = base_api_path + "/invoices/recurring";

const tourApiUrl = base_api_path + "api/boottours"; //don't add '/' in the end
const inventoryListApiUrl = base_api_path + "api/inventory/list";

Vue.component('invoice-mod-gst-api', {
    props: ['user'],
    components: {
        Datepicker,
        Autocomplete,
        typeahead,
        Multiselect,
        multiselecttaxinv,
        hsntypeahead
    },
    data() {
        return {

            token: "",

            //contact related
            new_contact: {

                id: '',
                team_id: '',
                open_id: '',
                name: '',
                email: '',
                image: '',
                phone: '',
                contact1: '',
                contact2: '',
                currency_id: '840',
                payment_terms: '15',
                bill_address1: '',
                bill_address2: '',
                bill_city: '',
                bill_state: '',
                bill_postal_code: '',
                bill_country_id: 'US',
                ship_phone: '',
                ship_contact: '',
                ship_address1: '',
                ship_address2: '',
                ship_city: '',
                ship_state: '',
                ship_postal_code: '',
                ship_country_id: 'US',
                instructions: '',
                account_no: '',
                id_no: '',
                vat_no: '',
                fax_no: '',
                mobile_no: '',
                toll_free_no: '',
                website: '',
                balance: 0,
                created_by_id: '',
                modified_by_id: '',
                created_at: '',
                updated_at: '',
                deleted_at: '',


                emails: [{
                    id: ''
                }],
                copy_billing_addr: false,
                contact_image: '',
                gst_code: ''

            },

            //copy_bill_addr: true,

            //invoice related
            new_item: {
                item_id: '',
                name: '',
                description: '',
                qty: 1,
                buy_price: 0.00,
                sell_price: 0.00,
                category: '3',
                type: '2',
                image_url: '',
                debit_gl: '3',  //Inventory GL
                credit_gl: '3',  //Inventory GL

                hsnsac_code: null,
                hsnsac_type: null,
                cgst: 0,
                sgst: 0,
                igst: 0,
                gst: 0,

                gst_hsnsac_id: '',
                tax_items: [],
                ded_items: []

            },

            gst_hsnsac: {

                type: null,
                category: '',
                sub_category: '',
                service: '',
                code: '',
                description: '',
                cgst: 0,
                sgst: 0,
                igst: 0,
                gst: 0
            },

            invoice_from: {

                name: '',
                billing_address: '',
                billing_address_line_2: '',
                billing_city: '',
                billing_state: '',
                billing_zip: '',
                billing_country: '',
                phone: '',
                website: '',
                email: ''

            },

            invoice_form: {

                //item_options: [],

                id: '',
                team_id: '',
                open_id: '',
                old_open_id: '',
                contact_id: '',
                status_id: 1,
                // logo: '',
                // from: '',
                title: '',
                summary: '',
                // from_email: '',
                // customer_name: '',
                // customer_email: '',
                poso_number: '',
                post_date: '',
                due_date: '',
                // ship_to_addr: '',
                // bill_to_addr: '',
                phone: '',
                ship_phone: '',
                // is_recurring: 0,
                // recurring_id: '',
                // frequency_id: '',
                // start_date: '',
                // end_date: '',
                // last_sent_date: '',
                discount_type: 0,
                discount_rate1: 0,
                tax_name1: '',
                tax_rate1: 0,
                sub_total: 0,
                discount_amount: 0,
                tax_amount: 0,
                total_amount: 0,
                amount_paid: 0,
                balance: 0,
                total_amount_local: 0,
                amount_paid_local: 0,
                balance_local: 0,
                currency_id: 356,
                currency_id_local: 356,
                currency_rate: 1,
                debit_gl: '',
                credit_gl_cb: '',
                terms: '',
                notes: '',
                footer: '',
                //  created_by_id: 0,
                //  modified_by_id: 0,
                //  created_at: '',
                //  updated_at: '',
                //  deleted_at: '',
                //  manual_id: '',
                tax_amount_local: 0,
                //  stripe_id: '',
                ded_amount: 0,
                //  encrypted_id: '',

                contact_tab: {
                    id: '',
                    team_id: '',
                    open_id: '',
                    name: '',
                    email: '',
                    image: '',
                    phone: '',
                    contact1: '',
                    contact2: '',
                    currency_id: '840',
                    payment_terms: '15',
                    bill_address1: '',
                    bill_address2: '',
                    bill_city: '',
                    bill_state: '',
                    bill_postal_code: '',
                    bill_country_id: 'US',
                    ship_phone: '',
                    ship_contact: '',
                    ship_address1: '',
                    ship_address2: '',
                    ship_city: '',
                    ship_state: '',
                    ship_postal_code: '',
                    ship_country_id: 'US',
                    instructions: '',
                    account_no: '',
                    id_no: '',
                    vat_no: '',
                    fax_no: '',
                    mobile_no: '',
                    toll_free_no: '',
                    website: '',
                    balance: 0,
                    created_by_id: '',
                    modified_by_id: '',
                    created_at: '',
                    updated_at: '',
                    deleted_at: '',
                    emails: ''
                },
                status_tab: {
                    team_id: '',
                    id: '',
                    text: ''
                },
                inv_items_tab: [
                    {
                        id: '',
                        team_id: '',
                        invoice_num: '',
                        item_id: '',
                        item_name: '',
                        item_price: 0,
                        item_qty: 0,
                        item_total: 0,
                        item_taxable: 0,
                        credit_gl: '',

                        tax_items: [],
                        ded_items: [],
                        hsn_sac_tab: [],
                        hsnsac_code: '',
                        row_discount_total: 0,
                        row_tax_total: 0

                    }
                ],
                inv_tax_tab: [
                    {
                        id: '',
                        team_id: '',
                        invoice_num: '',
                        tax_open_id: '',
                        tax_id: '',
                        tax_name: '',
                        tax_rate: 0,
                        tax_amount: 0,
                        tax_gl: 0,
                        tax_display: 0,
                        recoverable: 0,
                        compound: 0,
                        type: '', //TAX
                        exclude_tax: false,
                        item_num: ''
                    }
                ],

                company_currency_id: 840,
                tax_items: [],
                ded_items: []
            },

            //tax_rate1: 0,
            discount_rate1: 0,
            sub_total: 0,
            tax_amount: 0,
            ded_amount: 0,
            ded_amount_inc_tax: 0,
            ded_amount_ex_tax: 0,
            discount_amount: 0,
            total_amount: 0,
            amount_paid: 0,
            balance: 0,

            currencyConvert: {
                contact_country: '',
                company_country: '',
                currency_id: 840,
                company_currency_id: 356,
                rate: 0
            },

            converted_amount: 0,
            currency_code: '',
            diff_curr: false,

            tax_items: [],
            ded_items: [],
            tax_rates: [], //data will be initially loaded here
            ded_rates: [], //deductible list

            tax_rate: {
                name: '',
                rate: 0,
                tax_id: '',
                gl_account_open_id: 7,
                gl_account_name: '',
                tax_display: false, //display tax number on the invoice
                recoverable: false, //default false
                compound: false, //default false
                exclude_tax: false //if true tax calculation will ignore this deductible
            },

            tax_modal_flag: true,
            ded_modal_flag: false,

            tour: {
                tour_status: false
            },

            company_details: {
                line_tax: true
            }

        }
    },

    methods: {

        addEmails: function () {

            if(this.new_contact.emails.length > 4){

                return;
            }
            this.new_contact.emails.push({ id: '' });
        },

        removeEmailLine: function (items, index) {

            this.new_contact.emails.splice(index, 1);

        },

        /* Contact Page Code Start */
        //display contact
        showContactModal() {

            this.new_contact.id = '';
            this.new_contact.name = '';
            this.new_contact.email = '';

            this.new_contact.phone = '';
            this.new_contact.contact1 = '';
            this.new_contact.contact2 = '';

            //this.currency_id: 840, //USD
            //this.payment_terms: 15, //Due in 15 Days

            this.new_contact.bill_address1 = '';
            this.new_contact.bill_address2 = '';
            this.new_contact.bill_city = '';
            this.new_contact.bill_state = '';
            this.new_contact.bill_postal_code = '';
            //this.bill_country_id: 840,

            this.new_contact.copy_billing_addr = false;
            this.new_contact.ship_phone = '';
            this.new_contact.ship_contact = '';
            this.new_contact.ship_address1 = '';
            this.new_contact.ship_address2 = '';
            this.new_contact.ship_city = '';
            this.new_contact.ship_state = '';
            this.new_contact.ship_postal_code = '';
            //this.ship_country_id: 840,
            this.new_contact.instructions = '';

            this.new_contact.account_no = '';
            this.new_contact.id_no = '';
            this.new_contact.vat_no = '';
            this.new_contact.fax_no = '';
            this.new_contact.mobile_no = '';
            this.new_contact.toll_free_no = '';
            this.new_contact.website = '';
            this.new_contact.contact_image = '';

            this.showConfig(1); //address or company details
            this.showConfig(2); //localization

            //this.convertCurrency();

            $('#add-contact-modal').modal('show');

        },

        contactFields() {

            if (this.new_contact.copy_billing_addr == true) {

                this.new_contact.ship_phone         = this.new_contact.phone;
                this.new_contact.ship_contact       = this.new_contact.name;
                this.new_contact.ship_address1      = this.new_contact.bill_address1;
                this.new_contact.ship_address2      = this.new_contact.bill_address2;
                this.new_contact.ship_city          = this.new_contact.bill_city;
                this.new_contact.ship_state         = this.new_contact.bill_state;
                this.new_contact.ship_postal_code   = this.new_contact.bill_postal_code;
                this.new_contact.ship_country_id    = this.new_contact.bill_country_id;

            }

        },

        addContact() {

            this.contactFields();

            this.$validator.validateAll('contact_form').then(success => {
                if (!success) {
                // handle error
                return;
            }
        else {
                this.$http.post(contactApiUrl, this.new_contact).then((response) => {

                    //this.new_contact.open_id = response.data;
                    //this.new_contact.id = response.data;
                    //console.log(response);

                    Bus.$emit('contact-refresh');
                Bus.$emit('contactSearchQ', response.data); //so that new contact info gets populated auto

            },(errorResponse) => {
                    console.error("Error Posting to the server " + errorResponse);
                });

                $('#add-contact-modal').modal('hide');

                sweetAlert({
                    type: 'success',
                    title: 'Contact Created',
                    text: 'I will close in 2 seconds.',
                    timer: 2000
                });
            }
        });
        },

        updateContact() {

            this.contactFields(); //call method to populate few not reactive fields
            this.$validator.validateAll().then(success => {

                if (!success) {
                // handle error
                return;
            }
        else {

                var id = this.new_contact.id;

                this.$http.put(contactApiUrl + '/' + id, this.new_contact).then((response) => {

                    //console.log(response);

                    Bus.$emit('contact-refresh');

                $('#add-contact-modal').modal('hide');

                sweetAlert({
                    type: 'success',
                    title: 'Contact Updated',
                    text: 'I will close in 2 seconds.',
                    timer: 2000
                });
            },(errorResponse) => {
                    console.error("Error Posting to the server " + errorResponse);
                });
            }

        });

        },

        /* Contact Page Code Ends */

        showConfig (config_id) {

            this.$http.get(configApiUrl + '/' + config_id).then((response) => {

                switch(config_id) {

                case 1:

                    if(response.data.address.billing_country) {
                        this.new_contact.bill_country_id = response.data.address.billing_country;
                        $('#bill_country_id').val(this.new_contact.bill_country_id);
                        $('#bill_country_id').change();

                        this.new_contact.ship_country_id = response.data.address.billing_country;
                        $('#ship_country_id').val(this.new_contact.ship_country_id);
                        $('#ship_country_id').change();

                        this.new_contact.currency_id = response.data.address.billing_country_id; //if no currency then
                        $('#currency_id').val(this.new_contact.currency_id);
                        $('#currency_id').change();
                        // default billing address country currency
                    }

                    this.invoice_from.name                      = response.data.address.name;
                    this.invoice_from.billing_address           = response.data.address.billing_address;
                    this.invoice_from.billing_address_line_2    = response.data.address.billing_address_line_2;
                    this.invoice_from.billing_city              = response.data.address.billing_city;
                    this.invoice_from.billing_state             = response.data.address.billing_state;
                    this.invoice_from.billing_zip               = response.data.address.billing_zip;
                    this.invoice_from.billing_country           = response.data.address.billing_country;

                    this.invoice_from.phone                 = response.data.settings.phone;
                    this.invoice_from.website               = response.data.settings.website;
                    this.invoice_from.email                 = response.data.settings.email;

                    this.invoice_form.notes                 = response.data.settings.inv_notes;
                    this.invoice_form.footer                = response.data.settings.inv_footer;

                    //this.invoice_form.from_email = response.data.user_email;//this.user.email; //
                    break;

                case 2:

                    if (response.data.settings.currency_id) {
                        this.new_contact.currency_id            = response.data.settings.currency_id;
                        $('#currency_id').val(this.new_contact.currency_id);
                        $('#currency_id').change();

                        this.invoice_form.currency_id           = response.data.settings.currency_id;
                        this.invoice_form.company_currency_id   = response.data.settings.currency_id;

                        $('#inv_currency_id').val(this.invoice_form.currency_id);
                        $('#inv_currency_id').change();

                        //this.currencyConvert.contact_country = response.data.currency_code;
                        //this.currencyConvert.company_country = response.data.currency_code;

                        this.convertCurrency();

                    }

                    break;

                case 3:

                    this.tax_rates = response.data;
                    //this.invoice_form.tax_items = this.tax_rates;

                    break;

                case 4:


                    break;

                case 5:


                    break;

                case 7:

                    this.ded_rates = response.data;

                    break;
                }


            });
        },

        createInvoiceEditContact: function() { //contact edit on the create invoice/quote page

            if(this.invoice_form.contact_id){

                Bus.$emit('contact-edit',this.invoice_form.contact_tab.open_id);

                this.invoice_form.contact_id = '';
                //this.invoice_form.customer_name = '';
                //this.invoice_form.customer_email = '';
                //this.invoice_form.bill_to_addr = '';
                //this.invoice_form.ship_to_addr = '';

            }

        },

        createInvoiceAddContact: function() { //contact edit on the create invoice/quote page

            this.invoice_form.contact_id = '';
            //this.invoice_form.customer_name = '';
            //this.invoice_form.customer_email = '';
            //this.invoice_form.bill_to_addr = '';
            //this.invoice_form.ship_to_addr = '';

            this.showContactModal();

        },


        /* Contact Page Code End */

        /* Invoice Page Code Start */
        initInvoiceForm() {

            this.showConfig(1);
            this.showConfig(2);
            this.showConfig(3);
            this.showConfig(7);

            this.getInvoice(cont_open_id);

        },

        addLine: function () {
            this.invoice_form.inv_items_tab.push({
                item_id: '',
                item_name: '',
                item_price: 0,
                item_qty: 0,
                item_total: 0,
                credit_gl: 0,

                tax_items: [],
                ded_items: [],
                hsn_sac_tab: [],
                hsnsac_code: '',
                row_discount_total: 0,
                row_tax_total: 0
            });
        },

        /*

         removeLine: function (items, index) {

         this.invoice_form.inv_items_tab.splice(index, 1);

         },

         */

        removeLine: function (index) {
            // console.log(index);
            if (this.invoice_form.inv_items_tab.length > 1) {
                this.invoice_form.inv_items_tab.splice(index, 1);
            }
        },

        saveInvoice: function () {

            if(this.invoice_form.old_open_id == this.invoice_form.open_id){

                this.invoice_form.status_id = 1;
                this.saveInvoiceApi();

            }
            else {

                var apiUrl = invoiceApiUrl + "/" + "previous/" + this.invoice_form.open_id;

                this.$http.get(apiUrl).then((response) => {

                    if (response.data) {

                    this.invoice_form.status_id = 1;
                    this.saveInvoiceApi();

                }
            else {
                    sweetAlert({
                        title: 'Duplicate Invoice ID!',
                        text: 'A statement with this number already exists. Please enter a different invoice number.',
                        timer: 5000
                    }).then(
                        function () {
                        },
                        // handling the promise rejection
                        function (dismiss) {
                            if (dismiss === 'timer') {
                                //console.log('I was closed by the timer')
                            }
                        }
                    )

                    return;

                }

            },
                (errorResponse) => {
                    console.error("Error Posting to the server " + errorResponse);

                    return false;

                });
            }

        },

        saveInvoiceApi: function () {

            var inv_crt_title = 'Invoice Created', inv_crt_html, reload_page_url;


            this.$validator.validateAll('invoice_form').then(success => {

                if (!success) {
                // handle error
                return;
            }
            else {

                this.invoice_form.tax_items       = this.tax_items;
                this.invoice_form.ded_items       = this.ded_items;

                //this.invoice_form.tax_rate1       = this.tax_rate1;
                this.invoice_form.discount_rate1  = this.discount_rate1;
                this.invoice_form.sub_total       = this.sub_total;
                this.invoice_form.tax_amount      = this.tax_amount;
                this.invoice_form.ded_amount      = this.ded_amount_inc_tax + this.ded_amount_ex_tax;//this.ded_amount;
                //this.invoice_form.discount_amount = this.ded_amount; //this.discount_amount;
                this.invoice_form.amount_paid     = this.amount_paid;
                this.invoice_form.balance         = this.balance;
                this.invoice_form.total_amount    = this.total_amount;

                this.invoice_form.currency_id     = this.currencyConvert.currency_id;
                this.invoice_form.currency_rate   = this.currencyConvert.rate;

                this.invoice_form.post_date       = moment(this.invoice_form.post_date).format('YYYY-MM-DD');
                this.invoice_form.due_date        = moment(this.invoice_form.due_date).format('YYYY-MM-DD');

                /*
                 */

                this.$http.put(invoiceApiUrl + '/' + this.invoice_form.open_id, this.invoice_form).then((response) => {

                    if(response.data == false){

                    inv_crt_title = 'Edit Failed';
                    inv_crt_html = 'Failed to delete old invoice, however updated invoice was created. ' +
                        'Please verify new invoice and delete old invoice manually. ' +
                        'If problem persists then connect with customer care!';

                    sweetAlert({
                        title: inv_crt_title,
                        type: 'error',
                        html: inv_crt_html,
                        showCloseButton: true
                    });
                }
            else {

                    inv_crt_html = 'Invoice <b># ' + response.data.open_id + '</b> Successfully Updated. ';

                    if (this.invoice_form.status_id === 5) {
                        inv_crt_title = 'Recurring Invoice Created';
                        inv_crt_html = 'Recurring Invoice Successfully Created. Redirecting!';
                    }

                    sweetAlert({
                        title: inv_crt_title,
                        type: 'success',
                        html: inv_crt_html,
                        showCloseButton: false
                    });

                    reload_page_url = invoiceUrl + '/' + response.data.open_id;

                    if (this.invoice_form.status_id === 5) {
                        reload_page_url = recInvUrl;
                    }
                    //recInvUrl
                    window.location.href = reload_page_url;

                }

            }, (errorResponse) => {
                    console.error("Error Posting to the server " + errorResponse);
                });



            }
        });

        },

        getInvoice: function (open_id) {

            var apiUrl = invoiceApiUrl + "/" + open_id;

            this.$http.get(apiUrl).then((response) => {

                if(response.data) {

                this.invoice_form   = response.data;
                this.new_contact    = this.invoice_form.contact_tab;
                //this.new_contact.emails    = [{ id: '' }];
                // this is needed because emails array is not
                //not loading properly from above invoice_form.contact_tab.emails statement

                this.invoice_form.old_open_id    = this.invoice_form.open_id;

                this.tax_items      = this.invoice_form.taxTab;
                this.ded_items      = this.invoice_form.dedTab;

                for (var i = 0, l = this.invoice_form.taxTab.length; i < l; i++) {

                    this.tax_items[i].gl_account_id = this.invoice_form.taxTab[i].tax_gl;
                    this.tax_items[i].name          = this.invoice_form.taxTab[i].tax_name;
                    this.tax_items[i].amount        = this.invoice_form.taxTab[i].tax_amount;
                    this.tax_items[i].compound      = this.invoice_form.taxTab[i].compound;
                    this.tax_items[i].recoverable   = this.invoice_form.taxTab[i].recoverable;
                    this.tax_items[i].tax_id        = this.invoice_form.taxTab[i].tax_id;
                    this.tax_items[i].rate          = this.invoice_form.taxTab[i].tax_rate;
                    this.tax_items[i].type          = this.invoice_form.taxTab[i].type;

                }

                for (var i = 0, l = this.invoice_form.dedTab.length; i < l; i++) {

                    this.ded_items[i].gl_account_id    = this.invoice_form.dedTab[i].tax_gl;
                    this.ded_items[i].name             = this.invoice_form.dedTab[i].tax_name;
                    this.ded_items[i].amount           = this.invoice_form.dedTab[i].tax_amount;
                    this.ded_items[i].compound         = this.invoice_form.dedTab[i].compound;
                    this.ded_items[i].recoverable      = this.invoice_form.dedTab[i].recoverable;
                    this.ded_items[i].tax_id   = this.invoice_form.dedTab[i].tax_id;
                    this.ded_items[i].rate     = this.invoice_form.dedTab[i].tax_rate;
                    this.ded_items[i].type     = this.invoice_form.dedTab[i].type;

                }

                for (var i = 0, l = this.invoice_form.inv_items_tab.length; i < l; i++) {

                    this.invoice_form.inv_items_tab[i].hsn_sac_tab = [];
                    this.invoice_form.inv_items_tab[i].hsnsac_code = '';
                    this.invoice_form.inv_items_tab[i].row_discount_total = 0;
                    this.invoice_form.inv_items_tab[i].row_tax_total = 0;

                    for (var k = 0, m = this.invoice_form.inv_items_tab[i].tax_items.length; k < m; k++) {
                        this.invoice_form.inv_items_tab[i].tax_items[k].gl_account_id = this.invoice_form.inv_items_tab[i].tax_items[k].tax_gl;
                        this.invoice_form.inv_items_tab[i].tax_items[k].name = this.invoice_form.inv_items_tab[i].tax_items[k].tax_name;
                        this.invoice_form.inv_items_tab[i].tax_items[k].amount = this.invoice_form.inv_items_tab[i].tax_items[k].tax_amount;
                        this.invoice_form.inv_items_tab[i].tax_items[k].compound = this.invoice_form.inv_items_tab[i].tax_items[k].compound;
                        this.invoice_form.inv_items_tab[i].tax_items[k].recoverable = this.invoice_form.inv_items_tab[i].tax_items[k].recoverable;
                        this.invoice_form.inv_items_tab[i].tax_items[k].tax_id = this.invoice_form.inv_items_tab[i].tax_items[k].tax_id;
                        this.invoice_form.inv_items_tab[i].tax_items[k].rate = this.invoice_form.inv_items_tab[i].tax_items[k].tax_rate;
                        this.invoice_form.inv_items_tab[i].tax_items[k].type = this.invoice_form.inv_items_tab[i].tax_items[k].type;

                        this.invoice_form.inv_items_tab[i].row_tax_total = parseFloat(this.invoice_form.inv_items_tab[i].row_tax_total) + parseFloat(this.invoice_form.inv_items_tab[i].tax_items[k].amount);;
                    }

                    for (var k = 0, m = this.invoice_form.inv_items_tab[i].ded_items.length; k < m; k++) {
                        this.invoice_form.inv_items_tab[i].ded_items[k].gl_account_id = this.invoice_form.inv_items_tab[i].ded_items[k].tax_gl;
                        this.invoice_form.inv_items_tab[i].ded_items[k].name = this.invoice_form.inv_items_tab[i].ded_items[k].tax_name;
                        this.invoice_form.inv_items_tab[i].ded_items[k].amount = this.invoice_form.inv_items_tab[i].ded_items[k].tax_amount;
                        this.invoice_form.inv_items_tab[i].ded_items[k].compound = this.invoice_form.inv_items_tab[i].ded_items[k].compound;
                        this.invoice_form.inv_items_tab[i].ded_items[k].recoverable = this.invoice_form.inv_items_tab[i].ded_items[k].recoverable;
                        this.invoice_form.inv_items_tab[i].ded_items[k].tax_id = this.invoice_form.inv_items_tab[i].ded_items[k].tax_id;
                        this.invoice_form.inv_items_tab[i].ded_items[k].rate = this.invoice_form.inv_items_tab[i].ded_items[k].tax_rate;
                        this.invoice_form.inv_items_tab[i].ded_items[k].type = this.invoice_form.inv_items_tab[i].ded_items[k].type;

                        this.invoice_form.inv_items_tab[i].row_discount_total = parseFloat(this.invoice_form.inv_items_tab[i].row_discount_total) + parseFloat(this.invoice_form.inv_items_tab[i].ded_items[k].amount);
                    }

                }

            }
            else {
                sweetAlert({
                    title: 'Failed!',
                    text: 'Failed.',
                    timer: 5000
                }).then(
                    function () {},
                    // handling the promise rejection
                    function (dismiss) {
                        if (dismiss === 'timer') {
                            //console.log('I was closed by the timer')
                        }
                    }
                )

                return;

            }

        },
            (errorResponse) =>{
                console.error("Error Posting to the server " + errorResponse);

                return false;

            })

        },

        /* Invoice Page Code End */

        /* Inventory Page Code Start */

        saveNewItem: function () {

            this.$validator.validateAll('item_form').then(success => {
                if (!success) {
                // handle error
                return;
            }
            else {

                this.$http.post(inventoryApiUrl, this.new_item).then((response) => {
                    //this.contacts.push(postBody);
                    //console.log(response);
                    //Bus.$emit('inventory-refresh');

                    $('#add-product-modal').modal('hide');

                sweetAlert({
                    type: 'success',
                    title: 'New Inventory Added',
                    text: 'I will close in 2 seconds.',
                    timer: 2000
                });

                //this.postTransaction();

                // Get Item for option item in invoice_form
                this.getItemData() //update inventory list

            }, (errorResponse) => {
                    console.error("Error Posting to the server " + errorResponse);
                })

            }
        });

        },

        /* Inventory Page Code End */

        convertCurrency: function () {

            //console.log(this.invoice_form.company_currency_id);
            //console.log(this.invoice_form.currency_id);

            this.currencyConvert.currency_id         = this.invoice_form.currency_id;
            this.currencyConvert.company_currency_id = this.invoice_form.company_currency_id;

            this.$http.post(paymentsApiUrl + '/convert', this.currencyConvert).then((response) => {

                //console.log(response.data);
                var rate = parseFloat(response.data.rate);
            this.currencyConvert.rate = rate;
            this.currencyConvert.contact_country = response.data.contact_country;
            this.currencyConvert.company_country = response.data.company_country;

            //this.currency_code = this.currencyConvert.contact_country;

        },
            (errorResponse) =>{
                console.error("Error Posting to the server " + errorResponse);
            });

            if(this.currencyConvert.contact_country != this.currencyConvert.company_country)
            {
                this.diff_curr = true;
            }
            else {
                this.diff_curr = false;
            }

        },

        /*
         On Autocomplete Input
         @params {Object} val
         @params {Number} index
         */
        onAutocompleteInput(val, index) {
            /*
             let newItems = JSON.parse(JSON.stringify(this.invoice_form.inv_items_tab))
             newItems[index] = objectAssign(newItems[index],{ name: val })

             // Put it in the state
             this.$set(this.invoice_form, 'inv_items_tab', newItems)
             */
        },


        /*
         On Autocomplete Filled
         @params {Object} val
         @params {Number} index
         */
        onAutocompleteSelect(val, index) {

            /*
             // Destructure the val
             const { id, open_id, item_id, name, sell_price, qty, total, credit_gl } = val

             // Copy it without its reactive as a new items
             let newItems = JSON.parse(JSON.stringify(this.invoice_form.inv_items_tab))
             newItems[index] = { id, open_id, item_id: item_id, name, price: sell_price, qty, credit_gl }

             // Put it in the state
             this.$set(this.invoice_form, 'inv_items_tab', newItems)

             */
        },

        /*
         Get The Options for Autocomplete
         */
        getItemData() {

            /*
             $.ajaxPrefilter((options, originalOptions, xhr) => {
             var token = this.spark.csrfToken
             if (token) {
             xhr.setRequestHeader('X-XSRF-TOKEN', this.token);
             return xhr.setRequestHeader('X-CSRF-TOKEN', token);
             }
             });

             $.get(inventoryListApiUrl, (res) => {
             this.$set(this.invoice_form, 'item_options', res)
             })

             */
        },

        callLocationSearch () {

            this.$http.get(base_api_path + 'api/searchlocation').then((response) => {

                //console.log(response.data);

                this.new_contact.currency_id         = response.data.country.id;
            //this.new_contact.bill_city           = response.data.location.city;
            //this.new_contact.bill_state          = response.data.location.state_name;
            //this.new_contact.bill_postal_code    = response.data.location.postal_code;
            this.new_contact.bill_country_id     = response.data.location.iso_code;

            //this.new_contact.ship_city           = response.data.location.city;
            //this.new_contact.ship_state          = response.data.location.state_name;
            //this.new_contact.ship_postal_code    = response.data.location.postal_code;
            this.new_contact.ship_country_id     = response.data.location.iso_code;


        }, (errorResponse) => {
                console.error("Error Posting to the server " + errorResponse);
            });

        },

        selectDiffCustomer () {

            this.invoice_form.contact_id = '';
            this.new_contact.id = '';
            this.new_contact.open_id = '';

        },

        selectedTaxItems: function () {
            // `this` inside methods points to the Vue instance
            //console.log(this.invoice_form.tax_items);
            //alert('test');

        },

        selectedDedItems: function () {
            // `this` inside methods points to the Vue instance
            //console.log(this.invoice_form.tax_items);
            //alert('test');

        },

        addTaxRate (config_id) {

            //console.log(this.tax_rate);

            this.$validator.validateAll('tax_rate').then(success => {

                if (!success) {
                // handle error
                return;
            } else {

                this.$http.put(configApiUrl + '/' + config_id, this.tax_rate).then((response) => {

                    //this.tax_rates.push(this.tax_rate);
                    //tax_rates: [];
                    this.showConfig(config_id);

                $('#update_tax_ded_modal').modal('hide');

                this.tax_rate.name = '';
                this.tax_rate.rate = 0;
                this.tax_rate.tax_id = '';
                //this.tax_rate.gl_account_open_id = '';
                //this.tax_rate.gl_account_name = '';
                this.tax_rate.tax_display = false;
                this.tax_rate.recoverable = false;
                this.tax_rate.compound = false;
                this.tax_rate.exclude_tax = false;

                sweetAlert({
                    type: 'success',
                    title: 'Rate Added Successfully.',
                    text: 'I will close in 1 seconds.',
                    timer: 1000
                });


            }, (errorResponse) => {
                    console.error("Error Posting to the server " + errorResponse);
                })

            }
        });

        },

        showTaxDedModal (type) {

            if(type == 2){
                this.tax_modal_flag = false;
                this.ded_modal_flag = true;

                this.tax_rate.gl_account_open_id = 8;

            }else{
                this.tax_modal_flag = true;
                this.ded_modal_flag = false;

                this.tax_rate.gl_account_open_id = 7;

            }

            $('#update_tax_ded_modal').modal('show');

        },

        groupBy(list, keyGetter) {
            const map = new Map();
            list.forEach((item) => {
                const key = keyGetter(item);
            const collection = map.get(key);
            if (!collection) {
                map.set(key, [item]);
            } else {
                collection.push(item);
            }
        });
            return map;
        }


    },

    computed: {

        sub_total: function () {

            return this.sub_total = this.invoice_form.inv_items_tab.reduce(function (carry, item) {
                var sub_total = carry + ((item.item_qty * item.item_price) - item.row_discount_total);
                return parseFloat(sub_total.toFixed(2));
            }, 0);

        },

        products_tax_total: function() {

            var products_tax = [];

            if (this.invoice_form.inv_items_tab) {
                this.invoice_form.inv_items_tab.forEach(function(item, index) {

                    item.row_tax_total = 0;

                    if (item.tax_items) {
                        item.tax_items.forEach(function (itm, idx) {

                            item.row_tax_total += parseFloat(itm.amount);
                            var rowAmount = (parseFloat(itm.rate) / 100) * parseFloat(item.item_total);

                            products_tax.push({
                                name: itm.name,
                                rate: itm.rate,
                                amount: rowAmount.toFixed(2)
                            });
                        });
                    }

                    item.row_tax_total = parseFloat(item.row_tax_total);

                });

                var groupedTax = this.groupBy(products_tax, taxitem => taxitem.name);
                var returnDataSet = [];
                var totalTax = 0;

                groupedTax.forEach(function(itemValue, itemKey) {
                    var total = 0;

                    itemValue.forEach(function(item, index) {
                        total += parseFloat(item.amount);
                    });
                    totalTax += total;

                    returnDataSet.push({
                        name: itemKey,
                        total_amount: total,
                        rate: itemValue[0].rate
                    });
                });

                //totalTax = parseFloat(totalTax);
                //totalTax = totalTax.toFixed(2);

                return [returnDataSet, totalTax];
            }

            return 0;
        },

        products_ded_total: function() {
            var products_ded = [];
            if (this.invoice_form.inv_items_tab) {
                this.invoice_form.inv_items_tab.forEach(function(item, index) {

                    item.row_discount_total = 0;

                    if (item.ded_items) {
                        item.ded_items.forEach(function (itm, idx) {

                            item.row_discount_total += parseFloat(itm.amount);

                            products_ded.push({
                                name: itm.name,
                                rate: itm.rate,
                                amount: parseFloat(itm.amount)
                            });
                        });
                    }

                    item.item_total = (parseFloat(item.item_qty) * parseFloat(item.item_price)) - parseFloat(item.row_discount_total);

                });

                var groupedDed = this.groupBy(products_ded, deditem => deditem.name);
                var returnDataSet = [];
                var totalDed = 0;

                groupedDed.forEach(function(itemValue, itemKey) {
                    var total = 0;

                    itemValue.forEach(function(item, index) {
                        total += parseFloat(item.amount);
                    });
                    totalDed += total;

                    returnDataSet.push({
                        name: itemKey,
                        total_amount: total,
                        rate: itemValue[0].rate
                    });
                });


                return [returnDataSet, totalDed];
            }

            return 0;
        },

        tax_amount: function () {

            var st = this.sub_total;
            var da = this.ded_amount_inc_tax;
            var fst = (parseFloat(st) - parseFloat(da));
            var temp = 0;

            var tax = this.tax_items.reduce(function(total, obj) {

                temp += (parseFloat(fst) * parseFloat(obj.rate) / 100);

                return parseFloat(temp.toFixed(2));

            }, 0);

            return this.tax_amount= tax;

        },

        ded_amount_inc_tax: function () {

            var st = this.sub_total;
            //var da = this.ded_amount_inc_tax;
            var fst = parseFloat(st);// - parseFloat(da));
            var temp = 0;

            var ded = this.ded_items.reduce(function(total, obj) {

                if(!obj.exclude_tax) {

                    temp += (parseFloat(fst) * parseFloat(obj.rate) / 100);
                    return parseFloat(temp.toFixed(2));
                }
                else
                {
                    temp = temp + 0;
                    return parseFloat(temp.toFixed(2));
                }

            }, 0);

            return this.ded_amount_inc_tax = ded;

        },

        ded_amount_ex_tax: function () {

            var st = this.sub_total;
            //var da = this.ded_amount_inc_tax;
            var fst = parseFloat(st);// - parseFloat(da));
            var temp = 0;

            var ded = this.ded_items.reduce(function(total, obj) {

                if(obj.exclude_tax) {

                    temp += (parseFloat(fst) * parseFloat(obj.rate) / 100);
                    return parseFloat(temp.toFixed(2));
                }
                else
                {
                    temp = temp + 0;
                    return parseFloat(temp.toFixed(2));
                }

            }, 0);

            return this.ded_amount_ex_tax = ded;

        },

        total_amount: function () {

            var total_amount = this.sub_total + this.tax_amount
                - this.ded_amount_ex_tax -  this.ded_amount_inc_tax;
            if (this.products_tax_total !== 0) {
                total_amount = this.sub_total + this.tax_amount - this.ded_amount_ex_tax - this.ded_amount_inc_tax + this.products_tax_total[1];
                //- this.products_ded_total[1] //no need to remove deductible as it is already counted
            }
            //- this.ded_amount; //removed discount_amount and replaced it with ded_amount
            // * parseFloat(this.currencyConvert.rate);
            return this.total_amount = total_amount.toFixed(2);
        },


        converted_amount: function () {

            var converted_amount = parseFloat(this.total_amount) * parseFloat(this.currencyConvert.rate);
            return converted_amount = converted_amount.toFixed(2);
        }

    },

    mounted () {

        this.initInvoiceForm();

    },


    created () {

        Bus.$on('getToken', (token) => {
            this.token = token
        // Get Item for option item in invoice_form
        this.getItemData()
    })

        Bus.$on('contact-edit',  (id) => {

            this.$http.get(contactApiUrl + '/' + id).then((response) => {

            this.new_contact = response.data;
        this.new_contact.id = id;
        this.new_contact.copy_billing_addr = false;

        this.new_contact.currency_id = response.data.currency_id;
        $('#currency_id').val(this.new_contact.currency_id).change();

        var temp = this.new_contact.payment_terms.toString();
        $('select[name=payment_terms]').val(temp);
        this.new_contact.payment_terms = response.data.payment_terms;

        this.new_contact.bill_country_id = response.data.bill_country_id;
        $('#bill_country_id').val(this.new_contact.bill_country_id).change();

        this.new_contact.ship_country_id = response.data.ship_country_id;
        $('#ship_country_id').val(this.new_contact.ship_country_id).change();

        $('.selectpicker').selectpicker('refresh');

    }, (errorResponse) =>{
            console.error("Error getting response from the server " + errorResponse);
        });

        $('#add-contact-modal').modal('show');

    });

        /*
         Bus.$on('select2contact',  (data) => {

         //console.log('triggered' + data);
         this.invoice_form.contact_id = data.id; //need to replace with open id

         this.invoice_form.customer_email = data.email;

         this.invoice_form.bill_to_addr = [data.name,data.bill_address1,data.bill_address2,data.bill_city,data.bill_state,data.bill_country_id].filter(val => val).join(', ');

         this.invoice_form.ship_to_addr = [data.ship_contact,data.ship_address1,data.ship_address2,data.ship_city,data.ship_state,data.ship_country_id].filter(val => val).join(', ');

         this.invoice_form.currency_id = data.currency_id;
         $('#currency_id').val(this.invoice_form.currency_id).change();

         console.log(data.currency_id);

         });
         */

        Bus.$on('contact-search',  (data) => {

            //console.log(data);

            if(data === false) {
            this.invoice_form.contact_id = ''; //need to replace with open id

            //this.invoice_form.customer_name = '';

            //this.invoice_form.customer_email = '';

            //this.invoice_form.bill_to_addr = '';

            //this.invoice_form.ship_to_addr = '';

            //this.invoice_form.currency_id = '';
        }else if(data) {

            //this.invoice_form.contact_id = this.new_contact.open_id;
            //console.log(data);
            //console.log(JSON.parse(JSON.stringify(data)));

            this.invoice_form.contact_id = data.open_id;

            //this.invoice_form.customer_name = data.name;

            //this.invoice_form.customer_email = data.email;

            //this.invoice_form.bill_to_addr = [data.name, data.bill_address1, data.bill_address2, data.bill_city, data.bill_state, data.bill_country_id].filter(val => val).join(', ');

            this.new_contact.open_id            = data.open_id;
            this.new_contact.name               = data.name;
            this.new_contact.email              = data.email;
            this.new_contact.phone              = data.phone;
            this.new_contact.bill_address1      = data.bill_address1;
            this.new_contact.bill_address2      = data.bill_address2;
            this.new_contact.bill_city          = data.bill_city;
            this.new_contact.bill_state         = data.bill_state;
            this.new_contact.bill_country_id    = data.bill_country_id;

            this.new_contact.ship_phone         = data.ship_phone;
            this.new_contact.ship_contact       = data.ship_contact;
            this.new_contact.ship_address1      = data.ship_address1;
            this.new_contact.ship_address2      = data.ship_address2;
            this.new_contact.ship_city          = data.ship_city;
            this.new_contact.ship_state         = data.ship_state;
            this.new_contact.ship_country_id    = data.ship_country_id;


            //this.invoice_form.ship_to_addr = [data.ship_contact, data.ship_address1, data.ship_address2, data.ship_city, data.ship_state, data.ship_country_id].filter(val => val).join(', ');

            //this.invoice_form.currency_id = data.currency_id;

            $('#currency_id').val(this.invoice_form.currency_id).change();

            //console.log(data.name);
        }

        //console.log(document.getElementById('customer_name').value);
        //console.log(this.invoice_form.customer_name);


    });

        Bus.$on('closeTour',  (tour_id) => {

            //console.log('close event');
            this.tour.tour_status = true;

        this.$http.put(tourApiUrl + '/' + tour_id, this.tour).then((response) => {
            //this.contacts.push(postBody);
            //console.log(response);
        }, (errorResponse) => {
            console.error("Error Posting to the server " + errorResponse);
        });

    });

        Bus.$on('hsnsac-search',  (data) => {

            if(data) {

                //console.log(data);
                this.gst_hsnsac.type            = data.type;
                this.gst_hsnsac.category        = data.category;
                this.gst_hsnsac.sub_category    = data.sub_category;
                this.gst_hsnsac.service         = data.service;
                this.gst_hsnsac.code            = data.code;
                this.gst_hsnsac.description     = data.description;
                this.gst_hsnsac.cgst            = data.cgst;
                this.gst_hsnsac.sgst            = data.sgst;
                this.gst_hsnsac.igst            = data.igst;
                this.gst_hsnsac.gst             = data.gst;

                this.new_item.hsnsac_type       = data.type;
                this.new_item.code              = data.code;
                this.new_item.cgst              = data.cgst;
                this.new_item.sgst              = data.sgst;
                this.new_item.igst              = data.igst;
                this.new_item.gst               = data.gst;

            }


        });

    }

});