import Vuetable from './../VueTable2/Vuetable.vue'
import VuetablePagination from './../VueTable2/VuetablePagination.vue'
import VuetablePaginationDropdown from './../VueTable2/VuetablePaginationDropdown.vue'
import VuetablePaginationInfo from './../VueTable2/VuetablePaginationInfo.vue'
let E_SERVER_ERROR = 'Error communicating with the server'

const apiContactUrl = base_api_path + "api/contacts";
const apiInvoiceUrl = base_api_path + "api/invoices";
const apiRecInvUrl = base_api_path + "api/invoices/recurring";
const apiTransactionUrl = base_api_path + "api/transactions";
const apiGLAccountUrl = base_api_path + "api/glaccounts";
const apiItemUrl = base_api_path + "api/inventory";
const apiQuoteUrl = base_api_path + "api/quotations";
const apiPaymentUrl = base_api_path + "api/payments";
const apiCreditNoteUrl = base_api_path + "api/creditnotes";

const invoiceUrl = base_api_path + "/invoices";
const creditnoteUrl = base_api_path + "/creditnotes";
const quoteUrl = base_api_path + "/quotations";

Vue.component('contact-actions', {
    template: [
        '<div>',
        '<button class="btn-xs btn-icon waves-effect waves-light btn-default" @click="onClickEditContact(rowData)"><i class="fa fa-edit"></i></button>',
        '&nbsp;<button class="btn-xs btn-icon waves-effect waves-light btn-danger" @click="onClickDeleteContact(rowData)"><i class="fa fa-remove"></i></button>',
        '</div>'
    ].join(''),
    props: {
        rowData: {
            type: Object,
            required: true
        }
    },
    methods: {
        onClickEditContact: function(data) {

            Bus.$emit('contact-edit',data.id);

        },


        onClickDeleteContact: function(data) {

            sweetAlert({
                title: 'Are you sure?',
                text: "You won't be able to revert this! This will also delete any related invoices & payments.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function () {


                this.$http.delete(apiContactUrl + "/" + data.id).then((response) => {

                    this.$parent.reload();

                    /*
                     sweetAlert({
                     type: 'success',
                     title: 'Contact Deleted',
                     text: 'I will close in 2 seconds.',
                     timer: 2000
                     });
                     */

                    sweetAlert({
                        type: 'success',
                        title: 'Deleted!',
                        text: 'Your contact has been deleted.',
                        timer: 2000
                    });

                }, (errorResponse) => {
                    console.error("Error Posting to the server. Logout and login." + errorResponse);
                });


            }.bind(this))

            this.$parent.reload();

        }



    }
})

Vue.component('contact-detail-row', {
    template: [
        '<div @click="onClick">',
        '<div class="inline field">',
        '<label>Name: </label>',
        '<span>{{rowData.name}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Email: </label>',
        '<span>{{rowData.email}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Phone: </label>',
        '<span>{{rowData.phone}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>First Contact: </label>',
        '<span>{{rowData.contact1}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Balance: </label>',
        '<span>{{rowData.balance}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Website: </label>',
        '<span>{{rowData.website}}</span>',
        '</div>',
        '</div>'
    ].join(''),
    props: {
        rowData: {
            type: Object,
            required: true
        }
    },
    methods: {
        onClick: function(event) {
            console.log('my-detail-row: on-click', event.target)
        }
    },
})

let contactTableColumns = [
    {
        name: 'image',
        title: '',
        visible: true,
        callback: 'displayImage'
    },
    {
        name: 'name',
        title: 'Name',
        sortField: 'name'
    },
    {
        name: 'email',
        sortField: 'email',
        visible: true
    },
    {
        name: 'phone',
        sortField: 'phone'
    },
    {
        name: 'contact1',
        title: 'First Contact',
        sortField: 'contact1'
    },
    {
        name: 'balance',
        title: 'Balance',
        sortField: 'balance'
    },
    {
        name: 'website',
        title: 'Website',
        sortField: 'website',
        visible: false
    },
    {
        name: '__component:contact-actions',
        dataClass: 'center aligned'
    }
]

Vue.component('contact-table', {
    template: '#contact-table',
    components: {
        Vuetable,
        VuetablePagination,
        VuetablePaginationDropdown,
        VuetablePaginationInfo,
    },
    data: function() {
        return {
            loading: '',
            searchFor: '',
            moreParams: {},
            fields: contactTableColumns,
            sortOrder: [{
                field: 'id',
                direction: 'desc',
            }],
            multiSort: true,
            paginationComponent: 'vuetable-pagination',
            perPage: 10,
            paginationInfoTemplate: 'Showing record: {from} to {to} from {total} item(s)',
        }
    },
    watch: {
        'perPage' (val, oldVal) {
            this.$nextTick(function() {
                this.$refs.vuetable.refresh()
            })
        },
        'paginationComponent' (val, oldVal) {
            this.$nextTick(function() {
                this.$refs.pagination.setPaginationData(this.$refs.vuetable.tablePagination)
            })
        }
    },
    methods: {
        transform: function(data) {
            let transformed = {}
            transformed.pagination = {
                total: data.total,
                per_page: data.per_page,
                current_page: data.current_page,
                last_page: data.last_page,
                next_page_url: data.next_page_url,
                prev_page_url: data.prev_page_url,
                from: data.from,
                to: data.to
            }

            transformed.data = []
            data = data.data
            for (let i = 0; i < data.length; i++) {
                transformed['data'].push({
                    id: data[i].open_id,
                    name: data[i].name,
                    image: data[i].image,
                    email: data[i].email,
                    phone: data[i].phone,
                    contact1: data[i].contact1,
                    website: data[i].website,
                    balance: data[i].balance//,
                    //address: data[i].address.line1 + ' ' + data[i].address.line2 + ' ' + data[i].address.zipcode
                })
            }

            return transformed
        },
        showSettingsModal () {

            console.log('settings');
            $('#settingsModal').modal({
                detachable: false,
                onVisible: function() {
                    $('.ui.checkbox').checkbox()
                }
            }).modal('show')
        },
        showLoader: function() {
            this.loading = 'loading'
        },
        hideLoader: function() {
            this.loading = ''
        },
        allCap (value) {
            return value.toUpperCase()
        },
        getFieldTitle: function(field) {
            if (field.title !== '') return field.title

            if (field.name.slice(0, 2) === '__') {
                return field.name.indexOf(':') >= 0
                    ? field.name.split(':')[1]
                    : fiel.name.replace('__', '')
            }
        },
        formatDate (value, fmt) {
            if (value === null) return ''
            fmt = (typeof(fmt) === 'undefined') ? 'D MMM YYYY' : fmt
            return moment(value, 'YYYY-MM-DD').format(fmt)
        },
        gender (value) {
            return value === 'M'
                ? '<span class="ui teal label"><i class="male icon"></i>Male</span>'
                : '<span class="ui pink label"><i class="female icon"></i>Female</span>'
        },
        displayImage (value) {
            return '<img src="'+ value +'" alt="contact-img" title="contact-img" class="img-circle thumb-sm" />'
        },
        showDetailRow: function(value) {
            let icon = this.$refs.vuetable.isVisibleDetailRow(value) ? 'down' : 'right'
            return [
                '<a class="show-detail-row">',
                '<i class="chevron circle ' + icon + ' icon"></i>',
                '</a>'
            ].join('')
        },
        setFilter: function() {
            this.moreParams = {
                'filter': this.searchFor
            }
            this.$nextTick(function() {
                this.$refs.vuetable.refresh()
            })
        },
        resetFilter: function() {
            this.searchFor = ''
            this.setFilter()
        },
        preg_quote: function( str ) {
            // http://kevin.vanzonneveld.net
            // +   original by: booeyOH
            // +   improved by: Ates Goral (http://magnetiq.com)
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   bugfixed by: Onno Marsman
            // *     example 1: preg_quote("$40");
            // *     returns 1: '\$40'
            // *     example 2: preg_quote("*RRRING* Hello?");
            // *     returns 2: '\*RRRING\* Hello\?'
            // *     example 3: preg_quote("\\.+*?[^]$(){}=!<>|:");
            // *     returns 3: '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'

            return (str+'').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
        },
        highlight: function(needle, haystack) {
            return haystack.replace(
                new RegExp('(' + this.preg_quote(needle) + ')', 'ig'),
                '<mark>$1</mark>'
            )
        },
        rowClassCB: function(data, index) {
            return (index % 2) === 0 ? 'odd' : 'even'
        },
        onCellClicked (data, field, event) {
            console.log('cellClicked', field.name)
            if (field.name !== '__actions') {
                this.$refs.vuetable.toggleDetailRow(data.id)
            }
        },
        onCellDoubleClicked (data, field, event) {
            console.log('cellDoubleClicked:', field.name)
        },
        onLoadSuccess (response) {
            // set pagination data to pagination-info component
            this.$refs.paginationInfo.setPaginationData(response.data)

            let data = response.data.data
            if (this.searchFor !== '') {
                for (let n in data) {
                    data[n].name = this.highlight(this.searchFor, data[n].name)
                    data[n].email = this.highlight(this.searchFor, data[n].email)
                    data[n].phone = this.highlight(this.searchFor, data[n].phone)
                }
            }
        },
        onLoadError (response) {
            if (response.status == 400) {
                sweetAlert('Something\'s Wrong!', response.data.message, 'error')
            } else {
                sweetAlert('Oops', E_SERVER_ERROR, 'error')
            }
        },
        onPaginationData (tablePagination) {
            this.$refs.paginationInfo.setPaginationData(tablePagination)
            this.$refs.pagination.setPaginationData(tablePagination)
        },
        onChangePage (page) {
            this.$refs.vuetable.changePage(page)
        },
        registerEvents () {
            let self = this
            this.$on('vuetable:action', (action, data) => {
                self.onActions(action, data)
            })
            this.$on('vuetable:cell-clicked', (data, field, event) => {
                self.onCellClicked(data, field, event)
            })
            this.$on('vuetable:cell-dblclicked', (data, field, event) => {
                self.onCellDoubleClicked(data, field, event)
            })
            this.$on('vuetable:load-success', (response) => {
                self.onLoadSuccess(response)
            })
            this.$on('vuetable:load-error', (response) => {
                self.onLoadError(response)
            })
        }
    },
    created () {
        this.registerEvents();

        Bus.$on('contact-refresh', () => {

            this.$refs.vuetable.refresh();

        })
    }
})

//invoice

Vue.component('invoice-actions', {
    template: [
        '<div>',
        '<button class="btn-xs btn-icon waves-effect waves-light btn-default" @click="onClickViewInvoice(rowData)"><i class="fa fa-eye"></i></button>',
        '&nbsp;<button v-show="rowData.status_id != 4 && rowData.status_id != 7" class="btn-xs btn-icon waves-effect waves-light btn-primary" @click="onClickEditInvoice(rowData)"><i class="fa fa-pencil-square-o"></i></button>',
        '&nbsp;<button class="btn-xs btn-icon waves-effect waves-light btn-danger" @click="onClickDeleteInvoice(rowData)"><i class="fa fa-remove"></i></button>',
        '</div>'
    ].join(''),
    props: {
        rowData: {
            type: Object,
            required: true
        }
    },
    methods: {
        onClickViewInvoice: function(data) {
            //console.log('actions: on-click', data.name)
            //sweetAlert(action, data.name)
            window.location.href = invoiceUrl + '/' + data.id;

        },

        onClickEditInvoice: function(data) {
            //console.log('actions: on-click', data.name)
            //sweetAlert(action, data.name)
            window.location.href = invoiceUrl + '/' + data.id + '/edit';

        },

        onClickDeleteInvoice: function(data) {

            /*
            this.$http.delete(apiInvoiceUrl + '/' + data.id ).then((response) => {

                this.$parent.reload();

                sweetAlert({
                    type: 'success',
                    title: 'Invoice Deleted',
                    text: 'I will close in 2 seconds.',
                    timer: 2000
                });


            }, (errorResponse) => {
                console.error("Error Posting to the server " + errorResponse);
            })
            //console.log(response);
            this.$parent.reload();
            */

            //start

            sweetAlert({
                title: 'Are you sure?',
                text: "You won't be able to revert this! This will also delete all the associated payments & transactions.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function () {


                this.$http.delete(apiInvoiceUrl + '/' + data.id ).then((response) => {

                    this.$parent.reload();

                    /*
                     sweetAlert({
                     type: 'success',
                     title: 'Contact Deleted',
                     text: 'I will close in 2 seconds.',
                     timer: 2000
                     });
                     */

                    sweetAlert({
                        type: 'success',
                        title: 'Deleted!',
                        text: 'Your invoice "'+ data.id + '" has been deleted.',
                        timer: 2000
                    });

                }, (errorResponse) => {
                    console.error("Error Posting to the server. Logout and login." + errorResponse);
                });


            }.bind(this))

            this.$parent.reload();

            //end


        }
    }
})

Vue.component('invoice-detail-row', {
    template: [
        '<div @click="onClick">',
        '<div class="inline field">',
        '<label>Invoice Number: </label>',
        '<span>{{rowData.id}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Status: </label>',
        '<span>{{rowData.status_text}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Due Date: </label>',
        '<span>{{rowData.due_date}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Invoice Date: </label>',
        '<span>{{rowData.post_date}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Customer: </label>',
        '<span>{{rowData.contact_id}}</span>',
        '</div>',
        '</div>'
    ].join(''),
    props: {
        rowData: {
            type: Object,
            required: true
        }
    },
    methods: {
        onClick: function(event) {
            console.log('invoice-detail-row: on-click', event.target)
        }
    },
})

let invoiceTableColumns = [
    {
        name: 'id',
        title: '#',
        sortField: 'id',
        visible: true
    },
    {
        name: 'status_text',
        title: 'Status',
        sortField: 'status_text'
    },
    {
        name: 'due_date',
        title: 'Due',
        sortField: 'due_date',
        visible: true
    },
    {
        name: 'post_date',
        title: 'Invoiced On',
        sortField: 'post_date'
    },
    {
        name: 'contact_id',
        title: 'Customer',
        sortField: 'contact_id'
    },
    {
        name: 'total',
        title: 'Total',
        sortField: 'total'
    },
    {
        name: 'balance',
        title: 'Balance',
        sortField: 'balance'
    },
    {
        name: '__component:invoice-actions',
        dataClass: 'center aligned'
    }
]

Vue.component('invoice-table', {
    template: '#invoice-table',
    components: {
        Vuetable,
        VuetablePagination,
        VuetablePaginationDropdown,
        VuetablePaginationInfo,
    },
    data: function() {
        return {
            loading: '',
            searchFor: '',
            moreParams: {},
            fields: invoiceTableColumns,
            sortOrder: [{
                field: 'open_id',
                direction: 'desc',
            }],
            multiSort: true,
            paginationComponent: 'vuetable-pagination',
            perPage: 10,
            paginationInfoTemplate: 'Showing record: {from} to {to} from {total} item(s)',
        }
    },
    watch: {
        'perPage' (val, oldVal) {
            this.$nextTick(function() {
                this.$refs.vuetable.refresh()
            })
        },
        'paginationComponent' (val, oldVal) {
            this.$nextTick(function() {
                this.$refs.pagination.setPaginationData(this.$refs.vuetable.tablePagination)
            })
        }
    },
    methods: {
        transform: function(data) {
            let transformed = {}
            transformed.pagination = {
                total: data.total,
                per_page: data.per_page,
                current_page: data.current_page,
                last_page: data.last_page,
                next_page_url: data.next_page_url,
                prev_page_url: data.prev_page_url,
                from: data.from,
                to: data.to
            }

            transformed.data = []
            data = data.data
            for (let i = 0; i < data.length; i++) {
                transformed['data'].push({
                    id: data[i].open_id,
                    status_id: data[i].status_id,
                    status_text: data[i].status_tab.text,
                    due_date: data[i].due_date,
                    post_date: data[i].post_date,
                    contact_id: data[i].contact_tab.name,
                    total: data[i].total_amount,
                    balance: data[i].balance
                    //address: data[i].address.line1 + ' ' + data[i].address.line2 + ' ' + data[i].address.zipcode
                })
            }

            return transformed
        },
        showSettingsModal () {
            $('#settingsModal').modal({
                detachable: false,
                onVisible: function() {
                    $('.ui.checkbox').checkbox()
                }
            }).modal('show')
        },
        showLoader: function() {
            this.loading = 'loading'
        },
        hideLoader: function() {
            this.loading = ''
        },
        allCap (value) {
            return value.toUpperCase()
        },
        getFieldTitle: function(field) {
            if (field.title !== '') return field.title

            if (field.name.slice(0, 2) === '__') {
                return field.name.indexOf(':') >= 0
                    ? field.name.split(':')[1]
                    : fiel.name.replace('__', '')
            }
        },
        formatDate (value, fmt) {
            if (value === null) return ''
            fmt = (typeof(fmt) === 'undefined') ? 'D MMM YYYY' : fmt
            return moment(value, 'YYYY-MM-DD').format(fmt)
        },
        gender (value) {
            return value === 'M'
                ? '<span class="ui teal label"><i class="male icon"></i>Male</span>'
                : '<span class="ui pink label"><i class="female icon"></i>Female</span>'
        },
        showDetailRow: function(value) {
            let icon = this.$refs.vuetable.isVisibleDetailRow(value) ? 'down' : 'right'
            return [
                '<a class="show-detail-row">',
                '<i class="chevron circle ' + icon + ' icon"></i>',
                '</a>'
            ].join('')
        },
        setFilter: function() {
            this.moreParams = {
                'filter': this.searchFor
            }
            this.$nextTick(function() {
                this.$refs.vuetable.refresh()
            })
        },
        resetFilter: function() {
            this.searchFor = ''
            this.setFilter()
        },
        preg_quote: function( str ) {
            // http://kevin.vanzonneveld.net
            // +   original by: booeyOH
            // +   improved by: Ates Goral (http://magnetiq.com)
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   bugfixed by: Onno Marsman
            // *     example 1: preg_quote("$40");
            // *     returns 1: '\$40'
            // *     example 2: preg_quote("*RRRING* Hello?");
            // *     returns 2: '\*RRRING\* Hello\?'
            // *     example 3: preg_quote("\\.+*?[^]$(){}=!<>|:");
            // *     returns 3: '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'

            return (str+'').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
        },
        highlight: function(needle, haystack) {
            return haystack.replace(
                new RegExp('(' + this.preg_quote(needle) + ')', 'ig'),
                '<mark>$1</mark>'
            )
        },
        rowClassCB: function(data, index) {
            return (index % 2) === 0 ? 'odd' : 'even'
        },
        onCellClicked (data, field, event) {
            console.log('cellClicked', field.name)
            if (field.name !== '__actions') {
                this.$refs.vuetable.toggleDetailRow(data.id)
            }
        },
        onCellDoubleClicked (data, field, event) {
            console.log('cellDoubleClicked:', field.name)
        },
        onLoadSuccess (response) {
            // set pagination data to pagination-info component
            this.$refs.paginationInfo.setPaginationData(response.data)

            let data = response.data.data
            if (this.searchFor !== '') {
                for (let n in data) {
                    data[n].contact.name = this.highlight(this.searchFor, data[n].contact.name)
                    data[n].user_invoice_status.text = this.highlight(this.searchFor, data[n].user_invoice_status.text)
                }
            }
        },
        onLoadError (response) {
            if (response.status == 400) {
                sweetAlert('Something\'s Wrong!', response.data.message, 'error')
            } else {
                sweetAlert('Oops', E_SERVER_ERROR, 'error')
            }
        },
        onPaginationData (tablePagination) {
            this.$refs.paginationInfo.setPaginationData(tablePagination)
            this.$refs.pagination.setPaginationData(tablePagination)
        },
        onChangePage (page) {
            this.$refs.vuetable.changePage(page)
        },
        registerEvents () {
            let self = this
            this.$on('vuetable:action', (action, data) => {
                self.onActions(action, data)
            })
            this.$on('vuetable:cell-clicked', (data, field, event) => {
                self.onCellClicked(data, field, event)
            })
            this.$on('vuetable:cell-dblclicked', (data, field, event) => {
                self.onCellDoubleClicked(data, field, event)
            })
            this.$on('vuetable:load-success', (response) => {
                self.onLoadSuccess(response)
            })
            this.$on('vuetable:load-error', (response) => {
                self.onLoadError(response)
            })
        }
    },
    created () {
        this.registerEvents()
    }
})

//quotation

Vue.component('quote-actions', {
    template: [
        '<div>',
        '<button class="btn-xs btn-icon waves-effect waves-light btn-default" @click="onClickViewQuote(rowData)"><i class="fa fa-eye"></i></button>',
        '&nbsp;<button class="btn-xs btn-icon waves-effect waves-light btn-danger" @click="onClickDeleteQuote(rowData)"><i class="fa fa-remove"></i></button>',
        '</div>'
    ].join(''),
    props: {
        rowData: {
            type: Object,
            required: true
        }
    },
    methods: {

        onClickViewQuote: function(data) {
            //console.log('actions: on-click', data.name)
            //sweetAlert(action, data.name)
            window.location.href = quoteUrl + '/' + data.id;

        },

        onClickDeleteQuote: function(data) {

            //start
            sweetAlert({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function () {


                this.$http.delete(apiQuoteUrl + '/' + data.id ).then((response) => {

                    this.$parent.reload();

                    /*
                     sweetAlert({
                     type: 'success',
                     title: 'Contact Deleted',
                     text: 'I will close in 2 seconds.',
                     timer: 2000
                     });
                     */

                    sweetAlert({
                        type: 'success',
                        title: 'Deleted!',
                        text: 'Your quote has been deleted.',
                        timer: 2000
                    });

                }, (errorResponse) => {
                    console.error("Error Posting to the server. Logout and login." + errorResponse);
                });


            }.bind(this))

            this.$parent.reload();
            //end

        }
    }
})

Vue.component('quote-detail-row', {
    template: [
        '<div @click="onClick">',
        '<div class="inline field">',
        '<label>Quote Number: </label>',
        '<span>{{rowData.id}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Status: </label>',
        '<span>{{rowData.status_id}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Due Date: </label>',
        '<span>{{rowData.due_date}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Quote Date: </label>',
        '<span>{{rowData.post_date}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Customer: </label>',
        '<span>{{rowData.contact_id}}</span>',
        '</div>',
        '</div>'
    ].join(''),
    props: {
        rowData: {
            type: Object,
            required: true
        }
    },
    methods: {
        onClick: function(event) {
            console.log('invoice-detail-row: on-click', event.target)
        }
    },
})

let quoteTableColumns = [
    {
        name: 'id',
        title: '#',
        sortField: 'id',
        visible: true
    },
    {
        name: 'status_id',
        title: 'Status',
        sortField: 'status_id'
    },
    {
        name: 'due_date',
        title: 'Due',
        sortField: 'due_date',
        visible: true
    },
    {
        name: 'post_date',
        title: 'Quote On',
        sortField: 'post_date'
    },
    {
        name: 'contact_id',
        title: 'Customer',
        sortField: 'contact_id'
    },
    {
        name: 'total_amount',
        title: 'Quote Amount',
        sortField: 'total_amount'
    },
    {
        name: '__component:quote-actions',
        dataClass: 'center aligned'
    }
]

Vue.component('quote-table', {
    template: '#quote-table',
    components: {
        Vuetable,
        VuetablePagination,
        VuetablePaginationDropdown,
        VuetablePaginationInfo,
    },
    data: function() {
        return {
            loading: '',
            searchFor: '',
            moreParams: {},
            fields: quoteTableColumns,
            sortOrder: [{
                field: 'id',
                direction: 'desc',
            }],
            multiSort: true,
            paginationComponent: 'vuetable-pagination',
            perPage: 10,
            paginationInfoTemplate: 'Showing record: {from} to {to} from {total} item(s)',
        }
    },
    watch: {
        'perPage' (val, oldVal) {
            this.$nextTick(function() {
                this.$refs.vuetable.refresh()
            })
        },
        'paginationComponent' (val, oldVal) {
            this.$nextTick(function() {
                this.$refs.pagination.setPaginationData(this.$refs.vuetable.tablePagination)
            })
        }
    },
    methods: {
        transform: function(data) {
            let transformed = {}
            transformed.pagination = {
                total: data.total,
                per_page: data.per_page,
                current_page: data.current_page,
                last_page: data.last_page,
                next_page_url: data.next_page_url,
                prev_page_url: data.prev_page_url,
                from: data.from,
                to: data.to
            }

            transformed.data = []
            data = data.data
            for (let i = 0; i < data.length; i++) {
                transformed['data'].push({
                    id: data[i].open_id,
                    status_id: data[i].status_tab.text,
                    due_date: data[i].due_date,
                    post_date: data[i].post_date,
                    contact_id: data[i].customer_name,
                    total_amount: data[i].total_amount
                    //address: data[i].address.line1 + ' ' + data[i].address.line2 + ' ' + data[i].address.zipcode
                })
            }

            return transformed
        },
        showSettingsModal () {
            $('#settingsModal').modal({
                detachable: false,
                onVisible: function() {
                    $('.ui.checkbox').checkbox()
                }
            }).modal('show')
        },
        showLoader: function() {
            this.loading = 'loading'
        },
        hideLoader: function() {
            this.loading = ''
        },
        allCap (value) {
            return value.toUpperCase()
        },
        getFieldTitle: function(field) {
            if (field.title !== '') return field.title

            if (field.name.slice(0, 2) === '__') {
                return field.name.indexOf(':') >= 0
                    ? field.name.split(':')[1]
                    : fiel.name.replace('__', '')
            }
        },
        formatDate (value, fmt) {
            if (value === null) return ''
            fmt = (typeof(fmt) === 'undefined') ? 'D MMM YYYY' : fmt
            return moment(value, 'YYYY-MM-DD').format(fmt)
        },
        gender (value) {
            return value === 'M'
                ? '<span class="ui teal label"><i class="male icon"></i>Male</span>'
                : '<span class="ui pink label"><i class="female icon"></i>Female</span>'
        },
        showDetailRow: function(value) {
            let icon = this.$refs.vuetable.isVisibleDetailRow(value) ? 'down' : 'right'
            return [
                '<a class="show-detail-row">',
                '<i class="chevron circle ' + icon + ' icon"></i>',
                '</a>'
            ].join('')
        },
        setFilter: function() {
            this.moreParams = {
                'filter': this.searchFor
            }
            this.$nextTick(function() {
                this.$refs.vuetable.refresh()
            })
        },
        resetFilter: function() {
            this.searchFor = ''
            this.setFilter()
        },
        preg_quote: function( str ) {
            // http://kevin.vanzonneveld.net
            // +   original by: booeyOH
            // +   improved by: Ates Goral (http://magnetiq.com)
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   bugfixed by: Onno Marsman
            // *     example 1: preg_quote("$40");
            // *     returns 1: '\$40'
            // *     example 2: preg_quote("*RRRING* Hello?");
            // *     returns 2: '\*RRRING\* Hello\?'
            // *     example 3: preg_quote("\\.+*?[^]$(){}=!<>|:");
            // *     returns 3: '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'

            return (str+'').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
        },
        highlight: function(needle, haystack) {
            return haystack.replace(
                new RegExp('(' + this.preg_quote(needle) + ')', 'ig'),
                '<mark>$1</mark>'
            )
        },
        rowClassCB: function(data, index) {
            return (index % 2) === 0 ? 'odd' : 'even'
        },
        onCellClicked (data, field, event) {
            console.log('cellClicked', field.name)
            if (field.name !== '__actions') {
                this.$refs.vuetable.toggleDetailRow(data.id)
            }
        },
        onCellDoubleClicked (data, field, event) {
            console.log('cellDoubleClicked:', field.name)
        },
        onLoadSuccess (response) {
            // set pagination data to pagination-info component
            this.$refs.paginationInfo.setPaginationData(response.data)

            let data = response.data.data
            if (this.searchFor !== '') {
                for (let n in data) {
                    data[n].contact.name = this.highlight(this.searchFor, data[n].contact.name)
                    data[n].user_invoice_status.text = this.highlight(this.searchFor, data[n].user_invoice_status.text)
                }
            }
        },
        onLoadError (response) {
            if (response.status == 400) {
                sweetAlert('Something\'s Wrong!', response.data.message, 'error')
            } else {
                sweetAlert('Oops', E_SERVER_ERROR, 'error')
            }
        },
        onPaginationData (tablePagination) {
            this.$refs.paginationInfo.setPaginationData(tablePagination)
            this.$refs.pagination.setPaginationData(tablePagination)
        },
        onChangePage (page) {
            this.$refs.vuetable.changePage(page)
        },
        registerEvents () {
            let self = this
            this.$on('vuetable:action', (action, data) => {
                self.onActions(action, data)
            })
            this.$on('vuetable:cell-clicked', (data, field, event) => {
                self.onCellClicked(data, field, event)
            })
            this.$on('vuetable:cell-dblclicked', (data, field, event) => {
                self.onCellDoubleClicked(data, field, event)
            })
            this.$on('vuetable:load-success', (response) => {
                self.onLoadSuccess(response)
            })
            this.$on('vuetable:load-error', (response) => {
                self.onLoadError(response)
            })
        }
    },
    created () {
        this.registerEvents()
    }
})

//transactions

Vue.component('transaction-actions', {
    template: [
        '<div>',
        '<button class="btn-xs btn-icon waves-effect waves-light btn-default" @click="onClickEditTXN(rowData)"><i class="fa fa-edit"></i></button>',
        '&nbsp;<button class="btn-xs btn-icon waves-effect waves-light btn-danger" @click="onClickDeleteTXN(rowData)"><i class="fa fa-remove"></i></button>',
        '</div>'
    ].join(''),
    props: {
        rowData: {
            type: Object,
            required: true
        }
    },
    methods: {

        onClickEditTXN: function(data) {

            Bus.$emit('txn-edit',data.id);

        },

        onClickDeleteTXN: function(data) {

            //
            sweetAlert({
                title: 'Are you sure?',
                text: "Deleting a transaction can cause a data inconsistency. You won't be able to revert this! This will also update GL balance.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function () {


                this.$http.delete(apiTransactionUrl + "/" + data.id).then((response) => {

                    this.$parent.reload();

                    sweetAlert({
                        type: 'success',
                        title: 'Deleted!',
                        text: 'Your transaction has been deleted.',
                        timer: 2000
                    });

                }, (errorResponse) => {
                    console.error("Error Posting to the server. Logout and login." + errorResponse);
                });


            }.bind(this))

            this.$parent.reload();
            //

        }

    }
})

Vue.component('transaction-detail-row', {
    template: [
        '<div @click="onClick">',
        '<div class="inline field">',
        '<label>Category: </label>',
        '<span>{{rowData.category}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Type: </label>',
        '<span>{{rowData.txn_type}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Transaction Date: </label>',
        '<span>{{rowData.txn_date}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>GL Account: </label>',
        '<span>{{rowData.gl_account_id}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Amount: </label>',
        '<span>{{rowData.txn_amount}}</span>',
        '</div>',
        '</div>'
    ].join(''),
    props: {
        rowData: {
            type: Object,
            required: true
        }
    },
    methods: {
        onClick: function(event) {
            console.log('my-detail-row: on-click', event.target)
        }
    },
})

let transactionTableColumns = [

    {
        name: 'txn_date',
        title: 'Transaction Date',
        sortField: 'txn_date'
    },
    {
        name: 'category',
        title: 'Category',
        sortField: 'category'
    },
    {
        name: 'gl_account_id',
        title: 'GL Account',
        sortField: 'gl_account_id'
    },
    {
        name: 'txn_type',
        title: 'Type',
        sortField: 'txn_type',
        visible: true
    },
    /*
     {
     name: 'txn_amount',
     title: 'Amount',
     sortField: 'txn_amount'
     },
     */
    {
        name: 'description',
        title: 'Description',
        sortField: 'description'
    },
    {
        name: 'txn_debit_amt',
        title: 'Debit',
        sortField: 'txn_debit_amt'
    },
    {
        name: 'txn_credit_amt',
        title: 'Credit',
        sortField: 'txn_credit_amt'
    },
    {
        name: 'balance',
        title: 'Balance',
        sortField: 'balance'
    },
    {
        name: '__component:transaction-actions',
        dataClass: 'center aligned'
    }
]

Vue.component('transaction-table', {
    template: '#transaction-table',
    components: {
        Vuetable,
        VuetablePagination,
        VuetablePaginationDropdown,
        VuetablePaginationInfo,
    },
    data: function() {
        return {
            loading: '',
            searchFor: '',
            moreParams: {},
            fields: transactionTableColumns,
            sortOrder: [{
                field: 'id',
                direction: 'desc',
            }],
            multiSort: true,
            paginationComponent: 'vuetable-pagination',
            perPage: 10,
            paginationInfoTemplate: 'Showing record: {from} to {to} from {total} item(s)',
        }
    },
    watch: {
        'perPage' (val, oldVal) {
            this.$nextTick(function() {
                this.$refs.vuetable.refresh()
            })
        },
        'paginationComponent' (val, oldVal) {
            this.$nextTick(function() {
                this.$refs.pagination.setPaginationData(this.$refs.vuetable.tablePagination)
            })
        }
    },
    methods: {
        transform: function(data) {

            var credit_amt, debit_amt; //added by nikhil

            let transformed = {}
            transformed.pagination = {
                total: data.total,
                per_page: data.per_page,
                current_page: data.current_page,
                last_page: data.last_page,
                next_page_url: data.next_page_url,
                prev_page_url: data.prev_page_url,
                from: data.from,
                to: data.to
            }

            transformed.data = []
            data = data.data
            for (let i = 0; i < data.length; i++) {

                //var debit_amt;

                if(data[i].txn_type === 'C'){
                    credit_amt = data[i].txn_amount;
                    debit_amt  = 0;
                }
                else
                {
                    debit_amt   = data[i].txn_amount;
                    credit_amt  = 0;
                }

                transformed['data'].push({
                    id: data[i].open_id,
                    category: data[i].txn_cat_tab.text, //category,
                    txn_type: data[i].txn_type_tab.text,
                    txn_date: data[i].txn_date,
                    gl_account_id: data[i].gl_account_tab.name,
                    txn_amount: data[i].txn_amount,
                    description: data[i].description,
                    txn_credit_amt: credit_amt,
                    txn_debit_amt: debit_amt,
                    balance: data[i].balance

                })
            }

            return transformed
        },
        showSettingsModal () {
            $('#settingsModal').modal({
                detachable: false,
                onVisible: function() {
                    $('.ui.checkbox').checkbox()
                }
            }).modal('show')
        },
        showLoader: function() {
            this.loading = 'loading'
        },
        hideLoader: function() {
            this.loading = ''
        },
        allCap (value) {
            return value.toUpperCase()
        },
        getFieldTitle: function(field) {
            if (field.title !== '') return field.title

            if (field.name.slice(0, 2) === '__') {
                return field.name.indexOf(':') >= 0
                    ? field.name.split(':')[1]
                    : fiel.name.replace('__', '')
            }
        },
        formatDate (value, fmt) {
            if (value === null) return ''
            fmt = (typeof(fmt) === 'undefined') ? 'D MMM YYYY' : fmt
            return moment(value, 'YYYY-MM-DD').format(fmt)
        },
        gender (value) {
            return value === 'M'
                ? '<span class="ui teal label"><i class="male icon"></i>Male</span>'
                : '<span class="ui pink label"><i class="female icon"></i>Female</span>'
        },
        showDetailRow: function(value) {
            let icon = this.$refs.vuetable.isVisibleDetailRow(value) ? 'down' : 'right'
            return [
                '<a class="show-detail-row">',
                '<i class="chevron circle ' + icon + ' icon"></i>',
                '</a>'
            ].join('')
        },
        setFilter: function() {
            this.moreParams = {
                'filter': this.searchFor
            }
            this.$nextTick(function() {
                this.$refs.vuetable.refresh()
            })
        },
        resetFilter: function() {
            this.searchFor = ''
            this.setFilter()
        },
        preg_quote: function( str ) {

            return (str+'').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
        },
        highlight: function(needle, haystack) {
            return haystack.replace(
                new RegExp('(' + this.preg_quote(needle) + ')', 'ig'),
                '<mark>$1</mark>'
            )
        },
        rowClassCB: function(data, index) {
            return (index % 2) === 0 ? 'odd' : 'even'
        },
        onCellClicked (data, field, event) {
            console.log('cellClicked', field.name)
            if (field.name !== '__actions') {
                this.$refs.vuetable.toggleDetailRow(data.id)
            }
        },
        onCellDoubleClicked (data, field, event) {
            console.log('cellDoubleClicked:', field.name)
        },
        onLoadSuccess (response) {
            // set pagination data to pagination-info component
            this.$refs.paginationInfo.setPaginationData(response.data)

            let data = response.data.data
            if (this.searchFor !== '') {
                for (let n in data) {
                    data[n].category = this.highlight(this.searchFor, data[n].category)
                    data[n].txn_type = this.highlight(this.searchFor, data[n].txn_type)

                }
            }
        },
        onLoadError (response) {
            if (response.status == 400) {
                sweetAlert('Something\'s Wrong!', response.data.message, 'error')
            } else {
                sweetAlert('Oops', E_SERVER_ERROR, 'error')
            }
        },
        onPaginationData (tablePagination) {
            this.$refs.paginationInfo.setPaginationData(tablePagination)
            this.$refs.pagination.setPaginationData(tablePagination)
        },
        onChangePage (page) {
            this.$refs.vuetable.changePage(page)
        },
        registerEvents () {
            let self = this
            this.$on('vuetable:action', (action, data) => {
                self.onActions(action, data)
            })
            this.$on('vuetable:cell-clicked', (data, field, event) => {
                self.onCellClicked(data, field, event)
            })
            this.$on('vuetable:cell-dblclicked', (data, field, event) => {
                self.onCellDoubleClicked(data, field, event)
            })
            this.$on('vuetable:load-success', (response) => {
                self.onLoadSuccess(response)
            })
            this.$on('vuetable:load-error', (response) => {
                self.onLoadError(response)
            })
        }
    },
    created () {
        this.registerEvents()

        Bus.$on('transaction-refresh', () => {

            this.$refs.vuetable.refresh();

        })

    }
})

//gl accounts

Vue.component('glaccount-actions', {
    template: [
        '<div>',
        '<button class="btn-xs btn-icon waves-effect waves-light btn-default" @click="onClickEditGL(rowData)"><i class="fa fa-edit"></i></button>',
        '&nbsp;<button class="btn-xs btn-icon waves-effect waves-light btn-danger" @click="onClickDeleteGL(rowData)"><i class="fa fa-remove"></i></button>',
        '</div>'
    ].join(''),
    props: {
        rowData: {
            type: Object,
            required: true
        }
    },
    methods: {
        onClickEditGL: function(data) {

            Bus.$emit('glitem-edit',data.id);

        },

        onClickDeleteGL: function(data) {

            sweetAlert({
                title: 'Cant be deleted',
                text: "GL Account can not be deleted. You can update its balance. Please connect with customer care if needed.",
                type: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#d33'
            }).then(function () {

                /*
                
                this.$http.delete(apiGLAccountUrl + '/' + data.id ).then((response) => {

                    this.$parent.reload();

                }, (errorResponse) => {
                    console.error("Error Posting to the server " + errorResponse);
                })

                */

            }.bind(this))

            this.$parent.reload();

        }

    }
})

Vue.component('glaccount-detail-row', {
    template: [
        '<div @click="onClick">',
        '<div class="inline field">',
        '<label>Type: </label>',
        '<span>{{rowData.Type}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Name: </label>',
        '<span>{{rowData.name}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Internal Balance: </label>',
        '<span>{{rowData.int_balance}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>External Balance: </label>',
        '<span>{{rowData.ext_balance}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Description: </label>',
        '<span>{{rowData.description}}</span>',
        '</div>',
        '</div>'
    ].join(''),
    props: {
        rowData: {
            type: Object,
            required: true
        }
    },
    methods: {
        onClick: function(event) {
            console.log('my-detail-row: on-click', event.target)
        }
    },
})

let glaccountTableColumns = [
    {
        name: 'type',
        title: 'Type',
        sortField: 'type'
    },
    {
        name: 'name',
        title: 'Name',
        sortField: 'name',
        visible: true
    },
    {
        name: 'int_balance',
        title: 'Internal Balance',
        sortField: 'int_balance'
    },
    {
        name: 'ext_balance',
        title: 'External Balance',
        sortField: 'ext_balance'
    },
    {
        name: 'description',
        title: 'Description',
        sortField: 'description'
    },
    {
        name: '__component:glaccount-actions',
        dataClass: 'center aligned'
    }
]

Vue.component('glaccount-table', {
    template: '#glaccount-table',
    components: {
        Vuetable,
        VuetablePagination,
        VuetablePaginationDropdown,
        VuetablePaginationInfo,
    },
    data: function() {
        return {
            loading: '',
            searchFor: '',
            moreParams: {},
            fields: glaccountTableColumns,
            sortOrder: [{
                field: 'id',
                direction: 'desc',
            }],
            multiSort: true,
            paginationComponent: 'vuetable-pagination',
            perPage: 10,
            paginationInfoTemplate: 'Showing record: {from} to {to} from {total} item(s)',
        }
    },
    watch: {
        'perPage' (val, oldVal) {
            this.$nextTick(function() {
                this.$refs.vuetable.refresh()
            })
        },
        'paginationComponent' (val, oldVal) {
            this.$nextTick(function() {
                this.$refs.pagination.setPaginationData(this.$refs.vuetable.tablePagination)
            })
        }
    },
    methods: {
        transform: function(data) {
            let transformed = {}
            transformed.pagination = {
                total: data.total,
                per_page: data.per_page,
                current_page: data.current_page,
                last_page: data.last_page,
                next_page_url: data.next_page_url,
                prev_page_url: data.prev_page_url,
                from: data.from,
                to: data.to
            }

            transformed.data = []
            data = data.data
            for (let i = 0; i < data.length; i++) {
                transformed['data'].push({
                    id: data[i].open_id,
                    type: data[i].gl_type_tab.sub,
                    name: data[i].name,
                    int_balance: data[i].int_balance,
                    ext_balance: data[i].ext_balance,
                    description: data[i].description//,
                    //address: data[i].address.line1 + ' ' + data[i].address.line2 + ' ' + data[i].address.zipcode
                })
            }

            return transformed
        },
        showSettingsModal () {
            $('#settingsModal').modal({
                detachable: false,
                onVisible: function() {
                    $('.ui.checkbox').checkbox()
                }
            }).modal('show')
        },
        showLoader: function() {
            this.loading = 'loading'
        },
        hideLoader: function() {
            this.loading = ''
        },
        allCap (value) {
            return value.toUpperCase()
        },
        getFieldTitle: function(field) {
            if (field.title !== '') return field.title

            if (field.name.slice(0, 2) === '__') {
                return field.name.indexOf(':') >= 0
                    ? field.name.split(':')[1]
                    : fiel.name.replace('__', '')
            }
        },
        formatDate (value, fmt) {
            if (value === null) return ''
            fmt = (typeof(fmt) === 'undefined') ? 'D MMM YYYY' : fmt
            return moment(value, 'YYYY-MM-DD').format(fmt)
        },
        gender (value) {
            return value === 'M'
                ? '<span class="ui teal label"><i class="male icon"></i>Male</span>'
                : '<span class="ui pink label"><i class="female icon"></i>Female</span>'
        },
        showDetailRow: function(value) {
            let icon = this.$refs.vuetable.isVisibleDetailRow(value) ? 'down' : 'right'
            return [
                '<a class="show-detail-row">',
                '<i class="chevron circle ' + icon + ' icon"></i>',
                '</a>'
            ].join('')
        },
        setFilter: function() {
            this.moreParams = {
                'filter': this.searchFor
            }
            this.$nextTick(function() {
                this.$refs.vuetable.refresh()
            })
        },
        resetFilter: function() {
            this.searchFor = ''
            this.setFilter()
        },
        preg_quote: function( str ) {
            // http://kevin.vanzonneveld.net
            // +   original by: booeyOH
            // +   improved by: Ates Goral (http://magnetiq.com)
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   bugfixed by: Onno Marsman
            // *     example 1: preg_quote("$40");
            // *     returns 1: '\$40'
            // *     example 2: preg_quote("*RRRING* Hello?");
            // *     returns 2: '\*RRRING\* Hello\?'
            // *     example 3: preg_quote("\\.+*?[^]$(){}=!<>|:");
            // *     returns 3: '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'

            return (str+'').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
        },
        highlight: function(needle, haystack) {
            return haystack.replace(
                new RegExp('(' + this.preg_quote(needle) + ')', 'ig'),
                '<mark>$1</mark>'
            )
        },
        rowClassCB: function(data, index) {
            return (index % 2) === 0 ? 'odd' : 'even'
        },
        onCellClicked (data, field, event) {
            console.log('cellClicked', field.name)
            if (field.name !== '__actions') {
                this.$refs.vuetable.toggleDetailRow(data.id)
            }
        },
        onCellDoubleClicked (data, field, event) {
            console.log('cellDoubleClicked:', field.name)
        },
        onLoadSuccess (response) {
            // set pagination data to pagination-info component
            this.$refs.paginationInfo.setPaginationData(response.data)

            let data = response.data.data
            if (this.searchFor !== '') {
                for (let n in data) {
                    data[n].name = this.highlight(this.searchFor, data[n].name)
                    data[n].description = this.highlight(this.searchFor, data[n].description)
                }
            }
        },
        onLoadError (response) {
            if (response.status == 400) {
                sweetAlert('Something\'s Wrong!', response.data.message, 'error')
            } else {
                sweetAlert('Oops', E_SERVER_ERROR, 'error')
            }
        },
        onPaginationData (tablePagination) {
            this.$refs.paginationInfo.setPaginationData(tablePagination)
            this.$refs.pagination.setPaginationData(tablePagination)
        },
        onChangePage (page) {
            this.$refs.vuetable.changePage(page)
        },
        registerEvents () {
            let self = this
            this.$on('vuetable:action', (action, data) => {
                self.onActions(action, data)
            })
            this.$on('vuetable:cell-clicked', (data, field, event) => {
                self.onCellClicked(data, field, event)
            })
            this.$on('vuetable:cell-dblclicked', (data, field, event) => {
                self.onCellDoubleClicked(data, field, event)
            })
            this.$on('vuetable:load-success', (response) => {
                self.onLoadSuccess(response)
            })
            this.$on('vuetable:load-error', (response) => {
                self.onLoadError(response)
            })
        }
    },
    created () {
        this.registerEvents();

        Bus.$on('glaccount-refresh', () => {

            //console.log('refresh gl account complete');
            this.$refs.vuetable.refresh();

        })
    }
})

//inventory

Vue.component('inventory-actions', {
    template: [
        '<div>',
        '<button class="btn-xs btn-icon waves-effect waves-light btn-default" @click="onClickEditItem(rowData)"><i class="fa fa-edit"></i></button>',
        '&nbsp;<button class="btn-xs btn-icon waves-effect waves-light btn-danger" @click="onClickDeleteItem(rowData)"><i class="fa fa-remove"></i></button>',
        '</div>'
    ].join(''),
    props: {
        rowData: {
            type: Object,
            required: true
        }
    },
    methods: {
        onClickEditItem: function(data) {

            Bus.$emit('item-edit',data.id);

        },

        onClickDeleteItem: function(data) {

            this.$http.delete(apiItemUrl + "/" + data.id).then((response) => {

                this.$parent.reload();

                sweetAlert({
                    type: 'success',
                    title: 'Item Deleted',
                    text: 'I will close in 2 seconds.',
                    timer: 2000
                });

            }, (errorResponse) => {
                console.error("Error Posting to the server. Logout and login." + errorResponse);
            });

        }

    }
})

Vue.component('inventory-detail-row', {
    template: [
        '<div @click="onClick">',
        '<div class="inline field">',
        '<label>ID: </label>',
        '<span>{{rowData.item_id}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Name: </label>',
        '<span>{{rowData.name}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Description: </label>',
        '<span>{{rowData.description}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Qty: </label>',
        '<span>{{rowData.qty}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Buy Price: </label>',
        '<span>{{rowData.buy_price}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Sell Price: </label>',
        '<span>{{rowData.sell_price}}</span>',
        '</div>',
        '</div>'
    ].join(''),
    props: {
        rowData: {
            type: Object,
            required: true
        }
    },
    methods: {
        onClick: function(event) {
            console.log('my-detail-row: on-click', event.target)
        }
    },
})

let inventoryTableColumns = [
    {
        name: 'image',
        title: '',
        visible: true,
        callback: 'displayImage'
    },
    {
        name: 'item_id',
        title: 'Item ID',
        sortField: 'item_id'
    },
    {
        name: 'name',
        title: 'Name',
        sortField: 'name'
    },
    {
        name: 'description',
        title: 'Description',
        sortField: 'description',
        visible: true
    },
    {
        name: 'qty',
        title: 'Quantity',
        sortField: 'qty'
    },
    {
        name: 'buy_price',
        title: 'Buy Price',
        sortField: 'buy_price'
    },
    {
        name: 'sell_price',
        title: 'Sell Price',
        sortField: 'sell_price'
    },
    {
        name: '__component:inventory-actions',
        dataClass: 'center aligned'
    }
]

Vue.component('inventory-table', {
    template: '#inventory-table',
    components: {
        Vuetable,
        VuetablePagination,
        VuetablePaginationDropdown,
        VuetablePaginationInfo,
    },
    data: function() {
        return {
            loading: '',
            searchFor: '',
            moreParams: {},
            fields: inventoryTableColumns,
            sortOrder: [{
                field: 'id',
                direction: 'desc',
            }],
            multiSort: true,
            paginationComponent: 'vuetable-pagination',
            perPage: 10,
            paginationInfoTemplate: 'Showing record: {from} to {to} from {total} item(s)',
        }
    },
    watch: {
        'perPage' (val, oldVal) {
            this.$nextTick(function() {
                this.$refs.vuetable.refresh()
            })
        },
        'paginationComponent' (val, oldVal) {
            this.$nextTick(function() {
                this.$refs.pagination.setPaginationData(this.$refs.vuetable.tablePagination)
            })
        }
    },
    methods: {
        transform: function(data) {
            let transformed = {}
            transformed.pagination = {
                total: data.total,
                per_page: data.per_page,
                current_page: data.current_page,
                last_page: data.last_page,
                next_page_url: data.next_page_url,
                prev_page_url: data.prev_page_url,
                from: data.from,
                to: data.to
            }

            transformed.data = []
            data = data.data
            for (let i = 0; i < data.length; i++) {
                transformed['data'].push({
                    id: data[i].open_id,
                    image: data[i].image,
                    item_id: data[i].item_id,
                    name: data[i].name,
                    description: data[i].description,
                    qty: data[i].qty,
                    buy_price: data[i].buy_price,
                    sell_price: data[i].sell_price,
                    //address: data[i].address.line1 + ' ' + data[i].address.line2 + ' ' + data[i].address.zipcode
                })
            }

            return transformed
        },
        showSettingsModal () {

            console.log('settings');
            $('#settingsModal').modal({
                detachable: false,
                onVisible: function() {
                    $('.ui.checkbox').checkbox()
                }
            }).modal('show')
        },
        showLoader: function() {
            this.loading = 'loading'
        },
        hideLoader: function() {
            this.loading = ''
        },
        allCap (value) {
            return value.toUpperCase()
        },
        getFieldTitle: function(field) {
            if (field.title !== '') return field.title

            if (field.name.slice(0, 2) === '__') {
                return field.name.indexOf(':') >= 0
                    ? field.name.split(':')[1]
                    : fiel.name.replace('__', '')
            }
        },
        showDetailRow: function(value) {
            let icon = this.$refs.vuetable.isVisibleDetailRow(value) ? 'down' : 'right'
            return [
                '<a class="show-detail-row">',
                '<i class="chevron circle ' + icon + ' icon"></i>',
                '</a>'
            ].join('')
        },
        displayImage (value) {
            return '<img src="'+ value +'" alt="inventory-img" title="inventory-img" class="img-circle thumb-sm" />'
        },
        setFilter: function() {
            this.moreParams = {
                'filter': this.searchFor
            }
            this.$nextTick(function() {
                this.$refs.vuetable.refresh()
            })
        },
        resetFilter: function() {
            this.searchFor = ''
            this.setFilter()
        },
        preg_quote: function( str ) {
            // http://kevin.vanzonneveld.net
            // +   original by: booeyOH
            // +   improved by: Ates Goral (http://magnetiq.com)
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   bugfixed by: Onno Marsman
            // *     example 1: preg_quote("$40");
            // *     returns 1: '\$40'
            // *     example 2: preg_quote("*RRRING* Hello?");
            // *     returns 2: '\*RRRING\* Hello\?'
            // *     example 3: preg_quote("\\.+*?[^]$(){}=!<>|:");
            // *     returns 3: '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'

            return (str+'').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
        },
        highlight: function(needle, haystack) {
            return haystack.replace(
                new RegExp('(' + this.preg_quote(needle) + ')', 'ig'),
                '<mark>$1</mark>'
            )
        },
        rowClassCB: function(data, index) {
            return (index % 2) === 0 ? 'odd' : 'even'
        },
        onCellClicked (data, field, event) {
            console.log('cellClicked', field.name)
            if (field.name !== '__actions') {
                this.$refs.vuetable.toggleDetailRow(data.id)
            }
        },
        onCellDoubleClicked (data, field, event) {
            console.log('cellDoubleClicked:', field.name)
        },
        onLoadSuccess (response) {
            // set pagination data to pagination-info component
            this.$refs.paginationInfo.setPaginationData(response.data)

            let data = response.data.data
            if (this.searchFor !== '') {
                for (let n in data) {
                    data[n].item_id = this.highlight(this.searchFor, data[n].item_id)
                    data[n].name = this.highlight(this.searchFor, data[n].name)
                    data[n].description = this.highlight(this.searchFor, data[n].description)
                }
            }
        },
        onLoadError (response) {
            if (response.status == 400) {
                sweetAlert('Something\'s Wrong!', response.data.message, 'error')
            } else {
                sweetAlert('Oops', E_SERVER_ERROR, 'error')
            }
        },
        onPaginationData (tablePagination) {
            this.$refs.paginationInfo.setPaginationData(tablePagination)
            this.$refs.pagination.setPaginationData(tablePagination)
        },
        onChangePage (page) {
            this.$refs.vuetable.changePage(page)
        },
        registerEvents () {
            let self = this
            this.$on('vuetable:action', (action, data) => {
                self.onActions(action, data)
            })
            this.$on('vuetable:cell-clicked', (data, field, event) => {
                self.onCellClicked(data, field, event)
            })
            this.$on('vuetable:cell-dblclicked', (data, field, event) => {
                self.onCellDoubleClicked(data, field, event)
            })
            this.$on('vuetable:load-success', (response) => {
                self.onLoadSuccess(response)
            })
            this.$on('vuetable:load-error', (response) => {
                self.onLoadError(response)
            })
        }
    },
    created () {
        this.registerEvents();

        Bus.$on('inventory-refresh', () => {

            this.$refs.vuetable.refresh();

        })
    }
})

//payment

Vue.component('payment-actions', {
    template: [
        '<div>',
        '<button class="btn-xs btn-icon waves-effect waves-light btn-default" @click="onClickEditItem(rowData)"><i class="fa fa-edit"></i></button>',
        '&nbsp;<button class="btn-xs btn-icon waves-effect waves-light btn-danger" @click="onClickDeleteItem(rowData)"><i class="fa fa-remove"></i></button>',
        '</div>'
    ].join(''),
    props: {
        rowData: {
            type: Object,
            required: true
        }
    },
    methods: {
        onClickEditItem: function(data) {

            //Bus.$emit('item-edit',data.id);

        },

        onClickDeleteItem: function(data) {

            /*
             this.$http.delete(apiPaymentUrl + "/" + data.id).then((response) => {

             this.$parent.reload();

             sweetAlert({
             type: 'success',
             title: 'Payment Deleted',
             text: 'I will close in 2 seconds.',
             timer: 2000
             });

             }, (errorResponse) => {
             console.error("Error Posting to the server. Logout and login." + errorResponse);
             });
             */

        }

    }
})

Vue.component('payment-detail-row', {
    template: [
        '<div @click="onClick">',
        '<div class="inline field">',
        '<label>ID: </label>',
        '<span>{{rowData.id}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Invoice Number: </label>',
        '<span>{{rowData.invoice_open_id}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Name: </label>',
        '<span>{{rowData.customer_name}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Invoice Total: </label>',
        '<span>{{rowData.invoice_total}}</span>',
        '</div>',
        '<div class="inline field">',
        //'<label>Invoice Balance: </label>',
        //'<span>{{rowData.invoice_balance}}</span>',
        //'</div>',
        //'<div class="inline field">',
        '<label>Amount Paid (Local): </label>',
        '<span>{{rowData.amount_paid}}</span>',
        '</div>',
        '</div>'
    ].join(''),
    props: {
        rowData: {
            type: Object,
            required: true
        }
    },
    methods: {
        onClick: function(event) {
            console.log('my-detail-row: on-click', event.target)
        }
    },
})

let paymentTableColumns = [
    {
        name: 'id',
        title: 'ID',
        sortField: 'id'
    },
    {
        name: 'invoice_open_id',
        title: 'Invoice ID',
        sortField: 'invoice_open_id'
    },
    {
        name: 'customer_name',
        title: 'Name',
        sortField: 'customer_name',
        visible: true
    },
    {
        name: 'invoice_total',
        title: 'Invoice Total',
        sortField: 'invoice_total'
    },
    /*
     {
     name: 'invoice_balance',
     title: 'Invoice Balance',
     sortField: 'invoice_balance'
     },
     */
    {
        name: 'amount_paid',
        title: 'Amount Paid',
        sortField: 'amount_paid'
    },
    {
        name: 'payment_type',
        title: 'Payment Type',
        sortField: 'payment_type'
    },
    {
        name: '__component:payment-actions',
        dataClass: 'center aligned'
    }
]

Vue.component('payment-table', {
    template: '#payment-table',
    components: {
        Vuetable,
        VuetablePagination,
        VuetablePaginationDropdown,
        VuetablePaginationInfo,
    },
    data: function() {
        return {
            loading: '',
            searchFor: '',
            moreParams: {},
            fields: paymentTableColumns,
            sortOrder: [{
                field: 'id',
                direction: 'desc',
            }],
            multiSort: true,
            paginationComponent: 'vuetable-pagination',
            perPage: 10,
            paginationInfoTemplate: 'Showing record: {from} to {to} from {total} item(s)',
        }
    },
    watch: {
        'perPage' (val, oldVal) {
            this.$nextTick(function() {
                this.$refs.vuetable.refresh()
            })
        },
        'paginationComponent' (val, oldVal) {
            this.$nextTick(function() {
                this.$refs.pagination.setPaginationData(this.$refs.vuetable.tablePagination)
            })
        }
    },
    methods: {
        transform: function(data) {

            var pay_type_string;
            //var invoice_balance_cal;

            let transformed = {}
            transformed.pagination = {
                total: data.total,
                per_page: data.per_page,
                current_page: data.current_page,
                last_page: data.last_page,
                next_page_url: data.next_page_url,
                prev_page_url: data.prev_page_url,
                from: data.from,
                to: data.to
            }

            transformed.data = []
            data = data.data
            for (let i = 0; i < data.length; i++) {

                switch (data[i].payment_type_id){
                    case 1:
                        pay_type_string = 'ACH';
                        break;

                    case 2:
                        pay_type_string = 'Bank Transfer';
                        break;

                    case 3:
                        pay_type_string = 'Cash';
                        break;

                    case 4:
                        pay_type_string = 'Check';
                        break;

                    case 5:
                        pay_type_string = 'Credit Card';
                        break;

                    default:
                        pay_type_string = 'Online';

                }

                //invoice_balance_cal = data[i].invoice_tab.total_amount_local - data[i].amount_local;

                transformed['data'].push({
                    id: data[i].open_id,
                    invoice_open_id: data[i].invoice_tab.open_id,
                    customer_name: data[i].contact_tab.name,
                    invoice_total: data[i].invoice_tab.total_amount_local,
                    //invoice_balance: invoice_balance_cal,
                    amount_paid: data[i].amount_local,
                    payment_type: pay_type_string
                })
            }

            return transformed
        },
        showSettingsModal () {

            console.log('settings');
            $('#settingsModal').modal({
                detachable: false,
                onVisible: function() {
                    $('.ui.checkbox').checkbox()
                }
            }).modal('show')
        },
        showLoader: function() {
            this.loading = 'loading'
        },
        hideLoader: function() {
            this.loading = ''
        },
        allCap (value) {
            return value.toUpperCase()
        },
        getFieldTitle: function(field) {
            if (field.title !== '') return field.title

            if (field.name.slice(0, 2) === '__') {
                return field.name.indexOf(':') >= 0
                    ? field.name.split(':')[1]
                    : fiel.name.replace('__', '')
            }
        },
        showDetailRow: function(value) {
            let icon = this.$refs.vuetable.isVisibleDetailRow(value) ? 'down' : 'right'
            return [
                '<a class="show-detail-row">',
                '<i class="chevron circle ' + icon + ' icon"></i>',
                '</a>'
            ].join('')
        },
        displayImage (value) {
            return '<img src="'+ value +'" alt="inventory-img" title="inventory-img" class="img-circle thumb-sm" />'
        },
        setFilter: function() {
            this.moreParams = {
                'filter': this.searchFor
            }
            this.$nextTick(function() {
                this.$refs.vuetable.refresh()
            })
        },
        resetFilter: function() {
            this.searchFor = ''
            this.setFilter()
        },
        preg_quote: function( str ) {
            // http://kevin.vanzonneveld.net
            // +   original by: booeyOH
            // +   improved by: Ates Goral (http://magnetiq.com)
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   bugfixed by: Onno Marsman
            // *     example 1: preg_quote("$40");
            // *     returns 1: '\$40'
            // *     example 2: preg_quote("*RRRING* Hello?");
            // *     returns 2: '\*RRRING\* Hello\?'
            // *     example 3: preg_quote("\\.+*?[^]$(){}=!<>|:");
            // *     returns 3: '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'

            return (str+'').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
        },
        highlight: function(needle, haystack) {
            return haystack.replace(
                new RegExp('(' + this.preg_quote(needle) + ')', 'ig'),
                '<mark>$1</mark>'
            )
        },
        rowClassCB: function(data, index) {
            return (index % 2) === 0 ? 'odd' : 'even'
        },
        onCellClicked (data, field, event) {
            console.log('cellClicked', field.name)
            if (field.name !== '__actions') {
                this.$refs.vuetable.toggleDetailRow(data.id)
            }
        },
        onCellDoubleClicked (data, field, event) {
            console.log('cellDoubleClicked:', field.name)
        },
        onLoadSuccess (response) {
            // set pagination data to pagination-info component
            this.$refs.paginationInfo.setPaginationData(response.data)

            let data = response.data.data
            if (this.searchFor !== '') {
                for (let n in data) {

                    //search capacity
                    data[n].item_id = this.highlight(this.searchFor, data[n].item_id)
                    data[n].name = this.highlight(this.searchFor, data[n].name)
                    data[n].description = this.highlight(this.searchFor, data[n].description)
                }
            }
        },
        onLoadError (response) {
            if (response.status == 400) {
                sweetAlert('Something\'s Wrong!', response.data.message, 'error')
            } else {
                sweetAlert('Oops', E_SERVER_ERROR, 'error')
            }
        },
        onPaginationData (tablePagination) {
            this.$refs.paginationInfo.setPaginationData(tablePagination)
            this.$refs.pagination.setPaginationData(tablePagination)
        },
        onChangePage (page) {
            this.$refs.vuetable.changePage(page)
        },
        registerEvents () {
            let self = this
            this.$on('vuetable:action', (action, data) => {
                self.onActions(action, data)
            })
            this.$on('vuetable:cell-clicked', (data, field, event) => {
                self.onCellClicked(data, field, event)
            })
            this.$on('vuetable:cell-dblclicked', (data, field, event) => {
                self.onCellDoubleClicked(data, field, event)
            })
            this.$on('vuetable:load-success', (response) => {
                self.onLoadSuccess(response)
            })
            this.$on('vuetable:load-error', (response) => {
                self.onLoadError(response)
            })
        }
    },
    created () {
        this.registerEvents();

        Bus.$on('payment-refresh', () => {

            this.$refs.vuetable.refresh();

        })
    }
})

//refunds

Vue.component('refund-actions', {
    template: [
        '<div>',
        '<button class="btn-xs btn-icon waves-effect waves-light btn-default" @click="onClickEditItem(rowData)"><i class="fa fa-edit"></i></button>',
        '&nbsp;<button class="btn-xs btn-icon waves-effect waves-light btn-danger" @click="onClickDeleteItem(rowData)"><i class="fa fa-remove"></i></button>',
        '</div>'
    ].join(''),
    props: {
        rowData: {
            type: Object,
            required: true
        }
    },
    methods: {
        onClickEditItem: function(data) {

            //Bus.$emit('item-edit',data.id);

        },

        onClickDeleteItem: function(data) {

            /*
             this.$http.delete(apiPaymentUrl + "/" + data.id).then((response) => {

             this.$parent.reload();

             sweetAlert({
             type: 'success',
             title: 'Payment Deleted',
             text: 'I will close in 2 seconds.',
             timer: 2000
             });

             }, (errorResponse) => {
             console.error("Error Posting to the server. Logout and login." + errorResponse);
             });
             */

        }

    }
})

Vue.component('refund-detail-row', {
    template: [
        '<div @click="onClick">',
        '<div class="inline field">',
        '<label>ID: </label>',
        '<span>{{rowData.id}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Credit Note No: </label>',
        '<span>{{rowData.credit_open_id}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Name: </label>',
        '<span>{{rowData.customer_name}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Note Total: </label>',
        '<span>{{rowData.credit_total}}</span>',
        '</div>',
        '<div class="inline field">',
        //'<label>Invoice Balance: </label>',
        //'<span>{{rowData.invoice_balance}}</span>',
        //'</div>',
        //'<div class="inline field">',
        '<label>Amount Paid (Local): </label>',
        '<span>{{rowData.amount_paid}}</span>',
        '</div>',
        '</div>'
    ].join(''),
    props: {
        rowData: {
            type: Object,
            required: true
        }
    },
    methods: {
        onClick: function(event) {
            console.log('my-detail-row: on-click', event.target)
        }
    },
})

let refundTableColumns = [
    {
        name: 'id',
        title: 'ID',
        sortField: 'id'
    },
    {
        name: 'credit_open_id',
        title: 'Credit Note ID',
        sortField: 'credit_open_id'
    },
    {
        name: 'customer_name',
        title: 'Name',
        sortField: 'customer_name',
        visible: true
    },
    {
        name: 'credit_total',
        title: 'Note Total',
        sortField: 'credit_total'
    },
    /*
     {
     name: 'invoice_balance',
     title: 'Invoice Balance',
     sortField: 'invoice_balance'
     },
     */
    {
        name: 'amount_paid',
        title: 'Amount Refunded',
        sortField: 'amount_paid'
    },
    {
        name: 'payment_type',
        title: 'Payment Type',
        sortField: 'payment_type'
    },
    {
        name: '__component:payment-actions',
        dataClass: 'center aligned'
    }
]

Vue.component('refund-table', {
    template: '#refund-table',
    components: {
        Vuetable,
        VuetablePagination,
        VuetablePaginationDropdown,
        VuetablePaginationInfo,
    },
    data: function() {
        return {
            loading: '',
            searchFor: '',
            moreParams: {},
            fields: refundTableColumns,
            sortOrder: [{
                field: 'open_id',
                direction: 'desc',
            }],
            multiSort: true,
            paginationComponent: 'vuetable-pagination',
            perPage: 10,
            paginationInfoTemplate: 'Showing record: {from} to {to} from {total} item(s)',
        }
    },
    watch: {
        'perPage' (val, oldVal) {
            this.$nextTick(function() {
                this.$refs.vuetable.refresh()
            })
        },
        'paginationComponent' (val, oldVal) {
            this.$nextTick(function() {
                this.$refs.pagination.setPaginationData(this.$refs.vuetable.tablePagination)
            })
        }
    },
    methods: {
        transform: function(data) {

            var pay_type_string;
            //var invoice_balance_cal;

            let transformed = {}
            transformed.pagination = {
                total: data.total,
                per_page: data.per_page,
                current_page: data.current_page,
                last_page: data.last_page,
                next_page_url: data.next_page_url,
                prev_page_url: data.prev_page_url,
                from: data.from,
                to: data.to
            }

            transformed.data = []
            data = data.data
            for (let i = 0; i < data.length; i++) {

                switch (data[i].payment_type_id){
                    case 1:
                        pay_type_string = 'ACH';
                        break;

                    case 2:
                        pay_type_string = 'Bank Transfer';
                        break;

                    case 3:
                        pay_type_string = 'Cash';
                        break;

                    case 4:
                        pay_type_string = 'Check';
                        break;

                    case 5:
                        pay_type_string = 'Credit Card';
                        break;

                    default:
                        pay_type_string = 'Online';

                }

                //invoice_balance_cal = data[i].invoice_tab.total_amount_local - data[i].amount_local;

                transformed['data'].push({
                    id: data[i].open_id,
                    credit_open_id: data[i].credit_tab.open_id,
                    customer_name: data[i].contact_tab.name,
                    credit_total: data[i].credit_tab.total_amount_local,
                    //invoice_balance: invoice_balance_cal,
                    amount_paid: data[i].amount_local,
                    payment_type: pay_type_string
                })
            }

            return transformed
        },
        showSettingsModal () {

            console.log('settings');
            $('#settingsModal').modal({
                detachable: false,
                onVisible: function() {
                    $('.ui.checkbox').checkbox()
                }
            }).modal('show')
        },
        showLoader: function() {
            this.loading = 'loading'
        },
        hideLoader: function() {
            this.loading = ''
        },
        allCap (value) {
            return value.toUpperCase()
        },
        getFieldTitle: function(field) {
            if (field.title !== '') return field.title

            if (field.name.slice(0, 2) === '__') {
                return field.name.indexOf(':') >= 0
                    ? field.name.split(':')[1]
                    : fiel.name.replace('__', '')
            }
        },
        showDetailRow: function(value) {
            let icon = this.$refs.vuetable.isVisibleDetailRow(value) ? 'down' : 'right'
            return [
                '<a class="show-detail-row">',
                '<i class="chevron circle ' + icon + ' icon"></i>',
                '</a>'
            ].join('')
        },
        displayImage (value) {
            return '<img src="'+ value +'" alt="inventory-img" title="inventory-img" class="img-circle thumb-sm" />'
        },
        setFilter: function() {
            this.moreParams = {
                'filter': this.searchFor
            }
            this.$nextTick(function() {
                this.$refs.vuetable.refresh()
            })
        },
        resetFilter: function() {
            this.searchFor = ''
            this.setFilter()
        },
        preg_quote: function( str ) {
            // http://kevin.vanzonneveld.net
            // +   original by: booeyOH
            // +   improved by: Ates Goral (http://magnetiq.com)
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   bugfixed by: Onno Marsman
            // *     example 1: preg_quote("$40");
            // *     returns 1: '\$40'
            // *     example 2: preg_quote("*RRRING* Hello?");
            // *     returns 2: '\*RRRING\* Hello\?'
            // *     example 3: preg_quote("\\.+*?[^]$(){}=!<>|:");
            // *     returns 3: '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'

            return (str+'').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
        },
        highlight: function(needle, haystack) {
            return haystack.replace(
                new RegExp('(' + this.preg_quote(needle) + ')', 'ig'),
                '<mark>$1</mark>'
            )
        },
        rowClassCB: function(data, index) {
            return (index % 2) === 0 ? 'odd' : 'even'
        },
        onCellClicked (data, field, event) {
            console.log('cellClicked', field.name)
            if (field.name !== '__actions') {
                this.$refs.vuetable.toggleDetailRow(data.id)
            }
        },
        onCellDoubleClicked (data, field, event) {
            console.log('cellDoubleClicked:', field.name)
        },
        onLoadSuccess (response) {
            // set pagination data to pagination-info component
            this.$refs.paginationInfo.setPaginationData(response.data)

            let data = response.data.data
            if (this.searchFor !== '') {
                for (let n in data) {

                    //search capacity
                    data[n].item_id = this.highlight(this.searchFor, data[n].item_id)
                    data[n].name = this.highlight(this.searchFor, data[n].name)
                    data[n].description = this.highlight(this.searchFor, data[n].description)
                }
            }
        },
        onLoadError (response) {
            if (response.status == 400) {
                sweetAlert('Something\'s Wrong!', response.data.message, 'error')
            } else {
                sweetAlert('Oops', E_SERVER_ERROR, 'error')
            }
        },
        onPaginationData (tablePagination) {
            this.$refs.paginationInfo.setPaginationData(tablePagination)
            this.$refs.pagination.setPaginationData(tablePagination)
        },
        onChangePage (page) {
            this.$refs.vuetable.changePage(page)
        },
        registerEvents () {
            let self = this
            this.$on('vuetable:action', (action, data) => {
                self.onActions(action, data)
            })
            this.$on('vuetable:cell-clicked', (data, field, event) => {
                self.onCellClicked(data, field, event)
            })
            this.$on('vuetable:cell-dblclicked', (data, field, event) => {
                self.onCellDoubleClicked(data, field, event)
            })
            this.$on('vuetable:load-success', (response) => {
                self.onLoadSuccess(response)
            })
            this.$on('vuetable:load-error', (response) => {
                self.onLoadError(response)
            })
        }
    },
    created () {
        this.registerEvents();

        Bus.$on('payment-refresh', () => {

            this.$refs.vuetable.refresh();

        })
    }
})


//credit notes

Vue.component('creditnote-actions', {
    template: [
        '<div>',
        '<button class="btn-xs btn-icon waves-effect waves-light btn-default" @click="onClickViewCreditNote(rowData)"><i class="fa fa-eye"></i></button>',
        '&nbsp;<button class="btn-xs btn-icon waves-effect waves-light btn-danger" @click="onClickDeleteCreditNote(rowData)"><i class="fa fa-remove"></i></button>',
        '</div>'
    ].join(''),
    props: {
        rowData: {
            type: Object,
            required: true
        }
    },
    methods: {
        onClickViewCreditNote: function(data) {
            //console.log('actions: on-click', data.name)
            //sweetAlert(action, data.name)
            window.location.href = creditnoteUrl + '/' + data.id;

        },

        onClickDeleteCreditNote: function(data) {

            /*
             this.$http.delete(apiInvoiceUrl + '/' + data.id ).then((response) => {

             this.$parent.reload();

             sweetAlert({
             type: 'success',
             title: 'Invoice Deleted',
             text: 'I will close in 2 seconds.',
             timer: 2000
             });


             }, (errorResponse) => {
             console.error("Error Posting to the server " + errorResponse);
             })
             //console.log(response);
             this.$parent.reload();
             */

            //start

            sweetAlert({
                title: 'Are you sure?',
                text: "You won't be able to revert this! This will also delete all the associated payments & transactions.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function () {


                this.$http.delete(apiCreditNoteUrl + '/' + data.id ).then((response) => {

                    this.$parent.reload();

                    sweetAlert({
                        type: 'success',
                        title: 'Deleted!',
                        text: 'Your credit note "'+ data.id + '" has been deleted.',
                        timer: 2000
                    });

                }, (errorResponse) => {
                    console.error("Error Posting to the server. Logout and login." + errorResponse);
                });



            }.bind(this))

            this.$parent.reload();

            //end


        }
    }
})

Vue.component('creditnote-detail-row', {
    template: [
        '<div @click="onClick">',
        '<div class="inline field">',
        '<label>Credit Note Number: </label>',
        '<span>{{rowData.id}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Status: </label>',
        '<span>{{rowData.status_id}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Due Date: </label>',
        '<span>{{rowData.due_date}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Refund Date: </label>',
        '<span>{{rowData.post_date}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Customer: </label>',
        '<span>{{rowData.contact_id}}</span>',
        '</div>',
        '</div>'
    ].join(''),
    props: {
        rowData: {
            type: Object,
            required: true
        }
    },
    methods: {
        onClick: function(event) {
            console.log('invoice-detail-row: on-click', event.target)
        }
    },
})

let creditnoteTableColumns = [
    {
        name: 'id',
        title: '#',
        sortField: 'id',
        visible: true
    },
    {
        name: 'status_id',
        title: 'Status',
        sortField: 'status_id'
    },
    {
        name: 'due_date',
        title: 'Due',
        sortField: 'due_date',
        visible: true
    },
    {
        name: 'post_date',
        title: 'Refunded On',
        sortField: 'post_date'
    },
    {
        name: 'contact_id',
        title: 'Customer',
        sortField: 'contact_id'
    },
    {
        name: 'total',
        title: 'Total',
        sortField: 'total'
    },
    {
        name: 'balance',
        title: 'Balance',
        sortField: 'balance'
    },
    {
        name: '__component:creditnote-actions',
        dataClass: 'center aligned'
    }
]

Vue.component('creditnote-table', {
    template: '#creditnote-table',
    components: {
        Vuetable,
        VuetablePagination,
        VuetablePaginationDropdown,
        VuetablePaginationInfo,
    },
    data: function() {
        return {
            loading: '',
            searchFor: '',
            moreParams: {},
            fields: creditnoteTableColumns,
            sortOrder: [{
                field: 'open_id',
                direction: 'desc',
            }],
            multiSort: true,
            paginationComponent: 'vuetable-pagination',
            perPage: 10,
            paginationInfoTemplate: 'Showing record: {from} to {to} from {total} item(s)',
        }
    },
    watch: {
        'perPage' (val, oldVal) {
            this.$nextTick(function() {
                this.$refs.vuetable.refresh()
            })
        },
        'paginationComponent' (val, oldVal) {
            this.$nextTick(function() {
                this.$refs.pagination.setPaginationData(this.$refs.vuetable.tablePagination)
            })
        }
    },
    methods: {
        transform: function(data) {
            let transformed = {}
            transformed.pagination = {
                total: data.total,
                per_page: data.per_page,
                current_page: data.current_page,
                last_page: data.last_page,
                next_page_url: data.next_page_url,
                prev_page_url: data.prev_page_url,
                from: data.from,
                to: data.to
            }

            transformed.data = []
            data = data.data
            for (let i = 0; i < data.length; i++) {
                transformed['data'].push({
                    id: data[i].open_id,
                    status_id: data[i].status_tab.text,
                    due_date: data[i].due_date,
                    post_date: data[i].post_date,
                    contact_id: data[i].contact_tab.name,
                    total: data[i].total_amount,
                    balance: data[i].balance
                    //address: data[i].address.line1 + ' ' + data[i].address.line2 + ' ' + data[i].address.zipcode
                })
            }

            return transformed
        },
        showSettingsModal () {
            $('#settingsModal').modal({
                detachable: false,
                onVisible: function() {
                    $('.ui.checkbox').checkbox()
                }
            }).modal('show')
        },
        showLoader: function() {
            this.loading = 'loading'
        },
        hideLoader: function() {
            this.loading = ''
        },
        allCap (value) {
            return value.toUpperCase()
        },
        getFieldTitle: function(field) {
            if (field.title !== '') return field.title

            if (field.name.slice(0, 2) === '__') {
                return field.name.indexOf(':') >= 0
                    ? field.name.split(':')[1]
                    : fiel.name.replace('__', '')
            }
        },
        formatDate (value, fmt) {
            if (value === null) return ''
            fmt = (typeof(fmt) === 'undefined') ? 'D MMM YYYY' : fmt
            return moment(value, 'YYYY-MM-DD').format(fmt)
        },
        gender (value) {
            return value === 'M'
                ? '<span class="ui teal label"><i class="male icon"></i>Male</span>'
                : '<span class="ui pink label"><i class="female icon"></i>Female</span>'
        },
        showDetailRow: function(value) {
            let icon = this.$refs.vuetable.isVisibleDetailRow(value) ? 'down' : 'right'
            return [
                '<a class="show-detail-row">',
                '<i class="chevron circle ' + icon + ' icon"></i>',
                '</a>'
            ].join('')
        },
        setFilter: function() {
            this.moreParams = {
                'filter': this.searchFor
            }
            this.$nextTick(function() {
                this.$refs.vuetable.refresh()
            })
        },
        resetFilter: function() {
            this.searchFor = ''
            this.setFilter()
        },
        preg_quote: function( str ) {
            // http://kevin.vanzonneveld.net
            // +   original by: booeyOH
            // +   improved by: Ates Goral (http://magnetiq.com)
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   bugfixed by: Onno Marsman
            // *     example 1: preg_quote("$40");
            // *     returns 1: '\$40'
            // *     example 2: preg_quote("*RRRING* Hello?");
            // *     returns 2: '\*RRRING\* Hello\?'
            // *     example 3: preg_quote("\\.+*?[^]$(){}=!<>|:");
            // *     returns 3: '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'

            return (str+'').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
        },
        highlight: function(needle, haystack) {
            return haystack.replace(
                new RegExp('(' + this.preg_quote(needle) + ')', 'ig'),
                '<mark>$1</mark>'
            )
        },
        rowClassCB: function(data, index) {
            return (index % 2) === 0 ? 'odd' : 'even'
        },
        onCellClicked (data, field, event) {
            console.log('cellClicked', field.name)
            if (field.name !== '__actions') {
                this.$refs.vuetable.toggleDetailRow(data.id)
            }
        },
        onCellDoubleClicked (data, field, event) {
            console.log('cellDoubleClicked:', field.name)
        },
        onLoadSuccess (response) {
            // set pagination data to pagination-info component
            this.$refs.paginationInfo.setPaginationData(response.data)

            let data = response.data.data
            if (this.searchFor !== '') {
                for (let n in data) {
                    data[n].contact.name = this.highlight(this.searchFor, data[n].contact.name)
                    data[n].user_invoice_status.text = this.highlight(this.searchFor, data[n].user_invoice_status.text)
                }
            }
        },
        onLoadError (response) {
            if (response.status == 400) {
                sweetAlert('Something\'s Wrong!', response.data.message, 'error')
            } else {
                sweetAlert('Oops', E_SERVER_ERROR, 'error')
            }
        },
        onPaginationData (tablePagination) {
            this.$refs.paginationInfo.setPaginationData(tablePagination)
            this.$refs.pagination.setPaginationData(tablePagination)
        },
        onChangePage (page) {
            this.$refs.vuetable.changePage(page)
        },
        registerEvents () {
            let self = this
            this.$on('vuetable:action', (action, data) => {
                self.onActions(action, data)
            })
            this.$on('vuetable:cell-clicked', (data, field, event) => {
                self.onCellClicked(data, field, event)
            })
            this.$on('vuetable:cell-dblclicked', (data, field, event) => {
                self.onCellDoubleClicked(data, field, event)
            })
            this.$on('vuetable:load-success', (response) => {
                self.onLoadSuccess(response)
            })
            this.$on('vuetable:load-error', (response) => {
                self.onLoadError(response)
            })
        }
    },
    created () {
        this.registerEvents()
    }
})


//recurring

Vue.component('recurring-actions', {
    template: [
        '<div>',
        '<button class="btn-xs btn-icon waves-effect waves-light btn-default" @click="onClickEditRecInv(rowData)"><i class="fa fa-edit"></i></button>',
        '&nbsp;<button class="btn-xs btn-icon waves-effect waves-light btn-danger" @click="onClickDeleteRecInv(rowData)"><i class="fa fa-remove"></i></button>',
        '</div>'
    ].join(''),
    props: {
        rowData: {
            type: Object,
            required: true
        }
    },
    methods: {
        onClickEditRecInv: function(data) {

            //Bus.$emit('item-edit',data.id);

        },

        onClickDeleteRecInv: function(data) {


            this.$http.delete(apiRecInvUrl + "/" + data.id).then((response) => {

                this.$parent.reload();

                sweetAlert({
                    type: 'success',
                    title: 'Recurring Invoice Deleted',
                    text: 'I will close in 2 seconds.',
                    timer: 2000
                });

            }, (errorResponse) => {
                console.error("Error Posting to the server. Logout and login." + errorResponse);
            });


        }

    }
})

Vue.component('recurring-detail-row', {
    template: [
        '<div @click="onClick">',
        '<div class="inline field">',
        '<label>ID: </label>',
        '<span>{{rowData.id}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Invoice Number: </label>',
        '<span>{{rowData.status_id}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Invoice Total: </label>',
        '<span>{{rowData.start_date}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Name: </label>',
        '<span>{{rowData.ends}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Invoice Balance: </label>',
        '<span>{{rowData.invoices_created}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Amount Paid (Local): </label>',
        '<span>{{rowData.last_sent}}</span>',
        '</div>',
        '</div>'
    ].join(''),
    props: {
        rowData: {
            type: Object,
            required: true
        }
    },
    methods: {
        onClick: function(event) {
            console.log('my-detail-row: on-click', event.target)
        }
    },
})

let recurringTableColumns = [
    {
        name: 'id',
        title: 'ID',
        sortField: 'id'
    },
    {
        name: 'status_id',
        title: 'Status',
        sortField: 'status_id'
    },
    {
        name: 'frequency',
        title: 'Frequency',
        sortField: 'frequency',
        visible: true
    },
    {
        name: 'start_date',
        title: 'Start Date',
        sortField: 'start_date'
    },

    {
        name: 'ends',
        title: 'Ends',
        sortField: 'ends'
    },
    {
        name: 'invoices_created',
        title: 'Invoices Created',
        sortField: 'invoices_created'
    },
    {
        name: 'last_sent',
        title: 'Last Sent',
        sortField: 'last_sent'
    },
    {
        name: '__component:recurring-actions',
        dataClass: 'center aligned'
    }
]

Vue.component('recurring-table', {
    template: '#recurring-table',
    components: {
        Vuetable,
        VuetablePagination,
        VuetablePaginationDropdown,
        VuetablePaginationInfo,
    },
    data: function() {
        return {
            loading: '',
            searchFor: '',
            moreParams: {},
            fields: recurringTableColumns,
            sortOrder: [{
                field: 'id',
                direction: 'desc',
            }],
            multiSort: true,
            paginationComponent: 'vuetable-pagination',
            perPage: 10,
            paginationInfoTemplate: 'Showing record: {from} to {to} from {total} item(s)',
        }
    },
    watch: {
        'perPage' (val, oldVal) {
            this.$nextTick(function() {
                this.$refs.vuetable.refresh()
            })
        },
        'paginationComponent' (val, oldVal) {
            this.$nextTick(function() {
                this.$refs.pagination.setPaginationData(this.$refs.vuetable.tablePagination)
            })
        }
    },
    methods: {
        transform: function(data) {

            var recurring_status, recurring_frequency, recurring_ends, recurring_last;
            //var invoice_balance_cal;

            let transformed = {}
            transformed.pagination = {
                total: data.total,
                per_page: data.per_page,
                current_page: data.current_page,
                last_page: data.last_page,
                next_page_url: data.next_page_url,
                prev_page_url: data.prev_page_url,
                from: data.from,
                to: data.to
            }

            transformed.data = []
            data = data.data
            for (let i = 0; i < data.length; i++) {

                switch (data[i].status_id){
                    case 1:
                        recurring_status = 'Active';
                        break;

                    case 2:
                        recurring_status = 'Disabled';
                        break;

                    default:
                        recurring_status = 'Error';
                }

                switch (data[i].freq_type){
                    case 1:
                        recurring_frequency = data[i].freq + ' Days';
                        break;
                    case 2:
                        recurring_frequency = data[i].freq + ' Weeks';
                        break;
                    case 3:
                        recurring_frequency = data[i].freq + ' Months';
                        break;

                    default:
                        recurring_frequency = 'Error';
                }

                switch (data[i].ends){
                    case 1:
                        recurring_ends = 'Never';
                        break;
                    case 2:
                        recurring_ends = 'On ' + data[i].end_date;
                        break;
                    case 3:
                        recurring_ends = 'After ' + data[i].end_after_no_inv + ' Invoices';
                        break;

                    default:
                        recurring_ends = 'Error';
                }

                if(data[i].last_sent_date)
                {
                    recurring_last = data[i].last_sent_date;
                }
                else {
                    recurring_last = 'Never';
                }

                transformed['data'].push({
                    id: data[i].open_id,
                    status_id: recurring_status,
                    frequency: recurring_frequency,
                    start_date: data[i].start_date,
                    ends: recurring_ends,
                    invoices_created: data[i].invoices_created,
                    last_sent: recurring_last,
                })
            }

            return transformed
        },
        showSettingsModal () {

            console.log('settings');
            $('#settingsModal').modal({
                detachable: false,
                onVisible: function() {
                    $('.ui.checkbox').checkbox()
                }
            }).modal('show')
        },
        showLoader: function() {
            this.loading = 'loading'
        },
        hideLoader: function() {
            this.loading = ''
        },
        allCap (value) {
            return value.toUpperCase()
        },
        getFieldTitle: function(field) {
            if (field.title !== '') return field.title

            if (field.name.slice(0, 2) === '__') {
                return field.name.indexOf(':') >= 0
                    ? field.name.split(':')[1]
                    : fiel.name.replace('__', '')
            }
        },
        showDetailRow: function(value) {
            let icon = this.$refs.vuetable.isVisibleDetailRow(value) ? 'down' : 'right'
            return [
                '<a class="show-detail-row">',
                '<i class="chevron circle ' + icon + ' icon"></i>',
                '</a>'
            ].join('')
        },
        displayImage (value) {
            return '<img src="'+ value +'" alt="inventory-img" title="inventory-img" class="img-circle thumb-sm" />'
        },
        setFilter: function() {
            this.moreParams = {
                'filter': this.searchFor
            }
            this.$nextTick(function() {
                this.$refs.vuetable.refresh()
            })
        },
        resetFilter: function() {
            this.searchFor = ''
            this.setFilter()
        },
        preg_quote: function( str ) {
            // http://kevin.vanzonneveld.net
            // +   original by: booeyOH
            // +   improved by: Ates Goral (http://magnetiq.com)
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   bugfixed by: Onno Marsman
            // *     example 1: preg_quote("$40");
            // *     returns 1: '\$40'
            // *     example 2: preg_quote("*RRRING* Hello?");
            // *     returns 2: '\*RRRING\* Hello\?'
            // *     example 3: preg_quote("\\.+*?[^]$(){}=!<>|:");
            // *     returns 3: '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'

            return (str+'').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
        },
        highlight: function(needle, haystack) {
            return haystack.replace(
                new RegExp('(' + this.preg_quote(needle) + ')', 'ig'),
                '<mark>$1</mark>'
            )
        },
        rowClassCB: function(data, index) {
            return (index % 2) === 0 ? 'odd' : 'even'
        },
        onCellClicked (data, field, event) {
            console.log('cellClicked', field.name)
            if (field.name !== '__actions') {
                this.$refs.vuetable.toggleDetailRow(data.id)
            }
        },
        onCellDoubleClicked (data, field, event) {
            console.log('cellDoubleClicked:', field.name)
        },
        onLoadSuccess (response) {
            // set pagination data to pagination-info component
            this.$refs.paginationInfo.setPaginationData(response.data)

            let data = response.data.data
            if (this.searchFor !== '') {
                for (let n in data) {

                    //search capacity
                    data[n].item_id = this.highlight(this.searchFor, data[n].item_id)
                    data[n].name = this.highlight(this.searchFor, data[n].name)
                    data[n].description = this.highlight(this.searchFor, data[n].description)
                }
            }
        },
        onLoadError (response) {
            if (response.status == 400) {
                sweetAlert('Something\'s Wrong!', response.data.message, 'error')
            } else {
                sweetAlert('Oops', E_SERVER_ERROR, 'error')
            }
        },
        onPaginationData (tablePagination) {
            this.$refs.paginationInfo.setPaginationData(tablePagination)
            this.$refs.pagination.setPaginationData(tablePagination)
        },
        onChangePage (page) {
            this.$refs.vuetable.changePage(page)
        },
        registerEvents () {
            let self = this
            this.$on('vuetable:action', (action, data) => {
                self.onActions(action, data)
            })
            this.$on('vuetable:cell-clicked', (data, field, event) => {
                self.onCellClicked(data, field, event)
            })
            this.$on('vuetable:cell-dblclicked', (data, field, event) => {
                self.onCellDoubleClicked(data, field, event)
            })
            this.$on('vuetable:load-success', (response) => {
                self.onLoadSuccess(response)
            })
            this.$on('vuetable:load-error', (response) => {
                self.onLoadError(response)
            })
        }
    },
    created () {

    }
})