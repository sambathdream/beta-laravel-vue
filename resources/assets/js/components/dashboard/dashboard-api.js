const configApiUrl = base_api_path + "api/config"; //don't add '/' in the end
const tourApiUrl = base_api_path + "api/boottours"; //don't add '/' in the end

Vue.component('dashboard-api', {
    props: ['user'],

    data() {
        return {

            localization: {
                currency_id: 840, //USD
                language_id: 1, //english
                timezone: 'Etc/Greenwich', //UTC
                date_time_format: 5, //Dec 31, 2017
                week_first_day: 1, //monday
                year_first_month: 1, //janaury
                first_time: false
            },

            company_details: {

                billing_address: '',
                billing_address_line_2: '',
                billing_city: '',
                billing_state: '',
                billing_zip: '',
                billing_country: ''
            },

            tour: {
                tour_status: false
            }

        };
    },

    methods: {

        showConfig (config_id) {


            this.$http.get(configApiUrl + '/' + config_id).then((response) => {

                switch(config_id) {

                    case 2:

                        if(response.data.settings.currency_id) {
                            this.localization.currency_id = response.data.settings.currency_id;
                        }
                        var currency_id = this.localization.currency_id; //at the moment single language
                        $('#currency_id').val(currency_id).change();

                        if(response.data.settings.language_id) {
                            this.localization.language_id = response.data.settings.language_id;
                        }
                        var language_id = this.localization.language_id; //at the moment single language
                        $('#language_id').val(language_id).change();

                        if(response.data.settings.timezone) {
                            this.localization.timezone = response.data.settings.timezone;
                        }
                        var timezone = this.localization.timezone;
                        $('#timezone').val(timezone).change();

                        if(response.data.settings.date_time_format) {
                            this.localization.date_time_format = response.data.settings.date_time_format;
                        }
                        var date_time_format = this.localization.date_time_format;
                        $('#date_time_format').val(date_time_format).change();

                        if(response.data.settings.week_first_day) {
                            this.localization.week_first_day = response.data.settings.week_first_day;
                        }
                        var week_first_day = this.localization.week_first_day;
                        $('#week_first_day').val(week_first_day).change();

                        if(response.data.settings.year_first_month) {
                            this.localization.year_first_month = response.data.settings.year_first_month;
                        }
                        var year_first_month = this.localization.year_first_month;
                        $('#year_first_month').val(year_first_month).change();

                        break;

                }

            });
        },

        callConfigStoreApi (config_id, postBody) {

            this.$http.put(configApiUrl + '/' + config_id, postBody).then((response) => {
                //this.contacts.push(postBody);
                //console.log(response);
            }, (errorResponse) => {
                console.error("Error Posting to the server " + errorResponse);
            });

        },

        callLocationSearch () {

            this.$http.get(base_api_path + 'api/searchlocation').then((response) => {

                //console.log(response);

                this.localization.currency_id = response.data.country.id;
                this.localization.timezone    = response.data.location.timezone;
                this.company_details.billing_city     = response.data.location.city;
                this.company_details.billing_state    = response.data.location.state_name;
                this.company_details.billing_zip      = response.data.location.postal_code;
                this.company_details.billing_country  = response.data.location.iso_code;


            }, (errorResponse) => {
                console.error("Error Posting to the server " + errorResponse);
            });

        },

        updateConfig(id) {

            let postBody;

            switch(id) {

                case 2:
                    /*
                    var currency_id = $('#currency_id').val();
                    var language_id = $('#language_id').val();
                    var timezone = $('#timezone').val();
                    var date_time_format = $('#date_time_format').val();
                    var week_first_day = $('#week_first_day').val();
                    var year_first_month = $('#year_first_month').val();

                    this.localization.currency_id = currency_id;
                    this.localization.language_id = language_id;
                    this.localization.timezone = timezone;
                    this.localization.date_time_format = date_time_format;
                    this.localization.week_first_day = week_first_day;
                    this.localization.year_first_month = year_first_month;
                    */
                    this.localization.first_time = false;

                    postBody = this.localization;

                    $('#con-addr-modal').modal('show');
                    $('#con-close-modal').modal('hide');

                    break;

                case 5:

                    postBody = this.company_details;

                    $('#con-addr-modal').modal('hide');

                    Bus.$emit('startTour');

                    break;

            }

            this.callConfigStoreApi(id, postBody);
        }

    },

    mounted() {
        //console.log(Spark.state.currentTeam.name);
            if(first_time) {
                this.showConfig(2); //localization = 2
                this.callLocationSearch();
            }

    },
    created () {

        Bus.$on('closeTour',  (tour_id) => {

            console.log('close event');
            this.tour.tour_status = true;

            this.$http.put(tourApiUrl + '/' + tour_id, this.tour).then((response) => {
                //this.contacts.push(postBody);
                console.log(response);
            }, (errorResponse) => {
                console.error("Error Posting to the server " + errorResponse);
            });

        });
    }
});
