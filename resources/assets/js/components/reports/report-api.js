const reportApiUrl = base_api_path + "api/displayreport"; //don't add '/' in the end

import Datepicker from 'vuejs-datepicker';

Vue.component('report-api', {
    props: ['user', 'team'],
    components: {
        Datepicker
    },
    data() {
        return {
            report_type: '1',

            new_report: {
                type: '1',
                cmd: 'download',
                from_date: moment().subtract(30, 'days').format('YYYY-MM-DD'),
                to_date: moment().format('YYYY-MM-DD')
            }
            //inv_from_date: moment().subtract(30, 'days').format('YYYY-MM-DD'),
            //inv_to_date: moment().format('YYYY-MM-DD')

        }
    },
    mounted: function () {

    },
    methods: {
        downloadReport: function () {

            this.new_report.type = this.report_type;

            this.new_report.from_date = $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
            this.new_report.to_date = $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');

            this.$http.post(reportApiUrl, this.new_report).then((response) => {

                sweetAlert({
                    type: 'success',
                    title: 'Report Generated',
                    html:
                    '<a href="/storage/temp/reports/' + response.data.report_id + '">Click To Download</a>',
                    showCloseButton: true
                });

            },(errorResponse) => {
                console.error("Error Posting to the server " + errorResponse);
            });

            //$('#add-contact-modal').modal('hide');

        }


    },
    computed: {


    }


});
