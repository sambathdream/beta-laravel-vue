const contactApiUrl = base_api_path + "api/contacts"; //don't add '/' in the end
const configApiUrl = base_api_path + "api/config"; //don't add '/' in the end

const tourApiUrl = base_api_path + "api/boottours"; //don't add '/' in the end

Vue.component('contact-api', {
    props: ['user'],
    data() {
        return {

            //contact related
            new_contact: {

                id: '',
                open_id: '',

                name: '',
                email: '',
                emails: [{
                    id: ''
                }],

                phone: '',
                contact1: '',
                contact2: '',

                currency_id: '', //USD
                payment_terms: 15, //Due in 15 Days

                bill_address1: '',
                bill_address2: '',
                bill_city: '',
                bill_state: '',
                bill_postal_code: '',
                bill_country_id: '',

                copy_billing_addr: false,
                ship_phone: '',
                ship_contact: '',
                ship_address1: '',
                ship_address2: '',
                ship_city: '',
                ship_state: '',
                ship_postal_code: '',
                ship_country_id: '',
                instructions: '',

                account_no: '',
                id_no: '',
                vat_no: '',
                fax_no: '',
                mobile_no: '',
                toll_free_no: '',
                website: '',
                contact_image: '',
                gst_code: ''
            },

            tour: {
                tour_status: false
            }

        }
    },

    methods: {

        addEmails: function () {

            if(this.new_contact.emails.length > 4){

                return;
            }
            this.new_contact.emails.push({ id: '' });
        },

        removeEmailLine: function (items, index) {

            this.new_contact.emails.splice(index, 1);

        },

        showConfig (config_id) {

            this.$http.get(configApiUrl + '/' + config_id).then((response) => {

                switch(config_id) {

                    case 1:
                        
                        if(response.data.address.billing_country) {
                            this.new_contact.bill_country_id = response.data.address.billing_country;
                            $('#bill_country_id').val(this.new_contact.bill_country_id);
                            $('#bill_country_id').change();

                            this.new_contact.ship_country_id = response.data.address.billing_country;
                            $('#ship_country_id').val(this.new_contact.ship_country_id);
                            $('#ship_country_id').change();

                            this.new_contact.currency_id     = response.data.address.billing_country_id;
                            //if no currency then
                            // default billing address country currency
                        }

                        //this.invoice_form.invoice_from = [response.data.address.name,response.data.address.billing_address,response.data.address.billing_city,response.data.address.billing_state,response.data.address.billing_zip,response.data.address.billing_country].filter(val => val).join(', ')

                        //this.invoice_form.from_email = response.data.user_email;//this.user.email; //
                        break;

                    case 2:

                        if (response.data.settings.currency_id) {
                            this.new_contact.currency_id            = response.data.settings.currency_id;
                            //this.invoice_form.currency_id           = response.data.settings.currency_id;
                            //this.invoice_form.company_currency_id   = response.data.settings.currency_id;

                            //var currency_id                         = this.invoice_form.currency_id;
                            $('#currency_id').val(this.new_contact.currency_id);
                            $('#currency_id').change();

                            //this.currencyConvert.contact_country = response.data.currency_code;
                            //this.currencyConvert.company_country = response.data.currency_code;

                            //this.convertCurrency();

                        }

                        break;

                }


            });
        },

    /* Contact Page Code Start */
    //display contact
        showContactModal() {

            this.new_contact.id = '';
            this.new_contact.name = '';
            this.new_contact.email = '';

            this.new_contact.phone = '';
            this.new_contact.contact1 = '';
            this.new_contact.contact2 = '';

            //this.currency_id: 840, //USD
            //this.payment_terms: 15, //Due in 15 Days

            this.new_contact.bill_address1 = '';
            this.new_contact.bill_address2 = '';
            this.new_contact.bill_city = '';
            this.new_contact.bill_state = '';
            this.new_contact.bill_postal_code = '';
            //this.bill_country_id: 840,

            this.new_contact.copy_billing_addr = false;
            this.new_contact.ship_phone = '';
            this.new_contact.ship_contact = '';
            this.new_contact.ship_address1 = '';
            this.new_contact.ship_address2 = '';
            this.new_contact.ship_city = '';
            this.new_contact.ship_state = '';
            this.new_contact.ship_postal_code = '';
            //this.ship_country_id: 840,
            this.new_contact.instructions = '';

            this.new_contact.account_no = '';
            this.new_contact.id_no = '';
            this.new_contact.vat_no = '';
            this.new_contact.fax_no = '';
            this.new_contact.mobile_no = '';
            this.new_contact.toll_free_no = '';
            this.new_contact.website = '';
            this.new_contact.contact_image = '';

            //this.convertCurrency();

            $('#add-contact-modal').modal('show');

        },

        contactFields() {

            if (this.new_contact.copy_billing_addr == true) {

                this.new_contact.ship_phone         = this.new_contact.phone;
                this.new_contact.ship_contact       = this.new_contact.name;
                this.new_contact.ship_address1      = this.new_contact.bill_address1;
                this.new_contact.ship_address2      = this.new_contact.bill_address2;
                this.new_contact.ship_city          = this.new_contact.bill_city;
                this.new_contact.ship_state         = this.new_contact.bill_state;
                this.new_contact.ship_postal_code   = this.new_contact.bill_postal_code;
                this.new_contact.ship_country_id    = this.new_contact.bill_country_id;

            }

        },

        addContact() {

            this.contactFields();

            this.$validator.validateAll('contact_form').then(success => {
                if (!success) {
                    // handle error
                  return;
                }
                else {
                    this.$http.post(contactApiUrl, this.new_contact).then((response) => {

                        //this.new_contact.open_id = response.data;
                        //this.new_contact.id = response.data;
                        //console.log(response);

                        Bus.$emit('contact-refresh');
                        Bus.$emit('contactSearchQ', response.data); //so that new contact info gets populated auto

                    },(errorResponse) => {
                        console.error("Error Posting to the server " + errorResponse);
                    });

                    $('#add-contact-modal').modal('hide');

                    sweetAlert({
                        type: 'success',
                        title: 'Contact Created',
                        text: 'I will close in 2 seconds.',
                        timer: 2000
                    });
                }
            });
        },

        updateContact() {

            this.contactFields(); //call method to populate few not reactive fields
            this.$validator.validateAll().then(success => {

                if (!success) {
                    // handle error
                    return;
                }
                else {

                    var id = this.new_contact.id;

                    this.$http.put(contactApiUrl + '/' + id, this.new_contact).then((response) => {

                        console.log(response);

                        Bus.$emit('contact-refresh');

                        $('#add-contact-modal').modal('hide');

                        sweetAlert({
                            type: 'success',
                            title: 'Contact Updated',
                            text: 'I will close in 2 seconds.',
                            timer: 2000
                        });
                    },(errorResponse) => {
                        console.error("Error Posting to the server " + errorResponse);
                    });
                }

            });

        },


    },

    mounted () {

        this.showConfig(1); //address or company details
        this.showConfig(2); //localization

    },


    created () {

        Bus.$on('contact-edit',  (id) => {

            this.$http.get(contactApiUrl + '/' + id).then((response) => {

                this.new_contact = response.data;
                this.new_contact.id = id;
                this.new_contact.copy_billing_addr = false;

                this.new_contact.currency_id = response.data.currency_id;
                $('#currency_id').val(this.new_contact.currency_id).change();

                var temp = this.new_contact.payment_terms.toString();
                $('select[name=payment_terms]').val(temp);
                this.new_contact.payment_terms = response.data.payment_terms;

                this.new_contact.bill_country_id = response.data.bill_country_id;
                $('#bill_country_id').val(this.new_contact.bill_country_id).change();

                this.new_contact.ship_country_id = response.data.ship_country_id;
                $('#ship_country_id').val(this.new_contact.ship_country_id).change();

                $('.selectpicker').selectpicker('refresh');

            }, (errorResponse) =>{
                console.error("Error getting response from the server " + errorResponse);
            });

            $('#add-contact-modal').modal('show');

        });


        Bus.$on('closeTour',  (tour_id) => {

            //console.log('close event');
            this.tour.tour_status = true;

            this.$http.put(tourApiUrl + '/' + tour_id, this.tour).then((response) => {
                //this.contacts.push(postBody);
                //console.log(response);
            }, (errorResponse) => {
                console.error("Error Posting to the server " + errorResponse);
            });

        });

    }

});
