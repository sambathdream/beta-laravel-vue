
/*
 |--------------------------------------------------------------------------
 | Laravel Spark Components
 |--------------------------------------------------------------------------
 |
 | Here we will load the Spark components which makes up the core client
 | application. This is also a convenient spot for you to load all of
 | your components that you write while building your applications.
 */

import Vue from 'vue';
//import moment from 'moment';
import VeeValidate from 'vee-validate';

//import Multiselect from 'vue-multiselect';
//VeeValidate.installDateTimeValidators(moment);

Vue.use(VeeValidate, {
    fieldsBagName: 'whatever'
});

require('./../spark-components/bootstrap');

require('./home');

require('./datatables/all-data-tables');

require('./dashboard/dashboard-api');   //contact component

require('./contacts/contact-api');   //contact component

require('./cust-invoices/invoice-api');   //invoice component

require('./cust-invoices/invoice-create-api');   //invoice component
require('./cust-invoices/invoice-mod-api');   //invoice component
require('./cust-invoices/invoice-gst-api');   //invoice component
require('./cust-invoices/invoice-mod-gst-api');   //invoice component

require('./cust-invoices/design');   //invoice design component

require('./gateways/gateway-api');   //gateway component

require('./payments/payment-api');   //gateway component

require('./transactions/transaction-api');   //gateway component

require('./gl-accounts/glaccount-api');   //gateway component

require('./inventory/inventory-api');   //gateway component

require('./config/config-api');   //gateway component

require('./quotations/quotation-api');   //gateway component

require('./reports/report-api');   //report component

require('./temp/temp-api');   //temp component