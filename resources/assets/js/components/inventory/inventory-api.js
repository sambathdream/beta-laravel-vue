import hsntypeahead from './Typeahead-Gst.vue';
import multiselecttaxinv from 'vue-multiselect';

const apiItemUrl        = base_api_path + "api/inventory";
const apiTransactionUrl = base_api_path + "api/transactions";
const configApiUrl = base_api_path + "api/config"; //don't add '/' in the end

Vue.component('inventory-api', {
    props: ['user', 'team'],
    components: {
        hsntypeahead,
        multiselecttaxinv
    },
    data() {
        return {

            new_item: {
                id: '',
                item_id: '',
                name: '',
                description: '',
                qty: 0,
                buy_price: 0,
                sell_price: 0,
                category: '4',
                type: '1',
                image_url: '',
                debit_gl: '3',  //Inventory GL
                credit_gl: '3', //Inventory GL,

                hsnsac_code: '',
                hsnsac_type: '',
                cgst: 0,
                sgst: 0,
                igst: 0,
                gst: 0,

                gst_hsnsac_id: '',
                tax_items: []
            },

            new_txn: {
                category: 1,
                type: 'D',
                txn_date: '',//moment(date, 'YYYY-MM-DD').format('MM/DD/YYYY'),
                txn_amount: 0,
                description: '',
                debit_gl: '',
                credit_gl: ''
            },

            gst_hsnsac: {

                type: '',
                category: '',
                sub_category: '',
                service: '',
                code: '',
                description: '',
                cgst: 0,
                sgst: 0,
                igst: 0,
                gst: 0
            },

            tax_rates: [] //data will be initially loaded here

        }
    },
    mounted: function () {
        //  Vue.set(this.$data, 'form', _form);
    },
    methods: {

        showConfig (config_id) {

            this.$http.get(configApiUrl + '/' + config_id).then((response) => {

                switch(config_id) {

                    case 3:

                        this.tax_rates = response.data;
                        //this.invoice_form.tax_items = this.tax_rates;

                        break;

                }


            });
        },

        selectedTaxItems: function ()  {

           // this.new_item.tax_items =

        },

        showItemModal: function () {

            this.new_item.id            = '';
            this.new_item.item_id       = '';
            this.new_item.name          = '';
            this.new_item.description   = '';
            this.new_item.qty           = 0;
            this.new_item.buy_price     = 0;
            this.new_item.sell_price    = 0;
            this.new_item.category      = '4';
            this.new_item.type          = '1';

            $('#category').val(this.new_item.category).change();
            $('#type').val(this.new_item.type).change();

            //this.new_item.debit_gl = "";
            //this.new_item.credit_gl = "";

            this.showConfig(3);

            $('#add-product-modal').modal('show');
        },

        updateItem: function () {

            this.$validator.validateAll().then(success => {
                if (!success) {
                // handle error
                //console.log('errror in validation');
                return;
            }
            else {

                //console.log(apiItemUrl + '/' + this.new_item.id);

                    this.$http.put(apiItemUrl + '/' + this.new_item.id, this.new_item).then((response) => {

                    Bus.$emit('inventory-refresh');

                    $('#add-product-modal').modal('hide');

                    sweetAlert({
                        type: 'success',
                        title: 'Inventory Item # ' + this.new_item.item_id + ' Updated',
                        text: 'I will close in 2 seconds.',
                        timer: 2000
                    });

                }, (errorResponse) => {
                        console.error("Error Posting to the server " + errorResponse);
                    })


                }
            });


        },
        saveNewItem: function () {

            //var apiUrl = base_api_path + "api/inventory";

            this.$validator.validateAll('item_form').then(success => {
                if (!success) {
                    // handle error
                    return;
                }
                else {

                    this.$http.post(apiItemUrl, this.new_item).then((response) => {
                        //this.contacts.push(postBody);
                        //console.log(response);
                        Bus.$emit('inventory-refresh');

                        $('#add-product-modal').modal('hide');

                        sweetAlert({
                            type: 'success',
                            title: 'New Inventory Added',
                            text: 'I will close in 2 seconds.',
                            timer: 2000
                        });


                        this.postTransaction();


                    }, (errorResponse) => {
                        console.error("Error Posting to the server " + errorResponse);
                    })

                }
            });

        },

        postTransaction: function () {

            if(this.new_item.type === "1") {
                if(this.new_item.category === "1"){
                    this.new_txn.category  = "1";
                } else if(this.new_item.category === "2"){
                    this.new_txn.category  = "2";
                } else if(this.new_item.category === "3"){
                    this.new_txn.category  = "3";
                } else if(this.new_item.category === "4"){
                    this.new_txn.category  = "4";
                }
            } else

            if(this.new_item.type === "2") {
                if(this.new_item.category === "1"){
                    this.new_txn.category  = "5";
                } else if(this.new_item.category === "2"){
                    this.new_txn.category  = "6";
                } else if(this.new_item.category === "3"){
                    this.new_txn.category  = "7";
                } else if(this.new_item.category === "4"){
                    this.new_txn.category  = "8";
                }
            } else

            if(this.new_item.type === "3") {
                if(this.new_item.category === "1"){
                    this.new_txn.category  = "9";
                } else if(this.new_item.category === "2"){
                    this.new_txn.category  = "10";
                } else if(this.new_item.category === "3"){
                    this.new_txn.category  = "11";
                } else if(this.new_item.category === "4"){
                    this.new_txn.category  = "12";
                }
            } else

            if(this.new_item.type === "4") {
                this.new_txn.category  = "13";
            }

            //this.new_txn.type           = "D";
            this.new_txn.txn_date       = moment().format('YYYY-MM-DD');
            this.new_txn.txn_amount     = this.new_item.buy_price * this.new_item.qty;
            this.new_txn.description    = "Inventory Item";
            this.new_txn.debit_gl       = this.new_item.debit_gl; //will be posted immediately
            //this.new_txn.credit_gl      = ""; //will be posted during invoicing

            //console.log(this.new_txn);

            this.$http.post(apiTransactionUrl, this.new_txn).then((response) => {
                //console.log(response);

            }, (errorResponse) => {
                console.error("Error Posting to the server " + errorResponse);
            });

        }

    },
    computed: {


    },
    created () {

        Bus.$on('item-edit',  (id) => {

            //console.log('clicked edit 2' + id);
            this.$http.get(apiItemUrl + '/' + id).then((response) => {

                this.new_item.id            = response.data.open_id;

                this.new_item.item_id       = response.data.item_id;
                this.new_item.name          = response.data.name;

                this.new_item.description   = response.data.description;
                this.new_item.qty           = response.data.qty;
                this.new_item.buy_price     = response.data.buy_price;
                this.new_item.sell_price    = response.data.sell_price;
                this.new_item.image_url     = response.data.image_url;

                this.new_item.category      = response.data.category.toString();
                this.new_item.type          = response.data.type.toString();

                $('#category').val(this.new_item.category).change();
                $('#type').val(this.new_item.type).change();

                if(!response.data.debit_gl) {
                    response.data.debit_gl = "";
                }

                this.new_item.debit_gl = response.data.debit_gl.toString();
                $('#debit_gl').val(this.new_item.debit_gl).change();

                if(!response.data.credit_gl) {
                    response.data.credit_gl = "";
                }

                this.new_item.credit_gl = response.data.credit_gl.toString();
                $('#credit_gl').val(this.new_item.debit_gl).change();

                //console.log(response.data.inventoryTaxTab);
                //console.log(response.data);
                this.new_item.tax_items      = response.data.inventory_tax_tab;

                $('#add-product-modal').modal('show');


            }, (errorResponse) =>{
            console.error("Error getting response from the server " + errorResponse);
            });

        });

        Bus.$on('hsnsac-search',  (data) => {

            if(data) {

                //console.log(data);
                this.gst_hsnsac.gst_hsnsac_id   = data.id;
                this.gst_hsnsac.type            = data.type;
                this.gst_hsnsac.category        = data.category;
                this.gst_hsnsac.sub_category    = data.sub_category;
                this.gst_hsnsac.service         = data.service;
                this.gst_hsnsac.code            = data.code;
                this.gst_hsnsac.description     = data.description;
                this.gst_hsnsac.cgst            = data.cgst;
                this.gst_hsnsac.sgst            = data.sgst;
                this.gst_hsnsac.igst            = data.igst;
                this.gst_hsnsac.gst             = data.gst;

                this.new_item.gst_hsnsac_id     = data.id;
                this.new_item.hsnsac_type       = data.type;
                this.new_item.code              = data.code;
                this.new_item.cgst              = data.cgst;
                this.new_item.sgst              = data.sgst;
                this.new_item.igst              = data.igst;
                this.new_item.gst               = data.gst;

            }


        });

    }


});
