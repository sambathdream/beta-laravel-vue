const apiGLAccountUrl = base_api_path + "api/glaccounts";

Vue.component('glaccount-api', {
    props: ['user', 'team'],
    data() {
        return {
            new_glacc: {
                id: '',
                type: 1,
                name: '',
                description: '',
                int_balance: 0,
                ext_balance: 0
            }

        }
    },
    mounted: function () {
        //  Vue.set(this.$data, 'form', _form);
    },
    methods: {

        showGLModal: function () {

            //console.log('show');
            this.new_glacc.id = '';
            this.new_glacc.type = 1;
            this.new_glacc.name = '';
            this.new_glacc.description = '';
            this.new_glacc.int_balance = 0;
            this.new_glacc.ext_balance = 0;

            $('#add-gl-modal').modal('show');
        },

        saveNewGL: function () {

            this.$validator.validateAll().then(success => {
                if (!success) {
                // handle error
                console.log('errror in validation');
                return;
            }
            else {

                    this.$http.post(apiGLAccountUrl, this.new_glacc).then((response) => {
                        //this.contacts.push(postBody);
                        //console.log(response);
                        Bus.$emit('glaccount-refresh');
                        $('#add-gl-modal').modal('hide');
                        sweetAlert({
                            type: 'success',
                            title: 'GL Account Created',
                            text: 'I will close in 2 seconds.',
                            timer: 2000
                        });

                    }, (errorResponse) => {
                        console.error("Error Posting to the server " + errorResponse);
                    });

                }
            });
        },

        updateGL: function () {

            this.$validator.validateAll().then(success => {
                if (!success) {
                // handle error
                console.log('errror in validation');
                return;
            }
            else {

                this.$http.put(apiGLAccountUrl + '/' + this.new_glacc.id, this.new_glacc).then((response) => {
                    //this.contacts.push(postBody);
                    //console.log(response);
                    Bus.$emit('glaccount-refresh');
                $('#add-gl-modal').modal('hide');
                sweetAlert({
                    type: 'success',
                    title: 'GL Account Created',
                    text: 'I will close in 2 seconds.',
                    timer: 2000
                });

            }, (errorResponse) => {
                    console.error("Error Posting to the server " + errorResponse);
                });

            }
        });
        }

    },
    computed: {


    },
    created () {

        Bus.$on('glitem-edit',  (id) => {

            //console.log('clicked edit 2');

            this.$http.get(apiGLAccountUrl + '/' + id).then((response) => {

            //console.log(response.data);

            this.new_glacc = response.data;

        this.new_glacc.id = id;

        $('#add-gl-modal').modal('show');

        }, (errorResponse) =>{
                console.error("Error getting response from the server " + errorResponse);
            });

        });


        Bus.$on('updateTeams', function () {
            console.log('received');
        });


    }



});
