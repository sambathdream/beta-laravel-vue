const configApiUrl = base_api_path + "api/config"; //don't add '/' in the end

Vue.component('config-api', {

    props: ['user', 'team', 'billableType'],

    /**
     * The component's data.
     */
    data() {
        return {

            company_details: {

                name: '',
                id_number: '',
                vat_number: '',
                website: '',
                email: '',
                phone: '',
                inv_footer: '',
                inv_notes: '',

                email_subject: '',
                quote_footer: '',
                quote_notes: '',

                imageSrc: '',

                company_size: '',
                industry: '',

                invoice_prefix: '',
                quote_prefix: '',
                rec_invoice_prefix: '',
                line_tax: '',
                gst_enabled: '',
                gst_code: ''

            },

            company_address: {

                billing_address: '',
                billing_address_line_2: '',
                billing_city: '',
                billing_state: '',
                billing_zip: '',
                billing_country: ''
            },

            localization: {
                currency_id: 840, //USD
                language_id: 1, //english
                timezone: 'Etc/Greenwich', //UTC
                date_time_format: 5, //Dec 31, 2017
                week_first_day: 1, //monday
                year_first_month: 1
            },

            accounting: {
                paymentcr: '',
                paymentdb: '',
                processfeecr: '',
                processfeedb: '',
                invoicecr: '',
                invoicedb: '',
                productcr: '',
                productdb: '',
                manualcr: '',
                manualdb: '',
                taxcr: '',
                taxdb: ''
            },

            tax_rate: {
                name: '',
                rate: 0,
                tax_id: '',
                gl_account_open_id: 7,
                gl_account_name: '',
                tax_display: false, //display tax number on the invoice
                recoverable: false, //default false
                compound: false //default false
            },

            stripe_billing: {
                id: '',
                frequency: 3,
                signingsecret: '',
                webhook: '',
                auto_email: false,
                from_email: '',
                cc_email: ''

            },

            tax_rates_tax: [], //retrieved for all taxes

            tax_rates_ded: []  //retrieved for all deductibles

        };
    },


    /**
     * Prepare the component.
     */
    mounted() {
        this.showConfig(1);
    },


    methods: {

        previewThumbnail: function(event) {
            var input = event.target;

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                var vm = this;

                reader.onload = function(e) {
                    vm.company_details.imageSrc = e.target.result;
                }

                reader.readAsDataURL(input.files[0]);
            }
        },

        removeTaxRate (id,index) {

            this.tax_rates_tax.splice(index, 1);

            this.$http.delete(configApiUrl + "/" + id).then((response) => {

                sweetAlert({
                    type: 'success',
                    title: 'Tax Rate Deleted',
                    text: 'I will close in 2 seconds.',
                    timer: 2000
                });

            }, (errorResponse) => {
                console.error("Error Posting to the server. Logout and login." + errorResponse);
            });

        },

        removeDedRate (id,index) {

            this.tax_rates_ded.splice(index, 1);

            this.$http.delete(configApiUrl + "/" + id).then((response) => {

                sweetAlert({
                    type: 'success',
                    title: 'Deductible Rate Deleted',
                    text: 'I will close in 2 seconds.',
                    timer: 2000
                });

            }, (errorResponse) => {
                console.error("Error Posting to the server. Logout and login." + errorResponse);
            });

        },

        addTaxRate (config_id) {

            //console.log(this.tax_rate);

            this.$validator.validateAll('tax_rate').then(success => {

                if (!success) {
                    // handle error
                    return;
                } else {

                    this.$http.put(configApiUrl + '/' + config_id, this.tax_rate).then((response) => {

                        //this.tax_rates.push(this.tax_rate);
                        //tax_rates: [];
                        this.showConfig(config_id);

                        this.tax_rate.name = '';
                        this.tax_rate.rate = 0;
                        this.tax_rate.tax_id = '';
                        //this.tax_rate.gl_account_open_id = '';
                        //this.tax_rate.gl_account_name = '';
                        this.tax_rate.tax_display = false;
                        this.tax_rate.recoverable = false;
                        this.tax_rate.compound = false;

                        sweetAlert({
                            type: 'success',
                            title: 'Rate Added Successfully.',
                            text: 'I will close in 1 seconds.',
                            timer: 1000
                        });


                    }, (errorResponse) => {
                        console.error("Error Posting to the server " + errorResponse);
                    })

                }
            });

        },


        showConfig (config_id) {

            //console.log('test');

            this.$http.get(configApiUrl + '/' + config_id).then((response) => {

                switch(config_id) {

                    case 1:

                        this.company_details.name                   = response.data.address.name;

                        this.company_address.billing_address        = response.data.address.billing_address;
                        this.company_address.billing_address_line_2 = response.data.address.billing_address_line_2;
                        this.company_address.billing_city       = response.data.address.billing_city;
                        this.company_address.billing_state      = response.data.address.billing_state;
                        this.company_address.billing_zip        = response.data.address.billing_zip;
                        this.company_address.billing_country    = response.data.address.billing_country;

                        this.company_details.id_number  = response.data.settings.id_number;
                        this.company_details.vat_number = response.data.settings.vat_number;
                        this.company_details.website    = response.data.settings.website;
                        this.company_details.phone      = response.data.settings.phone;

                        this.company_details.inv_footer = response.data.settings.inv_footer;
                        this.company_details.inv_notes  = response.data.settings.inv_notes;
                        this.company_details.imageSrc   = response.data.settings.imageSrc;
                        this.company_details.email      = response.data.settings.email;

                        this.company_details.quote_footer    = response.data.settings.quote_footer;
                        this.company_details.quote_notes     = response.data.settings.quote_notes;
                        this.company_details.email_subject   = response.data.settings.email_subject;

                        this.company_details.company_size = response.data.settings.company_size;
                        $('#company_size').val(this.company_details.company_size).change();

                        this.company_details.industry   = response.data.settings.industry;
                        $('#industry').val(this.company_details.industry).change();

                        this.company_details.gst_code   = response.data.settings.gst_code;
                        this.company_details.line_tax   = response.data.settings.line_tax;

                        break;

                    case 2:

                        if(response.data.settings.currency_id) {
                            this.localization.currency_id = response.data.settings.currency_id;
                        }
                        var currency_id = this.localization.currency_id; //at the moment single language
                        $("#currency_id").select2("trigger", "select", {
                            data: { id: currency_id }
                        });

                        if(response.data.settings.language_id) {
                            this.localization.language_id = response.data.settings.language_id;
                        }
                        var language_id = this.localization.language_id; //at the moment single language
                        $("#language_id").select2("trigger", "select", {
                            data: { id: language_id }
                        });

                        if(response.data.settings.timezone) {
                            this.localization.timezone = response.data.settings.timezone;
                        }
                        var timezone = this.localization.timezone;
                        $("#timezone").select2("trigger", "select", {
                            data: { id: timezone }
                        });

                        if(response.data.settings.date_time_format) {
                            this.localization.date_time_format = response.data.settings.date_time_format;
                        }
                        var date_time_format = this.localization.date_time_format;
                        $("#date_time_format").select2("trigger", "select", {
                            data: { id: date_time_format }
                        });

                        if(response.data.settings.week_first_day) {
                            this.localization.week_first_day = response.data.settings.week_first_day;
                        }
                        var week_first_day = this.localization.week_first_day;
                        $("#week_first_day").select2("trigger", "select", {
                            data: { id: week_first_day }
                        });

                        if(response.data.settings.year_first_month) {
                            this.localization.year_first_month = response.data.settings.year_first_month;
                        }
                        var year_first_month = this.localization.year_first_month;
                        $("#year_first_month").select2("trigger", "select", {
                            data: { id: year_first_month }
                        });

                        break;

                    case 3:

                        this.tax_rates_tax = response.data;

                        break;

                    case 4:

                        //console.log(response);
                        this.accounting.paymentcr = response.data.paymentcr;
                        $("#paymentcr").select2("trigger", "select", {
                            data: { id: this.accounting.paymentcr }
                        });
                        this.accounting.paymentdb = response.data.paymentdb;
                        $("#paymentdb").select2("trigger", "select", {
                            data: { id: this.accounting.paymentdb }
                        });


                        this.accounting.processfeecr = response.data.processfeecr;
                        $("#processfeecr").select2("trigger", "select", {
                            data: { id: this.accounting.processfeecr }
                        });
                        this.accounting.processfeedb = response.data.processfeedb;
                        $("#processfeedb").select2("trigger", "select", {
                            data: { id: this.accounting.processfeedb }
                        });

                        this.accounting.invoicecr = response.data.invoicecr;
                        $("#invoicecr").select2("trigger", "select", {
                            data: { id: this.accounting.invoicecr }
                        });
                        this.accounting.invoicedb = response.data.invoicedb;
                        $("#invoicedb").select2("trigger", "select", {
                            data: { id: this.accounting.invoicedb }
                        });

                        this.accounting.productcr = response.data.productcr;
                        $("#productcr").select2("trigger", "select", {
                            data: { id: this.accounting.productcr }
                        });
                        this.accounting.productdb = response.data.productdb;
                        $("#productdb").select2("trigger", "select", {
                            data: { id: this.accounting.productdb }
                        });

                        this.accounting.manualcr = response.data.manualcr;
                        $("#manualcr").select2("trigger", "select", {
                            data: { id: this.accounting.manualcr }
                        });
                        this.accounting.manualdb = response.data.manualdb;
                        $("#manualdb").select2("trigger", "select", {
                            data: { id: this.accounting.manualdb }
                        });

                        this.accounting.taxcr = response.data.taxcr;
                        $("#taxcr").select2("trigger", "select", {
                            data: { id: this.accounting.taxcr }
                        });
                        this.accounting.taxdb = response.data.taxdb;
                        $("#taxdb").select2("trigger", "select", {
                            data: { id: this.accounting.taxdb }
                        });

                        break;

                    case 6:

                        this.stripe_billing.id              = response.data.id;
                        this.stripe_billing.frequency       = response.data.frequency;
                        this.stripe_billing.signingsecret   = response.data.signingsecret;
                        this.stripe_billing.webhook         = response.data.webhook;
                        this.stripe_billing.auto_email      = response.data.auto_email;
                        this.stripe_billing.from_email      = response.data.from_email;
                        this.stripe_billing.cc_email        = response.data.cc_email;

                        break;

                    case 7:

                        this.tax_rate.gl_account_open_id = 8;
                        
                        this.tax_rates_ded = response.data;

                        break;
                }


            });
        },

        callConfigStoreApi (config_id, postBody) {

            this.$validator.validateAll('company_details').then(success => {

                if (!success) {
                    // handle error
                    return;
                } else {

                    this.$http.put(configApiUrl + '/' + config_id, postBody).then((response) => {
                        //this.contacts.push(postBody);
                        //console.log(response);
                    }, (errorResponse) => {
                        console.error("Error Posting to the server " + errorResponse);
                    })
                }
            });

        },


        updateConfig(id) {

            let postBody;

            switch(id) {

                case 1:

                    var company_size = $('#company_size').val();
                    var industry = $('#industry').val();

                    this.company_details.company_size = company_size;
                    this.company_details.industry = industry;

                    postBody = this.company_details;

                    break;

                case 2:

                    var currency_id = $('#currency_id').val();
                    var language_id = $('#language_id').val();
                    var timezone = $('#timezone').val();
                    var date_time_format = $('#date_time_format').val();
                    var week_first_day = $('#week_first_day').val();
                    var year_first_month = $('#year_first_month').val();

                    this.localization.currency_id = currency_id;
                    this.localization.language_id = language_id;
                    this.localization.timezone = timezone;
                    this.localization.date_time_format = date_time_format;
                    this.localization.week_first_day = week_first_day;
                    this.localization.year_first_month = year_first_month;

                    postBody = this.localization;

                    break;

                case 4:

                    var paymentcr = $('#paymentcr').val();
                    var paymentdb = $('#paymentdb').val();
                    this.accounting.paymentcr = paymentcr;
                    this.accounting.paymentdb = paymentdb;

                    var processfeecr = $('#processfeecr').val();
                    var processfeedb = $('#processfeedb').val();
                    this.accounting.processfeecr = processfeecr;
                    this.accounting.processfeedb = processfeedb;

                    var invoicecr = $('#invoicecr').val();
                    var invoicedb = $('#invoicedb').val();
                    this.accounting.invoicecr = invoicecr;
                    this.accounting.invoicedb = invoicedb;

                    var taxcr = $('#taxcr').val();
                    var taxdb = $('#taxdb').val();
                    this.accounting.taxcr = taxcr;
                    this.accounting.taxdb = taxdb;

                    var productcr = $('#productcr').val();
                    var productdb = $('#productdb').val();
                    this.accounting.productcr = productcr;
                    this.accounting.productdb = productdb;

                    var manualcr = $('#manualcr').val();
                    var manualdb = $('#manualdb').val();
                    this.accounting.manualcr = manualcr;
                    this.accounting.manualdb = manualdb;

                    postBody = this.accounting;

                    break;

                case 5:

                    postBody = this.company_address;

                    break;

                case 6:

                    postBody = this.stripe_billing;


                    break;

            }

            this.callConfigStoreApi(id, postBody);
        }

    },

    computed: {

    }


});
