import Datepicker from 'vuejs-datepicker';

const apiTransactionUrl = base_api_path + "api/transactions";

Vue.component('transaction-api', {
    props: ['user', 'team'],
    components: {
        Datepicker
    },
    data() {
        return {

            new_txn: {
                category: 1,
                type: 'D',
                txn_date: '',//moment(date, 'YYYY-MM-DD').format('MM/DD/YYYY'),
                txn_amount: 0,
                description: '',
                debit_gl: '',
                credit_gl: ''
            }

        }
    },
    mounted: function () {
        //  Vue.set(this.$data, 'form', _form);
    },
    methods: {

        showTXNModal: function () {

            this.new_txn.category = 1;
            this.new_txn.type = 'D';
            this.new_txn.txn_date = moment().format('YYYY-MM-DD');
            this.new_txn.txn_amount = 0;
            this.new_txn.description = '';
            this.new_txn.debit_gl = '';
            this.new_txn.credit_gl = '';

            $('#add-txn-modal').modal('show');
        },

        saveNewItem: function () {

            //var apiUrl = base_api_path + "api/transactions";

            this.$validator.validateAll().then(success => {
                    if (!success) {
                        // handle error
                        console.log('errror in validation');
                        return;
                    }
                    else {

                        this.new_txn.txn_date    = moment(this.new_txn.txn_date).format('YYYY-MM-DD');

                        //console.log(this.new_txn);
                        this.$http.post(apiTransactionUrl, this.new_txn).then((response) => {
                            //this.contacts.push(postBody);
                            // console.log(response);

                            Bus.$emit('transaction-refresh');
                            $('#add-txn-modal').modal('hide');
                            sweetAlert({
                                type: 'success',
                                title: 'Transaction Posted',
                                text: 'I will close in 2 seconds.',
                                timer: 2000
                            });
                        }, (errorResponse) => {
                            console.error("Error Posting to the server " + errorResponse);
                        })
                    }
            });
        },

        txn_date_open: function () {
            this.$emit('input', newVal)
        }

    },
    computed: {


    },
    created () {

        Bus.$on('txn-edit',  (id) => {

            /*
            this.$http.get(apiItemUrl + '/' + id).then((response) => {

            }, (errorResponse) =>{
                console.error("Error getting response from the server " + errorResponse);
            });
            */

            sweetAlert({
                title: 'Not possible',
                text: "You can not edit the transaction. However, you can delete them.",
                type: 'error',
                showCancelButton: true,
                cancelButtonColor: '#d33'
            }).then(function () {


            }.bind(this))


        });

    }

});
