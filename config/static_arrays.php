<?php

class StaticArray
{

    public static $pay_type = [
        '1' => 'ACH',
        '2' => 'Bank Transfer',
        '3' => 'Cash',
        '4' => 'Check',
        '5' => 'Credit Card'
    ];

    public static $week_days = [
        '1' => 'Monday',
        '2' => 'Tuesday',
        '3' => 'Wednesday',
        '4' => 'Thursday',
        '5' => 'Friday',
        '6' => 'Saturday',
        '7' => 'Sunday'
    ];

    public static $year_months = [
        '1' => 'January',
        '2' => 'February',
        '3' => 'March',
        '4' => 'April',
        '5' => 'May',
        '6' => 'June',
        '7' => 'July',
        '8' => 'August',
        '9' => 'September',
        '10' => 'October',
        '11' => 'November',
        '12' => 'December'
    ];

    public static $company_size = [
        '0' => 'Not Specified',
        '1' => '1-10 Employees',
        '2' => '11-100 Employees',
        '3' => '101-1000 Employees',
        '4' => 'More than 1000 Employees'
    ];

    public static $industry = [

        '1' => 'Accounting & Legal',
        '2' => 'Advertising',
        '3' => 'Aerospace',
        '4' => 'Agriculture',
        '5' => 'Automotive',
        '6' => 'Banking & Finance',
        '7' => 'Biotechnology',
        '8' => 'Broadcasting',
        '9' => 'Business Services',
        '10' => 'Commodities & Chemicals',
        '11' => 'Communications',
        '12' => 'Computers & Hightech',
        '13' => 'Defense',
        '14' => 'Energy',
        '15' => 'Entertainment',
        '16' => 'Government',
        '17' => 'Healthcare & Life Sciences',
        '18' => 'Insurance',
        '19' => 'Manufacturing',
        '20' => 'Marketing',
        '21' => 'Media',
        '22' => 'Nonprofit & Higher Ed',
        '23' => 'Other',
        '24' => 'Pharmaceuticals',
        '25' => 'Photography',
        '26' => 'Professional Services & Consulting',
        '27' => 'Real Estate',
        '28' => 'Retail & Wholesale',
        '29' => 'Sports',
        '30' => 'Transportation',
        '31' => 'Travel & Luxury'

    ];

}