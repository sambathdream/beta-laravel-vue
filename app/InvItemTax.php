<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvItemTax extends Model
{
    //
    protected $table = 'inv_item_taxes';

    //    //
    use SoftDeletes;

}
