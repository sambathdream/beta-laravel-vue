<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectTestersIssues extends Model
{
    protected $guarded = [];

    public function issueType()
    {
        return $this->belongsTo(IssueTypes::class);
    }
}
