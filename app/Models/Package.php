<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    //
    use SoftDeletes;

    const PERCENTAGE_DISCOUNT = 'percentage';

    const AMOUNT_DISCOUNT = 'amount';

    protected $guarded = ['id'];

    protected $casts = [
        'discount_type' => 'integer',
        'first_project_per_tester_charge' => 'float',
        'regular_per_tester_charge' => 'float',
        'discount_after_number_of_tester' => 'integer',
        'discount_amount' => 'float'
    ];
    /**
     * Always capitalize the first letter
     */
    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }

    public function hasDiscount()
    {
        return $this->discount_after_number_of_tester > 0 && !is_null($this->discount_type);
    }

    public function hasDiscountInPercentage()
    {
        if ($this->attributes['discount_type'] === 1) {
            return true;
        }
        return false;
    }
}
