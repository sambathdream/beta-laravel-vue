<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserInvoiceItem extends Model
{
    //
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function invoiceTab()
    {
        return $this->belongsTo('App\UserInvoice','invoice_num','id');
    }


    public function itemTaxTab()
    {
        return $this->hasMany('App\UserInvoiceTax','item_num', 'id');
    }


}
