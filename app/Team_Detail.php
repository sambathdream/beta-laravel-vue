<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team_Detail extends Model
{
    //
    protected $table = 'team_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
}
