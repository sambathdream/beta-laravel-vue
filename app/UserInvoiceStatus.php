<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInvoiceStatus extends Model
{
    //
    protected $table = 'user_invoice_status';
}
