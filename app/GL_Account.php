<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GL_Account extends Model
{
    //
    protected $table = 'gl_accounts';

    //    //
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function txnTab()
    {
        return $this->hasMany('App\Transaction');
    }

    public function glTypeTab()
    {
        return $this->belongsTo('App\GL_Type','type'); //hit the primary key in the txn_categories table
    }

}
