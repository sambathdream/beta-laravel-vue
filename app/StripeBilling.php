<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StripeBilling extends Model
{
    //
    protected $table = 'stripe_billing';
}
