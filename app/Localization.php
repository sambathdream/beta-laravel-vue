<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Localization extends Model
{
    //    //
    protected $table = 'localization';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
}
