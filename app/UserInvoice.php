<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserInvoice extends Model
{
    //
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function contactTab()
    {
        return $this->belongsTo('App\Contact','contact_id','id'); //hit the primary key in the txn_categories table
    }

    public function statusTab()
    {
        return $this->belongsTo('App\UserInvoiceStatus','status_id','id'); //hit the primary key in the txn_categories table
    }

    public function invItemsTab()
    {
        return $this->hasMany('App\UserInvoiceItem','invoice_num', 'id');
    }

    public function invTaxTab()
    {
        return $this->hasMany('App\UserInvoiceTax','invoice_num', 'id');
    }

    public function paymentsTab()
    {
        return $this->hasMany('App\UserPayment','invoice_id', 'id');
    }
}
