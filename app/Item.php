<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    //
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function inventoryTaxTab()
    {
        return $this->hasMany('App\InvItemTax','item_num', 'id');
    }

    public function hsnSacTab()
    {
        return $this->hasMany('App\Hsnsac','id', 'gst_hsnsac_id');
    }

}
