<?php

namespace App\Listeners;

//use App\Events\TeamCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Laravel\Spark\Events\Teams\TeamCreated;

use App\Team_Detail;
use App\Localization;
use App\GL_Account;
use App\Accounting;
use Illuminate\Support\Facades\Auth;

class ListenTeamCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TeamCreated  $event
     * @return void
     */
    public function handle(TeamCreated $event)
    {
        //
        $user_id    = 0; //Auth::user()->id; //during registration user id is not available

        //\DB::table('gl_types')->insert(['text' => $event->team->id, 'sub' => 'Accounts Receivable']);

        $localization = new Localization;

        $localization->team_id          = $event->team->id;
        $localization->currency_id      = 840;
        $localization->language_id      = 840;
        $localization->timezone         = 'Etc/Greenwich';
        $localization->date_time_format = 2;
        $localization->week_first_day   = 1;
        $localization->year_first_month = 1;
        $localization->created_by_id    = $user_id;
        $localization->modified_by_id   = $user_id;

        $localization->save();


        $team_detail = new Team_Detail;

        $team_detail->team_id          = $event->team->id;
        /*
            $team_detail->id_number        = null;
            $team_detail->vat_number       = null;
            $team_detail->website          = null;
            $team_detail->phone            = null;
            $team_detail->company_size     = null;
            $team_detail->industry         = null;
        */
        $team_detail->created_by_id    = $user_id;
        $team_detail->modified_by_id   = $user_id;

        $team_detail->save();

        $gl_account               = new GL_Account;
        $gl_account->team_id      = $event->team->id;
        $gl_account->open_id      = 1;
        $gl_account->type         = 1; //$request->type; //Accounts Receivable
        $gl_account->name         = 'Accounts Receivable'; //$request->name;
        $gl_account->int_balance  = 0;
        $gl_account->ext_balance  = 0;
        $gl_account->description  = 'Accounts Receivable System Account';
        $gl_account->created_by_id   = $user_id;
        $gl_account->modified_by_id  = $user_id;
        $gl_account->save();

        $gl_account               = new GL_Account;
        $gl_account->team_id      = $event->team->id;
        $gl_account->open_id      = 2;
        $gl_account->type         = 9; //$request->type; //Accounts Payable
        $gl_account->name         = 'Accounts Payable'; //$request->name;
        $gl_account->int_balance  = 0;
        $gl_account->ext_balance  = 0;
        $gl_account->description  = 'Accounts Payable System Account';
        $gl_account->created_by_id   = $user_id;
        $gl_account->modified_by_id  = $user_id;
        $gl_account->save();

        $gl_account               = new GL_Account;
        $gl_account->team_id      = $event->team->id;
        $gl_account->open_id      = 3;
        $gl_account->type         = 2; //$request->type; //Inventory
        $gl_account->name         = 'Inventory'; //$request->name;
        $gl_account->int_balance  = 0;
        $gl_account->ext_balance  = 0;
        $gl_account->description  = 'Inventory System Account';
        $gl_account->created_by_id   = $user_id;
        $gl_account->modified_by_id  = $user_id;
        $gl_account->save();

        $gl_account               = new GL_Account;
        $gl_account->team_id      = $event->team->id;
        $gl_account->open_id      = 4;
        $gl_account->type         = 11; //$request->type; //Income
        $gl_account->name         = 'Income'; //$request->name;
        $gl_account->int_balance  = 0;
        $gl_account->ext_balance  = 0;
        $gl_account->description  = 'Income System Account';
        $gl_account->created_by_id   = $user_id;
        $gl_account->modified_by_id  = $user_id;
        $gl_account->save();

        $gl_account               = new GL_Account;
        $gl_account->team_id      = $event->team->id;
        $gl_account->open_id      = 5;
        $gl_account->type         = 11; //$request->type; //Income
        $gl_account->name         = 'Sales'; //$request->name;
        $gl_account->int_balance  = 0;
        $gl_account->ext_balance  = 0;
        $gl_account->description  = 'Sales System Account';
        $gl_account->created_by_id   = $user_id;
        $gl_account->modified_by_id  = $user_id;
        $gl_account->save();

        $gl_account               = new GL_Account;
        $gl_account->team_id      = $event->team->id;
        $gl_account->open_id      = 6;
        $gl_account->type         = 13; //$request->type; //Expenses
        $gl_account->name         = 'Payment Processor Fees'; //$request->name;
        $gl_account->int_balance  = 0;
        $gl_account->ext_balance  = 0;
        $gl_account->description  = 'Payment Processor Fees System Account';
        $gl_account->created_by_id   = $user_id;
        $gl_account->modified_by_id  = $user_id;
        $gl_account->save();

        $gl_account               = new GL_Account;
        $gl_account->team_id      = $event->team->id;
        $gl_account->open_id      = 7;
        $gl_account->type         = 7; //Other Current Liability
        $gl_account->name         = 'Income Tax'; //$request->name;
        $gl_account->int_balance  = 0;
        $gl_account->ext_balance  = 0;
        $gl_account->description  = 'Taxes To Be Paid';
        $gl_account->created_by_id   = $user_id;
        $gl_account->modified_by_id  = $user_id;
        $gl_account->save();

        $gl_account               = new GL_Account;
        $gl_account->team_id      = $event->team->id;
        $gl_account->open_id      = 8;
        $gl_account->type         = 11; //Income Contra Account - Sales discount
        $gl_account->name         = 'Sales Discount'; //$request->name;
        $gl_account->int_balance  = 0;
        $gl_account->ext_balance  = 0;
        $gl_account->description  = 'Sales Discount (Contra Income Account)';
        $gl_account->created_by_id   = $user_id;
        $gl_account->modified_by_id  = $user_id;
        $gl_account->save();

        
        $accounting               = Accounting::firstOrNew(['team_id' => $event->team->id]);
        $accounting->team_id      = $event->team->id;
        $accounting->paymentcr    = 4;  //Income
        $accounting->paymentdb    = 5;  //Sales
        $accounting->processfeecr = 6;  //Payment Processor Fees
        $accounting->processfeedb = 6;  //Payment Processor Fees
        $accounting->invoicecr    = 1;  //Accounts Receivable //not needed
        $accounting->invoicedb    = 1;  //Accounts Receivable
        $accounting->taxcr        = 2;  //Accounts Payable
        $accounting->taxdb        = 1;  //Accounts Receivable

        $accounting->productcr    = 3;  //Inventory
        $accounting->productdb    = 3;  //Inventory
        $accounting->manualcr     = 1;  //Accounts Receivable
        $accounting->manualdb     = 1;  //Accounts Receivable

        $accounting->created_by_id   = $user_id;
        $accounting->modified_by_id  = $user_id;
        $accounting->save();

    }
}
