<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Laravel\Spark\Events\Auth\UserRegistered;

use Illuminate\Support\Facades\Auth;

use App\BootTour;

class ListenUserCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserCreated  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        //
        $boot_tour = BootTour::firstOrNew(['user_id' => $event->user->id]);
        $boot_tour->user_id = $event->user->id;

        $boot_tour->save();

    }
}
