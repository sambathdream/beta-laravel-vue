<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInvoiceTerm extends Model
{
    //
    protected $table = 'user_invoice_terms';
}
