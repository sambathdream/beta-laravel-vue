<?php

namespace App\Traits;

use App\Team;
use App\Team_Detail;
use App\Contact;

use App\UserInvoice;
use App\UserInvoiceItem;
use App\UserInvoiceTax;
use App\UserPayment;

use App\UserCredit;
use App\UserCreditItem;
use App\UserCreditTax;
use App\UserRefund;

use App\UserRecInvoice;

use App\Transaction;
use App\GL_Account;
use App\Localization;
use App\Country;

use App\UserGateway;
use App\StripeBilling;
use App\Accounting;

use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Mail\Message;
use Jenssegers\Optimus\Optimus;

//use MongoDB\Driver\ReadConcern;
use Symfony\Component\HttpFoundation\Response;

trait stripeBillingTrait
{

    protected function verifyStripeSign(Request $request, $team_id)
    {

        $user_gateway = UserGateway::where('team_id', '=', $team_id)
            ->where('gateway_id', '=', 1)
            ->first();

        $gate_data = decrypt($user_gateway->gate_data);
        $gate_data = explode("#", $gate_data);

        \Stripe\Stripe::setApiKey($gate_data[0]);
        //\Stripe\Stripe::setApiKey("sk_test_Ohxdc5JY7QcRUU1NpA3F60C7");

        $payload = $request->getContent();

        $stripe_billing = StripeBilling::where('team_id', '=', $team_id)->first();
        // You can find your endpoint's secret in your webhook settings
        $endpoint_secret = $stripe_billing->signingsecret;
        //"whsec_O2GPL6bhOmoyi0ixl7FuDBgvXjSuIiPu";

        $sig_header = $_SERVER["HTTP_STRIPE_SIGNATURE"];

        $event = null;

        try {
            $event = \Stripe\Webhook::constructEvent(
                $payload, $sig_header, $endpoint_secret
            );
        } catch(\UnexpectedValueException $e) {
            // Invalid payload
            return new Response('Webhook Failed', 400); // PHP 5.4 or greater
            exit();
        } catch(\Stripe\Error\SignatureVerification $e) {
            // Invalid signature
            return new Response('Webhook Failed', 400); // PHP 5.4 or greater
            exit();
        }

    }

    /*
    protected function createStripeContact($contact_id_no, $contact_name_email, $team_id, $user_id)
    {

        $last_contact   = Contact::where('team_id', '=', $team_id)
                            ->orderBy('id', 'desc')
                            ->first();          //returns last latest row

        if ($last_contact) {
            $new_contact_number = $last_contact->open_id + 1;
        } else {
            $new_contact_number = 1;
        }

        $contact                    =   new Contact;
        $contact->team_id           =   $team_id;
        $contact->open_id           =   $new_contact_number;
        $contact->id_no             =   $contact_id_no;
        $contact->payment_terms     =   0; //due on receipt

        //$contact->image = "https://robohash.org/" . $contact->id_no;
        $contact->image = "https://api.adorable.io/avatars/40/" . $contact->id_no;

        $contact->email             =   empty($contact_name_email)
                                        ? null : $contact_name_email;

        $contact->name              =   empty($contact_name_email)
                                        ? $contact_id_no : $contact_name_email;

        $contact->save();
        return $contact;

    }
    */

    /*
    protected function updateStripeContact(array $request, Contact $contact, $team_id, $user_id) //,
    {

        $default_source = $request['data']['object']['default_source'];

        //return $default_source;
        $sources = null;

        if($default_source){

            foreach($request['data']['object']['sources']['data'] as $source)
            {
                if($default_source == $source['id'])
                {
                    $sources = $source;
                    break;
                }
            }

        }


        if($sources) {

            $contact->name  = empty($sources['name'])
                                ? 'Stripe_' . $request['data']['object']['id'] : $sources['name'];


            $contact->email = empty($request['data']['object']['email'])
                                ? null : $request['data']['object']['email'];

            $country        = empty($sources['country']) ?
                                $sources['country'] : substr($sources['address_country'], 0, 2);

            if ($country) {

                $contact_curr           = Country::where('iso_3166_2', '=', $country)
                                            ->first();

                if($contact_curr) {
                    $contact->currency_id = $contact_curr->id;
                }
            }

            $contact->bill_address1     = empty($sources['address_line1'])
                                            ? null : ucwords(strtolower($sources['address_line1']));

            $contact->bill_address2     = empty($sources['address_line2'])
                                            ? null : ucwords(strtolower($sources['address_line2']));

            $contact->bill_city         = empty($sources['address_city'])
                                            ? null : ucwords(strtolower($sources['address_city']));

            $contact->bill_state        = empty($sources['address_state'])
                                            ? null : ucwords(strtolower($sources['address_state']));

            $contact->bill_postal_code  = empty($sources['address_zip'])
                                            ? null : $sources['address_zip'];

            $contact->bill_country_id   = empty($contact_curr->iso_3166_2)
                                            ? $sources['country'] : $contact_curr->iso_3166_2;

            $contact->save();

            return $contact;

        }

    }
    */

    protected function returnStripeContactInfo(array $request) //,
    {

        $default_source = $request['data']['object']['default_source'];

        //return $default_source;
        $sources = null;

        if($default_source){

            foreach($request['data']['object']['sources']['data'] as $source)
            {
                if($default_source == $source['id'])
                {
                    $sources = $source;
                    break;
                }
            }

        }


        if($sources) {

            $contact        = new Contact;

            $contact->name  = empty($sources['name'])
                ? 'Stripe_' . $request['data']['object']['id'] : $sources['name'];


            $contact->email = empty($request['data']['object']['email'])
                ? null : $request['data']['object']['email'];

            $country        = empty($sources['country']) ?
                $sources['country'] : substr($sources['address_country'], 0, 2);

            if ($country) {

                $contact_curr           = Country::where('iso_3166_2', '=', $country)
                    ->first();

                if($contact_curr) {
                    $contact->currency_id = $contact_curr->id;
                }
            }

            $contact->bill_address1     = empty($sources['address_line1'])
                ? null : ucwords(strtolower($sources['address_line1']));

            $contact->bill_address2     = empty($sources['address_line2'])
                ? null : ucwords(strtolower($sources['address_line2']));

            $contact->bill_city         = empty($sources['address_city'])
                ? null : ucwords(strtolower($sources['address_city']));

            $contact->bill_state        = empty($sources['address_state'])
                ? null : ucwords(strtolower($sources['address_state']));

            $contact->bill_postal_code  = empty($sources['address_zip'])
                ? null : $sources['address_zip'];

            $contact->bill_country_id   = empty($contact_curr->iso_3166_2)
                ? $sources['country'] : $contact_curr->iso_3166_2;

            //$contact->save(); //don't save

            $contact->id_no = $request['data']['object']['id'];

            $contact->payment_terms = 0;

            //create request
            $contact_request = new \Illuminate\Http\Request();

            $contact_request->replace([
                'name' => $contact->name,
                'email' => $contact->email,
                'id_no' => $contact->id_no,
                'payment_terms' => $contact->payment_terms,
                'currency_id' => $contact->currency_id,
                'bill_address1' => $contact->bill_address1,
                'bill_address2' => $contact->bill_address2,
                'bill_city' => $contact->bill_city,
                'bill_state' => $contact->bill_state,
                'bill_postal_code' => $contact->bill_postal_code,
                'bill_country_id' => $contact->bill_country_id

            ]);

            return $contact_request;

        }

    }

    /*
    protected function updateStripeAPIContact(array $request, Contact $contact, $team_id, $user_id)
    {

        //return $request;

        $default_source = $request['default_source'];

        //return $default_source;
        $sources = null;

        if($default_source){

            foreach($request['sources']['data'] as $source)
            {
                if($default_source == $source['id'])
                {
                    $sources = $source;
                    break;
                }
            }

        }


        if($sources) {

            //return $sources['country'];

            $contact->name  = empty($sources['name']) ? 'Stripe_' . $request['id'] : ucwords(strtolower($sources['name']));

            $contact->email = empty($request['email']) ? null : strtolower($request['email']);

            $country        = empty($sources['country']) ?
                $sources['country'] : substr($sources['address_country'], 0, 2);

            //return $country;

            if ($country) {

                $contact_curr           = Country::where('iso_3166_2', '=', $country)
                    ->first();

                if($contact_curr) {
                    $contact->currency_id = $contact_curr->id;
                }
            }

            $contact->bill_address1     = empty($sources['address_line1'])
                ? null : ucwords(strtolower($sources['address_line1']));

            $contact->bill_address2     = empty($sources['address_line2'])
                ? null : ucwords(strtolower($sources['address_line2']));

            $contact->bill_city         = empty($sources['address_city'])
                ? null : ucwords(strtolower($sources['address_city']));

            $contact->bill_state        = empty($sources['address_state'])
                ? null : ucwords(strtolower($sources['address_state']));

            $contact->bill_postal_code  = empty($sources['address_zip'])
                ? null : $sources['address_zip'];

            $contact->bill_country_id   = empty($contact_curr->iso_3166_2)
                ? $sources['country'] : $contact_curr->iso_3166_2;

            $contact->save();

            return $contact;

        }

    }
    */

    /*
    protected function createStripeInvoice(Request $request, Contact $contact, $team_id, $user_id)
    {
        $invoice                        = new UserInvoice;
        $invoice->team_id               = $team_id;
        $invoice->contact_id            = $contact->id;
        $invoice->status_id             = 1; //open bills by stripe
        $invoice->title                 = 'Invoice';

        $last_invoice                   = UserInvoice::where('team_id', '=', $team_id)
                                        ->orderBy('id', 'desc')
                                        //->withTrashed()
                                        ->first();          //returns last latest row

        if ($last_invoice) {
            $new_invoice_number         = $last_invoice->open_id + 1;
        } else {
            $new_invoice_number         = 1;
        }

        $invoice->open_id               = $new_invoice_number;

        $invoice->discount_rate1        = 0;
        $invoice->discount_amount       = 0;
        $invoice->tax_amount            = 0;
        $invoice->sub_total             = 0;
        $invoice->total_amount          = 0;
        $invoice->amount_paid           = 0;
        $invoice->balance               = 0;

        $accounting                     = Accounting::where('team_id', '=', $team_id)->first();
        $invoice->debit_gl              = $accounting->invoicedb;
        $invoice->credit_gl_cb          = false;

        $invoice->post_date             = Carbon::now()->toDateTimeString(); //update date
        $invoice->due_date              = Carbon::now()->toDateTimeString(); //update date

        $team_detail                    = Team_Detail::where('team_id', '=', $team_id)->first();

        if ($team_detail) {

            $invoice->notes             = $team_detail->inv_footer;
            $invoice->footer            = $team_detail->inv_notes;
        }

        $invoice->stripe_id             = $request->data['object']['id'];

        $sub_total                      = 0;
        $line_items                     = null;
        $invoice->save();               //need to save to get id

        if($request->data['object']['lines']['data'][0]['object'] == "line_item") {
            $line_items = $request->data['object']['lines']['data'];


            foreach ($line_items as $item) {


                $invoiceItem = new UserInvoiceItem;

                $invoiceItem->team_id       = $team_id;
                $invoiceItem->invoice_num   = empty($invoice->id) ? null : $invoice->id;

                $invoiceItem->item_price    = empty($item['amount']) ? 0 : ($item['amount'] / 100);
                $invoiceItem->item_qty      = 1;
                $invoiceItem->item_total    = empty($item['amount']) ? 0 : ($item['amount'] / 100);
                $invoiceItem->credit_gl     = null;

                if($item['type'] == 'invoiceitem')
                {
                    $invoiceItem->item_id = 'Charge';

                    $invoiceItem->item_name = empty($item['description'])
                        ? 'No description' : ucwords(strtolower($item['description']));

                }elseif($item['type'] == 'subscription')
                {
                    $invoiceItem->item_id = empty($item['plan']['id'])
                        ? 'Invoice' : ucwords(strtolower($item['plan']['id'] . ' ' . $item['plan']['object']));

                    $start_date = Carbon::createFromTimestamp($item['period']['start'])->toFormattedDateString();
                    $end_date   = Carbon::createFromTimestamp($item['period']['end'])->toFormattedDateString();

                    $stat_descr = empty($item['statement_descriptor']) ? ' ' : ucwords(strtolower($item['statement_descriptor']));

                    $item_name  = empty($item['name']) ? ' ' : ucwords(strtolower($item['name']));

                    $item_name  = $item_name . $stat_descr . '(' . $start_date . ' - ' . $end_date . ')';

                    $invoiceItem->item_name = $item_name;
                }

                $invoiceItem->save();

                $sub_total                  = $sub_total + $invoiceItem->item_total;

            }

        }

        $invoice->sub_total             = $sub_total;

        $invoice->discount_rate1        = 0;
        $invoice->discount_amount       = 0;
        $invoice->tax_amount            = 0;

        $invoice->total_amount          = $sub_total;
        $invoice->amount_paid           = 0; //empty($invoice->amount_paid) ? 0 : $invoice->amount_paid;
        $invoice->balance               = $sub_total - $invoice->amount_paid;


        $invoice->currency_id           = 840;
        $invoice->currency_id_local     = 840;
        $invoice->currency_rate         = 1;

        $invoice->total_amount_local    = $invoice->currency_rate * $invoice->total_amount;
        $invoice->amount_paid_local     = $invoice->currency_rate * $invoice->amount_paid;
        $invoice->balance_local         = $invoice->currency_rate * $invoice->balance;

        $invoice->created_by_id         = $user_id; //automatically created
        $invoice->modified_by_id        = $user_id; //automatically created

        $optimus                        = new Optimus(522588247, 282319719, 383644817);
        $invoice->encrypted_id          = $optimus->encode($invoice->id);

        $invoice->save();
        return $invoice;

    }
    */

    protected function createStripeInvoiceWithPayment(Request $request, Contact $contact, $team_id, $user_id)
    {
        $invoice                        = new UserInvoice;
        $invoice->team_id               = $team_id;
        $invoice->contact_id            = $contact->id;
        $invoice->status_id             = 4; //invoice paid
        $invoice->title                 = 'Invoice';

        $last_invoice                   = UserInvoice::where('team_id', '=', $team_id)
            ->orderBy('open_id', 'desc')
            //->withTrashed()
            ->first();          //returns last latest row

        if ($last_invoice) {
            $new_invoice_number         = $last_invoice->open_id + 1;
        } else {
            $new_invoice_number         = 1;
        }

        $invoice->open_id               = $new_invoice_number;

        $invoice->discount_rate1        = 0;
        $invoice->discount_amount       = 0;
        $invoice->tax_amount            = 0;
        $invoice->sub_total             = 0;
        $invoice->total_amount          = 0;
        $invoice->amount_paid           = 0;
        $invoice->balance               = 0;

        $accounting                     = Accounting::where('team_id', '=', $team_id)->first();
        $invoice->debit_gl              = $accounting->invoicedb;
        $invoice->credit_gl_cb          = false;

        $invoice->post_date             = Carbon::now()->toDateTimeString(); //update date
        $invoice->due_date              = Carbon::now()->toDateTimeString(); //update date

        $team_detail                    = Team_Detail::where('team_id', '=', $team_id)->first();

        if ($team_detail) {

            $invoice->notes             = $team_detail->inv_footer;
            $invoice->footer            = $team_detail->inv_notes;
        }

        $invoice->stripe_id             = $request->data['object']['id'];

        $sub_total                      = 0;
        $line_items                     = null;
        $invoice->save();               //need to save to get id

        if($request->data['object']['lines']['data'][0]['object'] == "line_item") {
            $line_items = $request->data['object']['lines']['data'];


            foreach ($line_items as $item) {


                $invoiceItem = new UserInvoiceItem;

                $invoiceItem->team_id       = $team_id;
                $invoiceItem->invoice_num   = empty($invoice->id) ? null : $invoice->id;

                $invoiceItem->item_price    = empty($item['amount']) ? 0 : ($item['amount'] / 100);
                $invoiceItem->item_qty      = 1;
                $invoiceItem->item_total    = empty($item['amount']) ? 0 : ($item['amount'] / 100);
                $invoiceItem->credit_gl     = null;

                if($item['type'] == 'invoiceitem')
                {
                    $invoiceItem->item_id = 'Charge';

                    $invoiceItem->item_name = empty($item['description'])
                        ? 'No description' : ucwords(strtolower($item['description']));

                }elseif($item['type'] == 'subscription')
                {
                    $invoiceItem->item_id = empty($item['plan']['id'])
                        ? 'Invoice' : ucwords(strtolower($item['plan']['id'] . ' ' . $item['plan']['object']));

                    $start_date = Carbon::createFromTimestamp($item['period']['start'])->toFormattedDateString();
                    $end_date   = Carbon::createFromTimestamp($item['period']['end'])->toFormattedDateString();

                    $stat_descr = empty($item['statement_descriptor']) ? ' ' : ucwords(strtolower($item['statement_descriptor']));

                    $item_name  = empty($item['name']) ? ' ' : ucwords(strtolower($item['name']));

                    $item_name  = $item_name . $stat_descr . '(' . $start_date . ' - ' . $end_date . ')';

                    $invoiceItem->item_name = $item_name;
                }

                $invoiceItem->save();

                $sub_total                  = $sub_total + $invoiceItem->item_total;

            }

        }

        if($request->data['object']['starting_balance'])
        {
            $starting_balance = $request->data['object']['starting_balance'] / 100;

            if($starting_balance > 0) {

                $invoiceItem = new UserInvoiceItem;

                $invoiceItem->team_id = $team_id;
                $invoiceItem->invoice_num = empty($invoice->id) ? null : $invoice->id;

                $invoiceItem->item_price = $starting_balance;
                $invoiceItem->item_qty = 1;
                $invoiceItem->item_total = $starting_balance;
                $invoiceItem->credit_gl = null;

                $invoiceItem->item_id = 'Previous Dues';
                $invoiceItem->item_name = 'Starting Balance';

                $invoiceItem->save();

                $sub_total = $sub_total + $invoiceItem->item_total;
                
            }

        }


        $invoice->sub_total             = $sub_total;

        $invoice->discount_rate1        = 0;
        $invoice->discount_amount       = 0;
        $invoice->tax_amount            = 0;

        $invoice->total_amount          = $sub_total;
        $invoice->amount_paid           = ( $request->data['object']['amount_due'] / 100 );
        $invoice->balance               = $sub_total - $invoice->amount_paid;


        $invoice->currency_id           = 840;
        $invoice->currency_id_local     = 840;
        $invoice->currency_rate         = 1;

        $invoice->total_amount_local    = $invoice->currency_rate * $invoice->total_amount;
        $invoice->amount_paid_local     = $invoice->currency_rate * $invoice->amount_paid;
        $invoice->balance_local         = $invoice->currency_rate * $invoice->balance;

        $invoice->created_by_id         = $user_id; //automatically created
        $invoice->modified_by_id        = $user_id; //automatically created

        $optimus                        = new Optimus(522588247, 282319719, 383644817);
        $invoice->encrypted_id          = $optimus->encode($invoice->id);

        $invoice->save();

        $last_payment               = UserPayment::where('team_id', '=', $team_id)
            ->orderBy('id', 'desc')
            ->first();          //returns last latest row

        if ($last_payment) {
            $new_pay_number         = $last_payment->open_id + 1;
        } else {
            $new_pay_number         = 1;
        }

        $payment                    = new UserPayment;

        $payment->team_id           = $team_id;
        $payment->open_id           = $new_pay_number;
        $payment->contact_id        = empty($invoice->contact_id) ? null : $invoice->contact_id;
        $payment->invoice_id        = empty($invoice->id) ? null : $invoice->id;

        $payment->payment_type_id       = 5;

        $payment->amount                = $invoice->amount_paid;
        $payment->amount_local          = $invoice->amount_paid_local;

        $payment->transaction_reference = 'Stripe Pay';
        $payment->payment_date          = Carbon::now()->toDateTimeString();

        $payment->stripe_charge_id      = $request['data']['object']['id'];

        $payment->created_by_id         = $user_id;
        $payment->modified_by_id        = $user_id;

        $payment->save();


        return $invoice;

    }

    /*
    protected function updateStripeInvoice(Request $request, UserInvoice $invoice, $team_id, $user_id)
    {

        $invoice->post_date         = Carbon::now()->toDateTimeString(); //update date
        $invoice->due_date          = Carbon::now()->toDateTimeString(); //update date

        $team_detail                = Team_Detail::where('team_id', '=', $team_id)->first();

        if ($team_detail) {

            $invoice->notes         = $team_detail->inv_footer;
            $invoice->footer        = $team_detail->inv_notes;
        }

        $invoice->created_by_id     = $user_id; //automatically created
        $invoice->modified_by_id    = $user_id; //automatically created

        $sub_total                  = empty($invoice->sub_total) ? 0 : $invoice->sub_total;

        if($request->data['object']['lines']['data'][0]['object'] == "line_item")
        {
            $line_items             = $request->data['object']['lines']['data'];
        }

        $invoice->save();

        foreach($line_items as $item){


            $invoiceItem                = new UserInvoiceItem;

            $invoiceItem->team_id       = $team_id;
            $invoiceItem->invoice_num   = empty($invoice->id) ? null : $invoice->id;
            //$invoiceItem->contact_id    = $request->contact_id;

            $invoiceItem->item_id       = empty($item['plan']['id'])
                        ? 'Invoice' : ucwords(strtolower($item['plan']['id'] . ' ' . $item['plan']['object']));

            $invoiceItem->item_name     = empty($item['description'])
                        ? 'Charge' : ucwords(strtolower($item['description']));

            $invoiceItem->item_price    = empty($item['amount']) ? 0 : ($item['amount']/100);

            $invoiceItem->item_qty      = 1;

            $invoiceItem->item_total    = empty($item['amount']) ? 0 : ($item['amount']/100);

            $invoiceItem->credit_gl     = null;

            $invoiceItem->save();

            if($invoice->status_id == 5)
            {
                UserInvoiceItem::find($invoiceItem->id)->delete();
            }

            $sub_total  = $sub_total + $invoiceItem->item_total;

        }

        $invoice->sub_total             = $sub_total;

        $invoice->discount_rate1        = 0;
        $invoice->discount_amount       = 0;
        $invoice->tax_amount            = 0;

        $invoice->total_amount          = $sub_total;
        $invoice->amount_paid           = empty($invoice->amount_paid) ? 0 : $invoice->amount_paid;
        $invoice->balance               = $sub_total - $invoice->amount_paid;

        $invoice->currency_rate         = 1;
        $invoice->total_amount_local    = $invoice->currency_rate * $invoice->total_amount;
        $invoice->amount_paid_local     = $invoice->currency_rate * $invoice->amount_paid;
        $invoice->balance_local         = $invoice->currency_rate * $invoice->balance;

        $invoice->save();
        return $invoice;

    }
    */

    protected function createStripeInvoiceForCharge(Request $request, Contact $contact, $team_id, $user_id)
    {
        $invoice                        = new UserInvoice;
        $invoice->team_id               = $team_id;
        $invoice->contact_id            = $contact->id;
        $invoice->status_id             = 1; //invoice created
        $invoice->title                 = 'Invoice';

        $last_invoice                   = UserInvoice::where('team_id', '=', $team_id)
                                            ->orderBy('open_id', 'desc')
                                            //->withTrashed()
                                            ->first();          //returns last latest row

        if ($last_invoice) {
            $new_invoice_number         = $last_invoice->open_id + 1;
        } else {
            $new_invoice_number         = 1;
        }

        $invoice->open_id               = $new_invoice_number;

        $invoice->discount_rate1        = 0;
        $invoice->discount_amount       = 0;
        $invoice->tax_amount            = 0;
        $invoice->sub_total             = 0;
        $invoice->total_amount          = 0;
        $invoice->amount_paid           = 0;
        $invoice->balance               = 0;

        $accounting                     = Accounting::where('team_id', '=', $team_id)->first();
        $invoice->debit_gl              = $accounting->invoicedb;
        $invoice->credit_gl_cb          = false;

        $invoice->post_date             = Carbon::createFromTimestamp($request['data']['object']['created'])->toDateTimeString();
        $invoice->due_date              = Carbon::createFromTimestamp($request['data']['object']['created'])->toDateTimeString();

        $team_detail                    = Team_Detail::where('team_id', '=', $team_id)->first();

        if ($team_detail) {

            $invoice->notes             = $team_detail->inv_footer;
            $invoice->footer            = $team_detail->inv_notes;
        }

        $invoice->stripe_id             = empty($request['data']['object']['invoice']) ?
                            $request['data']['object']['id'] : $request['data']['object']['invoice']; //charge id will be stored

        $sub_total                      = 0;
        $line_items                     = null;

        $invoice->save();               //need to save to get id

        $invoiceItem                    = new UserInvoiceItem;

        $invoiceItem->team_id           = $team_id;
        $invoiceItem->invoice_num       = empty($invoice->id) ? null : $invoice->id;

        $invoiceItem->item_price        = empty($request['data']['object']['amount'])
                                            ? 0 : ($request['data']['object']['amount'] / 100);

        $invoiceItem->item_qty          = 1;
        $invoiceItem->item_total        = $invoiceItem->item_price * $invoiceItem->item_qty;
        $invoiceItem->credit_gl         = null;

        $invoiceItem->item_id = 'Charge';

        $invoiceItem->item_name = empty($request['data']['object']['description'])
            ? 'No description' : ucwords(strtolower($request['data']['object']['description']));


        $invoiceItem->save();

        $sub_total                      = $sub_total + $invoiceItem->item_total;

        $invoice->sub_total             = $sub_total;

        $invoice->discount_rate1        = 0;
        $invoice->discount_amount       = 0;
        $invoice->tax_amount            = 0;

        $invoice->total_amount          = $sub_total;

        $invoice->amount_paid           = 0;

        $invoice->balance               = $sub_total - $invoice->amount_paid;

        $invoice->currency_id           = 840;
        $invoice->currency_id_local     = 840;
        $invoice->currency_rate         = 1;

        $invoice->total_amount_local    = $invoice->currency_rate * $invoice->total_amount;
        $invoice->amount_paid_local     = $invoice->currency_rate * $invoice->amount_paid;
        $invoice->balance_local         = $invoice->currency_rate * $invoice->balance;

        $invoice->created_by_id         = $user_id; //automatically created
        $invoice->modified_by_id        = $user_id; //automatically created

        $optimus                    = new Optimus(522588247, 282319719, 383644817);
        $invoice->encrypted_id      = $optimus->encode($invoice->id);
        
        $invoice->save();

        return $invoice;

    }

    protected function chargeStripeSuccess(Request $request, UserInvoice $invoice, $team_id, $user_id)
    {

        //get the invoice and save
        $invoice->status_id         = 4;
        $amount_paid                = $request['data']['object']['amount'] / 100;
        $amount_refunded            = $request['data']['object']['amount_refunded'] / 100;
        $amount_paid_local          = $invoice->currency_rate * $amount_paid;

        $invoice->amount_paid       = (empty($invoice->amount_paid) ? 0 : $invoice->amount_paid) + ($amount_paid)
                                        - $amount_refunded;

        $invoice->balance           = (empty($invoice->balance) ? 0 : $invoice->balance)
                                        - $amount_paid;

        $invoice->amount_paid_local = $invoice->currency_rate * $invoice->amount_paid;
        $invoice->balance_local     = $invoice->currency_rate * $invoice->balance;

        $invoice->save();
        //invoice saved end

        $last_payment               = UserPayment::where('team_id', '=', $team_id)
                                        ->orderBy('id', 'desc')
                                        ->first();          //returns last latest row

        if ($last_payment) {
            $new_pay_number         = $last_payment->open_id + 1;
        } else {
            $new_pay_number         = 1;
        }

        $payment                    = new UserPayment;

        $payment->team_id           = $team_id;
        $payment->open_id           = $new_pay_number;
        $payment->contact_id        = empty($invoice->contact_id) ? null : $invoice->contact_id;
        $payment->invoice_id        = empty($invoice->id) ? null : $invoice->id;

        $payment->payment_type_id       = 5;

        $payment->amount                = $amount_paid;
        $payment->amount_local          = $amount_paid_local;

        $payment->transaction_reference = 'Stripe Pay';
        $payment->payment_date          = Carbon::now()->toDateTimeString();

        $payment->stripe_charge_id      = $request['data']['object']['id'];

        $payment->created_by_id         = $user_id;
        $payment->modified_by_id        = $user_id;

        $payment->save();

        //return response()->json('true');
        $pay_reference_txn          = $payment->id;

        $invoice_credit_gl          = $invoice->debit_gl;

        /*post transaction*/
        /*

        $last_txn                   = Transaction::where('team_id', '=', $team_id)
            ->orderBy('created_at', 'desc')
            ->first();          //returns last latest row

        if ($last_txn) {
            $last_txn_number = $last_txn->open_id + 1;
        } else {
            $last_txn_number = 1;
        }


        $accounting                 = Accounting::where('team_id', '=', $invoice->team_id)->first();
        $payment_debit_gl           = $accounting->paymentdb;

        //debit gl
        if ($payment_debit_gl) {

            $transaction                    = new Transaction;
            $transaction->team_id           = $team_id;

            $transaction->open_id           = $last_txn_number;

            $transaction->category          = 47;
            $transaction->txn_type          = "D";
            $transaction->txn_date          = Carbon::parse('today')->toDateString();

            $transaction->gl_account_id     = $payment_debit_gl;
            $transaction->txn_amount        = $amount_paid_local;
            $transaction->description       = 'Invoice Payment';

            $transaction->created_by_id     = $user_id;
            $transaction->modified_by_id    = $user_id;

            $transaction->save();

            $gl_account                     = GL_Account::where('team_id', '=', $team_id)
                ->where('open_id', '=', $payment_debit_gl)
                ->first();

            if ($gl_account) {
                $gl_account->int_balance    = $gl_account->int_balance + $transaction->txn_amount;
            }

            $gl_account->save();

            $transaction->balance           = $gl_account->int_balance;
            $transaction->pay_ref           = $pay_reference_txn;
            $transaction->save();

        }

        //credit gl
        if ($invoice_credit_gl) {
            $transaction                    = new Transaction;
            $transaction->team_id           = $team_id;

            $transaction->open_id           = $last_txn_number + 1;

            $transaction->category          = 47;
            $transaction->txn_type          = "C";//D or C ??
            $transaction->txn_date          = Carbon::parse('today')->toDateString();

            $transaction->gl_account_id     = $invoice_credit_gl;
            $transaction->txn_amount        = $amount_paid_local;

            $transaction->description       = 'Invoice Payment';
            $transaction->created_by_id     = $user_id;
            $transaction->modified_by_id    = $user_id;

            $transaction->save();

            $gl_account                     = GL_Account::where('team_id', '=', $team_id)
                ->where('open_id', '=', $invoice_credit_gl)
                ->first();

            if ($gl_account) {
                $gl_account->int_balance    = $gl_account->int_balance - $transaction->txn_amount;
            }

            $gl_account->save();

            $transaction->balance           = $gl_account->int_balance;
            $transaction->pay_ref           = $pay_reference_txn;
            $transaction->save();

        }

        */


    }

    protected function retrieveStripeCustomer($stripe_cust_id, $team_id)
    {
        //call the API to retrieve customer
        try {
            // Use Stripe's library to make requests...

            $user_gateway = UserGateway::where('team_id', '=', $team_id)
                ->where('gateway_id', '=', 1)
                ->first();

            $gate_data = decrypt($user_gateway->gate_data);
            $gate_data = explode("#", $gate_data);

            \Stripe\Stripe::setApiKey($gate_data[0]);

            $stripe_cust_request = \Stripe\Customer::retrieve($stripe_cust_id);

            $object = [ 'object' => $stripe_cust_request ];
            $stripe_cust_request    = [ 'data' => $object ];

            return (array) $stripe_cust_request;


        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            return new Response('Webhook Failed', 400); // PHP 5.4 or greater
            exit();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            return new Response('Webhook Failed', 400); // PHP 5.4 or greater
            exit();
        }

        return new Response('Webhook Failed', 400); // PHP 5.4 or greater
        exit();

    }

    /*
    protected function retrieveStripeInvoice($stripe_invoice_id, $team_id)
    {
        //call the API to retrieve customer
        try {
            // Use Stripe's library to make requests...

            $user_gateway = UserGateway::where('team_id', '=', $team_id)
                ->where('gateway_id', '=', 1)
                ->first();

            $gate_data = decrypt($user_gateway->gate_data);
            $gate_data = explode("#", $gate_data);

            \Stripe\Stripe::setApiKey($gate_data[0]);

            $stripe_cust_invoices = \Stripe\Invoice::retrieve("$stripe_invoice_id");

            return $stripe_cust_invoices;

        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            return new Response('Webhook Failed', 400); // PHP 5.4 or greater
            exit();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            return new Response('Webhook Failed', 400); // PHP 5.4 or greater
            exit();
        }

    }
    */

    protected function chargeStripeRefundInvoice(Request $request, Contact $contact, $team_id, $user_id)
    {

        $invoice                        = new UserCredit;
        $invoice->team_id               = $team_id;
        $invoice->contact_id            = $contact->id;
        $invoice->status_id             = 8; //refunded
        $invoice->title                 = 'Credit Note';

        $last_invoice                   = UserCredit::where('team_id', '=', $team_id)
                                            ->orderBy('open_id', 'desc')
                                            ->first();          //returns last latest row

        if ($last_invoice) {
            $new_invoice_number         = $last_invoice->open_id + 1;
        } else {
            $new_invoice_number         = 1;
        }

        $invoice->open_id               = $new_invoice_number;

        $invoice->discount_rate1        = 0;
        $invoice->discount_amount       = 0;
        $invoice->tax_amount            = 0;

        $sub_total                      = 0;
        $invoice->sub_total             = 0;

        $invoice->total_amount          = 0;
        $invoice->amount_paid           = 0;
        $invoice->balance               = 0;
        $invoice->total_amount_local    = 0;
        $invoice->amount_paid_local     = 0;
        $invoice->balance_local         = 0;

        $invoice->currency_id           = 840;
        $invoice->currency_id_local     = 840;
        $invoice->currency_rate         = 1;

        $accounting                     = Accounting::where('team_id', '=', $team_id)->first();
        $invoice->credit_gl              = $accounting->invoicedb;
        $invoice->debit_gl_cb          = false;

        $invoice->post_date             = Carbon::now()->toDateTimeString(); //update date
        $invoice->due_date              = Carbon::now()->toDateTimeString(); //update date

        $invoice->notes                 = 'Refund complete. No further action needed.';
        $invoice->footer                = 'Thanks! We really appreciate your business.';

        $invoice->stripe_id             = $request->data['object']['id'];

        $invoice->save();               //need to save to get id


        if($request['data']['object']['refunds']['object'] == "list") {
            $ref_items = $request['data']['object']['refunds']['data'];

            foreach ($ref_items as $item) {

                $amount_refund                  = empty($item['amount']) ? 0 : $item['amount'] / 100;
                $amount_refund                  = $amount_refund * -1;
                $amount_refund_local            = ( $invoice->currency_rate * $amount_refund ) * -1;

                $invoiceItem                    = new UserCreditItem;
                $invoiceItem->team_id           = $team_id;

                $invoiceItem->creditnote_num       = empty($invoice->id) ? null : $invoice->id;

                $invoiceItem->item_price        = $amount_refund;
                $invoiceItem->item_qty          = 1;
                $invoiceItem->item_total        = $invoiceItem->item_price * $invoiceItem->item_qty;
                $invoiceItem->debit_gl         = null;

                $invoiceItem->item_id           = 'Refund';
                $invoiceItem->item_name         = 'Completed on '
                                        . Carbon::createFromTimestamp($item['created'])->formatLocalized('%A %d %B %Y');

                $invoiceItem->save();

                $sub_total                      = $sub_total + $invoiceItem->item_total;

            }
        }

        $invoice->sub_total             = $sub_total;

        $invoice->total_amount          = $sub_total;
        $invoice->amount_paid           = 0;
        $invoice->balance               = 0;

        $invoice->total_amount_local    = $invoice->currency_rate * $invoice->total_amount;
        $invoice->amount_paid_local     = $invoice->currency_rate * $invoice->amount_paid;
        $invoice->balance_local         = $invoice->currency_rate * $invoice->balance;

        $optimus                        = new Optimus(1568794001, 671972209, 2122508544);//credit note specific
        $invoice->encrypted_id          = $optimus->encode($invoice->id);

        $invoice->created_by_id         = $user_id; //automatically created
        $invoice->modified_by_id        = $user_id; //automatically created

        $invoice->save();
        
        $last_payment = UserRefund::where('team_id', '=', $team_id)
            ->orderBy('id', 'desc')
            ->first();          //returns last latest row

        if ($last_payment) {
            $new_pay_number = $last_payment->open_id + 1;
        } else {
            $new_pay_number = 1;
        }

        $payment = new UserRefund;

        $payment->team_id = $team_id;
        $payment->open_id = $new_pay_number;
        $payment->contact_id = empty($invoice->contact_id) ? null : $invoice->contact_id;
        $payment->creditnote_num = empty($invoice->id) ? null : $invoice->id;

        $payment->payment_type_id = 5;

        $payment->amount = $invoice->total_amount;
        $payment->amount_local = $invoice->total_amount_local;

        $payment->transaction_reference = 'Credit Note';
        $payment->payment_date = Carbon::now()->toDateTimeString();

        $payment->stripe_charge_id = $request['data']['object']['id'];

        $payment->created_by_id = $user_id;
        $payment->modified_by_id = $user_id;

        $payment->save();


        return $invoice->id;

    }

}