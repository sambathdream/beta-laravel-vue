<?php

namespace App\Traits;

use App\Contact;
use App\UserInvoice;
use App\UserInvoiceItem;
use App\UserQuote;
use App\UserQuoteItem;
use App\UserInvoiceTax;
use App\UserQuoteTax;

use App\UserPayment;
use App\Transaction;
use App\GL_Account;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Optimus\Optimus;

use Symfony\Component\HttpFoundation\Response;

trait contactTrait
{

    protected function createContact(Request $request, $team_id, $user_id)
    {

        $last_contact = Contact::where('team_id', '=', $team_id)
            ->orderBy('id', 'desc')
            ->first();          //returns last latest row

        if ($last_contact) {
            $new_contact_number = $last_contact->open_id + 1;
        } else {
            $new_contact_number = 1;
        }

        $contact = new Contact;

        $contact->team_id = $team_id;
        $contact->open_id = $new_contact_number;

        $contact->name = ucwords(strtolower($request->name));
        $contact->email = strtolower($request->email);
        $contact->emails        = empty($request->emails) ? null : json_encode($request->emails); //
        
        $contact->phone = empty($request->phone) ? null : $request->phone;
        $contact->contact1 = ucwords(strtolower($request->contact1));
        $contact->contact2 = ucwords(strtolower($request->contact2));

        $contact->currency_id = empty($request->currency_id) ? 840 : $request->currency_id;
        $contact->payment_terms = empty($request->payment_terms) ? 15 : $request->payment_terms;

        $contact->bill_address1 = ucwords(strtolower($request->bill_address1));
        $contact->bill_address2 = ucwords(strtolower($request->bill_address2));

        $contact->bill_city = ucwords(strtolower($request->bill_city));
        $contact->bill_state = ucwords(strtolower($request->bill_state));
        $contact->bill_postal_code = $request->bill_postal_code;
        $contact->bill_country_id = $request->bill_country_id;

        $contact->ship_phone = $request->ship_phone;
        $contact->ship_contact = ucwords(strtolower($request->ship_contact));
        $contact->ship_address1 = ucwords(strtolower($request->ship_address1));
        $contact->ship_address2 = ucwords(strtolower($request->ship_address2));
        $contact->ship_city = ucwords(strtolower($request->ship_city));
        $contact->ship_state = $request->ship_state;
        $contact->ship_postal_code = $request->ship_postal_code;
        $contact->ship_country_id = $request->ship_country_id;
        $contact->instructions = $request->instructions;

        $contact->account_no = $request->account_no;
        $contact->id_no = $request->id_no;
        $contact->vat_no = $request->vat_no;
        $contact->fax_no = $request->fax_no;
        $contact->mobile_no = $request->mobile_no;
        $contact->toll_free_no = $request->toll_free_no;
        $contact->website = strtolower($request->website);
        //gst_code
        $contact->gst_code = empty($request->gst_code) ? null : $request->gst_code;

        $contact->created_by_id = $user_id;
        $contact->modified_by_id = $user_id;

        $contact->save();

        if ($contact->id) {

            $optimus = new Optimus(522588247, 282319719, 383644817);
            //image generation
            $encrypt_id = $optimus->encode($contact->id);

            $con_img = \DefaultProfileImage::create($request->name, 256, '#FFF', '#5fbeaa'); //
            \Storage::put("public/contacts/" . $encrypt_id . ".png", $con_img->encode());

            $contact = Contact::find($contact->id);
            $contact->image = "/storage/contacts/" . $encrypt_id . ".png";//$request->image;
            $contact->save();

        }

        return $contact;

    }

}