<?php

namespace App\Traits;

use App\Team;
use App\Team_Detail;
use App\Contact;
use App\UserInvoice;
use App\UserInvoiceItem;
use App\UserInvoiceTax;
use App\UserRecInvoice;

use App\Transaction;
use App\GL_Account;
use App\Localization;
use App\Country;

use App\UserGateway;
use App\StripeBilling;
use App\UserPayment;
use App\Accounting;
//use App\Traits\stripeBillingTrait;

use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Mail\Message;
use Jenssegers\Optimus\Optimus;

//use MongoDB\Driver\ReadConcern;
use Symfony\Component\HttpFoundation\Response;

trait invoiceTrait
{

    public function createInvoice(Request $request)
    {
        //
        $team_id      = Auth::user()->currentTeam->id;
        $user_id      = Auth::user()->id;

        $invoice                    = new UserInvoice;

        $invoice->team_id           = $team_id;

        if($request->contact_id) {
            $contact = Contact::where('team_id', '=', $team_id)
                ->where('open_id', '=', $request->contact_id)
                ->first();

            if(!$contact){
                return response()->json(false);
            }

            $invoice->contact_id = $contact->id;
        }
        else{

            return response()->json(false);
        }

        $invoice->status_id         = empty($request->status_id) ? 1 : $request->status_id;

        $invoice_title              = empty($request->title) ? null : ucwords(strtolower($request->title));
        $invoice->title             = empty($request->invoice_title) ? $invoice_title : ucwords(strtolower($request->invoice_title));

        $invoice_summary            = empty($request->summary) ? null : $request->summary;
        $invoice->summary           = empty($request->invoice_summary) ? $invoice_summary : $request->invoice_summary;

        if($request->open_id){

            $inv_number = $request->open_id;

        }else {

            $last_inv = UserInvoice::where('team_id','=',$team_id)
                ->orderBy('open_id','desc')
                ->first();          //returns last latest row

            if ($last_inv) {
                $inv_number = $last_inv->open_id + 1;
            } else {
                $inv_number = 1;
            }
        }

        $invoice->open_id           = $inv_number;

        //$invoice->customer_name     = empty($request->customer_name) ? null : ucwords(strtolower($request->customer_name));
        //$invoice->customer_email    = empty($request->customer_email) ? null : strtolower($request->customer_email);

        //$invoice->manual_id         = empty($request->manual_id) ? null : $request->manual_id;
        $invoice->poso_number       = empty($request->poso_number) ? null : $request->poso_number;

        $invoice->post_date         = Carbon::parse($request->invoice_date)->toDateTimeString();//$request->invoice_date;
        $invoice->due_date          = Carbon::parse($request->due_date)->toDateTimeString(); //$request->due_date;

        //$invoice->bill_to_addr      = empty($request->bill_to_addr) ? null : $request->bill_to_addr;
        //$invoice->ship_to_addr      = empty($request->ship_to_addr) ? null : $request->ship_to_addr;

        $invoice->debit_gl          = empty($request->debit_gl) ? null : $request->debit_gl;
        $invoice->credit_gl_cb      = empty($request->credit_gl_cb) ? false : $request->credit_gl_cb;
        //$invoice->phone           = $request->phone;
        //$invoice->ship_phone      = $request->ship_phone; //new fields commented at the moment

        $invoice->amount_paid       = 0;
        //empty($request->amount_paid) ? 0 : $request->amount_paid;
        $invoice->sub_total         = empty($request->sub_total) ? 0 : $request->sub_total;

        /*
        $invoice->is_recurring      = $request->is_recurring;
        $invoice->frequency_id      = $request->frequency_id;
        $invoice->start_date        = $request->start_date;
        $invoice->end_date          = $request->end_date;
        $invoice->last_sent_date    = $request->last_sent_date;
        $invoice->recurring_invoice_id  = $request->recurring_invoice_id;
        */

        //$invoice->discount_type   = $request->discount_type;
        $invoice->discount_rate1    = empty($request->discount_rate1) ? 0 : $request->discount_rate1;
        $invoice->discount_amount   = empty($request->discount_amount) ? 0 : $request->discount_amount;

        $invoice->ded_amount        = empty($request->ded_amount) ? 0 : $request->ded_amount;

        //$invoice->tax_rate1         = empty($request->tax_rate1) ? 0 : $request->tax_rate1;
        $invoice->tax_amount        = empty($request->tax_amount) ? 0 : round($request->tax_amount,2);

        $invoice->total_amount      = empty($request->total_amount) ? 0 : $request->total_amount;
        $invoice->balance           = empty($request->total_amount) ? 0 : $request->total_amount;
        //empty($request->balance) ? 0 : $request->balance;

        $invoice->currency_id       = empty($request->currency_id) ? 0 : $request->currency_id;
        $invoice->currency_id_local = empty($request->company_currency_id) ? 0 : $request->company_currency_id;

        $invoice->currency_rate     = empty($request->currency_rate) ? 1 : $request->currency_rate;

        $invoice->total_amount_local = $invoice->currency_rate * $invoice->total_amount;
        $invoice->amount_paid_local  = $invoice->currency_rate * $invoice->amount_paid;
        $invoice->balance_local      = $invoice->currency_rate * $invoice->balance;
        $invoice->tax_amount_local   = $invoice->currency_rate * $invoice->tax_amount;


        //$invoice->terms             = $request->terms;
        $invoice->notes             = empty($request->notes) ? null : $request->notes;
        $invoice->footer            = empty($request->footer) ? null : $request->footer;

        $invoice->created_by_id     = $user_id;
        $invoice->modified_by_id    = $user_id;

        $invoice->save();

        $optimus                    = new Optimus(522588247, 282319719, 383644817);
        $invoice->encrypted_id      = $optimus->encode($invoice->id);
        $invoice->save();

        //post a fin transaction - get open id
        $last_txn = Transaction::where('team_id','=',$team_id)
            ->orderBy('id','desc')
            ->first();          //returns last latest row

        if($last_txn) {
            $last_txn_number = $last_txn->open_id;
        }
        else {
            $last_txn_number = 0;
        }


        foreach($request->items as $item){

            $invoiceItem                = new UserInvoiceItem;

            $invoiceItem->team_id       = $team_id;
            $invoiceItem->invoice_num   = $invoice->id;
            //$invoiceItem->contact_id    = $request->contact_id;

            $invoiceItem->item_id       = empty($item['id']) ? $item['item_id'] : $item['item_id'];
            //need to be fixed
            //if $item['id'] found then we should reduce the quantity because that means product is
            //from the inventory

            $item_name                  = empty($item['item_name']) ? null : $item['item_name'];
            $invoiceItem->item_name     = empty($item['name']) ? $item_name : $item['name'];

            $item_price                 = empty($item['item_price']) ? 0 : $item['item_price'];
            $invoiceItem->item_price    = empty($item['price']) ? $item_price : $item['price'];

            $item_qty                   = empty($item['item_qty']) ? 0 : $item['item_qty'];
            $invoiceItem->item_qty      = empty($item['qty']) ? $item_qty : $item['qty'];

            $item_total                 = empty($item['item_total']) ? 0 : $item['item_total'];
            $invoiceItem->item_total    = empty($item['total']) ? $item_total : $item['total'];

            $invoiceItem->credit_gl     = empty($item['credit_gl']) ? null : $item['credit_gl'];

            $invoiceItem->save();

            if($invoice->credit_gl_cb and $invoice->status_id != 5) {

                if (!empty($item['credit_gl'])) {

                    $transaction = new Transaction;
                    $transaction->team_id = $team_id;

                    $last_txn_number = $last_txn_number + 1;

                    $transaction->open_id = $last_txn_number;

                    $transaction->category = 47;
                    $transaction->txn_type = "C";//$request->type;
                    $transaction->txn_date = $invoice->post_date;

                    $transaction->gl_account_id = $invoiceItem->credit_gl;
                    $transaction->txn_amount = $invoiceItem->item_total * $invoice->currency_rate;

                    $transaction->description = 'Invoice Sale';
                    $transaction->flag = 1;
                    $transaction->created_by_id = $user_id;
                    $transaction->modified_by_id = $user_id;

                    $transaction->save();

                    $gl_account = GL_Account::where('team_id', '=', $team_id)
                        ->where('open_id', '=', $invoiceItem->credit_gl)
                        ->first();

                    if ($gl_account) {
                        $gl_account->int_balance = $gl_account->int_balance - $transaction->txn_amount;
                    }

                    $gl_account->save();

                    $transaction->balance = $gl_account->int_balance;

                    //$transaction->txn_ref = ;
                    $transaction->inv_ref = $invoice->id;
                    //$transaction->pay_ref = ;

                    $transaction->save();

                }

            }

            if($invoice->status_id == 5)
            {
                UserInvoiceItem::find($invoiceItem->id)->delete();
            }


            $invoiceItem->hsnsac_code = empty($item['hsnsac_code']) ? null : $item['hsnsac_code'];
            $invoiceItem->save();

            $line_tax_items = empty($item['tax_items']) ? false : $item['tax_items'];
            $line_ded_items = empty($item['ded_items']) ? false : $item['ded_items'];

            if($line_tax_items) {

                /*
                foreach ($item['tax_items'] as $tax_row) {

                    $tax_items = $request->tax_items;
                    array_push($tax_items,$tax_row);

                }
                */

                foreach ($item['tax_items'] as $tax_row) {

                    $invoiceTaxItem = new UserInvoiceTax;

                    $invoiceTaxItem->team_id = $team_id;
                    $invoiceTaxItem->invoice_num = $invoice->id;
                    $invoiceTaxItem->item_num = $invoiceItem->id;

                    $tax_row_open_id = empty($tax_row['tax_open_id']) ? null : $tax_row['tax_open_id'];
                    $invoiceTaxItem->tax_open_id = empty($tax_row['id']) ? $tax_row_open_id : $tax_row['id'];

                    $invoiceTaxItem->tax_id = empty($tax_row['tax_id']) ? null : $tax_row['tax_id'];

                    $tax_row_tax_name = empty($tax_row['tax_name']) ? null : $tax_row['tax_name'];
                    $invoiceTaxItem->tax_name = empty($tax_row['name']) ? $tax_row_tax_name : $tax_row['name'];

                    $tax_row_tax_rate = empty($tax_row['tax_rate']) ? 0 : round($tax_row['tax_rate'], 3);
                    $invoiceTaxItem->tax_rate = empty($tax_row['rate']) ? $tax_row_tax_rate : round($tax_row['rate'], 3);

                    $tax_row_tax_amount = empty($tax_row['tax_amount']) ? 0 : round($tax_row['tax_amount'], 2);
                    $invoiceTaxItem->tax_amount = empty($tax_row['amount']) ? $tax_row_tax_amount : round($tax_row['amount'], 2);

                    $tax_row_tax_gl = empty($tax_row['tax_gl']) ? null : $tax_row['tax_gl'];
                    $invoiceTaxItem->tax_gl = empty($tax_row['gl_account_id']) ? $tax_row_tax_gl : $tax_row['gl_account_id'];

                    $invoiceTaxItem->tax_display = empty($tax_row['tax_display']) ? false : $tax_row['tax_display'];
                    $invoiceTaxItem->recoverable = empty($tax_row['recoverable']) ? false : $tax_row['recoverable'];
                    $invoiceTaxItem->compound = empty($tax_row['compound']) ? false : $tax_row['compound'];

                    $invoiceTaxItem->type = 'TAX';

                    $invoiceTaxItem->save();

                    if ($invoiceTaxItem->tax_gl and $invoice->status_id != 5) {

                        //start

                        //if($team_id == 1) {

                        $transaction = new Transaction;
                        $transaction->team_id = $team_id;

                        $last_txn_number = $last_txn_number + 1;

                        $transaction->open_id = $last_txn_number;

                        $transaction->category = 47;
                        $transaction->txn_type = "C";//$request->type; //credit
                        $transaction->txn_date = $invoice->post_date;

                        $transaction->gl_account_id = $invoiceTaxItem->tax_gl;
                        $transaction->txn_amount = $invoiceTaxItem->tax_amount * $invoice->currency_rate;

                        $transaction->description = 'Tax Item';
                        $transaction->flag = 1;
                        $transaction->created_by_id = $user_id;
                        $transaction->modified_by_id = $user_id;

                        $transaction->save();

                        $gl_account = GL_Account::where('team_id', '=', $team_id)
                            ->where('open_id', '=', $invoiceTaxItem->tax_gl)
                            ->first();

                        if ($gl_account) {
                            $gl_account->int_balance = $gl_account->int_balance - $transaction->txn_amount;
                            $gl_account->save();
                        }

                        $transaction->balance = $gl_account->int_balance;

                        //$transaction->txn_ref = ;
                        $transaction->inv_ref = $invoice->id;
                        //$transaction->pay_ref = ;

                        $transaction->save();

                        //}

                        //end


                    }

                    if ($invoice->status_id == 5) {
                        UserInvoiceTax::find($invoiceTaxItem->id)->delete();
                    }

                }

            }
            if($line_ded_items) {

                foreach($item['ded_items'] as $tax_row){

                    $invoiceTaxItem                = new UserInvoiceTax;

                    $invoiceTaxItem->team_id       = $team_id;
                    $invoiceTaxItem->invoice_num   = $invoice->id;
                    $invoiceTaxItem->item_num      = $invoiceItem->id;

                    $tax_row_open_id               = empty($tax_row['tax_open_id']) ? null : $tax_row['tax_open_id'];
                    $invoiceTaxItem->tax_open_id   = empty($tax_row['id']) ? $tax_row_open_id : $tax_row['id'];

                    $invoiceTaxItem->tax_id        = empty($tax_row['tax_id']) ? null : $tax_row['tax_id'];

                    $tax_row_tax_name              = empty($tax_row['tax_name']) ? null : $tax_row['tax_name'];
                    $invoiceTaxItem->tax_name      = empty($tax_row['name']) ? $tax_row_tax_name : $tax_row['name'];

                    $tax_row_tax_rate              = empty($tax_row['tax_rate']) ? 0 : round($tax_row['tax_rate'],3);
                    $invoiceTaxItem->tax_rate      = empty($tax_row['rate']) ? $tax_row_tax_rate : round($tax_row['rate'],3);

                    $tax_row_tax_amount            = empty($tax_row['tax_amount']) ? 0 : round($tax_row['tax_amount'],2);
                    $invoiceTaxItem->tax_amount    = empty($tax_row['amount']) ? $tax_row_tax_amount : round($tax_row['amount'],2);

                    $tax_row_tax_gl                = empty($tax_row['tax_gl']) ? null : $tax_row['tax_gl'];
                    $invoiceTaxItem->tax_gl        = empty($tax_row['gl_account_id']) ? $tax_row_tax_gl : $tax_row['gl_account_id'];

                    $invoiceTaxItem->tax_display   = empty($tax_row['tax_display']) ? false : $tax_row['tax_display'];
                    $invoiceTaxItem->recoverable   = empty($tax_row['recoverable']) ? false : $tax_row['recoverable'];
                    $invoiceTaxItem->compound      = empty($tax_row['compound']) ? false : $tax_row['compound'];

                    $invoiceTaxItem->type          = 'DED';

                    $invoiceTaxItem->save();

                    if ($invoiceTaxItem->tax_gl and $invoice->status_id != 5) {

                        //start

                        //if($team_id == 1) {

                        $transaction = new Transaction;
                        $transaction->team_id = $team_id;

                        $last_txn_number = $last_txn_number + 1;

                        $transaction->open_id = $last_txn_number;

                        $transaction->category = 47;
                        $transaction->txn_type = "C";//$request->type;//credit
                        $transaction->txn_date = $invoice->post_date;

                        $transaction->gl_account_id = $invoiceTaxItem->tax_gl;
                        $transaction->txn_amount = $invoiceTaxItem->tax_amount * $invoice->currency_rate;

                        $transaction->description = 'Deductible';
                        $transaction->flag = 1;
                        $transaction->created_by_id = $user_id;
                        $transaction->modified_by_id = $user_id;

                        $transaction->save();

                        $gl_account = GL_Account::where('team_id', '=', $team_id)
                            ->where('open_id', '=', $invoiceTaxItem->tax_gl)
                            ->first();

                        if ($gl_account) {
                            $gl_account->int_balance = $gl_account->int_balance - $transaction->txn_amount;
                            $gl_account->save();
                        }

                        $transaction->balance = $gl_account->int_balance;

                        //$transaction->txn_ref = ;
                        $transaction->inv_ref = $invoice->id;
                        //$transaction->pay_ref = ;

                        $transaction->save();

                        //}

                        //end


                    }

                    if($invoice->status_id == 5)
                    {
                        UserInvoiceTax::find($invoiceTaxItem->id)->delete();
                    }

                }

            }

        }

        foreach($request->tax_items as $tax_row){

            $invoiceTaxItem                = new UserInvoiceTax;

            $invoiceTaxItem->team_id       = $team_id;
            $invoiceTaxItem->invoice_num   = $invoice->id;

            $tax_row_open_id               = empty($tax_row['tax_open_id']) ? null : $tax_row['tax_open_id'];
            $invoiceTaxItem->tax_open_id   = empty($tax_row['id']) ? $tax_row_open_id : $tax_row['id'];

            $invoiceTaxItem->tax_id        = empty($tax_row['tax_id']) ? null : $tax_row['tax_id'];

            $tax_row_tax_name              = empty($tax_row['tax_name']) ? null : $tax_row['tax_name'];
            $invoiceTaxItem->tax_name      = empty($tax_row['name']) ? $tax_row_tax_name : $tax_row['name'];

            $tax_row_tax_rate              = empty($tax_row['tax_rate']) ? 0 : round($tax_row['tax_rate'],3);
            $invoiceTaxItem->tax_rate      = empty($tax_row['rate']) ? $tax_row_tax_rate : round($tax_row['rate'],3);

            $tax_row_tax_amount            = empty($tax_row['tax_amount']) ? 0 : round($tax_row['tax_amount'],2);
            $invoiceTaxItem->tax_amount    = empty($tax_row['amount']) ? $tax_row_tax_amount : round($tax_row['amount'],2);

            $tax_row_tax_gl                = empty($tax_row['tax_gl']) ? null : $tax_row['tax_gl'];
            $invoiceTaxItem->tax_gl        = empty($tax_row['gl_account_id']) ? $tax_row_tax_gl : $tax_row['gl_account_id'];

            $invoiceTaxItem->tax_display   = empty($tax_row['tax_display']) ? false : $tax_row['tax_display'];
            $invoiceTaxItem->recoverable   = empty($tax_row['recoverable']) ? false : $tax_row['recoverable'];
            $invoiceTaxItem->compound      = empty($tax_row['compound']) ? false : $tax_row['compound'];

            $invoiceTaxItem->type          = 'TAX';

            $invoiceTaxItem->save();

            if ($invoiceTaxItem->tax_gl and $invoice->status_id != 5) {

                //start

                //if($team_id == 1) {

                $transaction = new Transaction;
                $transaction->team_id = $team_id;

                $last_txn_number = $last_txn_number + 1;

                $transaction->open_id = $last_txn_number;

                $transaction->category = 47;
                $transaction->txn_type = "C";//$request->type; //credit
                $transaction->txn_date = $invoice->post_date;

                $transaction->gl_account_id = $invoiceTaxItem->tax_gl;
                $transaction->txn_amount = $invoiceTaxItem->tax_amount * $invoice->currency_rate;

                $transaction->description = 'Tax Item';
                $transaction->flag = 1;
                $transaction->created_by_id = $user_id;
                $transaction->modified_by_id = $user_id;

                $transaction->save();

                $gl_account = GL_Account::where('team_id', '=', $team_id)
                    ->where('open_id', '=', $invoiceTaxItem->tax_gl)
                    ->first();

                if ($gl_account) {
                    $gl_account->int_balance = $gl_account->int_balance - $transaction->txn_amount;
                    $gl_account->save();
                }

                $transaction->balance = $gl_account->int_balance;

                //$transaction->txn_ref = ;
                $transaction->inv_ref = $invoice->id;
                //$transaction->pay_ref = ;

                $transaction->save();

                //}

                //end


            }

            if($invoice->status_id == 5)
            {
                UserInvoiceTax::find($invoiceTaxItem->id)->delete();
            }

        }

        foreach($request->ded_items as $tax_row){

            $invoiceTaxItem                = new UserInvoiceTax;

            $invoiceTaxItem->team_id       = $team_id;
            $invoiceTaxItem->invoice_num   = $invoice->id;

            $tax_row_open_id               = empty($tax_row['tax_open_id']) ? null : $tax_row['tax_open_id'];
            $invoiceTaxItem->tax_open_id   = empty($tax_row['id']) ? $tax_row_open_id : $tax_row['id'];

            $invoiceTaxItem->tax_id        = empty($tax_row['tax_id']) ? null : $tax_row['tax_id'];

            $tax_row_tax_name              = empty($tax_row['tax_name']) ? null : $tax_row['tax_name'];
            $invoiceTaxItem->tax_name      = empty($tax_row['name']) ? $tax_row_tax_name : $tax_row['name'];

            $tax_row_tax_rate              = empty($tax_row['tax_rate']) ? 0 : round($tax_row['tax_rate'],3);
            $invoiceTaxItem->tax_rate      = empty($tax_row['rate']) ? $tax_row_tax_rate : round($tax_row['rate'],3);

            $tax_row_tax_amount            = empty($tax_row['tax_amount']) ? 0 : round($tax_row['tax_amount'],2);
            $invoiceTaxItem->tax_amount    = empty($tax_row['amount']) ? $tax_row_tax_amount : round($tax_row['amount'],2);

            $tax_row_tax_gl                = empty($tax_row['tax_gl']) ? null : $tax_row['tax_gl'];
            $invoiceTaxItem->tax_gl        = empty($tax_row['gl_account_id']) ? $tax_row_tax_gl : $tax_row['gl_account_id'];

            $invoiceTaxItem->tax_display   = empty($tax_row['tax_display']) ? false : $tax_row['tax_display'];
            $invoiceTaxItem->recoverable   = empty($tax_row['recoverable']) ? false : $tax_row['recoverable'];
            $invoiceTaxItem->compound      = empty($tax_row['compound']) ? false : $tax_row['compound'];

            $invoiceTaxItem->type          = 'DED';

            $invoiceTaxItem->save();

            if ($invoiceTaxItem->tax_gl and $invoice->status_id != 5) {

                //start

                //if($team_id == 1) {

                $transaction = new Transaction;
                $transaction->team_id = $team_id;

                $last_txn_number = $last_txn_number + 1;

                $transaction->open_id = $last_txn_number;

                $transaction->category = 47;
                $transaction->txn_type = "C";//$request->type;//credit
                $transaction->txn_date = $invoice->post_date;

                $transaction->gl_account_id = $invoiceTaxItem->tax_gl;
                $transaction->txn_amount = $invoiceTaxItem->tax_amount * $invoice->currency_rate;

                $transaction->description = 'Deductible';
                $transaction->flag = 1;
                $transaction->created_by_id = $user_id;
                $transaction->modified_by_id = $user_id;

                $transaction->save();

                $gl_account = GL_Account::where('team_id', '=', $team_id)
                    ->where('open_id', '=', $invoiceTaxItem->tax_gl)
                    ->first();

                if ($gl_account) {
                    $gl_account->int_balance = $gl_account->int_balance - $transaction->txn_amount;
                    $gl_account->save();
                }

                $transaction->balance = $gl_account->int_balance;

                //$transaction->txn_ref = ;
                $transaction->inv_ref = $invoice->id;
                //$transaction->pay_ref = ;

                $transaction->save();

                //}

                //end


            }

            if($invoice->status_id == 5)
            {
                UserInvoiceTax::find($invoiceTaxItem->id)->delete();
            }

        }

        //post financial txn
        //debit gl
        if ($request->debit_gl and $invoice->status_id != 5) {

            $last_txn = Transaction::where('team_id','=',$team_id)
                ->orderBy('id','desc')
                ->first();          //returns last latest row

            if($last_txn) {
                $last_txn_number = $last_txn->open_id + 1;
            }
            else {
                $last_txn_number = 1;
            }

            $transaction = new Transaction;
            $transaction->team_id = $team_id;

            $transaction->open_id = $last_txn_number;

            $transaction->category = 47;
            $transaction->txn_type = "D";//$request->type;
            $transaction->txn_date = $invoice->post_date;

            $transaction->gl_account_id  = $invoice->debit_gl;
            $transaction->txn_amount     = ( $invoice->balance * $invoice->currency_rate );

            $transaction->description    = 'Invoice Sale';
            //$transaction->flag           = 1;
            $transaction->created_by_id  = $user_id;
            $transaction->modified_by_id = $user_id;

            $transaction->save();

            $gl_account  = GL_Account::where('team_id','=',$team_id)
                ->where('open_id','=',$invoice->debit_gl)
                ->first();

            if($gl_account) {
                $gl_account->int_balance = $gl_account->int_balance + $transaction->txn_amount;
            }

            $gl_account->save();

            $transaction->balance = $gl_account->int_balance;

            //$transaction->txn_ref = ;
            $transaction->inv_ref = $invoice->id;
            //$transaction->pay_ref = ;

            $transaction->save();
        }


        if($invoice->status_id == 5)
        {

            UserInvoice::find($invoice->id)->delete();  //delete the invoice

            $recurring                      = $request->recurring;

            $last_rec_inv = UserRecInvoice::where('team_id','=',$team_id)
                ->orderBy('id','desc')
                ->first();          //returns last latest row

            if($last_rec_inv) {
                $new_recInv_number = $last_rec_inv->open_id + 1;
            }
            else {
                $new_recInv_number = 1;
            }

            $recInvoice                     = new UserRecInvoice;

            $recInvoice->team_id            = $team_id;
            $recInvoice->open_id            = $new_recInv_number;
            $recInvoice->status_id          = 1; //active/created

            $recInvoice->invoice_id         = $invoice->id;

            $recInvoice->freq               = empty($recurring['freq']) ? 30 : $recurring['freq'];
            $recInvoice->freq_type          = empty($recurring['freq_type']) ? 1 : $recurring['freq_type'];

            $recInvoice->reminder_to_cust   = empty($recurring['reminder_to_cust']) ? true : $recurring['reminder_to_cust'];
            $recInvoice->reminder_to_user   = empty($recurring['reminder_to_user']) ? true : $recurring['reminder_to_user'];
            $recInvoice->invoice_to_cust    = empty($recurring['invoice_to_cust']) ? true : $recurring['invoice_to_cust'];
            $recInvoice->invoice_to_user    = empty($recurring['invoice_to_user']) ? true : $recurring['invoice_to_user'];

            $recInvoice->ends               = empty($recurring['ends']) ? 1 : $recurring['ends'];//1:never

            $recInvoice->start_date         = empty($recurring['start_date']) ? null : $recurring['start_date'];
            $recInvoice->end_after_no_inv   = empty($recurring['end_after_no_inv']) ? 1 : $recurring['end_after_no_inv'];

            if($recInvoice->ends == 1){ //ends never

                $recInvoice->end_date       = Carbon::createFromFormat('Y-m-d', '9999-12-31')->toDateTimeString();
            }
            elseif($recInvoice->ends == 2) //specific date
            {
                $recInvoice->end_date       = empty($recurring['end_date']) ? null : $recurring['end_date'];
            }
            elseif($recInvoice->ends == 3) //# of invoices
            {
                //e.g. days = 45
                //check the start date
                //days * # of invoices
                $new_end_date_days = $recInvoice->freq * ($recInvoice->end_after_no_inv - 1);

                if($recInvoice->freq_type == 1) //days
                {
                    $new_end_date = Carbon::now()->addDays($new_end_date_days)->toDateTimeString();

                }
                elseif($recInvoice->freq_type == 2) //weeks
                {
                    $new_end_date = Carbon::now()->addWeeks($new_end_date_days)->toDateTimeString();

                }
                elseif($recInvoice->freq_type == 3) //months
                {
                    $new_end_date = Carbon::now()->addMonths($new_end_date_days)->toDateTimeString();
                }

                $recInvoice->end_date = $new_end_date;
            }

            $recInvoice->invoices_created   = 0;

            $recInvoice->created_by_id      =   $user_id;
            $recInvoice->modified_by_id     =   $user_id;

            $recInvoice->save();

        }

        $response = [
            'encrypted_id' => $invoice->encrypted_id,
            'open_id' => $invoice->open_id,
            'id' => $invoice->id
        ];

        return $response;

    }

    public function updateInvoice(Request $request)
    {
        //
        //return true;

        $team_id                    = Auth::user()->currentTeam->id;
        $user_id                    = Auth::user()->id;

        $invoice                    = new UserInvoice;

        $invoice->team_id           = $team_id;

        if($request->contact_id) {

            $contact = Contact::where('team_id', '=', $team_id)
                ->where('id', '=', $request->contact_id)
                ->first();

            if(!$contact){

                return response()->json(false);

            }

            $invoice->contact_id = $contact->id;
        }

        $invoice->status_id         = empty($request->status_id) ? 1 : $request->status_id;

        $invoice_title              = empty($request->title) ? null : ucwords(strtolower($request->title));
        $invoice->title             = empty($request->invoice_title) ? $invoice_title : ucwords(strtolower($request->invoice_title));

        $invoice_summary            = empty($request->summary) ? null : $request->summary;
        $invoice->summary           = empty($request->invoice_summary) ? $invoice_summary : $request->invoice_summary;

        if($request->open_id){

            $inv_number = $request->open_id;

        }else {

            $last_inv = UserInvoice::where('team_id','=',$team_id)
                ->orderBy('id','desc')
                ->first();          //returns last latest row

            if ($last_inv) {
                $inv_number = $last_inv->open_id + 1;
            } else {
                $inv_number = 1;
            }
        }

        $invoice->open_id           = $inv_number;

        $invoice->poso_number       = empty($request->poso_number) ? null : $request->poso_number;

        $invoice_date               = empty($request->invoice_date) ? Carbon::now()->toDateTimeString() : Carbon::parse($request->invoice_date)->toDateTimeString();
        $post_date                  = empty($request->post_date) ? $invoice_date : Carbon::parse($request->post_date)->toDateTimeString();
        $invoice->post_date         = $post_date;
        $invoice->due_date          = Carbon::parse($request->due_date)->toDateTimeString(); //$request->due_date;

        $invoice->debit_gl          = empty($request->debit_gl) ? null : $request->debit_gl;
        $invoice->credit_gl_cb      = empty($request->credit_gl_cb) ? false : $request->credit_gl_cb;

        $invoice->amount_paid       = 0;

        $invoice->sub_total         = empty($request->sub_total) ? 0 : $request->sub_total;

        $invoice->discount_rate1    = empty($request->discount_rate1) ? 0 : $request->discount_rate1;
        $invoice->discount_amount   = empty($request->discount_amount) ? 0 : $request->discount_amount;

        $invoice->ded_amount        = empty($request->ded_amount) ? 0 : $request->ded_amount;

        $invoice->tax_amount        = empty($request->tax_amount) ? 0 : round($request->tax_amount,2);

        $invoice->total_amount      = empty($request->total_amount) ? 0 : $request->total_amount;
        $invoice->balance           = empty($request->total_amount) ? 0 : $request->total_amount;

        $invoice->currency_id       = empty($request->currency_id) ? 0 : $request->currency_id;
        $invoice->currency_id_local = empty($request->company_currency_id) ? 0 : $request->company_currency_id;

        $invoice->currency_rate     = empty($request->currency_rate) ? 1 : $request->currency_rate;

        $invoice->total_amount_local = $invoice->currency_rate * $invoice->total_amount;
        $invoice->amount_paid_local  = $invoice->currency_rate * $invoice->amount_paid;
        $invoice->balance_local      = $invoice->currency_rate * $invoice->balance;

        $invoice->notes             = empty($request->notes) ? null : $request->notes;
        $invoice->footer            = empty($request->footer) ? null : $request->footer;

        $invoice->created_by_id     = $user_id;
        $invoice->modified_by_id    = $user_id;

        $invoice->save();

        $optimus                    = new Optimus(522588247, 282319719, 383644817);
        $invoice->encrypted_id      = $optimus->encode($invoice->id);
        $invoice->save();

        //post a fin transaction - get open id
        $last_txn = Transaction::where('team_id','=',$team_id)
            ->orderBy('id','desc')
            ->first();          //returns last latest row

        if($last_txn) {
            $last_txn_number = $last_txn->open_id;
        }
        else {
            $last_txn_number = 0;
        }


        //in store we used $request->items here we are using $request->inv_items_tab
        foreach($request->inv_items_tab as $item){

            $invoiceItem                = new UserInvoiceItem;

            $invoiceItem->team_id       = $team_id;
            $invoiceItem->invoice_num   = $invoice->id;
            //$invoiceItem->contact_id    = $request->contact_id;

            $item_id                    = empty($item['id']) ? null : $item['id'];
            $invoiceItem->item_id       = empty($item['item_id']) ? $item_id : $item['item_id'];
            //need to be fixed
            //if $item['id'] found then we should reduce the quantity because that means product is
            //from the inventory

            $item_name                  = empty($item['item_name']) ? null : $item['item_name'];
            $invoiceItem->item_name     = empty($item['name']) ? $item_name : $item['name'];

            $item_price                 = empty($item['item_price']) ? 0 : $item['item_price'];
            $invoiceItem->item_price    = empty($item['price']) ? $item_price : $item['price'];

            $item_qty                   = empty($item['item_qty']) ? 0 : $item['item_qty'];
            $invoiceItem->item_qty      = empty($item['qty']) ? $item_qty : $item['qty'];

            $item_total                 = empty($item['item_total']) ? 0 : $item['item_total'];
            $invoiceItem->item_total    = empty($item['total']) ? $item_total : $item['total'];

            $invoiceItem->credit_gl     = empty($item['credit_gl']) ? null : $item['credit_gl'];

            $invoiceItem->save();

            if($invoice->credit_gl_cb and $invoice->status_id != 5) {

                if (!empty($item['credit_gl'])) {

                    $transaction = new Transaction;
                    $transaction->team_id = $team_id;

                    $last_txn_number = $last_txn_number + 1;

                    $transaction->open_id = $last_txn_number;

                    $transaction->category = 47;
                    $transaction->txn_type = "C";//$request->type;
                    $transaction->txn_date = $invoice->post_date;

                    $transaction->gl_account_id = $invoiceItem->credit_gl;
                    $transaction->txn_amount = $invoiceItem->item_total * $invoice->currency_rate;

                    $transaction->description = 'Invoice Sale';
                    $transaction->flag = 1;
                    $transaction->created_by_id = $user_id;
                    $transaction->modified_by_id = $user_id;

                    $transaction->save();

                    $gl_account = GL_Account::where('team_id', '=', $team_id)
                        ->where('open_id', '=', $invoiceItem->credit_gl)
                        ->first();

                    if ($gl_account) {
                        $gl_account->int_balance = $gl_account->int_balance - $transaction->txn_amount;
                    }

                    $gl_account->save();

                    $transaction->balance = $gl_account->int_balance;

                    //$transaction->txn_ref = ;
                    $transaction->inv_ref = $invoice->id;
                    //$transaction->pay_ref = ;

                    $transaction->save();

                }

            }

            if($invoice->status_id == 5)
            {
                UserInvoiceItem::find($invoiceItem->id)->delete();
            }

            $invoiceItem->hsnsac_code = empty($item['hsnsac_code']) ? null : $item['hsnsac_code'];
            $invoiceItem->save();
            
        }

        foreach($request->tax_items as $tax_row){

            $invoiceTaxItem                = new UserInvoiceTax;

            $invoiceTaxItem->team_id       = $team_id;
            $invoiceTaxItem->invoice_num   = $invoice->id;

            $tax_row_open_id               = empty($tax_row['tax_open_id']) ? null : $tax_row['tax_open_id'];
            $invoiceTaxItem->tax_open_id   = empty($tax_row['id']) ? $tax_row_open_id : $tax_row['id'];
            //is it really an open id? it seems like we are storing id in the tax_open_id

            $invoiceTaxItem->tax_id        = empty($tax_row['tax_id']) ? null : $tax_row['tax_id'];

            $tax_row_tax_name              = empty($tax_row['tax_name']) ? null : $tax_row['tax_name'];
            $invoiceTaxItem->tax_name      = empty($tax_row['name']) ? $tax_row_tax_name : $tax_row['name'];

            $tax_row_tax_rate              = empty($tax_row['tax_rate']) ? 0 : round($tax_row['tax_rate'],3);
            $invoiceTaxItem->tax_rate      = empty($tax_row['rate']) ? $tax_row_tax_rate : round($tax_row['rate'],3);

            $tax_row_tax_amount            = empty($tax_row['tax_amount']) ? 0 : round($tax_row['tax_amount'],2);
            $invoiceTaxItem->tax_amount    = empty($tax_row['amount']) ? $tax_row_tax_amount : round($tax_row['amount'],2);

            $tax_row_tax_gl                = empty($tax_row['tax_gl']) ? null : $tax_row['tax_gl'];
            $invoiceTaxItem->tax_gl        = empty($tax_row['gl_account_id']) ? $tax_row_tax_gl : $tax_row['gl_account_id'];

            $invoiceTaxItem->tax_display   = empty($tax_row['tax_display']) ? false : $tax_row['tax_display'];
            $invoiceTaxItem->recoverable   = empty($tax_row['recoverable']) ? false : $tax_row['recoverable'];
            $invoiceTaxItem->compound      = empty($tax_row['compound']) ? false : $tax_row['compound'];

            $invoiceTaxItem->type          = 'TAX';

            $invoiceTaxItem->save();

            if ($invoiceTaxItem->tax_gl and $invoice->status_id != 5) {

                //start

                //if($team_id == 1) {

                $transaction = new Transaction;
                $transaction->team_id = $team_id;

                $last_txn_number = $last_txn_number + 1;

                $transaction->open_id = $last_txn_number;

                $transaction->category = 47;
                $transaction->txn_type = "C";//$request->type; //credit
                $transaction->txn_date = $invoice->post_date;

                $transaction->gl_account_id = $invoiceTaxItem->tax_gl;
                $transaction->txn_amount = $invoiceTaxItem->tax_amount * $invoice->currency_rate;

                $transaction->description = 'Tax Item';
                $transaction->flag = 1;
                $transaction->created_by_id = $user_id;
                $transaction->modified_by_id = $user_id;

                $transaction->save();

                $gl_account = GL_Account::where('team_id', '=', $team_id)
                    ->where('open_id', '=', $invoiceTaxItem->tax_gl)
                    ->first();

                if ($gl_account) {
                    $gl_account->int_balance = $gl_account->int_balance - $transaction->txn_amount;
                    $gl_account->save();
                }

                $transaction->balance = $gl_account->int_balance;

                //$transaction->txn_ref = ;
                $transaction->inv_ref = $invoice->id;
                //$transaction->pay_ref = ;

                $transaction->save();

                //}

                //end


            }

            if($invoice->status_id == 5)
            {
                UserInvoiceTax::find($invoiceTaxItem->id)->delete();
            }

        }

        foreach($request->ded_items as $tax_row){

            $invoiceTaxItem                = new UserInvoiceTax;

            $invoiceTaxItem->team_id       = $team_id;
            $invoiceTaxItem->invoice_num   = $invoice->id;

            $tax_row_open_id               = empty($tax_row['tax_open_id']) ? null : $tax_row['tax_open_id'];
            $invoiceTaxItem->tax_open_id   = empty($tax_row['id']) ? $tax_row_open_id : $tax_row['id'];

            $invoiceTaxItem->tax_id        = empty($tax_row['tax_id']) ? null : $tax_row['tax_id'];

            $tax_row_tax_name              = empty($tax_row['tax_name']) ? null : $tax_row['tax_name'];
            $invoiceTaxItem->tax_name      = empty($tax_row['name']) ? $tax_row_tax_name : $tax_row['name'];

            $tax_row_tax_rate              = empty($tax_row['tax_rate']) ? 0 : round($tax_row['tax_rate'],3);
            $invoiceTaxItem->tax_rate      = empty($tax_row['rate']) ? $tax_row_tax_rate : round($tax_row['rate'],3);

            $tax_row_tax_amount            = empty($tax_row['tax_amount']) ? 0 : round($tax_row['tax_amount'],2);
            $invoiceTaxItem->tax_amount    = empty($tax_row['amount']) ? $tax_row_tax_amount : round($tax_row['amount'],2);

            $tax_row_tax_gl                = empty($tax_row['tax_gl']) ? null : $tax_row['tax_gl'];
            $invoiceTaxItem->tax_gl        = empty($tax_row['gl_account_id']) ? $tax_row_tax_gl : $tax_row['gl_account_id'];

            $invoiceTaxItem->tax_display   = empty($tax_row['tax_display']) ? false : $tax_row['tax_display'];
            $invoiceTaxItem->recoverable   = empty($tax_row['recoverable']) ? false : $tax_row['recoverable'];
            $invoiceTaxItem->compound      = empty($tax_row['compound']) ? false : $tax_row['compound'];

            $invoiceTaxItem->type          = 'DED';

            $invoiceTaxItem->save();

            if ($invoiceTaxItem->tax_gl and $invoice->status_id != 5) {

                //start

                //if($team_id == 1) {

                $transaction = new Transaction;
                $transaction->team_id = $team_id;

                $last_txn_number = $last_txn_number + 1;

                $transaction->open_id = $last_txn_number;

                $transaction->category = 47;
                $transaction->txn_type = "C";//$request->type;//credit
                $transaction->txn_date = $invoice->post_date;

                $transaction->gl_account_id = $invoiceTaxItem->tax_gl;
                $transaction->txn_amount = $invoiceTaxItem->tax_amount * $invoice->currency_rate;

                $transaction->description = 'Deductible';
                $transaction->flag = 1;
                $transaction->created_by_id = $user_id;
                $transaction->modified_by_id = $user_id;

                $transaction->save();

                $gl_account = GL_Account::where('team_id', '=', $team_id)
                    ->where('open_id', '=', $invoiceTaxItem->tax_gl)
                    ->first();

                if ($gl_account) {
                    $gl_account->int_balance = $gl_account->int_balance - $transaction->txn_amount;
                    $gl_account->save();
                }

                $transaction->balance = $gl_account->int_balance;

                //$transaction->txn_ref = ;
                $transaction->inv_ref = $invoice->id;
                //$transaction->pay_ref = ;

                $transaction->save();

                //}

                //end


            }

            if($invoice->status_id == 5)
            {
                UserInvoiceTax::find($invoiceTaxItem->id)->delete();
            }

        }


        //post financial txn
        //debit gl
        if ($request->debit_gl and $invoice->status_id != 5) {

            $last_txn = Transaction::where('team_id','=',$team_id)
                ->orderBy('id','desc')
                ->first();          //returns last latest row

            if($last_txn) {
                $last_txn_number = $last_txn->open_id + 1;
            }
            else {
                $last_txn_number = 1;
            }

            $transaction = new Transaction;
            $transaction->team_id = $team_id;

            $transaction->open_id = $last_txn_number;

            $transaction->category = 47;
            $transaction->txn_type = "D";//$request->type;
            $transaction->txn_date = $invoice->post_date;

            $transaction->gl_account_id  = $invoice->debit_gl;
            $transaction->txn_amount     = ( $invoice->balance * $invoice->currency_rate );

            $transaction->description    = 'Invoice Sale';
            //$transaction->flag           = 1;
            $transaction->created_by_id  = $user_id;
            $transaction->modified_by_id = $user_id;

            $transaction->save();

            $gl_account  = GL_Account::where('team_id','=',$team_id)
                ->where('open_id','=',$invoice->debit_gl)
                ->first();

            if($gl_account) {
                $gl_account->int_balance = $gl_account->int_balance + $transaction->txn_amount;
            }

            $gl_account->save();

            $transaction->balance = $gl_account->int_balance;

            //$transaction->txn_ref = ;
            $transaction->inv_ref = $invoice->id;
            //$transaction->pay_ref = ;

            $transaction->save();
        }


        if($invoice->status_id == 5)
        {

            UserInvoice::find($invoice->id)->delete();  //delete the invoice

            $recurring                      = $request->recurring;

            $last_rec_inv = UserRecInvoice::where('team_id','=',$team_id)
                ->orderBy('id','desc')
                ->first();          //returns last latest row

            if($last_rec_inv) {
                $new_recInv_number = $last_rec_inv->open_id + 1;
            }
            else {
                $new_recInv_number = 1;
            }

            $recInvoice                     = new UserRecInvoice;

            $recInvoice->team_id            = $team_id;
            $recInvoice->open_id            = $new_recInv_number;
            $recInvoice->status_id          = 1; //active/created

            $recInvoice->invoice_id         = $invoice->id;

            $recInvoice->freq               = empty($recurring['freq']) ? 30 : $recurring['freq'];
            $recInvoice->freq_type          = empty($recurring['freq_type']) ? 1 : $recurring['freq_type'];

            $recInvoice->reminder_to_cust   = empty($recurring['reminder_to_cust']) ? true : $recurring['reminder_to_cust'];
            $recInvoice->reminder_to_user   = empty($recurring['reminder_to_user']) ? true : $recurring['reminder_to_user'];
            $recInvoice->invoice_to_cust    = empty($recurring['invoice_to_cust']) ? true : $recurring['invoice_to_cust'];
            $recInvoice->invoice_to_user    = empty($recurring['invoice_to_user']) ? true : $recurring['invoice_to_user'];

            $recInvoice->ends               = empty($recurring['ends']) ? 1 : $recurring['ends'];//1:never

            $recInvoice->start_date         = empty($recurring['start_date']) ? null : $recurring['start_date'];
            $recInvoice->end_after_no_inv   = empty($recurring['end_after_no_inv']) ? 1 : $recurring['end_after_no_inv'];

            if($recInvoice->ends == 1){ //ends never

                $recInvoice->end_date       = Carbon::createFromFormat('Y-m-d', '9999-12-31')->toDateTimeString();
            }
            elseif($recInvoice->ends == 2) //specific date
            {
                $recInvoice->end_date       = empty($recurring['end_date']) ? null : $recurring['end_date'];
            }
            elseif($recInvoice->ends == 3) //# of invoices
            {
                //e.g. days = 45
                //check the start date
                //days * # of invoices
                $new_end_date_days = $recInvoice->freq * ($recInvoice->end_after_no_inv - 1);

                if($recInvoice->freq_type == 1) //days
                {
                    $new_end_date = Carbon::now()->addDays($new_end_date_days)->toDateTimeString();

                }
                elseif($recInvoice->freq_type == 2) //weeks
                {
                    $new_end_date = Carbon::now()->addWeeks($new_end_date_days)->toDateTimeString();

                }
                elseif($recInvoice->freq_type == 3) //months
                {
                    $new_end_date = Carbon::now()->addMonths($new_end_date_days)->toDateTimeString();
                }

                $recInvoice->end_date = $new_end_date;
            }

            $recInvoice->invoices_created   = 0;

            $recInvoice->created_by_id      =   $user_id;
            $recInvoice->modified_by_id     =   $user_id;

            $recInvoice->save();

        }

        $destroy_old_invoice        = $this->destroyInvoice($request->old_open_id,$team_id,$user_id);

        if(!$destroy_old_invoice){
            return false;
        }

        $response = [
            'encrypted_id' => $invoice->encrypted_id,
            'open_id' => $invoice->open_id,
            'id' => $invoice->id
        ];

        return $response;

    }

    protected function destroyInvoice($id, $team_id, $user_id)
    {

        $open_id = $id; //system will pass open invoice number and not the invoice id

        $invoice = UserInvoice::where('team_id','=',$team_id)
            ->where('open_id','=',$open_id)
            ->first();          //returns single row

        if($invoice) {

            $invoice->modified_by_id = $user_id;   //user who deleted the contact
            $invoice->save();

            //find and delete invoice items
            UserInvoiceItem::where('team_id', '=', $team_id)
                ->where('invoice_num', '=', $invoice->id)
                ->delete();          //update multiple row

            $UserInvoiceTax = UserInvoiceTax::where('team_id', '=', $team_id)
                ->where('invoice_num', $invoice->id)
                ->delete();          //update multiple row

            //start of invoice related transactions
            $transactions = Transaction::where('team_id', '=', $team_id)
                ->where('inv_ref', $invoice->id)
                ->get();          //get multiple row

            foreach($transactions as $txn) {

                $gl_account = GL_Account::where('team_id', '=', $team_id)
                    ->where('open_id', '=', $txn->gl_account_id)
                    ->first();

                if ($gl_account) {

                    if ($txn->txn_type == 'C') {

                        $gl_account->int_balance = $gl_account->int_balance + $txn->txn_amount;

                    }
                    elseif($txn->txn_type == 'D')
                    {

                        $gl_account->int_balance = $gl_account->int_balance - $txn->txn_amount;

                    }

                    $gl_account->save();

                }

                //update transaction balances for transactions posted after this txn

                $post_txns    =   Transaction::where('team_id','=',$team_id)
                    ->where('id','>',$txn->id)
                    ->where('gl_account_id','=',$txn->gl_account_id)
                    ->get();

                foreach($post_txns as $txn_row)
                {

                    if ($txn->txn_type == 'C')
                    {
                        $txn_row->balance = $txn_row->balance + $txn->txn_amount;
                        $txn_row->save();
                    }
                    elseif ($txn->txn_type == 'D')
                    {
                        $txn_row->balance = $txn_row->balance - $txn->txn_amount;
                        $txn_row->save();
                    }

                }

                //end of post balance update

            }

            $transactions = Transaction::where('team_id', '=', $team_id)
                ->where('inv_ref', $invoice->id)
                ->delete();

            //end of invoice related transactions and gl update

            //find and delete transactions related to payments
            $user_payments = UserPayment::select('id')
                ->where('team_id', '=', $team_id)
                ->where('invoice_id', $invoice->id)
                ->get();          //may get multiple row

            //start of payment related transactions
            if($user_payments) {

                $plucked_pay = $user_payments->pluck('id');
                $payment_ids = $plucked_pay->all();

                $transactions = Transaction::where('team_id', '=', $team_id)
                    ->whereIn('pay_ref', $payment_ids)
                    ->get();

                foreach ($transactions as $txn) {

                    $gl_account = GL_Account::where('team_id', '=', $team_id)
                        ->where('open_id', '=', $txn->gl_account_id)
                        ->first();

                    if ($gl_account) {

                        if ($txn->txn_type == 'C') {

                            $gl_account->int_balance = $gl_account->int_balance + $txn->txn_amount;

                        } elseif ($txn->txn_type == 'D') {

                            $gl_account->int_balance = $gl_account->int_balance - $txn->txn_amount;

                        }

                        $gl_account->save();

                    }


                    //update transaction balances for transactions posted after this txn

                    $post_txns    =   Transaction::where('team_id','=',$team_id)
                                        ->where('id','>',$txn->id)
                                        ->where('gl_account_id','=',$txn->gl_account_id)
                                        ->get();

                    foreach($post_txns as $txn_row)
                    {

                        if ($txn->txn_type == 'C')
                        {
                            $txn_row->balance = $txn_row->balance + $txn->txn_amount;
                            $txn_row->save();
                        }
                        elseif ($txn->txn_type == 'D')
                        {
                            $txn_row->balance = $txn_row->balance - $txn->txn_amount;
                            $txn_row->save();
                        }

                    }

                    //end of post balance update



                }

                Transaction::where('team_id', '=', $team_id)
                    ->whereIn('pay_ref', $payment_ids)
                    ->delete();

            }
            //end of payment related transactions

            //find and delete the payments associated
            UserPayment::where('team_id', '=', $team_id)
                ->where('invoice_id', $invoice->id)
                ->delete();          //update multiple row

            //find and delete the invoice
            UserInvoice::find($invoice->id)->delete();

        }

        return true;

    }

    protected function sendEmail(Request $request, $id, $team_name, $team_id, $user_id, $from_email, $from_name)
    {

        //$team_name  = Auth::user()->currentTeam->name;
        //$team_id    = Auth::user()->currentTeam->id;
        //$user_id    = Auth::user()->id;

        $optimus = new Optimus(522588247, 282319719, 383644817);

        $invoice = UserInvoice::where('id','=',$optimus->decode($id))
            ->where('team_id','=',$team_id)
            ->with('contactTab','statusTab','invItemsTab','invTaxTab')
            ->first();

        //return $invoice;

        if($invoice) {

            $invitation_key = $id;   //need to use id and not the invoice number to avoid duplication

            /*PDF Related Code Starts*/

            $accounting     = 0;
            $gateway_id     = 0;
            $public_key     = 0;
            $inv_country    = Country::where('id','=',$invoice->currency_id)->first();
            $team           = Team::find($invoice->team_id);
            $command        = 'Invoice';
            $team_details   = Team_Detail::where('team_id','=',$invoice->team_id)->first();

            $pdf = \PDF::loadView('user.common.invquote.common_pdf',
                compact('invoice','invitation_key','gateway_id',
                    'public_key','accounting','inv_country',
                    'team','command','team_details'))
                ->setPaper('a4')->setOrientation('portrait')
                ->setOption('margin-bottom', 0)
                ->setOption('footer-right', '[page]');

            //$pdf_name   = 'Invoice_' . Carbon::parse('today')->toDateString() . '_' . rand(10000,99999) . '.pdf';
            $pdf_name   = 'Invoice_' . Carbon::parse('today')->toDateString() . '_' . Carbon::now()->timestamp . '.pdf';

            $pathToFile = storage_path('app/private/temp/invoices/'. $pdf_name);

            $pdf->save($pathToFile);//storage_path('app/private/temp/invoices/' . $pdf_name));

            /*PDF Related Code Ends*/

            $share_url = 'https://finance.pi.team/public/invoices/' . $invitation_key;
            $download_url = $share_url . '/download';

            /*
            $subject = 'Invoice # ' . $invoice->open_id . ' from ' . $team_name;

            if ($request->to_email1 and $request->to_email2) {
                $to_emails = array($request->to_email1, $request->to_email2);
            } elseif ($request->to_email1) {
                $to_emails = array($request->to_email1);
            }

            */

            $to_emails      = false;
            $cc_emails      = false; //$request->cc_emails;


            if(!$request->subject) {

                $customer = Contact::where('team_id', '=', $team_id)
                    ->where('id', '=', $invoice->contact_id)
                    ->first();

                if ($customer) {
                    $customer_name = $customer->name;
                } else {
                    $customer_name = '';
                }

                $subject = 'Invoice ' . '# ' . $invoice->open_id . ' for ' . $customer_name . ' from ' . $team->name;
            }
            else {
                $subject = $request->subject;
            }

            foreach($request->to_emails as $email)
            {
                if(strlen($email['id']) > 3) {
                    $to_emails[] = $email['id'];
                }
            }

            foreach($request->cc_emails as $email)
            {
                if(strlen($email['id']) > 3) {
                    $cc_emails[] = $email['id'];
                }
            }

            /*
            if($request->copy_myself){

                $cc_email[] = $from_email;
            }
            */

            $email_message  = $request->message;

            $localization   = Localization::where('team_id', '=', $team_id)->first();
            $inv_country    = Country::where('id', '=', $invoice->currency_id)->first();

            $type = 'Invoice';

            if ($cc_emails and count($cc_emails) > 0) {

                //$cc_email = $request->cc_email1;

                Mail::send('emails.invquote.send_invqt_email', compact('invoice', 'team_name', 'from_email',
                    'share_url', 'download_url', 'type', 'inv_country', 'email_message'),
                    function (Message $message) use ($subject, $from_email, $from_name, $to_emails, $cc_emails, $pathToFile) {
                        $message->subject($subject)
                            ->attach($pathToFile)
                            ->to($to_emails)// $contact->email $contact->name
                            ->cc($cc_emails)
                            ->from($from_email, $from_name)
                            ->replyTo($from_email, $from_name)
                            ->embedData([
                                'asm' => ['group_id' => 699],
                            ],
                                'sendgrid/x-smtpapi');

                    });

            } else {

                Mail::send('emails.invquote.send_invqt_email', compact('invoice', 'team_name', 'from_email',
                    'share_url', 'download_url', 'type', 'inv_country', 'email_message'),
                    function (Message $message) use ($subject, $from_email, $from_name, $to_emails, $pathToFile) {

                        $message->subject($subject)
                            ->attach($pathToFile)
                            ->to($to_emails)// $contact->email $contact->name
                            ->from($from_email, $from_name)
                            ->replyTo($from_email, $from_name)
                            ->embedData([
                                'asm' => ['group_id' => 699],
                            ],
                                'sendgrid/x-smtpapi');

                    });

            }

            return ('true');

            /*
            
            if ($cc_emails and count($cc_emails) > 0) {

                //$cc_email = $request->cc_email1;

                Mail::send('emails.invquote.send_email', compact('invoice', 'team_name', 'from_email',
                                                    'share_url', 'download_url', 'type', 'inv_country', 'email_message'),
                    function (Message $message) use ($subject, $from_email, $from_name, $to_emails, $cc_emails, $pathToFile) {
                        $message->subject($subject)
                            ->attach($pathToFile)
                            ->to($to_emails)// $contact->email $contact->name
                            ->cc($cc_emails)
                            ->from($from_email, $from_name)
                            ->replyTo($from_email, $from_name)
                            ->embedData([
                                'asm' => ['group_id' => 699],
                            ],
                                'sendgrid/x-smtpapi');

                    });

            } else {

                Mail::send('emails.invquote.send_email', compact('invoice', 'team_name', 'from_email',
                                                    'share_url', 'download_url', 'type', 'inv_country', 'email_message'),
                    function (Message $message) use ($subject, $from_email, $from_name, $to_emails, $pathToFile) {

                        $message->subject($subject)
                            ->attach($pathToFile)
                            ->to($to_emails)// $contact->email $contact->name
                            ->from($from_email, $from_name)
                            ->replyTo($from_email, $from_name)
                            ->embedData([
                                'asm' => ['group_id' => 699],
                            ],
                                'sendgrid/x-smtpapi');

                    });


            }

            return ('true');

            */

        }
        else{

           // return ('Error'); //redirect()->action('UserInvoiceController@index');
        }

    }

}