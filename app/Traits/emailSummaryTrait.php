<?php

namespace App\Traits;

use App\Team;
use App\Team_Detail;

use App\User;

use App\Contact;

use App\UserInvoice;
use App\UserInvoiceItem;
use App\UserInvoiceTax;
use App\UserRecInvoice;

use App\Transaction;
use App\GL_Account;
use App\Localization;
use App\Country;

use App\UserGateway;
use App\StripeBilling;
use App\UserPayment;
use App\Accounting;
//use App\Traits\stripeBillingTrait;

use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Mail\Message;
use Jenssegers\Optimus\Optimus;

use Symfony\Component\HttpFoundation\Response;

trait emailSummaryTrait
{

    protected function finSummary()
    {

        //$teams = Team::all();

        /*
        $teams = Team::where('id','=',1) //harry
                //->orWhere('id','=',4)
                //->orWhere('id','=',268)
                ->get();
        */

        //return response()->json($teams);


        foreach($teams as $team)
        {

            $team_id = $team->id;

            $user = User::where('id', '=', $team->owner_id)
                ->first();
            //return $team->name;


            if(!$user){
                //continue;
            }

            $team_name = $team->name;

            $recent_invoices = UserInvoice::orderBy('id', 'DESC')
                ->where('team_id', '=',$team_id)
                ->with('statusTab', 'contactTab')
                ->take(5)
                ->get();


            $invoice_count = empty($recent_invoices->count()) ? 0 : $recent_invoices->count();

            $lastweek_sunday = Carbon::parse('previous sunday')->toDateString();
            $lastweek_monday = Carbon::parse('previous sunday')->subDays(6)->toDateString();

            //$currentmonth           = Carbon::parse('this month')->format('F Y');
            $thismonth_start = Carbon::parse('first day of this month')->toDateString();
            $thismonth_end = Carbon::parse('last day of this month')->toDateString();

            $thisyr_start = Carbon::parse('first day of this year')->toDateString();
            $thisyr_end = Carbon::parse('last day of this year')->toDateString();

            $sales_last_week = UserInvoice::where('team_id', '=', $team_id)
                            ->where('post_date', '>=', $lastweek_monday)
                            ->where('post_date', '<=', $lastweek_sunday)
                            ->sum('total_amount');

            $sales_this_month = UserInvoice::where('team_id', '=', $team_id)
                            ->where('post_date', '>=', $thismonth_start)
                            ->where('post_date', '<=', $thismonth_end)
                            ->sum('total_amount');

            $sales_this_yr = UserInvoice::where('team_id', '=', $team_id)
                            ->where('post_date', '>=', $thisyr_start)
                            ->where('post_date', '<=', $thisyr_end)
                            ->sum('total_amount');

            $pay_last_week = UserPayment::where('team_id', '=', $team_id)
                            ->where('payment_date', '>=', $lastweek_monday)
                            ->where('payment_date', '<=', $lastweek_sunday)
                            ->sum('amount');

            $pay_this_month = UserPayment::where('team_id', '=', $team_id)
                            ->where('payment_date', '>=', $thismonth_start)
                            ->where('payment_date', '<=', $thismonth_end)
                            ->sum('amount');

            $pay_this_yr = UserPayment::where('team_id', '=', $team_id)
                            ->where('payment_date', '>=', $thisyr_start)
                            ->where('payment_date', '<=', $thisyr_end)
                            ->sum('amount');

            $localization = Localization::where('team_id', '=', $team_id)->first();

            $comp_country = Country::where('id', '=', $localization->currency_id)->first();

            $lastweek_monday_human = Carbon::parse('previous sunday')->subDays(6)->toFormattedDateString();
            $lastweek_sunday_human = Carbon::parse('previous sunday')->toFormattedDateString();

            $subject            = 'From your Pi.TEAM Account: Activity Report for ' . $lastweek_sunday_human;
            $from_email         = 'your-week@pimail.team';//Auth::user()->email;
            $from_name          = 'Team Pi';
            $to_emails          = $user->email; //'lineflux@gmail.com';
            $support_email      = 'support@pi.team';
            $support_name       = 'Pi Support';
            $cc_email           = '';
            $pathToFile         = '';


            Mail::send('emails.weekly.fin-summary', compact('recent_invoices',
                'sales_last_week', 'sales_this_month', 'pay_last_week', 'pay_this_month',
                'sales_this_yr', 'pay_this_yr', 'invoice_count', 'comp_country', 'team_name', 'lastweek_monday_human', 'lastweek_sunday_human'),
                function (Message $message) use (
                    $subject, $from_email, $from_name, $to_emails,
                    $cc_email, $pathToFile, $support_email, $support_name
                ) {
                    $message->subject($subject)
                        //->attach($pathToFile)
                        ->to($to_emails)// $contact->email $contact->name
                        //->cc($cc_email)
                        ->from($from_email, $from_name)
                        ->replyTo($support_email, $support_name)
                        ->embedData([
                            'asm' => ['group_id' => 693],
                        ],
                            'sendgrid/x-smtpapi');

                });


            echo $team->id . ',' . $team->name . ',';
            sleep(0.2);

        }




    }

}