<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    //    //    //
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function glAccountTab()
    {
        return $this->belongsTo('App\GL_Account','gl_account_id','open_id');
    }

    public function txnCatTab()
    {
        return $this->belongsTo('App\Txn_Category','category'); //hit the primary key in the txn_categories table
    }

    public function txnTypeTab()
    {
        return $this->belongsTo('App\Txn_Type','txn_type','type'); //hit the primary key in the txn_categories table
    }

    public function paymentTab()
    {
        return $this->belongsTo('App\UserPayment','pay_ref','id');
    }

    public function invoiceTab()
    {
        return $this->belongsTo('App\UserInvoice','inv_ref','id');
    }

}
