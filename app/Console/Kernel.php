<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App\Contact;
use App\UserInvoice;
use App\UserInvoiceItem;
use App\UserInvoiceTax;
use App\UserRecInvoice;

//use Carbon\Carbon;
//use App\Traits\emailSummaryTrait;


class Kernel extends ConsoleKernel
{

    //use emailSummaryTrait;
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        /*
        $schedule->call(function () {

            //DB::table('recent_users')->delete();
            //$contact            = new Contact;
            //$contact->team_id   = 1;
            //$contact->name      = 'test';
            //$contact->save();

            $recInvList = UserRecInvoice::where('start_date','<=',Carbon::now())
                ->where('end_date','>=',Carbon::now())
                ->get();          //return all eligible rows

            foreach($recInvList as $recInv)
            {
                if($recInv->freq_type == 1) //days
                {
                    //11th april repeat every 25 days //end date
                    //start date + ( no. of days * 0 ) = create 1st
                    //start date + ( no. of days * 1 ) = create 2nd
                    $invoice_date = Carbon::parse($recInv->start_date)
                        ->addDays($recInv->freq * $recInv->invoices_created)->toDateTimeString();

                    if($invoice_date <= Carbon::now()->toDateTimeString())
                    {
                        //then proceed
                        //< means past date, may be we need to create more than 1 invoice

                        //retrieve template (invoice, items and tax)
                        //create new invoice
                        //copy template

                    }

                }
                elseif($recInv->freq_type == 2) //weeks
                {
                    //11th april repeat every 2 weeks //end date 08/15 //10 invoices
                    $invoice_date = Carbon::parse($recInv->start_date)
                        ->addWeeks($recInv->freq * $recInv->invoices_created)->toDateTimeString();

                    if($invoice_date <= Carbon::now()->toDateTimeString())
                    {
                        //then proceed
                        //< means past date, may be we need to create more than 1 invoice
                    }

                }
                elseif($recInv->freq_type == 3) //months
                {
                    $invoice_date = Carbon::parse($recInv->start_date)
                        ->addMonths($recInv->freq * $recInv->invoices_created)->toDateTimeString();

                    if($invoice_date <= Carbon::now()->toDateTimeString())
                    {
                        //then proceed
                        //< means past date, may be we need to create more than 1 invoice
                    }
                }
            }

        })->yearly();
        */

        /*
        $schedule->call(function () {

            //$this->finSummary();

            Mail::raw('Text to e-mail', function ($message) {

                $message->from('support@pi.team', 'Your Application');

                $message->to('lineflux@gmail.com', 'test')->subject('Your Reminder!');
                //
            });


        })->everyMinute();
        */
            //->emailOutputTo('avansaberinc@gmail.com');//->mondays()->at('8:00');

    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
