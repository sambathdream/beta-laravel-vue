<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserCreditItem extends Model
{
    //
    use SoftDeletes;

    protected $table = 'user_credit_items';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function creditTab()
    {
        return $this->belongsTo('App\UserCredit','creditnote_num','id');
    }
}
