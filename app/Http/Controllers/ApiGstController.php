<?php

namespace App\Http\Controllers;

use App\Hsnsac;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiGstController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listGSTHsnSac(Request $request){
        //need to fix this - we can't return ID to the frontend
        $q = $request->input('q');

        $team_id = Auth::user()->currentTeam->id;
        //$user_id = Auth::user()->id;

        if ($q) {
            $hsnsac_list = Hsnsac:://where('team_id', '=', $team_id)
                where('category', 'like', '%' . $q . '%')
                ->orWhere('sub_category','like','%'.$q.'%')
                ->orWhere('service','like','%'.$q.'%')
                ->orWhere('code','like','%'.$q.'%')
                ->get();
        } else {
            $hsnsac_list = Hsnsac:://where('team_id', '=', $team_id)
                all();
            //::select(['id', 'name', 'email','image','bill_address1','bill_address2'])
            //->get()

        }

        //return Contact::find(1);
        if ($q) {
            $returnJson["total_count"] = count($hsnsac_list);
            $returnJson["incomplete_results"] = false;
            $returnJson["items"] = $hsnsac_list;

            return response()->json($returnJson);
        } else
            return response()->json($hsnsac_list);
    }

}
