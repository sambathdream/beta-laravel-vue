<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Traits\creditNoteTrait;

use App\UserCredit;

use Illuminate\Support\Facades\Auth;
use Jenssegers\Optimus\Optimus;

class ApiCreditNoteController extends Controller
{
    use creditNoteTrait;

    public function __construct()
    {

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $team_id = Auth::user()->currentTeam->id;

        $request = request();

        if (request()->has('sort')) {
            list($sortCol, $sortDir) = explode('|', request()->sort);
            $query = UserCredit::orderBy($sortCol, $sortDir)
                ->with('creditItemsTab','creditTaxTab','contactTab','statusTab')
                ->where('team_id', '=', $team_id);
        } else {
            $query = UserCredit::orderBy('id', 'asc')
                ->with('creditItemsTab','creditTaxTab','contactTab','statusTab')
                ->where('team_id', '=', $team_id);
        }
        if ($request->exists('filter')) {
            $query->where(function ($q) use ($request) {
                $value = "%{$request->filter}%";
                $q->where('amount', 'like', $value);
            });
        }

        $perPage = request()->has('per_page') ? (int) request()->per_page : null;
// The headers 'Access-Control-Allow-Origin' and 'Access-Control-Allow-Methods'
// are to allow you to call this from any domain (see CORS for more info).
// This is for local testing only. You should not do this in production server,
// unless you know what it means.
        return response()->json($query->paginate($perPage));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //return response()->json($request);

        $invoice_flag    = $this->createCredit($request);

        if($invoice_flag)
        {
            return response()->json($invoice_flag);
        }
        else {
            return response()->json([
                'error' => 'Contact Not Found!',
            ], 404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $team_id      = Auth::user()->currentTeam->id;
        $user_id      = Auth::user()->id;

        $response = $this->destroyCredit($id,$team_id,$user_id);

        return response()->json($response);

    }

    /**
     *
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sendCreditNote(Request $request, $id)
    {

        $team_name  = Auth::user()->currentTeam->name;
        $team_id    = Auth::user()->currentTeam->id;
        $user_id    = Auth::user()->id;

        $from_email = Auth::user()->email;
        $from_name  = Auth::user()->name;

        if($team_id == 1)
        {
            //return response()->json($request);
        }

        return $this->sendCreditNoteEmail($request, $id, $team_name, $team_id, $user_id, $from_email, $from_name);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function validateCreditNoteID($id)
    {
        //
        $team_id      = Auth::user()->currentTeam->id;
        $user_id      = Auth::user()->id;

        $found_invoice = UserCredit::where('team_id','=',$team_id)
                        ->where('open_id','=',$id)
                        ->first();          //returns last latest row

        if($found_invoice) {

            return response()->json(false);    //means duplicate invoice exists

        }
        else {

            return response()->json(true);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function previousCreditNoteManID()
    {
        //
        $team_id      = Auth::user()->currentTeam->id;
        $user_id      = Auth::user()->id;

        $last_invoice = UserCredit::where('team_id','=',$team_id)
            ->orderBy('id','desc')
            ->first();          //returns last latest row

        if($last_invoice) {

            $old_open_id  = $last_invoice->open_id;

            $numbers = preg_replace('/[^0-9]/', '', $old_open_id);
            $letters = preg_replace('/[^a-zA-Z]/', '', $old_open_id);

            $new_open_id = $letters . ( $numbers + 1 );

        }
        else {
            $new_open_id = 1;
            $old_open_id = 0;
        }

        $response = [
            'old_open_id' => $old_open_id,
            'new_open_id' => $new_open_id
        ];

        return response()->json($response);

    }

}
