<?php

namespace App\Http\Controllers;

use App\Country;
use App\Team_Detail;
use App\Localization;
use App\Tax_Rate;
use App\Accounting;
use App\Team;
use App\StripeBilling;

use GeoIP;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Optimus\Optimus;

class ApiConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        //id = 1 = company details
        $user_id = Auth::user()->id;
        $team_id = Auth::user()->currentTeam->id;

        if($id == 1) {
            $team_detail = Team_Detail::where('team_id', '=', $team_id)
                ->first();

            $billing_country = Auth::user()->currentTeam->billing_country;

            $country_id = Country::where('iso_3166_2','=',$billing_country)->first();

            $response = [
                'address' => [
                    'name' => empty(Auth::user()->currentTeam->name) ? null : Auth::user()->currentTeam->name,
                    'billing_address' => empty(Auth::user()->currentTeam->billing_address) ? null : Auth::user()->currentTeam->billing_address,
                    'billing_address_line_2' => empty(Auth::user()->currentTeam->billing_address_line_2) ? null : Auth::user()->currentTeam->billing_address_line_2,
                    'billing_city' => empty(Auth::user()->currentTeam->billing_city) ? null : Auth::user()->currentTeam->billing_city,
                    'billing_state' => empty(Auth::user()->currentTeam->billing_state) ? null : Auth::user()->currentTeam->billing_state,
                    'billing_zip' => empty(Auth::user()->currentTeam->billing_zip) ? null : Auth::user()->currentTeam->billing_zip,
                    'billing_country' => empty($billing_country) ? null : $billing_country,
                    'billing_country_id' => empty($country_id->id) ? null : $country_id->id,
                ],
                'user_email' => empty(Auth::user()->email) ? null : Auth::user()->email,
                'settings' => $team_detail
            ];
        }
        elseif($id == 2) {
            $localization = Localization::where('team_id', '=', $team_id)
                ->first();

            $currency = Country::where('id','=',$localization->currency_id)->first();

            $response = [
                'settings' => $localization,
                'currency_code' => $currency->currency_code
            ];
        }
        elseif($id == 3) {
            $tax_rates = Tax_Rate::where('team_id', '=', $team_id)
                ->where('type','=','TAX')
                ->with('glAccountTab')
                ->get();

            $response = $tax_rates;

            foreach ($response as $key => $row)
            {
                $response[$key]['gl_account_name'] = $row->glAccountTab['name'];
            }

        }
        elseif($id == 4) {
            $accounting = Accounting::where('team_id', '=', $team_id)->first();

            $response = $accounting;
        }
        elseif($id == 6) {

            $stripe_billing = StripeBilling::where('team_id', '=', $team_id)->first();

            if(!$stripe_billing){

                $stripe_billing = new StripeBilling;

                $stripe_billing->team_id = $team_id;
                $stripe_billing->frequency = 3; //monthly
                $stripe_billing->signingsecret = null;
                $stripe_billing->created_by_id = $user_id;
                $stripe_billing->modified_by_id = $user_id;

                $stripe_billing->auto_email = false;
                $stripe_billing->from_email = '';
                $stripe_billing->cc_email = '';

                $optimus = new Optimus(914614073, 864868105, 2138572997);

                $stripe_billing->webhook = "https://finance.pi.team/public/payments/stripebill/" .  $optimus->encode($stripe_billing->team_id);

                $stripe_billing->save();

                //$stripe_billing = StripeBilling::where('team_id', '=', $team_id)->first();

            }

            $response = $stripe_billing;
        }
        elseif($id == 7) {
            $tax_rates = Tax_Rate::where('team_id', '=', $team_id)
                ->where('type','=','DED')  //deductibles
                ->with('glAccountTab')
                ->get();

            $response = $tax_rates;

            foreach ($response as $key => $row)
            {
                $response[$key]['gl_account_name'] = $row->glAccountTab['name'];
            }

        }

        return response()->json($response);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user_id = Auth::user()->id;
        $team_id = Auth::user()->currentTeam->id;

        if($id == 1) {

            $team_detail = Team_Detail::firstOrNew(['team_id' => $team_id]);

            $team_detail->id_number     = $request->id_number;
            $team_detail->vat_number    = $request->vat_number;
            $team_detail->website       = $request->website;
            $team_detail->phone         = $request->phone;
            $team_detail->company_size  = $request->company_size;
            $team_detail->industry      = $request->industry;

            $team_detail->email         = $request->email;
            $team_detail->inv_footer    = $request->inv_footer;
            $team_detail->inv_notes     = $request->inv_notes;

            $team_detail->email_subject   = $request->email_subject;
            $team_detail->quote_footer    = $request->quote_footer;
            $team_detail->quote_notes     = $request->quote_notes;

            $team_detail->imageSrc      = $request->imageSrc;

            $team_detail->gst_code      = empty($request->gst_code) ? null : $request->gst_code;
            $team_detail->line_tax      = empty($request->line_tax) ? false : $request->line_tax;

            //$team_detail->created_by_id   = $user_id;
            $team_detail->modified_by_id  = $user_id;

            $team_detail->save();

        }
        elseif($id == 2) {

            $localization = Localization::firstOrNew(['team_id' => $team_id]);

            //return ($request->week_first_day);

            $localization->currency_id = $request->currency_id;
            $localization->language_id = $request->language_id;
            $localization->timezone = $request->timezone;
            $localization->date_time_format = $request->date_time_format;
            $localization->week_first_day = $request->week_first_day;
            $localization->year_first_month = $request->year_first_month;
            $localization->first_time   = false;

            //$localization->created_by_id   = $user_id;
            $localization->modified_by_id  = $user_id;

            $localization->save();
        }
        elseif($id == 3) {

            $last_taxrate = Tax_Rate::where('team_id','=',$team_id)
                ->where('type','=','TAX')
                ->orderBy('created_at','desc')
                ->first();          //returns last latest row

            if($last_taxrate) {
                $new_tax_rate = $last_taxrate->open_id + 1;
            }
            else {
                $new_tax_rate = 1;
            }

            $tax_rate = new Tax_Rate;

            $tax_rate->team_id = $team_id;
            $tax_rate->open_id = $new_tax_rate;
            $tax_rate->name = $request->name;
            $tax_rate->rate = $request->rate;

            $tax_rate->tax_id       = empty($request->tax_id) ? null : $request->tax_id;

            $tax_rate->gl_account_id  = empty($request->gl_account_open_id) ? null : $request->gl_account_open_id;

            $tax_rate->tax_display  = empty($request->tax_display) ? false : $request->tax_display; //display tax number on the invoice
            $tax_rate->recoverable  = empty($request->recoverable) ? false : $request->recoverable;
            $tax_rate->compound     = empty($request->compound) ? false : $request->compound;


            //$tax_rate->created_by_id   = $user_id;
            $tax_rate->modified_by_id  = $user_id;

            $tax_rate->type         = 'TAX';

            $tax_rate->save();
        }
        elseif($id == 4) {


            $accounting = Accounting::firstOrNew(['team_id' => $team_id]);

            $accounting->paymentcr = $request->paymentcr;
            $accounting->paymentdb = $request->paymentdb;

            //$accounting->created_by_id   = $user_id;
            $accounting->modified_by_id  = $user_id;

            $accounting->save();

            //return ($request->paymentcr);

        }
        elseif($id == 5) {

            $team = Team::find($team_id);

            $team->billing_address          = $request->billing_address;
            $team->billing_address_line_2   = $request->billing_address_line_2;
            $team->billing_city             = $request->billing_city;
            $team->billing_state            = $request->billing_state;
            $team->billing_zip              = $request->billing_zip;
            $team->billing_country          = $request->billing_country;

            $team->save();

            //return ($request->paymentcr);

        }
        elseif($id == 6) {

            $stripe_billing = StripeBilling::where('team_id','=',$team_id)
                ->first();

            //$stripe_billing->team_id = $team_id;
            $stripe_billing->frequency      = $request->frequency; //monthly
            $stripe_billing->signingsecret  = $request->signingsecret;

            $stripe_billing->modified_by_id = $user_id;

            $stripe_billing->auto_email = $request->auto_email;
            $stripe_billing->from_email = $request->from_email;
            $stripe_billing->cc_email   = $request->cc_email;


            $stripe_billing->save();

            //return ($request->paymentcr);

        }
        elseif($id == 7) {

            $last_taxrate = Tax_Rate::where('team_id','=',$team_id)
                ->where('type','=','DED')
                ->orderBy('created_at','desc')
                ->first();          //returns last latest row

            if($last_taxrate) {
                $new_tax_rate = $last_taxrate->open_id + 1;
            }
            else {
                $new_tax_rate = 1;
            }

            $tax_rate = new Tax_Rate;

            $tax_rate->team_id = $team_id;
            $tax_rate->open_id = $new_tax_rate;
            $tax_rate->name = $request->name;
            $tax_rate->rate = $request->rate;

            $tax_rate->tax_id       = empty($request->tax_id) ? null : $request->tax_id;

            $tax_rate->gl_account_id  = empty($request->gl_account_open_id) ? null : $request->gl_account_open_id;

            $tax_rate->tax_display  = empty($request->tax_display) ? false : $request->tax_display; //display tax number on the invoice
            $tax_rate->recoverable  = empty($request->recoverable) ? false : $request->recoverable;
            $tax_rate->compound     = empty($request->compound) ? false : $request->compound;


            //$tax_rate->created_by_id   = $user_id;
            $tax_rate->modified_by_id  = $user_id;

            $tax_rate->type         = 'DED';

            $tax_rate->save();
        }

        return ('true');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $user_id  = Auth::user()->id;
        $team_id  = Auth::user()->currentTeam->id;

        $tax_rate = Tax_Rate::where('team_id', '=', $team_id)
            ->where('id', '=', $id)
            ->first();

        if($tax_rate) {

            $id                       = $tax_rate->id;
            $tax_rate->modified_by_id = $user_id;   //user who deleted the contact
            $tax_rate->save();

            Tax_Rate::find($id)->delete();

            return response()->json('true');
        }

        return response()->json('false');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function findLocation()
    {
        //
        $ip = (string)GeoIP::getClientIP();
        $location = geoip()->getLocation($ip);
        //(object) geoip()->getLocation($ip)->toArray();
        //response()->json(geoip()->getLocation($ip)->toArray());

        if ($location->iso_code) {
            $country = Country::where('iso_3166_2', '=', $location->iso_code)->first();
        }
        elseif ($location->currency) {
            $country = Country::where('currency_code', '=', $location->currency)->first();
        }
        elseif ($location->country) {
            $country = Country::where('name', '=', $location->country)->first();
        }

        $response = [
            'location' => $location->toArray(),
            'country' => $country
        ];

        return response()->json($response);

    }

}
