<?php

namespace App\Http\Controllers;

use App\Team;
use App\Team_Detail;
use App\Contact;
use App\UserQuote;
use App\UserQuoteItem;
use App\Localization;
use App\Country;
use App\UserQuoteTax;

use App\Transaction;
use App\GL_Account;

use App\Traits\invoiceTrait;

use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Mail\Message;
use Jenssegers\Optimus\Optimus;

class ApiQuotationController extends Controller
{

    use invoiceTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        ////
        $team_id = Auth::user()->currentTeam->id;

        $request = request();

        if (request()->has('sort')) {
            list($sortCol, $sortDir) = explode('|', request()->sort);
            $query = UserQuote::orderBy($sortCol, $sortDir)
                ->with('statusTab','contactTab')
                ->where('team_id','=',$team_id); //need to replace company with team
        } else {
            $query = UserQuote::orderBy('id', 'asc')
                ->with('statusTab','contactTab')
                ->where('team_id','=',$team_id);
        }

        //temp comment - please remove this once below fields confirmed

        if ($request->exists('filter')) {
            $query->where(function($q) use($request) {
                $value = "%{$request->filter}%";
                $q->where('name', 'like', $value); //->where('text', 'like', $value)
            });
        }


        $perPage = request()->has('per_page') ? (int) request()->per_page : null;

// The headers 'Access-Control-Allow-Origin' and 'Access-Control-Allow-Methods'
// are to allow you to call this from any domain (see CORS for more info).
// This is for local testing only. You should not do this in production server,
// unless you know what it means.
        return response()->json($query->paginate($perPage));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //
        $team_id      = Auth::user()->currentTeam->id;
        $user_id      = Auth::user()->id;

        $quote                    = new UserQuote;

        $quote->team_id           = $team_id;

        if($request->contact_id) {
            $contact = Contact::where('team_id', '=', $team_id)
                ->where('open_id', '=', $request->contact_id)
                ->first();

            if(!$contact){
                return response()->json(false);
            }

            $quote->contact_id = $contact->id;
        }

        if($request->open_id){

            $quote_number = $request->open_id;

        }else {

            $last_quote = UserQuote::where('team_id','=',$team_id)
                ->orderBy('open_id','desc')
                ->first();          //returns last latest row

            if ($last_quote) {
                $quote_number = $last_inv->open_id + 1;
            } else {
                $quote_number = 1;
            }
        }

        $quote->title             = empty($request->invoice_title) ? null : ucwords(strtolower($request->invoice_title));
        $quote->summary           = empty($request->invoice_summary) ? null : $request->invoice_summary;

        $quote->open_id           = $quote_number; //empty($request->open_id) ? null : $request->open_id;

        $quote->customer_name     = empty($request->customer_name) ? null : ucwords(strtolower($request->customer_name));
        $quote->customer_email    = empty($request->customer_email) ? null : strtolower($request->customer_email);

        $quote->poso_number       = empty($request->poso_number) ? null : $request->poso_number;
        $quote->post_date         = $request->invoice_date;
        $quote->due_date          = $request->due_date;

        $quote->debit_gl        = empty($request->debit_gl) ? null : $request->debit_gl;
        $quote->credit_gl_cb    = empty($request->credit_gl_cb) ? true : $request->credit_gl_cb;

        $quote->amount_paid       = 0;

        $quote->sub_total         = empty($request->sub_total) ? 0 : $request->sub_total;

        $quote->discount_rate1    = empty($request->discount_rate1) ? 0 : $request->discount_rate1;
        $quote->discount_amount   = empty($request->discount_amount) ? 0 : $request->discount_amount;

        $quote->ded_amount        = empty($request->ded_amount) ? 0 : $request->ded_amount;

        $quote->tax_amount        = empty($request->tax_amount) ? 0 : $request->tax_amount;

        $quote->total_amount      = empty($request->total_amount) ? 0 : $request->total_amount;
        $quote->balance           = empty($request->total_amount) ? 0 : $request->total_amount;

        $quote->currency_id       = empty($request->currency_id) ? 0 : $request->currency_id;
        $quote->currency_id_local = empty($request->company_currency_id) ? 0 : $request->company_currency_id;

        $quote->currency_rate     = empty($request->currency_rate) ? 1 : $request->currency_rate;

        $quote->total_amount_local = $quote->currency_rate * $quote->total_amount;
        $quote->amount_paid_local  = $quote->currency_rate * $quote->amount_paid;
        $quote->balance_local      = $quote->currency_rate * $quote->balance;

        $quote->notes             = empty($request->notes) ? null : $request->notes;
        $quote->footer            = empty($request->footer) ? null : $request->footer;

        $quote->created_by_id     = $user_id;
        $quote->modified_by_id    = $user_id;

        $quote->save();

        //post a fin transaction - get open id
        $last_txn = Transaction::where('team_id','=',$team_id)
            ->orderBy('id','desc')
            ->first();          //returns last latest row

        if($last_txn) {
            $last_txn_number = $last_txn->open_id;
        }
        else {
            $last_txn_number = 0;
        }

        foreach($request->items as $item){

            $quoteItem                = new UserQuoteItem;

            $quoteItem->team_id       = $team_id;
            $quoteItem->quote_num   = $quote->id;

            $quoteItem->item_id       = empty($item['id']) ? $item['item_id'] : $item['item_id'];
            //need to be fixed
            //if $item['id'] found then we should reduce the quantity because that means product is
            //from the inventory

            $quoteItem->item_name     = empty($item['name']) ? null : $item['name'];
            $quoteItem->item_price    = empty($item['price']) ? 0 : $item['price'];
            $quoteItem->item_qty      = empty($item['qty']) ? 0 : $item['qty'];
            $quoteItem->item_total    = empty($item['total']) ? 0 : $item['total'];

            $quoteItem->credit_gl     = empty($item['credit_gl']) ? null : $item['credit_gl'];

            $quoteItem->save();

            $quoteItem->hsnsac_code = empty($item['hsnsac_code']) ? null : $item['hsnsac_code'];
            $quoteItem->save();

        }

        foreach($request->tax_items as $tax_row){

            $quoteTaxItem                = new UserQuoteTax;

            $quoteTaxItem->team_id       = $team_id;
            $quoteTaxItem->quote_num     = $quote->id;

            $quoteTaxItem->tax_open_id   = empty($tax_row['id']) ? null : $tax_row['id'];
            $quoteTaxItem->tax_id        = empty($tax_row['tax_id']) ? null : $tax_row['tax_id'];
            $quoteTaxItem->tax_name      = empty($tax_row['name']) ? null : $tax_row['name'];
            $quoteTaxItem->tax_rate      = empty($tax_row['rate']) ? 0 : $tax_row['rate'];
            $quoteTaxItem->tax_amount    = empty($tax_row['amount']) ? 0 : $tax_row['amount'];
            $quoteTaxItem->tax_display   = empty($tax_row['tax_display']) ? false : $tax_row['tax_display'];
            $quoteTaxItem->recoverable   = empty($tax_row['recoverable']) ? false : $tax_row['recoverable'];
            $quoteTaxItem->compound      = empty($tax_row['compound']) ? false : $tax_row['compound'];

            $tax_row_tax_gl              = empty($tax_row['tax_gl']) ? null : $tax_row['tax_gl'];
            $quoteTaxItem->tax_gl        = empty($tax_row['gl_account_id']) ? $tax_row_tax_gl : $tax_row['gl_account_id'];

            $quoteTaxItem->type          = 'TAX';

            $quoteTaxItem->save();

        }

        foreach($request->ded_items as $tax_row){

            $quoteTaxItem                = new UserQuoteTax;

            $quoteTaxItem->team_id       = $team_id;
            $quoteTaxItem->quote_num     = $quote->id;

            $quoteTaxItem->tax_open_id   = empty($tax_row['id']) ? null : $tax_row['id'];
            $quoteTaxItem->tax_id        = empty($tax_row['tax_id']) ? null : $tax_row['tax_id'];
            $quoteTaxItem->tax_name      = empty($tax_row['name']) ? null : $tax_row['name'];
            $quoteTaxItem->tax_rate      = empty($tax_row['rate']) ? 0 : $tax_row['rate'];
            $quoteTaxItem->tax_amount    = empty($tax_row['amount']) ? 0 : $tax_row['amount'];
            $quoteTaxItem->tax_display   = empty($tax_row['tax_display']) ? false : $tax_row['tax_display'];
            $quoteTaxItem->recoverable   = empty($tax_row['recoverable']) ? false : $tax_row['recoverable'];
            $quoteTaxItem->compound      = empty($tax_row['compound']) ? false : $tax_row['compound'];

            $tax_row_tax_gl              = empty($tax_row['tax_gl']) ? null : $tax_row['tax_gl'];
            $quoteTaxItem->tax_gl        = empty($tax_row['gl_account_id']) ? $tax_row_tax_gl : $tax_row['gl_account_id'];
            
            $quoteTaxItem->type          = 'DED';

            $quoteTaxItem->save();

        }

        $optimus                =   new Optimus(510951751, 1629280375, 743111650); //quote only
        $quote->encrypted_id    =   $optimus->encode($quote->id);
        $quote->save();

        $response = [
            'encrypted_id' => $quote->encrypted_id,
            'open_id' => $quote->open_id,
            'id' => $quote->id
        ];

        return response()->json($response);


    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $team_id    = Auth::user()->currentTeam->id;
        $user_id    = Auth::user()->id;

        $open_id    = $id; //system will pass open invoice number and not the invoice id

        $quote = UserQuote::where('team_id','=',$team_id)
            ->with('contactTab','statusTab','quoteItemsTab','quoteTaxTab')
            ->where('open_id','=',$open_id)
            ->first();          //returns single row

        if($quote){

            //$optimus        = new Optimus(522588247, 282319719, 383644817);
            //$invitation_key = $optimus->encode($invoice->id);

            //$invoice->encrypted_id = $invitation_key;
            $quote->save();

            return response()->json($quote);

        }
        else{

            return response()->json([
                'error' => 'Record not found',
            ], 404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function listQuotes(Request $request)
    {
        //need to fix this - we can't return ID to the frontend
        $q = $request->input('q');

        $team_id = Auth::user()->currentTeam->id;
        //$user_id = Auth::user()->id;

        if ($q) {
            $quotes = UserQuote::where('team_id','=',$team_id)
                ->with('statusTab','contactTab')
                ->get();

        } else {
            $quotes = UserQuote::where('team_id','=',$team_id)
                ->with('statusTab','contactTab')
                ->get();
        }

        //return Contact::find(1);
        if ($q) {

            $returnJson["total_count"] = count($quotes);
            $returnJson["incomplete_results"] = false;
            $returnJson["items"] = $quotes;

            return response()->json($returnJson);
        }
        else {

            return response()->json($quotes);

        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $team_id    = Auth::user()->currentTeam->id;
        $user_id    = Auth::user()->id;

        $open_id = $id; //system will pass open invoice number and not the invoice id

        $quote = UserQuote::where('team_id','=',$team_id)
            ->where('open_id','=',$open_id)
            ->first();          //returns single row

        if($quote) {

            $quote->modified_by_id = $user_id;   //user who deleted the contact
            $quote->save();

            UserQuote::find($quote->id)->delete();

            UserQuoteItem::where('team_id', '=', $team_id)
                ->where('quote_num', '=', $quote->id)
                ->delete();          //update multiple row

            $UserQuoteTax = UserQuoteTax::where('team_id', '=', $team_id)
                ->where('quote_num', $quote->id)
                ->delete();          //update multiple row

            return response()->json('true');
        }

        return response()->json('false');
    }

    /**
     *
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sendQuote(Request $request, $id)
    {

        $team_name  = Auth::user()->currentTeam->name;
        $team_id    = Auth::user()->currentTeam->id;
        $user_id    = Auth::user()->id;

        $optimus    = new Optimus(510951751, 1629280375, 743111650);

        $invoice    = UserQuote::where('id','=',$optimus->decode($id))
                        ->where('team_id','=',$team_id)
                        ->with('contactTab')
                        ->first();

        $invitation_key = $id;   //need to use id and not the invoice number to avoid duplication

        $share_url      = 'https://finance.pi.team/public/quotations/' . $invitation_key;
        $download_url   = $share_url . '/download';


        $to_emails      = false;
        $cc_emails      = false; //$request->cc_emails;

        $subject        = $request->subject;

        foreach($request->to_emails as $email)
        {
            if(strlen($email['id']) > 3) {
                $to_emails[] = $email['id'];
            }
        }

        foreach($request->cc_emails as $email)
        {
            if(strlen($email['id']) > 3) {
                $cc_emails[] = $email['id'];
            }
        }

        $email_message  = $request->message;

        $from_email     = Auth::user()->email;
        $from_name      = Auth::user()->name;

        $localization   = Localization::where('team_id','=',$team_id)->first();
        $inv_country    = Country::where('id','=',$localization->currency_id)->first();

        $type = 'Quote';

        /*PDF Related Code Starts*/

        $accounting     = 0;
        $gateway_id     = 0;
        $public_key     = 0;
        $team           = Team::find($invoice->team_id);
        $command        = 'Quotation';
        $team_details   = Team_Detail::where('team_id','=',$invoice->team_id)->first();

        $pdf            = \PDF::loadView('user.common.invquote.common_pdf',
                            compact('invoice','invitation_key','gateway_id',
                                'public_key','accounting','inv_country',
                                'team','command','team_details'))
                            ->setPaper('a4')->setOrientation('portrait')
                            ->setOption('margin-bottom', 0)
                            ->setOption('footer-right', '[page]');

        $pdf_name       = 'Quote_' . Carbon::parse('today')->toDateString() . '_' . Carbon::now()->timestamp . '.pdf';

        $pathToFile     = storage_path('app/private/temp/quotations/'. $pdf_name);

        $pdf->save($pathToFile);//storage_path('app/private/temp/invoices/' . $pdf_name));

        /*PDF Related Code Ends*/

        if ($cc_emails and count($cc_emails) > 0) {

            //$cc_email = $request->cc_email1;

            Mail::send('emails.invquote.send_invqt_email', compact('invoice', 'team_name', 'from_email',
                'share_url', 'download_url', 'type', 'inv_country', 'email_message'),
                function (Message $message) use ($subject, $from_email, $from_name, $to_emails, $cc_emails, $pathToFile) {
                    $message->subject($subject)
                        ->attach($pathToFile)
                        ->to($to_emails)// $contact->email $contact->name
                        ->cc($cc_emails)
                        ->from($from_email, $from_name)
                        ->replyTo($from_email, $from_name)
                        ->embedData([
                            'asm' => ['group_id' => 699],
                        ],
                            'sendgrid/x-smtpapi');

                });

        } else {

            Mail::send('emails.invquote.send_invqt_email', compact('invoice', 'team_name', 'from_email',
                'share_url', 'download_url', 'type', 'inv_country', 'email_message'),
                function (Message $message) use ($subject, $from_email, $from_name, $to_emails, $pathToFile) {

                    $message->subject($subject)
                        ->attach($pathToFile)
                        ->to($to_emails)// $contact->email $contact->name
                        ->from($from_email, $from_name)
                        ->replyTo($from_email, $from_name)
                        ->embedData([
                            'asm' => ['group_id' => 699],
                        ],
                            'sendgrid/x-smtpapi');

                });

        }

        return ('true');

        /*
        if ($cc_emails and count($cc_emails) > 0) {

            $cc_email = $request->cc_email1;

            Mail::send('emails.invquote.send_email', compact('invoice', 'team_name', 'from_email',
                                                'share_url', 'download_url', 'type', 'inv_country','email_message'),
                function (Message $message) use ($subject, $from_email, $from_name, $to_emails, $cc_emails, $pathToFile) {
                    $message->subject($subject)
                        ->attach($pathToFile)
                        ->to($to_emails)// $contact->email $contact->name
                        ->cc($cc_emails)
                        ->from($from_email, $from_name)
                        ->replyTo($from_email, $from_name)
                        ->embedData([
                            'asm' => ['group_id' => 699],
                        ],
                            'sendgrid/x-smtpapi');

                });

        }
        else {

            Mail::send('emails.invquote.send_email', compact('invoice', 'team_name', 'from_email',
                                                    'share_url', 'download_url', 'type', 'inv_country','email_message'),
                function (Message $message) use ($subject, $from_email, $from_name, $to_emails, $pathToFile) {
                    $message->subject($subject)
                        ->attach($pathToFile)
                        ->to($to_emails)// $contact->email $contact->name
                        ->from($from_email, $from_name)
                        ->replyTo($from_email, $from_name)
                        ->embedData([
                            'asm' => ['group_id' => 699],
                        ],
                            'sendgrid/x-smtpapi');

                });

        }
        */

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function previousQuoteManID()
    {
        //
        $team_id      = Auth::user()->currentTeam->id;
        $user_id      = Auth::user()->id;

        $last_quote = UserQuote::where('team_id','=',$team_id)
            ->orderBy('id','desc')
            ->first();          //returns last latest row

        if($last_quote) {

            $old_open_id  = $last_quote->open_id;

            $numbers = preg_replace('/[^0-9]/', '', $old_open_id);
            $letters = preg_replace('/[^a-zA-Z]/', '', $old_open_id);

            $new_open_id = $letters . ( $numbers + 1 );

        }
        else {
            $old_open_id = 0;
            $new_open_id = 1;
        }

        $response = [
            'old_open_id' => $old_open_id,
            'new_open_id' => $new_open_id
        ];

        return response()->json($response);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function validateQuoteID($id)
    {
        //
        $team_id      = Auth::user()->currentTeam->id;
        $user_id      = Auth::user()->id;


        $found_quote = UserQuote::where('team_id','=',$team_id)
            ->where('open_id','=',$id)
            ->first();          //returns last latest row

        if($found_quote) {

            return response()->json(false);    //means duplicate invoice exists

        }
        else {

            return response()->json(true);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function convertToInvoice(Request $request)
    {
        //
        //return response()->json($request);
        //return response()->json($request->invoice_key);

        $team_id      = Auth::user()->currentTeam->id;
        $user_id      = Auth::user()->id;

        $optimus      = new Optimus(510951751, 1629280375, 743111650);
        $quote_id     = $optimus->decode($request->invoice_key);

        $quote        = UserQuote::where('team_id','=',$team_id)
                        ->with('contactTab','statusTab','quoteItemsTab')
                        ->where('id','=',$quote_id)
                        ->first();          //returns single row


        if($quote){

            $quote_tax_items = UserQuoteTax::where('team_id','=',$team_id)
                ->where('quote_num','=',$quote->id)
                ->where('type','=','TAX')
                ->get();

            $quote_ded_items = UserQuoteTax::where('team_id','=',$team_id)
                ->where('quote_num','=',$quote->id)
                ->where('type','=','DED')
                ->get();

            $invoice_request = new \Illuminate\Http\Request();

            $invoice_request->replace([

                'invoice_title' => empty($request->invoice_title) ? $quote->title : $request->invoice_title,

                'invoice_summary'=> $quote->summary,
                'debit_gl'=> $quote->debit_gl,
                'credit_gl_cb'=> $quote->credit_gl_cb,

                'contact_id'=> $quote->contactTab->open_id,
                'customer_name' => $quote->customer_name,
                'customer_email' => $quote->customer_email,
                //'open_id' => $quote->,
                'status_id' => $quote->status_id,
                'poso_number' => $quote->poso_number,
                'invoice_date' => $request->invoice_date,
                'due_date' => $request->due_date,
                'currency_id' => $quote->currency_id,
                'company_currency_id' => $quote->currency_id_local,
                'currency_rate' => $quote->currency_rate,

                'items' => $quote->quoteItemsTab,
                'tax_items' => $quote_tax_items,
                'ded_items' => $quote_ded_items,

                'discount_rate1' => $quote->discount_rate1,
                'sub_total' => $quote->sub_total,
                'tax_amount' => $quote->tax_amount,
                'discount_amount'  => $quote->discount_amount,
                'ded_amount' => $quote->ded_amount,
                'amount_paid' => $quote->amount_paid,
                'balance' => $quote->balance,
                'total_amount' => $quote->total_amount,

                'notes' => $quote->notes,
                'footer' => $quote->footer


            ]);

            //return response()->json($invoice_request);

            $invoice_flag     = $this->createInvoice($invoice_request);
            return response()->json($invoice_flag);

        }

        return response()->json('false');

    }

}
