<?php

namespace App\Http\Controllers;

use App\Team;
use App\Team_Detail;
use App\Contact;
use App\UserInvoice;
use App\UserRecInvoice;
use App\UserInvoiceItem;
use App\UserInvoiceTax;
use App\Transaction;
use App\GL_Account;
use App\Localization;
use App\Country;

use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Mail\Message;
use Jenssegers\Optimus\Optimus;

class ApiRecInvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $team_id = Auth::user()->currentTeam->id;

        $request = request();

        if (request()->has('sort')) {
            list($sortCol, $sortDir) = explode('|', request()->sort);
            $query = UserRecInvoice::orderBy($sortCol, $sortDir)
                ->where('team_id','=',$team_id);
        } else {
            $query = UserRecInvoice::orderBy('id', 'asc')
                ->where('team_id','=',$team_id);
        }

        //temp comment - please remove this once below fields confirmed
        /*
        if ($request->exists('filter')) {
            $query->where(function($q) use($request) {
                $value = "%{$request->filter}%";
                $q->where('name', 'like', $value); //->where('text', 'like', $value)
            });
        }
        */

        $perPage = request()->has('per_page') ? (int) request()->per_page : null;

        return response()->json($query->paginate($perPage));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        /*
        $team_id      = Auth::user()->currentTeam->id;
        $user_id      = Auth::user()->id;

        $recurring  = $request->recurring;

        $recInvoice                     = new UserRecInvoice;

        $recInvoice->team_id            = $team_id;
        $recInvoice->open_id            = 1;
        $recInvoice->status_id          = 1; //active/created

        $recInvoice->invoice_id         = empty($recurring['invoice_id']) ? 1 : $recurring['invoice_id'];

        $recInvoice->freq               = empty($recurring['freq']) ? 30 : $recurring['freq'];
        $recInvoice->freq_type          = empty($recurring['freq_type']) ? 1 : $recurring['freq_type'];

        $recInvoice->reminder_to_cust   = empty($recurring['reminder_to_cust']) ? true : $recurring['reminder_to_cust'];
        $recInvoice->reminder_to_user   = empty($recurring['reminder_to_user']) ? true : $recurring['reminder_to_user'];
        $recInvoice->invoice_to_cust    = empty($recurring['invoice_to_cust']) ? true : $recurring['invoice_to_cust'];
        $recInvoice->invoice_to_user    = empty($recurring['invoice_to_user']) ? true : $recurring['invoice_to_user'];

        $recInvoice->ends               = empty($recurring['ends']) ? 1 : $recurring['ends'];//1:never
        $recInvoice->start_date         = empty($recurring['start_date']) ? null : $recurring['start_date'];
        $recInvoice->end_date           = empty($recurring['end_date']) ? null : $recurring['end_date'];
        $recInvoice->end_after_no_inv   = empty($recurring['end_after_no_inv']) ? 1 : $recurring['end_after_no_inv'];
        */
        /*
        $recInvoice->freq               = empty($request->freq) ? 30 : $request->freq;
        $recInvoice->freq_type          = empty($request->freq_type) ? 1 : $request->freq_type;

        $recInvoice->reminder_to_cust   = empty($request->reminder_to_cust) ? true : $request->reminder_to_cust;
        $recInvoice->reminder_to_user   = empty($request->reminder_to_user) ? true : $request->reminder_to_user;
        $recInvoice->invoice_to_cust    = empty($request->invoice_to_cust) ? true : $request->invoice_to_cust;
        $recInvoice->invoice_to_user    = empty($request->invoice_to_user) ? true : $request->invoice_to_user;

        $recInvoice->ends               = empty($request->ends) ? 1 : $request->ends;//1:never
        $recInvoice->start_date         = $request->start_date;//) ? null : $request->start_date;
        $recInvoice->end_date           = $request->end_date;//) ? null : $request->end_date;
        $recInvoice->end_after_no_inv   = empty($request->end_after_no_inv) ? 1 : $request->end_after_no_inv;
        */

        /*
        $recInvoice->invoices_created   = 0;

        $recInvoice->created_by_id      =   $user_id;
        $recInvoice->modified_by_id     =   $user_id;

        $recInvoice->save();

        return response()->json('true');

        */
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user_id                    =   Auth::user()->id;
        $team_id                    =   Auth::user()->currentTeam->id;

        $recInvoice                 =   UserRecInvoice::where('team_id','=',$team_id)
                                        ->where('open_id','=',$id)
                                        ->first();

        $id                         =   $recInvoice->id;
        $recInvoice->modified_by_id =   $user_id;   //user who deleted the contact
        $recInvoice->save();

        UserRecInvoice::find($id)->delete();

        return response()->json('true');
    }
}
