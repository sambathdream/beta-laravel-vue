<?php

namespace App\Http\Controllers;

use App\UserQuote;
//use App\UserQuoteItem;
use App\Contact;
use App\Transaction;
use App\GL_Account;
use App\Team;
use App\Country;
use App\Localization;
use App\UserPayment;
use App\Tax_Rate;
use App\BootTour;
use App\Team_Detail;
use App\Accounting;

use Crypt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Optimus\Optimus;

class QuotationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('teamSubscribed');

        //new Optimus(522588247, 282319719, 383644817);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $team_id      = Auth::user()->currentTeam->id;
        $user_id      = Auth::user()->id;

        $total_revenue = UserQuote::where('team_id','=',$team_id)
            ->sum('total_amount');

        $now = date('Y-m-d');

        $total_sales = UserQuote::where('team_id','=',$team_id)
            ->where('created_at','=',$now)
            ->count('id');

        $total_payments = UserPayment::where('team_id','=',$team_id)
            ->where('payment_date','>=',$now)
            ->count('id');

        $localization = Localization::where('team_id','=',$team_id)->first();

        $inv_country  = Country::where('id','=',$localization->currency_id)->first();

        return view('user.quotations.quotations',compact('total_revenue','total_sales','total_payments','inv_country'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $team_id      = Auth::user()->currentTeam->id;
        $user_id      = Auth::user()->id;

        $gl_accounts  = GL_Account::where('team_id','=',$team_id)
                        ->orderBy('type','name')
                        ->get();

        $tax_rates = Tax_Rate::where('team_id', '=', $team_id)
            ->get();

        $boot_tour = BootTour::where('user_id', $user_id)
            ->first();

        return view('user.quotations.create_quote',
            compact('gl_accounts','boot_tour','tax_rates'))
            ->withEncryptedCsrfToken(Crypt::encrypt(csrf_token()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $team_id    = Auth::user()->currentTeam->id;
        $user_id    = Auth::user()->id;

        $open_id = $id; //system will pass open invoice number and not the invoice id

        $invoice = UserQuote::where('team_id','=',$team_id)
            ->with('contactTab','statusTab','quoteItemsTab','quoteTaxTab')
            ->where('open_id','=',$open_id)
            ->first();          //returns single row

        $optimus        = new Optimus(510951751, 1629280375, 743111650);
        $invitation_key = $optimus->encode($invoice->id);


        if($invoice){

            $accounting     = 0;
            $gateway_id     = 0;
            $public_key     = 0;

            $inv_country    = Country::where('id','=',$invoice->currency_id)->first();

            $team           = Team::find($team_id);
            $command        = 'Quotation';
            $team_details   = Team_Detail::where('team_id','=',$team_id)->first();

            return view('user.quotations.view_quotation',
                compact('invoice','invitation_key','gateway_id',
                    'public_key','accounting','inv_country',
                    'team','command','team_details'));


        }
        else{
            return redirect()->action('QuotationController@index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
