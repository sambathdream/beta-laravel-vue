<?php

namespace App\Http\Controllers;

use App\UserGateway;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiGatewayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $team_id = Auth::user()->currentTeam->id;

        $user_gateways = UserGateway::where('team_id','=',$team_id)
            ->select(['id', 'team_id', 'gateway_id', 'gate_data'])
            ->get();

        //return response()->json($user_gateways);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $team_id = Auth::user()->currentTeam->id;

        $user_gateway = UserGateway::where('team_id','=',$team_id)
            ->where('gateway_id','=',$id)
            //->select(['id', 'team_id', 'gateway_id', 'gate_data'])
            ->first();

        $user_gateway->gate_data = decrypt($user_gateway->gate_data);

        return response()->json($user_gateway);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        ////
        //
        $team_id                = Auth::user()->currentTeam->id;
        $user_id                = Auth::user()->id;


        $user_gateway = UserGateway::where('team_id','=',$team_id)
                    ->where('gateway_id','=',$id)
                    ->first();

        //return response()->json($id);

        if(!$user_gateway)
        {

            $user_gateway           =   new UserGateway;
            $user_gateway->team_id      =   $team_id;
            $user_gateway->gateway_id   =   $id;

        }

        $user_gateway->gate_data    =   encrypt($request->gate_data);
        $user_gateway->status       =   true;

        $user_gateway->created_by_id   =   $user_id;
        $user_gateway->modified_by_id  =   $user_id;
        $user_gateway->save();

        $falseRows = UserGateway::where('gateway_id','!=',$id)->update(['status' => false]);

        //$falseRows->save();

        return response()->json('true');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $team_id                = Auth::user()->currentTeam->id;
        $user_id                = Auth::user()->id;


        $user_gateway = UserGateway::where('team_id','=',$team_id)
            ->where('gateway_id','=',$id)
            ->first();

        $user_gateway->modified_by_id  =   $user_id;
        $user_gateway->save();

        UserGateway::find($user_gateway->id)->delete();

        return response()->json('true');

    }
}
