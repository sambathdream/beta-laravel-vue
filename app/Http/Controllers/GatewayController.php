<?php

namespace App\Http\Controllers;

use App\UserGateway;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Optimus\Optimus;

class GatewayController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('teamSubscribed');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $optimus = new Optimus(522588247, 282319719, 383644817);

        $team_id = Auth::user()->currentTeam->id;

        $enc_team_id = "https://finance.pi.team/public/payments/" . $optimus->encode($team_id) . "/webhook";
        /*
        $user_gateways = UserGateway::where('team_id','=',$team_id)
            ->select(['id', 'team_id', 'gateway_id', 'gate_data'])
            ->get();

        foreach ($user_gateways as $user_gateway){
            $user_gateway->gate_data = decrypt($user_gateway->gate_data);
            $user_gateway->save();
        }
        */

        return view('user.gateways.pay_gateways', compact('enc_team_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
