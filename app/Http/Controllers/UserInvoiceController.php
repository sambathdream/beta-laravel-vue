<?php

namespace App\Http\Controllers;

use App\UserInvoice;
use App\UserInvoiceItem;
use App\Contact;
use App\Transaction;
use App\GL_Account;
use App\Team;
use App\UserGateway;
use App\Country;
use App\UserPayment;
use App\Localization;
use App\Tax_Rate;
use App\BootTour;
use App\Team_Detail;

use Crypt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Optimus\Optimus;

class UserInvoiceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('teamSubscribed');

        //new Optimus(522588247, 282319719, 383644817);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $team_id      = Auth::user()->currentTeam->id;
        $user_id      = Auth::user()->id;

        $total_revenue = UserInvoice::where('team_id','=',$team_id)
            ->sum('total_amount_local');

        $now = \Carbon\Carbon::now()->format('Y-m-d');

//        return $now;
        $total_sales = UserInvoice::where('team_id','=',$team_id)
            ->where('created_at','>=',$now)
            ->count('id');

        $total_payments = UserPayment::where('team_id','=',$team_id)
            ->where('payment_date','>=',$now)
            ->count('id');

        $localization = Localization::where('team_id','=',$team_id)->first();

        $inv_country  = Country::where('id','=',$localization->currency_id)->first();

        $boot_tour = BootTour::where('user_id', $user_id)
            ->first();

        //gl account balance for total receivable
        $total_receivable   = GL_Account::where('team_id','=',$team_id)
                                ->where('open_id','=','1')
                                ->first();

        return view('user.invoices.invoices',compact('total_revenue','total_sales',
            'total_payments','inv_country','boot_tour','total_receivable'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $team_id      = Auth::user()->currentTeam->id;
        $user_id      = Auth::user()->id;

        $gl_accounts  = GL_Account::where('team_id','=',$team_id)
                        ->orderBy('type','name')
                        ->get();

        $tax_rates = Tax_Rate::where('team_id', '=', $team_id)
            ->get();

        $boot_tour = BootTour::where('user_id', $user_id)
            ->first();

        if($team_id == 1 or $team_id == 268 or $team_id == 20){

            return view('user.common.invquote.gst_create',
                compact('gl_accounts','boot_tour','tax_rates'))
                ->withEncryptedCsrfToken(Crypt::encrypt(csrf_token()));

        }

        return view('user.invoices.create_invoice',
            compact('gl_accounts','boot_tour','tax_rates'))
            ->withEncryptedCsrfToken(Crypt::encrypt(csrf_token()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $team_id    = Auth::user()->currentTeam->id;
        $user_id    = Auth::user()->id;

        $open_id = $id; //system will pass open invoice number and not the invoice id

        $invoice = UserInvoice::where('team_id','=',$team_id)
            ->with('contactTab','statusTab','invItemsTab','invTaxTab')
            ->where('open_id','=',$open_id)
            ->first();          //returns single row

        //return response()->json($invoice);

        $optimus = new Optimus(522588247, 282319719, 383644817);
        $invitation_key = $optimus->encode($invoice->id);
        

        if($invoice){

            if($invoice->contactTab->emails)
            {
                //$invoice->contactTab->emails =  json_decode($invoice->contactTab->emails);
            }

            if($team_id == 1 or $team_id == 268 or $team_id == 20) {

                $invoiceItems = UserInvoiceItem::where('team_id', '=', $team_id)
                    ->where('invoice_num', '=', $invoice->id)
                    ->with('itemTaxTab')
                    //->orderBy('itemTaxTab.type')
                    ->get();
                //

                $invoiceItems = $invoiceItems->sortBy('itemTaxTab.type');

                //return response()->json($invoiceItems);

            }
            else{

                $invoiceItems = 0;

            }

            $user_gateway = UserGateway::where('team_id','=',$invoice->team_id)
                ->where('status','=',true)
                ->first();

            if($user_gateway) {
                $user_gateway->gate_data = decrypt($user_gateway->gate_data);
                $gate_data = explode("#", $user_gateway->gate_data);

            }

            $gateway_id = empty($user_gateway->gateway_id) ? 0 : $user_gateway->gateway_id;
            $public_key = empty($gate_data[1]) ? null : $gate_data[1];

            $inv_country    = Country::where('id','=',$invoice->currency_id)->first();

            $team               = Team::find($team_id);
            $command            = 'Invoice';
            $team_details       = Team_Detail::where('team_id','=',$team_id)->first();

            if($team_id == 1 or $team_id == 268 or $team_id == 20){

                return view('user.common.invquote.gst_common_view',
                    compact('invoice','invitation_key','gateway_id',
                        'public_key','inv_country', 'team','command',
                        'team_details','invoiceItems'));

            }

            return view('user.invoices.view_invoice',
                compact('invoice','invitation_key','gateway_id',
                    'public_key','inv_country', 'team','command',
                    'team_details'));



        }
        else{
            return redirect()->action('UserInvoiceController@index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //$invoice = Invoice::find($id);

        //$invoiceitems = InvoiceItem::find($invoice->invoice_id);

        //$customer   = Contact::find($invoice->customer_id);

        //return view('user.invoices.view_invoice',compact('invoice','invoiceitems','customer'));
        $open_id = $id;

        $team_id    = Auth::user()->currentTeam->id;
        $user_id    = Auth::user()->id;

        $invoice = UserInvoice::where('team_id','=',$team_id)
                        ->where('open_id','=',$open_id)
                        ->first();

        if($invoice->status_id == 4){
            return response()->json('Invoice already paid, cant be edited');
        }

        if($invoice) {

            $boot_tour = BootTour::where('user_id', $user_id)
                ->first();

            $gl_accounts  = GL_Account::where('team_id','=',$team_id)
                ->orderBy('type','name')
                ->get();

            $tax_rates = Tax_Rate::where('team_id', '=', $team_id)
                ->get();

            $open_id = $invoice->open_id;

            $command = 'Invoice';

            if($team_id == 1 or $team_id == 268 or $team_id == 20){
                return view('user.invoices.gst_edit_invoice',
                    compact('open_id','command','gl_accounts','tax_rates','boot_tour'))
                    ->withEncryptedCsrfToken(Crypt::encrypt(csrf_token()));
            }

            return view('user.invoices.edit_invoice',
                compact('open_id','command','gl_accounts','tax_rates','boot_tour'))
                    ->withEncryptedCsrfToken(Crypt::encrypt(csrf_token()));

        }
        else{
            abort(404);
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function designInvoice()
    {
        //
        \PDF::loadFile('http://www.github.com')->snappy()->setTemporaryFolder(storage_path('/app/public/temp'))->save('github.pdf');
        //return view('user.invoices.invoice_design');

    }


}
