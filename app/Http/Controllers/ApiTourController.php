<?php

namespace App\Http\Controllers;

use App\BootTour;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiTourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user_id = Auth::user()->id;

        $boot_tour = BootTour::firstOrNew(['user_id' => $user_id]);

        switch ($id)
        {
            case 1:
                $boot_tour->dashboard = $request->tour_status;
                break;

            case 2:
                $boot_tour->invoices = $request->tour_status;
                break;

            case 3:
                $boot_tour->create_invoice = $request->tour_status;
                break;

            case 4:
                $boot_tour->quote = $request->tour_status;
                break;

            case 5:
                $boot_tour->create_quote = $request->tour_status;
                break;

            case 6:
                $boot_tour->bills = $request->tour_status;
                break;

            case 7:
                $boot_tour->create_bill = $request->tour_status;
                break;

            case 8:
                $boot_tour->inventory = $request->tour_status;
                break;

            case 9:
                $boot_tour->transactions = $request->tour_status;
                break;

            case 10:
                $boot_tour->glaccounts = $request->tour_status;
                break;

            case 11:
                $boot_tour->reports = $request->tour_status;
                break;

            case 12:
                $boot_tour->time = $request->tour_status;
                break;

            case 13:
                $boot_tour->expenses = $request->tour_status;
                break;

            case 14:
                $boot_tour->gateways = $request->tour_status;
                break;

            case 15:
                $boot_tour->spark_settings = $request->tour_status;
                break;

            default:

        }

        $boot_tour->user_id = $user_id;
        $boot_tour->save();
        /**/

        return response()->json($request->tour_status);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
