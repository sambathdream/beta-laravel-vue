<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Omnipay\Omnipay;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('teamSubscribed');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('user.payments.payments');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        /*
        $gateway = Omnipay::create('Stripe');
        $gateway->setApiKey('sk_test_uvUwubu1Dd8qeF3Yvxr8b8ro');

        $formData = array('number' => '4242424242424242', 'expiryMonth' => '6', 'expiryYear' => '2019', 'cvv' => '123');
        $response = $gateway->purchase(array('amount' => '12.00', 'currency' => 'USD', 'card' => $formData))->send();

        if ($response->isSuccessful()) {
            // payment was successful: update database
            return response()->json($response);

        } elseif ($response->isRedirect()) {
            // redirect to offsite payment gateway
            return $response->redirect();
        } else {
            // payment failed: display message to customer
            return response()->json($response->getMessage());
        }
        */


        \Stripe\Stripe::setApiKey('sk_test_uvUwubu1Dd8qeF3Yvxr8b8ro');
        $myCard = array('number' => '4242424242424242', 'exp_month' => 8, 'exp_year' => 2018);
        $charge = \Stripe\Charge::create(array('card' => $myCard, 'amount' => 2000, 'currency' => 'usd'));
        echo $charge;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
