<?php

namespace App\Http\Controllers;

use App\Country;
use App\UserInvoice;
use App\UserGateway;
use App\UserPayment;
use App\Transaction;
use App\Accounting;
use App\GL_Account;

use Carbon\Carbon;

use Swap;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Optimus\Optimus;


class ApiPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $team_id = Auth::user()->currentTeam->id;

        $request = request();

        if (request()->has('sort')) {
            list($sortCol, $sortDir) = explode('|', request()->sort);
            $query = UserPayment::orderBy($sortCol, $sortDir)
                ->with('invoiceTab','contactTab')
                ->where('team_id', '=', $team_id);
        } else {
            $query = UserPayment::orderBy('id', 'asc')
                ->with('invoiceTab','contactTab')
                ->where('team_id', '=', $team_id);
        }
        if ($request->exists('filter')) {
            $query->where(function ($q) use ($request) {
                $value = "%{$request->filter}%";
                $q->where('amount', 'like', $value);
            });
        }

        $perPage = request()->has('per_page') ? (int) request()->per_page : null;
// The headers 'Access-Control-Allow-Origin' and 'Access-Control-Allow-Methods'
// are to allow you to call this from any domain (see CORS for more info).
// This is for local testing only. You should not do this in production server,
// unless you know what it means.
        return response()->json($query->paginate($perPage));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $team_id    = Auth::user()->currentTeam->id;
        $user_id    = Auth::user()->id;

        $optimus    = new Optimus(522588247, 282319719, 383644817);

        $invoice    = UserInvoice::where('id','=',$optimus->decode($request->invoice_key))
                    ->where('team_id','=',$team_id)
                    ->first();          //returns single row

        if($invoice) {

            $last_payment = UserPayment::where('team_id', '=', $team_id)
                            ->orderBy('id', 'desc')
                            ->first();          //returns last latest row

            if ($last_payment) {
                $new_pay_number = $last_payment->open_id + 1;
            } else {
                $new_pay_number = 1;
            }

            $payment = new UserPayment;

            $payment->team_id    = $team_id;
            $payment->open_id    = $new_pay_number;
            $payment->contact_id = empty($invoice->contact_id) ? null : $invoice->contact_id;
            $payment->invoice_id = empty($invoice->id) ? null : $invoice->id;

            $payment->payment_type_id       = empty($request->payment_type_id) ? 3 : $request->payment_type_id;

            $payment->amount                = empty($request->amount) ? 0 : $request->amount;
            $payment->amount_local          = empty($request->amount_local) ? 0 : $request->amount_local;

            $payment->transaction_reference = empty($request->transaction_reference) ? 'Manual Pay' : $request->transaction_reference;
            $payment->payment_date          = empty($request->payment_date)
                ? Carbon::parse('today')->toDateString() : Carbon::parse($request->payment_date)->toDateString();

            $payment->created_by_id     = $user_id;
            $payment->modified_by_id    = $user_id;

            $payment->save();

            //return response()->json('true');
            $pay_reference_txn = $payment->id;

            /*update invoice balance start*/

            $amount_paid                = $payment->amount;
            $amount_paid_local          = $payment->amount_local;

            $invoice->amount_paid       = $invoice->amount_paid + $amount_paid;
            $invoice->amount_paid_local = $invoice->amount_paid_local + $amount_paid_local;

            $invoice->balance           = $invoice->balance - $amount_paid;
            $invoice->balance_local     = $invoice->balance_local - $amount_paid_local;

            if($invoice->balance <= 0){

                $invoice->status_id = 4;

            }else{

                $invoice->status_id = 7;

            }

            $invoice_credit_gl          = $invoice->debit_gl;

            $invoice->save();

            /*update invoice balance end*/

            /*post transaction*/

            $last_txn = Transaction::where('team_id', '=', $team_id)
                        ->orderBy('created_at', 'desc')
                        ->first();          //returns last latest row

            if ($last_txn) {
                $last_txn_number = $last_txn->open_id + 1;
            } else {
                $last_txn_number = 1;
            }

            $accounting = Accounting::where('team_id', '=', $invoice->team_id)
                ->first();
            $payment_debit_gl = $accounting->paymentdb;

            //debit gl
            if ($payment_debit_gl) {

                $transaction                    = new Transaction;
                $transaction->team_id           = $team_id;

                $transaction->open_id           = $last_txn_number;

                $transaction->category          = 47;
                $transaction->txn_type          = "D";
                $transaction->txn_date          = Carbon::parse('today')->toDateString();

                $transaction->gl_account_id     = $payment_debit_gl;
                $transaction->txn_amount        = $amount_paid_local;
                $transaction->description       = 'Invoice Payment';

                $transaction->created_by_id     = $user_id;
                $transaction->modified_by_id    = $user_id;

                $transaction->save();

                $gl_account = GL_Account::where('team_id', '=', $team_id)
                    ->where('open_id', '=', $payment_debit_gl)
                    ->first();

                if ($gl_account) {
                    $gl_account->int_balance = $gl_account->int_balance + $transaction->txn_amount;
                }

                $gl_account->save();

                $transaction->balance = $gl_account->int_balance;
                $transaction->pay_ref = $pay_reference_txn;
                $transaction->save();

            }

            //credit gl
            if ($invoice_credit_gl) {
                $transaction                    = new Transaction;
                $transaction->team_id           = $team_id;

                $transaction->open_id           = $last_txn_number + 1;

                $transaction->category          = 47;
                $transaction->txn_type          = "C";//D or C ??
                $transaction->txn_date          = $payment->payment_date;

                $transaction->gl_account_id     = $invoice_credit_gl;
                $transaction->txn_amount        = $amount_paid_local;

                $transaction->description       = 'Invoice Payment';
                $transaction->created_by_id     = $user_id;
                $transaction->modified_by_id    = $user_id;

                $transaction->save();

                $gl_account = GL_Account::where('team_id', '=', $team_id)
                    ->where('open_id', '=', $invoice_credit_gl)
                    ->first();

                if ($gl_account) {
                    $gl_account->int_balance = $gl_account->int_balance - $transaction->txn_amount;
                }

                $gl_account->save();

                $transaction->balance = $gl_account->int_balance;
                $transaction->pay_ref = $pay_reference_txn;
                $transaction->save();

            }

            /*end post transaction*/

            $response = [
                'balance_local' => $amount_paid_local,
                'balance' => $amount_paid
            ];

            return response()->json($response);

        }

        return response()->json([
            'error' => 'Failed!',
        ], 404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //


    }

    public function convertCurrency(Request $request)
    {
        $country = Country::where('id','=',$request->currency_id)->first();

        $company_country = Country::where('id','=',$request->company_currency_id)->first();

        $currency_conv = $country->currency_code . '/' . $company_country->currency_code;

        $rate = Swap::latest($currency_conv, ['cache' => false]);

        $response = [
            'contact_country' => $country->currency_code,
            'company_country' => $company_country->currency_code,
            'rate' => $rate->getValue()
        ];

        return response()->json($response);
    }
}
