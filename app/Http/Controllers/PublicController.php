<?php

namespace App\Http\Controllers;

use Crypt;

use App\Team;
use App\Contact;
use App\UserInvoice;
use App\UserCredit;
use App\Accounting;
//use App\UserInvoiceItem;
use App\UserQuote;
use App\UserInvInvitation;
use App\UserGateway;
use App\Transaction;
use App\GL_Account;
use App\Country;
use App\Localization;
use App\UserPayment;
use App\Team_Detail;

use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; //auth added for force logout otherwise public controller

use Razorpay\Api\Api;
use Jenssegers\Optimus\Optimus;

class PublicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //showInvoice
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showInvoice($id)
    {

        Auth::logout(); //if logged in then force logout else ignore

        $optimus = new Optimus(522588247, 282319719, 383644817);

        $invoice = UserInvoice::with('contactTab','statusTab','invItemsTab','invTaxTab')
            ->where('id','=',$optimus->decode($id))
            ->first();          //returns single row
        //

        if($invoice){

            $user_gateway   = UserGateway::where('team_id','=',$invoice->team_id)
                ->where('status','=',true)
                ->first();

            if($user_gateway) {
                $user_gateway->gate_data = decrypt($user_gateway->gate_data);
                $gate_data = explode("#", $user_gateway->gate_data);
            }

            $gateway_id = empty($user_gateway->gateway_id) ? 0 : $user_gateway->gateway_id;
            $public_key = empty($gate_data[1]) ? null : $gate_data[1];

            $invitation_key =   $id;

            $inv_country    = Country::where('id','=',$invoice->currency_id)->first();

            $team               = Team::find($invoice->team_id);
            $command            = 'Invoice';
            $team_details       = Team_Detail::where('team_id','=',$invoice->team_id)->first();

            return view('user.invoices.view_invoice',
                compact('invoice','invitation_key', 'gateway_id',
                    'public_key','inv_country', 'team','command','team_details'));

        }
        else{

            return ('error'); //redirect()->action('UserInvoiceController@index');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showQuote($id)
    {

        Auth::logout(); //if logged in then force logout else ignore

        $optimus = new Optimus(510951751, 1629280375, 743111650);

        $invoice = UserQuote::with('contactTab','statusTab','quoteItemsTab','quoteTaxTab')
            ->where('id','=',$optimus->decode($id))
            ->first();          //returns single row
        //

        if($invoice){

            $team_id        = $invoice->team_id;
            $invitation_key = $id;

            $accounting     = 0;
            $gateway_id     = 0;
            $public_key     = 0;

            $inv_country    = Country::where('id','=',$invoice->currency_id)->first();

            $team           = Team::find($team_id);
            $command        = 'Quotation';
            $team_details   = Team_Detail::where('team_id','=',$team_id)->first();

            return view('user.quotations.view_quotation',
                compact('invoice','invitation_key','gateway_id',
                    'public_key','accounting','inv_country',
                    'team','command','team_details'));

        }
        else{

            return ('error'); //redirect()->action('UserInvoiceController@index');
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showCreditNote($id)
    {

        Auth::logout(); //if logged in then force logout else ignore

        $optimus = new Optimus(1568794001, 671972209, 2122508544);

        $invoice = UserCredit::with('contactTab','statusTab','creditItemsTab','creditTaxTab')
            ->where('id','=',$optimus->decode($id))
            ->first();          //returns single row
        //

        if($invoice){

            $invitation_key = $invoice->encrypted_id;
            $team_id        = $invoice->team_id;

            $gateway_id = 0;
            $public_key = null;

            $inv_country    = Country::where('id','=',$invoice->currency_id)->first();

            $team               = Team::find($team_id);
            $command            = 'CreditNote';
            $team_details       = Team_Detail::where('team_id','=',$team_id)->first();

            return view('user.credit-notes.view-creditnote',
                compact('invoice','invitation_key', 'gateway_id',
                    'public_key','inv_country', 'team','command','team_details'));

        }
        else{

            return ('error'); //redirect()->action('UserInvoiceController@index');
        }

    }

    /**
     *
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function downloadInvoice($id)
    {
        $optimus = new Optimus(522588247, 282319719, 383644817);

        $invoice = UserInvoice::where('id','=',$optimus->decode($id))
            ->with('contactTab','statusTab','invItemsTab','invTaxTab')
            ->first();          //returns single row
        //

        if($invoice){

            $invitation_key =   $id;

            $accounting     = 0;
            $gateway_id     = 0;
            $public_key     = 0;

            $inv_country        = Country::where('id','=',$invoice->currency_id)->first();

            $team               = Team::find($invoice->team_id);
            $command            = 'Invoice';
            $team_details       = Team_Detail::where('team_id','=',$invoice->team_id)->first();

            $pdf = \PDF::loadView('user.common.invquote.common_pdf',
                compact('invoice','invitation_key','gateway_id',
                    'public_key','accounting','inv_country',
                    'team','command','team_details'))
                ->setPaper('a4')->setOrientation('portrait')
                ->setOption('margin-bottom', 0)
                ->setOption('footer-right', '[page]');

            $pdf_name = 'Invoice_' . Carbon::parse('today')->toDateString() . '.pdf';

            return $pdf->download($pdf_name);
            //$pdf->save(storage_path('app/private/temp/invoices/' . $pdf_name));

            //storage_path(). 'app/private/temp/invoices'


        }
        else{

            return ('Error'); //redirect()->action('UserInvoiceController@index');
        }


    }

    /**
     *
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function downloadQuote($id)
    {

        $optimus = new Optimus(510951751, 1629280375, 743111650);

        $invoice = UserQuote::where('id','=',$optimus->decode($id))
            ->with('contactTab','statusTab','quoteItemsTab','quoteTaxTab')
            ->first();          //returns single row

        if($invoice){

            $invitation_key =   $id;

            $accounting     = 0;
            $gateway_id     = 0;
            $public_key     = 0;

            $inv_country        = Country::where('id','=',$invoice->currency_id)->first();

            $team               = Team::find($invoice->team_id);
            $command            = 'Quotation';
            $team_details       = Team_Detail::where('team_id','=',$invoice->team_id)->first();

            $pdf = \PDF::loadView('user.common.invquote.common_pdf',
                compact('invoice','invitation_key','gateway_id',
                    'public_key','accounting','inv_country',
                    'team','command','team_details'))
                ->setPaper('a4')->setOrientation('portrait')
                ->setOption('margin-bottom', 0)
                ->setOption('footer-right', '[page]');

            $pdf_name = 'Quotation_' . Carbon::parse('today')->toDateString() . '.pdf';

            return $pdf->download($pdf_name);

        }
        else{

            return ('Error'); //redirect()->action('UserInvoiceController@index');
        }
    }

    /**
     *
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function downloadCreditNote($id)
    {

        $optimus = new Optimus(1568794001, 671972209, 2122508544);

        $invoice = UserCredit::where('id','=',$optimus->decode($id))
            ->with('contactTab','statusTab','creditItemsTab','creditTaxTab')
            ->first();          //returns single row
        
        if($invoice){

            $invitation_key = $invoice->encrypted_id;
            $team_id        = $invoice->team_id;

            $accounting     = 0;
            $gateway_id     = 0;
            $public_key = null;

            $inv_country    = Country::where('id','=',$invoice->currency_id)->first();

            $team               = Team::find($team_id);
            $command            = 'CreditNote';
            $team_details       = Team_Detail::where('team_id','=',$team_id)->first();

            $pdf = \PDF::loadView('user.credit-notes.view-pdf',
                compact('invoice','invitation_key','gateway_id',
                    'public_key','accounting','inv_country',
                    'team','command','team_details'))
                ->setPaper('a4')->setOrientation('portrait')
                ->setOption('margin-bottom', 0)
                ->setOption('footer-right', '[page]');

            $pdf_name = 'CreditNote_' . Carbon::parse('today')->toDateString() . '.pdf';

            return $pdf->download($pdf_name);

        }
        else{

            return ('Error'); //redirect()->action('UserInvoiceController@index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function createPayment($id)
    {


    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePayment($id, Request $request)
    {
        //
        $optimus        = new Optimus(522588247, 282319719, 383644817);

        $invoice        = UserInvoice::where('id','=',$optimus->decode($id))
            ->with('contactTab')
            ->first();  //returns single row

        if($invoice) {

            $team_id    = $invoice->team_id;
            $user_id    = 0;
            $amount     = $invoice->balance * 100;

            $inv_country = Country::where('id', '=', $invoice->currency_id)->first();

            $user_gateway = UserGateway::where('team_id', '=', $team_id)
                ->where('gateway_id', '=', $request->gateway_id)
                ->first();

            $gate_data = decrypt($user_gateway->gate_data);
            $gate_data = explode("#", $gate_data);

            //stripe
            if ($request->gateway_id == 1) {

                \Stripe\Stripe::setApiKey($gate_data[0]); //sk_test_Ohxdc5JY7QcRUU1NpA3F60C7

                $response = \Stripe\Charge::create(array(
                    "amount" => $amount,
                    "currency" => $inv_country->currency_code,
                    "source" => $request->stripe_token, // obtained with Stripe.js
                    "description" => "Invoice Charge",
                ));

                //return response()->json('true');

            } //Razor Pay
            elseif ($request->gateway_id == 3) {

                //$razor_secret = $gate_data[0];

                $api = new Api($gate_data[1], $gate_data[0]);
                //api key rzp_test_qqId1PvSrrDoKq api secret eKlTHeb7Dq2QJ1oUG8zGMjb4

                $payment = $api->payment->fetch($request->razorpay_payment_id)->capture(array('amount' => $amount));
                // Captures a payment

            }


            $last_payment = UserPayment::where('team_id', '=', $team_id)
                ->orderBy('id', 'desc')
                ->first();          //returns last latest row

            if ($last_payment) {
                $new_pay_number = $last_payment->open_id + 1;
            } else {
                $new_pay_number = 1;
            }

            $payment = new UserPayment;
            $payment->team_id = $team_id;
            $payment->open_id = $new_pay_number;
            $payment->contact_id = empty($invoice->contact_id) ? null : $invoice->contact_id;
            $payment->invoice_id = empty($invoice->id) ? null : $invoice->id;

            $payment->payment_type_id = 6;
            //empty($request->payment_type_id) ? 6 : $request->payment_type_id

            $payment->amount_local = empty($invoice->balance_local) ? 0 : $invoice->balance_local;
            $payment->amount = empty($invoice->balance) ? 0 : $invoice->balance;
            $payment->transaction_reference = 'Auto Pay Gateway';
            $payment->payment_date = Carbon::parse('today')->toDateString();

            $payment->created_by_id = 0;
            $payment->modified_by_id = 0;

            $payment->save();

            $pay_reference_txn = $payment->id;

            /*update invoice balance start*/

            $amount_paid                = $invoice->balance;
            $amount_paid_local          = $invoice->balance_local;

            $invoice->amount_paid       = $invoice->amount_paid + $amount_paid;
            $invoice->amount_paid_local = $invoice->amount_paid_local + $amount_paid_local;

            $invoice->status_id         = 4;
            $invoice->balance           = 0;
            $invoice->balance_local     = 0;

            $invoice_credit_gl          = $invoice->debit_gl;

            $invoice->save();

            /*update invoice balance end*/

            /*post transaction*/

            $last_txn = Transaction::where('team_id', '=', $team_id)
                ->orderBy('created_at', 'desc')
                ->first();          //returns last latest row

            if ($last_txn) {
                $last_txn_number = $last_txn->open_id + 1;
            } else {
                $last_txn_number = 1;
            }

            $accounting = Accounting::where('team_id', '=', $invoice->team_id)
                ->first();
            $payment_debit_gl = $accounting->paymentdb;

            //debit gl
            if ($payment_debit_gl) {

                $transaction                    = new Transaction;
                $transaction->team_id           = $team_id;

                $transaction->open_id           = $last_txn_number;

                $transaction->category          = 47;
                $transaction->txn_type          = "D";
                $transaction->txn_date          = Carbon::parse('today')->toDateString();

                $transaction->gl_account_id     = $payment_debit_gl;
                $transaction->txn_amount        = $amount_paid_local;
                $transaction->description       = 'Invoice Payment';

                $transaction->created_by_id     = $user_id;
                $transaction->modified_by_id    = $user_id;

                $transaction->save();

                $gl_account = GL_Account::where('team_id', '=', $team_id)
                    ->where('open_id', '=', $payment_debit_gl)
                    ->first();

                if ($gl_account) {
                    $gl_account->int_balance = $gl_account->int_balance + $transaction->txn_amount;
                }

                $gl_account->save();

                $transaction->balance = $gl_account->int_balance;
                $transaction->pay_ref = $pay_reference_txn;
                $transaction->save();

            }

            //credit gl
            if ($invoice_credit_gl) {
                $transaction                    = new Transaction;
                $transaction->team_id           = $team_id;

                $transaction->open_id           = $last_txn_number + 1;

                $transaction->category          = 47;
                $transaction->txn_type          = "C";//D or C ??
                $transaction->txn_date          = Carbon::parse('today')->toDateString();

                $transaction->gl_account_id     = $invoice_credit_gl;
                $transaction->txn_amount        = $amount_paid_local;

                $transaction->description       = 'Invoice Payment';
                $transaction->created_by_id     = $user_id;
                $transaction->modified_by_id    = $user_id;

                $transaction->save();

                $gl_account = GL_Account::where('team_id', '=', $team_id)
                    ->where('open_id', '=', $invoice_credit_gl)
                    ->first();

                if ($gl_account) {
                    $gl_account->int_balance = $gl_account->int_balance - $transaction->txn_amount;
                }

                $gl_account->save();

                $transaction->balance = $gl_account->int_balance;
                $transaction->pay_ref = $pay_reference_txn;
                $transaction->save();

            }

            /*end post transaction*/

            $response = [
                'balance_local' => $amount_paid_local,
                'balance' => $amount_paid
            ];

            return response()->json($response);
        }

        return response()->json([
            'error' => 'Failed!',
        ], 404);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postTransaction($id, Request $request)
    {
        //
        $optimus        = new Optimus(522588247, 282319719, 383644817);

        $invoice        = UserInvoice::find($optimus->decode($id));

        if($invoice) {

            $invoice->amount_paid   = $invoice->amount_paid + $request->txn_amount;
            $invoice->balance       = $invoice->balance - $request->txn_amount;

            $invoice->amount_paid_local = $invoice->amount_paid_local + $request->txn_amount_local;
            $invoice->balance_local     = $invoice->balance_local - $request->txn_amount_local;

            $invoice_credit_gl          = $invoice->debit_gl;

            $invoice->save();

            $team_id = $invoice->team_id;
            $user_id = 0;

            $last_txn = Transaction::where('team_id', '=', $team_id)
                ->orderBy('created_at', 'desc')
                ->first();          //returns last latest row

            if ($last_txn) {
                $last_txn_number = $last_txn->open_id + 1;
            } else {
                $last_txn_number = 1;
            }

            $accounting = Accounting::where('team_id', '=', $invoice->team_id)
                ->first();
            $payment_debit_gl = $accounting->paymentdb;

            //debit gl
            if ($payment_debit_gl) {

                $transaction = new Transaction;
                $transaction->team_id = $team_id;

                $transaction->open_id = $last_txn_number;

                $transaction->category = $request->category;
                $transaction->txn_type = "D";//$request->type;
                $transaction->txn_date = $request->txn_date;

                $transaction->gl_account_id = $request->debit_gl;
                $transaction->txn_amount = $request->txn_amount_local;
                $transaction->description = $request->description;

                $transaction->created_by_id = $user_id;
                $transaction->modified_by_id = $user_id;

                $transaction->save();

                $gl_account = GL_Account::where('team_id', '=', $team_id)
                    ->where('open_id', '=', $request->debit_gl)
                    ->first();

                if ($gl_account) {
                    $gl_account->int_balance = $gl_account->int_balance + $transaction->txn_amount;
                }

                $gl_account->save();

            }

            //credit gl
            if ($invoice_credit_gl) {
                $transaction = new Transaction;
                $transaction->team_id = $team_id;

                $transaction->open_id = $last_txn_number + 1;

                $transaction->category = $request->category;
                $transaction->txn_type = "C"; //$request->type;    //D or C ??
                $transaction->txn_date = $request->txn_date;

                $transaction->gl_account_id = $request->credit_gl;
                $transaction->txn_amount = $request->txn_amount_local;
                //$transaction->balance      = $request->balance;

                $transaction->description = $request->description;
                //$transaction->flag = 1;
                $transaction->created_by_id = $user_id;
                $transaction->modified_by_id = $user_id;

                $transaction->save();

                $gl_account = GL_Account::where('team_id', '=', $team_id)
                    ->where('open_id', '=', $request->credit_gl)
                    ->first();

                if ($gl_account) {
                    $gl_account->int_balance = $gl_account->int_balance - $transaction->txn_amount;
                }

                $gl_account->save();

            }

            return response()->json('true');
        }

        return response()->json('false');


    }

    /**
     *
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function stripeWebHook($id)
    {
        /*
        $optimus = new Optimus(522588247, 282319719, 383644817);

        $team = Team::where('id','=',$optimus->decode($id))
            ->first();          //returns single row

        $user_gateway = UserGateway::where('team_id','=',$team->id)
            ->where('gateway_id','=','1')           //stripe
            ->first();

        $gate_data = decrypt($user_gateway->gate_data);
        $gate_data = explode("#", $gate_data);

        $stripe_secret = $gate_data[0];

        \Stripe\Stripe::setApiKey($stripe_secret);
        */

        // Retrieve the request's body and parse it as JSON
        //$input = @file_get_contents("php://input");
        //$event_json = json_decode($input);

        // Do something with $event_json

        http_response_code(200);    //middleware VerifyCsrfToken excludes this URI from CSRF protection //for stripe

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function pdftest()
    {

        //showInvoice
        $id = 167503046;

        $optimus = new Optimus(522588247, 282319719, 383644817);

        $invoice = UserInvoice::where('id','=',$optimus->decode($id))
            ->with('contactTab','statusTab','invItemsTab')
            ->first();          //returns single row

        $image_uri = (string) \Image::make('storage/common/piteam_logo.png')->encode('data-url');

        return view('user.common.invquote.common_pdf',compact('id','invoice','image_uri'));
    }

    public function authenticate(Request $request)
    {

        $email      = $request->email;
        $password   = $request->password;

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            // Authentication passed...
            //return Crypt::encrypt(csrf_token());

            $user = Auth::user();
            //$user = \App\User::find(1);
            // Creating a token without scopes...
            $token = $user->createToken('Test Token Name')->accessToken;
            return ['token' => $token];

        }

        return response()->json([
            'error' => 'Record not found',
        ], 404);
        //return response()->json($request);
    }

}
