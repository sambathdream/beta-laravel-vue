<?php

namespace App\Http\Controllers;

use App\Item;
use App\InvItemTax;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Optimus\Optimus;

class ApiInventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $team_id = Auth::user()->currentTeam->id;

        $request = request();

        if (request()->has('sort')) {
            list($sortCol, $sortDir) = explode('|', request()->sort);
            $query = Item::orderBy($sortCol, $sortDir)
                ->where('team_id', '=', $team_id);
        } else {
            $query = Item::orderBy('id', 'asc')
                ->where('team_id', '=', $team_id);
        }
        if ($request->exists('filter')) {
            $query->where(function ($q) use ($request) {
                $value = "%{$request->filter}%";
                $q->where('name', 'like', $value)
                    ->orWhere('item_id', 'like', $value)
                    ->orWhere('description', 'like', $value)
                    ->orWhere('buy_price', 'like', $value)
                    ->orWhere('sell_price', 'like', $value);
            });
        }

        $perPage = request()->has('per_page') ? (int) request()->per_page : null;
// The headers 'Access-Control-Allow-Origin' and 'Access-Control-Allow-Methods'
// are to allow you to call this from any domain (see CORS for more info).
// This is for local testing only. You should not do this in production server,
// unless you know what it means.
        return response()->json($query->paginate($perPage));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //
        $team_id    = Auth::user()->currentTeam->id;
        $user_id    = Auth::user()->id;

        $last_item = Item::where('team_id','=',$team_id)
            ->orderBy('id','desc')
            ->first();          //returns last latest row

        if($last_item) {
            $new_item_num = $last_item->open_id + 1;
        }
        else {
            $new_item_num = 1;
        }

        $item               = new Item;
        $item->team_id      = $team_id;

        $item->open_id      = $new_item_num;

        $item->item_id      = $request->item_id;
        $item->name         = $request->name;

        $item->description  = empty($request->description) ? null : $request->description;

        $item->qty          = empty($request->qty) ? 0 : $request->qty;
        $item->buy_price    = empty($request->buy_price) ? 0 : $request->buy_price;
        $item->sell_price   = empty($request->sell_price) ? 0 : $request->sell_price;

        $item->category     = empty($request->category) ? 1 : $request->category;
        $item->type         = empty($request->type) ? : $request->type;

        //$item->image_url    = $request->image_url;
        $item->debit_gl     = empty($request->debit_gl) ? 0 : $request->debit_gl;
        $item->credit_gl    = empty($request->credit_gl) ? 0 : $request->credit_gl;

        $item->gst_hsnsac_id    = empty($request->gst_hsnsac_id) ? 0 : $request->gst_hsnsac_id;

        $item->created_by_id   = $user_id;
        $item->modified_by_id  = $user_id;

        $item->save();

        if($item->id) {

            $optimus        = new Optimus(522588247, 282319719, 383644817);
            //image generation
            $encrypt_id     = $optimus->encode($item->id);

            $item_img       = \DefaultProfileImage::create($request->name, 256, '#FFF', '#5fbeaa');
            \Storage::put("public/inventory/" . $encrypt_id . ".png", $item_img->encode());

            $item           = Item::find($item->id);
            $item->image    =   "/storage/inventory/" . $encrypt_id . ".png";//$request->image;

            $item->save();

        }

        if($team_id == 1) {
            foreach ($request->tax_items as $tax_row) {

                $invTaxItem = new InvItemTax;

                $invTaxItem->team_id = $team_id;
                $invTaxItem->item_num = $item->id;

                $tax_row_open_id = empty($tax_row['tax_open_id']) ? null : $tax_row['tax_open_id'];
                $invTaxItem->open_id = empty($tax_row['id']) ? $tax_row_open_id : $tax_row['id'];

                $invTaxItem->tax_id = empty($tax_row['tax_id']) ? null : $tax_row['tax_id'];

                $tax_row_tax_name = empty($tax_row['tax_name']) ? null : $tax_row['tax_name'];
                $invTaxItem->name = empty($tax_row['name']) ? $tax_row_tax_name : $tax_row['name'];

                $tax_row_tax_rate = empty($tax_row['tax_rate']) ? 0 : round($tax_row['tax_rate'], 3);
                $invTaxItem->rate = empty($tax_row['rate']) ? $tax_row_tax_rate : round($tax_row['rate'], 3);

                $tax_row_tax_amount = empty($tax_row['tax_amount']) ? 0 : round($tax_row['tax_amount'], 2);
                $invTaxItem->amount = empty($tax_row['amount']) ? $tax_row_tax_amount : round($tax_row['amount'], 2);

                $tax_row_tax_gl = empty($tax_row['tax_gl']) ? null : $tax_row['tax_gl'];
                $invTaxItem->gl_account_id = empty($tax_row['gl_account_id']) ? $tax_row_tax_gl : $tax_row['gl_account_id'];

                $invTaxItem->tax_display = empty($tax_row['tax_display']) ? false : $tax_row['tax_display'];
                $invTaxItem->recoverable = empty($tax_row['recoverable']) ? false : $tax_row['recoverable'];
                $invTaxItem->compound = empty($tax_row['compound']) ? false : $tax_row['compound'];

                $invTaxItem->type = 'TAX';

                $invTaxItem->save();

            }
        }

        return response()->json('true');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $team_id = Auth::user()->currentTeam->id;
        $user_id = Auth::user()->id;

        $item    = Item::where('team_id','=',$team_id)
            ->with('inventoryTaxTab','hsnSacTab')
            ->where('open_id','=',$id)
            ->first();

        return response()->json($item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        ////
        $team_id    = Auth::user()->currentTeam->id;
        $user_id    = Auth::user()->id;

        $item       = Item::where('team_id','=',$team_id)
            ->where('open_id','=',$id)
            ->first();

        $item->item_id      = $request->item_id;
        $item->name         = $request->name;
        $item->description  = $request->description;

        if($request->qty) {
            $item->qty = $request->qty;
        }
        if($request->buy_price) {
            $item->buy_price = $request->buy_price;
        }
        if($request->sell_price) {
            $item->sell_price = $request->sell_price;
        }
        $item->category     = $request->category;
        $item->type         = $request->type;

        //$item->image_url    = $request->image_url;
        if($request->debit_gl) {
            $item->debit_gl = $request->debit_gl;
        }
        if($request->credit_gl) {
            $item->credit_gl = $request->credit_gl;
        }

        $item->modified_by_id  = $user_id;

        $item->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user_id    = Auth::user()->id;
        $team_id    = Auth::user()->currentTeam->id;

        $item       = Item::where('team_id','=',$team_id)
                        ->where('open_id','=',$id)
                        ->first();

        if($item) {

            $item->modified_by_id = $user_id;   //user who deleted the contact
            $item->save();

            Item::find($item->id)->delete();

            $invItemTaxes = InvItemTax::where('team_id', '=', $team_id)
                ->where('item_num', '=', $item->id)
                ->delete();

        }

        return response()->json('true');
    }

    public function listItems(Request $request)
    {
        //need to fix this - we can't return ID to the frontend
        $q=$request->input('q');

        $team_id = Auth::user()->currentTeam->id;
        $user_id    = Auth::user()->id;

        if($q)
        {
            $items = Item::where('team_id','=',$team_id)
                ->with('inventoryTaxTab','hsnSacTab')
                ->where('name','like','%'.$q.'%')
                ->get();
        }
        else
        {
            $items = Item::where('team_id','=',$team_id)
                ->with('inventoryTaxTab','hsnSacTab')
                ->get();
            //::select(['id', 'name', 'email','image','bill_address1','bill_address2'])
            //->get()

        }

        //return Contact::find(1);
        if($q)
        {
            $returnJson["total_count"] = count($items);
            $returnJson["incomplete_results"] = false;
            $returnJson["items"] = $items;

            return response()->json($returnJson);
        }
        else
            return response()->json($items);
    }

}
