<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\InvoiceCreated;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;
//use Ignited\LaravelOmnipay\Facades\OmnipayFacade as Omnipay;
use Omnipay\Omnipay;
use Razorpay\Api\Api;
use Charts;
use Swap;
use GeoIP;
use Jenssegers\Optimus\Optimus;
use App\UserInvoice;
use App\UserInvoiceItem;
use App\Contact;
use App\Country;
use App\Localization;
use App\User;
use App\Team;
use App\Team_Detail;
use App\UserGateway;
use App\StripeBilling;
use App\Accounting;
use App\UserPayment;
use App\GL_Account;

use Excel;

use Carbon\Carbon;

use App\Traits\emailSummaryTrait;
use App\Traits\stripeBillingTrait;

class HomeController extends Controller
{

    use stripeBillingTrait, emailSummaryTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');

        // $this->middleware('teamSubscribed');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function show()
    {
        //return view('home');
        $chart = Charts::create('line', 'highcharts')
            ->title('My nice chart')
            ->labels(['First', 'Second', 'Third'])
            ->values([5,10,20])
            ->dimensions(1000,500)
            ->responsive(false);
        return view('temp.graph', ['chart' => $chart]);
    }

    public function temp2()
    {
        $gateway = Omnipay::create('Stripe');
        $gateway->setApiKey('sk_test_uvUwubu1Dd8qeF3Yvxr8b8ro');

        $formData = array('number' => '4242424242424242', 'expiryMonth' => '6', 'expiryYear' => '2019', 'cvv' => '123');
        $response = $gateway->purchase(array('amount' => '10.00', 'currency' => 'USD', 'card' => $formData))->send();

        if ($response->isSuccessful()) {
            // payment was successful: update database
            return response()->json($response);

        } elseif ($response->isRedirect()) {
            // redirect to offsite payment gateway
            return $response->redirect();
        } else {
            // payment failed: display message to customer
            return response()->json($response->getMessage());
        }

        return view('temp.temp');
    }

    public function testRazor()
    {

        //return view('temp.temp3');//->withEncryptedCsrfToken(Crypt::encrypt(csrf_token()));
    }

    public function razorPost()
    {
        $api_key = 'rzp_test_gHOsYKzfAMk99I';
        $api_secret = '09pk1qg1LCElgzVD6g5Gavgx';

        $api = new Api($api_key, $api_secret);
        //$api->payment->all($options); // Returns array of payment objects
        //$payment = $api->payment->fetch($id); // Returns a particular payment

        $id = $_POST['razorpay_payment_id'];

        //capture Rs 5100
        $payment = $api->payment->fetch($id)->capture(array('amount'=>5100));

        return response()->json($payment);
        //$amount = 10;
        //$api->payment->fetch($id)->capture(array('amount'=>$amount)); // Captures a payment
        //$api->payment->fetch($id)->refund(); // Refunds a payment
        //$api->payment->fetch($id)->refund(array('amount'=>$refundAmount)); // Partially refunds a payment

        // To get the payment details
        //echo $payment->amount;
        //echo $payment->currency;
        // And so on for other attributes


        return view('temp.temp');
    }

    public function yodlee()
    {

        $yodleeApiUrl = 'https://developer.api.yodlee.com/ysl/restserver/v1';
        $yodleeApiCobrandLogin = 'sbCobfasterfene86';
        $yodleeApiCobrandPassword = 'cba61175-3421-4e2d-8cb1-6bde59bad3f1';

// Create a new instance of the SDK.
        $yodleeApi = new \YodleeApi\Client($yodleeApiUrl);

// Login the cobrand.
        $yodleeApi->cobrand()->login($yodleeApiCobrandLogin, $yodleeApiCobrandPassword);

        $yodleeApi->user()->login('sbMemfasterfene861', 'sbMemfasterfene861#123');

// Fetch all available banks, institutions etc. that are supported by Yodlee.
        //$providers = $yodleeApi->providers()->get();

        //return response()->json($providers);

        // return view('temp.temp2'); //->withEncryptedCsrfToken(Crypt::encrypt(csrf_token()));
    }

    public function country()
    {

        $ip = (string) GeoIP::getClientIP();
        $test = geoip()->getLocation('27.974.399.65');

        //return json_encode(array_values((array)geoip()->getLocation($ip))[0]);
        return response()->json(geoip()->getLocation($ip)->toArray());


        // $countries = \Webpatser\Countries\Countries::orderBy('name', 'asc')->select('name', 'iso_3166_2 AS code')->get()->toArray();
        // return $countries;

        // Get the latest EUR/USD rate
        $rate = Swap::latest('USD/INR', ['cache' => false]);

        //$rate->getDate()->format('Y-m-d');

        //$rate = Swap::historical('USD/INR', \Carbon\Carbon::createFromDate(2010, 9, 26));

        return response()->json($rate->getValue());

    }

    public function report()
    {


        return view('temp.report');

    }

    public function invoiceTemplateTest()
    {


        return view('user.common.invquote.temp_view');

    }

    public function invoiceView()
    {
        //
        //showInvoice
        $id = 1280784997;

        //https://finance.pi.team/public/invoices/1280784997/download
        $optimus = new Optimus(522588247, 282319719, 383644817);

        $invoice = UserInvoice::where('id','=',$optimus->decode($id))
            ->first();          //returns single row
        //

        if($invoice){

            $invoiceitems   =   UserInvoiceItem::where('invoice_num','=',$invoice->id)->get();

            //$invoicestatus  =   UserInvoiceStatus::find($invoice->invoice_status_id);

            $contact        =   Contact::find($invoice->contact_id);

            //return view('user.invoices.view_invoice',compact('id','contact','invoice','invoiceitems','invoicestatus'));

            $image_uri = (string) \Image::make('storage/common/piteam_logo.png')->encode('data-url');

            return view('user.invoices.pdf_invoice', compact('id','contact','invoice','invoiceitems','image_uri'));
            //return $pdf->download('invoice.pdf');

        }
        else{

            return ('error'); //redirect()->action('UserInvoiceController@index');
        }
    }

    public function stripeHookTestPost(Request $request, $id)
    {

        $optimus     = new Optimus(914614073, 864868105, 2138572997);
        $dec_team_id = $optimus->decode($id);
        $team        = Team::where('id', '=', $dec_team_id)->first();
        $team_id     = $team->id;

        if(!$team){

            return new Response('Webhook Failed', 500); //return error response

        }

        $user_gateway = UserGateway::where('team_id', '=', $team_id)
            ->where('gateway_id', '=', 1)
            ->first();

        $gate_data = decrypt($user_gateway->gate_data);
        $gate_data = explode("#", $gate_data);

        \Stripe\Stripe::setApiKey($gate_data[0]);
        //\Stripe\Stripe::setApiKey("sk_test_Ohxdc5JY7QcRUU1NpA3F60C7");

        $payload = $request->getContent();

        $stripe_billing = StripeBilling::where('team_id', '=', $team_id)->first();
        // You can find your endpoint's secret in your webhook settings
        $endpoint_secret = $stripe_billing->signingsecret;
        //"whsec_O2GPL6bhOmoyi0ixl7FuDBgvXjSuIiPu";

        $sig_header = $_SERVER["HTTP_STRIPE_SIGNATURE"];

        $event = null;

        try {
            $event = \Stripe\Webhook::constructEvent(
                $payload, $sig_header, $endpoint_secret
            );
        } catch(\UnexpectedValueException $e) {
            // Invalid payload
            return new Response('Webhook Failed', 400); // PHP 5.4 or greater
            exit();
        } catch(\Stripe\Error\SignatureVerification $e) {
            // Invalid signature
            return new Response('Webhook Failed', 400); // PHP 5.4 or greater
            exit();
        }


        //return new Response('Webhook Success', 200);
        //$payload = json_decode($request->getContent(), true);

        $method = 'stripe'.studly_case(str_replace('.', '_', $request->type));

        //return response()->json($method);

        if($method == 'stripeCustomerUpdated'
            or $method == 'stripeCustomerCreated'
            //or $method == 'stripeCustomerSourceCreated'
            //or $method == 'stripeCustomerSourceUpdated'
        )
        {

            $contact_id         = $request->data['object']['id'];

            $contact            = Contact::where('team_id', '=', $team_id)
                ->where('id_no', '=', $contact_id)
                ->first();

            if(!$contact){

                $last_contact   = Contact::where('team_id', '=', $team_id)
                    ->orderBy('id', 'desc')
                    ->first();          //returns last latest row

                if ($last_contact) {
                    $new_contact_number = $last_contact->open_id + 1;
                } else {
                    $new_contact_number = 1;
                }

                $contact                    =   new Contact;
                $contact->team_id           =   $team_id;
                $contact->open_id           =   $new_contact_number;
                $contact->id_no             =   $request->data['object']['id'];
                $contact->payment_terms     =   0; //due on receipt

                //$contact->image = "https://robohash.org/" . $contact->id_no;
                $contact->image = "https://api.adorable.io/avatars/40/" . $contact->id_no;

            }


            $sources                    = empty($request->data['object']['sources']['data'][0])
                ? null : $request->data['object']['sources']['data'][0];

            if($sources) {

                $contact->name = empty($sources['name'])
                    ? 'Stripe_' . $request->data['object']['id'] : $sources['name'];

                $contact->email = empty($request->data['object']['email'])
                    ? null : $request->data['object']['email'];

                /*
                $country    = empty($sources['address_country'])
                    ? $sources['country'] : substr($sources['address_country'],0,2);
                */
                $country    = empty($sources['country']) ?
                    $sources['country'] : substr($sources['address_country'],0,2);

                if($country) {

                    $contact_curr = Country::where('iso_3166_2', '=', $country)
                        ->first();

                    $contact->currency_id = $contact_curr->id;
                }

                $contact->bill_address1 = empty($sources['address_line1'])
                    ? null : ucwords(strtolower($sources['address_line1']));

                $contact->bill_address2 = empty($sources['address_line2'])
                    ? null : ucwords(strtolower($sources['address_line2']));

                $contact->bill_city = empty($sources['address_city'])
                    ? null : ucwords(strtolower($sources['address_city']));

                $contact->bill_state = empty($sources['address_state'])
                    ? null : ucwords(strtolower($sources['address_state']));

                $contact->bill_postal_code = empty($sources['address_zip'])
                    ? null : $sources['address_zip'];

                $contact->bill_country_id = empty($contact_curr->iso_3166_2)
                    ? $sources['country'] : $contact_curr->iso_3166_2;

            }
            elseif($request->data['object']['email']){

                $contact->name = $request->data['object']['email'];
                $contact->email = $request->data['object']['email'];
            }

            $contact->save();

            //return response()->json($contact);

        }

        //customer.updated
        //invoice.created
        //charge.succeeded
        //InvoicePaymentSucceeded

        if($method == 'stripeInvoiceCreated'
            //or $method == 'stripeInvoiceitemCreated'
        )
        {

            $contact_id                 = $request->data['object']['customer'];

            //return response()->json();

            if($contact_id) {

                $contact = Contact::where('team_id', '=', $team_id)
                    ->where('id_no', '=', $contact_id)
                    ->first();


                $invoice = UserInvoice::where('team_id', '=', $team_id)
                    ->where('status_id', '=', 6) //retrieve any open bill
                    ->where('contact_id','=',$contact->id)
                    ->orderBy('id', 'desc')
                    ->first();          //returns last latest row

                if (!$invoice) {    //if no invoice then create new else

                    $invoice                    = new UserInvoice;
                    $invoice->team_id           = $team_id;
                    $invoice->contact_id        = $contact->id;
                    $invoice->status_id         = 6; //open bills by stripe
                    $invoice->title             = 'Invoice';

                    $last_invoice               = UserInvoice::where('team_id', '=', $team_id)
                        ->orderBy('id', 'desc')
                        ->first();          //returns last latest row

                    if ($last_invoice) {
                        $new_invoice_number = $last_invoice->open_id + 1;
                    } else {
                        $new_invoice_number = 1;
                    }

                    $invoice->open_id           = $new_invoice_number;

                    $invoice->sub_total         = 0;

                    $invoice->discount_rate1    = 0;
                    $invoice->discount_amount   = 0;
                    $invoice->tax_amount        = 0;

                    $invoice->total_amount      = 0;
                    $invoice->amount_paid       = 0;
                    $invoice->balance           = 0;

                    $invoice->currency_id       = 840; //usa
                    $invoice->currency_id_local = 840; //usa

                    $invoice->currency_rate     = 1;
                    $invoice->total_amount_local    = $invoice->currency_rate * $invoice->total_amount;
                    $invoice->amount_paid_local     = $invoice->currency_rate * $invoice->amount_paid;
                    $invoice->balance_local         = $invoice->currency_rate * $invoice->balance;

                }               //else continue updating existing invoice line items

                //existing invoice line items
                $invoice->post_date         = Carbon::now()->toDateTimeString();
                $invoice->due_date          = Carbon::now()->toDateTimeString();

                $invoice->debit_gl          = 1;
                $invoice->credit_gl_cb      = false;

                if($method == 'stripeInvoiceCreated') {

                    $team_detail = Team_Detail::where('team_id', '=', $team_id)
                        ->first();
                    if ($team_detail) {
                        $invoice->notes     = $team_detail->inv_footer;
                        $invoice->footer    = $team_detail->inv_notes;
                    }

                    $invoice->created_by_id = 0; //automatically created
                    $invoice->modified_by_id = 0; //automatically created


                    if($request->data['object']['lines']['data'][0]['object'] == "line_item")
                    {
                        $line_items = $request->data['object']['lines']['data'];
                    }

                }
                elseif($method == 'stripeInvoiceitemCreated'){

                    $line_items[0] = $request->data['object'];

                }

                $invoice->save();
                //return response()->json($line_items);

                $sub_total = empty($invoice->sub_total) ? 0 : $invoice->sub_total;

                foreach($line_items as $item){


                    $invoiceItem                = new UserInvoiceItem;

                    $invoiceItem->team_id       = $team_id;
                    $invoiceItem->invoice_num   = empty($invoice->id) ? null : $invoice->id;
                    //$invoiceItem->contact_id    = $request->contact_id;

                    $invoiceItem->item_id       = empty($item['plan']['id'])
                        ? 'Invoice' : ucwords(strtolower($item['plan']['id']
                            . ' ' . $item['plan']['object']));

                    $invoiceItem->item_name     = empty($item['description'])
                        ? 'Charge' : ucwords(strtolower($item['description']));

                    $invoiceItem->item_price    = empty($item['amount'])
                        ? 0 : ($item['amount']/100);

                    $invoiceItem->item_qty      = 1;

                    $invoiceItem->item_total    = empty($item['amount'])
                        ? 0 : ($item['amount']/100);

                    $invoiceItem->credit_gl     = null;

                    $invoiceItem->save();

                    //return response()->json($invoiceItem);

                    /*
                    if($invoice->credit_gl_cb and $invoice->status_id != 5) {

                        if (!empty($item['credit_gl'])) {

                            $transaction = new Transaction;
                            $transaction->team_id = $team_id;

                            $last_txn_number = $last_txn_number + 1;

                            $transaction->open_id = $last_txn_number;

                            $transaction->category = 47;
                            $transaction->txn_type = "C";//$request->type;
                            $transaction->txn_date = $invoice->post_date;

                            $transaction->gl_account_id = $invoiceItem->credit_gl;
                            $transaction->txn_amount = $invoiceItem->item_total * $invoice->currency_rate;

                            $transaction->description = 'Invoice Sale';
                            $transaction->flag = 1;
                            $transaction->created_by_id = $user_id;
                            $transaction->modified_by_id = $user_id;

                            $transaction->save();

                            $gl_account = GL_Account::where('team_id', '=', $team_id)
                                ->where('open_id', '=', $invoiceItem->credit_gl)
                                ->first();

                            if ($gl_account) {
                                $gl_account->int_balance = $gl_account->int_balance - $transaction->txn_amount;
                            }

                            $gl_account->save();

                            $transaction->balance = $gl_account->int_balance;

                            //$transaction->txn_ref = ;
                            $transaction->inv_ref = $invoice->id;
                            //$transaction->pay_ref = ;

                            $transaction->save();

                        }

                    }
                    */

                    if($invoice->status_id == 5)
                    {
                        UserInvoiceItem::find($invoiceItem->id)->delete();
                    }

                    $sub_total  = $sub_total + $invoiceItem->item_total;

                }

                $invoice->sub_total         = $sub_total;

                $invoice->discount_rate1    = 0;
                $invoice->discount_amount   = 0;
                $invoice->tax_amount        = 0;

                $invoice->total_amount      = $sub_total;
                $invoice->amount_paid       = 0;
                $invoice->balance           = $sub_total;

                $invoice->currency_rate         = 1;
                $invoice->total_amount_local    = $invoice->currency_rate * $invoice->total_amount;
                $invoice->amount_paid_local     = $invoice->currency_rate * $invoice->amount_paid;
                $invoice->balance_local         = $invoice->currency_rate * $invoice->balance;

                $invoice->save();

                //return response()->json($invoice);

            }

        }

        if($method == 'stripeChargeSucceeded')
        {
            $contact_id                 = $request->data['object']['customer'];

            //return response()->json($contact_id);

            if($contact_id) {

                $contact = Contact::where('team_id', '=', $team_id)
                    ->where('id_no', '=', $contact_id)
                    ->first();

                $invoice = UserInvoice::where('team_id', '=', $team_id)
                    ->where('status_id', '=', 6)//retrieve any open bill
                    ->where('contact_id', '=', $contact->id)
                    //->orderBy('id', 'desc')
                    ->first();          //returns last latest row

                /*
                if (!$invoice) {    //if no invoice then create new else

                    $invoice                = new UserInvoice;
                    $invoice->team_id       = $team_id;
                    $invoice->contact_id    = $contact->id;
                    $invoice->status_id     = 6; //open bills by stripe
                    $invoice->title         = 'Invoice';

                    $last_invoice = UserInvoice::where('team_id', '=', $team_id)
                        ->orderBy('id', 'desc')
                        ->first();          //returns last latest row

                    if ($last_invoice) {
                        $new_invoice_number = $last_invoice->open_id + 1;
                    } else {
                        $new_invoice_number = 1;
                    }

                    $invoice->open_id = $new_invoice_number;

                }               //else continue updating existing invoice line items
                */
                if($invoice) {

                    $amount_paid            = $request->data['object']['amount'] / 100;
                    $invoice->amount_paid   = (empty($invoice->amount_paid) ? 0 : $invoice->amount_paid)
                        + ($amount_paid);

                    $invoice->balance       = (empty($invoice->balance) ? 0 : $invoice->balance)
                        - $amount_paid;

                    $invoice->amount_paid_local = $invoice->currency_rate * $invoice->amount_paid;
                    $invoice->balance_local     = $invoice->currency_rate * $invoice->balance;

                    $invoice->save();

                }

            }
            else{
                return new Response('Webhook Failed', 500);
            }
        }

        return new Response('Webhook Handled', 200);
        //return response()->json($request);

    }

    public function stripeHookTestGet(Request $request, $id)
    {

        //return response()->json($request->getContent());

    }


    public function retStripeCustomers()
    {
        $team_id        = Auth::user()->currentTeam->id;
        $user_id        = 0;

        $stripe_cust_id = 'cus_AAc8oXWxpl3zzP';

        //$stripe_cust_request = $this->retrieveStripeCustomer($stripe_cust_id,$team_id);

        //return $stripe_cust_request;


        $user_gateway = UserGateway::where('team_id', '=', $team_id)
            ->where('gateway_id', '=', 1)
            ->first();

        $gate_data = decrypt($user_gateway->gate_data);
        $gate_data = explode("#", $gate_data);

        \Stripe\Stripe::setApiKey($gate_data[0]);

        $stripe_cust_list = \Stripe\Customer::all(array("limit" => 50));//

        //return json_encode($stripe_cust_list);

        $stripe_cust_list_array = (array) $stripe_cust_list['data'];

        //return $stripe_cust_list_array;

        foreach($stripe_cust_list_array as $request) {

            $contact_id_no      = $request['id'];
            $contact_name_email = $request['email'];

            //echo $contact_name_email;

            $contact            = Contact::where('team_id', '=', $team_id)->where('id_no', '=', $contact_id_no)->first();

            if(!$contact)
            {

                echo $contact_id_no . '\n';

                $contact = $this->createStripeContact($contact_id_no, $contact_name_email, $team_id, $user_id);

                //$contact = $this->updateStripeAPIContact((array)$stripe_cust, $contact, $team_id, $user_id);

                $default_source = $request['default_source'];

                $sources = null;

                if ($default_source) {

                    foreach ($request['sources']['data'] as $source) {
                        if ($default_source == $source['id']) {
                            $sources = $source;
                            break;
                        }
                    }

                }


                if ($sources) {

                    //return $sources['country'];

                    $contact->name = empty($sources['name']) ? 'Stripe_' . $request['id'] : ucwords(strtolower($sources['name']));

                    $contact->email = empty($request['email']) ? null : strtolower($request['email']);

                    $country = empty($sources['country']) ?
                        $sources['country'] : substr($sources['address_country'], 0, 2);

                    //return $country;

                    if ($country) {

                        $contact_curr = Country::where('iso_3166_2', '=', $country)
                            ->first();

                        if ($contact_curr) {
                            $contact->currency_id = $contact_curr->id;
                        }
                    }

                    $contact->bill_address1 = empty($sources['address_line1'])
                        ? null : ucwords(strtolower($sources['address_line1']));

                    $contact->bill_address2 = empty($sources['address_line2'])
                        ? null : ucwords(strtolower($sources['address_line2']));

                    $contact->bill_city = empty($sources['address_city'])
                        ? null : ucwords(strtolower($sources['address_city']));

                    $contact->bill_state = empty($sources['address_state'])
                        ? null : ucwords(strtolower($sources['address_state']));

                    $contact->bill_postal_code = empty($sources['address_zip'])
                        ? null : $sources['address_zip'];

                    $contact->bill_country_id = empty($contact_curr->iso_3166_2)
                        ? $sources['country'] : $contact_curr->iso_3166_2;

                    $contact->save();


                }

            }

        }
        return 'true';


    }

    public function retStripeInv()
    {
        $team_id        = Auth::user()->currentTeam->id;
        $user_id        = 0;

        $stripe_cust_id = 'cus_AAc8oXWxpl3zzP';

        //$stripe_cust_request = $this->retrieveStripeCustomer($stripe_cust_id,$team_id);

        //return $stripe_cust_request;


        $user_gateway = UserGateway::where('team_id', '=', $team_id)
            ->where('gateway_id', '=', 1)
            ->first();

        $gate_data = decrypt($user_gateway->gate_data);
        $gate_data = explode("#", $gate_data);

        \Stripe\Stripe::setApiKey($gate_data[0]);

        $stripe_inv_list = \Stripe\Invoice::all(array("limit" => 64)); //, "date[lte]" => '1491177599'

        //return json_encode($stripe_inv_list);

        $stripe_inv_list_array = array_reverse((array) $stripe_inv_list['data']);

        //return $stripe_inv_list_array;

        foreach($stripe_inv_list_array as $request) {

            $stripe_inv_no              = $request['id'];
            $stripe_customer            = $request['customer'];

            $contact                    = Contact::where('team_id', '=', $team_id)
                                        ->where('id_no', '=', $stripe_customer)->first();

            if($contact) {


                $invoice = UserInvoice::where('team_id', '=', $team_id)
                                ->where('stripe_id', '=', $stripe_inv_no)
                                ->first();

                if (!$invoice) {

                    $invoice = new UserInvoice;
                    $invoice->team_id = $team_id;
                    $invoice->contact_id = $contact->id;
                    $invoice->status_id = 1; //open bills by stripe
                    $invoice->title = 'Invoice';

                    $last_invoice = UserInvoice::where('team_id', '=', $team_id)
                        ->orderBy('id', 'desc')
                        //->withTrashed()
                        ->first();          //returns last latest row

                    if ($last_invoice) {
                        $new_invoice_number = $last_invoice->open_id + 1;
                    } else {
                        $new_invoice_number = 1;
                    }

                    $invoice->open_id = $new_invoice_number;

                    $invoice->discount_rate1 = 0;
                    $invoice->discount_amount = 0;
                    $invoice->tax_amount = 0;
                    $invoice->sub_total = 0;
                    $invoice->total_amount = 0;
                    $invoice->amount_paid = 0;
                    $invoice->balance = 0;

                    $accounting = Accounting::where('team_id', '=', $team_id)->first();
                    $invoice->debit_gl = $accounting->invoicedb;
                    $invoice->credit_gl_cb = false;

                    //date
                    $invoice->post_date = Carbon::createFromTimestamp($request['date'])->toDateTimeString(); //update date
                    $invoice->due_date = Carbon::createFromTimestamp($request['date'])->toDateTimeString(); //update date

                    $team_detail = Team_Detail::where('team_id', '=', $team_id)->first();

                    if ($team_detail) {

                        $invoice->notes = $team_detail->inv_footer;
                        $invoice->footer = $team_detail->inv_notes;
                    }

                    $invoice->stripe_id = $request['id'];

                    $sub_total = 0;
                    $line_items = null;
                    $invoice->save();               //need to save to get id

                    if ($request['lines']['data'][0]['object'] == "line_item") {
                        $line_items = $request['lines']['data'];


                        foreach ($line_items as $item) {


                            $invoiceItem = new UserInvoiceItem;

                            $invoiceItem->team_id = $team_id;
                            $invoiceItem->invoice_num = empty($invoice->id) ? null : $invoice->id;

                            $invoiceItem->item_price = empty($item['amount']) ? 0 : ($item['amount'] / 100);
                            $invoiceItem->item_qty = 1;
                            $invoiceItem->item_total = empty($item['amount']) ? 0 : ($item['amount'] / 100);
                            $invoiceItem->credit_gl = null;

                            if ($item['type'] == 'invoiceitem') {
                                $invoiceItem->item_id = 'Charge';

                                $invoiceItem->item_name = empty($item['description'])
                                    ? 'No description' : ucwords(strtolower($item['description']));

                            } elseif ($item['type'] == 'subscription') {
                                $invoiceItem->item_id = empty($item['plan']['id'])
                                    ? 'Invoice' : ucwords(strtolower($item['plan']['id'] . ' ' . $item['plan']['object']));

                                $start_date = Carbon::createFromTimestamp($item['period']['start'])->toFormattedDateString();
                                $end_date = Carbon::createFromTimestamp($item['period']['end'])->toFormattedDateString();

                                $stat_descr = empty($item['statement_descriptor']) ? ' ' : ucwords(strtolower($item['statement_descriptor']));

                                $item_name = empty($item['name']) ? ' ' : ucwords(strtolower($item['name']));

                                $item_name = $item_name . $stat_descr . '(' . $start_date . ' - ' . $end_date . ')';

                                $invoiceItem->item_name = $item_name;
                            }

                            $invoiceItem->save();

                            $sub_total = $sub_total + $invoiceItem->item_total;

                        }

                    }

                    $invoice->sub_total = $sub_total;

                    $invoice->discount_rate1 = 0;
                    $invoice->discount_amount = 0;
                    $invoice->tax_amount = 0;

                    $invoice->total_amount = $sub_total;
                    $invoice->amount_paid = 0; //empty($invoice->amount_paid) ? 0 : $invoice->amount_paid;
                    $invoice->balance = $sub_total - $invoice->amount_paid;


                    $invoice->currency_id       = 840;
                    $invoice->currency_id_local = 840;
                    $invoice->currency_rate     = 1;

                    $invoice->total_amount_local = $invoice->currency_rate * $invoice->total_amount;
                    $invoice->amount_paid_local = $invoice->currency_rate * $invoice->amount_paid;
                    $invoice->balance_local = $invoice->currency_rate * $invoice->balance;

                    $invoice->created_by_id = $user_id; //automatically created
                    $invoice->modified_by_id = $user_id; //automatically created

                    $invoice->save();

                    echo " invoice created: " . $invoice->stripe_id;

                }
                else{
                    echo " duplicate invoice: " . $invoice->stripe_id;
                }
            }
            else{
                echo " contact not found: " . $stripe_customer;
            }
        }
        //return 'true';

        echo " end";
    }

    public function retStripeCharge()
    {
        $team_id        = Auth::user()->currentTeam->id;
        $user_id        = 0;

        //$stripe_cust_id = 'cus_AAc8oXWxpl3zzP';

        //$stripe_cust_request = $this->retrieveStripeCustomer($stripe_cust_id,$team_id);

        //return $stripe_cust_request;


        $user_gateway = UserGateway::where('team_id', '=', $team_id)
            ->where('gateway_id', '=', 1)
            ->first();

        $gate_data = decrypt($user_gateway->gate_data);
        $gate_data = explode("#", $gate_data);

        \Stripe\Stripe::setApiKey($gate_data[0]);

        $stripe_charge_list = \Stripe\Charge::all(array("limit" => 10)); //, "date[lte]" => '1491177599'

        //mustafizrahman
        //return json_encode($stripe_charge_list);

        $stripe_charge_list_array = array_reverse((array) $stripe_charge_list['data']);

        //return $stripe_charge_list_array;

        foreach($stripe_charge_list_array as $request) {

            if($request['captured']) {
                $stripe_charge_no = $request['id'];
                $stripe_customer = $request['customer'];
                $stripe_inv_no = $request['invoice'];

                $contact = Contact::where('team_id', '=', $team_id)
                    ->where('id_no', '=', $stripe_customer)->first();

                if ($contact) {


                    $invoice = UserInvoice::where('team_id', '=', $team_id)
                        ->where('stripe_id', '=', $stripe_inv_no)
                        ->first();

                    if ($invoice) {

                        //get the invoice and save
                        $invoice->status_id = 4;
                        $amount_paid = $request['amount'] / 100;
                        $amount_refunded = $request['amount_refunded'] / 100;
                        $amount_paid_local = $invoice->currency_rate * $amount_paid;

                        $invoice->amount_paid = (empty($invoice->amount_paid) ? 0 : $invoice->amount_paid) + ($amount_paid)
                            - $amount_refunded;

                        $invoice->balance = (empty($invoice->balance) ? 0 : $invoice->balance)
                            - $amount_paid;

                        $invoice->amount_paid_local = $invoice->currency_rate * $invoice->amount_paid;
                        $invoice->balance_local = $invoice->currency_rate * $invoice->balance;

                        //$invoice->save();
                        //invoice saved end

                        $last_payment = UserPayment::where('team_id', '=', $team_id)
                            ->orderBy('id', 'desc')
                            ->first();          //returns last latest row

                        if ($last_payment) {
                            $new_pay_number = $last_payment->open_id + 1;
                        } else {
                            $new_pay_number = 1;
                        }

                        $payment = new UserPayment;

                        $payment->team_id = $team_id;
                        $payment->open_id = $new_pay_number;
                        $payment->contact_id = empty($invoice->contact_id) ? null : $invoice->contact_id;
                        $payment->invoice_id = empty($invoice->id) ? null : $invoice->id;

                        $payment->payment_type_id = 5;

                        $payment->amount = $amount_paid;
                        $payment->amount_local = $amount_paid_local;

                        $payment->transaction_reference = 'Stripe Pay';
                        $payment->payment_date = Carbon::now()->toDateTimeString();;

                        $payment->stripe_charge_id = $stripe_charge_no;

                        $payment->created_by_id = $user_id;
                        $payment->modified_by_id = $user_id;

                        $payment->save();

                        echo " payment no: " . $stripe_charge_no . " & invoice no " . $stripe_inv_no;

                    } else {
                        echo " duplicate invoice: " . $invoice->stripe_id;
                    }
                } else {
                    echo " contact not found: " . $stripe_customer;
                }

            }

        }
        //return 'true';

        echo " end";
    }

    public function emailSummaryTest()
    {
        //return $this->finSummary();

        /*
        Mail::raw('Text to e-mail', function ($message) {

            $message->from('support@pimail.team', 'Your Application');

            $message->to('lineflux@gmail.com', 'test')->subject('Your Reminder!');
            //
        });

        return;
        */

        //Update tax account

        /*
        $invoice = Contact::where('id','=','510')
            ->first()
            ->toArray();          //returns single row

        //return response()->json($invoice);

        Excel::create('Filename', function($excel) use ($invoice) {


            $excel->sheet('Sheetname', function($sheet) use ($invoice) {

                $sheet->fromArray($invoice);


            });

        })->download('xls');
        */
        return;
        //update payment table start

        $invoices                    = UserInvoice::where('team_id','=','107')
                                            ->where('open_id','>','1261')
                                            ->where('open_id','<=','1401')
                                            ->get();

        //return response()->json($invoices);
        $start = 1262;

        foreach($invoices as $inv){

            $inv->open_id = $start;

            $start = $start + 1;

            //$inv->save();
        }


        return response()->json('true');

        /*
        $last_payment                   = UserPayment::where('team_id', '=', '107')
            ->orderBy('id', 'desc')
            ->first();          //returns last latest row

        if ($last_payment) {
            $new_pay_number         = $last_payment->open_id + 1;
        } else {
            $new_pay_number         = 1;
        }

        $payment                        = new UserPayment;

        $payment->team_id               = 107;
        $payment->open_id               = $new_pay_number;
        $payment->contact_id            = empty($invoice->contact_id) ? null : $invoice->contact_id;
        $payment->invoice_id            = empty($invoice->id) ? null : $invoice->id;

        $payment->payment_type_id       = 5;

        $payment->amount                = $invoice->total_amount;
        $payment->amount_local          = $invoice->total_amount_local;

        $payment->transaction_reference = 'Refund';
        $payment->payment_date          = Carbon::now()->toDateTimeString();

        $payment->stripe_charge_id      = 'ch_1AVDVeF06MLhX8atCW20FSAe';

        $payment->created_by_id         = 0;
        $payment->modified_by_id        = 0;

        //$payment->save();

        return response()->json($payment);
        //update payment table end

        $teams = Team::where('id','>','145')
                    //->where('id','<','145')
                    ->get();

        //return response()->json($teams);

        */
        foreach($teams as $ret_team) {

            /*
            $gl_account          = new GL_Account;
            $gl_account->team_id = $ret_team->id;
            $gl_account->open_id = 7;
            $gl_account->type    = 7; //Other Current Liability
            $gl_account->name    = 'Income Tax'; //$request->name;
            $gl_account->int_balance    = 0;
            $gl_account->ext_balance    = 0;
            $gl_account->description    = 'Taxes To Be Paid';
            $gl_account->created_by_id  = 0;
            $gl_account->modified_by_id = 0;
            $gl_account->save();
            */

            /*
            $gl_account               = new GL_Account;
            $gl_account->team_id      = $ret_team->id;
            $gl_account->open_id      = 8;
            $gl_account->type         = 11; //Income Contra Account - Sales discount
            $gl_account->name         = 'Sales Discount'; //$request->name;
            $gl_account->int_balance  = 0;
            $gl_account->ext_balance  = 0;
            $gl_account->description  = 'Sales Discount (Contra Income Account)';
            $gl_account->created_by_id   = 0;
            $gl_account->modified_by_id  = 0;
            $gl_account->save();
            */

        }

        return response()->json($teams);
        //update tax account


        $team = Team::where('id','=',46)->first();

        //return \Hash::make('password');

        $team_id = $team->id;

        $user = User::where('id', '=', $team->owner_id)
            ->first();
        //return $team->name;


        if(!$user){
            //continue;
        }

        $team_name = $team->name;

        $recent_invoices = UserInvoice::orderBy('id', 'DESC')
            ->where('team_id', '=',$team_id)
            ->with('statusTab', 'contactTab')
            ->take(5)
            ->get();

        /*
        if(!$recent_invoices){

            echo ' no invoice (' . $team_id . ') ';
            continue;
        }
        */

        $invoice_count = empty($recent_invoices->count()) ? 0 : $recent_invoices->count();

        $lastweek_sunday = Carbon::parse('previous sunday')->toDateString();
        $lastweek_monday = Carbon::parse('previous sunday')->subDays(6)->toDateString();

        //$currentmonth           = Carbon::parse('this month')->format('F Y');
        $thismonth_start = Carbon::parse('first day of this month')->toDateString();
        $thismonth_end = Carbon::parse('last day of this month')->toDateString();

        $thisyr_start = Carbon::parse('first day of this year')->toDateString();
        $thisyr_end = Carbon::parse('last day of this year')->toDateString();

        $sales_last_week = UserInvoice::where('team_id', '=', $team_id)
            ->where('post_date', '>=', $lastweek_monday)
            ->where('post_date', '<=', $lastweek_sunday)
            ->sum('total_amount');

        $sales_this_month = UserInvoice::where('team_id', '=', $team_id)
            ->where('post_date', '>=', $thismonth_start)
            ->where('post_date', '<=', $thismonth_end)
            ->sum('total_amount');

        $sales_this_yr = UserInvoice::where('team_id', '=', $team_id)
            ->where('post_date', '>=', $thisyr_start)
            ->where('post_date', '<=', $thisyr_end)
            ->sum('total_amount');

        $pay_last_week = UserPayment::where('team_id', '=', $team_id)
            ->where('payment_date', '>=', $lastweek_monday)
            ->where('payment_date', '<=', $lastweek_sunday)
            ->sum('amount');

        $pay_this_month = UserPayment::where('team_id', '=', $team_id)
            ->where('payment_date', '>=', $thismonth_start)
            ->where('payment_date', '<=', $thismonth_end)
            ->sum('amount');

        $pay_this_yr = UserPayment::where('team_id', '=', $team_id)
            ->where('payment_date', '>=', $thisyr_start)
            ->where('payment_date', '<=', $thisyr_end)
            ->sum('amount');

        $localization = Localization::where('team_id', '=', $team_id)->first();

        $comp_country = Country::where('id', '=', $localization->currency_id)->first();

        $lastweek_monday_human = Carbon::parse('previous sunday')->subDays(6)->toFormattedDateString();
        $lastweek_sunday_human = Carbon::parse('previous sunday')->toFormattedDateString();

        $subject            = 'From your Pi.TEAM Account: Activity Report for ' . $lastweek_sunday_human;
        $from_email         = 'your-week@mailer.pi.team';//Auth::user()->email;
        $from_name          = 'Team Pi';
        $to_emails          = $user->email; //'lineflux@gmail.com';
        $support_email      = 'support@pi.team';
        $support_name       = 'Pi Support';
        $cc_email           = '';
        $pathToFile         = '';

        return view('emails.weekly.fin-summary', compact('recent_invoices',
            'sales_last_week','sales_this_month','pay_last_week','pay_this_month',
            'sales_this_yr','pay_this_yr','invoice_count','comp_country','team_name',
            'lastweek_monday_human', 'lastweek_sunday_human'));
    }

}
