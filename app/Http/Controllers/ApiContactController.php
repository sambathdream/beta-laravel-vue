<?php

namespace App\Http\Controllers;

use App\Contact;

use App\UserInvoice;
use App\UserInvoiceItem;
use App\UserInvoiceTax;

use App\UserQuote;
use App\UserQuoteItem;
use App\UserQuoteTax;

use App\UserPayment;
use App\Transaction;
use App\GL_Account;

use App\UserCredit;
use App\UserCreditItem;
use App\UserCreditTax;
use App\UserRefund;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Optimus\Optimus;

use App\Traits\contactTrait;

class ApiContactController extends Controller
{

    use contactTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

        $team_id = Auth::user()->currentTeam->id;

        $request = request();

        /*
        
            $query = Contact::select(\DB::raw('contacts.open_id, contacts.name, contacts.image, 
                contacts.email, contacts.phone, contacts.contact1, contacts.website,
                SUM(user_invoices.balance_local) As balance'))
                ->leftJoin('user_invoices', 'user_invoices.contact_id', '=', 'contacts.id')
                ->where('contacts.team_id', $team_id)
                ->groupBy('contacts.id');

        */

            if (request()->has('sort')) {
                list($sortCol, $sortDir) = explode('|', request()->sort);
                $query = Contact::orderBy($sortCol, $sortDir)
                    ->where('team_id', '=', $team_id);
            } else {
                $query = Contact::orderBy('id', 'asc')
                    ->where('team_id', '=', $team_id);
            }


        if ($request->exists('filter')) {
            $query->where(function ($q) use ($request) {
                $value = "%{$request->filter}%";
                $q->where('name', 'like', $value)
                    ->orWhere('phone', 'like', $value)
                    ->orWhere('email', 'like', $value);
            });
        }

        $perPage = request()->has('per_page') ? (int)request()->per_page : null;
// The headers 'Access-Control-Allow-Origin' and 'Access-Control-Allow-Methods'
// are to allow you to call this from any domain (see CORS for more info).
// This is for local testing only. You should not do this in production server,
// unless you know what it means.
        return response()->json($query->paginate($perPage));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $team_id = Auth::user()->currentTeam->id;
        $user_id = Auth::user()->id;

        $contact = $this->createContact($request, $team_id, $user_id);

        return response()->json($contact);

        /*
        $last_contact = Contact::where('team_id', '=', $team_id)
            ->orderBy('id', 'desc')
            ->first();          //returns last latest row

        if ($last_contact) {
            $new_contact_number = $last_contact->open_id + 1;
        } else {
            $new_contact_number = 1;
        }

        $contact = new Contact;

        $contact->team_id = $team_id;
        $contact->open_id = $new_contact_number;

        $contact->name = ucwords(strtolower($request->name));
        $contact->email = strtolower($request->email);

        $contact->phone = empty($request->phone) ? null : $request->phone;
        $contact->contact1 = ucwords(strtolower($request->contact1));
        $contact->contact2 = ucwords(strtolower($request->contact2));

        $contact->currency_id = empty($request->currency_id) ? 840 : $request->currency_id;
        $contact->payment_terms = empty($request->payment_terms) ? 15 : $request->payment_terms;

        $contact->bill_address1 = ucwords(strtolower($request->bill_address1));
        $contact->bill_address2 = ucwords(strtolower($request->bill_address2));

        $contact->bill_city = ucwords(strtolower($request->bill_city));
        $contact->bill_state = ucwords(strtolower($request->bill_state));
        $contact->bill_postal_code = $request->bill_postal_code;
        $contact->bill_country_id = $request->bill_country_id;

        $contact->ship_phone = $request->ship_phone;
        $contact->ship_contact = ucwords(strtolower($request->ship_contact));
        $contact->ship_address1 = ucwords(strtolower($request->ship_address1));
        $contact->ship_address2 = ucwords(strtolower($request->ship_address2));
        $contact->ship_city = ucwords(strtolower($request->ship_city));
        $contact->ship_state = $request->ship_state;
        $contact->ship_postal_code = $request->ship_postal_code;
        $contact->ship_country_id = $request->ship_country_id;
        $contact->instructions = $request->instructions;

        $contact->account_no = $request->account_no;
        $contact->id_no = $request->id_no;
        $contact->vat_no = $request->vat_no;
        $contact->fax_no = $request->fax_no;
        $contact->mobile_no = $request->mobile_no;
        $contact->toll_free_no = $request->toll_free_no;
        $contact->website = strtolower($request->website);

        $contact->created_by_id = $user_id;
        $contact->modified_by_id = $user_id;

        $contact->save();

        if ($contact->id) {

            $optimus = new Optimus(522588247, 282319719, 383644817);
            //image generation
            $encrypt_id = $optimus->encode($contact->id);

            $con_img = \DefaultProfileImage::create($request->name, 256, '#FFF', '#5fbeaa'); //
            \Storage::put("public/contacts/" . $encrypt_id . ".png", $con_img->encode());

            $contact = Contact::find($contact->id);
            $contact->image = "/storage/contacts/" . $encrypt_id . ".png";//$request->image;
            $contact->save();

        }

        return response()->json($contact);
        */

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $team_id = Auth::user()->currentTeam->id;
        $user_id = Auth::user()->id;

        $contact = Contact::where('team_id', '=', $team_id)
            ->where('open_id', '=', $id)
            ->first();

        $contact->emails = json_decode($contact->emails);

        return response()->json($contact);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $team_id = Auth::user()->currentTeam->id;
        $user_id = Auth::user()->id;

        $contact = Contact::where('team_id', '=', $team_id)
            ->where('open_id', '=', $id)
            ->first();

        $contact->name = ucwords(strtolower($request->name));
        $contact->email = strtolower($request->email);
        $contact->emails        = empty($request->emails) ? null : json_encode($request->emails); //
        
        $contact->phone = $request->phone;
        $contact->contact1 = ucwords(strtolower($request->contact1));
        $contact->contact2 = ucwords(strtolower($request->contact2));

        $contact->currency_id = $request->currency_id;
        $contact->payment_terms = $request->payment_terms;

        $contact->bill_address1 = ucwords(strtolower($request->bill_address1));
        $contact->bill_address2 = ucwords(strtolower($request->bill_address2));

        $contact->bill_city = ucwords(strtolower($request->bill_city));
        $contact->bill_state = ucwords(strtolower($request->bill_state));
        $contact->bill_postal_code = $request->bill_postal_code;
        $contact->bill_country_id = $request->bill_country_id;

        $contact->ship_phone = $request->ship_phone;
        $contact->ship_contact = ucwords(strtolower($request->ship_contact));
        $contact->ship_address1 = ucwords(strtolower($request->ship_address1));
        $contact->ship_address2 = ucwords(strtolower($request->ship_address2));
        $contact->ship_city = ucwords(strtolower($request->ship_city));
        $contact->ship_state = ucwords(strtolower($request->ship_state));
        $contact->ship_postal_code = $request->ship_postal_code;
        $contact->ship_country_id = $request->ship_country_id;
        $contact->instructions = $request->instructions;

        $contact->account_no = $request->account_no;
        $contact->id_no = $request->id_no;
        $contact->vat_no = $request->vat_no;
        $contact->fax_no = $request->fax_no;
        $contact->mobile_no = $request->mobile_no;
        $contact->toll_free_no = $request->toll_free_no;
        $contact->website = strtolower($request->website);

        $contact->gst_code = empty($request->gst_code) ? null : $request->gst_code;

        $contact->modified_by_id = $user_id;

        $contact->save();

        return response()->json('true');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user_id = Auth::user()->id;
        $team_id = Auth::user()->currentTeam->id;

        $contact = Contact::where('team_id', '=', $team_id)
            ->where('open_id', '=', $id)
            ->first();

        if ($contact) {


            $contact_id = $contact->id;
            $contact->modified_by_id = $user_id;   //user who deleted the contact
            $contact->save();

            Contact::find($contact_id)->delete();

            /* delete invoice & delete invoice items start */
            $invoices = UserInvoice::select('id')
                ->where('team_id', '=', $team_id)
                ->where('contact_id', '=', $contact_id)
                ->get();

            if ($invoices) {

                $invoices_update = UserInvoice::where('team_id', '=', $team_id)
                    ->where('contact_id', '=', $contact_id)
                    ->update(['modified_by_id' => $user_id]);

                $plucked = $invoices->pluck('id');
                $invoice_ids = $plucked->all();

                $UserInvoiceItem = UserInvoiceItem::where('team_id', '=', $team_id)
                    ->whereIn('invoice_num', $invoice_ids)
                    ->delete();          //update multiple row

                $UserInvoiceTax = UserInvoiceTax::where('team_id', '=', $team_id)
                    ->whereIn('invoice_num', $invoice_ids)
                    ->delete();          //update multiple row

                //find and delete transactions related to invoices
                $transactions = Transaction::where('team_id', '=', $team_id)
                    ->whereIn('inv_ref', $invoice_ids)
                    ->get();          //get multiple row

                foreach ($transactions as $txn) {

                    $gl_account = GL_Account::where('team_id', '=', $team_id)
                        ->where('open_id', '=', $txn->gl_account_id)
                        ->first();

                    if ($gl_account) {

                        if ($txn->txn_type == 'C') {

                            $gl_account->int_balance = $gl_account->int_balance + $txn->txn_amount;

                        } elseif ($txn->txn_type == 'D') {

                            $gl_account->int_balance = $gl_account->int_balance - $txn->txn_amount;

                        }

                        $gl_account->save();

                    }

                    //update transaction balances for transactions posted after this txn

                    $post_txns    =   Transaction::where('team_id','=',$team_id)
                        ->where('id','>',$txn->id)
                        ->where('gl_account_id','=',$txn->gl_account_id)
                        ->get();

                    foreach($post_txns as $txn_row)
                    {

                        if ($txn->txn_type == 'C')
                        {
                            $txn_row->balance = $txn_row->balance + $txn->txn_amount;
                            $txn_row->save();
                        }
                        elseif ($txn->txn_type == 'D')
                        {
                            $txn_row->balance = $txn_row->balance - $txn->txn_amount;
                            $txn_row->save();
                        }

                    }

                    //end of post balance update

                }

                $transactions = Transaction::where('team_id', '=', $team_id)
                    ->whereIn('inv_ref', $invoice_ids)
                    ->delete();

                //end of transactions and gl update

                $payments = UserPayment::select('id')
                    ->where('team_id', '=', $team_id)
                    ->whereIn('invoice_id', $invoice_ids)
                    ->get();          //get multiple row

                if ($payments) {

                    $plucked_pay = $payments->pluck('id');
                    $payments_ids = $plucked_pay->all();

                    //find and delete transactions related to invoices
                    $transactions = Transaction::where('team_id', '=', $team_id)
                        ->whereIn('pay_ref', $payments_ids)
                        ->get();          //get multiple row

                    foreach ($transactions as $txn) {

                        $gl_account = GL_Account::where('team_id', '=', $team_id)
                            ->where('open_id', '=', $txn->gl_account_id)
                            ->first();

                        if ($gl_account) {

                            if ($txn->txn_type == 'C') {

                                $gl_account->int_balance = $gl_account->int_balance + $txn->txn_amount;

                            } elseif ($txn->txn_type == 'D') {

                                $gl_account->int_balance = $gl_account->int_balance - $txn->txn_amount;

                            }

                            $gl_account->save();

                        }

                        //update transaction balances for transactions posted after this txn

                        $post_txns    =   Transaction::where('team_id','=',$team_id)
                            ->where('id','>',$txn->id)
                            ->where('gl_account_id','=',$txn->gl_account_id)
                            ->get();

                        foreach($post_txns as $txn_row)
                        {

                            if ($txn->txn_type == 'C')
                            {
                                $txn_row->balance = $txn_row->balance + $txn->txn_amount;
                                $txn_row->save();
                            }
                            elseif ($txn->txn_type == 'D')
                            {
                                $txn_row->balance = $txn_row->balance - $txn->txn_amount;
                                $txn_row->save();
                            }

                        }

                        //end of post balance update

                    }

                    $transactions = Transaction::where('team_id', '=', $team_id)
                        ->whereIn('pay_ref', $payments_ids)
                        ->delete();

                    //end of transactions and gl update

                    $payments = UserPayment::where('team_id', '=', $team_id)
                        ->whereIn('invoice_id', $invoice_ids)
                        ->delete();          //update multiple row

                }

                /* delete invoice & delete invoice items end */
                $invoice = UserInvoice::where('team_id', '=', $team_id)
                    ->where('contact_id', '=', $contact_id)
                    ->delete();

            }

            /* delete quotes & delete quote items start */
            $quotes = UserQuote::select('id')
                ->where('team_id', '=', $team_id)
                ->where('contact_id', '=', $contact_id)
                ->get();

            if ($quotes) {

                $plucked_quote = $quotes->pluck('id');
                $quote_ids = $plucked_quote->all();

                $UserQuoteItem = UserQuoteItem::where('team_id', '=', $team_id)
                    ->whereIn('quote_num', $quote_ids)
                    ->delete();          //update multiple row

                $UserQuoteTax = UserQuoteTax::where('team_id', '=', $team_id)
                    ->whereIn('quote_num', $quote_ids)
                    ->delete();          //update multiple row

                $quotes = UserQuote::where('team_id', '=', $team_id)
                    ->where('contact_id', '=', $contact_id)
                    ->update(['modified_by_id' => $user_id]);

                $quotes = UserQuote::where('team_id', '=', $team_id)
                    ->where('contact_id', '=', $contact_id)
                    ->delete();
            }

            /* delete credit notes, refunds & delete credit note items start */
            $credit_notes = UserCredit::select('id')
                ->where('team_id', '=', $team_id)
                ->where('contact_id', '=', $contact_id)
                ->get();

            if ($credit_notes) {

                $plucked_cn = $credit_notes->pluck('id');
                $cn_ids = $plucked_cn->all();

                $UserCreditItem = UserCreditItem::where('team_id', '=', $team_id)
                    ->whereIn('creditnote_num', $cn_ids)
                    ->delete();          //update multiple row

                $UserCreditTax = UserCreditTax::where('team_id', '=', $team_id)
                    ->whereIn('creditnote_num', $cn_ids)
                    ->delete();          //update multiple row

                $credit_notes = UserCredit::where('team_id', '=', $team_id)
                    ->where('contact_id', '=', $contact_id)
                    ->update(['modified_by_id' => $user_id]);

                $credit_notes = UserCredit::where('team_id', '=', $team_id)
                    ->where('contact_id', '=', $contact_id)
                    ->delete();


                //delete transactions related to credit notes

                //find and delete transactions related to invoices
                $transactions = Transaction::where('team_id', '=', $team_id)
                    ->whereIn('inv_ref', $cn_ids)
                    ->get();          //get multiple row

                foreach ($transactions as $txn) {

                    $gl_account = GL_Account::where('team_id', '=', $team_id)
                        ->where('open_id', '=', $txn->gl_account_id)
                        ->first();

                    if ($gl_account) {

                        if ($txn->txn_type == 'C') {

                            $gl_account->int_balance = $gl_account->int_balance + $txn->txn_amount;

                        } elseif ($txn->txn_type == 'D') {

                            $gl_account->int_balance = $gl_account->int_balance - $txn->txn_amount;

                        }

                        $gl_account->save();

                    }

                    //update transaction balances for transactions posted after this txn

                    $post_txns    =   Transaction::where('team_id','=',$team_id)
                        ->where('id','>',$txn->id)
                        ->where('gl_account_id','=',$txn->gl_account_id)
                        ->get();

                    foreach($post_txns as $txn_row)
                    {

                        if ($txn->txn_type == 'C')
                        {
                            $txn_row->balance = $txn_row->balance + $txn->txn_amount;
                            $txn_row->save();
                        }
                        elseif ($txn->txn_type == 'D')
                        {
                            $txn_row->balance = $txn_row->balance - $txn->txn_amount;
                            $txn_row->save();
                        }

                    }

                    //end of post balance update

                }

                $transactions = Transaction::where('team_id', '=', $team_id)
                    ->whereIn('inv_ref', $cn_ids)
                    ->delete();

                //end of delete transactions

            }

            /* delete associated refunds */

            $refunds = UserRefund::select('id')
                            ->where('team_id', '=', $team_id)
                            ->where('contact_id', '=', $contact_id)
                            ->get();

            if($refunds){

                $plucked        = $refunds->pluck('id');
                $refund_ids     = $plucked->all();

                //find and delete transactions related to refunds
                $transactions = Transaction::where('team_id', '=', $team_id)
                    ->whereIn('pay_ref', $refund_ids)
                    ->get();          //get multiple row

                foreach ($transactions as $txn) {

                    $gl_account = GL_Account::where('team_id', '=', $team_id)
                        ->where('open_id', '=', $txn->gl_account_id)
                        ->first();

                    if ($gl_account) {

                        if ($txn->txn_type == 'C') {

                            $gl_account->int_balance = $gl_account->int_balance + $txn->txn_amount;

                        } elseif ($txn->txn_type == 'D') {

                            $gl_account->int_balance = $gl_account->int_balance - $txn->txn_amount;

                        }

                        $gl_account->save();

                    }

                    //update transaction balances for transactions posted after this txn

                    $post_txns    =   Transaction::where('team_id','=',$team_id)
                        ->where('id','>',$txn->id)
                        ->where('gl_account_id','=',$txn->gl_account_id)
                        ->get();

                    foreach($post_txns as $txn_row)
                    {

                        if ($txn->txn_type == 'C')
                        {
                            $txn_row->balance = $txn_row->balance + $txn->txn_amount;
                            $txn_row->save();
                        }
                        elseif ($txn->txn_type == 'D')
                        {
                            $txn_row->balance = $txn_row->balance - $txn->txn_amount;
                            $txn_row->save();
                        }

                    }

                    //end of post balance update

                }

                $transactions = Transaction::where('team_id', '=', $team_id)
                    ->whereIn('pay_ref', $refund_ids)
                    ->delete();

                //end of transactions and gl update refunds

                $refunds = UserRefund::where('team_id', '=', $team_id)
                    ->where('contact_id', '=', $contact_id)
                    ->update(['modified_by_id' => $user_id]);

                $refunds = UserRefund::where('team_id', '=', $team_id)
                    ->where('contact_id', '=', $contact_id)
                    ->delete();

            }


        }
        return response()->json('true');
    }

    public function listContacts(Request $request)
    {
        //need to fix this - we can't return ID to the frontend
        $q = $request->input('q');

        $team_id = Auth::user()->currentTeam->id;
        //$user_id = Auth::user()->id;

        if ($q) {
            $contacts = Contact::where('team_id', '=', $team_id)
                ->where('name', 'like', '%' . $q . '%')
                //->orWhere('email','like','%'.$q.'%')
                ->get();
        } else {
            $contacts = Contact::where('team_id', '=', $team_id)
                ->get();
            //::select(['id', 'name', 'email','image','bill_address1','bill_address2'])
            //->get()

        }

        //return Contact::find(1);
        if ($q) {
            $returnJson["total_count"] = count($contacts);
            $returnJson["incomplete_results"] = false;
            $returnJson["items"] = $contacts;

            return response()->json($returnJson);
        } else
            return response()->json($contacts);
    }
}