<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\UserCredit;

use App\Contact;
use App\Transaction;
use App\GL_Account;
use App\Team;
use App\Country;
use App\UserPayment;
use App\Localization;
use App\Tax_Rate;
use App\Team_Detail;
use App\BootTour;

use Crypt;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Optimus\Optimus;

class CreditNoteController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('teamSubscribed');

        //new Optimus(522588247, 282319719, 383644817);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $team_id      = Auth::user()->currentTeam->id;
        $user_id      = Auth::user()->id;

        $gl_accounts  = GL_Account::where('team_id','=',$team_id)
            ->orderBy('type','name')
            ->get();

        $tax_rates = Tax_Rate::where('team_id', '=', $team_id)
            ->get();

        $boot_tour = BootTour::where('user_id', $user_id)
            ->first();

        $command = 'CreditNote';

        return view('user.credit-notes.create-creditnote',
            compact('gl_accounts','boot_tour','tax_rates','command'))
            ->withEncryptedCsrfToken(Crypt::encrypt(csrf_token()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $team_id    = Auth::user()->currentTeam->id;
        $user_id    = Auth::user()->id;

        $open_id = $id; //system will pass open invoice number and not the invoice id

        $invoice = UserCredit::where('team_id','=',$team_id)
            ->with('contactTab','statusTab','creditItemsTab','creditTaxTab')
            ->where('open_id','=',$open_id)
            ->first();          //returns single row

        if($invoice){

            $invitation_key = $invoice->encrypted_id;

            $gateway_id = 0;
            $public_key = null;

            $inv_country    = Country::where('id','=',$invoice->currency_id)->first();

            $team               = Team::find($team_id);
            $command            = 'CreditNote';
            $team_details       = Team_Detail::where('team_id','=',$team_id)->first();

            return view('user.credit-notes.view-creditnote',
                compact('invoice','invitation_key','gateway_id',
                    'public_key','inv_country',
                    'team','command','team_details'));

        }
        else{
            return redirect()->action('UserInvoiceController@index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
