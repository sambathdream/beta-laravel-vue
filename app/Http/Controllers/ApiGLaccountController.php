<?php

namespace App\Http\Controllers;

use App\GL_Account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiGLaccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $team_id = Auth::user()->currentTeam->id;

        $request = request();

        if (request()->has('sort')) {
            list($sortCol, $sortDir) = explode('|', request()->sort);
            $query = GL_Account::orderBy($sortCol, $sortDir)
                ->with('glTypeTab')
                ->where('team_id', '=', $team_id);
        } else {
            $query = GL_Account::orderBy('id', 'asc')
                ->with('glTypeTab')
                ->where('team_id', '=', $team_id);
        }

        if ($request->exists('filter')) {
            $query->where(function ($q) use ($request) {
                $value = "%{$request->filter}%";
                $q->where('name', 'like', $value)
                    ->orWhere('description', 'like', $value);
            });
        }

        $perPage = request()->has('per_page') ? (int)request()->per_page : null;

// The headers 'Access-Control-Allow-Origin' and 'Access-Control-Allow-Methods'
// are to allow you to call this from any domain (see CORS for more info).
// This is for local testing only. You should not do this in production server,
// unless you know what it means.
        return response()->json($query->paginate($perPage));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $team_id = Auth::user()->currentTeam->id;

        $last_gl = GL_Account::where('team_id','=',$team_id)
            ->orderBy('id','desc')
            ->first();          //returns last latest row

        if($last_gl) {
            $new_gl_num = $last_gl->open_id + 1;
        }
        else {
            $new_gl_num = 1;
        }

        $team_id                  = Auth::user()->currentTeam->id;
        $user_id                  = Auth::user()->id;

        $gl_account               = new GL_Account;
        $gl_account->team_id      = $team_id;

        $gl_account->open_id      = $new_gl_num;

        $gl_account->type         = $request->type;
        $gl_account->name         = $request->name;

        $gl_account->int_balance  = $request->int_balance;
        $gl_account->ext_balance  = $request->ext_balance;

        $gl_account->description  = $request->description;

        $gl_account->created_by_id   = $user_id;
        $gl_account->modified_by_id  = $user_id;

        $gl_account->save();


        return response()->json('true');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        ////
        $team_id  = Auth::user()->currentTeam->id;
        $user_id  = Auth::user()->id;

        $gl_item = GL_Account::where('team_id','=',$team_id)
                ->where('open_id','=',$id)
                ->first();

        return response()->json($gl_item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $team_id  = Auth::user()->currentTeam->id;
        $user_id  = Auth::user()->id;

        $gl_account       = GL_Account::where('team_id','=',$team_id)
            ->where('open_id','=',$id)
            ->first();

        $gl_account->type     = $request->type;
        $gl_account->name         = $request->name;

        if($request->int_balance) {
            $gl_account->int_balance = $request->int_balance;
        }
        if($request->ext_balance) {
            $gl_account->ext_balance = $request->ext_balance;
        }

        if($request->description) {
            $gl_account->description = $request->description;
        }

        $gl_account->modified_by_id  = $user_id;

        $gl_account->save();


        return response()->json('true');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //I think we should not allow user to delete GL Account
        /*
        $user_id    = Auth::user()->id;
        $team_id    = Auth::user()->currentTeam->id;

        $gl_account  = GL_Account::where('team_id','=',$team_id)
                        ->where('open_id','=',$id)
                        ->first();

        $gl_account->modified_by_id  =   $user_id;   //user who deleted the contact
        $gl_account->save();

        GL_Account::find($gl_account->id)->delete();
        */

        return response()->json('true');
    }
}
