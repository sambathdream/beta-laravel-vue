<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use Charts;
//use GeoIP;

use App\UserInvoice;
use App\UserQuote;
use App\UserPayment;
use App\Country;
use App\Localization;
use App\BootTour;

use Carbon\Carbon;

class ApiDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $team_id      = Auth::user()->currentTeam->id;
        $user_id      = Auth::user()->id;

        $last2lastmonth         = Carbon::parse('this month')->subMonths(2)->format('F Y');
        $last2lastmonth_start   = Carbon::parse('first day of this month')->subMonths(2)->toDateString();
        $last2lastmonth_end     = Carbon::parse('last day of this month')->subMonths(2)->toDateString();
        $sales_last2last_month  = UserInvoice::where('team_id','=',$team_id)
            ->where('post_date','>=',$last2lastmonth_start)
            ->where('post_date','<=',$last2lastmonth_end)
            ->sum('total_amount');
        $quote_last2last_month  = UserQuote::where('team_id','=',$team_id)
            ->where('post_date','>=',$last2lastmonth_start)
            ->where('post_date','<=',$last2lastmonth_end)
            ->sum('total_amount');
        $pay_last2last_month    = UserPayment::where('team_id','=',$team_id)
            ->where('payment_date','>=',$last2lastmonth_start)
            ->where('payment_date','<=',$last2lastmonth_end)
            ->sum('amount');

        $lastmonth              = Carbon::parse('last month')->format('F Y');
        $lastmonth_start        = Carbon::parse('first day of last month')->toDateString();
        $lastmonth_end          = Carbon::parse('last day of last month')->toDateString();
        $sales_last_month       = UserInvoice::where('team_id','=',$team_id)
            ->where('post_date','>=',$lastmonth_start)
            ->where('post_date','<=',$lastmonth_end)
            ->sum('total_amount');
        $quote_last_month       = UserQuote::where('team_id','=',$team_id)
            ->where('post_date','>=',$lastmonth_start)
            ->where('post_date','<=',$lastmonth_end)
            ->sum('total_amount');
        $pay_last_month         = UserPayment::where('team_id','=',$team_id)
            ->where('payment_date','>=',$lastmonth_start)
            ->where('payment_date','<=',$lastmonth_end)
            ->sum('amount');

        $currentmonth           = Carbon::parse('this month')->format('F Y');
        $thismonth_start        = Carbon::parse('first day of this month')->toDateString();
        $thismonth_end          = Carbon::parse('last day of this month')->toDateString();
        $sales_this_month       = UserInvoice::where('team_id','=',$team_id)
            ->where('post_date','>=',$thismonth_start)
            ->where('post_date','<=',$thismonth_end)
            ->sum('total_amount');
        $quote_this_month       = UserQuote::where('team_id','=',$team_id)
            ->where('post_date','>=',$thismonth_start)
            ->where('post_date','<=',$thismonth_end)
            ->sum('total_amount');
        $pay_this_month         = UserPayment::where('team_id','=',$team_id)
            ->where('payment_date','>=',$thismonth_start)
            ->where('payment_date','<=',$thismonth_end)
            ->sum('amount');

        $chart1                 = Charts::create('bar', 'highcharts')
            ->title(' ')
            ->colors(['#5FBEAA'])
            ->elementLabel('Sales / Month')
            ->labels([$last2lastmonth, $lastmonth, $currentmonth])
            ->values([$sales_last2last_month,$sales_last_month,$sales_this_month])
            ->Responsive(false)->Dimensions(0, 354);

        $lastweek_sunday        = Carbon::parse('previous sunday')->toDateString();
        $lastweek_monday        = Carbon::parse('previous sunday')->subDays(7)->toDateString();

        $sales_last_week        = UserInvoice::where('team_id','=',$team_id)
            ->where('post_date','>=',$lastweek_monday)
            ->where('post_date','<=',$lastweek_sunday)
            ->sum('total_amount');

        $thisweek_monday        = Carbon::now()->startOfWeek();
        $thisweek_sunday        = Carbon::now()->endOfWeek();
        $sales_this_week        = UserInvoice::where('team_id','=',$team_id)
            ->where('post_date','>=',$thisweek_monday)
            ->where('post_date','<=',$thisweek_sunday)
            ->sum('total_amount');

        $target_this_week       = 1000; //make this dynamic???
        $sales_in_perct         = ($sales_this_week * 100) / $target_this_week;
        if($sales_in_perct > 100) { $sales_in_perct = 100; }
        $chart3                 = Charts::create('percentage', 'justgage')
            ->title('Revenue / Week')
            ->elementLabel('% of the target')
            ->values([$sales_in_perct,0,100])
            ->responsive(false)
            ->height(284)
            ->width(0);

        /*
        $chart0 = Charts::create('donut', 'highcharts')
            ->title(' ')
            ->labels(['First', 'Second', 'Third'])
            ->values([5,10,20])
            ->responsive(true);
        */

        $chart2                 = Charts::multi('line', 'highcharts')
            ->title(' ')
            ->responsive(true)
            ->colors(['#36404A', '#5FBEAA', '#5D9CEC'])
            //->elementLabel('Total Views')
            //->x_axis_title('Year')
            //->y_axis_title('Number of Units')
            ->labels([$last2lastmonth, $lastmonth, $currentmonth])
            ->dataset('Quotations', [$quote_last2last_month,$quote_last_month,$quote_this_month])
            ->dataset('Invoices', [$sales_last2last_month,$sales_last_month,$sales_this_month])
            ->dataset('Payments', [$pay_last2last_month,$pay_last_month,$pay_this_month]);

        $invoice_total_count   = UserInvoice::where('team_id','=',$team_id)
            //->where('balance','>',0)
            ->count('id');

        $invoice_unpaid_count   = UserInvoice::where('team_id','=',$team_id)
            ->where('balance','>',0)
            ->count('id');

        $invoice_paid_count     = UserInvoice::where('team_id','=',$team_id)
            ->where('balance','<=',0)
            ->count('id');

        $invoice_unpaid         =  Charts::create('progressbar', 'progressbarjs')
            ->title(' ')
            ->colors(['#FF5733'])
            ->values([$invoice_unpaid_count,0,$invoice_total_count])
            ->responsive(true)
            ->height(20)
            ->width(0);

        $invoice_paid           =  Charts::create('progressbar', 'progressbarjs')
            ->title(' ')
            ->colors(['#DAF7A6'])
            ->values([$invoice_paid_count,0,$invoice_total_count])
            ->responsive(true)
            ->height(20)
            ->width(0);


        $recent_invoices       = UserInvoice::orderBy('id','DESC')
            ->where('team_id','=',$team_id)
            ->with('statusTab','contactTab')
            ->take(5)
            ->get();


        $localization = Localization::where('team_id', '=', $team_id)->first();

        $first_time = $localization->first_time;

        $total_revenue          = UserInvoice::where('team_id','=',$team_id)
            ->sum('total_amount_local');

        $now                    = \Carbon\Carbon::now()->format('Y-m-d');

        $total_sales            = UserInvoice::where('team_id','=',$team_id)
            ->where('created_at','>=',$now)
            ->count('id');
        $total_payments         = UserPayment::where('team_id','=',$team_id)
            ->where('payment_date','>=',$now)
            ->count('id');

        $inv_country  = Country::where('id','=',$localization->currency_id)->first();

        if($first_time){
            $countries = Country::all();
        }
        else{
            $countries = $inv_country;
        }

        $boot_tour = BootTour::where('user_id', $user_id)
            ->first();

        /*
         *
        return view('user.dashboard.dashboard',compact('chart0','chart1','chart2','chart3',
            'sales_this_week','target_this_week','sales_last_week','sales_last_month',
            'sales_in_perct','recent_invoices','countries','first_time', 'boot_tour',
            'total_revenue','total_sales','total_payments','inv_country',
            'invoice_unpaid','invoice_paid','invoice_total_count','invoice_paid_count','invoice_unpaid_count'));

        */

        $response = [
            'sales_this_week' => $sales_this_week,
            'target_this_week' => $target_this_week,
            'sales_last_week' => $sales_last_week,
            'sales_last_month' => $sales_last_month,

            'sales_in_perct' => $sales_in_perct,
            //'recent_invoices' => $recent_invoices,
            'countries' => $countries,
            'first_time' => $first_time,
            //'boot_tour',
            'total_revenue' => $total_revenue,
            'total_sales' => $total_sales,
            'total_payments' => $total_payments,
            'inv_country' => $inv_country,
            //'invoice_unpaid' => $invoice_unpaid,
            //'invoice_paid' => $invoice_paid,
            'invoice_total_count' => $invoice_total_count,
            'invoice_paid_count' => $invoice_paid_count,
            'invoice_unpaid_count' => $invoice_unpaid_count,

        ];

        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
