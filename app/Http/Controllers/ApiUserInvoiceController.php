<?php

namespace App\Http\Controllers;

use App\Team;
use App\Team_Detail;
use App\Contact;
use App\UserInvoice;
use App\UserInvoiceItem;
use App\UserInvoiceTax;
use App\UserRecInvoice;

use App\Transaction;
use App\GL_Account;
use App\Localization;
use App\Country;

use App\UserGateway;
use App\StripeBilling;
use App\UserPayment;
use App\Accounting;

use App\Traits\stripeBillingTrait;
use App\Traits\invoiceTrait;
use App\Traits\contactTrait;
use App\Traits\creditNoteTrait;

use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Mail\Message;
use Jenssegers\Optimus\Optimus;

use Symfony\Component\HttpFoundation\Response;

class ApiUserInvoiceController extends Controller
{

    use stripeBillingTrait, invoiceTrait, contactTrait, creditNoteTrait;

    public function __construct()
    {

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
        //
        $team_id = Auth::user()->currentTeam->id;

        $request = request();

        if (request()->has('sort')) {
            list($sortCol, $sortDir) = explode('|', request()->sort);
            $query = UserInvoice::orderBy($sortCol, $sortDir)
                ->with('statusTab','contactTab')
                ->where('team_id','=',$team_id) //
                ->where('status_id','!=',5);   //5: recurring
        } else {
            $query = UserInvoice::orderBy('id', 'asc')
                ->with('statusTab','contactTab')
                ->where('team_id','=',$team_id)
                ->where('status_id','!=',5);   //5: recurring
        }

        //temp comment - please remove this once below fields confirmed

        if ($request->exists('filter')) {
            $query->where(function($q) use($request) {
                $value = "%{$request->filter}%";
                $q->where('name', 'like', $value); //->where('text', 'like', $value)
            });
        }


        $perPage = request()->has('per_page') ? (int) request()->per_page : null;

        return response()->json($query->paginate($perPage));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function listInvoices(Request $request)
    {
        //need to fix this - we can't return ID to the frontend
        $q = $request->input('q');

        $team_id = Auth::user()->currentTeam->id;
        //$user_id = Auth::user()->id;

        if ($q) {
            $invoices = UserInvoice::where('team_id','=',$team_id)
                                    ->where('status_id','!=',5)
                                    ->with('statusTab','contactTab')
                                    ->get();

        } else {
            $invoices = UserInvoice::where('team_id','=',$team_id)
                                    ->where('status_id','!=',5)
                                    ->with('statusTab','contactTab')
                                    ->get();
        }

        //return Contact::find(1);
        if ($q) {

            $returnJson["total_count"] = count($invoices);
            $returnJson["incomplete_results"] = false;
            $returnJson["items"] = $invoices;

            return response()->json($returnJson);
        }
        else {

            return response()->json($invoices);

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $invoice_flag    = $this->createInvoice($request);

        if($invoice_flag == false)
        {
            return response()->json(['error' => 'Error'], 404);
        }

        return response()->json($invoice_flag);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $team_id    = Auth::user()->currentTeam->id;
        $user_id    = Auth::user()->id;

        $open_id    = $id; //system will pass open invoice number and not the invoice id

        $invoice = UserInvoice::where('team_id','=',$team_id)
            ->with('contactTab','statusTab','invItemsTab','invTaxTab')
            ->where('open_id','=',$open_id)
            ->first();          //returns single row

        if($invoice){

            //$optimus        = new Optimus(522588247, 282319719, 383644817);
            //$invitation_key = $optimus->encode($invoice->id);

            //$invoice->encrypted_id = $invitation_key;
            //$invoice->save();

            //$invoice->invItemsTab

            //start of line item related taxes
            foreach($invoice->invItemsTab as $key => $invItem)
            {
                $invoice->invItemsTab[$key]->tax_items = [];
                $invoice->invItemsTab[$key]->ded_items = [];
            }
            //end of line item related taxes

            $invoice->dedTab        = [];
            $invoice->taxTab        = [];

            foreach ($invoice->invTaxTab as $tax_tab) {
                if ($tax_tab->type == 'DED') {

                    if($tax_tab->item_num){
                        foreach($invoice->invItemsTab as $key => $invItem)
                        {
                            if($invItem->id == $tax_tab->item_num) //tax_items
                            {
                                $invoice->invItemsTab[$key]->ded_items =
                                    array_prepend($invoice->invItemsTab[$key]->ded_items, $tax_tab);
                            }
                        }
                    }
                    else {
                        $invoice->dedTab = array_prepend($invoice->dedTab, $tax_tab);
                    }
                }
                elseif ($tax_tab->type == 'TAX'){

                    if($tax_tab->item_num){
                        foreach($invoice->invItemsTab as $key => $invItem)
                        {
                            if($invItem->id == $tax_tab->item_num) //tax_items
                            {
                                $invoice->invItemsTab[$key]->tax_items =
                                    array_prepend($invoice->invItemsTab[$key]->tax_items, $tax_tab);
                            }
                        }
                    }
                    else {
                        $invoice->taxTab = array_prepend($invoice->taxTab, $tax_tab);
                    }

                }
            }

            return response()->json($invoice);

        }
        else{

            return response()->json([
                'error' => 'Record not found',
            ], 404);
        }

    }

    public function returnPubId($id)
    {

        $team_id    = Auth::user()->currentTeam->id;
        $user_id    = Auth::user()->id;

        $open_id    = $id; //system will pass open invoice number and not the invoice id

        $invoice = UserInvoice::where('team_id','=',$team_id)
            ->where('open_id','=',$open_id)
            ->first();          //returns single row

        if($invoice) {

            $optimus        = new Optimus(522588247, 282319719, 383644817);
            $invitation_key = $optimus->encode($invoice->id);

            return response()->json($invitation_key);

        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //return response()->json($request);

        $invoice_flag    = $this->updateInvoice($request);

        return response()->json($invoice_flag);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $team_id      = Auth::user()->currentTeam->id;
        $user_id      = Auth::user()->id;

        $response = $this->destroyInvoice($id,$team_id,$user_id);

        return response()->json($response);

    }

    public function destroyUsingContact($id)
    {

        /*
        $team_id    = Auth::user()->currentTeam->id;
        $user_id    = Auth::user()->id;

        $contact_id = $id;

        $invoice    = UserInvoice::where('team_id','=',$team_id)
                    ->where('contact_id','=',$contact_id)
                    ->update(['modified_by_id' => $user_id]);

        UserInvoiceItem::where('team_id','=',$team_id)
                    ->whereIn('invoice_num','=',$invoice->id)
                    ->delete();          //update multiple row

        UserInvoice::where('team_id','=',$team_id)
                    ->where('contact_id','=',$contact_id)
                    ->delete();

        return response()->json('true');
        */
    }

    /**
     *
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sendInvoice(Request $request, $id)
    {

        $team_name  = Auth::user()->currentTeam->name;
        $team_id    = Auth::user()->currentTeam->id;
        $user_id    = Auth::user()->id;

        $from_email = Auth::user()->email;
        $from_name  = Auth::user()->name;

        if($team_id == 1)
        {
            //return response()->json($request);
        }

        return $this->sendEmail($request, $id, $team_name, $team_id, $user_id, $from_email, $from_name);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function previousInvManID()
    {
        //
        $team_id      = Auth::user()->currentTeam->id;
        $user_id      = Auth::user()->id;

        $last_invoice = UserInvoice::where('team_id','=',$team_id)
            ->orderBy('open_id','desc')
            ->first();          //returns last latest row

        if($last_invoice) {

            $old_open_id  = $last_invoice->open_id;

            $numbers = preg_replace('/[^0-9]/', '', $old_open_id);
            $letters = preg_replace('/[^a-zA-Z]/', '', $old_open_id);

            $new_open_id = $letters . ( $numbers + 1 );

        }
        else {
            $new_open_id = 1;
            $old_open_id = 0;
        }

        $response = [
            'old_open_id' => $old_open_id,
            'new_open_id' => $new_open_id
        ];

        return response()->json($response);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function validateInvID($id)
    {
        //
        $team_id      = Auth::user()->currentTeam->id;
        $user_id      = Auth::user()->id;

        $found_invoice = UserInvoice::where('team_id','=',$team_id)
            ->where('open_id','=',$id)
            ->first();          //returns last latest row

        if($found_invoice) {

            return response()->json(false);    //means duplicate invoice exists

        }
        else {

            return response()->json(true);
        }

    }

    public function stripeBillConvert(Request $request, $id)
    {

        $team_id        = Auth::user()->currentTeam->id;
        $user_id        = Auth::user()->id;

        $invoice        = UserInvoice::where('team_id','=',$team_id)
            ->where('open_id','=',$id)
            ->first();          //returns last latest row

        $invoice->status_id         = 1;

        $invoice->post_date         = Carbon::now()->toDateTimeString();
        $invoice->due_date          = Carbon::now()->toDateTimeString();

        $invoice_items              = UserInvoiceItem::where('team_id','=',$team_id)
            ->where('invoice_num','=',$invoice->id)
            ->get();

        //return response()->json($invoice_items);

        $invoice->sub_total         = 0;

        foreach($invoice_items as $item){

            $invoice->sub_total = $invoice->sub_total + $item->item_total;

        }

        //return response()->json($invoice);

        $invoice->discount_rate1        = 0;
        $invoice->discount_amount       = 0;
        $invoice->tax_amount            = 0;

        $invoice->total_amount          = $invoice->sub_total;

        //$invoice->amount_paid           = 0;

        $invoice->balance               = $invoice->sub_total - $invoice->amount_paid;

        $invoice->total_amount_local    = $invoice->currency_rate * $invoice->total_amount;
        //$invoice->amount_paid_local     = $invoice->currency_rate * $invoice->amount_paid;
        $invoice->balance_local         = ( $invoice->currency_rate * $invoice->balance ) - $invoice->amount_paid_local;

        $invoice->save();

        //return response()->json('true');

        //post financial txn
        //debit gl
        if ($invoice->debit_gl and $invoice->status_id != 5) {

            $last_txn = Transaction::where('team_id','=',$team_id)
                ->orderBy('id','desc')
                ->first();          //returns last latest row

            if($last_txn) {
                $last_txn_number = $last_txn->open_id + 1;
            }
            else {
                $last_txn_number = 1;
            }

            $transaction                    = new Transaction;
            $transaction->team_id           = $team_id;

            $transaction->open_id           = $last_txn_number;

            $transaction->category          = 47;
            $transaction->txn_type          = "D";//$request->type;
            $transaction->txn_date          = $invoice->post_date;

            $transaction->gl_account_id     = $invoice->debit_gl;
            $transaction->txn_amount        = ( $invoice->total_amount * $invoice->currency_rate );

            $transaction->description       = 'Invoice Sale';
            //$transaction->flag            = 1;
            $transaction->created_by_id     = 0;
            $transaction->modified_by_id    = 0;

            $transaction->save();

            $gl_account                     = GL_Account::where('team_id','=',$team_id)
                ->where('open_id','=',$invoice->debit_gl)
                ->first();

            if($gl_account) {
                $gl_account->int_balance    = $gl_account->int_balance + $transaction->txn_amount;
            }

            $gl_account->save();

            $transaction->balance           = $gl_account->int_balance;

            //$transaction->txn_ref = ;
            $transaction->inv_ref           = $invoice->id;
            //$transaction->pay_ref = ;

            $transaction->save();
        }


        return response()->json(true);

    }

    public function stripeHookPost(Request $request, $id)
    {

        //return response()->json($this->showSearchResults($request));

        $optimus     = new Optimus(914614073, 864868105, 2138572997);
        $dec_team_id = $optimus->decode($id);
        $team        = Team::where('id', '=', $dec_team_id)->first();
        $team_id     = $team->id;
        $team_name   = $team->name;
        $user_id     = 0;

        if(!$team){

            return new Response('Webhook Failed', 500); //return error response

        }

        $this->verifyStripeSign($request, $team_id);

        $method = 'stripe'.studly_case(str_replace('.', '_', $request->type));

        /*

        if($method == 'stripeInvoiceCreated' and $team_id != 5)
        {

            $stripe_cust_id                 = $request->data['object']['customer'];

            ///return response()->json($stripe_cust_id);

            if($stripe_cust_id) {

                $contact = Contact::where('team_id', '=', $team_id)->where('id_no', '=', $stripe_cust_id)->first();

                //return response()->json($contact);

                if(!$contact){

                    //call the API to retrieve customer
                    //if no contact then create one

                    //$stripe_cust_id = 'cus_AgTxrcDHITsiMl';

                    $stripe_cust_request = $this->retrieveStripeCustomer($stripe_cust_id,$team_id);

                    if($stripe_cust_request) {

                        $contact_id_no      = $stripe_cust_request['data']['object']['id'];
                        $contact_name_email = $stripe_cust_request['data']['object']['email'];


                        $contact = $this->createStripeContact($contact_id_no, $contact_name_email, $team_id, $user_id);

                        $contact = $this->updateStripeContact($stripe_cust_request, $contact, $team_id, $user_id);

                        //return $contact;

                    }else{
                        return new Response('Webhook Failed', 500);
                    }


                }

                //if invoice amount is zero then don't create an invoice, just return webhook
                if($request->data['object']['amount_due'] == 0){
                    return new Response('Webhook Handled', 200);
                }

                $invoice        = UserInvoice::where('team_id', '=', $team_id)
                    ->where('status_id', '=', 1) //6: retrieve any open bill 1: new create
                    ->where('contact_id','=',$contact->id)
                    ->where('stripe_id','=',$request->data['object']['id'])
                    //->orderBy('id', 'desc')
                    ->first();          //return open bill

                if (!$invoice) {    //if none found then create new

                    $invoice = $this->createStripeInvoice($request, $contact, $team_id, $user_id);

                    //return response()->json($invoice);
                    return new Response('Webhook Handled', 200);
                }               //else continue updating existing invoice line items

                //$invoice = $this->updateStripeInvoice($request, $invoice, $team_id, $user_id);

            }

        }

        */


        if($method == 'stripeInvoicePaymentSucceeded')
        {

            $stripe_cust_id                 = $request->data['object']['customer'];

            if($stripe_cust_id) {

                $contact = Contact::where('team_id', '=', $team_id)->where('id_no', '=', $stripe_cust_id)->first();

                if(!$contact){

                    //call the API to retrieve customer
                    //if no contact then create one
                    //$stripe_cust_id = 'cus_AgTxrcDHITsiMl';
                    $stripe_cust_request = $this->retrieveStripeCustomer($stripe_cust_id,$team_id);

                    if($stripe_cust_request) {

                        $contact_request = $this->returnStripeContactInfo($stripe_cust_request);
                        $contact         = $this->createContact($contact_request, $team_id, $user_id);

                    }else{
                        return new Response('Webhook Failed', 500);
                    }

                }

                //if invoice amount is zero then don't create an invoice, just return webhook
                if($request->data['object']['amount_due'] == 0){
                    return new Response('Webhook Handled', 200);
                }

                $invoice        = UserInvoice::where('team_id', '=', $team_id)
                    ->where('status_id', '=', 1) //6: retrieve any open bill 1: new create
                    ->where('contact_id','=',$contact->id)
                    ->where('stripe_id','=',$request->data['object']['id'])
                    ->first();          //return open bill

                if (!$invoice) {    //if none found then create new

                    $invoice = $this->createStripeInvoiceWithPayment($request, $contact, $team_id, $user_id);

                    $stripe_billing = StripeBilling::where('team_id', '=', $team_id)->first();

                    if ($stripe_billing) {

                        if ($stripe_billing->auto_email) {

                            $email_request = new \Illuminate\Http\Request();

                            if($stripe_billing->from_email){

                                $from_email = $stripe_billing->from_email;

                            }else{

                                return;

                            }

                            $from_name      = $team_name;

                            $to_email       = $contact->email;
                            $cc_email       = empty($stripe_billing->cc_email) ? null : $stripe_billing->cc_email;

                            $to_emails[0]['id']   = $to_email;

                            $cc_emails[0]['id']   = $cc_email;


                            $email_request->replace(['to_emails' => $to_emails, 'cc_emails' => $cc_emails]);

                            //return response()->json($email_request);

                            $optimus        = new Optimus(522588247, 282319719, 383644817);
                            $invoice_key    = $optimus->encode($invoice->id);

                            $email_flag     = $this->sendEmail($email_request, $invoice_key, $team_name,
                                $team_id, $user_id, $from_email, $from_name);


                            return new Response('Webhook Handled', 200);

                        }

                    }

                    return new Response('Webhook Handled', 200);

                }

            }

        }

        if($method == 'stripeChargeSucceeded')
        {

            $stripe_cust_id                 = $request->data['object']['customer'];

            if($stripe_cust_id) {

                $contact = Contact::where('team_id', '=', $team_id)->where('id_no', '=', $stripe_cust_id)->first();

                if(!$contact){

                    //call the API to retrieve customer
                    //if no contact then create one
                    //$stripe_cust_id = 'cus_AgTxrcDHITsiMl';
                    $stripe_cust_request = $this->retrieveStripeCustomer($stripe_cust_id,$team_id);

                    if($stripe_cust_request) {

                        $contact_request = $this->returnStripeContactInfo($stripe_cust_request);
                        $contact         = $this->createContact($contact_request, $team_id, $user_id);

                    }else{
                        return new Response('Webhook Failed', 500);
                    }

                }

                $invoice = null;

                if($request->data['object']['invoice']) {

                    /*
                    $invoice = UserInvoice::where('team_id', '=', $team_id)
                                    ->where('status_id', '=', 1)//6: retrieve any open bill 1: new create
                                    ->where('contact_id', '=', $contact->id)
                                    ->where('stripe_id', '=', $request->data['object']['invoice'])
                                    ->first();          //return open bill
                    */
                    //if($invoice){

                        return new Response('Webhook Handled', 200);

                    //}

                }

                if (!$invoice) {    //if none found then create new

                    $invoice = $this->createStripeInvoiceForCharge($request, $contact, $team_id, $user_id);

                    if($invoice) {

                        $charge = $this->chargeStripeSuccess($request, $invoice, $team_id, $user_id);

                        $stripe_billing = StripeBilling::where('team_id', '=', $team_id)->first();

                        if ($stripe_billing) {

                            if ($stripe_billing->auto_email) {

                                $email_request = new \Illuminate\Http\Request();

                                if($stripe_billing->from_email){

                                    $from_email = $stripe_billing->from_email;

                                }else{

                                    return;

                                }

                                $from_name      = $team_name;

                                $to_email       = $contact->email;
                                $cc_email       = empty($stripe_billing->cc_email) ? null : $stripe_billing->cc_email;

                                $to_emails[0]['id']   = $to_email;

                                $cc_emails[0]['id']   = $cc_email;


                                $email_request->replace(['to_emails' => $to_emails, 'cc_emails' => $cc_emails]);

                                //return response()->json($email_request);

                                $optimus        = new Optimus(522588247, 282319719, 383644817);
                                $invoice_key    = $optimus->encode($invoice->id);

                                $email_flag     = $this->sendEmail($email_request, $invoice_key, $team_name,
                                    $team_id, $user_id, $from_email, $from_name);


                                return new Response('Webhook Handled', 200);

                            }

                        }

                    }

                }               //else continue updating existing invoice line items

            }
            else{
                return new Response('Webhook Failed', 500);
            }
        }

        if($method == 'stripeChargeRefunded')
        {

            $stripe_cust_id                 = $request->data['object']['customer'];

            if($stripe_cust_id) {

                $contact = Contact::where('team_id', '=', $team_id)->where('id_no', '=', $stripe_cust_id)->first();

                if(!$contact){

                    //call the API to retrieve customer
                    //if no contact then create one
                    //$stripe_cust_id = 'cus_AgTxrcDHITsiMl';
                    $stripe_cust_request = $this->retrieveStripeCustomer($stripe_cust_id,$team_id);

                    if($stripe_cust_request) {

                        $contact_request = $this->returnStripeContactInfo($stripe_cust_request);
                        $contact         = $this->createContact($contact_request, $team_id, $user_id);

                    }else{
                        return new Response('Webhook Failed', 500);
                    }

                }

                if($contact) {

                    $invoice_no = $this->chargeStripeRefundInvoice($request, $contact, $team_id, $user_id);

                    $stripe_billing = StripeBilling::where('team_id', '=', $team_id)->first();

                    if ($stripe_billing) {

                        if ($stripe_billing->auto_email) {

                            $email_request = new \Illuminate\Http\Request();

                            if($stripe_billing->from_email){

                                $from_email = $stripe_billing->from_email;

                            }else{

                                return;

                            }

                            $from_name      = $team_name;

                            $to_email       = $contact->email;
                            $cc_email       = empty($stripe_billing->cc_email) ? null : $stripe_billing->cc_email;

                            $to_emails[0]['id']   = $to_email;

                            $cc_emails[0]['id']   = $cc_email;


                            $email_request->replace(['to_emails' => $to_emails, 'cc_emails' => $cc_emails]);

                            //return response()->json($email_request);

                            $optimus        = new Optimus(1568794001, 671972209, 2122508544); //for credit notes
                            $invoice_key    = $optimus->encode($invoice_no);

                            $email_flag     = $this->sendCreditNoteEmail($email_request, $invoice_key, $team_name,
                                $team_id, $user_id, $from_email, $from_name);


                            return new Response('Webhook Handled', 200);

                        }

                    }

                }

                return new Response('Webhook Handled', 200);


            }
            else{
                return new Response('Webhook Failed', 500);
            }
        }

        /*

        if($method == 'stripeInvoicePaymentFailed')
        {

            $stripe_cust_id                 = $request->data['object']['customer'];

            if($stripe_cust_id) {

                $contact = Contact::where('team_id', '=', $team_id)->where('id_no', '=', $stripe_cust_id)->first();

                //return response()->json($contact);

                if(!$contact){
                    return new Response('Webhook Handled', 200);
                }

                $invoice = null;

                if($request->data['object']['id']) {

                    $invoice = UserInvoice::where('team_id', '=', $team_id)
                        ->where('status_id', '=', 1)//6: retrieve any open bill 1: new create
                        ->where('contact_id', '=', $contact->id)
                        ->where('stripe_id', '=', $request->data['object']['id'])
                        ->first();          //return open bill

                    if($invoice) {

                        $response = $this->destroyInvoice($invoice->open_id,$team_id,$user_id);

                        return new Response('Webhook Handled', 200);

                    }

                }

            }
            else{
                return new Response('Webhook Failed', 500);
            }
        }

        */

        return new Response('Webhook Handled', 200);

    }


}
