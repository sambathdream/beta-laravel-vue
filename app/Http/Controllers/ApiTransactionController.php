<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\GL_Account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ApiTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //
        $team_id = Auth::user()->currentTeam->id;

        //$query = Transaction::with('gl_account')->where('team_id', '=', $team_id)->first();
        //return response()->json($query);

        $request = request();

        if (request()->has('sort')) {
            list($sortCol, $sortDir) = explode('|', request()->sort);
            $query = Transaction::orderBy($sortCol, $sortDir)
                ->with('glAccountTab','txnCatTab','txnTypeTab')
                ->where('team_id', '=', $team_id);
        } else {
            $query = Transaction::orderBy('id', 'asc')
                ->with('glAccountTab','txnCatTab','txnTypeTab')
                ->where('team_id', '=', $team_id);
        }

        /*
        $collection = array();
        foreach($query as $row){
            $row->category = StaticArray::$txn_category->get($row->category);
            $collection[] = $row;
        }
        */

        if ($request->exists('filter')) {
            $query->where(function ($q) use ($request) {
                $value = "%{$request->filter}%";
                $q->where('category', 'like', $value)
                    ->orWhere('txn_type', 'like', $value);
            });
        }

        $perPage = request()->has('per_page') ? (int)request()->per_page : null;

// The headers 'Access-Control-Allow-Origin' and 'Access-Control-Allow-Methods'
// are to allow you to call this from any domain (see CORS for more info).
// This is for local testing only. You should not do this in production server,
// unless you know what it means.
        return response()->json($query->paginate($perPage));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $team_id = Auth::user()->currentTeam->id;
        $user_id = Auth::user()->id;

        $last_txn = Transaction::where('team_id','=',$team_id)
            ->orderBy('id','desc')
            ->first();          //returns last latest row

        if($last_txn) {
            $last_txn_number = $last_txn->open_id + 1;
        }
        else {
            $last_txn_number = 1;
        }

        //debit gl
        $debit_txn = 0; //used for reference creation (txn_ref field)

        if ($request->debit_gl) {

            $transaction = new Transaction;
            $transaction->team_id = $team_id;

            $transaction->open_id = $last_txn_number;

            $transaction->category = $request->category;
            $transaction->txn_type = "D";//$request->type;
            $transaction->txn_date = $request->txn_date;

            $transaction->gl_account_id = $request->debit_gl;
            $transaction->txn_amount = $request->txn_amount;
            //$transaction->balance      = $request->balance;

            $transaction->description = $request->description;
            $transaction->flag = 1;
            $transaction->created_by_id = $user_id;
            $transaction->modified_by_id = $user_id;

            $transaction->save();

            $gl_account  = GL_Account::where('team_id','=',$team_id)
                            ->where('open_id','=',$request->debit_gl)
                            ->first();

            if($request->txn_amount) {
                $gl_account->int_balance = $gl_account->int_balance + $request->txn_amount;
            }

            $gl_account->save();

            $transaction->balance = $gl_account->int_balance;
            $transaction->txn_ref = 0;

            $transaction->save();

            $debit_txn = $transaction->id;

        }

        //credit gl
        $credit_txn = 0;

        if ($request->credit_gl) {

            $transaction = new Transaction;
            $transaction->team_id = $team_id;

            $transaction->open_id = $last_txn_number + 1;

            $transaction->category = $request->category;
            $transaction->txn_type = "C"; //$request->type;    //D or C ??
            $transaction->txn_date = $request->txn_date;

            $transaction->gl_account_id = $request->credit_gl;
            $transaction->txn_amount = $request->txn_amount;
            //$transaction->balance      = $request->balance;

            $transaction->description = $request->description;
            $transaction->flag = 1;
            $transaction->created_by_id = $user_id;
            $transaction->modified_by_id = $user_id;

            $transaction->save();

            $gl_account  = GL_Account::where('team_id','=',$team_id)
                ->where('open_id','=',$request->credit_gl)
                ->first();

            if($request->txn_amount) {
                $gl_account->int_balance = $gl_account->int_balance - $request->txn_amount;
            }

            $gl_account->save();

            $transaction->balance = $gl_account->int_balance;

            $transaction->txn_ref = $debit_txn;

            $transaction->save();

            $credit_txn = $transaction->id;

        }

        if($debit_txn > 0 and $credit_txn > 0) {

            $debit_txn_row = Transaction::where('team_id','=',$team_id)
                                        ->where('id','=',$debit_txn)
                                        ->first();

            $debit_txn_row->txn_ref = $credit_txn;
            $debit_txn_row->save();
        }

        return response()->json('true');

    }
    
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user_id                       =   Auth::user()->id;
        $team_id                       =   Auth::user()->currentTeam->id;

        $transaction                   =   Transaction::where('team_id','=',$team_id)
                                            ->where('open_id','=',$id)
                                            ->first();

        $txn_id                        =   $transaction->id;
        $transaction->modified_by_id   =   $user_id;   //user who deleted the contact
        $transaction->save();

        if($transaction) {

            $gl_account                =   GL_Account::where('team_id', '=', $team_id)
                                            ->where('open_id', '=', $transaction->gl_account_id)
                                            ->first();

            if ($gl_account) {

                if ($transaction->txn_type == 'C') {

                    $gl_account->int_balance = $gl_account->int_balance + $transaction->txn_amount;

                } elseif ($transaction->txn_type == 'D') {

                    $gl_account->int_balance = $gl_account->int_balance - $transaction->txn_amount;

                }

                $gl_account->save();

            }

            //update transaction balances for transactions posted after this txn

            $post_txns                   =   Transaction::where('team_id','=',$team_id)
                                                ->where('id','>',$txn_id)
                                                ->where('gl_account_id','=',$transaction->gl_account_id)
                                                ->get();

            foreach($post_txns as $txn)
            {

                if ($transaction->txn_type == 'C')
                {
                    $txn->balance = $txn->balance + $transaction->txn_amount;
                    $txn->save();
                }
                elseif ($transaction->txn_type == 'D')
                {
                    $txn->balance = $txn->balance - $transaction->txn_amount;
                    $txn->save();
                }

            }

            //end of post balance update


        }

        Transaction::find($txn_id)->delete();

    }
}
