<?php

namespace App\Http\Controllers;

use App\Contact;
use App\UserInvoice;
use App\UserCredit;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


use Carbon\Carbon;
use Datatables;
use Html;
use Excel;


class ApiReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //


    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function displayReport(Request $request)
    {
        //
        $team_id = Auth::user()->currentTeam->id;

        $carbon_from_date   = Carbon::now()->subDays(29)->toDateTimeString();
        $carbon_to_date     = Carbon::now()->toDateTimeString();

        $from_date          = empty($request->from_date) ? $carbon_from_date : $request->from_date;
        $to_date            = empty($request->to_date) ? $carbon_to_date : $request->to_date;

        if($request->type == '1') {

            $contacts = Contact::select(\DB::raw('contacts.name, contacts.email, 
                SUM(user_invoices.total_amount_local) As total_amount, 
                SUM(user_invoices.amount_paid_local) As amount_paid, 
                SUM(user_invoices.balance_local) As balance'))
                ->leftJoin('user_invoices', 'user_invoices.contact_id', '=', 'contacts.id')
                ->where('user_invoices.team_id', $team_id)
                ->where('contacts.team_id', $team_id)
                ->where('user_invoices.post_date', '>=', $from_date)
                ->where('user_invoices.post_date', '<=', $to_date)
                ->groupBy('contacts.id')
                ->get();

        }

        if($request->type == '2') {

            /*
            $invoices = UserInvoice::where('team_id','=',$team_id)
                ->where('post_date', '>=', $from_date)
                ->where('post_date', '<=', $to_date)
                ->with('contactTab','paymentsTab','statusTab')
                ->get();
            */

            $invoices = UserInvoice::select(\DB::raw('contacts.name as name, contacts.email,
                user_invoices.open_id as open_id,
                user_invoices.post_date as post_date, 
                user_invoice_status.text as status,
                user_invoices.total_amount_local As total_amount_local, 
                user_invoices.amount_paid_local As amount_paid_local, 
                user_invoices.balance_local As balance_local, 
                user_payments.payment_date as payment_date'))
                ->join('contacts', 'user_invoices.contact_id', '=', 'contacts.id')
                ->join('user_invoice_status', 'user_invoices.status_id', '=', 'user_invoice_status.id')
                ->leftJoin('user_payments', 'user_invoices.id', '=', 'user_payments.invoice_id')
                ->where('user_invoices.team_id', $team_id)
                ->where('contacts.team_id', $team_id)
                ->where('user_invoices.post_date', '>=', $from_date)
                ->where('user_invoices.post_date', '<=', $to_date)
                ->groupBy('user_invoices.id')
                ->get();

            //return response()->json($invoices);

        }

        if($request->type == '3') {

            $payments = UserInvoice::select(\DB::raw('
                contacts.name as name, 
                user_invoices.open_id as open_id,
                user_invoices.post_date as post_date,
                user_payments.payment_date as payment_date,
                user_invoices.total_amount_local As total_amount_local,
                user_payments.amount_local as payment_amount
                '))
                ->join('contacts', 'user_invoices.contact_id', '=', 'contacts.id')
                ->rightJoin('user_payments', 'user_invoices.id', '=', 'user_payments.invoice_id')
                ->where('user_invoices.team_id', $team_id)
                ->where('contacts.team_id', $team_id)
                ->where('user_invoices.post_date', '>=', $from_date)
                ->where('user_invoices.post_date', '<=', $to_date)
                ->groupBy('user_invoices.id')
                ->get();

            //return response()->json($payments);

        }

        if($request->type == '4') {

            $products = UserInvoice::select(\DB::raw('
                contacts.name as name, 
                user_invoices.open_id,
                user_invoices.post_date, 
                user_invoice_status.text as status,
                user_invoices.total_amount_local, 
                user_invoice_items.item_id,
                user_invoice_items.item_name,
                user_invoice_items.item_price,
                user_invoice_items.item_qty,
                user_invoice_items.item_total
                '))
                ->join('contacts', 'user_invoices.contact_id', '=', 'contacts.id')
                ->join('user_invoice_status', 'user_invoices.status_id', '=', 'user_invoice_status.id')
                ->rightJoin('user_invoice_items', 'user_invoices.id', '=', 'user_invoice_items.invoice_num')
                ->where('user_invoices.team_id', $team_id)
                ->where('contacts.team_id', $team_id)
                ->where('user_invoices.post_date', '>=', $from_date)
                ->where('user_invoices.post_date', '<=', $to_date)
                //->groupBy('user_invoices.id')
                ->get();

            //return response()->json($invoices);

        }

        if($request->type == '5') {

            $taxes = UserInvoice::select(\DB::raw('
                contacts.name as name, 
                user_invoices.open_id,
                user_invoices.post_date, 
                user_invoice_status.text as status,
                user_invoices.total_amount_local, 
                user_invoice_taxes.tax_name,
                user_invoice_taxes.tax_rate,
                user_invoice_taxes.tax_amount
                '))
                ->join('contacts', 'user_invoices.contact_id', '=', 'contacts.id')
                ->join('user_invoice_status', 'user_invoices.status_id', '=', 'user_invoice_status.id')
                ->rightJoin('user_invoice_taxes', 'user_invoices.id', '=', 'user_invoice_taxes.invoice_num')
                ->where('user_invoices.team_id', $team_id)
                ->where('contacts.team_id', $team_id)
                ->where('user_invoices.post_date', '>=', $from_date)
                ->where('user_invoices.post_date', '<=', $to_date)
                //->groupBy('user_invoices.id')
                ->get();

        }

        if($request->type == '6') {

            //$team_id = 107;

            $transactions = Contact::select(\DB::raw('
                contacts.open_id as id,
                contacts.name as name, 
                user_invoices.open_id,
                user_invoices.post_date, 
                user_invoice_status.text as status,
                user_invoices.total_amount_local
                '))
                ->join('user_invoices', 'user_invoices.contact_id', '=', 'contacts.id')
                ->join('user_invoice_status', 'user_invoices.status_id', '=', 'user_invoice_status.id')
                //->leftJoin('user_credits', 'contacts.id', '=', 'user_credits.contact_id')
                //->rightJoin('user_invoice_taxes', 'user_invoices.id', '=', 'user_invoice_taxes.invoice_num')
                ->where('user_invoices.team_id', $team_id)
                ->where('contacts.team_id', $team_id)
                ->where('user_invoices.post_date', '>=', $from_date)
                ->where('user_invoices.post_date', '<=', $to_date)
                //->groupBy('contacts.open_id')
                ->get();

            return response()->json($transactions);
            /*

            SELECT c.open_id, c.name, ui.open_id, ui.post_date, ui.total_amount_local, uis.text
            FROM `contacts` as c JOIN `user_invoices` as ui
            on c.id = ui.contact_id join user_invoice_status as uis
            on ui.status_id = uis.id WHERE c.team_id = 5

            $contacts = Contact::select(\DB::raw('contacts.name, contacts.email,
                SUM(user_invoices.total_amount_local) As total_amount,
                SUM(user_invoices.amount_paid_local) As amount_paid,
                SUM(user_invoices.balance_local) As balance'))
                ->leftJoin('user_invoices', 'user_invoices.contact_id', '=', 'contacts.id')
                ->where('user_invoices.team_id', $team_id)
                ->where('contacts.team_id', $team_id)
                ->where('user_invoices.post_date', '>=', $from_date)
                ->where('user_invoices.post_date', '<=', $to_date)
                ->groupBy('contacts.id')
                ->get();
            */

        }


        if($request->type == '7') {

            $team_id = 107;

            $refunds = UserCredit::select(\DB::raw('
                contacts.name as name, 
                user_credits.open_id as open_id,
                user_credits.post_date as post_date,
                user_refunds.payment_date as refund_date,
                user_credits.total_amount_local As total_amount_local,
                user_refunds.amount_local as refund_amount
                '))
                ->join('contacts', 'user_credits.contact_id', '=', 'contacts.id')
                ->rightJoin('user_refunds', 'user_credits.id', '=', 'user_refunds.creditnote_num')
                ->where('user_credits.team_id', $team_id)
                ->where('contacts.team_id', $team_id)
                ->where('user_credits.post_date', '>=', $from_date)
                ->where('user_credits.post_date', '<=', $to_date)
                ->groupBy('user_credits.id')
                ->get();

            //return response()->json($refunds);

        }

        if($request->type == '1' and $request->cmd == 'display') {

            return Datatables::of($contacts)
                //->editColumn('title', '{!! str_limit($title, 60) !!}')
                ->editColumn('email', function ($model) {
                    return Html::mailto($model->email, $model->email);
                })
                ->make(true);
        }

        if($request->type == '2' and $request->cmd == 'display') {

            return Datatables::of($invoices)
                //->editColumn('title', '{!! str_limit($title, 60) !!}')
                ->editColumn('open_id', function ($model) {
                    return Html::link('/invoices/' . $model->open_id, $model->open_id);
                })
                ->make(true);
        }

        if($request->type == '3' and $request->cmd == 'display') {

            return Datatables::of($payments)
                //->editColumn('title', '{!! str_limit($title, 60) !!}')
                ->editColumn('open_id', function ($model) {
                    return Html::link('/invoices/' . $model->open_id, $model->open_id);
                })
                ->make(true);
        }

        if($request->type == '4' and $request->cmd == 'display') {

            return Datatables::of($products)
                //->editColumn('title', '{!! str_limit($title, 60) !!}')
                ->editColumn('open_id', function ($model) {
                    return Html::link('/invoices/' . $model->open_id, $model->open_id);
                })
                ->make(true);
        }

        if($request->type == '5' and $request->cmd == 'display') {

            return Datatables::of($taxes)
                //->editColumn('title', '{!! str_limit($title, 60) !!}')
                ->editColumn('open_id', function ($model) {
                    return Html::link('/invoices/' . $model->open_id, $model->open_id);
                })
                ->make(true);
        }

        // 6 missing

        if($request->type == '7' and $request->cmd == 'display') {

            return Datatables::of($refunds)
                //->editColumn('title', '{!! str_limit($title, 60) !!}')
                ->editColumn('open_id', function ($model) {
                    return Html::link('/creditnotes/' . $model->open_id, $model->open_id);
                })
                ->make(true);
        }

        if($request->type == '1' and $request->cmd == 'download') {

            $contacts = $contacts->toArray();

            $timestamp = 'Customer_' . $team_id . strval(Carbon::now()->timestamp);

            //return $timestamp;

            Excel::create($timestamp, function($excel) use ($contacts, $timestamp) {


                $excel->sheet($timestamp, function($sheet) use ($contacts) {

                    $sheet->fromArray($contacts);


                });

            })->store('csv');

            $response = [
                'report_id' => $timestamp . '.csv'
            ];

            return response()->json($response);

        }

        if($request->type == '2' and $request->cmd == 'download') {

            $invoices = $invoices->toArray();

            $timestamp = 'Invoice_' . $team_id . strval(Carbon::now()->timestamp);

            //return $timestamp;

            Excel::create($timestamp, function($excel) use ($invoices, $timestamp) {


                $excel->sheet($timestamp, function($sheet) use ($invoices) {

                    $sheet->fromArray($invoices);


                });

            })->store('csv');

            $response = [
                'report_id' => $timestamp . '.csv'
            ];

            return response()->json($response);

        }

        if($request->type == '3' and $request->cmd == 'download') {

            $payments = $payments->toArray();

            $timestamp = 'Payments_' . $team_id . strval(Carbon::now()->timestamp);

            Excel::create($timestamp, function($excel) use ($payments, $timestamp) {


                $excel->sheet($timestamp, function($sheet) use ($payments) {

                    $sheet->fromArray($payments);

                });

            })->store('csv');

            $response = [
                'report_id' => $timestamp . '.csv'
            ];

            return response()->json($response);

        }

        if($request->type == '4' and $request->cmd == 'download') {

            $products = $products->toArray();

            $timestamp = 'Inventory_' . $team_id . strval(Carbon::now()->timestamp);

            //return $timestamp;

            Excel::create($timestamp, function($excel) use ($products, $timestamp) {


                $excel->sheet($timestamp, function($sheet) use ($products) {

                    $sheet->fromArray($products);


                });

            })->store('csv');

            $response = [
                'report_id' => $timestamp . '.csv'
            ];

            return response()->json($response);

        }


        if($request->type == '5' and $request->cmd == 'download') {

            $products = $taxes->toArray();

            $timestamp = 'Taxes_' . $team_id . strval(Carbon::now()->timestamp);

            Excel::create($timestamp, function($excel) use ($taxes, $timestamp) {


                $excel->sheet($timestamp, function($sheet) use ($taxes) {

                    $sheet->fromArray($taxes);

                });

            })->store('csv');

            $response = [
                'report_id' => $timestamp . '.csv'
            ];

            return response()->json($response);

        }

        //6 missing

        if($request->type == '7' and $request->cmd == 'download') {

            $refunds = $refunds->toArray();

            $timestamp = 'Refunds_' . $team_id . strval(Carbon::now()->timestamp);

            Excel::create($timestamp, function($excel) use ($refunds, $timestamp) {


                $excel->sheet($timestamp, function($sheet) use ($refunds) {

                    $sheet->fromArray($refunds);

                });

            })->store('csv');

            $response = [
                'report_id' => $timestamp . '.csv'
            ];

            return response()->json($response);

        }

    }

}
