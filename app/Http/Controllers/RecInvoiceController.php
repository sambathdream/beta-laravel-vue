<?php

namespace App\Http\Controllers;

use App\UserInvoice;
//use App\UserInvoiceItem;
use App\Contact;
use App\Transaction;
use App\GL_Account;
use App\Team;
use App\UserGateway;
use App\Country;
use App\UserPayment;
use App\Localization;
use App\Tax_Rate;
use App\BootTour;
use App\Team_Detail;

use Crypt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Optimus\Optimus;

class RecInvoiceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('teamSubscribed');

        //new Optimus(522588247, 282319719, 383644817);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $team_id      = Auth::user()->currentTeam->id;
        $user_id      = Auth::user()->id;

        $total_revenue = UserInvoice::where('team_id','=',$team_id)
            ->sum('total_amount_local');

        $now = \Carbon\Carbon::now()->format('Y-m-d');

//        return $now;
        $total_sales = UserInvoice::where('team_id','=',$team_id)
            ->where('created_at','>=',$now)
            ->count('id');

        $total_payments = UserPayment::where('team_id','=',$team_id)
            ->where('payment_date','>=',$now)
            ->count('id');

        $localization = Localization::where('team_id','=',$team_id)->first();

        $inv_country  = Country::where('id','=',$localization->currency_id)->first();

        $boot_tour = BootTour::where('user_id', $user_id)
            ->first();

        return view('user.invoices.rec_invoices',compact('total_revenue','total_sales',
            'total_payments','inv_country','boot_tour'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $team_id      = Auth::user()->currentTeam->id;
        $user_id      = Auth::user()->id;

        $gl_accounts  = GL_Account::where('team_id','=',$team_id)
            ->orderBy('type','name')
            ->get();

        $tax_rates = Tax_Rate::where('team_id', '=', $team_id)
            ->get();

        $boot_tour = BootTour::where('user_id', $user_id)
            ->first();

        $command = 'RecInvoice';

        return view('user.invoices.create_rec_invoice',
            compact('gl_accounts','boot_tour','tax_rates','command'))
            ->withEncryptedCsrfToken(Crypt::encrypt(csrf_token()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
