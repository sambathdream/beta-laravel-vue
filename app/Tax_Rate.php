<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tax_Rate extends Model
{
    //
    protected $table = 'tax_rates';

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function glAccountTab()
    {
        return $this->belongsTo('App\GL_Account','gl_account_id','open_id');
    }

}
