<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserQuote extends Model
{
    ////
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function contactTab()
    {
        return $this->belongsTo('App\Contact','contact_id','id'); //hit the primary key in the txn_categories table
    }

    public function statusTab()
    {
        return $this->belongsTo('App\UserInvoiceStatus','status_id','id'); //hit the primary key in the txn_categories table
    }

    public function quoteItemsTab()
    {
        return $this->hasMany('App\UserQuoteItem','quote_num', 'id');
    }

    public function quoteTaxTab()
    {
        return $this->hasMany('App\UserQuoteTax','quote_num', 'id');
    }
    
}
