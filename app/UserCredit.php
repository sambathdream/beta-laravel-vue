<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserCredit extends Model
{
    //
    use SoftDeletes;

    protected $table = 'user_credits';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function contactTab()
    {
        return $this->belongsTo('App\Contact','contact_id','id'); //hit the primary key in the txn_categories table
    }

    public function statusTab()
    {
        return $this->belongsTo('App\UserInvoiceStatus','status_id','id'); //hit the primary key in the txn_categories table
    }

    public function creditItemsTab()
    {
        return $this->hasMany('App\UserCreditItem','creditnote_num', 'id');
    }

    public function creditTaxTab()
    {
        return $this->hasMany('App\UserCreditTax','creditnote_num', 'id');
    }

}
