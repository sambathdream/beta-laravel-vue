<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPayment extends Model
{
    //
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function contactTab()
    {
        return $this->belongsTo('App\Contact','contact_id','id'); //hit the primary key in the txn_categories table
    }

    public function invoiceTab()
    {
        return $this->belongsTo('App\UserInvoice','invoice_id','id');
    }

}
