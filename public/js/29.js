webpackJsonp([29],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/project/rating.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "ProjectRating",
  props: {
    title: {
      default: function _default() {
        return "Visual Accuracy";
      }
    },
    maxRating: {
      default: function _default() {
        return 5;
      }
    },
    value: {
      default: function _default() {
        return {
          score: 0,
          suggestion: ""
        };
      }
    },
    disabled: {
      default: function _default() {
        return false;
      }
    }
  },
  data: function data() {
    return {
      add_suggestion: false,
      ratingValue: {
        score: 0,
        suggestion: ""
      }
    };
  },

  methods: {
    emitChange: function emitChange() {
      this.$emit("input", this.ratingValue);
      this.$emit("change", this.ratingValue);
    },
    addSuggestion: function addSuggestion(e) {
      this.ratingValue.suggestion = e.target.value;
      this.emitChange();
    },
    setRating: function setRating(v) {
      if (this.disabled) {
        return true;
      }
      this.ratingValue.score = v;
    },
    selected: function selected(v) {
      if (this.disabled) {
        return true;
      }
      this.setRating(v);
      this.emitChange();
    }
  },
  computed: {
    starBlocks: function starBlocks() {
      return Array.from({ length: this.maxRating }).map(function (_, i) {
        return i + 1;
      });
    },
    ratingFillColor: function ratingFillColor() {
      if (this.ratingValue.score === 1) return "#F83636";
      if (this.ratingValue.score === 2) return "#FF8A0D";
      if (this.ratingValue.score === 3) return "#FFC21F";
      if (this.ratingValue.score === 4) return "#73DF23";
      if (this.ratingValue.score === 5) return "#02C506";
    },
    ratingItemStyle: function ratingItemStyle() {
      var baseStyle = {};
      if (!this.disabled) {
        baseStyle.cursor = "pointer";
      }
      if (this.ratingValue.score) {
        return _extends({}, baseStyle, {
          color: "white",
          background: this.ratingFillColor
        });
      }
      return _extends({}, baseStyle);
    }
  },
  mounted: function mounted() {
    this.ratingValue = _extends({}, this.value);
  },

  watch: {
    value: function value(newVal, oldVal) {
      this.ratingValue = _extends({}, newVal);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/tester/project-fillup-accepted.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__("./node_modules/vue/dist/vue.common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_components_components_project_rating_vue__ = __webpack_require__("./resources/assets/components/components/project/rating.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_components_components_project_rating_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_components_components_project_rating_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  name: "project-fillup-accepted",
  components: {
    starRatings: __WEBPACK_IMPORTED_MODULE_1_components_components_project_rating_vue___default.a
  },
  data: function data() {
    return {
      cnt1: 3,
      cnt2: 2,
      cnt3: 1,
      seen: false
    };
  },

  methods: {
    removeField: function removeField(event) {
      event.target.closest(".issue-link").remove();
    },
    addField: function addField(event) {
      this.cnt1++;
    },
    addexplanation: function addexplanation(event) {
      console.log(event.target);
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d8562cf\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/project-fillup-accepted.vue":
/***/ (function(module, exports, __webpack_require__) {

var escape = __webpack_require__("./node_modules/css-loader/lib/url/escape.js");
exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.custom-container[data-v-0d8562cf] {\n  max-width: 100%;\n}\n.custom-container .text-green[data-v-0d8562cf] {\n    color: #2cac3d;\n}\n.custom-container .text-red[data-v-0d8562cf] {\n    color: #e63423;\n}\n.custom-container .text-bold[data-v-0d8562cf] {\n    font-family: \"BrandonTextBold\" !important;\n}\n.custom-container .text-medium[data-v-0d8562cf] {\n    font-family: \"BrandonTextMedium\";\n}\n.custom-container .test-proj-fillup-page[data-v-0d8562cf] {\n    width: 100%;\n}\n.custom-container .test-proj-fillup-page .white-box[data-v-0d8562cf] {\n      background-color: #fff;\n      -webkit-box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n              box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n      border-radius: 4px;\n      padding: 20px;\n      margin-bottom: 20px;\n      font-size: 17px;\n      line-height: 22px;\n      color: #606368;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title[data-v-0d8562cf] {\n        font-size: 20px;\n        font-family: \"UniNeueBold\";\n        border-bottom: 1px solid #dadada;\n        color: #2cac3d;\n        padding-bottom: 10px;\n        margin-bottom: 10px;\n        width: auto;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title .badge-wrap[data-v-0d8562cf] {\n          border: 1px solid #2cac3d;\n          background-color: #2cac3d;\n          color: #fff;\n          font-size: 16px;\n          right: -5px;\n          position: absolute;\n          border-top-left-radius: 16px;\n          border-bottom-left-radius: 16px;\n          padding: 2px 12px;\n          top: -5px;\n          font-family: \"BrandonTextRegular\";\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status[data-v-0d8562cf] {\n        padding: 0;\n        padding-top: 8px;\n        overflow: hidden;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status br[data-v-0d8562cf] {\n          display: none;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status:first-child .proj-progress[data-v-0d8562cf]::before {\n          width: calc(50% - 30px);\n          left: 15px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status:last-child .proj-progress[data-v-0d8562cf]::after {\n          width: calc(50% - 30px);\n          right: 15px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status .proj-progress[data-v-0d8562cf] {\n          position: relative;\n          margin-bottom: 5px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status .proj-progress i[data-v-0d8562cf] {\n            color: rgba(175, 177, 179, 0.5);\n            font-size: 22px;\n            line-height: 24px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status .proj-progress[data-v-0d8562cf]::before {\n            content: \"\";\n            position: absolute;\n            height: 3px;\n            width: calc(50% - 5px);\n            background-color: #ececec;\n            left: -10px;\n            top: 11px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status .proj-progress[data-v-0d8562cf]::after {\n            content: \"\";\n            position: absolute;\n            height: 3px;\n            width: calc(50% - 5px);\n            background-color: #ececec;\n            right: -10px;\n            top: 11px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.active[data-v-0d8562cf] {\n        color: #2cac3d;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.active .proj-progress i[data-v-0d8562cf] {\n          color: #2cac3d;\n          font-size: 34px;\n          line-height: 24px;\n          position: relative;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.active .proj-progress i[data-v-0d8562cf]::after {\n            height: 15px;\n            width: 15px;\n            background-color: #2cac3d;\n            content: \"\";\n            border-radius: 50%;\n            position: absolute;\n            left: 7px;\n            top: 4px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.active .proj-progress[data-v-0d8562cf]::before {\n          background-color: #2cac3d;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.done-step[data-v-0d8562cf] {\n        color: #2cac3d;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.done-step .proj-progress i[data-v-0d8562cf] {\n          color: #2cac3d;\n          font-size: 34px;\n          line-height: 24px;\n          position: relative;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.done-step .proj-progress i[data-v-0d8562cf]::after {\n            height: 15px;\n            width: 15px;\n            background-color: #2cac3d;\n            content: \"\";\n            border-radius: 50%;\n            position: absolute;\n            left: 7px;\n            top: 4px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.done-step .proj-progress[data-v-0d8562cf]::before {\n          background-color: #2cac3d;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.done-step .proj-progress[data-v-0d8562cf]::after {\n          background-color: #2cac3d;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-desc[data-v-0d8562cf] {\n        letter-spacing: 0.01rem;\n        margin-bottom: 5px;\n}\n.custom-container .test-proj-fillup-page .white-box .notes-list[data-v-0d8562cf] {\n        padding-left: 15px;\n        margin-bottom: 0px;\n}\n.custom-container .test-proj-fillup-page .white-box .notes-list li[data-v-0d8562cf] {\n          position: relative;\n}\n.custom-container .test-proj-fillup-page .white-box .notes-list li[data-v-0d8562cf]::before {\n            content: \"-\";\n            position: absolute;\n            left: -15px;\n            top: -1px;\n            color: #606368;\n            font-size: 20px;\n}\n.custom-container .test-proj-fillup-page .white-box .view-more-link[data-v-0d8562cf] {\n        font-size: 15px;\n        font-family: \"BrandonTextMedium\";\n        color: #2cac3d;\n        text-decoration: underline !important;\n        text-align: right;\n}\n.custom-container .test-proj-fillup-page .white-box .app-title[data-v-0d8562cf] {\n        min-width: 110px;\n        display: inline-block;\n}\n.custom-container .test-proj-fillup-page .white-box .test-device-list span[data-v-0d8562cf] {\n        margin-right: 15px;\n        margin-top: 10px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-issues-tab .line-seprator[data-v-0d8562cf] {\n        margin: 0 0 20px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-issues-tab .issue-block .issue-type[data-v-0d8562cf] {\n        margin-bottom: 5px;\n        font-size: 18px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-issues-tab .issue-block .form-group .form-control[data-v-0d8562cf] {\n        margin-bottom: 20px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-issues-tab .issue-block .form-group .remove-icon[data-v-0d8562cf] {\n        color: #606368;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-issues-tab .issue-block .form-group .remove-icon i[data-v-0d8562cf] {\n          font-size: 34px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-attachment-tab .add-files-btn[data-v-0d8562cf] {\n        width: 160px;\n        float: right;\n        margin-top: 0;\n        margin-bottom: 20px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-attachment-tab .add-files-btn input[data-v-0d8562cf] {\n          opacity: 0;\n          position: absolute;\n          top: 0;\n          right: 0;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-attachment-tab .attachment-table-wrap .attachment-table[data-v-0d8562cf] {\n        border-radius: 6px;\n        margin-bottom: 0;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-attachment-tab .attachment-table-wrap .attachment-table tr td[data-v-0d8562cf]:first-child {\n          min-width: 60px;\n          max-width: 60px;\n          width: 60px;\n          text-align: center;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-attachment-tab .attachment-table-wrap .attachment-table tr td[data-v-0d8562cf]:last-child {\n          min-width: 130px;\n          max-width: 130px;\n          width: 130px;\n          display: table-cell;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-attachment-tab .attachment-table-wrap .attachment-table tr td[data-v-0d8562cf]:nth-child(3) {\n          min-width: 160px;\n          max-width: 160px;\n          width: 160px;\n          display: table-cell;\n          text-align: center;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-attachment-tab .attachment-table-wrap .attachment-table tr td a[data-v-0d8562cf] {\n          margin-right: 10px;\n          color: #1f1f1f;\n          font-size: 20px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .total-score-txt[data-v-0d8562cf] {\n        font-size: 25px;\n        font-family: \"UniNeueBold\";\n        color: #363e48;\n        text-align: center;\n        width: 100%;\n        margin-bottom: 25px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .total-score-txt .effective-badge[data-v-0d8562cf] {\n          font-family: \"BrandonTextRegular\";\n          color: #fff;\n          font-size: 12px;\n          background-color: #4be05e;\n          border: 1px solid #2cac3d;\n          border-radius: 16px;\n          padding: 1px 8px;\n          vertical-align: middle;\n          margin-bottom: 5px;\n          display: inline-block;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion[data-v-0d8562cf] {\n        border: none;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion[data-v-0d8562cf]:hover {\n          -webkit-box-shadow: none;\n                  box-shadow: none;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header[data-v-0d8562cf] {\n          padding: 0 !important;\n          border-bottom: none;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header[data-v-0d8562cf]:hover {\n            outline: none !important;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a.collapsed[data-v-0d8562cf] {\n            border: 2px solid #118921;\n            background-color: #2cac3d;\n            color: #fff;\n            border-radius: 6px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a.collapsed[data-v-0d8562cf]:before {\n              background-image: url(" + escape(__webpack_require__("./resources/assets/assets/img/right-arrow.png")) + ");\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a.collapsed span[data-v-0d8562cf] {\n              color: #fff;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a.collapsed img[data-v-0d8562cf] {\n              display: inline-block;\n              vertical-align: initial;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a[data-v-0d8562cf] {\n            padding: 10px 20px !important;\n            padding-left: 35px !important;\n            border-radius: 0px;\n            border-top-right-radius: 6px;\n            border-top-left-radius: 6px;\n            text-align: left;\n            color: #2cac3d;\n            font-family: \"UniNeueHeavy\";\n            font-size: 20px;\n            text-align: left;\n            text-transform: uppercase;\n            background-color: #f2f2f2;\n            border: none !important;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a[data-v-0d8562cf]:before {\n              background-image: url(" + escape(__webpack_require__("./resources/assets/assets/img/down-arrow.png")) + ");\n              content: \"\";\n              position: absolute;\n              left: 15px;\n              top: 20px;\n              height: 14px;\n              width: 14px;\n              background-size: 100%;\n              background-position: center;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a span[data-v-0d8562cf] {\n              float: right;\n              font-family: \"BrandonTextBold\";\n              font-size: 20px;\n              text-transform: none;\n              color: #2f2f2f;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-body[data-v-0d8562cf] {\n          padding: 0 10px 10px;\n          background-color: #f2f2f2;\n          border-bottom-left-radius: 6px;\n          border-bottom-right-radius: 6px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap[data-v-0d8562cf] {\n            background-color: #fff;\n            border: 1px solid #dadada;\n            padding: 15px;\n            border-radius: 6px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap h4[data-v-0d8562cf] {\n              font-size: 18px;\n              color: #2cac3d;\n              font-family: \"BrandonTextBold\";\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap .detail-block[data-v-0d8562cf] {\n              font-family: \"BrandonTextMedium\";\n              font-size: 14px;\n              line-height: 16px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap .detail-block span[data-v-0d8562cf] {\n                width: calc(100% / 3 - 5px);\n                display: inline-block;\n                vertical-align: top;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap .explanation-wrap textarea[data-v-0d8562cf] {\n              resize: none;\n              padding: 0px 10px;\n              font-size: 17px;\n              color: #9c9c9c;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap .explanation-wrap textarea[data-v-0d8562cf]:hover, .custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap .explanation-wrap textarea[data-v-0d8562cf]:focus {\n                outline: none !important;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap .ratings-block[data-v-0d8562cf] {\n              margin-top: 10px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap .ratings-block img[data-v-0d8562cf] {\n                width: 100%;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap .green-step-btn-wrap .green-step-btn[data-v-0d8562cf] {\n              margin: 0;\n              margin-top: 30px;\n              font-size: 12px;\n              line-height: 14px;\n              text-transform: uppercase;\n              padding: 4px 8px;\n              cursor: pointer;\n}\n.custom-container .green-step-btn[data-v-0d8562cf] {\n    width: 100%;\n    max-width: 180px;\n    padding: 7px 10px;\n    border: 2px solid #118921;\n    background-color: #2cac3d;\n    font-size: 14px;\n    font-family: \"BrandonTextMedium\";\n    letter-spacing: 0.05rem;\n    color: #fff;\n    border-radius: 20px;\n    display: inline-block;\n    text-align: center;\n    margin: 10px 15px 30px 0px;\n}\n.custom-container .green-step-btn[data-v-0d8562cf]:hover {\n      background-color: #158f25;\n      color: #fff;\n      outline: none !important;\n}\n@media screen and (max-width: 1281px) {\n.custom-container .test-proj-fillup-page .white-box[data-v-0d8562cf] {\n    font-size: 16px;\n}\n.custom-container .test-proj-fillup-page .green-step-btn[data-v-0d8562cf] {\n    font-size: 13px;\n}\n}\n@media screen and (max-width: 1200px) {\n.custom-container .test-proj-fillup-page .green-step-btn[data-v-0d8562cf] {\n    font-size: 12px;\n    letter-spacing: 0;\n}\n.custom-container .test-proj-fillup-page .white-box[data-v-0d8562cf] {\n    font-size: 14px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title[data-v-0d8562cf] {\n      font-size: 18px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title .badge-wrap[data-v-0d8562cf] {\n        font-size: 15px;\n}\n}\n@media screen and (max-width: 991px) {\n.custom-container .test-proj-fillup-page .white-box[data-v-0d8562cf] {\n    padding: 15px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title .badge-wrap[data-v-0d8562cf] {\n      right: 0;\n}\n}\n@media screen and (max-width: 767px) {\n.custom-container .test-proj-fillup-page .status-bar[data-v-0d8562cf] {\n    font-size: 14px;\n}\n.custom-container .test-proj-fillup-page .green-step-btn[data-v-0d8562cf] {\n    margin: 10px 15px 10px 0;\n    max-width: 175px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title .badge-wrap[data-v-0d8562cf] {\n    right: 0;\n    top: -3px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status br[data-v-0d8562cf] {\n    display: block;\n}\n.custom-container .test-proj-fillup-page .white-box .notes-list[data-v-0d8562cf] {\n    margin-bottom: 15px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.active span[data-v-0d8562cf] {\n    display: block !important;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a[data-v-0d8562cf] {\n    font-size: 18px;\n    padding: 10px !important;\n    padding-left: 30px !important;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a[data-v-0d8562cf]::before {\n      top: 18px;\n      left: 10px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a span[data-v-0d8562cf] {\n      font-size: 18px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-issues-tab .issue-block .issue-type[data-v-0d8562cf] {\n    font-size: 16px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-issues-tab .issue-block .form-group .remove-icon[data-v-0d8562cf] {\n    margin-top: 3px;\n    display: block;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-issues-tab .issue-block .form-group .remove-icon i[data-v-0d8562cf] {\n      font-size: 24px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-attachment-tab .add-files-btn[data-v-0d8562cf] {\n    float: left;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-attachment-tab .attachment-table-wrap .attachment-table[data-v-0d8562cf] {\n    border-radius: 6px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-attachment-tab .attachment-table-wrap .attachment-table tr td[data-v-0d8562cf]:nth-child(2) {\n      min-width: 320px;\n}\n}\n@media screen and (max-width: 575px) {\n.custom-container .step-page-title[data-v-0d8562cf] {\n    font-size: 26px;\n}\n.custom-container .test-proj-fillup-page .white-box[data-v-0d8562cf] {\n    font-size: 14px;\n    line-height: 18px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title.proj-name-wrap[data-v-0d8562cf] {\n      padding-bottom: 35px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title.proj-name-wrap .proj-name[data-v-0d8562cf] {\n        display: block;\n        width: 100%;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title.proj-name-wrap .badge-wrap[data-v-0d8562cf] {\n        position: relative;\n        float: right;\n        margin-right: -15px;\n        margin-top: 10px;\n        font-size: 13px;\n        display: block;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status:first-child .proj-progress[data-v-0d8562cf]::before {\n      width: calc(50% - 25px);\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status:last-child .proj-progress[data-v-0d8562cf]::after {\n      width: calc(50% - 25px);\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.active .proj-progress i[data-v-0d8562cf] {\n      font-size: 28px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.active .proj-progress i[data-v-0d8562cf]::after {\n        height: 13px;\n        width: 13px;\n        left: 5px;\n        top: 5px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.done-step .proj-progress i[data-v-0d8562cf] {\n      font-size: 28px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.done-step .proj-progress i[data-v-0d8562cf]::after {\n        height: 13px;\n        width: 13px;\n        left: 5px;\n        top: 5px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .total-score-txt[data-v-0d8562cf] {\n      font-size: 22px;\n      margin-bottom: 20px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a[data-v-0d8562cf] {\n      font-size: 16px;\n      padding: 5px 10px !important;\n      padding-left: 30px !important;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a[data-v-0d8562cf]::before {\n        top: 12px;\n        left: 10px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a span[data-v-0d8562cf] {\n        font-size: 16px;\n}\n.custom-container .green-step-btn[data-v-0d8562cf] {\n    margin: 10px 15px 10px 0;\n}\n}\n@media screen and (max-width: 360px) {\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .total-score-txt[data-v-0d8562cf] {\n    font-size: 18px;\n    margin-bottom: 15px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a[data-v-0d8562cf] {\n    font-size: 14px;\n    padding-left: 20px !important;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a[data-v-0d8562cf]::before {\n      top: 12px;\n      left: 8px;\n      height: 10px;\n      width: 10px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a span[data-v-0d8562cf] {\n      font-size: 14px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-ed68c038\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project/rating.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.ratings-block[data-v-ed68c038] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.ratings-block .rating-item[data-v-ed68c038] {\n    -webkit-box-flex: 1;\n        -ms-flex: 1;\n            flex: 1;\n    min-height: 40px;\n    margin-right: 12px;\n    text-align: center;\n    font-size: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    font-weight: bold;\n}\n.ratings-block .rating-item[data-v-ed68c038]:not(.fill) {\n      color: #606368 !important;\n      background: #f2f2f2 !important;\n}\n.ratings-block .rating-item i[data-v-ed68c038] {\n      margin-left: 3px;\n}\n.ratings-block .rating-item.fill i[data-v-ed68c038]:before {\n      content: \"\\F005\";\n}\n.ratings-block .rating-item.first[data-v-ed68c038] {\n      border-bottom-left-radius: 20px;\n      border-top-left-radius: 20px;\n}\n.ratings-block .rating-item.last[data-v-ed68c038] {\n      border-top-right-radius: 20px;\n      border-bottom-right-radius: 20px;\n}\n.detail-block-wrap[data-v-ed68c038] {\n  background-color: #fff;\n  border: 1px solid #dadada;\n  padding: 15px;\n  border-radius: 6px;\n}\n.detail-block-wrap h4[data-v-ed68c038] {\n    font-size: 18px;\n    color: #2cac3d;\n    font-family: \"BrandonTextBold\";\n}\n.detail-block-wrap .detail-block[data-v-ed68c038] {\n    font-family: \"BrandonTextMedium\";\n    font-size: 14px;\n    line-height: 16px;\n}\n.detail-block-wrap .detail-block span[data-v-ed68c038] {\n      width: calc(100% / 3 - 5px);\n      display: inline-block;\n      vertical-align: top;\n}\n.detail-block-wrap .explanation-wrap textarea[data-v-ed68c038] {\n    resize: none;\n    padding: 0px 10px;\n    font-size: 17px;\n    color: #9c9c9c;\n}\n.detail-block-wrap .explanation-wrap textarea[data-v-ed68c038]:hover, .detail-block-wrap .explanation-wrap textarea[data-v-ed68c038]:focus {\n      outline: none !important;\n}\n.detail-block-wrap .ratings-block[data-v-ed68c038] {\n    margin-top: 10px;\n}\n.detail-block-wrap .ratings-block img[data-v-ed68c038] {\n      width: 100%;\n}\n.detail-block-wrap .green-step-btn-wrap .green-step-btn[data-v-ed68c038] {\n    margin: 0;\n    margin-top: 30px;\n    font-size: 12px;\n    line-height: 14px;\n    text-transform: uppercase;\n    padding: 4px 8px;\n    cursor: pointer;\n    color: #fff;\n}\n.green-step-btn[data-v-ed68c038] {\n  width: 100%;\n  max-width: 180px;\n  padding: 7px 10px;\n  border: 2px solid #118921;\n  background-color: #2cac3d;\n  color: #fff;\n  font-size: 14px;\n  font-family: \"BrandonTextMedium\";\n  letter-spacing: 0.05rem;\n  color: #fff;\n  border-radius: 20px;\n  display: inline-block;\n  text-align: center;\n  margin: 10px 15px 30px 0px;\n}\n.green-step-btn[data-v-ed68c038]:hover {\n    background-color: #158f25;\n    color: #fff;\n    outline: none !important;\n}\n@media screen and (max-width: 1281px) {\n.custom-container .test-proj-fillup-page .white-box[data-v-ed68c038] {\n    font-size: 16px;\n}\n.custom-container .test-proj-fillup-page .green-step-btn[data-v-ed68c038] {\n    font-size: 13px;\n}\n}\n@media screen and (max-width: 1200px) {\n.detail-block-wrap .green-step-btn-wrap[data-v-ed68c038] {\n    max-width: 200px;\n    float: right;\n}\n.detail-block-wrap .green-step-btn-wrap .green-step-btn[data-v-ed68c038] {\n      margin-top: 20px;\n}\n}\n@media screen and (max-width: 767px) {\n.detail-block-wrap h4[data-v-ed68c038] {\n    font-size: 16px;\n}\n.detail-block-wrap .ratings-block[data-v-ed68c038] {\n    margin-top: 0;\n}\n.detail-block-wrap .green-step-btn-wrap[data-v-ed68c038] {\n    max-width: 160px;\n}\n.detail-block-wrap .green-step-btn-wrap .green-step-btn[data-v-ed68c038] {\n      margin-top: 10px;\n      font-size: 12px;\n      line-height: 12px;\n}\n}\n@media screen and (max-width: 575px) {\n.detail-block-wrap .green-step-btn-wrap[data-v-ed68c038] {\n    max-width: 130px;\n}\n.detail-block-wrap .green-step-btn-wrap .green-step-btn[data-v-ed68c038] {\n      font-size: 10px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-0d8562cf\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/tester/project-fillup-accepted.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "bg-color" }, [
    _c("div", { staticClass: "container custom-container" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "test-proj-fillup-page" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "white-box" }, [
            _c("div", { staticClass: "row" }, [
              _vm._m(1),
              _vm._v(" "),
              _c("div", { staticClass: "col-12 col-sm-6 col-xl-3" }, [
                _c(
                  "p",
                  { staticClass: "mb-1 mt-2 mt-sm-0" },
                  [
                    _c("span", { staticClass: "text-bold" }, [
                      _vm._v("Devices :")
                    ]),
                    _vm._v(" "),
                    _c("device-selector", {
                      attrs: { disabled: "" },
                      model: {
                        value: _vm.testProject.selected_devices,
                        callback: function($$v) {
                          _vm.$set(_vm.testProject, "selected_devices", $$v)
                        },
                        expression: "testProject.selected_devices"
                      }
                    }),
                    _vm._v(" "),
                    _c("span", {}, [_vm._v("The Beta Plan")])
                  ],
                  1
                ),
                _vm._v(" "),
                _vm._m(2)
              ]),
              _vm._v(" "),
              _vm._m(3),
              _vm._v(" "),
              _vm._m(4)
            ]),
            _vm._v(" "),
            _vm._m(5),
            _vm._v(" "),
            _vm._m(6)
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "white-box" }, [
            _vm._m(7),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-12" }, [
                _c(
                  "div",
                  { staticClass: "testing-process-tabwrap" },
                  [
                    _c(
                      "b-tabs",
                      [
                        _c(
                          "b-tab",
                          {
                            staticClass: "active-tab",
                            attrs: { title: "Questions", active: "" }
                          },
                          [
                            _c("div", { staticClass: "tester-questions-tab" }, [
                              _c("div", { staticClass: "row" }, [
                                _c("div", { staticClass: "col-12 col-md-5" }, [
                                  _c("p", { staticClass: "mb-sm-1" }, [
                                    _c("span", { staticClass: "text-medium" }, [
                                      _vm._v("Q1.")
                                    ]),
                                    _vm._v(
                                      "\n                              Did you like the ambient sounds?\n                            "
                                    )
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "col-12 col-md-7" }, [
                                  _c("div", { staticClass: "form-group" }, [
                                    _c("input", {
                                      staticClass: "form-control",
                                      attrs: { type: "text", placeholder: "" }
                                    })
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "col-12 col-md-5" }, [
                                  _c("p", { staticClass: "mb-sm-1" }, [
                                    _c("span", { staticClass: "text-medium" }, [
                                      _vm._v("Q2.")
                                    ]),
                                    _vm._v(
                                      "\n                              Were you able to access all areas?\n                            "
                                    )
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "col-12 col-md-7" }, [
                                  _c("div", { staticClass: "form-group" }, [
                                    _c("input", {
                                      staticClass: "form-control",
                                      attrs: { type: "text", placeholder: "" }
                                    })
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "col-12 col-md-5" }, [
                                  _c("p", { staticClass: "mb-sm-1" }, [
                                    _c("span", { staticClass: "text-medium" }, [
                                      _vm._v("Q3.")
                                    ]),
                                    _vm._v(
                                      "\n                              Are there changes you would like to see?\n                            "
                                    )
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "col-12 col-md-7" }, [
                                  _c("div", { staticClass: "form-group" }, [
                                    _c("input", {
                                      staticClass: "form-control",
                                      attrs: { type: "text", placeholder: "" }
                                    })
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "col-12 col-md-5" }, [
                                  _c("p", { staticClass: "mb-sm-1" }, [
                                    _c("span", { staticClass: "text-medium" }, [
                                      _vm._v("Q4.")
                                    ]),
                                    _vm._v(
                                      "\n                              Would you pay for this app? How much?\n                            "
                                    )
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "col-12 col-md-7" }, [
                                  _c("div", { staticClass: "form-group" }, [
                                    _c("input", {
                                      staticClass: "form-control",
                                      attrs: { type: "text", placeholder: "" }
                                    })
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "col-12 col-md-5" }, [
                                  _c("p", { staticClass: "mb-sm-1" }, [
                                    _c("span", { staticClass: "text-medium" }, [
                                      _vm._v("Q5.")
                                    ]),
                                    _vm._v(
                                      "\n                              Would you refer to your friends as it is today?\n                            "
                                    )
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "col-12 col-md-7" }, [
                                  _c("div", { staticClass: "form-group" }, [
                                    _c("input", {
                                      staticClass: "form-control",
                                      attrs: { type: "text", placeholder: "" }
                                    })
                                  ])
                                ])
                              ])
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c("b-tab", { attrs: { title: "Ratings/Scores" } }, [
                          _c("div", { staticClass: "tester-rating-tab" }, [
                            _c("h4", { staticClass: "total-score-txt" }, [
                              _vm._v(
                                "\n                          Total Score: 79\n                          "
                              ),
                              _c("span", { staticClass: "effective-badge" }, [
                                _vm._v("Effective")
                              ])
                            ]),
                            _vm._v(" "),
                            _c(
                              "div",
                              { attrs: { role: "tablist" } },
                              [
                                _c(
                                  "b-card",
                                  {
                                    staticClass:
                                      "mb-3 mb-sm-4 custom-accordion",
                                    attrs: { "no-body": "" }
                                  },
                                  [
                                    _c(
                                      "b-card-header",
                                      {
                                        staticClass: "p-1",
                                        attrs: {
                                          "header-tag": "header",
                                          role: "tab"
                                        }
                                      },
                                      [
                                        _c(
                                          "b-btn",
                                          {
                                            directives: [
                                              {
                                                name: "b-toggle",
                                                rawName:
                                                  "v-b-toggle.accordion1",
                                                modifiers: { accordion1: true }
                                              }
                                            ],
                                            attrs: { block: "", href: "#" }
                                          },
                                          [
                                            _vm._v(
                                              "\n                                content\n                                "
                                            ),
                                            _c(
                                              "span",
                                              { staticClass: "text-right" },
                                              [
                                                _vm._v(
                                                  "\n                                  Total Score : 00\n                                "
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "b-collapse",
                                      {
                                        attrs: {
                                          id: "accordion1",
                                          visible: "",
                                          accordion: "my-accordion",
                                          role: "tabpanel"
                                        }
                                      },
                                      [
                                        _c(
                                          "b-card-body",
                                          [_c("starRatings")],
                                          1
                                        )
                                      ],
                                      1
                                    )
                                  ],
                                  1
                                ),
                                _vm._v(" "),
                                _c(
                                  "b-card",
                                  {
                                    staticClass:
                                      "mb-3 mb-sm-4 custom-accordion",
                                    attrs: { "no-body": "" }
                                  },
                                  [
                                    _c(
                                      "b-card-header",
                                      {
                                        staticClass: "p-1",
                                        attrs: {
                                          "header-tag": "header",
                                          role: "tab"
                                        }
                                      },
                                      [
                                        _c(
                                          "b-btn",
                                          {
                                            directives: [
                                              {
                                                name: "b-toggle",
                                                rawName:
                                                  "v-b-toggle.accordion2",
                                                modifiers: { accordion2: true }
                                              }
                                            ],
                                            attrs: { block: "", href: "#" }
                                          },
                                          [
                                            _vm._v(
                                              "\n                                Gameplay\n                                "
                                            ),
                                            _c(
                                              "span",
                                              { staticClass: "text-right" },
                                              [
                                                _vm._v(
                                                  "\n                                  Total Score : 00\n                                "
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "b-collapse",
                                      {
                                        attrs: {
                                          id: "accordion2",
                                          accordion: "my-accordion",
                                          role: "tabpanel"
                                        }
                                      },
                                      [
                                        _c(
                                          "b-card-body",
                                          [_c("starRatings")],
                                          1
                                        )
                                      ],
                                      1
                                    )
                                  ],
                                  1
                                ),
                                _vm._v(" "),
                                _c(
                                  "b-card",
                                  {
                                    staticClass:
                                      "mb-3 mb-sm-4 custom-accordion",
                                    attrs: { "no-body": "" }
                                  },
                                  [
                                    _c(
                                      "b-card-header",
                                      {
                                        staticClass: "p-1",
                                        attrs: {
                                          "header-tag": "header",
                                          role: "tab"
                                        }
                                      },
                                      [
                                        _c(
                                          "b-btn",
                                          {
                                            directives: [
                                              {
                                                name: "b-toggle",
                                                rawName:
                                                  "v-b-toggle.accordion3",
                                                modifiers: { accordion3: true }
                                              }
                                            ],
                                            attrs: { block: "", href: "#" }
                                          },
                                          [
                                            _vm._v(
                                              "\n                                multimedia\n                                "
                                            ),
                                            _c(
                                              "span",
                                              { staticClass: "text-right" },
                                              [
                                                _vm._v(
                                                  "\n                                  Total Score : 00\n                                "
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "b-collapse",
                                      {
                                        attrs: {
                                          id: "accordion3",
                                          accordion: "my-accordion",
                                          role: "tabpanel"
                                        }
                                      },
                                      [
                                        _c(
                                          "b-card-body",
                                          [_c("starRatings")],
                                          1
                                        )
                                      ],
                                      1
                                    )
                                  ],
                                  1
                                )
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c("h4", { staticClass: "total-score-txt mb-0" }, [
                              _vm._v(
                                "\n                          Total Score: 79\n                          "
                              ),
                              _c("span", { staticClass: "effective-badge" }, [
                                _vm._v("Effective")
                              ])
                            ])
                          ])
                        ]),
                        _vm._v(" "),
                        _c("b-tab", { attrs: { title: "Issues" } }, [
                          _c("div", { staticClass: "tester-issues-tab" }, [
                            _c("div", { staticClass: "issue-block" }, [
                              _c("div", { staticClass: "row" }, [
                                _c("div", { staticClass: "col-12" }, [
                                  _c(
                                    "p",
                                    {
                                      staticClass:
                                        "text-bold text-green issue-type"
                                    },
                                    [_vm._v("Black Widow -  High type bug:")]
                                  ),
                                  _vm._v(" "),
                                  _c("p", [
                                    _vm._v(
                                      "The issue(s) is critical / deadly to the app usability and must be dealt with immediately."
                                    )
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "col-12" }, [
                                  _c(
                                    "div",
                                    { staticClass: "form-group mb-3 mb-md-5" },
                                    _vm._l(_vm.cnt1, function(cnt) {
                                      return _c(
                                        "div",
                                        { staticClass: "issue-link" },
                                        [
                                          _c("div", { staticClass: "row" }, [
                                            _c(
                                              "div",
                                              {
                                                staticClass: "col-10 col-md-11"
                                              },
                                              [
                                                _c("input", {
                                                  staticClass:
                                                    "form-control custom-form-control",
                                                  attrs: {
                                                    type: "text",
                                                    placeholder: ""
                                                  }
                                                })
                                              ]
                                            ),
                                            _vm._v(" "),
                                            cnt == _vm.cnt1
                                              ? _c(
                                                  "div",
                                                  {
                                                    staticClass:
                                                      "col-2 col-md-1"
                                                  },
                                                  [
                                                    _c(
                                                      "a",
                                                      {
                                                        staticClass:
                                                          "remove-icon",
                                                        attrs: {
                                                          "data-bug-type":
                                                            "high",
                                                          href: "#"
                                                        },
                                                        on: {
                                                          click: _vm.addField
                                                        }
                                                      },
                                                      [
                                                        _c("i", {
                                                          staticClass:
                                                            "fa fa-plus-circle"
                                                        })
                                                      ]
                                                    )
                                                  ]
                                                )
                                              : _c(
                                                  "div",
                                                  {
                                                    staticClass:
                                                      "col-2 col-md-1"
                                                  },
                                                  [
                                                    _c(
                                                      "a",
                                                      {
                                                        staticClass:
                                                          "remove-icon",
                                                        attrs: {
                                                          "data-bug-type":
                                                            "high",
                                                          href: "#"
                                                        },
                                                        on: {
                                                          click: _vm.removeField
                                                        }
                                                      },
                                                      [
                                                        _c("i", {
                                                          staticClass:
                                                            "fa fa-minus-circle"
                                                        })
                                                      ]
                                                    )
                                                  ]
                                                )
                                          ])
                                        ]
                                      )
                                    })
                                  )
                                ])
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "issue-block" }, [
                              _c("div", { staticClass: "row" }, [
                                _c("div", { staticClass: "col-12" }, [
                                  _c(
                                    "p",
                                    {
                                      staticClass:
                                        "text-bold text-green issue-type"
                                    },
                                    [_vm._v("Wasp -  Medium type bug:")]
                                  ),
                                  _vm._v(" "),
                                  _c("p", [
                                    _vm._v(
                                      "The issue(s) affects the flow / enjoyment of the app but can wait until the next update to be solved."
                                    )
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "col-12" }, [
                                  _c(
                                    "div",
                                    { staticClass: "form-group mb-3 mb-md-5" },
                                    _vm._l(_vm.cnt2, function(cnt) {
                                      return _c(
                                        "div",
                                        { staticClass: "issue-link" },
                                        [
                                          _c("div", { staticClass: "row" }, [
                                            _c(
                                              "div",
                                              {
                                                staticClass: "col-10 col-md-11"
                                              },
                                              [
                                                _c("input", {
                                                  staticClass:
                                                    "form-control custom-form-control",
                                                  attrs: {
                                                    type: "text",
                                                    placeholder: ""
                                                  }
                                                })
                                              ]
                                            ),
                                            _vm._v(" "),
                                            cnt == _vm.cnt2
                                              ? _c(
                                                  "div",
                                                  {
                                                    staticClass:
                                                      "col-2 col-md-1"
                                                  },
                                                  [
                                                    _c(
                                                      "a",
                                                      {
                                                        staticClass:
                                                          "remove-icon",
                                                        attrs: {
                                                          "data-bug-type":
                                                            "medium",
                                                          href: "#"
                                                        },
                                                        on: {
                                                          click: _vm.addField
                                                        }
                                                      },
                                                      [
                                                        _c("i", {
                                                          staticClass:
                                                            "fa fa-plus-circle"
                                                        })
                                                      ]
                                                    )
                                                  ]
                                                )
                                              : _c(
                                                  "div",
                                                  {
                                                    staticClass:
                                                      "col-2 col-md-1"
                                                  },
                                                  [
                                                    _c(
                                                      "a",
                                                      {
                                                        staticClass:
                                                          "remove-icon",
                                                        attrs: {
                                                          "data-bug-type":
                                                            "medium",
                                                          href: "#"
                                                        },
                                                        on: {
                                                          click: _vm.removeField
                                                        }
                                                      },
                                                      [
                                                        _c("i", {
                                                          staticClass:
                                                            "fa fa-minus-circle"
                                                        })
                                                      ]
                                                    )
                                                  ]
                                                )
                                          ])
                                        ]
                                      )
                                    })
                                  )
                                ])
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "issue-block" }, [
                              _c("div", { staticClass: "row" }, [
                                _c("div", { staticClass: "col-12" }, [
                                  _c(
                                    "p",
                                    {
                                      staticClass:
                                        "text-bold text-green issue-type"
                                    },
                                    [_vm._v("Gnat - Low type bug:")]
                                  ),
                                  _vm._v(" "),
                                  _c("p", [
                                    _vm._v(
                                      "The issue(s) is annoying but not critical to app function or flow.."
                                    )
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "col-12" }, [
                                  _c(
                                    "div",
                                    { staticClass: "form-group mb-2" },
                                    [
                                      _c("div", { staticClass: "issue-link" }, [
                                        _c("div", { staticClass: "row" }, [
                                          _c(
                                            "div",
                                            { staticClass: "col-10 col-md-11" },
                                            [
                                              _c("input", {
                                                staticClass: "form-control",
                                                attrs: {
                                                  type: "text",
                                                  placeholder: ""
                                                }
                                              })
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            { staticClass: "col-2 col-md-1" },
                                            [
                                              _c(
                                                "a",
                                                {
                                                  staticClass: "remove-icon",
                                                  attrs: { href: "#" },
                                                  on: { click: _vm.addField }
                                                },
                                                [
                                                  _c("i", {
                                                    staticClass:
                                                      "fa fa-plus-circle"
                                                  })
                                                ]
                                              )
                                            ]
                                          )
                                        ])
                                      ])
                                    ]
                                  )
                                ])
                              ])
                            ]),
                            _vm._v(" "),
                            _c("hr", { staticClass: "line-seprator" }),
                            _vm._v(" "),
                            _c("div", { staticClass: "issue-block" }, [
                              _c("div", { staticClass: "row" }, [
                                _c("div", { staticClass: "col-12" }, [
                                  _c(
                                    "p",
                                    {
                                      staticClass:
                                        "text-bold text-green issue-type"
                                    },
                                    [_vm._v("Did you find any cosmetic issue?")]
                                  )
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "col-12" }, [
                                  _c(
                                    "div",
                                    { staticClass: "form-group mb-3 mb-md-5" },
                                    [
                                      _c("div", { staticClass: "issue-link" }, [
                                        _c("div", { staticClass: "row" }, [
                                          _c(
                                            "div",
                                            { staticClass: "col-10 col-md-11" },
                                            [
                                              _c("input", {
                                                staticClass: "form-control",
                                                attrs: {
                                                  type: "text",
                                                  placeholder: ""
                                                }
                                              })
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            { staticClass: "col-2 col-md-1" },
                                            [
                                              _c(
                                                "a",
                                                {
                                                  staticClass: "remove-icon",
                                                  attrs: { href: "#" }
                                                },
                                                [
                                                  _c("i", {
                                                    staticClass:
                                                      "fa fa-plus-circle"
                                                  })
                                                ]
                                              )
                                            ]
                                          )
                                        ])
                                      ])
                                    ]
                                  )
                                ])
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "issue-block" }, [
                              _c("div", { staticClass: "row" }, [
                                _c("div", { staticClass: "col-12" }, [
                                  _c(
                                    "p",
                                    {
                                      staticClass:
                                        "text-bold text-green issue-type"
                                    },
                                    [
                                      _vm._v(
                                        "Did you find any other issues you would like to mention?"
                                      )
                                    ]
                                  )
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "col-12" }, [
                                  _c("div", { staticClass: "form-group" }, [
                                    _c("div", { staticClass: "issue-link" }, [
                                      _c("div", { staticClass: "row" }, [
                                        _c(
                                          "div",
                                          { staticClass: "col-10 col-md-11" },
                                          [
                                            _c("input", {
                                              staticClass: "form-control mb-0",
                                              attrs: {
                                                type: "text",
                                                placeholder: ""
                                              }
                                            })
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "col-2 col-md-1" },
                                          [
                                            _c(
                                              "a",
                                              {
                                                staticClass: "remove-icon",
                                                attrs: { href: "#" }
                                              },
                                              [
                                                _c("i", {
                                                  staticClass:
                                                    "fa fa-plus-circle"
                                                })
                                              ]
                                            )
                                          ]
                                        )
                                      ])
                                    ])
                                  ])
                                ])
                              ])
                            ])
                          ])
                        ]),
                        _vm._v(" "),
                        _c("b-tab", { attrs: { title: "Attachment" } }, [
                          _c("div", { staticClass: "tester-attachment-tab" }, [
                            _c("div", { staticClass: "row" }, [
                              _c("div", { staticClass: "col-12" }, [
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "text-uppercase green-step-btn add-files-btn"
                                  },
                                  [
                                    _c("span", [
                                      _vm._v(
                                        "\n                                Add Files\n                              "
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("input", {
                                      staticClass: "btn",
                                      attrs: { type: "file" }
                                    })
                                  ]
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "attachment-table-wrap" },
                              [
                                _c("div", { staticClass: "row" }, [
                                  _c("div", { staticClass: "col-12" }, [
                                    _c(
                                      "div",
                                      { staticClass: "table-responsive" },
                                      [
                                        _c(
                                          "table",
                                          {
                                            staticClass:
                                              "table table-bordered attachment-table"
                                          },
                                          [
                                            _c("tbody", [
                                              _c("tr", [
                                                _c(
                                                  "td",
                                                  { attrs: { scope: "row" } },
                                                  [
                                                    _c("img", {
                                                      staticClass: "img-fluid",
                                                      attrs: {
                                                        src: __webpack_require__("./resources/assets/assets/img/mp4-icon.png")
                                                      }
                                                    })
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "td",
                                                  {
                                                    staticClass: " text-medium"
                                                  },
                                                  [_vm._v("File Name.mp4")]
                                                ),
                                                _vm._v(" "),
                                                _c("td", [_vm._v("236 MB")]),
                                                _vm._v(" "),
                                                _c(
                                                  "td",
                                                  {
                                                    staticClass: "text-center"
                                                  },
                                                  [
                                                    _c(
                                                      "a",
                                                      {
                                                        staticClass:
                                                          "d-inline-block",
                                                        attrs: { href: "#" }
                                                      },
                                                      [
                                                        _c("i", {
                                                          staticClass:
                                                            "fa fa-eye"
                                                        })
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "a",
                                                      {
                                                        staticClass:
                                                          "d-inline-block",
                                                        attrs: { href: "#" }
                                                      },
                                                      [
                                                        _c("i", {
                                                          staticClass:
                                                            "fa fa-trash"
                                                        })
                                                      ]
                                                    )
                                                  ]
                                                )
                                              ]),
                                              _vm._v(" "),
                                              _c("tr", [
                                                _c(
                                                  "td",
                                                  { attrs: { scope: "row" } },
                                                  [
                                                    _c("img", {
                                                      staticClass: "img-fluid",
                                                      attrs: {
                                                        src: __webpack_require__("./resources/assets/assets/img/pdf-icon.png")
                                                      }
                                                    })
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "td",
                                                  {
                                                    staticClass: "text-medium"
                                                  },
                                                  [_vm._v("File Name_2.pdf")]
                                                ),
                                                _vm._v(" "),
                                                _c("td", [_vm._v("112 KB")]),
                                                _vm._v(" "),
                                                _c(
                                                  "td",
                                                  {
                                                    staticClass: "text-center"
                                                  },
                                                  [
                                                    _c(
                                                      "a",
                                                      {
                                                        staticClass:
                                                          "d-inline-block",
                                                        attrs: { href: "#" }
                                                      },
                                                      [
                                                        _c("i", {
                                                          staticClass:
                                                            "fa fa-eye"
                                                        })
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "a",
                                                      {
                                                        staticClass:
                                                          "d-inline-block",
                                                        attrs: { href: "#" }
                                                      },
                                                      [
                                                        _c("i", {
                                                          staticClass:
                                                            "fa fa-trash"
                                                        })
                                                      ]
                                                    )
                                                  ]
                                                )
                                              ]),
                                              _vm._v(" "),
                                              _c("tr", [
                                                _c(
                                                  "td",
                                                  { attrs: { scope: "row" } },
                                                  [
                                                    _c("img", {
                                                      staticClass: "img-fluid",
                                                      attrs: {
                                                        src: __webpack_require__("./resources/assets/assets/img/png-icon.png")
                                                      }
                                                    })
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "td",
                                                  {
                                                    staticClass: "text-medium"
                                                  },
                                                  [_vm._v("File Name_3.png")]
                                                ),
                                                _vm._v(" "),
                                                _c("td", [_vm._v("1.25 MB")]),
                                                _vm._v(" "),
                                                _c(
                                                  "td",
                                                  {
                                                    staticClass: "text-center"
                                                  },
                                                  [
                                                    _c(
                                                      "a",
                                                      {
                                                        staticClass:
                                                          "d-inline-block",
                                                        attrs: { href: "#" }
                                                      },
                                                      [
                                                        _c("i", {
                                                          staticClass:
                                                            "fa fa-eye"
                                                        })
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "a",
                                                      {
                                                        staticClass:
                                                          "d-inline-block",
                                                        attrs: { href: "#" }
                                                      },
                                                      [
                                                        _c("i", {
                                                          staticClass:
                                                            "fa fa-trash"
                                                        })
                                                      ]
                                                    )
                                                  ]
                                                )
                                              ]),
                                              _vm._v(" "),
                                              _c("tr", [
                                                _c(
                                                  "td",
                                                  { attrs: { scope: "row" } },
                                                  [
                                                    _c("img", {
                                                      staticClass: "img-fluid",
                                                      attrs: {
                                                        src: __webpack_require__("./resources/assets/assets/img/jpg-icon.png")
                                                      }
                                                    })
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "td",
                                                  {
                                                    staticClass: "text-medium"
                                                  },
                                                  [_vm._v("File Name_4.jpg")]
                                                ),
                                                _vm._v(" "),
                                                _c("td", [_vm._v("1.25 MB")]),
                                                _vm._v(" "),
                                                _c(
                                                  "td",
                                                  {
                                                    staticClass: "text-center"
                                                  },
                                                  [
                                                    _c(
                                                      "a",
                                                      {
                                                        staticClass:
                                                          "d-inline-block",
                                                        attrs: { href: "#" }
                                                      },
                                                      [
                                                        _c("i", {
                                                          staticClass:
                                                            "fa fa-eye"
                                                        })
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "a",
                                                      {
                                                        staticClass:
                                                          "d-inline-block",
                                                        attrs: { href: "#" }
                                                      },
                                                      [
                                                        _c("i", {
                                                          staticClass:
                                                            "fa fa-trash"
                                                        })
                                                      ]
                                                    )
                                                  ]
                                                )
                                              ])
                                            ])
                                          ]
                                        )
                                      ]
                                    )
                                  ])
                                ])
                              ]
                            )
                          ])
                        ])
                      ],
                      1
                    )
                  ],
                  1
                )
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _vm._m(8)
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "white-box" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-12" }, [
          _c("h4", { staticClass: "block-title border-bottom-0" }, [
            _vm._v("\n                  Project Status\n                ")
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-12" }, [
          _c("div", { staticClass: "row" }, [
            _c(
              "div",
              { staticClass: "col-2 text-center proj-status done-step" },
              [
                _c("p", { staticClass: "proj-progress" }, [
                  _c("i", { staticClass: "fa fa-circle-thin" })
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "d-none d-md-block text-medium" }, [
                  _vm._v("New")
                ])
              ]
            ),
            _vm._v(" "),
            _c("div", { staticClass: "col-2 text-center proj-status active" }, [
              _c("p", { staticClass: "proj-progress" }, [
                _c("i", { staticClass: "fa fa-circle-thin" })
              ]),
              _vm._v(" "),
              _c("span", { staticClass: "d-none d-md-block text-medium" }, [
                _vm._v("Accepted/"),
                _c("br"),
                _vm._v("Working")
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-2 text-center proj-status" }, [
              _c("p", { staticClass: "proj-progress" }, [
                _c("i", { staticClass: "fa fa-circle-thin" })
              ]),
              _vm._v(" "),
              _c("span", { staticClass: "d-none d-md-block text-medium" }, [
                _vm._v("Submitted")
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-2 text-center proj-status" }, [
              _c("p", { staticClass: "proj-progress" }, [
                _c("i", { staticClass: "fa fa-circle-thin" })
              ]),
              _vm._v(" "),
              _c("span", { staticClass: "d-none d-md-block text-medium" }, [
                _vm._v(" Under Review")
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-2 text-center proj-status" }, [
              _c("p", { staticClass: "proj-progress" }, [
                _c("i", { staticClass: "fa fa-circle-thin" })
              ]),
              _vm._v(" "),
              _c("span", { staticClass: "d-none d-md-block text-medium" }, [
                _vm._v("Completed")
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-2 text-center proj-status" }, [
              _c("p", { staticClass: "proj-progress" }, [
                _c("i", { staticClass: "fa fa-circle-thin" })
              ]),
              _vm._v(" "),
              _c("span", { staticClass: "d-none d-md-block text-medium" }, [
                _vm._v(" Paid")
              ])
            ])
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-12" }, [
      _c("h4", { staticClass: "text-bold block-title proj-name-wrap" }, [
        _c("span", { staticClass: "proj-name" }, [_vm._v("Test")]),
        _vm._v(" "),
        _c("span", { staticClass: "badge-wrap text-right" }, [
          _c("span", { staticClass: "status-badge" }, [
            _vm._v(
              "\n                      Status:Open Spot(Unlimited Candidates)\n                    "
            )
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "mb-xl-5 mb-3 mt-2 mt-sm-0" }, [
      _c("span", { staticClass: "text-bold" }, [_vm._v("Type :")]),
      _vm._v(" "),
      _c("span", {}, [_vm._v("Games")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-12 col-sm-6 col-xl-3" }, [
      _c("p", { staticClass: "mb-1 mt-2 mt-sm-0" }, [
        _c("span", { staticClass: "text-bold" }, [_vm._v("Start Date : ")]),
        _vm._v(" "),
        _c("span", [_vm._v("10 January, 2018")])
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "mb-xl-5 mb-3 mt-2 mt-sm-0" }, [
        _c("span", { staticClass: "text-bold" }, [_vm._v("End Date : ")]),
        _vm._v(" "),
        _c("span", [_vm._v("15 January, 2018")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "col-12 col-xl-6 text-xl-right text-left" },
      [
        _c("p", { staticClass: "mb-xl-5 mb-3 mt-2 mt-sm-0" }, [
          _c("span", { staticClass: "text-bold" }, [
            _vm._v("Estimated Tester Time needed to complete: ")
          ]),
          _vm._v(" "),
          _c("span", { staticClass: "d-block" }, [_vm._v("1 hr 15 Minutes")])
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12" }, [
        _c("h4", { staticClass: "text-bold block-title" }, [
          _vm._v("Project Description\n                ")
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-12" }, [
        _c("p", { staticClass: "proj-desc" }, [
          _vm._v(
            "\n                  The project description goes here, and this project will have a number of testers, testing based on usability, bug detection and overall fun factor. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the...\n                "
          )
        ]),
        _vm._v(" "),
        _c(
          "a",
          {
            staticClass:
              "view-more-link mb-xl-5 mb-md-3 md-1 w-100 float-right",
            attrs: { href: "#" }
          },
          [_vm._v("\n                  View More\n                ")]
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-12 col-md-6 mb-2 mt-2 mt-sm-0" }, [
        _c("h4", { staticClass: "block-title text-bold" }, [
          _vm._v("Links Of App")
        ]),
        _vm._v(" "),
        _c("p", [
          _c("span", { staticClass: "text-bold app-title" }, [
            _vm._v("Title :")
          ]),
          _vm._v(" "),
          _c("span", { staticClass: "app-link" }, [_vm._v("thebetaplan.com")])
        ]),
        _vm._v(" "),
        _c("p", [
          _c("span", { staticClass: "text-bold app-title" }, [
            _vm._v("Title :")
          ]),
          _vm._v(" "),
          _c("span", { staticClass: "app-link" }, [_vm._v("thebetaplan.com")])
        ]),
        _vm._v(" "),
        _c("p", [
          _c("span", { staticClass: "text-bold app-title" }, [
            _vm._v("Title :")
          ]),
          _vm._v(" "),
          _c("span", { staticClass: "app-link" }, [_vm._v("thebetaplan.com")])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-12 col-md-6 mb-2 mt-2 mt-sm-0" }, [
        _c("h4", { staticClass: "block-title text-bold" }, [
          _vm._v("Upload App")
        ]),
        _vm._v(" "),
        _c("p", [
          _c("span", { staticClass: "text-bold" }, [_vm._v("The Beta Plan")])
        ]),
        _vm._v(" "),
        _c("p", [
          _c("span", { staticClass: "text-bold" }, [_vm._v("The Beta Plan")])
        ]),
        _vm._v(" "),
        _c("p", [
          _c("span", { staticClass: "text-bold" }, [_vm._v("The Beta Plan")])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12 col-md-6" }, [
        _c("h4", { staticClass: "block-title text-bold" }, [
          _vm._v("Notes/Instruction")
        ]),
        _vm._v(" "),
        _c("p", { staticClass: "mb-1" }, [
          _vm._v("Notes and Instruction goes here...")
        ]),
        _vm._v(" "),
        _c("ul", { staticClass: "notes-list" }, [
          _c("li", [_vm._v("point one")]),
          _vm._v(" "),
          _c("li", [_vm._v("point two")])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-12 col-md-6" }, [
        _c("h4", { staticClass: "block-title text-bold" }, [
          _vm._v("Required Devices")
        ]),
        _vm._v(" "),
        _c("p", { staticClass: "test-device-list" }, [
          _c("span", { staticClass: "d-inline-block" }, [
            _c("img", {
              staticClass: "img-fluid",
              attrs: { src: __webpack_require__("./resources/assets/assets/img/test-device-1.png") }
            })
          ]),
          _vm._v(" "),
          _c("span", { staticClass: "d-inline-block" }, [
            _c("img", {
              staticClass: "img-fluid",
              attrs: { src: __webpack_require__("./resources/assets/assets/img/test-device-2.png") }
            })
          ]),
          _vm._v(" "),
          _c("span", { staticClass: "d-inline-block" }, [
            _c("img", {
              staticClass: "img-fluid",
              attrs: { src: __webpack_require__("./resources/assets/assets/img/test-device-3.png") }
            })
          ]),
          _vm._v(" "),
          _c("span", { staticClass: "d-inline-block" }, [
            _c("img", {
              staticClass: "img-fluid",
              attrs: { src: __webpack_require__("./resources/assets/assets/img/test-device-4.png") }
            })
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12" }, [
        _c("h4", { staticClass: "block-title" }, [
          _vm._v("\n                  Testing Process\n                ")
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-12 text-center" }, [
      _c(
        "a",
        {
          staticClass: "btn text-uppercase green-step-btn",
          attrs: { href: "#" }
        },
        [_vm._v("review and submit")]
      ),
      _vm._v(" "),
      _c(
        "a",
        {
          staticClass: "btn text-uppercase green-step-btn",
          attrs: { href: "#" }
        },
        [_vm._v("save as draft")]
      )
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0d8562cf", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-ed68c038\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/project/rating.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "detail-block-wrap mb-3" }, [
    _c("h4", [_vm._v("Visual Accuracy")]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        {
          staticClass: "col-12",
          class: { "col-xl-12": _vm.disabled, "col-xl-10": !_vm.disabled }
        },
        [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "ratings-block" },
            _vm._l(_vm.starBlocks, function(item, i) {
              return _c(
                "div",
                {
                  key: i,
                  staticClass: "rating-item",
                  class: {
                    first: i === 0,
                    last: i === _vm.starBlocks.length - 1,
                    fill: _vm.ratingValue.score >= item
                  },
                  style: _vm.ratingItemStyle,
                  on: {
                    mouseover: function($event) {
                      _vm.setRating(item)
                    },
                    mouseout: function($event) {
                      _vm.setRating(_vm.value.score)
                    },
                    click: function($event) {
                      _vm.selected(item)
                    }
                  }
                },
                [
                  _vm._v("\n          " + _vm._s(item) + " "),
                  _c("i", { staticClass: "fa fa-star-o" })
                ]
              )
            })
          )
        ]
      ),
      _vm._v(" "),
      !_vm.disabled
        ? _c("div", { staticClass: "col-12 col-xl-2 pl-xl-0 float-right" }, [
            _c("div", { staticClass: "green-step-btn-wrap" }, [
              _c(
                "a",
                {
                  staticClass: "green-step-btn",
                  on: {
                    click: function($event) {
                      _vm.add_suggestion = !_vm.add_suggestion
                    }
                  }
                },
                [_vm._v("add explaination/ suggestion")]
              )
            ])
          ])
        : _vm._e()
    ]),
    _vm._v(" "),
    _vm.add_suggestion
      ? _c("div", { staticClass: "explanation-wrap mt-2" }, [
          _c("textarea", {
            staticClass: "form-control",
            attrs: { placeholder: "your explanation", rows: "2" },
            domProps: { value: _vm.ratingValue.suggestion },
            on: { input: _vm.addSuggestion }
          })
        ])
      : _vm._e(),
    _vm._v(" "),
    !_vm.add_suggestion && _vm.ratingValue.suggestion
      ? _c("div", { staticClass: "explanation-wrap mt-2" }, [
          _vm._v(_vm._s(_vm.ratingValue.suggestion))
        ])
      : _vm._e()
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "detail-block" }, [
      _c("span", { staticClass: "d-none d-md-inline-block" }, [
        _vm._v(
          "\n          Physics issues, bugs/image jitters. Causes motion sickness\n        "
        )
      ]),
      _vm._v(" "),
      _c("span", { staticClass: "text-center d-none d-md-inline-block" }, [
        _vm._v(
          "\n          Some good representations, some poor representations.\n        "
        )
      ]),
      _vm._v(" "),
      _c("span", { staticClass: "text-right d-none d-md-inline-block" }, [
        _vm._v(
          "\n          No bugs, jitters, image is accurately represented. No motion sickness.\n        "
        )
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-ed68c038", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d8562cf\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/project-fillup-accepted.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d8562cf\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/project-fillup-accepted.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("6886690a", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d8562cf\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project-fillup-accepted.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d8562cf\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project-fillup-accepted.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-ed68c038\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project/rating.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-ed68c038\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project/rating.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("828170ac", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-ed68c038\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./rating.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-ed68c038\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./rating.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/assets/img/down-arrow.png":
/***/ (function(module, exports) {

module.exports = "/images/down-arrow.png?afd9f6fee15e6136539c501c6456bdb2";

/***/ }),

/***/ "./resources/assets/assets/img/jpg-icon.png":
/***/ (function(module, exports) {

module.exports = "/images/jpg-icon.png?02d417bf68e3ed0a3c80849c3dd39f62";

/***/ }),

/***/ "./resources/assets/assets/img/mp4-icon.png":
/***/ (function(module, exports) {

module.exports = "/images/mp4-icon.png?75fbd58bfab425dab9fe6c627a10b6d4";

/***/ }),

/***/ "./resources/assets/assets/img/pdf-icon.png":
/***/ (function(module, exports) {

module.exports = "/images/pdf-icon.png?0524da1a544c06be796160a67dbfba92";

/***/ }),

/***/ "./resources/assets/assets/img/png-icon.png":
/***/ (function(module, exports) {

module.exports = "/images/png-icon.png?866941ffb652edd5a4746535d58f5cb9";

/***/ }),

/***/ "./resources/assets/assets/img/right-arrow.png":
/***/ (function(module, exports) {

module.exports = "/images/right-arrow.png?2464ea6c943c56469cc6289852f7a3e4";

/***/ }),

/***/ "./resources/assets/assets/img/test-device-1.png":
/***/ (function(module, exports) {

module.exports = "/images/test-device-1.png?ca006337f2ce3652843331514e07da3f";

/***/ }),

/***/ "./resources/assets/assets/img/test-device-2.png":
/***/ (function(module, exports) {

module.exports = "/images/test-device-2.png?7e85e5afce72acbb94e430440b19b9ff";

/***/ }),

/***/ "./resources/assets/assets/img/test-device-3.png":
/***/ (function(module, exports) {

module.exports = "/images/test-device-3.png?3cdb3e3f4343ea4d714e3b341994cf8c";

/***/ }),

/***/ "./resources/assets/assets/img/test-device-4.png":
/***/ (function(module, exports) {

module.exports = "/images/test-device-4.png?ff2a656e113eb44caef72b06b261b546";

/***/ }),

/***/ "./resources/assets/components/components/project/rating.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-ed68c038\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project/rating.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/project/rating.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-ed68c038\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/project/rating.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-ed68c038"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\project\\rating.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-ed68c038", Component.options)
  } else {
    hotAPI.reload("data-v-ed68c038", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/pages/tester/project-fillup-accepted.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d8562cf\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/project-fillup-accepted.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/tester/project-fillup-accepted.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-0d8562cf\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/tester/project-fillup-accepted.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-0d8562cf"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\tester\\project-fillup-accepted.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0d8562cf", Component.options)
  } else {
    hotAPI.reload("data-v-0d8562cf", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});