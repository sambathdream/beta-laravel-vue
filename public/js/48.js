webpackJsonp([48],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/test-project/moderate-test-projects.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__("./node_modules/vue/dist/vue.common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment__ = __webpack_require__("./node_modules/moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



__WEBPACK_IMPORTED_MODULE_0_vue___default.a.filter("dateString", function (value) {
  return __WEBPACK_IMPORTED_MODULE_1_moment___default()(String(value)).format("MMM Do YYYY");
});
/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    moment: __WEBPACK_IMPORTED_MODULE_1_moment___default.a
  },
  name: "moderate-test-projects",
  data: function data() {
    return {
      testprojects: {},
      formstate: {},
      model: {},
      originalUser: {},
      aDevices: {},
      additional_devices: [],
      project_types: {}
    };
  },

  mounted: function mounted() {
    var _this = this;

    axios.get("/api/projects?test=1").then(function (response) {
      _this.testprojects = response.data;
      console.log(_this.testprojects);
    }).catch(function (error) {});
    axios.get("/api/devices").then(function (response) {
      _this.aDevices = response.data;
    }).then(function (_ref) {
      var data = _ref.data.data;
      return _this.assignData(data);
    }).catch(function (error) {});

    axios.get("/api/project-types").then(function (response) {
      _this.project_types = response.data;
    }).catch(function (error) {});

    this.originalUser = window.USER;
  },
  methods: {}
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-68ae35cf\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/test-project/moderate-test-projects.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.border-row-bottom[data-v-68ae35cf] {\r\n  border-bottom: 1px solid #797979 !important;\n}\nselect[data-v-68ae35cf] {\r\n  height: 34px;\n}\n.font-12[data-v-68ae35cf] {\r\n  font-weight: 400;\r\n  font-style: normal;\r\n  font-size: 12px;\n}\n.font-24[data-v-68ae35cf] {\r\n  font-weight: 400;\r\n  font-style: normal;\r\n  font-size: 24px;\n}\n.font-bold-12[data-v-68ae35cf] {\r\n  font-weight: 700;\r\n  font-style: normal;\r\n  font-size: 12px;\n}\n.font-bold-16[data-v-68ae35cf] {\r\n  font-weight: 700;\r\n  font-style: normal;\r\n  font-size: 16px;\n}\n.font-title-color[data-v-68ae35cf] {\r\n  color: #666699;\n}\n.view-submision-btn[data-v-68ae35cf] {\r\n  color: #fff !important;\r\n  padding: 5px !important;\r\n  white-space: normal;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-68ae35cf\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/test-project/moderate-test-projects.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-md-12 mb-5" }, [
        _c("div", { staticClass: "row" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-3" }, [
            _c("div", { staticClass: "form-group" }, [
              _c(
                "label",
                { staticClass: "product_label", attrs: { for: "devices" } },
                [_vm._v("Devices:")]
              ),
              _vm._v(" "),
              _c(
                "select",
                {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.model.aDevices,
                      expression: "model.aDevices"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { name: "devices", checkbox: "" },
                  on: {
                    change: function($event) {
                      var $$selectedVal = Array.prototype.filter
                        .call($event.target.options, function(o) {
                          return o.selected
                        })
                        .map(function(o) {
                          var val = "_value" in o ? o._value : o.value
                          return val
                        })
                      _vm.$set(
                        _vm.model,
                        "aDevices",
                        $event.target.multiple
                          ? $$selectedVal
                          : $$selectedVal[0]
                      )
                    }
                  }
                },
                [
                  _c("option", { attrs: { value: "", selected: "" } }, [
                    _vm._v("All Devices")
                  ]),
                  _vm._v(" "),
                  _vm._l(_vm.aDevices, function(aDevice) {
                    return _c("option", { domProps: { value: aDevice.id } }, [
                      _vm._v(" " + _vm._s(aDevice.name) + " ")
                    ])
                  })
                ],
                2
              )
            ])
          ]),
          _vm._v(" "),
          _vm._m(1),
          _vm._v(" "),
          _vm._m(2)
        ]),
        _vm._v(" "),
        _vm._m(3),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-12" }, [
            _c(
              "div",
              { ref: "testprojectList", staticClass: "card box_shadow pb-2" },
              [_vm._m(4), _vm._v(" "), _vm._m(5)]
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-3" }, [
      _c("div", { staticClass: "form-group" }, [
        _c(
          "label",
          { staticClass: "product_label", attrs: { for: "status" } },
          [_vm._v("Status:")]
        ),
        _vm._v(" "),
        _c(
          "select",
          {
            staticClass: "form-control",
            attrs: { id: "example-select", name: "status-select", size: "1" }
          },
          [
            _c("option", { attrs: { value: "0" } }, [_vm._v("Running")]),
            _vm._v(" "),
            _c("option", { attrs: { value: "1" } }, [
              _vm._v("Pending Approval")
            ]),
            _vm._v(" "),
            _c("option", { attrs: { value: "2" } }, [_vm._v("Completed")]),
            _vm._v(" "),
            _c("option", { attrs: { value: "3" } }, [
              _vm._v("Unpaid Projects - pending payments to receive")
            ])
          ]
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-3" }, [
      _c("div", { staticClass: "form-group" }, [
        _c(
          "label",
          { staticClass: "product_label", attrs: { for: "status" } },
          [_vm._v("Tester:")]
        ),
        _vm._v(" "),
        _c("input", {
          staticClass: "form-control p-2",
          attrs: {
            name: "Tester",
            type: "text",
            placeholder: "Enter Tester Name"
          }
        })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-3" }, [
      _c("div", { staticClass: "form-group" }, [
        _c(
          "label",
          { staticClass: "product_label", attrs: { for: "status" } },
          [_vm._v("Sort by:")]
        ),
        _vm._v(" "),
        _c(
          "select",
          {
            staticClass: "form-control",
            attrs: { id: "example-select-2", name: "example-select", size: "1" }
          },
          [
            _c("option", { attrs: { value: "0" } }, [
              _vm._v("Recent Activity")
            ]),
            _vm._v(" "),
            _c("option", { attrs: { value: "1" } }, [_vm._v("Posted Date")]),
            _vm._v(" "),
            _c("option", { attrs: { value: "2" } }, [_vm._v("End Date")]),
            _vm._v(" "),
            _c("option", { attrs: { value: "3" } }, [_vm._v("Lowest Amount")]),
            _vm._v(" "),
            _c("option", { attrs: { value: "4" } }, [_vm._v("Highest Amount")])
          ]
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row pb-1" }, [
      _c("div", { staticClass: "col-md-12" }, [
        _c("h4", { staticClass: "pull-right" }, [
          _c(
            "a",
            {
              staticClass:
                "btn btn-primary rounded-0 mr-2 mb-3 mb-sm-0 top-btn",
              attrs: { href: "#" }
            },
            [_vm._v(" Moderate Test Projects ")]
          ),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass:
                "btn btn-primary rounded-0 mr-2 mb-3 mb-sm-0 top-btn",
              attrs: { href: "#" }
            },
            [_vm._v(" Manage Test Projects ")]
          )
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-header bg-white border-0" }, [
      _c("label", { staticClass: "font-24" }, [
        _vm._v("Test Project Moderation")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-body" }, [
      _c("div", { staticClass: "border-row-bottom mb-4" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-4 pb-3" }, [
            _c("span", { staticClass: "font-bold-16 font-title-color" }, [
              _vm._v("Tester 1")
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-4 pb-3" }, [
            _c("span", { staticClass: "font-bold-16 font-title-color" }, [
              _vm._v("Oculus VR Applicatio Test")
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row pb-3" }, [
          _c("div", { staticClass: "col-md-4 col-12 text-left" }, [
            _c("ul", { staticClass: "pl-0" }, [
              _c("li", [
                _c("label", { staticClass: "font-bold-12" }, [
                  _vm._v("Email: ")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "font-12" }, [
                  _vm._v("tester1@gmail.com")
                ])
              ]),
              _vm._v(" "),
              _c("li", [
                _c("label", { staticClass: "font-bold-12" }, [
                  _vm._v("Birth Day: ")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "font-12" }, [
                  _vm._v("December 12, 1990")
                ])
              ]),
              _vm._v(" "),
              _c("li", [
                _c("label", { staticClass: "font-bold-12" }, [
                  _vm._v("Gender: ")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "font-12" }, [_vm._v("Male")])
              ]),
              _vm._v(" "),
              _c("li", [
                _c("label", { staticClass: "font-bold-12" }, [
                  _vm._v("Registered On ")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "font-12" }, [
                  _vm._v("February 1, 2016")
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-4 col-12 text-left" }, [
            _c("ul", { staticClass: "pl-0 mb-0" }, [
              _c("li", [
                _c("label", { staticClass: "font-bold-12" }, [
                  _vm._v("Publisher: ")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "font-12" }, [
                  _vm._v("Mrs. Natasha Boyle")
                ])
              ]),
              _vm._v(" "),
              _c("li", [
                _c("label", { staticClass: "font-bold-12" }, [
                  _vm._v("Devices: ")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "font-12" }, [
                  _vm._v("Cardboard, Oculus Rift")
                ])
              ]),
              _vm._v(" "),
              _c("li", [
                _c("label", { staticClass: "font-bold-12" }, [
                  _vm._v("Type: ")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "font-12" }, [
                  _vm._v("VR Application")
                ])
              ]),
              _vm._v(" "),
              _c("li", [
                _c("label", { staticClass: "font-bold-12" }, [
                  _vm._v("Tester Working")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "font-12" }, [_vm._v("1000 Testers")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "col-md-2 col-12 text-left text-md-center" },
            [
              _c(
                "a",
                {
                  staticClass:
                    "btn btn-primary rounded-0 px-5 view-submision-btn mb-1 mb-sm-2"
                },
                [_vm._v("View Submission")]
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "col-md-2 col-12 text-left text-md-right" },
            [
              _c("ul", { staticClass: "pl-0" }, [
                _c("li", [
                  _c("label", { staticClass: "font-bold-12 mb-0" }, [
                    _vm._v("Submitted at: ")
                  ]),
                  _vm._v(" "),
                  _c("span", { staticClass: "font-12" }, [
                    _vm._v(" February 15, 2018 ")
                  ])
                ])
              ]),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "btn btn-info text-white",
                  attrs: { href: "javascript:;" }
                },
                [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: __webpack_require__("./resources/assets/assets/img/edit_18x18.png") }
                  })
                ]
              ),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "btn btn-warning text-white",
                  attrs: { href: "javascript:;" }
                },
                [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: __webpack_require__("./resources/assets/assets/img/bin_18x18.png") }
                  })
                ]
              )
            ]
          )
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "border-row-bottom mb-4" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-4 pb-3" }, [
            _c("span", { staticClass: "font-bold-16 font-title-color" }, [
              _vm._v("Tester 1")
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-4 pb-3" }, [
            _c("span", { staticClass: "font-bold-16 font-title-color" }, [
              _vm._v("Oculus VR Applicatio Test")
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row pb-3" }, [
          _c("div", { staticClass: "col-md-4 col-12 text-left" }, [
            _c("ul", { staticClass: "pl-0" }, [
              _c("li", [
                _c("label", { staticClass: "font-bold-12" }, [
                  _vm._v("Email: ")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "font-12" }, [
                  _vm._v("tester1@gmail.com")
                ])
              ]),
              _vm._v(" "),
              _c("li", [
                _c("label", { staticClass: "font-bold-12" }, [
                  _vm._v("Birth Day: ")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "font-12" }, [
                  _vm._v("December 12, 1990")
                ])
              ]),
              _vm._v(" "),
              _c("li", [
                _c("label", { staticClass: "font-bold-12" }, [
                  _vm._v("Gender: ")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "font-12" }, [_vm._v("Male")])
              ]),
              _vm._v(" "),
              _c("li", [
                _c("label", { staticClass: "font-bold-12" }, [
                  _vm._v("Registered On ")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "font-12" }, [
                  _vm._v("February 1, 2016")
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-4 col-12 text-left" }, [
            _c("ul", { staticClass: "pl-0 mb-0" }, [
              _c("li", [
                _c("label", { staticClass: "font-bold-12" }, [
                  _vm._v("Publisher: ")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "font-12" }, [
                  _vm._v("Mrs. Natasha Boyle")
                ])
              ]),
              _vm._v(" "),
              _c("li", [
                _c("label", { staticClass: "font-bold-12" }, [
                  _vm._v("Devices: ")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "font-12" }, [
                  _vm._v("Cardboard, Oculus Rift")
                ])
              ]),
              _vm._v(" "),
              _c("li", [
                _c("label", { staticClass: "font-bold-12" }, [
                  _vm._v("Type: ")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "font-12" }, [
                  _vm._v("VR Application")
                ])
              ]),
              _vm._v(" "),
              _c("li", [
                _c("label", { staticClass: "font-bold-12" }, [
                  _vm._v("Tester Working")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "font-12" }, [_vm._v("1000 Testers")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "col-md-2 col-12 text-left text-md-center" },
            [
              _c(
                "a",
                {
                  staticClass:
                    "btn btn-primary rounded-0 px-5 view-submision-btn mb-1 mb-sm-2"
                },
                [_vm._v("View Submission")]
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "col-md-2 col-12 text-left text-md-right" },
            [
              _c("ul", { staticClass: "pl-0" }, [
                _c("li", [
                  _c("label", { staticClass: "font-bold-12 mb-0" }, [
                    _vm._v("Submitted at: ")
                  ]),
                  _vm._v(" "),
                  _c("span", { staticClass: "font-12" }, [
                    _vm._v(" February 15, 2018 ")
                  ])
                ])
              ]),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "btn btn-info text-white",
                  attrs: { href: "javascript:;" }
                },
                [_c("i", { staticClass: "fa fa-edit" })]
              ),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "btn btn-warning text-white",
                  attrs: { href: "javascript:;" }
                },
                [_c("i", { staticClass: "fa fa-trash" })]
              )
            ]
          )
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-68ae35cf", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-68ae35cf\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/test-project/moderate-test-projects.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-68ae35cf\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/test-project/moderate-test-projects.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("09473095", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-68ae35cf\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./moderate-test-projects.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-68ae35cf\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./moderate-test-projects.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/assets/img/bin_18x18.png":
/***/ (function(module, exports) {

module.exports = "/images/bin_18x18.png?ab62865331bd6d9e0406f4e24ef6627b";

/***/ }),

/***/ "./resources/assets/assets/img/edit_18x18.png":
/***/ (function(module, exports) {

module.exports = "/images/edit_18x18.png?a324553d85fa702694c7317bc177aef4";

/***/ }),

/***/ "./resources/assets/components/pages/test-project/moderate-test-projects.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-68ae35cf\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/test-project/moderate-test-projects.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/test-project/moderate-test-projects.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-68ae35cf\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/test-project/moderate-test-projects.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-68ae35cf"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\test-project\\moderate-test-projects.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-68ae35cf", Component.options)
  } else {
    hotAPI.reload("data-v-68ae35cf", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});