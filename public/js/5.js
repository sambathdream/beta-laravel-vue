webpackJsonp([5],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/packages/form.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__("./node_modules/vue/dist/vue.common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_form__ = __webpack_require__("./node_modules/vue-form/dist/vue-form.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_form___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_vue_form__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_src_validations_validations_js__ = __webpack_require__("./resources/assets/validations/validations.js");
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




__WEBPACK_IMPORTED_MODULE_0_vue___default.a.use(__WEBPACK_IMPORTED_MODULE_1_vue_form___default.a, __WEBPACK_IMPORTED_MODULE_2_src_validations_validations_js__["a" /* default */]);

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "packageForm",
  data: function data() {
    return {
      discount: "no",
      formstate: {},
      model: {},
      errors: null
    };
  },

  methods: {
    onSubmit: function onSubmit() {
      var _this = this;

      if (this.formstate.$invalid) {
        return;
      } else {
        var requestData = _extends({}, this.model);
        var p = void 0;
        if (this.model.id) {
          p = axios.put("/api/packages/" + this.model.id, requestData);
        } else {
          p = axios.post("/api/packages", requestData);
        }
        p.then(function (response) {
          _this.$router.push({ name: "admin.packages.list" });
        }).catch(function (error) {
          if (Array.isArray(error.response.data.message)) {
            _this.errors = error.response.data.message;
          } else {
            _this.errors = [error.response.data.message];
          }
        });
      }
    }
  },
  mounted: function mounted() {
    var _this2 = this;

    if (this.$route.params.id) {
      axios.get("/api/packages/" + this.$route.params.id).then(function (_ref) {
        var data = _ref.data.data;

        _this2.model = data;
        if (_this2.model.discount_after_number_of_tester) {
          _this2.discount = "yes";
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3b4dfcd9\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/packages/form.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.custom-container[data-v-3b4dfcd9] {\n  max-width: 100%;\n  padding: 0;\n}\n.text-blue[data-v-3b4dfcd9] {\n  color: #0082cc;\n}\n.text-red[data-v-3b4dfcd9] {\n  color: #e63423;\n}\n.text-green[data-v-3b4dfcd9] {\n  color: #2cac3d;\n}\n.text-purple[data-v-3b4dfcd9] {\n  color: #3e3a94;\n}\n.text-bold[data-v-3b4dfcd9] {\n  font-family: \"BrandonTextBold\";\n}\n.text-medium[data-v-3b4dfcd9] {\n  font-family: \"BrandonTextMedium\";\n}\n.page-title[data-v-3b4dfcd9] {\n  font-family: \"UniNeueBold\";\n  font-size: 30px;\n  width: 100%;\n  text-align: center;\n  color: #363e48;\n  margin: 5px 0 25px;\n}\n.checkbox label[data-v-3b4dfcd9]:after,\n.radio label[data-v-3b4dfcd9]:after {\n  content: \"\";\n  display: table;\n  clear: both;\n}\n.checkbox .cr[data-v-3b4dfcd9],\n.radio .cr[data-v-3b4dfcd9] {\n  position: relative;\n  display: inline-block;\n  border: 1px solid #3e3a94;\n  border-radius: 0.25em;\n  width: 1.3em;\n  height: 1.3em;\n  float: left;\n  margin-right: 0.5em;\n  color: #3e3a94;\n}\n.radio .cr[data-v-3b4dfcd9] {\n  border-radius: 50%;\n}\n.checkbox .cr .cr-icon[data-v-3b4dfcd9],\n.radio .cr .cr-icon[data-v-3b4dfcd9] {\n  position: absolute;\n  font-size: 0.8em;\n  line-height: 0;\n  top: 50%;\n  left: 20%;\n}\n.radio .cr .cr-icon[data-v-3b4dfcd9] {\n  margin-left: 0.04em;\n}\n.checkbox label input[type=\"checkbox\"][data-v-3b4dfcd9],\n.radio label input[type=\"radio\"][data-v-3b4dfcd9] {\n  display: none;\n}\n.checkbox label input[type=\"checkbox\"] + .cr > .cr-icon[data-v-3b4dfcd9],\n.radio label input[type=\"radio\"] + .cr > .cr-icon[data-v-3b4dfcd9] {\n  -webkit-transform: scale(3) rotateZ(-20deg);\n          transform: scale(3) rotateZ(-20deg);\n  opacity: 0;\n  -webkit-transition: all 0.3s ease-in;\n  transition: all 0.3s ease-in;\n}\n.checkbox label input[type=\"checkbox\"]:checked + .cr > .cr-icon[data-v-3b4dfcd9],\n.radio label input[type=\"radio\"]:checked + .cr > .cr-icon[data-v-3b4dfcd9] {\n  -webkit-transform: scale(1) rotateZ(0deg);\n          transform: scale(1) rotateZ(0deg);\n  opacity: 1;\n}\n.checkbox label input[type=\"checkbox\"]:disabled + .cr[data-v-3b4dfcd9],\n.radio label input[type=\"radio\"]:disabled + .cr[data-v-3b4dfcd9] {\n  opacity: 0.5;\n}\n.packages-page[data-v-3b4dfcd9] {\n  width: 100%;\n}\n.packages-page .white-box[data-v-3b4dfcd9] {\n    background-color: #fff;\n    -webkit-box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n            box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n    border-radius: 4px;\n    padding: 20px;\n    margin-bottom: 20px;\n    font-size: 15px;\n    color: #606368;\n}\n.packages-page .white-box .block-subtitle[data-v-3b4dfcd9] {\n      font-family: \"BrandonTextBold\";\n      font-size: 20px;\n      color: #3e3a94;\n      border-bottom: 1px solid #dadada;\n      padding-bottom: 10px;\n      margin-bottom: 15px;\n}\n.packages-page .white-box .custom-form-control[data-v-3b4dfcd9] {\n      font-size: 15px;\n      border-radius: 4px;\n      padding: 2px 8px;\n}\n.packages-page .white-box .custom-form-control[data-v-3b4dfcd9]:hover {\n        outline: none !important;\n}\n.packages-page .white-box .custom-form-control[data-v-3b4dfcd9]:focus {\n        outline: none !important;\n}\n.purple-btn[data-v-3b4dfcd9] {\n  cursor: pointer;\n  width: auto;\n  background-color: #5651b9;\n  border: 2px solid #3e3a94;\n  color: #fff;\n  font-size: 15px;\n  line-height: 16px;\n  font-family: \"BrandonTextMedium\";\n  border-radius: 20px !important;\n  padding: 6px 20px;\n  -webkit-transition: all 0.4s ease;\n  transition: all 0.4s ease;\n  display: block;\n  text-align: center;\n  max-width: 150px;\n  margin: 0 auto;\n}\n.purple-btn[data-v-3b4dfcd9]:hover {\n    outline: none !important;\n    background-color: #3e3a94;\n    color: #fff;\n}\n.purple-btn[data-v-3b4dfcd9]:focus {\n    outline: none !important;\n    color: #fff;\n}\n@media screen and (max-width: 1281px) {\n.packages-page .white-box[data-v-3b4dfcd9] {\n    font-size: 16px;\n}\n.packages-page .purple-btn[data-v-3b4dfcd9] {\n    padding: 6px;\n}\n}\n@media screen and (max-width: 1200px) {\n.page-title[data-v-3b4dfcd9] {\n    font-size: 28px;\n    margin: 0 0 20px;\n}\n.packages-page .white-box[data-v-3b4dfcd9] {\n    font-size: 14px;\n}\n.packages-page .white-box .block-title[data-v-3b4dfcd9] {\n      font-size: 18px;\n}\n}\n@media screen and (max-width: 991px) {\n.custom-container .packages-page .white-box[data-v-3b4dfcd9] {\n    padding: 15px;\n}\n}\n@media screen and (max-width: 575px) {\n.custom-container[data-v-3b4dfcd9] {\n    padding: 0;\n}\n.custom-container .page-title[data-v-3b4dfcd9] {\n      font-size: 26px;\n      margin: 0 0 20px;\n}\n.custom-container .packages-page .purple-btn[data-v-3b4dfcd9] {\n      margin-top: 10px;\n}\n.custom-container .packages-page .white-box[data-v-3b4dfcd9] {\n      font-size: 14px;\n      line-height: 18px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-3b4dfcd9\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/packages/form.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { attrs: { id: "scroll-top" } }, [
    _c("div", { staticClass: "container custom-container" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-12" }, [
          _c(
            "h1",
            { staticClass: "page-title" },
            [
              _vm._v("\n               Packages\n              "),
              _c(
                "router-link",
                {
                  staticClass: "btn purple-btn text-uppercase",
                  staticStyle: { float: "right" },
                  attrs: { to: { name: "admin.packages.list" } }
                },
                [_vm._v("Back")]
              )
            ],
            1
          )
        ])
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "packages-page" },
        [
          _c(
            "vue-form",
            {
              staticClass: "form-horizontal",
              attrs: { state: _vm.formstate },
              on: {
                submit: function($event) {
                  $event.preventDefault()
                  _vm.onSubmit($event)
                }
              }
            },
            [
              _c("div", { staticClass: "white-box" }, [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-12" }, [
                    _c("h4", { staticClass: "block-subtitle" }, [
                      _vm._v(
                        "\n                        Package Details\n                        "
                      )
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-lg-12" }, [
                    _vm.errors
                      ? _c("div", { staticStyle: { color: "red" } }, [
                          _c(
                            "ul",
                            { attrs: { id: "example-1" } },
                            _vm._l(_vm.errors, function(error) {
                              return _c("li", { key: error }, [
                                _vm._v(
                                  "\n                                  " +
                                    _vm._s(error) +
                                    "\n                              "
                                )
                              ])
                            })
                          )
                        ])
                      : _vm._e()
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _c(
                    "div",
                    { staticClass: "col-md-12" },
                    [
                      _c(
                        "validate",
                        { staticClass: "row pb-3", attrs: { tag: "div" } },
                        [
                          _c(
                            "div",
                            { staticClass: "col-md-6 mb-2 mb-sm-2 mb-md-1" },
                            [_c("span", [_vm._v("Package Name :")])]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "col-md-6" },
                            [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.model.name,
                                    expression: "model.name"
                                  }
                                ],
                                staticClass:
                                  "form-control custom-form-control ",
                                attrs: {
                                  type: "text",
                                  name: "name",
                                  required: "",
                                  autofocus: "",
                                  placeholder: "Enter Package Name"
                                },
                                domProps: { value: _vm.model.name },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.model,
                                      "name",
                                      $event.target.value
                                    )
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "field-messages",
                                {
                                  staticClass: "text-danger",
                                  attrs: {
                                    name: "name",
                                    show: "$invalid && $submitted"
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      attrs: { slot: "required" },
                                      slot: "required"
                                    },
                                    [_vm._v("Please enter package name")]
                                  )
                                ]
                              )
                            ],
                            1
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "validate",
                        { staticClass: "row pb-3", attrs: { tag: "div" } },
                        [
                          _c(
                            "div",
                            { staticClass: "col-md-6 mb-2 mb-sm-2 mb-md-1" },
                            [
                              _c("span", [
                                _vm._v(
                                  "For first project, charge of per Tester is (In USD) :"
                                )
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "col-md-6" },
                            [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value:
                                      _vm.model.first_project_per_tester_charge,
                                    expression:
                                      "model.first_project_per_tester_charge"
                                  }
                                ],
                                staticClass:
                                  "form-control custom-form-control ",
                                attrs: {
                                  type: "text",
                                  placeholder: "",
                                  name: "first_project_per_tester_charge",
                                  required: ""
                                },
                                domProps: {
                                  value:
                                    _vm.model.first_project_per_tester_charge
                                },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.model,
                                      "first_project_per_tester_charge",
                                      $event.target.value
                                    )
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "field-messages",
                                {
                                  staticClass: "text-danger",
                                  attrs: {
                                    name: "first_project_per_tester_charge",
                                    show: "$invalid && $submitted"
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      attrs: { slot: "required" },
                                      slot: "required"
                                    },
                                    [
                                      _vm._v(
                                        "Please enter per tester charge in USD"
                                      )
                                    ]
                                  )
                                ]
                              )
                            ],
                            1
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "validate",
                        { staticClass: "row pb-3", attrs: { tag: "div" } },
                        [
                          _c(
                            "div",
                            { staticClass: "col-md-6 mb-2 mb-sm-2 mb-md-1" },
                            [
                              _c("span", [
                                _vm._v(
                                  "For second project onward, charge of per Tester is (In USD) :"
                                )
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "col-md-6" },
                            [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.model.regular_per_tester_charge,
                                    expression:
                                      "model.regular_per_tester_charge"
                                  }
                                ],
                                staticClass:
                                  "form-control custom-form-control ",
                                attrs: {
                                  type: "text",
                                  placeholder: "",
                                  name: "regular_per_tester_charge",
                                  required: ""
                                },
                                domProps: {
                                  value: _vm.model.regular_per_tester_charge
                                },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.model,
                                      "regular_per_tester_charge",
                                      $event.target.value
                                    )
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "field-messages",
                                {
                                  staticClass: "text-danger",
                                  attrs: {
                                    name: "regular_per_tester_charge",
                                    show: "$invalid && $submitted"
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      attrs: { slot: "required" },
                                      slot: "required"
                                    },
                                    [
                                      _vm._v(
                                        "Please enter per tester charge after first project in USD"
                                      )
                                    ]
                                  )
                                ]
                              )
                            ],
                            1
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "row pb-3" }, [
                        _c(
                          "div",
                          { staticClass: "col-md-6 mb-2 mb-sm-2 mb-md-1" },
                          [
                            _c("span", [
                              _vm._v(
                                "Do you want to give discount in this package? "
                              )
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c("div", { staticClass: "col-md-6" }, [
                          _c(
                            "div",
                            { staticClass: "radio form-check-inline mr-5" },
                            [
                              _c("label", [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.discount,
                                      expression: "discount"
                                    }
                                  ],
                                  attrs: {
                                    type: "radio",
                                    name: "o3",
                                    value: "yes"
                                  },
                                  domProps: {
                                    checked: _vm._q(_vm.discount, "yes")
                                  },
                                  on: {
                                    change: function($event) {
                                      _vm.discount = "yes"
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _c("span", { staticClass: "cr" }, [
                                  _c("i", {
                                    staticClass: "cr-icon fa fa-circle"
                                  })
                                ]),
                                _vm._v(
                                  "\n                            Yes\n                          "
                                )
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "radio form-check-inline" },
                            [
                              _c("label", [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.discount,
                                      expression: "discount"
                                    }
                                  ],
                                  attrs: {
                                    type: "radio",
                                    name: "o3",
                                    value: "no",
                                    checked: ""
                                  },
                                  domProps: {
                                    checked: _vm._q(_vm.discount, "no")
                                  },
                                  on: {
                                    change: function($event) {
                                      _vm.discount = "no"
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _c("span", { staticClass: "cr" }, [
                                  _c("i", {
                                    staticClass: "cr-icon fa fa-circle"
                                  })
                                ]),
                                _vm._v(
                                  "\n                          No\n                        "
                                )
                              ])
                            ]
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _vm.discount == "yes"
                        ? _c("div", [
                            _c("div", { staticClass: "row pb-3" }, [
                              _c(
                                "div",
                                { staticClass: "col-md-6 mb-2 mb-sm-0" },
                                [
                                  _c("span", [
                                    _vm._v(
                                      "Discount applied after Testers greater than :"
                                    )
                                  ])
                                ]
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "col-md-6" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value:
                                        _vm.model
                                          .discount_after_number_of_tester,
                                      expression:
                                        "model.discount_after_number_of_tester"
                                    }
                                  ],
                                  staticClass:
                                    "form-control custom-form-control ",
                                  attrs: { type: "number", placeholder: "" },
                                  domProps: {
                                    value:
                                      _vm.model.discount_after_number_of_tester
                                  },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.model,
                                        "discount_after_number_of_tester",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "row pb-3" }, [
                              _c(
                                "div",
                                { staticClass: "col-md-6 mb-2 mb-sm-0" },
                                [
                                  _c("span", [
                                    _vm._v("What will be discount type :")
                                  ])
                                ]
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "col-md-6" }, [
                                _c(
                                  "div",
                                  {
                                    staticClass: "radio form-check-inline mr-5"
                                  },
                                  [
                                    _c("label", [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.model.discount_type,
                                            expression: "model.discount_type"
                                          }
                                        ],
                                        attrs: {
                                          type: "radio",
                                          name: "discount",
                                          checked: ""
                                        },
                                        domProps: {
                                          value: 1,
                                          checked: _vm._q(
                                            _vm.model.discount_type,
                                            1
                                          )
                                        },
                                        on: {
                                          change: function($event) {
                                            _vm.$set(
                                              _vm.model,
                                              "discount_type",
                                              1
                                            )
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("span", { staticClass: "cr" }, [
                                        _c("i", {
                                          staticClass: "cr-icon fa fa-circle"
                                        })
                                      ]),
                                      _vm._v(
                                        "\n                                  Percentage\n                                "
                                      )
                                    ])
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "radio form-check-inline" },
                                  [
                                    _c("label", [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.model.discount_type,
                                            expression: "model.discount_type"
                                          }
                                        ],
                                        attrs: {
                                          type: "radio",
                                          name: "discount"
                                        },
                                        domProps: {
                                          value: 2,
                                          checked: _vm._q(
                                            _vm.model.discount_type,
                                            2
                                          )
                                        },
                                        on: {
                                          change: function($event) {
                                            _vm.$set(
                                              _vm.model,
                                              "discount_type",
                                              2
                                            )
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("span", { staticClass: "cr" }, [
                                        _c("i", {
                                          staticClass: "cr-icon fa fa-circle"
                                        })
                                      ]),
                                      _vm._v(
                                        "\n                                Amount\n                              "
                                      )
                                    ])
                                  ]
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "row pb-3" }, [
                              _c(
                                "div",
                                { staticClass: "col-md-6 mb-2 mb-sm-0" },
                                [
                                  _c("span", [
                                    _vm._v(
                                      "What will be discount amount/percentage :"
                                    )
                                  ])
                                ]
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "col-md-6" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.model.discount_amount,
                                      expression: "model.discount_amount"
                                    }
                                  ],
                                  staticClass:
                                    "form-control custom-form-control ",
                                  attrs: { type: "number", placeholder: "" },
                                  domProps: {
                                    value: _vm.model.discount_amount
                                  },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.model,
                                        "discount_amount",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ])
                            ])
                          ])
                        : _vm._e()
                    ],
                    1
                  )
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-12 text-center" }, [
                  _c(
                    "button",
                    {
                      staticClass: "btn purple-btn text-uppercase",
                      attrs: { type: "submit" }
                    },
                    [_vm._v("save package")]
                  )
                ])
              ])
            ]
          )
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-3b4dfcd9", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3b4dfcd9\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/packages/form.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3b4dfcd9\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/packages/form.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("7aa68b24", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3b4dfcd9\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./form.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3b4dfcd9\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./form.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/components/pages/packages/form.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3b4dfcd9\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/packages/form.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/packages/form.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-3b4dfcd9\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/packages/form.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-3b4dfcd9"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\packages\\form.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3b4dfcd9", Component.options)
  } else {
    hotAPI.reload("data-v-3b4dfcd9", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});