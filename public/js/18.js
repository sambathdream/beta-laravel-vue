webpackJsonp([18],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/device-selector.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "DeviceSelector",
  props: {
    disabled: {
      type: Boolean,
      defualt: function defualt() {
        return false;
      }
    },
    value: {
      type: Array,
      required: true,
      default: function _default() {
        return [];
      }
    }
  },
  data: function data() {
    return {
      devices: []
    };
  },

  methods: {
    toggleItem: function toggleItem(device) {
      if (this.disabled) {
        return true;
      }
      var index = this.value.indexOf(device.id);
      var newDevices = [];
      if (index >= 0) {
        newDevices = this.value.filter(function (i) {
          return i !== device.id;
        });
      } else {
        newDevices = [].concat(_toConsumableArray(this.value), [device.id]);
      }
      this.$emit("input", newDevices);
      this.$emit("change", newDevices);
    }
  },
  mounted: function mounted() {
    var _this = this;

    if (!this.$store.state.devices.length) {
      axios.get("/api/device").then(function (_ref) {
        var data = _ref.data;

        _this.$store.commit("set_devices", data);
        _this.devices = data;
      });
    } else {
      this.devices = this.$store.state.devices;
    }
  },
  computed: {
    availableDevices: function availableDevices() {
      var _this2 = this;

      if (this.disabled) {
        return this.devices.filter(function (_ref2) {
          var id = _ref2.id;
          return _this2.value.indexOf(id) >= 0;
        });
      }
      return this.devices;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/project-testing-process-review.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__("./node_modules/vue/dist/vue.common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment__ = __webpack_require__("./node_modules/moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_components_components_device_selector__ = __webpack_require__("./resources/assets/components/components/device-selector.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_components_components_device_selector___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_components_components_device_selector__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_components_components_project_rating_vue__ = __webpack_require__("./resources/assets/components/components/project/rating.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_components_components_project_rating_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_components_components_project_rating_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  name: "project_testing_process_review",
  components: {
    starRatings: __WEBPACK_IMPORTED_MODULE_3_components_components_project_rating_vue___default.a,
    DeviceSelector: __WEBPACK_IMPORTED_MODULE_2_components_components_device_selector___default.a
  },
  data: function data() {
    return {
      testProject: {},
      user: {},
      ratings: {},
      projectTester: {},
      answer_map: {},
      totalHighTypeIssues: 2,
      totalMediumTypeIssues: 2,
      totalLowTypeIssues: 1,
      totalCosmeticTypeIssues: 1,
      totalOtherTypeIssues: 1,
      seen: false,
      issues: {
        high: {},
        medium: {},
        low: {},
        cosmetic: {},
        other: {}
      }
    };
  },

  computed: {
    currentTestStatus: function currentTestStatus() {
      if (this.projectTester && this.projectTester.latest_status_data) {
        if (this.projectTester.latest_status.toLowerCase() === "submitted") {
          return "In Review, Submitted: " + __WEBPACK_IMPORTED_MODULE_1_moment___default()(this.projectTester.latest_status_data.updated_at).format("DD MMM, YYYY HH:mm A");
        }
        return this.projectTester.latest_status;
      }
      // @TODO when user is not associated with test project this should be status of project itself.
      return "Running / Open";
    }
  },
  mounted: function mounted() {
    var _this = this;

    axios.get("/api/tester/user/" + this.$route.params.id).then(function (_ref) {
      var data = _ref.data.data;

      _this.projectTester = data;
      if (_this.groupedIssues) {
        _this.issues = data.groupedIssues;
      }
    }).catch(function (error) {});
    axios.get("/api/projects/" + this.$route.params.id).then(function (_ref2) {
      var data = _ref2.data.data;

      _this.testProject = data;
      _this.testProject.selected_devices = data.devices.map(function (_ref3) {
        var id = _ref3.id;
        return id;
      });
      return axios.get("/api/projects/" + _this.testProject.id + "/my-ratings");
    }).then(function (_ref4) {
      var data = _ref4.data;

      var groups = _.groupBy(data, "collection_name");
      var ratings = {};
      Object.keys(groups).forEach(function (k) {
        return ratings[k] = _.pick(groups[k][0], ["score", "suggestion"]);
      });
      _this.ratings = ratings;
    }).catch(function (error) {});
  },
  methods: {
    removeField: function removeField(type, event) {
      if (type == "high") {
        this.totalHighTypeIssues--;
      } else if (type == "medium") {
        this.totalMediumTypeIssues--;
      } else if (type == "low") {
        this.totalLowTypeIssues--;
      } else if (type == "cosmetic") {
        this.totalCosmeticTypeIssues--;
      } else if (type == "other") {
        this.totalOtherTypeIssues--;
      } else {}
    },
    addField: function addField(type, event) {
      if (type == "high") {
        this.totalHighTypeIssues++;
      } else if (type == "medium") {
        this.totalMediumTypeIssues++;
      } else if (type == "low") {
        this.totalLowTypeIssues++;
      } else if (type == "cosmetic") {
        this.totalCosmeticTypeIssues++;
      } else if (type == "other") {
        this.totalOtherTypeIssues++;
      } else {}
    },
    addexplanation: function addexplanation(event) {
      console.log(event.target);
    },
    reviewProject: function reviewProject(event) {
      var _this2 = this;

      axios.post("/api/tester/update-test-status/" + this.$route.params.id, {
        test_status: "REVIEW",
        issues: this.issues,
        project_id: this.$route.params.id
      }).then(function (response) {
        if (response.data.success) {
          _this2.$router.push({ name: "tester.project-view-submission" });
        }
      }).catch(function (error) {});
    },
    saveData: function saveData() {
      var _this3 = this;

      var requestData = {
        email: this.$store.state.user.email,
        project_question_answers: this.answer_map,
        issues: this.issues,
        project_id: this.$route.params.id
      };
      axios.put("/api/tester/" + this.$store.state.user.id, requestData).then(function (response) {
        _this3.$router.push({ name: "tester.dashboard" });
      }).catch(function (error) {
        return _this3.errors = error.response.data.message;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/project/rating.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "ProjectRating",
  props: {
    title: {
      default: function _default() {
        return "Visual Accuracy";
      }
    },
    maxRating: {
      default: function _default() {
        return 5;
      }
    },
    value: {
      default: function _default() {
        return {
          score: 0,
          suggestion: ""
        };
      }
    },
    disabled: {
      default: function _default() {
        return false;
      }
    }
  },
  data: function data() {
    return {
      add_suggestion: false,
      ratingValue: {
        score: 0,
        suggestion: ""
      }
    };
  },

  methods: {
    emitChange: function emitChange() {
      this.$emit("input", this.ratingValue);
      this.$emit("change", this.ratingValue);
    },
    addSuggestion: function addSuggestion(e) {
      this.ratingValue.suggestion = e.target.value;
      this.emitChange();
    },
    setRating: function setRating(v) {
      if (this.disabled) {
        return true;
      }
      this.ratingValue.score = v;
    },
    selected: function selected(v) {
      if (this.disabled) {
        return true;
      }
      this.setRating(v);
      this.emitChange();
    }
  },
  computed: {
    starBlocks: function starBlocks() {
      return Array.from({ length: this.maxRating }).map(function (_, i) {
        return i + 1;
      });
    },
    ratingFillColor: function ratingFillColor() {
      if (this.ratingValue.score === 1) return "#F83636";
      if (this.ratingValue.score === 2) return "#FF8A0D";
      if (this.ratingValue.score === 3) return "#FFC21F";
      if (this.ratingValue.score === 4) return "#73DF23";
      if (this.ratingValue.score === 5) return "#02C506";
    },
    ratingItemStyle: function ratingItemStyle() {
      var baseStyle = {};
      if (!this.disabled) {
        baseStyle.cursor = "pointer";
      }
      if (this.ratingValue.score) {
        return _extends({}, baseStyle, {
          color: "white",
          background: this.ratingFillColor
        });
      }
      return _extends({}, baseStyle);
    }
  },
  mounted: function mounted() {
    this.ratingValue = _extends({}, this.value);
  },

  watch: {
    value: function value(newVal, oldVal) {
      this.ratingValue = _extends({}, newVal);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/publisher/file-viewer.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "FileViewer",
  props: {
    files: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    colorTheme: {
      type: String,
      default: function _default() {
        return 'publisher';
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/publisher/steps.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Steps",
  props: {
    steps: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    active: {
      type: Number,
      default: function _default() {
        return 1;
      }
    },
    projectTypeClass: {
      type: String,
      default: function _default() {
        return 'paid-project';
      }
    }
  },
  methods: {
    isCompleted: function isCompleted(id) {
      if (id <= this.active) {
        return true;
      }
      return false;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/tester/project-status-test-project.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "ProjectStatusTestProject"
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/tester/test-project-details.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_components_components_device_selector__ = __webpack_require__("./resources/assets/components/components/device-selector.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_components_components_device_selector___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_components_components_device_selector__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_components_components_publisher_file_viewer__ = __webpack_require__("./resources/assets/components/components/publisher/file-viewer.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_components_components_publisher_file_viewer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_components_components_publisher_file_viewer__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "TestProjectDetails",
  components: {
    DeviceSelector: __WEBPACK_IMPORTED_MODULE_0_components_components_device_selector___default.a,
    FileViewer: __WEBPACK_IMPORTED_MODULE_1_components_components_publisher_file_viewer___default.a
  },
  props: {
    testProject: {
      type: Object,
      default: function _default() {
        return {};
      }
    },
    currentTestStatus: {
      type: String,
      default: function _default() {
        return '';
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/tester/project-submission.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__("./node_modules/vue/dist/vue.common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment__ = __webpack_require__("./node_modules/moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_components_components_device_selector__ = __webpack_require__("./resources/assets/components/components/device-selector.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_components_components_device_selector___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_components_components_device_selector__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_components_components_project_rating_vue__ = __webpack_require__("./resources/assets/components/components/project/rating.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_components_components_project_rating_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_components_components_project_rating_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_components_components_publisher_steps__ = __webpack_require__("./resources/assets/components/components/publisher/steps.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_components_components_publisher_steps___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_components_components_publisher_steps__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_components_components_tester_test_project_details__ = __webpack_require__("./resources/assets/components/components/tester/test-project-details.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_components_components_tester_test_project_details___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_components_components_tester_test_project_details__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_components_components_tester_project_status_test_project__ = __webpack_require__("./resources/assets/components/components/tester/project-status-test-project.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_components_components_tester_project_status_test_project___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_components_components_tester_project_status_test_project__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_components_components_project_testing_process_review__ = __webpack_require__("./resources/assets/components/components/project-testing-process-review.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_components_components_project_testing_process_review___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_components_components_project_testing_process_review__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_src_services_test_project__ = __webpack_require__("./resources/assets/services/test-project.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_src_assets_img_black_widow_32x32_png__ = __webpack_require__("./resources/assets/assets/img/black_widow_32x32.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_src_assets_img_black_widow_32x32_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_src_assets_img_black_widow_32x32_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_src_assets_img_gnat_32x32_png__ = __webpack_require__("./resources/assets/assets/img/gnat_32x32.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_src_assets_img_gnat_32x32_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_src_assets_img_gnat_32x32_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_src_assets_img_wasp_32x32_png__ = __webpack_require__("./resources/assets/assets/img/wasp_32x32.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_src_assets_img_wasp_32x32_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_src_assets_img_wasp_32x32_png__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//














var VueScrollTo = __webpack_require__("./node_modules/vue-scrollto/vue-scrollto.js");
__WEBPACK_IMPORTED_MODULE_0_vue___default.a.use(VueScrollTo, {
  container: "body",
  duration: 500,
  easing: "ease",
  offset: -100,
  cancelable: true,
  onDone: false,
  onCancel: false,
  x: false,
  y: true
});
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "project_submission",
  components: {
    starRatings: __WEBPACK_IMPORTED_MODULE_3_components_components_project_rating_vue___default.a,
    DeviceSelector: __WEBPACK_IMPORTED_MODULE_2_components_components_device_selector___default.a,
    Steps: __WEBPACK_IMPORTED_MODULE_4_components_components_publisher_steps___default.a,
    TestProjectDetails: __WEBPACK_IMPORTED_MODULE_5_components_components_tester_test_project_details___default.a,
    ProjectStatusTestProject: __WEBPACK_IMPORTED_MODULE_6_components_components_tester_project_status_test_project___default.a,
    ProjectTestingProcessReview: __WEBPACK_IMPORTED_MODULE_7_components_components_project_testing_process_review___default.a
  },
  data: function data() {
    return {
      isSubmitted: 0,
      testproject: {},
      user: {},
      projectTester: {},
      ratings: {},
      answer_map: {},
      bugEmpty: false,
      issueTypes: [{
        img: __WEBPACK_IMPORTED_MODULE_9_src_assets_img_black_widow_32x32_png___default.a,
        name: "High",
        title: "Black Widow -  High type bug",
        typeDesc: "The issue(s) is critical / deadly to the app usability and must be dealt with immediately.",
        bugs: [{ id: 1, desc: "" }]
      }, {
        img: __WEBPACK_IMPORTED_MODULE_11_src_assets_img_wasp_32x32_png___default.a,
        name: "Medium",
        title: "Wasp -  Medium type bug",
        typeDesc: "The issue(s) affects the flow / enjoyment of the app but can wait until the next update to be solved.",
        bugs: [{ id: 1, desc: "" }]
      }, {
        img: __WEBPACK_IMPORTED_MODULE_10_src_assets_img_gnat_32x32_png___default.a,
        name: "Low",
        title: "Gnat - Low type bug",
        typeDesc: "The issue(s) is annoying but not critical to app function or flow.",
        bugs: [{ id: 1, desc: "" }]
      }, {
        img: "",
        name: "Cosmetic",
        title: "Are there any cosmetic issues you would like to mention?",
        typeDesc: "",
        bugs: [{ id: 1, desc: "" }]
      }, {
        img: "",
        name: "Other",
        title: "Do you have any suggestions you would like to pass on to the Publisher?",
        typeDesc: "",
        bugs: [{ id: 1, desc: "" }]
      }],
      steps: [{ id: 1, name: "New" }, { id: 2, name: "Accepted/Working" }, { id: 3, name: "Submitted" }, { id: 4, name: "Under Review" }, { id: 5, name: "Completed" }]
    };
  },

  computed: {
    currentTestStatus: function currentTestStatus() {
      if (this.projectTester && this.projectTester.latest_status_data) {
        if (this.projectTester.latest_status.toLowerCase() === "submitted") {
          return "In Review, Submitted: " + __WEBPACK_IMPORTED_MODULE_1_moment___default()(this.projectTester.latest_status_data.updated_at).format("DD MMM, YYYY HH:mm A");
        }
        return this.projectTester.latest_status;
      }
      // @TODO when user is not associated with test project this should be status of project itself.
      return "Running / Open";
    },
    get_project_current_stage: function get_project_current_stage() {
      if (this.projectTester && this.projectTester.latest_status) {
        return Object(__WEBPACK_IMPORTED_MODULE_8_src_services_test_project__["a" /* testProjectStatusWeight */])(this.projectTester.latest_status);
      } else {
        return 1;
      }
    }
  },
  mounted: function mounted() {
    var _this = this;

    axios.get("/api/tester/user/" + this.$route.params.id).then(function (_ref) {
      var data = _ref.data.data;

      _this.projectTester = data;
      _this.isSubmitted = _this.projectTester.all_status.includes("Submitted");
      if (_this.groupedIssues) {
        _this.issues = data.groupedIssues;
      }
    }).catch(function (error) {});
    axios.get("/api/projects/" + this.$route.params.id).then(function (_ref2) {
      var data = _ref2.data.data;

      _this.testproject = data;
      _this.testproject.selected_devices = data.devices.map(function (_ref3) {
        var id = _ref3.id;
        return id;
      });
      return axios.get("/api/projects/" + _this.testproject.id + "/my-ratings");
    }).then(function (_ref4) {
      var data = _ref4.data;

      var groups = _.groupBy(data, "collection_name");
      var ratings = {};
      Object.keys(groups).forEach(function (k) {
        return ratings[k] = _.pick(groups[k][0], ["score", "suggestion"]);
      });
      _this.ratings = ratings;
    }).catch(function (error) {});
  },
  methods: {
    removeField: function removeField(issueTypeIndex, bugIndex, event) {
      this.issueTypes[issueTypeIndex].bugs.splice(bugIndex, 1);
    },
    addField: function addField(issueTypeIndex, bugIndex, event) {
      var bug = this.issueTypes[issueTypeIndex].bugs[bugIndex];
      // if( bug.desc == "" ){
      //   this.bugEmpty = true;
      //   VueScrollTo.scrollTo('.testing-process-tabwrap');
      // }else{
      //   this.bugEmpty = false;
      this.issueTypes[issueTypeIndex].bugs.push({
        desc: "",
        id: bugIndex++
      });
      // }
    },
    addexplanation: function addexplanation(event) {
      console.log(event.target);
    },
    save: function save() {
      var _this2 = this;

      var submitted = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      // this.validateBugs();
      var requestData = {
        email: this.$store.state.user.email,
        project_question_answers: this.answer_map,
        issues: this.issueTypes,
        project_id: this.$route.params.id
      };
      // if(!this.bugEmpty){
      this.saveRatings().then(function () {
        return axios.put("/api/tester/" + _this2.$store.state.user.id, requestData);
      }).then(function (response) {
        var route = submitted ? "tester.project-view-submission" : "tester.dashboard";
        _this2.$router.push({ name: route });
      }).catch(function (error) {
        return _this2.errors = error.response.data.message;
      });
      // }
    },
    saveRatings: function saveRatings() {
      return axios.post("/api/tester/" + this.$store.state.user.id + "/rating/" + this.testproject.id, this.ratings);
    }
    // validateBugs: function(){
    //   this.bugEmpty = false;
    //   this.issueTypes.forEach(bugType => {
    //     bugType.bugs.forEach( bug => {
    //       if(bug.desc == "" && !this.bugEmpty){
    //         this.bugEmpty = true;
    //       }
    //     });
    //   });
    //   if(this.bugEmpty){
    //     VueScrollTo.scrollTo('.testing-process-tabwrap');
    //   }
    // },

  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/device-selector.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\nimg[data-v-0d653902] {\n  height: 50px;\n  width: 50px;\n  display: inline-block;\n  margin-right: 10px;\n  float: left;\n  opacity: 0.5;\n  cursor: pointer;\n}\nimg.disabled[data-v-0d653902] {\n  cursor: auto;\n}\nimg.selected[data-v-0d653902] {\n  opacity: 1;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-259f8d43\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/project-submission.vue":
/***/ (function(module, exports, __webpack_require__) {

var escape = __webpack_require__("./node_modules/css-loader/lib/url/escape.js");
exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.custom-container[data-v-259f8d43] {\n  max-width: 100%;\n}\n.custom-container .text-green[data-v-259f8d43] {\n    color: #2cac3d;\n}\n.custom-container .text-red[data-v-259f8d43] {\n    color: #e63423;\n}\n.custom-container .text-bold[data-v-259f8d43] {\n    font-family: \"BrandonTextBold\" !important;\n}\n.custom-container .text-medium[data-v-259f8d43] {\n    font-family: \"BrandonTextMedium\";\n}\n.custom-container .test-proj-fillup-page[data-v-259f8d43] {\n    width: 100%;\n}\n.custom-container .test-proj-fillup-page .white-box[data-v-259f8d43] {\n      background-color: #fff;\n      -webkit-box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n              box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n      border-radius: 4px;\n      padding: 20px;\n      margin-bottom: 20px;\n      font-size: 14px;\n      line-height: 22px;\n      letter-spacing: 0;\n      color: #606368;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title[data-v-259f8d43] {\n        font-size: 18px;\n        font-family: \"UniNeueBold\";\n        border-bottom: 1px solid #dadada;\n        color: #606368;\n        padding-bottom: 10px;\n        margin-bottom: 10px;\n        width: auto;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title .badge-wrap[data-v-259f8d43] {\n          border: 1px solid #2cac3d;\n          background-color: #2cac3d;\n          color: #fff;\n          font-size: 16px;\n          right: -5px;\n          position: absolute;\n          border-top-left-radius: 16px;\n          border-bottom-left-radius: 16px;\n          padding: 2px 12px;\n          top: -5px;\n          font-family: \"BrandonTextRegular\";\n}\n.custom-container .test-proj-fillup-page .white-box .block-title.proj-name-wrap[data-v-259f8d43] {\n        color: #2cac3d;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title.proj-name-wrap .proj-name[data-v-259f8d43] {\n          font-family: \"UniNeueBold\";\n          font-size: 24px;\n          line-height: 18px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status[data-v-259f8d43] {\n        padding: 0;\n        padding-top: 8px;\n        overflow: hidden;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status br[data-v-259f8d43] {\n          display: none;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status:first-child .proj-progress[data-v-259f8d43]::before {\n          width: calc(50% - 30px);\n          left: 15px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status:last-child .proj-progress[data-v-259f8d43]::after {\n          width: calc(50% - 30px);\n          right: 15px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status .proj-progress[data-v-259f8d43] {\n          position: relative;\n          margin-bottom: 5px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status .proj-progress i[data-v-259f8d43] {\n            color: rgba(175, 177, 179, 0.5);\n            font-size: 22px;\n            line-height: 24px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status .proj-progress[data-v-259f8d43]::before {\n            content: \"\";\n            position: absolute;\n            height: 3px;\n            width: calc(50% - 5px);\n            background-color: #ececec;\n            left: -10px;\n            top: 11px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status .proj-progress[data-v-259f8d43]::after {\n            content: \"\";\n            position: absolute;\n            height: 3px;\n            width: calc(50% - 5px);\n            background-color: #ececec;\n            right: -10px;\n            top: 11px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.active[data-v-259f8d43] {\n        color: #2cac3d;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.active .proj-progress i[data-v-259f8d43] {\n          color: #2cac3d;\n          font-size: 34px;\n          line-height: 24px;\n          position: relative;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.active .proj-progress i[data-v-259f8d43]::after {\n            height: 15px;\n            width: 15px;\n            background-color: #2cac3d;\n            content: \"\";\n            border-radius: 50%;\n            position: absolute;\n            left: 7px;\n            top: 4px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.active .proj-progress[data-v-259f8d43]::before {\n          background-color: #2cac3d;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.done-step[data-v-259f8d43] {\n        color: #2cac3d;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.done-step .proj-progress i[data-v-259f8d43] {\n          color: #2cac3d;\n          font-size: 34px;\n          line-height: 24px;\n          position: relative;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.done-step .proj-progress i[data-v-259f8d43]::after {\n            height: 15px;\n            width: 15px;\n            background-color: #2cac3d;\n            content: \"\";\n            border-radius: 50%;\n            position: absolute;\n            left: 7px;\n            top: 4px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.done-step .proj-progress[data-v-259f8d43]::before {\n          background-color: #2cac3d;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.done-step .proj-progress[data-v-259f8d43]::after {\n          background-color: #2cac3d;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-desc[data-v-259f8d43] {\n        letter-spacing: 0.01rem;\n        margin-bottom: 5px;\n}\n.custom-container .test-proj-fillup-page .white-box .notes-list[data-v-259f8d43] {\n        padding-left: 15px;\n        margin-bottom: 0px;\n}\n.custom-container .test-proj-fillup-page .white-box .notes-list li[data-v-259f8d43] {\n          position: relative;\n}\n.custom-container .test-proj-fillup-page .white-box .notes-list li[data-v-259f8d43]::before {\n            content: \"-\";\n            position: absolute;\n            left: -15px;\n            top: -1px;\n            color: #606368;\n            font-size: 20px;\n}\n.custom-container .test-proj-fillup-page .white-box .project-link[data-v-259f8d43] {\n        color: #2cac3d;\n        text-decoration: underline !important;\n}\n.custom-container .test-proj-fillup-page .white-box .app-title[data-v-259f8d43] {\n        min-width: 110px;\n        display: inline-block;\n}\n.custom-container .test-proj-fillup-page .white-box .test-device-list span[data-v-259f8d43] {\n        margin-right: 15px;\n        margin-top: 10px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-issues-tab .line-seprator[data-v-259f8d43] {\n        margin: 0 0 20px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-issues-tab .issue-block .issue-type[data-v-259f8d43] {\n        margin-bottom: 0px;\n        font-size: 18px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-issues-tab .issue-block .form-group .form-control[data-v-259f8d43] {\n        margin-bottom: 20px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-issues-tab .issue-block .form-group .remove-icon[data-v-259f8d43] {\n        color: #606368;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-issues-tab .issue-block .form-group .remove-icon i[data-v-259f8d43] {\n          font-size: 34px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-attachment-tab .add-files-btn[data-v-259f8d43] {\n        width: 160px;\n        float: right;\n        margin-top: 0;\n        margin-bottom: 20px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-attachment-tab .add-files-btn input[data-v-259f8d43] {\n          opacity: 0;\n          position: absolute;\n          top: 0;\n          right: 0;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-attachment-tab .attachment-table-wrap .attachment-table[data-v-259f8d43] {\n        border-radius: 6px;\n        margin-bottom: 0;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-attachment-tab .attachment-table-wrap .attachment-table tr td[data-v-259f8d43]:first-child {\n          min-width: 60px;\n          max-width: 60px;\n          width: 60px;\n          text-align: center;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-attachment-tab .attachment-table-wrap .attachment-table tr td[data-v-259f8d43]:last-child {\n          min-width: 130px;\n          max-width: 130px;\n          width: 130px;\n          display: table-cell;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-attachment-tab .attachment-table-wrap .attachment-table tr td[data-v-259f8d43]:nth-child(3) {\n          min-width: 160px;\n          max-width: 160px;\n          width: 160px;\n          display: table-cell;\n          text-align: center;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-attachment-tab .attachment-table-wrap .attachment-table tr td a[data-v-259f8d43] {\n          margin-right: 10px;\n          color: #1f1f1f;\n          font-size: 20px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .total-score-txt[data-v-259f8d43] {\n        font-size: 20px;\n        font-family: \"UniNeueBold\";\n        color: #363e48;\n        text-align: center;\n        width: 100%;\n        margin-bottom: 15px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .total-score-txt .effective-badge[data-v-259f8d43] {\n          font-family: \"BrandonTextRegular\";\n          color: #fff;\n          font-size: 12px;\n          background-color: #4be05e;\n          border: 1px solid #2cac3d;\n          border-radius: 16px;\n          padding: 1px 8px;\n          vertical-align: middle;\n          margin-bottom: 5px;\n          display: inline-block;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion[data-v-259f8d43] {\n        border: none;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion[data-v-259f8d43]:hover {\n          -webkit-box-shadow: none;\n                  box-shadow: none;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header[data-v-259f8d43] {\n          padding: 0 !important;\n          border-bottom: none;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header[data-v-259f8d43]:hover {\n            outline: none !important;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a.collapsed[data-v-259f8d43] {\n            border: 2px solid #118921;\n            background-color: #2cac3d;\n            color: #fff;\n            border-radius: 6px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a.collapsed[data-v-259f8d43]:before {\n              background-image: url(" + escape(__webpack_require__("./resources/assets/assets/img/right-arrow.png")) + ");\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a.collapsed span[data-v-259f8d43] {\n              color: #fff;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a.collapsed img[data-v-259f8d43] {\n              display: inline-block;\n              vertical-align: initial;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a[data-v-259f8d43] {\n            padding: 10px 20px !important;\n            padding-left: 35px !important;\n            border-radius: 0px;\n            border-top-right-radius: 6px;\n            border-top-left-radius: 6px;\n            text-align: left;\n            color: #2cac3d;\n            font-family: \"UniNeueHeavy\";\n            font-size: 16px;\n            text-align: left;\n            text-transform: uppercase;\n            background-color: #f2f2f2;\n            border: none !important;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a[data-v-259f8d43]:before {\n              background-image: url(" + escape(__webpack_require__("./resources/assets/assets/img/down-arrow.png")) + ");\n              content: \"\";\n              position: absolute;\n              left: 15px;\n              top: 15px;\n              height: 14px;\n              width: 14px;\n              background-size: 100%;\n              background-position: center;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a span[data-v-259f8d43] {\n              float: right;\n              font-family: \"BrandonTextBold\";\n              font-size: 16px;\n              text-transform: none;\n              color: #2f2f2f;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-body[data-v-259f8d43] {\n          padding: 0 10px 10px;\n          background-color: #f2f2f2;\n          border-bottom-left-radius: 6px;\n          border-bottom-right-radius: 6px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap[data-v-259f8d43] {\n            background-color: #fff;\n            border: 1px solid #dadada;\n            padding: 15px;\n            border-radius: 6px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap h4[data-v-259f8d43] {\n              font-size: 18px;\n              color: #2cac3d;\n              font-family: \"BrandonTextBold\";\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap .detail-block[data-v-259f8d43] {\n              font-family: \"BrandonTextMedium\";\n              font-size: 14px;\n              line-height: 16px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap .detail-block span[data-v-259f8d43] {\n                width: calc(100% / 3 - 5px);\n                display: inline-block;\n                vertical-align: top;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap .explanation-wrap textarea[data-v-259f8d43] {\n              resize: none;\n              padding: 0px 10px;\n              font-size: 17px;\n              color: #9c9c9c;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap .explanation-wrap textarea[data-v-259f8d43]:hover, .custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap .explanation-wrap textarea[data-v-259f8d43]:focus {\n                outline: none !important;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap .ratings-block[data-v-259f8d43] {\n              margin-top: 10px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap .ratings-block img[data-v-259f8d43] {\n                width: 100%;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap .green-step-btn-wrap .green-step-btn[data-v-259f8d43] {\n              margin: 0;\n              margin-top: 30px;\n              font-size: 12px;\n              line-height: 14px;\n              text-transform: uppercase;\n              padding: 4px 8px;\n              cursor: pointer;\n}\n.custom-container .green-step-btn[data-v-259f8d43] {\n    width: 100%;\n    max-width: 180px;\n    padding: 7px 10px;\n    border: 2px solid #118921;\n    background-color: #2cac3d;\n    font-size: 14px;\n    font-family: \"BrandonTextMedium\";\n    letter-spacing: 0.05rem;\n    color: #fff;\n    border-radius: 20px;\n    display: inline-block;\n    text-align: center;\n    margin: 10px 15px 30px 0px;\n}\n.custom-container .green-step-btn[data-v-259f8d43]:hover {\n      background-color: #158f25;\n      color: #fff;\n      outline: none !important;\n}\n@media screen and (max-width: 1281px) {\n.custom-container .test-proj-fillup-page .white-box[data-v-259f8d43] {\n    font-size: 16px;\n}\n.custom-container .test-proj-fillup-page .green-step-btn[data-v-259f8d43] {\n    font-size: 13px;\n}\n}\n@media screen and (max-width: 1200px) {\n.custom-container .test-proj-fillup-page .green-step-btn[data-v-259f8d43] {\n    font-size: 12px;\n    letter-spacing: 0;\n}\n.custom-container .test-proj-fillup-page .white-box[data-v-259f8d43] {\n    font-size: 14px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title[data-v-259f8d43] {\n      font-size: 18px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title .badge-wrap[data-v-259f8d43] {\n        font-size: 15px;\n}\n}\n@media screen and (max-width: 991px) {\n.custom-container .test-proj-fillup-page .white-box[data-v-259f8d43] {\n    padding: 15px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title .badge-wrap[data-v-259f8d43] {\n      right: 0;\n}\n}\n@media screen and (max-width: 767px) {\n.custom-container .test-proj-fillup-page .status-bar[data-v-259f8d43] {\n    font-size: 14px;\n}\n.custom-container .test-proj-fillup-page .green-step-btn[data-v-259f8d43] {\n    margin: 10px 15px 10px 0;\n    max-width: 175px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title .badge-wrap[data-v-259f8d43] {\n    right: 0;\n    top: -3px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status br[data-v-259f8d43] {\n    display: block;\n}\n.custom-container .test-proj-fillup-page .white-box .notes-list[data-v-259f8d43] {\n    margin-bottom: 15px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.active span[data-v-259f8d43] {\n    display: block !important;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a[data-v-259f8d43] {\n    font-size: 16px;\n    padding: 10px !important;\n    padding-left: 30px !important;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a[data-v-259f8d43]::before {\n      top: 15px;\n      left: 10px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a span[data-v-259f8d43] {\n      font-size: 16px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-issues-tab .issue-block .issue-type[data-v-259f8d43] {\n    font-size: 16px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-issues-tab .issue-block .form-group .remove-icon[data-v-259f8d43] {\n    margin-top: 3px;\n    display: block;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-issues-tab .issue-block .form-group .remove-icon i[data-v-259f8d43] {\n      font-size: 24px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-attachment-tab .add-files-btn[data-v-259f8d43] {\n    float: left;\n    max-width: 100px;\n    padding: 2px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-attachment-tab .attachment-table-wrap .attachment-table[data-v-259f8d43] {\n    border-radius: 6px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-attachment-tab .attachment-table-wrap .attachment-table tr td[data-v-259f8d43]:nth-child(2) {\n      min-width: 320px;\n}\n}\n@media screen and (max-width: 575px) {\n.custom-container .step-page-title[data-v-259f8d43] {\n    font-size: 26px;\n}\n.custom-container .test-proj-fillup-page .white-box[data-v-259f8d43] {\n    font-size: 14px;\n    line-height: 18px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title.proj-name-wrap[data-v-259f8d43] {\n      padding-bottom: 40px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title.proj-name-wrap .proj-name[data-v-259f8d43] {\n        display: block;\n        width: 100%;\n        font-size: 20px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title.proj-name-wrap .badge-wrap[data-v-259f8d43] {\n        position: relative;\n        float: left;\n        margin-left: -15px;\n        margin-top: 15px;\n        font-size: 13px;\n        border-bottom-left-radius: 0;\n        border-top-left-radius: 0;\n        border-top-right-radius: 16px;\n        border-bottom-right-radius: 16px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status:first-child .proj-progress[data-v-259f8d43]::before {\n      width: calc(50% - 25px);\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status:last-child .proj-progress[data-v-259f8d43]::after {\n      width: calc(50% - 25px);\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.active .proj-progress i[data-v-259f8d43] {\n      font-size: 28px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.active .proj-progress i[data-v-259f8d43]::after {\n        height: 13px;\n        width: 13px;\n        left: 5px;\n        top: 5px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.done-step .proj-progress i[data-v-259f8d43] {\n      font-size: 28px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.done-step .proj-progress i[data-v-259f8d43]::after {\n        height: 13px;\n        width: 13px;\n        left: 5px;\n        top: 5px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .total-score-txt[data-v-259f8d43] {\n      font-size: 18px;\n      margin-bottom: 20px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a[data-v-259f8d43] {\n      font-size: 14px;\n      padding: 5px 10px !important;\n      padding-left: 30px !important;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a[data-v-259f8d43]::before {\n        top: 9px;\n        left: 10px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a span[data-v-259f8d43] {\n        font-size: 14px;\n}\n.custom-container .green-step-btn[data-v-259f8d43] {\n    margin: 10px 15px 10px 0;\n}\n}\n@media screen and (max-width: 360px) {\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .total-score-txt[data-v-259f8d43] {\n    font-size: 14px;\n    margin-bottom: 15px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a[data-v-259f8d43] {\n    font-size: 13px;\n    padding-left: 20px !important;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a[data-v-259f8d43]::before {\n      top: 10px;\n      left: 6px;\n      height: 10px;\n      width: 10px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a span[data-v-259f8d43] {\n      font-size: 13px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-9c7ea048\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/tester/test-project-details.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.custom-container[data-v-9c7ea048] {\n  max-width: 100%;\n}\n.custom-container .text-green[data-v-9c7ea048] {\n    color: #2cac3d;\n}\n.custom-container .text-red[data-v-9c7ea048] {\n    color: #e63423;\n}\n.custom-container .text-bold[data-v-9c7ea048] {\n    font-family: \"BrandonTextBold\" !important;\n}\n.custom-container .text-medium[data-v-9c7ea048] {\n    font-family: \"BrandonTextMedium\";\n}\n.custom-container .test-proj-fillup-page[data-v-9c7ea048] {\n    width: 100%;\n}\n.custom-container .test-proj-fillup-page .white-box[data-v-9c7ea048] {\n      background-color: #fff;\n      -webkit-box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n              box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n      border-radius: 4px;\n      padding: 20px;\n      margin-bottom: 20px;\n      font-size: 14px;\n      line-height: 22px;\n      letter-spacing: 0;\n      color: #606368;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title[data-v-9c7ea048] {\n        font-size: 18px;\n        font-family: \"UniNeueBold\";\n        border-bottom: 1px solid #dadada;\n        color: #606368;\n        padding-bottom: 10px;\n        margin-bottom: 10px;\n        width: auto;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title .badge-wrap[data-v-9c7ea048] {\n          border: 1px solid #2cac3d;\n          background-color: #2cac3d;\n          color: #fff;\n          font-size: 16px;\n          right: -5px;\n          position: absolute;\n          border-top-left-radius: 16px;\n          border-bottom-left-radius: 16px;\n          padding: 2px 12px;\n          top: -5px;\n          font-family: \"BrandonTextRegular\";\n}\n.custom-container .test-proj-fillup-page .white-box .proj-name-wrap .proj-name[data-v-9c7ea048] {\n        font-family: \"UniNeueBold\";\n        font-size: 24px;\n        line-height: 18px;\n        color: #2cac3d;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-desc[data-v-9c7ea048] {\n        letter-spacing: 0.01rem;\n        margin-bottom: 5px;\n}\n.custom-container .test-proj-fillup-page .white-box .notes-list[data-v-9c7ea048] {\n        padding-left: 15px;\n        margin-bottom: 0px;\n}\n.custom-container .test-proj-fillup-page .white-box .notes-list li[data-v-9c7ea048] {\n          position: relative;\n}\n.custom-container .test-proj-fillup-page .white-box .notes-list li[data-v-9c7ea048]::before {\n            content: \"-\";\n            position: absolute;\n            left: -15px;\n            top: -1px;\n            color: #606368;\n            font-size: 20px;\n}\n.custom-container .test-proj-fillup-page .white-box .project-link[data-v-9c7ea048] {\n        color: #2cac3d;\n        text-decoration: underline !important;\n}\n.custom-container .test-proj-fillup-page .white-box .test-device-list span[data-v-9c7ea048] {\n        margin-right: 15px;\n        margin-top: 10px;\n}\n@media screen and (max-width: 1200px) {\n.custom-container .test-proj-fillup-page .white-box .block-title .badge-wrap[data-v-9c7ea048] {\n    font-size: 15px;\n}\n}\n@media screen and (max-width: 991px) {\n.custom-container .test-proj-fillup-page .white-box[data-v-9c7ea048] {\n    padding: 15px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title .badge-wrap[data-v-9c7ea048] {\n      right: 0;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-name-wrap .proj-name[data-v-9c7ea048] {\n      font-size: 22px;\n}\n}\n@media screen and (max-width: 767px) {\n.custom-container .test-proj-fillup-page .status-bar[data-v-9c7ea048] {\n    font-size: 14px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title[data-v-9c7ea048] {\n    font-size: 16px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title .badge-wrap[data-v-9c7ea048] {\n      right: 0;\n      top: -3px;\n}\n.custom-container .test-proj-fillup-page .white-box .notes-list[data-v-9c7ea048] {\n    margin-bottom: 15px;\n}\n}\n@media screen and (max-width: 575px) {\n.custom-container .test-proj-fillup-page .white-box[data-v-9c7ea048] {\n    line-height: 18px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title.proj-name-wrap[data-v-9c7ea048] {\n      padding-bottom: 40px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title.proj-name-wrap .proj-name[data-v-9c7ea048] {\n        display: block;\n        width: 100%;\n        font-size: 20px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title.proj-name-wrap .badge-wrap[data-v-9c7ea048] {\n        position: relative;\n        float: left;\n        margin-left: -15px;\n        margin-top: 15px;\n        font-size: 13px;\n        border-bottom-left-radius: 0;\n        border-top-left-radius: 0;\n        border-top-right-radius: 16px;\n        border-bottom-right-radius: 16px;\n}\n.custom-container .test-proj-fillup-page .white-box .project-link[data-v-9c7ea048] {\n      display: block;\n}\n}\n@media screen and (max-width: 360px) {\n.custom-container .test-proj-fillup-page .white-box .block-title.proj-name-wrap .badge-wrap[data-v-9c7ea048] {\n    font-size: 11px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-d754b8aa\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project-testing-process-review.vue":
/***/ (function(module, exports, __webpack_require__) {

var escape = __webpack_require__("./node_modules/css-loader/lib/url/escape.js");
exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.text-green[data-v-d754b8aa] {\n  color: #2cac3d;\n}\n.text-red[data-v-d754b8aa] {\n  color: #e63423;\n}\n.text-bold[data-v-d754b8aa] {\n  font-family: \"BrandonTextBold\" !important;\n}\n.text-medium[data-v-d754b8aa] {\n  font-family: \"BrandonTextMedium\";\n}\n.white-box[data-v-d754b8aa] {\n  background-color: #fff;\n  -webkit-box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n          box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n  border-radius: 4px;\n  padding: 20px;\n  margin-bottom: 20px;\n  font-size: 14px;\n  line-height: 22px;\n  color: #606368;\n}\n.white-box .block-title[data-v-d754b8aa] {\n    font-size: 18px;\n    font-family: \"UniNeueBold\";\n    border-bottom: 1px solid #dadada;\n    color: #606368;\n    padding-bottom: 10px;\n    margin-bottom: 10px;\n    width: auto;\n}\n.white-box .block-title .badge-wrap[data-v-d754b8aa] {\n      border: 1px solid #2cac3d;\n      background-color: #2cac3d;\n      color: #fff;\n      font-size: 16px;\n      right: -5px;\n      position: absolute;\n      border-top-left-radius: 16px;\n      border-bottom-left-radius: 16px;\n      padding: 2px 12px;\n      top: -5px;\n      font-family: \"BrandonTextRegular\";\n}\n.white-box .test-device-list span[data-v-d754b8aa] {\n    margin-right: 15px;\n    margin-top: 10px;\n}\n.white-box .tester-issues-tab .line-seprator[data-v-d754b8aa] {\n    margin: 0 0 20px;\n}\n.white-box .tester-issues-tab .issue-block .issue-type[data-v-d754b8aa] {\n    margin-bottom: 0px;\n    font-size: 18px;\n}\n.white-box .tester-issues-tab .issue-block .form-group .form-control[data-v-d754b8aa] {\n    margin-bottom: 20px;\n}\n.white-box .tester-issues-tab .issue-block .form-group .remove-icon[data-v-d754b8aa] {\n    color: #606368;\n}\n.white-box .tester-issues-tab .issue-block .form-group .remove-icon i[data-v-d754b8aa] {\n      font-size: 34px;\n}\n.white-box .tester-attachment-tab .add-files-btn[data-v-d754b8aa] {\n    width: 160px;\n    float: right;\n    margin-top: 0;\n    margin-bottom: 20px;\n}\n.white-box .tester-attachment-tab .add-files-btn input[data-v-d754b8aa] {\n      opacity: 0;\n      position: absolute;\n      top: 0;\n      right: 0;\n}\n.white-box .tester-attachment-tab .attachment-table-wrap .attachment-table[data-v-d754b8aa] {\n    border-radius: 6px;\n    margin-bottom: 0;\n}\n.white-box .tester-attachment-tab .attachment-table-wrap .attachment-table tr td[data-v-d754b8aa]:first-child {\n      min-width: 60px;\n      max-width: 60px;\n      width: 60px;\n      text-align: center;\n}\n.white-box .tester-attachment-tab .attachment-table-wrap .attachment-table tr td[data-v-d754b8aa]:last-child {\n      min-width: 130px;\n      max-width: 130px;\n      width: 130px;\n      display: table-cell;\n}\n.white-box .tester-attachment-tab .attachment-table-wrap .attachment-table tr td[data-v-d754b8aa]:nth-child(3) {\n      min-width: 160px;\n      max-width: 160px;\n      width: 160px;\n      display: table-cell;\n      text-align: center;\n}\n.white-box .tester-attachment-tab .attachment-table-wrap .attachment-table tr td a[data-v-d754b8aa] {\n      margin-right: 10px;\n      color: #1f1f1f;\n      font-size: 20px;\n}\n.white-box .tester-rating-tab .total-score-txt[data-v-d754b8aa] {\n    font-size: 20px;\n    font-family: \"UniNeueBold\";\n    color: #363e48;\n    text-align: center;\n    width: 100%;\n    margin-bottom: 15px;\n}\n.white-box .tester-rating-tab .total-score-txt .effective-badge[data-v-d754b8aa] {\n      font-family: \"BrandonTextRegular\";\n      color: #fff;\n      font-size: 12px;\n      background-color: #4be05e;\n      border: 1px solid #2cac3d;\n      border-radius: 16px;\n      padding: 1px 8px;\n      vertical-align: middle;\n      margin-bottom: 5px;\n      display: inline-block;\n}\n.white-box .tester-rating-tab .custom-accordion[data-v-d754b8aa] {\n    border: none;\n}\n.white-box .tester-rating-tab .custom-accordion[data-v-d754b8aa]:hover {\n      -webkit-box-shadow: none;\n              box-shadow: none;\n}\n.white-box .tester-rating-tab .custom-accordion .card-header[data-v-d754b8aa] {\n      padding: 0 !important;\n      border-bottom: none;\n}\n.white-box .tester-rating-tab .custom-accordion .card-header[data-v-d754b8aa]:hover {\n        outline: none !important;\n}\n.white-box .tester-rating-tab .custom-accordion .card-header a.collapsed[data-v-d754b8aa] {\n        border: 2px solid #118921;\n        background-color: #2cac3d;\n        color: #fff;\n        border-radius: 6px;\n}\n.white-box .tester-rating-tab .custom-accordion .card-header a.collapsed[data-v-d754b8aa]:before {\n          background-image: url(" + escape(__webpack_require__("./resources/assets/assets/img/right-arrow.png")) + ");\n}\n.white-box .tester-rating-tab .custom-accordion .card-header a.collapsed span[data-v-d754b8aa] {\n          color: #fff;\n}\n.white-box .tester-rating-tab .custom-accordion .card-header a.collapsed img[data-v-d754b8aa] {\n          display: inline-block;\n          vertical-align: initial;\n}\n.white-box .tester-rating-tab .custom-accordion .card-header a[data-v-d754b8aa] {\n        padding: 10px 20px !important;\n        padding-left: 35px !important;\n        border-radius: 0px;\n        border-top-right-radius: 6px;\n        border-top-left-radius: 6px;\n        text-align: left;\n        color: #2cac3d;\n        font-family: \"UniNeueHeavy\";\n        font-size: 16px;\n        text-align: left;\n        text-transform: uppercase;\n        background-color: #f2f2f2;\n        border: none !important;\n}\n.white-box .tester-rating-tab .custom-accordion .card-header a[data-v-d754b8aa]:before {\n          background-image: url(" + escape(__webpack_require__("./resources/assets/assets/img/down-arrow.png")) + ");\n          content: \"\";\n          position: absolute;\n          left: 15px;\n          top: 15px;\n          height: 14px;\n          width: 14px;\n          background-size: 100%;\n          background-position: center;\n}\n.white-box .tester-rating-tab .custom-accordion .card-header a span[data-v-d754b8aa] {\n          float: right;\n          font-family: \"BrandonTextBold\";\n          font-size: 16px;\n          text-transform: none;\n          color: #2f2f2f;\n}\n.white-box .tester-rating-tab .custom-accordion .card-body[data-v-d754b8aa] {\n      padding: 0 10px 10px;\n      background-color: #f2f2f2;\n      border-bottom-left-radius: 6px;\n      border-bottom-right-radius: 6px;\n}\n.white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap[data-v-d754b8aa] {\n        background-color: #fff;\n        border: 1px solid #dadada;\n        padding: 15px;\n        border-radius: 6px;\n}\n.white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap h4[data-v-d754b8aa] {\n          font-size: 18px;\n          color: #2cac3d;\n          font-family: \"BrandonTextBold\";\n}\n.white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap .detail-block[data-v-d754b8aa] {\n          font-family: \"BrandonTextMedium\";\n          font-size: 14px;\n          line-height: 16px;\n}\n.white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap .detail-block span[data-v-d754b8aa] {\n            width: calc(100% / 3 - 5px);\n            display: inline-block;\n            vertical-align: top;\n}\n.white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap .explanation-wrap textarea[data-v-d754b8aa] {\n          resize: none;\n          padding: 0px 10px;\n          font-size: 17px;\n          color: #9c9c9c;\n}\n.white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap .explanation-wrap textarea[data-v-d754b8aa]:hover, .white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap .explanation-wrap textarea[data-v-d754b8aa]:focus {\n            outline: none !important;\n}\n.white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap .ratings-block[data-v-d754b8aa] {\n          margin-top: 10px;\n}\n.white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap .ratings-block img[data-v-d754b8aa] {\n            width: 100%;\n}\n.white-box .tester-rating-tab .custom-accordion .card-body .detail-block-wrap .green-step-btn-wrap .green-step-btn[data-v-d754b8aa] {\n          margin: 0;\n          margin-top: 30px;\n          font-size: 12px;\n          line-height: 14px;\n          text-transform: uppercase;\n          padding: 4px 8px;\n          cursor: pointer;\n}\n.green-step-btn[data-v-d754b8aa] {\n  width: 100%;\n  max-width: 180px;\n  padding: 7px 10px;\n  border: 2px solid #118921;\n  background-color: #2cac3d;\n  font-size: 14px;\n  font-family: \"BrandonTextMedium\";\n  letter-spacing: 0.05rem;\n  color: #fff;\n  border-radius: 20px;\n  display: inline-block;\n  text-align: center;\n  margin: 10px 15px 30px 0px;\n}\n.green-step-btn[data-v-d754b8aa]:hover {\n    background-color: #158f25;\n    color: #fff;\n    outline: none !important;\n}\n@media screen and (max-width: 1281px) {\n.custom-container .test-proj-fillup-page .white-box[data-v-d754b8aa] {\n    font-size: 16px;\n}\n.custom-container .test-proj-fillup-page .green-step-btn[data-v-d754b8aa] {\n    font-size: 13px;\n}\n}\n@media screen and (max-width: 1200px) {\n.custom-container .test-proj-fillup-page .green-step-btn[data-v-d754b8aa] {\n    font-size: 12px;\n    letter-spacing: 0;\n}\n.custom-container .test-proj-fillup-page .white-box[data-v-d754b8aa] {\n    font-size: 14px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title[data-v-d754b8aa] {\n      font-size: 18px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title .badge-wrap[data-v-d754b8aa] {\n        font-size: 15px;\n}\n}\n@media screen and (max-width: 991px) {\n.custom-container .test-proj-fillup-page .white-box[data-v-d754b8aa] {\n    padding: 15px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title .badge-wrap[data-v-d754b8aa] {\n      right: 0;\n}\n}\n@media screen and (max-width: 767px) {\n.custom-container .test-proj-fillup-page .status-bar[data-v-d754b8aa] {\n    font-size: 14px;\n}\n.custom-container .test-proj-fillup-page .green-step-btn[data-v-d754b8aa] {\n    margin: 10px 15px 10px 0;\n    max-width: 175px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title .badge-wrap[data-v-d754b8aa] {\n    right: 0;\n    top: -3px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status br[data-v-d754b8aa] {\n    display: block;\n}\n.custom-container .test-proj-fillup-page .white-box .notes-list[data-v-d754b8aa] {\n    margin-bottom: 15px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.active span[data-v-d754b8aa] {\n    display: block !important;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a[data-v-d754b8aa] {\n    font-size: 16px;\n    padding: 10px !important;\n    padding-left: 30px !important;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a[data-v-d754b8aa]::before {\n      top: 15px;\n      left: 10px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a span[data-v-d754b8aa] {\n      font-size: 16px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-issues-tab .issue-block .issue-type[data-v-d754b8aa] {\n    font-size: 16px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-issues-tab .issue-block .form-group .remove-icon[data-v-d754b8aa] {\n    margin-top: 3px;\n    display: block;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-issues-tab .issue-block .form-group .remove-icon i[data-v-d754b8aa] {\n      font-size: 24px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-attachment-tab .add-files-btn[data-v-d754b8aa] {\n    float: left;\n    max-width: 100px;\n    padding: 2px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-attachment-tab .attachment-table-wrap .attachment-table[data-v-d754b8aa] {\n    border-radius: 6px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-attachment-tab .attachment-table-wrap .attachment-table tr td[data-v-d754b8aa]:nth-child(2) {\n      min-width: 320px;\n}\n}\n@media screen and (max-width: 575px) {\n.custom-container .step-page-title[data-v-d754b8aa] {\n    font-size: 26px;\n}\n.custom-container .test-proj-fillup-page .white-box[data-v-d754b8aa] {\n    font-size: 14px;\n    line-height: 18px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title.proj-name-wrap[data-v-d754b8aa] {\n      padding-bottom: 40px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title.proj-name-wrap .proj-name[data-v-d754b8aa] {\n        display: block;\n        width: 100%;\n        font-size: 20px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title.proj-name-wrap .badge-wrap[data-v-d754b8aa] {\n        position: relative;\n        float: left;\n        margin-left: -15px;\n        margin-top: 15px;\n        font-size: 13px;\n        border-bottom-left-radius: 0;\n        border-top-left-radius: 0;\n        border-top-right-radius: 16px;\n        border-bottom-right-radius: 16px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status:first-child .proj-progress[data-v-d754b8aa]::before {\n      width: calc(50% - 25px);\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status:last-child .proj-progress[data-v-d754b8aa]::after {\n      width: calc(50% - 25px);\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.active .proj-progress i[data-v-d754b8aa] {\n      font-size: 28px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.active .proj-progress i[data-v-d754b8aa]::after {\n        height: 13px;\n        width: 13px;\n        left: 5px;\n        top: 5px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.done-step .proj-progress i[data-v-d754b8aa] {\n      font-size: 28px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.done-step .proj-progress i[data-v-d754b8aa]::after {\n        height: 13px;\n        width: 13px;\n        left: 5px;\n        top: 5px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .total-score-txt[data-v-d754b8aa] {\n      font-size: 18px;\n      margin-bottom: 20px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a[data-v-d754b8aa] {\n      font-size: 14px;\n      padding: 5px 10px !important;\n      padding-left: 30px !important;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a[data-v-d754b8aa]::before {\n        top: 9px;\n        left: 10px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a span[data-v-d754b8aa] {\n        font-size: 14px;\n}\n.custom-container .green-step-btn[data-v-d754b8aa] {\n    margin: 10px 15px 10px 0;\n}\n}\n@media screen and (max-width: 360px) {\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .total-score-txt[data-v-d754b8aa] {\n    font-size: 14px;\n    margin-bottom: 15px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a[data-v-d754b8aa] {\n    font-size: 13px;\n    padding-left: 20px !important;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a[data-v-d754b8aa]::before {\n      top: 10px;\n      left: 6px;\n      height: 10px;\n      width: 10px;\n}\n.custom-container .test-proj-fillup-page .white-box .tester-rating-tab .custom-accordion .card-header a span[data-v-d754b8aa] {\n      font-size: 13px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e009baa8\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/tester/project-status-test-project.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e55f98a2\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/file-viewer.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.publisher[data-v-e55f98a2] {\n  background: #fff;\n}\n.file[data-v-e55f98a2] {\n  padding: 0px;\n  color: #606368;\n  text-decoration: none;\n  margin-top: 3px;\n  border-radius: 4px;\n  margin-left: 0px;\n}\n.file a[data-v-e55f98a2] {\n    color: #606368;\n    padding: 0;\n}\n.file a[data-v-e55f98a2]:hover {\n      text-decoration: underline !important;\n}\n.tester[data-v-e55f98a2] {\n  background: #fff;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-ed68c038\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project/rating.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.ratings-block[data-v-ed68c038] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.ratings-block .rating-item[data-v-ed68c038] {\n    -webkit-box-flex: 1;\n        -ms-flex: 1;\n            flex: 1;\n    min-height: 40px;\n    margin-right: 12px;\n    text-align: center;\n    font-size: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    font-weight: bold;\n}\n.ratings-block .rating-item[data-v-ed68c038]:not(.fill) {\n      color: #606368 !important;\n      background: #f2f2f2 !important;\n}\n.ratings-block .rating-item i[data-v-ed68c038] {\n      margin-left: 3px;\n}\n.ratings-block .rating-item.fill i[data-v-ed68c038]:before {\n      content: \"\\F005\";\n}\n.ratings-block .rating-item.first[data-v-ed68c038] {\n      border-bottom-left-radius: 20px;\n      border-top-left-radius: 20px;\n}\n.ratings-block .rating-item.last[data-v-ed68c038] {\n      border-top-right-radius: 20px;\n      border-bottom-right-radius: 20px;\n}\n.detail-block-wrap[data-v-ed68c038] {\n  background-color: #fff;\n  border: 1px solid #dadada;\n  padding: 15px;\n  border-radius: 6px;\n}\n.detail-block-wrap h4[data-v-ed68c038] {\n    font-size: 18px;\n    color: #2cac3d;\n    font-family: \"BrandonTextBold\";\n}\n.detail-block-wrap .detail-block[data-v-ed68c038] {\n    font-family: \"BrandonTextMedium\";\n    font-size: 14px;\n    line-height: 16px;\n}\n.detail-block-wrap .detail-block span[data-v-ed68c038] {\n      width: calc(100% / 3 - 5px);\n      display: inline-block;\n      vertical-align: top;\n}\n.detail-block-wrap .explanation-wrap textarea[data-v-ed68c038] {\n    resize: none;\n    padding: 0px 10px;\n    font-size: 17px;\n    color: #9c9c9c;\n}\n.detail-block-wrap .explanation-wrap textarea[data-v-ed68c038]:hover, .detail-block-wrap .explanation-wrap textarea[data-v-ed68c038]:focus {\n      outline: none !important;\n}\n.detail-block-wrap .ratings-block[data-v-ed68c038] {\n    margin-top: 10px;\n}\n.detail-block-wrap .ratings-block img[data-v-ed68c038] {\n      width: 100%;\n}\n.detail-block-wrap .green-step-btn-wrap .green-step-btn[data-v-ed68c038] {\n    margin: 0;\n    margin-top: 30px;\n    font-size: 12px;\n    line-height: 14px;\n    text-transform: uppercase;\n    padding: 4px 8px;\n    cursor: pointer;\n    color: #fff;\n}\n.green-step-btn[data-v-ed68c038] {\n  width: 100%;\n  max-width: 180px;\n  padding: 7px 10px;\n  border: 2px solid #118921;\n  background-color: #2cac3d;\n  color: #fff;\n  font-size: 14px;\n  font-family: \"BrandonTextMedium\";\n  letter-spacing: 0.05rem;\n  color: #fff;\n  border-radius: 20px;\n  display: inline-block;\n  text-align: center;\n  margin: 10px 15px 30px 0px;\n}\n.green-step-btn[data-v-ed68c038]:hover {\n    background-color: #158f25;\n    color: #fff;\n    outline: none !important;\n}\n@media screen and (max-width: 1281px) {\n.custom-container .test-proj-fillup-page .white-box[data-v-ed68c038] {\n    font-size: 16px;\n}\n.custom-container .test-proj-fillup-page .green-step-btn[data-v-ed68c038] {\n    font-size: 13px;\n}\n}\n@media screen and (max-width: 1200px) {\n.detail-block-wrap .green-step-btn-wrap[data-v-ed68c038] {\n    max-width: 200px;\n    float: right;\n}\n.detail-block-wrap .green-step-btn-wrap .green-step-btn[data-v-ed68c038] {\n      margin-top: 20px;\n}\n}\n@media screen and (max-width: 767px) {\n.detail-block-wrap h4[data-v-ed68c038] {\n    font-size: 16px;\n}\n.detail-block-wrap .ratings-block[data-v-ed68c038] {\n    margin-top: 0;\n}\n.detail-block-wrap .green-step-btn-wrap[data-v-ed68c038] {\n    max-width: 160px;\n}\n.detail-block-wrap .green-step-btn-wrap .green-step-btn[data-v-ed68c038] {\n      margin-top: 10px;\n      font-size: 12px;\n      line-height: 12px;\n}\n}\n@media screen and (max-width: 575px) {\n.detail-block-wrap .green-step-btn-wrap[data-v-ed68c038] {\n    max-width: 130px;\n}\n.detail-block-wrap .green-step-btn-wrap .green-step-btn[data-v-ed68c038] {\n      font-size: 10px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-eea29f5a\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/steps.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.steps-container[data-v-eea29f5a] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  margin: 25px 0 10px;\n}\n.steps-container div[data-v-eea29f5a] {\n  -webkit-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n}\n.step[data-v-eea29f5a] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-line-pack: center;\n      align-content: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.title-container[data-v-eea29f5a] {\n  width: 100%;\n  text-align: center;\n  display: block;\n  margin-top: 0px;\n  line-height: 0px;\n}\n.title[data-v-eea29f5a] {\n  font-size: 17px;\n  color: #85878b;\n}\n.line[data-v-eea29f5a] {\n  height: 3px;\n  border-top: 3px solid #ececec;\n  -webkit-box-flex: 2;\n      -ms-flex: 2;\n          flex: 2;\n}\n.dot[data-v-eea29f5a] {\n  width: 30px;\n  position: relative;\n  top: -14px;\n  max-width: 30px;\n  height: 30px;\n  border-radius: 50%;\n  background: white;\n  -webkit-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  border: 3px solid transparent;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-line-pack: center;\n      align-content: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.dot .fill[data-v-eea29f5a] {\n    background: transparent;\n    border: 2px solid rgba(175, 177, 179, 0.5);\n    width: 16px;\n    height: 16px;\n    max-width: 16px;\n    border-radius: 50%;\n}\n.paid-project .active .title[data-v-eea29f5a] {\n  color: #01aef7;\n}\n.paid-project .active .line.fill[data-v-eea29f5a] {\n  border-color: #01aef7;\n}\n.paid-project .active .dot[data-v-eea29f5a] {\n  border-color: #01aef7;\n}\n.paid-project .active .dot .fill[data-v-eea29f5a] {\n    background: #01aef7;\n    border-color: transparent;\n}\n.test-project .active .title[data-v-eea29f5a] {\n  color: #2cac3d;\n  font-family: \"BrandonTextBold\";\n}\n.test-project .active .line.fill[data-v-eea29f5a] {\n  border-color: #2cac3d;\n}\n.test-project .active .dot[data-v-eea29f5a] {\n  border-color: #2cac3d;\n}\n.test-project .active .dot .fill[data-v-eea29f5a] {\n    background: #2cac3d;\n    border-color: transparent;\n}\n@media screen and (max-width: 991px) {\n.dot[data-v-eea29f5a] {\n    width: 28px;\n    top: -13px;\n    max-width: 28px;\n    height: 28px;\n}\n.dot .fill[data-v-eea29f5a] {\n      width: 14px;\n      height: 14px;\n      max-width: 14px;\n}\n.title[data-v-eea29f5a] {\n    font-size: 15px;\n}\n}\n@media screen and (max-width: 767px) {\n.title-container[data-v-eea29f5a] {\n    line-height: 16px;\n}\n}\n@media screen and (max-width: 575px) {\n.title-container[data-v-eea29f5a] {\n    line-height: 16px;\n}\n.test-project .step-wrap .title[data-v-eea29f5a], .paid-project .step-wrap .title[data-v-eea29f5a] {\n    display: none;\n}\n.test-project .current_active_step .title[data-v-eea29f5a], .paid-project .current_active_step .title[data-v-eea29f5a] {\n    display: block;\n}\n.dot[data-v-eea29f5a] {\n    width: 20px;\n    top: -9px;\n    max-width: 20px;\n    height: 20px;\n}\n.dot .fill[data-v-eea29f5a] {\n      width: 10px;\n      height: 10px;\n      max-width: 10px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-0d653902\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/device-selector.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    _vm._l(_vm.availableDevices, function(device) {
      return _c("span", { staticClass: "d-inline-block pr-3" }, [
        _c("img", {
          staticClass: "img-fluid",
          class: {
            selected: _vm.value.indexOf(device.id) !== -1,
            disabled: _vm.disabled
          },
          attrs: { title: device.name, src: device.icon },
          on: {
            click: function($event) {
              _vm.toggleItem(device)
            }
          }
        })
      ])
    })
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0d653902", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-259f8d43\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/tester/project-submission.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "bg-color" }, [
    _c("div", { staticClass: "container custom-container" }, [
      _c("div", { staticClass: "row" }, [
        _c(
          "div",
          { staticClass: "test-proj-fillup-page" },
          [
            _c("div", { staticClass: "white-box" }, [
              _c("div", { staticClass: "row" }, [
                _vm._m(0),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "col-12" },
                  [
                    _c("Steps", {
                      attrs: {
                        steps: _vm.steps,
                        active: _vm.get_project_current_stage,
                        projectTypeClass: "test-project"
                      }
                    })
                  ],
                  1
                )
              ])
            ]),
            _vm._v(" "),
            _c("TestProjectDetails", {
              attrs: {
                testProject: _vm.testproject,
                currentTestStatus: _vm.currentTestStatus
              }
            }),
            _vm._v(" "),
            _c("div", { staticClass: "white-box" }, [
              _vm._m(1),
              _vm._v(" "),
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-12" }, [
                  _c(
                    "div",
                    { staticClass: "testing-process-tabwrap" },
                    [
                      _c(
                        "b-tabs",
                        [
                          _c(
                            "b-tab",
                            {
                              staticClass: "active-tab",
                              attrs: { title: "Questions", active: "" }
                            },
                            [
                              _c(
                                "div",
                                { staticClass: "tester-questions-tab" },
                                _vm._l(_vm.testproject.questions, function(
                                  question
                                ) {
                                  return _c("div", { staticClass: "row" }, [
                                    _c(
                                      "div",
                                      { staticClass: "col-12 col-md-5" },
                                      [
                                        _c("p", { staticClass: "mb-sm-1" }, [
                                          _c(
                                            "span",
                                            { staticClass: "text-medium" },
                                            [_vm._v("Q1.")]
                                          ),
                                          _vm._v(
                                            "\n                            " +
                                              _vm._s(question.question) +
                                              "\n                          "
                                          )
                                        ])
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      { staticClass: "col-12 col-md-7" },
                                      [
                                        _c(
                                          "div",
                                          { staticClass: "form-group" },
                                          [
                                            _c("input", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value:
                                                    _vm.answer_map[question.id],
                                                  expression:
                                                    "answer_map[question.id]"
                                                }
                                              ],
                                              staticClass: "form-control",
                                              attrs: {
                                                type: "text",
                                                placeholder: ""
                                              },
                                              domProps: {
                                                value:
                                                  _vm.answer_map[question.id]
                                              },
                                              on: {
                                                input: function($event) {
                                                  if ($event.target.composing) {
                                                    return
                                                  }
                                                  _vm.$set(
                                                    _vm.answer_map,
                                                    question.id,
                                                    $event.target.value
                                                  )
                                                }
                                              }
                                            })
                                          ]
                                        )
                                      ]
                                    )
                                  ])
                                })
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c("b-tab", { attrs: { title: "Ratings/Scores" } }, [
                            _c("div", { staticClass: "tester-rating-tab" }, [
                              _c("h4", { staticClass: "total-score-txt" }, [
                                _vm._v(
                                  "\n                        Total Score: 79\n                        "
                                ),
                                _c("span", { staticClass: "effective-badge" }, [
                                  _vm._v("Effective")
                                ])
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { attrs: { role: "tablist" } },
                                [
                                  _c(
                                    "b-card",
                                    {
                                      staticClass:
                                        "mb-3 mb-sm-4 custom-accordion",
                                      attrs: { "no-body": "" }
                                    },
                                    [
                                      _c(
                                        "b-card-header",
                                        {
                                          staticClass: "p-1",
                                          attrs: {
                                            "header-tag": "header",
                                            role: "tab"
                                          }
                                        },
                                        [
                                          _c(
                                            "b-btn",
                                            {
                                              directives: [
                                                {
                                                  name: "b-toggle",
                                                  rawName:
                                                    "v-b-toggle.accordion1",
                                                  modifiers: {
                                                    accordion1: true
                                                  }
                                                }
                                              ],
                                              attrs: { block: "", href: "#" }
                                            },
                                            [
                                              _vm._v(
                                                "\n                              content\n                              "
                                              ),
                                              _c(
                                                "span",
                                                { staticClass: "text-right" },
                                                [
                                                  _vm._v(
                                                    "\n                                Total Score : 00\n                              "
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "b-collapse",
                                        {
                                          attrs: {
                                            id: "accordion1",
                                            visible: "",
                                            accordion: "my-accordion",
                                            role: "tabpanel"
                                          }
                                        },
                                        [
                                          _c(
                                            "b-card-body",
                                            [
                                              _c("starRatings", {
                                                model: {
                                                  value: _vm.ratings.content,
                                                  callback: function($$v) {
                                                    _vm.$set(
                                                      _vm.ratings,
                                                      "content",
                                                      $$v
                                                    )
                                                  },
                                                  expression: "ratings.content"
                                                }
                                              })
                                            ],
                                            1
                                          )
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "b-card",
                                    {
                                      staticClass:
                                        "mb-3 mb-sm-4 custom-accordion",
                                      attrs: { "no-body": "" }
                                    },
                                    [
                                      _c(
                                        "b-card-header",
                                        {
                                          staticClass: "p-1",
                                          attrs: {
                                            "header-tag": "header",
                                            role: "tab"
                                          }
                                        },
                                        [
                                          _c(
                                            "b-btn",
                                            {
                                              directives: [
                                                {
                                                  name: "b-toggle",
                                                  rawName:
                                                    "v-b-toggle.accordion2",
                                                  modifiers: {
                                                    accordion2: true
                                                  }
                                                }
                                              ],
                                              attrs: { block: "", href: "#" }
                                            },
                                            [
                                              _vm._v(
                                                "\n                              Gameplay\n                              "
                                              ),
                                              _c(
                                                "span",
                                                { staticClass: "text-right" },
                                                [
                                                  _vm._v(
                                                    "\n                                Total Score : 00\n                              "
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "b-collapse",
                                        {
                                          attrs: {
                                            id: "accordion2",
                                            accordion: "my-accordion",
                                            role: "tabpanel"
                                          }
                                        },
                                        [
                                          _c(
                                            "b-card-body",
                                            [
                                              _c("starRatings", {
                                                model: {
                                                  value: _vm.ratings.gameplay,
                                                  callback: function($$v) {
                                                    _vm.$set(
                                                      _vm.ratings,
                                                      "gameplay",
                                                      $$v
                                                    )
                                                  },
                                                  expression: "ratings.gameplay"
                                                }
                                              })
                                            ],
                                            1
                                          )
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "b-card",
                                    {
                                      staticClass:
                                        "mb-3 mb-sm-4 custom-accordion",
                                      attrs: { "no-body": "" }
                                    },
                                    [
                                      _c(
                                        "b-card-header",
                                        {
                                          staticClass: "p-1",
                                          attrs: {
                                            "header-tag": "header",
                                            role: "tab"
                                          }
                                        },
                                        [
                                          _c(
                                            "b-btn",
                                            {
                                              directives: [
                                                {
                                                  name: "b-toggle",
                                                  rawName:
                                                    "v-b-toggle.accordion3",
                                                  modifiers: {
                                                    accordion3: true
                                                  }
                                                }
                                              ],
                                              attrs: { block: "", href: "#" }
                                            },
                                            [
                                              _vm._v(
                                                "\n                              multimedia\n                              "
                                              ),
                                              _c(
                                                "span",
                                                { staticClass: "text-right" },
                                                [
                                                  _vm._v(
                                                    "\n                                Total Score : 00\n                              "
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "b-collapse",
                                        {
                                          attrs: {
                                            id: "accordion3",
                                            accordion: "my-accordion",
                                            role: "tabpanel"
                                          }
                                        },
                                        [
                                          _c(
                                            "b-card-body",
                                            [
                                              _c("starRatings", {
                                                model: {
                                                  value: _vm.ratings.multimedia,
                                                  callback: function($$v) {
                                                    _vm.$set(
                                                      _vm.ratings,
                                                      "multimedia",
                                                      $$v
                                                    )
                                                  },
                                                  expression:
                                                    "ratings.multimedia"
                                                }
                                              })
                                            ],
                                            1
                                          )
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "h4",
                                { staticClass: "total-score-txt mb-0" },
                                [
                                  _vm._v(
                                    "\n                        Total Score: 79\n                        "
                                  ),
                                  _c(
                                    "span",
                                    { staticClass: "effective-badge" },
                                    [_vm._v("Effective")]
                                  )
                                ]
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c("b-tab", { attrs: { title: "Issues" } }, [
                            _c("div", { staticClass: "tester-issues-tab" }, [
                              _c(
                                "div",
                                { staticClass: "issue-block" },
                                _vm._l(_vm.issueTypes, function(
                                  issueType,
                                  issueIndex
                                ) {
                                  return _c(
                                    "div",
                                    { key: issueIndex, staticClass: "row" },
                                    [
                                      _c("div", { staticClass: "col-12" }, [
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "icon-wrap d-inline-block align-middle"
                                          },
                                          [
                                            _c("img", {
                                              staticClass: "img-fluid",
                                              attrs: { src: issueType.img }
                                            })
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "content-wrap d-inline-block align-middle"
                                          },
                                          [
                                            _c(
                                              "p",
                                              {
                                                staticClass:
                                                  "text-bold text-green issue-type"
                                              },
                                              [
                                                _vm._v(
                                                  " " + _vm._s(issueType.title)
                                                )
                                              ]
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c("p", { staticClass: "mt-1" }, [
                                          _vm._v(_vm._s(issueType.typeDesc))
                                        ])
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "col-12" }, [
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "form-group mb-3 mb-md-5"
                                          },
                                          _vm._l(issueType.bugs, function(
                                            bug,
                                            bugIndex
                                          ) {
                                            return _c(
                                              "div",
                                              {
                                                key: bugIndex,
                                                staticClass: "issue-link"
                                              },
                                              [
                                                _c(
                                                  "div",
                                                  { staticClass: "row" },
                                                  [
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "col-10 col-md-11"
                                                      },
                                                      [
                                                        _c("input", {
                                                          directives: [
                                                            {
                                                              name: "model",
                                                              rawName:
                                                                "v-model",
                                                              value: bug.desc,
                                                              expression:
                                                                "bug.desc"
                                                            }
                                                          ],
                                                          staticClass:
                                                            "form-control custom-form-control",
                                                          attrs: {
                                                            type: "text",
                                                            "v-name": bug.desc,
                                                            placeholder: ""
                                                          },
                                                          domProps: {
                                                            value: bug.desc
                                                          },
                                                          on: {
                                                            input: function(
                                                              $event
                                                            ) {
                                                              if (
                                                                $event.target
                                                                  .composing
                                                              ) {
                                                                return
                                                              }
                                                              _vm.$set(
                                                                bug,
                                                                "desc",
                                                                $event.target
                                                                  .value
                                                              )
                                                            }
                                                          }
                                                        })
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    issueType.bugs.length ==
                                                    bugIndex + 1
                                                      ? _c(
                                                          "div",
                                                          {
                                                            staticClass:
                                                              "col-2 col-md-1"
                                                          },
                                                          [
                                                            _c(
                                                              "div",
                                                              {
                                                                staticClass:
                                                                  "remove-icon",
                                                                on: {
                                                                  click: function(
                                                                    $event
                                                                  ) {
                                                                    _vm.addField(
                                                                      issueIndex,
                                                                      bugIndex
                                                                    )
                                                                  }
                                                                }
                                                              },
                                                              [
                                                                _c("i", {
                                                                  staticClass:
                                                                    "fa fa-plus-circle"
                                                                })
                                                              ]
                                                            )
                                                          ]
                                                        )
                                                      : _c(
                                                          "div",
                                                          {
                                                            staticClass:
                                                              "col-2 col-md-1"
                                                          },
                                                          [
                                                            _c(
                                                              "div",
                                                              {
                                                                staticClass:
                                                                  "remove-icon",
                                                                on: {
                                                                  click: function(
                                                                    $event
                                                                  ) {
                                                                    _vm.removeField(
                                                                      issueIndex,
                                                                      bugIndex
                                                                    )
                                                                  }
                                                                }
                                                              },
                                                              [
                                                                _c("i", {
                                                                  staticClass:
                                                                    "fa fa-minus-circle"
                                                                })
                                                              ]
                                                            )
                                                          ]
                                                        )
                                                  ]
                                                )
                                              ]
                                            )
                                          })
                                        )
                                      ])
                                    ]
                                  )
                                })
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c("b-tab", { attrs: { title: "Attachment" } }, [
                            _c(
                              "div",
                              { staticClass: "tester-attachment-tab" },
                              [
                                _c("div", { staticClass: "row" }, [
                                  _c("div", { staticClass: "col-12" }, [
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "text-uppercase green-step-btn add-files-btn"
                                      },
                                      [
                                        _c("span", [
                                          _vm._v(
                                            "\n                              Add Files\n                            "
                                          )
                                        ]),
                                        _vm._v(" "),
                                        _c("input", {
                                          staticClass: "btn",
                                          attrs: { type: "file" }
                                        })
                                      ]
                                    )
                                  ])
                                ]),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "attachment-table-wrap" },
                                  [
                                    _c("div", { staticClass: "row" }, [
                                      _c("div", { staticClass: "col-12" }, [
                                        _c(
                                          "div",
                                          { staticClass: "table-responsive" },
                                          [
                                            _c(
                                              "table",
                                              {
                                                staticClass:
                                                  "table table-bordered attachment-table"
                                              },
                                              [
                                                _c("tbody", [
                                                  _c("tr", [
                                                    _c(
                                                      "td",
                                                      {
                                                        attrs: { scope: "row" }
                                                      },
                                                      [
                                                        _c("img", {
                                                          staticClass:
                                                            "img-fluid",
                                                          attrs: {
                                                            src: __webpack_require__("./resources/assets/assets/img/mp4-icon.png")
                                                          }
                                                        })
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "td",
                                                      {
                                                        staticClass:
                                                          " text-medium"
                                                      },
                                                      [_vm._v("File Name.mp4")]
                                                    ),
                                                    _vm._v(" "),
                                                    _c("td", [
                                                      _vm._v("236 MB")
                                                    ]),
                                                    _vm._v(" "),
                                                    _c(
                                                      "td",
                                                      {
                                                        staticClass:
                                                          "text-center"
                                                      },
                                                      [
                                                        _c(
                                                          "a",
                                                          {
                                                            staticClass:
                                                              "d-inline-block",
                                                            attrs: { href: "#" }
                                                          },
                                                          [
                                                            _c("i", {
                                                              staticClass:
                                                                "fa fa-eye"
                                                            })
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "a",
                                                          {
                                                            staticClass:
                                                              "d-inline-block",
                                                            attrs: { href: "#" }
                                                          },
                                                          [
                                                            _c("i", {
                                                              staticClass:
                                                                "fa fa-trash"
                                                            })
                                                          ]
                                                        )
                                                      ]
                                                    )
                                                  ]),
                                                  _vm._v(" "),
                                                  _c("tr", [
                                                    _c(
                                                      "td",
                                                      {
                                                        attrs: { scope: "row" }
                                                      },
                                                      [
                                                        _c("img", {
                                                          staticClass:
                                                            "img-fluid",
                                                          attrs: {
                                                            src: __webpack_require__("./resources/assets/assets/img/pdf-icon.png")
                                                          }
                                                        })
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "td",
                                                      {
                                                        staticClass:
                                                          "text-medium"
                                                      },
                                                      [
                                                        _vm._v(
                                                          "File Name_2.pdf"
                                                        )
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c("td", [
                                                      _vm._v("112 KB")
                                                    ]),
                                                    _vm._v(" "),
                                                    _c(
                                                      "td",
                                                      {
                                                        staticClass:
                                                          "text-center"
                                                      },
                                                      [
                                                        _c(
                                                          "a",
                                                          {
                                                            staticClass:
                                                              "d-inline-block",
                                                            attrs: { href: "#" }
                                                          },
                                                          [
                                                            _c("i", {
                                                              staticClass:
                                                                "fa fa-eye"
                                                            })
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "a",
                                                          {
                                                            staticClass:
                                                              "d-inline-block",
                                                            attrs: { href: "#" }
                                                          },
                                                          [
                                                            _c("i", {
                                                              staticClass:
                                                                "fa fa-trash"
                                                            })
                                                          ]
                                                        )
                                                      ]
                                                    )
                                                  ]),
                                                  _vm._v(" "),
                                                  _c("tr", [
                                                    _c(
                                                      "td",
                                                      {
                                                        attrs: { scope: "row" }
                                                      },
                                                      [
                                                        _c("img", {
                                                          staticClass:
                                                            "img-fluid",
                                                          attrs: {
                                                            src: __webpack_require__("./resources/assets/assets/img/png-icon.png")
                                                          }
                                                        })
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "td",
                                                      {
                                                        staticClass:
                                                          "text-medium"
                                                      },
                                                      [
                                                        _vm._v(
                                                          "File Name_3.png"
                                                        )
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c("td", [
                                                      _vm._v("1.25 MB")
                                                    ]),
                                                    _vm._v(" "),
                                                    _c(
                                                      "td",
                                                      {
                                                        staticClass:
                                                          "text-center"
                                                      },
                                                      [
                                                        _c(
                                                          "a",
                                                          {
                                                            staticClass:
                                                              "d-inline-block",
                                                            attrs: { href: "#" }
                                                          },
                                                          [
                                                            _c("i", {
                                                              staticClass:
                                                                "fa fa-eye"
                                                            })
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "a",
                                                          {
                                                            staticClass:
                                                              "d-inline-block",
                                                            attrs: { href: "#" }
                                                          },
                                                          [
                                                            _c("i", {
                                                              staticClass:
                                                                "fa fa-trash"
                                                            })
                                                          ]
                                                        )
                                                      ]
                                                    )
                                                  ]),
                                                  _vm._v(" "),
                                                  _c("tr", [
                                                    _c(
                                                      "td",
                                                      {
                                                        attrs: { scope: "row" }
                                                      },
                                                      [
                                                        _c("img", {
                                                          staticClass:
                                                            "img-fluid",
                                                          attrs: {
                                                            src: __webpack_require__("./resources/assets/assets/img/jpg-icon.png")
                                                          }
                                                        })
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "td",
                                                      {
                                                        staticClass:
                                                          "text-medium"
                                                      },
                                                      [
                                                        _vm._v(
                                                          "File Name_4.jpg"
                                                        )
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c("td", [
                                                      _vm._v("1.25 MB")
                                                    ]),
                                                    _vm._v(" "),
                                                    _c(
                                                      "td",
                                                      {
                                                        staticClass:
                                                          "text-center"
                                                      },
                                                      [
                                                        _c(
                                                          "a",
                                                          {
                                                            staticClass:
                                                              "d-inline-block",
                                                            attrs: { href: "#" }
                                                          },
                                                          [
                                                            _c("i", {
                                                              staticClass:
                                                                "fa fa-eye"
                                                            })
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "a",
                                                          {
                                                            staticClass:
                                                              "d-inline-block",
                                                            attrs: { href: "#" }
                                                          },
                                                          [
                                                            _c("i", {
                                                              staticClass:
                                                                "fa fa-trash"
                                                            })
                                                          ]
                                                        )
                                                      ]
                                                    )
                                                  ])
                                                ])
                                              ]
                                            )
                                          ]
                                        )
                                      ])
                                    ])
                                  ]
                                )
                              ]
                            )
                          ])
                        ],
                        1
                      )
                    ],
                    1
                  )
                ])
              ]),
              _vm._v(" "),
              _vm.isSubmitted
                ? _c("div", { staticClass: "white-box" }, [
                    _c("div", { staticClass: "row" }, [
                      _c(
                        "div",
                        { staticClass: "col-12" },
                        [_c("project-testing-process-review")],
                        1
                      )
                    ])
                  ])
                : _vm._e()
            ])
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-12 text-center" }, [
        _c(
          "a",
          {
            staticClass: "btn text-uppercase green-step-btn",
            on: {
              click: function($event) {
                _vm.save(true)
              }
            }
          },
          [_vm._v("review and submit")]
        ),
        _vm._v(" "),
        _c(
          "a",
          {
            staticClass: "btn text-uppercase green-step-btn",
            on: {
              click: function($event) {
                _vm.save()
              }
            }
          },
          [_vm._v("save as draft")]
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-12" }, [
      _c("h4", { staticClass: "block-title border-bottom-0" }, [
        _vm._v("\n                Project Status\n              ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12" }, [
        _c("h4", { staticClass: "block-title" }, [
          _vm._v("\n                Testing Process\n              ")
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-259f8d43", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-9c7ea048\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/tester/test-project-details.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "white-box" }, [
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12" }, [
        _c("h4", { staticClass: "text-bold block-title proj-name-wrap" }, [
          _c("span", { staticClass: "proj-name" }, [
            _vm._v(_vm._s(_vm.testProject.name))
          ]),
          _vm._v(" "),
          _c("span", { staticClass: "badge-wrap text-right" }, [
            _c("span", { staticClass: "status-badge" }, [
              _vm._v(
                "\n            Status:" +
                  _vm._s(_vm.currentTestStatus) +
                  "\n          "
              )
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row pb-3 pb-md-4" }, [
      _c("div", { staticClass: "col-md-3" }, [
        _c("div", {}, [
          _c("span", { staticClass: "text-bold" }, [
            _vm._v("\n          Type : \n        ")
          ]),
          _vm._v(" "),
          _c("span", {}, [
            _vm._v(
              " " +
                _vm._s(
                  _vm.testProject.project_type
                    ? _vm.testProject.project_type.name
                    : ""
                ) +
                " "
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-5" }),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-4 text-md-right text-left" }, [
        _c("span", { staticClass: "text-bold" }, [
          _vm._v("Estimated Tester Time needed to complete :")
        ]),
        _c("br"),
        _vm._v(" "),
        _c("span", {}, [
          _vm._v(_vm._s(_vm.testProject.estimate_tester_time) + " hours")
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row pb-3 pb-md-4" }, [
      _vm._m(0),
      _vm._v(" "),
      _c("div", { staticClass: "col-12" }, [
        _c("p", { staticClass: "proj-desc" }, [
          _vm._v(
            "\n        " +
              _vm._s(_vm.testProject.project_description) +
              "\n      "
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row pb-3 pb-md-4" }, [
      _c("div", { staticClass: "col-md-6 col-12 pb-md-0 pb-3" }, [
        _c("h4", { staticClass: "text-bold block-title" }, [_vm._v("Links")]),
        _vm._v(" "),
        _c(
          "div",
          {},
          _vm._l(_vm.testProject.links, function(link) {
            return _c("span", { key: link.id }, [
              _vm._v(" \n          " + _vm._s(link.title) + ":  "),
              _c("a", { attrs: { href: link.link, target: "_blank" } }, [
                _vm._v(" " + _vm._s(link.link))
              ])
            ])
          })
        )
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "col-md-6 col-12" },
        [
          _c("h4", { staticClass: "text-bold block-title" }, [_vm._v("Files")]),
          _vm._v(" "),
          _c("file-viewer", {
            attrs: { colorTheme: "tester", files: _vm.testProject.media }
          })
        ],
        1
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12 col-md-6 pb-md-0 pb-3" }, [
        _c("h4", { staticClass: "block-title text-bold" }, [
          _vm._v("Notes/Instruction")
        ]),
        _vm._v(" "),
        _c("p", { staticClass: "mb-1" }, [
          _vm._v(
            _vm._s(
              _vm.testProject.notes_instruction
                ? _vm.testProject.notes_instruction
                : "No notes given"
            )
          )
        ])
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "col-12 col-md-6" },
        [
          _c("h4", { staticClass: "block-title text-bold" }, [
            _vm._v("Required Devices")
          ]),
          _vm._v(" "),
          _c("device-selector", {
            attrs: { disabled: "" },
            model: {
              value: _vm.testProject.selected_devices,
              callback: function($$v) {
                _vm.$set(_vm.testProject, "selected_devices", $$v)
              },
              expression: "testProject.selected_devices"
            }
          })
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-12" }, [
      _c("h4", { staticClass: "text-bold block-title" }, [
        _vm._v("Project Description\n      ")
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-9c7ea048", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-d754b8aa\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/project-testing-process-review.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "testing-process-tabwrap" },
    [
      _c(
        "b-tabs",
        [
          _c(
            "b-tab",
            {
              staticClass: "active-tab",
              attrs: { title: "Questions", active: "" }
            },
            [
              _c("div", { staticClass: "tester-questions-tab" }, [
                _c("div", { staticClass: "row mb-sm-3 mb-2" }, [
                  _c("div", { staticClass: "col-12" }, [
                    _c("p", { staticClass: "mb-0 mb-sm-1" }, [
                      _c("span", { staticClass: "text-medium mr-1" }, [
                        _vm._v("Q1.")
                      ]),
                      _vm._v(" "),
                      _c("span", {}, [
                        _vm._v(
                          "\n                Did you like the ambient sounds?\n              "
                        )
                      ])
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-12" }, [
                    _c(
                      "p",
                      { staticClass: "mb-0 mb-sm-1 text-medium ml-3 pl-3" },
                      [
                        _vm._v(
                          "\n              Yes, but would like volume control\n            "
                        )
                      ]
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "row mb-sm-3 mb-2" }, [
                  _c("div", { staticClass: "col-12" }, [
                    _c("p", { staticClass: "mb-0 mb-sm-1" }, [
                      _c("span", { staticClass: "text-medium mr-1" }, [
                        _vm._v("Q2.")
                      ]),
                      _vm._v(" "),
                      _c("span", {}, [
                        _vm._v(
                          "\n                was popularised in the 1960s with the release of Letraset sheets ?\n              "
                        )
                      ])
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-12" }, [
                    _c(
                      "p",
                      { staticClass: "mb-0 mb-sm-1 text-medium ml-3 pl-3" },
                      [
                        _vm._v(
                          "\n              It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.\n            "
                        )
                      ]
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "row mb-sm-3 mb-2" }, [
                  _c("div", { staticClass: "col-12" }, [
                    _c("p", { staticClass: "mb-0 mb-sm-1" }, [
                      _c("span", { staticClass: "text-medium mr-1" }, [
                        _vm._v("Q3.")
                      ]),
                      _vm._v(" "),
                      _c("span", {}, [
                        _vm._v(
                          "\n                was popularised release of Letraset sheets ?\n              "
                        )
                      ])
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-12" }, [
                    _c(
                      "p",
                      { staticClass: "mb-0 mb-sm-1 text-medium ml-3 pl-3" },
                      [
                        _vm._v(
                          "\n              It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.\n            "
                        )
                      ]
                    )
                  ])
                ])
              ])
            ]
          ),
          _vm._v(" "),
          _c("b-tab", { attrs: { title: "Ratings/Scores" } }, [
            _c("div", { staticClass: "tester-rating-tab" }, [
              _c("h4", { staticClass: "total-score-txt" }, [
                _vm._v("\n          Total Score: 88\n          "),
                _c("span", { staticClass: "effective-badge" }, [
                  _vm._v("Exemplary")
                ])
              ]),
              _vm._v(" "),
              _c(
                "div",
                { attrs: { role: "tablist" } },
                [
                  _c(
                    "b-card",
                    {
                      staticClass: "mb-3 mb-sm-4 custom-accordion",
                      attrs: { "no-body": "" }
                    },
                    [
                      _c(
                        "b-card-header",
                        {
                          staticClass: "p-1",
                          attrs: { "header-tag": "header", role: "tab" }
                        },
                        [
                          _c(
                            "b-btn",
                            {
                              directives: [
                                {
                                  name: "b-toggle",
                                  rawName: "v-b-toggle.accordion1",
                                  modifiers: { accordion1: true }
                                }
                              ],
                              attrs: { block: "", href: "#" }
                            },
                            [
                              _vm._v(
                                "\n                content\n                "
                              ),
                              _c("span", { staticClass: "text-right" }, [
                                _vm._v(
                                  "\n                  Total Score : 00\n                "
                                )
                              ])
                            ]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "b-collapse",
                        {
                          attrs: {
                            id: "accordion1",
                            visible: "",
                            accordion: "my-accordion",
                            role: "tabpanel"
                          }
                        },
                        [
                          _c(
                            "b-card-body",
                            [
                              _c("starRatings", {
                                attrs: { disabled: true },
                                model: {
                                  value: _vm.ratings.content,
                                  callback: function($$v) {
                                    _vm.$set(_vm.ratings, "content", $$v)
                                  },
                                  expression: "ratings.content"
                                }
                              })
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "b-card",
                    {
                      staticClass: "mb-3 mb-sm-4 custom-accordion",
                      attrs: { "no-body": "" }
                    },
                    [
                      _c(
                        "b-card-header",
                        {
                          staticClass: "p-1",
                          attrs: { "header-tag": "header", role: "tab" }
                        },
                        [
                          _c(
                            "b-btn",
                            {
                              directives: [
                                {
                                  name: "b-toggle",
                                  rawName: "v-b-toggle.accordion2",
                                  modifiers: { accordion2: true }
                                }
                              ],
                              attrs: { block: "", href: "#" }
                            },
                            [
                              _vm._v(
                                "\n                Gameplay\n                "
                              ),
                              _c("span", { staticClass: "text-right" }, [
                                _vm._v(
                                  "\n                  Total Score : 00\n                "
                                )
                              ])
                            ]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "b-collapse",
                        {
                          attrs: {
                            id: "accordion2",
                            accordion: "my-accordion",
                            role: "tabpanel"
                          }
                        },
                        [
                          _c(
                            "b-card-body",
                            [
                              _c("starRatings", {
                                attrs: { disabled: true },
                                model: {
                                  value: _vm.ratings.gameplay,
                                  callback: function($$v) {
                                    _vm.$set(_vm.ratings, "gameplay", $$v)
                                  },
                                  expression: "ratings.gameplay"
                                }
                              })
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "b-card",
                    {
                      staticClass: "mb-3 mb-sm-4 custom-accordion",
                      attrs: { "no-body": "" }
                    },
                    [
                      _c(
                        "b-card-header",
                        {
                          staticClass: "p-1",
                          attrs: { "header-tag": "header", role: "tab" }
                        },
                        [
                          _c(
                            "b-btn",
                            {
                              directives: [
                                {
                                  name: "b-toggle",
                                  rawName: "v-b-toggle.accordion3",
                                  modifiers: { accordion3: true }
                                }
                              ],
                              attrs: { block: "", href: "#" }
                            },
                            [
                              _vm._v(
                                "\n                multimedia\n                "
                              ),
                              _c("span", { staticClass: "text-right" }, [
                                _vm._v(
                                  "\n                  Total Score : 00\n                "
                                )
                              ])
                            ]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "b-collapse",
                        {
                          attrs: {
                            id: "accordion3",
                            accordion: "my-accordion",
                            role: "tabpanel"
                          }
                        },
                        [
                          _c(
                            "b-card-body",
                            [
                              _c("starRatings", {
                                attrs: { disabled: true },
                                model: {
                                  value: _vm.ratings.multimedia,
                                  callback: function($$v) {
                                    _vm.$set(_vm.ratings, "multimedia", $$v)
                                  },
                                  expression: "ratings.multimedia"
                                }
                              })
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c("h4", { staticClass: "total-score-txt mb-0" }, [
                _vm._v("\n          Total Score: 88\n          "),
                _c("span", { staticClass: "effective-badge" }, [
                  _vm._v("Exemplary")
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("b-tab", { attrs: { title: "Issues" } }, [
            _c("div", { staticClass: "tester-issues-tab" }, [
              _c("div", { staticClass: "issue-block mb-md-5 mb-sm-3 mb-3" }, [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-12" }, [
                    _c(
                      "div",
                      { staticClass: "icon-wrap d-inline-block align-middle" },
                      [
                        _c("img", {
                          staticClass: "img-fluid",
                          attrs: { src: __webpack_require__("./resources/assets/assets/img/black_widow_32x32.png") }
                        })
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "content-wrap d-inline-block align-middle"
                      },
                      [
                        _c(
                          "p",
                          { staticClass: "text-bold text-green issue-type" },
                          [_vm._v("Black Widow - High Type Bugs:")]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "mt-2" }, [
                      _c("p", { staticClass: "mb-sm-3 mb-2" }, [
                        _vm._v("1. This is High priority bug one")
                      ]),
                      _vm._v(" "),
                      _c("p", { staticClass: "mb-sm-3 mb-2" }, [
                        _vm._v("2. This is High priority bug two")
                      ]),
                      _vm._v(" "),
                      _c("p", { staticClass: "mb-sm-3 mb-2" }, [
                        _vm._v("3. This is High priority bug three")
                      ])
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "issue-block mb-md-5 mb-sm-3 mb-3" }, [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-12" }, [
                    _c(
                      "div",
                      { staticClass: "icon-wrap d-inline-block align-middle" },
                      [
                        _c("img", {
                          staticClass: "img-fluid",
                          attrs: { src: __webpack_require__("./resources/assets/assets/img/wasp_32x32.png") }
                        })
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "content-wrap d-inline-block align-middle"
                      },
                      [
                        _c(
                          "p",
                          { staticClass: "text-bold text-green issue-type" },
                          [_vm._v("Wasp - Medium Type Bugs:")]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "mt-2" }, [
                      _c("p", { staticClass: "mb-sm-3 mb-2" }, [
                        _vm._v(
                          "1. There is one type B bug found, that is when i start files i will flashing splash screen two time."
                        )
                      ]),
                      _vm._v(" "),
                      _c("p", { staticClass: "mb-sm-3 mb-2" }, [
                        _vm._v(
                          "2. There is second medium bug found, that is when i start files i will flashing splash screen two time."
                        )
                      ])
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "issue-block mb-sm-0 mb-3" }, [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-12" }, [
                    _c(
                      "div",
                      { staticClass: "icon-wrap d-inline-block align-middle" },
                      [
                        _c("img", {
                          staticClass: "img-fluid",
                          attrs: { src: __webpack_require__("./resources/assets/assets/img/gnat_32x32.png") }
                        })
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "content-wrap d-inline-block align-middle"
                      },
                      [
                        _c(
                          "p",
                          { staticClass: "text-bold text-green issue-type" },
                          [_vm._v("Gnat - Low Type Bugs:")]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "mt-2" }, [
                      _c("p", { staticClass: "mb-sm-3 mb-2" }, [
                        _vm._v("1. There is one Low bug found.")
                      ])
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("hr", { staticClass: "line-seprator" }),
              _vm._v(" "),
              _c("div", { staticClass: "issue-block mb-md-5 mb-sm-3 mb-3" }, [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-12" }, [
                    _c(
                      "p",
                      { staticClass: "text-bold text-green issue-type" },
                      [
                        _vm._v(
                          "Are there any cosmetic issues you'd like to mention?"
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c("p", { staticClass: "mb-sm-3 mb-2" }, [
                      _vm._v(
                        "1. there is white screen generating during running games"
                      )
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "issue-block" }, [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-12" }, [
                    _c(
                      "p",
                      { staticClass: "text-bold text-green issue-type" },
                      [
                        _vm._v(
                          "Do you have any suggestions you would like to pass on to the Publisher?"
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c("p", { staticClass: "mb-sm-3 mb-2" }, [
                      _vm._v(
                        "1. there is white screen generating during running games"
                      )
                    ])
                  ])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("b-tab", { attrs: { title: "Attachment" } }, [
            _c("div", { staticClass: "tester-attachment-tab" }, [
              _c("div", { staticClass: "attachment-table-wrap" }, [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-12" }, [
                    _c("div", { staticClass: "table-responsive" }, [
                      _c(
                        "table",
                        {
                          staticClass: "table table-bordered attachment-table"
                        },
                        [
                          _c("tbody", [
                            _c("tr", [
                              _c("td", { attrs: { scope: "row" } }, [
                                _c("img", {
                                  staticClass: "img-fluid",
                                  attrs: { src: __webpack_require__("./resources/assets/assets/img/mp4-icon.png") }
                                })
                              ]),
                              _vm._v(" "),
                              _c("td", { staticClass: " text-medium" }, [
                                _vm._v("File Name.mp4")
                              ]),
                              _vm._v(" "),
                              _c("td", [_vm._v("236 MB")]),
                              _vm._v(" "),
                              _c("td", { staticClass: "text-center" }, [
                                _c(
                                  "a",
                                  {
                                    staticClass: "d-inline-block",
                                    attrs: { href: "#" }
                                  },
                                  [_c("i", { staticClass: "fa fa-eye" })]
                                ),
                                _vm._v(" "),
                                _c(
                                  "a",
                                  {
                                    staticClass: "d-inline-block",
                                    attrs: { href: "#" }
                                  },
                                  [_c("i", { staticClass: "fa fa-trash" })]
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _c("tr", [
                              _c("td", { attrs: { scope: "row" } }, [
                                _c("img", {
                                  staticClass: "img-fluid",
                                  attrs: { src: __webpack_require__("./resources/assets/assets/img/pdf-icon.png") }
                                })
                              ]),
                              _vm._v(" "),
                              _c("td", { staticClass: "text-medium" }, [
                                _vm._v("File Name_2.pdf")
                              ]),
                              _vm._v(" "),
                              _c("td", [_vm._v("112 KB")]),
                              _vm._v(" "),
                              _c("td", { staticClass: "text-center" }, [
                                _c(
                                  "a",
                                  {
                                    staticClass: "d-inline-block",
                                    attrs: { href: "#" }
                                  },
                                  [_c("i", { staticClass: "fa fa-eye" })]
                                ),
                                _vm._v(" "),
                                _c(
                                  "a",
                                  {
                                    staticClass: "d-inline-block",
                                    attrs: { href: "#" }
                                  },
                                  [_c("i", { staticClass: "fa fa-trash" })]
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _c("tr", [
                              _c("td", { attrs: { scope: "row" } }, [
                                _c("img", {
                                  staticClass: "img-fluid",
                                  attrs: { src: __webpack_require__("./resources/assets/assets/img/png-icon.png") }
                                })
                              ]),
                              _vm._v(" "),
                              _c("td", { staticClass: "text-medium" }, [
                                _vm._v("File Name_3.png")
                              ]),
                              _vm._v(" "),
                              _c("td", [_vm._v("1.25 MB")]),
                              _vm._v(" "),
                              _c("td", { staticClass: "text-center" }, [
                                _c(
                                  "a",
                                  {
                                    staticClass: "d-inline-block",
                                    attrs: { href: "#" }
                                  },
                                  [_c("i", { staticClass: "fa fa-eye" })]
                                ),
                                _vm._v(" "),
                                _c(
                                  "a",
                                  {
                                    staticClass: "d-inline-block",
                                    attrs: { href: "#" }
                                  },
                                  [_c("i", { staticClass: "fa fa-trash" })]
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _c("tr", [
                              _c("td", { attrs: { scope: "row" } }, [
                                _c("img", {
                                  staticClass: "img-fluid",
                                  attrs: { src: __webpack_require__("./resources/assets/assets/img/jpg-icon.png") }
                                })
                              ]),
                              _vm._v(" "),
                              _c("td", { staticClass: "text-medium" }, [
                                _vm._v("File Name_4.jpg")
                              ]),
                              _vm._v(" "),
                              _c("td", [_vm._v("1.25 MB")]),
                              _vm._v(" "),
                              _c("td", { staticClass: "text-center" }, [
                                _c(
                                  "a",
                                  {
                                    staticClass: "d-inline-block",
                                    attrs: { href: "#" }
                                  },
                                  [_c("i", { staticClass: "fa fa-eye" })]
                                ),
                                _vm._v(" "),
                                _c(
                                  "a",
                                  {
                                    staticClass: "d-inline-block",
                                    attrs: { href: "#" }
                                  },
                                  [_c("i", { staticClass: "fa fa-trash" })]
                                )
                              ])
                            ])
                          ])
                        ]
                      )
                    ])
                  ])
                ])
              ])
            ])
          ])
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-d754b8aa", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-e009baa8\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/tester/project-status-test-project.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c(
        "div",
        { staticClass: "d-inline-block text-center proj-status active" },
        [
          _c("p", { staticClass: "proj-progress" }, [
            _c("i", { staticClass: "fa fa-circle-thin" })
          ]),
          _vm._v(" "),
          _c("span", { staticClass: "d-none d-md-block text-medium" }, [
            _vm._v("New")
          ])
        ]
      ),
      _vm._v(" "),
      _c("div", { staticClass: "d-inline-block text-center proj-status" }, [
        _c("p", { staticClass: "proj-progress" }, [
          _c("i", { staticClass: "fa fa-circle-thin" })
        ]),
        _vm._v(" "),
        _c("span", { staticClass: "d-none d-md-block text-medium" }, [
          _vm._v("Accepted/Working")
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "d-inline-block text-center proj-status" }, [
        _c("p", { staticClass: "proj-progress" }, [
          _c("i", { staticClass: "fa fa-circle-thin" })
        ]),
        _vm._v(" "),
        _c("span", { staticClass: "d-none d-md-block text-medium" }, [
          _vm._v("Submitted")
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "d-inline-block text-center proj-status" }, [
        _c("p", { staticClass: "proj-progress" }, [
          _c("i", { staticClass: "fa fa-circle-thin" })
        ]),
        _vm._v(" "),
        _c("span", { staticClass: "d-none d-md-block text-medium" }, [
          _vm._v(" Under Review")
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "d-inline-block text-center proj-status" }, [
        _c("p", { staticClass: "proj-progress" }, [
          _c("i", { staticClass: "fa fa-circle-thin" })
        ]),
        _vm._v(" "),
        _c("span", { staticClass: "d-none d-md-block text-medium" }, [
          _vm._v("Completed")
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-e009baa8", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-e55f98a2\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/publisher/file-viewer.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "row" }, [
    _c(
      "div",
      { staticClass: "col-12" },
      _vm._l(_vm.files, function(file) {
        return _c("div", { key: file.id, class: ["file", _vm.colorTheme] }, [
          _c("a", { attrs: { href: file.url, target: "_blank" } }, [
            _vm._v(
              "\n     " +
                _vm._s(file.file_name) +
                " - " +
                _vm._s(_vm._f("fileSize")(file.size)) +
                "\n    "
            )
          ])
        ])
      })
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-e55f98a2", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-ed68c038\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/project/rating.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "detail-block-wrap mb-3" }, [
    _c("h4", [_vm._v("Visual Accuracy")]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        {
          staticClass: "col-12",
          class: { "col-xl-12": _vm.disabled, "col-xl-10": !_vm.disabled }
        },
        [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "ratings-block" },
            _vm._l(_vm.starBlocks, function(item, i) {
              return _c(
                "div",
                {
                  key: i,
                  staticClass: "rating-item",
                  class: {
                    first: i === 0,
                    last: i === _vm.starBlocks.length - 1,
                    fill: _vm.ratingValue.score >= item
                  },
                  style: _vm.ratingItemStyle,
                  on: {
                    mouseover: function($event) {
                      _vm.setRating(item)
                    },
                    mouseout: function($event) {
                      _vm.setRating(_vm.value.score)
                    },
                    click: function($event) {
                      _vm.selected(item)
                    }
                  }
                },
                [
                  _vm._v("\n          " + _vm._s(item) + " "),
                  _c("i", { staticClass: "fa fa-star-o" })
                ]
              )
            })
          )
        ]
      ),
      _vm._v(" "),
      !_vm.disabled
        ? _c("div", { staticClass: "col-12 col-xl-2 pl-xl-0 float-right" }, [
            _c("div", { staticClass: "green-step-btn-wrap" }, [
              _c(
                "a",
                {
                  staticClass: "green-step-btn",
                  on: {
                    click: function($event) {
                      _vm.add_suggestion = !_vm.add_suggestion
                    }
                  }
                },
                [_vm._v("add explaination/ suggestion")]
              )
            ])
          ])
        : _vm._e()
    ]),
    _vm._v(" "),
    _vm.add_suggestion
      ? _c("div", { staticClass: "explanation-wrap mt-2" }, [
          _c("textarea", {
            staticClass: "form-control",
            attrs: { placeholder: "your explanation", rows: "2" },
            domProps: { value: _vm.ratingValue.suggestion },
            on: { input: _vm.addSuggestion }
          })
        ])
      : _vm._e(),
    _vm._v(" "),
    !_vm.add_suggestion && _vm.ratingValue.suggestion
      ? _c("div", { staticClass: "explanation-wrap mt-2" }, [
          _vm._v(_vm._s(_vm.ratingValue.suggestion))
        ])
      : _vm._e()
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "detail-block" }, [
      _c("span", { staticClass: "d-none d-md-inline-block" }, [
        _vm._v(
          "\n          Physics issues, bugs/image jitters. Causes motion sickness\n        "
        )
      ]),
      _vm._v(" "),
      _c("span", { staticClass: "text-center d-none d-md-inline-block" }, [
        _vm._v(
          "\n          Some good representations, some poor representations.\n        "
        )
      ]),
      _vm._v(" "),
      _c("span", { staticClass: "text-right d-none d-md-inline-block" }, [
        _vm._v(
          "\n          No bugs, jitters, image is accurately represented. No motion sickness.\n        "
        )
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-ed68c038", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-eea29f5a\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/publisher/steps.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { class: ["steps-container ", _vm.projectTypeClass] },
    _vm._l(_vm.steps, function(step) {
      return _c(
        "div",
        {
          key: step.id,
          class: [
            "step-wrap",
            {
              active: _vm.isCompleted(step.id),
              current_active_step: step.id === _vm.active
            }
          ]
        },
        [
          _c("div", { staticClass: "step" }, [
            _c("div", {
              staticClass: "line",
              class: { fill: step.id <= _vm.active }
            }),
            _vm._v(" "),
            _vm._m(0, true),
            _vm._v(" "),
            _c("div", {
              staticClass: "line",
              class: {
                fill: step.id < _vm.active || step.id === _vm.steps.length
              }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "title-container" }, [
            _c("div", { staticClass: "title" }, [_vm._v(_vm._s(step.name))])
          ])
        ]
      )
    })
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "dot" }, [
      _c("div", { staticClass: "fill" })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-eea29f5a", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-scrollto/vue-scrollto.js":
/***/ (function(module, exports, __webpack_require__) {

(function (global, factory) {
	 true ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(global['vue-scrollto'] = factory());
}(this, (function () { 'use strict';

/**
 * https://github.com/gre/bezier-easing
 * BezierEasing - use bezier curve for transition easing function
 * by Gaëtan Renaudeau 2014 - 2015 – MIT License
 */

// These values are established by empiricism with tests (tradeoff: performance VS precision)
var NEWTON_ITERATIONS = 4;
var NEWTON_MIN_SLOPE = 0.001;
var SUBDIVISION_PRECISION = 0.0000001;
var SUBDIVISION_MAX_ITERATIONS = 10;

var kSplineTableSize = 11;
var kSampleStepSize = 1.0 / (kSplineTableSize - 1.0);

var float32ArraySupported = typeof Float32Array === 'function';

function A (aA1, aA2) { return 1.0 - 3.0 * aA2 + 3.0 * aA1; }
function B (aA1, aA2) { return 3.0 * aA2 - 6.0 * aA1; }
function C (aA1)      { return 3.0 * aA1; }

// Returns x(t) given t, x1, and x2, or y(t) given t, y1, and y2.
function calcBezier (aT, aA1, aA2) { return ((A(aA1, aA2) * aT + B(aA1, aA2)) * aT + C(aA1)) * aT; }

// Returns dx/dt given t, x1, and x2, or dy/dt given t, y1, and y2.
function getSlope (aT, aA1, aA2) { return 3.0 * A(aA1, aA2) * aT * aT + 2.0 * B(aA1, aA2) * aT + C(aA1); }

function binarySubdivide (aX, aA, aB, mX1, mX2) {
  var currentX, currentT, i = 0;
  do {
    currentT = aA + (aB - aA) / 2.0;
    currentX = calcBezier(currentT, mX1, mX2) - aX;
    if (currentX > 0.0) {
      aB = currentT;
    } else {
      aA = currentT;
    }
  } while (Math.abs(currentX) > SUBDIVISION_PRECISION && ++i < SUBDIVISION_MAX_ITERATIONS);
  return currentT;
}

function newtonRaphsonIterate (aX, aGuessT, mX1, mX2) {
 for (var i = 0; i < NEWTON_ITERATIONS; ++i) {
   var currentSlope = getSlope(aGuessT, mX1, mX2);
   if (currentSlope === 0.0) {
     return aGuessT;
   }
   var currentX = calcBezier(aGuessT, mX1, mX2) - aX;
   aGuessT -= currentX / currentSlope;
 }
 return aGuessT;
}

var src = function bezier (mX1, mY1, mX2, mY2) {
  if (!(0 <= mX1 && mX1 <= 1 && 0 <= mX2 && mX2 <= 1)) {
    throw new Error('bezier x values must be in [0, 1] range');
  }

  // Precompute samples table
  var sampleValues = float32ArraySupported ? new Float32Array(kSplineTableSize) : new Array(kSplineTableSize);
  if (mX1 !== mY1 || mX2 !== mY2) {
    for (var i = 0; i < kSplineTableSize; ++i) {
      sampleValues[i] = calcBezier(i * kSampleStepSize, mX1, mX2);
    }
  }

  function getTForX (aX) {
    var intervalStart = 0.0;
    var currentSample = 1;
    var lastSample = kSplineTableSize - 1;

    for (; currentSample !== lastSample && sampleValues[currentSample] <= aX; ++currentSample) {
      intervalStart += kSampleStepSize;
    }
    --currentSample;

    // Interpolate to provide an initial guess for t
    var dist = (aX - sampleValues[currentSample]) / (sampleValues[currentSample + 1] - sampleValues[currentSample]);
    var guessForT = intervalStart + dist * kSampleStepSize;

    var initialSlope = getSlope(guessForT, mX1, mX2);
    if (initialSlope >= NEWTON_MIN_SLOPE) {
      return newtonRaphsonIterate(aX, guessForT, mX1, mX2);
    } else if (initialSlope === 0.0) {
      return guessForT;
    } else {
      return binarySubdivide(aX, intervalStart, intervalStart + kSampleStepSize, mX1, mX2);
    }
  }

  return function BezierEasing (x) {
    if (mX1 === mY1 && mX2 === mY2) {
      return x; // linear
    }
    // Because JavaScript number are imprecise, we should guarantee the extremes are right.
    if (x === 0) {
      return 0;
    }
    if (x === 1) {
      return 1;
    }
    return calcBezier(getTForX(x), mY1, mY2);
  };
};

var easings = {
    ease: [0.25, 0.1, 0.25, 1.0],
    linear: [0.00, 0.0, 1.00, 1.0],
    "ease-in": [0.42, 0.0, 1.00, 1.0],
    "ease-out": [0.00, 0.0, 0.58, 1.0],
    "ease-in-out": [0.42, 0.0, 0.58, 1.0]
};

// https://github.com/WICG/EventListenerOptions/blob/gh-pages/explainer.md#feature-detection
var supportsPassive = false;
try {
    var opts = Object.defineProperty({}, "passive", {
        get: function get() {
            supportsPassive = true;
        }
    });
    window.addEventListener("test", null, opts);
} catch (e) {}

var _ = {
    $: function $(selector) {
        if (typeof selector !== "string") {
            return selector;
        }
        return document.querySelector(selector);
    },
    on: function on(element, events, handler) {
        var opts = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : { passive: false };

        if (!(events instanceof Array)) {
            events = [events];
        }
        for (var i = 0; i < events.length; i++) {
            element.addEventListener(events[i], handler, supportsPassive ? opts : false);
        }
    },
    off: function off(element, events, handler) {
        if (!(events instanceof Array)) {
            events = [events];
        }
        for (var i = 0; i < events.length; i++) {
            element.removeEventListener(events[i], handler);
        }
    },
    cumulativeOffset: function cumulativeOffset(element) {
        var top = 0;
        var left = 0;

        do {
            top += element.offsetTop || 0;
            left += element.offsetLeft || 0;
            element = element.offsetParent;
        } while (element);

        return {
            top: top,
            left: left
        };
    }
};

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
  return typeof obj;
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
};





















var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var abortEvents = ["mousedown", "wheel", "DOMMouseScroll", "mousewheel", "keyup", "touchmove"];

var defaults$$1 = {
    container: "body",
    duration: 500,
    easing: "ease",
    offset: 0,
    cancelable: true,
    onDone: false,
    onCancel: false,
    x: false,
    y: true
};

function setDefaults(options) {
    defaults$$1 = _extends({}, defaults$$1, options);
}

var scroller = function scroller() {
    var element = void 0; // element to scroll to
    var container = void 0; // container to scroll
    var duration = void 0; // duration of the scrolling
    var easing = void 0; // easing to be used when scrolling
    var offset = void 0; // offset to be added (subtracted)
    var cancelable = void 0; // indicates if user can cancel the scroll or not.
    var onDone = void 0; // callback when scrolling is done
    var onCancel = void 0; // callback when scrolling is canceled / aborted
    var x = void 0; // scroll on x axis
    var y = void 0; // scroll on y axis

    var initialX = void 0; // initial X of container
    var targetX = void 0; // target X of container
    var initialY = void 0; // initial Y of container
    var targetY = void 0; // target Y of container
    var diffX = void 0; // difference
    var diffY = void 0; // difference

    var abort = void 0; // is scrolling aborted

    var abortEv = void 0; // event that aborted scrolling
    var abortFn = function abortFn(e) {
        if (!cancelable) return;
        abortEv = e;
        abort = true;
    };
    var easingFn = void 0;

    var timeStart = void 0; // time when scrolling started
    var timeElapsed = void 0; // time elapsed since scrolling started

    var progress = void 0; // progress

    function scrollTop(container) {
        var scrollTop = container.scrollTop;

        if (container.tagName.toLowerCase() === "body") {
            // in firefox body.scrollTop always returns 0
            // thus if we are trying to get scrollTop on a body tag
            // we need to get it from the documentElement
            scrollTop = scrollTop || document.documentElement.scrollTop;
        }

        return scrollTop;
    }

    function scrollLeft(container) {
        var scrollLeft = container.scrollLeft;

        if (container.tagName.toLowerCase() === "body") {
            // in firefox body.scrollLeft always returns 0
            // thus if we are trying to get scrollLeft on a body tag
            // we need to get it from the documentElement
            scrollLeft = scrollLeft || document.documentElement.scrollLeft;
        }

        return scrollLeft;
    }

    function step(timestamp) {
        if (abort) return done();
        if (!timeStart) timeStart = timestamp;

        timeElapsed = timestamp - timeStart;

        progress = Math.min(timeElapsed / duration, 1);
        progress = easingFn(progress);

        topLeft(container, initialY + diffY * progress, initialX + diffX * progress);

        timeElapsed < duration ? window.requestAnimationFrame(step) : done();
    }

    function done() {
        if (!abort) topLeft(container, targetY, targetX);
        timeStart = false;

        _.off(container, abortEvents, abortFn);
        if (abort && onCancel) onCancel(abortEv);
        if (!abort && onDone) onDone();
    }

    function topLeft(element, top, left) {
        if (y) element.scrollTop = top;
        if (x) element.scrollLeft = left;
        if (element.tagName.toLowerCase() === "body") {
            // in firefox body.scrollTop doesn't scroll the page
            // thus if we are trying to scrollTop on a body tag
            // we need to scroll on the documentElement
            if (y) document.documentElement.scrollTop = top;
            if (x) document.documentElement.scrollLeft = left;
        }
    }

    function scrollTo(target, _duration) {
        var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

        if ((typeof _duration === "undefined" ? "undefined" : _typeof(_duration)) === "object") {
            options = _duration;
        } else if (typeof _duration === "number") {
            options.duration = _duration;
        }

        element = _.$(target);

        if (!element) {
            return console.warn("[vue-scrollto warn]: Trying to scroll to an element that is not on the page: " + target);
        }

        container = _.$(options.container || defaults$$1.container);
        duration = options.duration || defaults$$1.duration;
        easing = options.easing || defaults$$1.easing;
        offset = options.offset || defaults$$1.offset;
        cancelable = options.hasOwnProperty("cancelable") ? options.cancelable !== false : defaults$$1.cancelable;
        onDone = options.onDone || defaults$$1.onDone;
        onCancel = options.onCancel || defaults$$1.onCancel;
        x = options.x === undefined ? defaults$$1.x : options.x;
        y = options.y === undefined ? defaults$$1.y : options.y;

        var cumulativeOffsetContainer = _.cumulativeOffset(container);
        var cumulativeOffsetElement = _.cumulativeOffset(element);

        if (typeof offset === "function") {
            offset = offset();
        }

        initialY = scrollTop(container);
        targetY = cumulativeOffsetElement.top - cumulativeOffsetContainer.top + offset;

        initialX = scrollLeft(container);
        targetX = cumulativeOffsetElement.left - cumulativeOffsetContainer.left + offset;

        abort = false;

        diffY = targetY - initialY;
        diffX = targetX - initialX;

        if (typeof easing === "string") {
            easing = easings[easing] || easings["ease"];
        }

        easingFn = src.apply(src, easing);

        if (!diffY && !diffX) return;

        _.on(container, abortEvents, abortFn, { passive: true });

        window.requestAnimationFrame(step);

        return function () {
            abortEv = null;
            abort = true;
        };
    }

    return scrollTo;
};

var _scroller = scroller();

var bindings = []; // store binding data

function deleteBinding(el) {
    for (var i = 0; i < bindings.length; ++i) {
        if (bindings[i].el === el) {
            bindings.splice(i, 1);
            return true;
        }
    }
    return false;
}

function findBinding(el) {
    for (var i = 0; i < bindings.length; ++i) {
        if (bindings[i].el === el) {
            return bindings[i];
        }
    }
}

function getBinding(el) {
    var binding = findBinding(el);

    if (binding) {
        return binding;
    }

    bindings.push(binding = {
        el: el,
        binding: {}
    });

    return binding;
}

function handleClick(e) {
    e.preventDefault();
    var ctx = getBinding(this).binding;

    if (typeof ctx.value === "string") {
        return _scroller(ctx.value);
    }
    _scroller(ctx.value.el || ctx.value.element, ctx.value);
}

var VueScrollTo$1 = {
    bind: function bind(el, binding) {
        getBinding(el).binding = binding;
        _.on(el, "click", handleClick);
    },
    unbind: function unbind(el) {
        deleteBinding(el);
        _.off(el, "click", handleClick);
    },
    update: function update(el, binding) {
        getBinding(el).binding = binding;
    },

    scrollTo: _scroller,
    bindings: bindings
};

var install = function install(Vue, options) {
    if (options) setDefaults(options);
    Vue.directive("scroll-to", VueScrollTo$1);
    Vue.prototype.$scrollTo = VueScrollTo$1.scrollTo;
};

if (typeof window !== "undefined" && window.Vue) {
    window.VueScrollTo = VueScrollTo$1;
    window.VueScrollTo.setDefaults = setDefaults;
    Vue.use(install);
}

VueScrollTo$1.install = install;

return VueScrollTo$1;

})));


/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/device-selector.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/device-selector.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("4b79eeec", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./device-selector.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./device-selector.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-259f8d43\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/project-submission.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-259f8d43\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/project-submission.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("d1f52998", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-259f8d43\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project-submission.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-259f8d43\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project-submission.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-9c7ea048\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/tester/test-project-details.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-9c7ea048\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/tester/test-project-details.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("145f1b88", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-9c7ea048\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./test-project-details.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-9c7ea048\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./test-project-details.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-d754b8aa\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project-testing-process-review.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-d754b8aa\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project-testing-process-review.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("23a63f04", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-d754b8aa\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project-testing-process-review.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-d754b8aa\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project-testing-process-review.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e009baa8\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/tester/project-status-test-project.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e009baa8\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/tester/project-status-test-project.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("355a0aee", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e009baa8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project-status-test-project.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e009baa8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project-status-test-project.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e55f98a2\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/file-viewer.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e55f98a2\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/file-viewer.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("a93db18a", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e55f98a2\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./file-viewer.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e55f98a2\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./file-viewer.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-ed68c038\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project/rating.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-ed68c038\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project/rating.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("828170ac", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-ed68c038\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./rating.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-ed68c038\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./rating.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-eea29f5a\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/steps.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-eea29f5a\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/steps.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("756018d8", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-eea29f5a\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./steps.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-eea29f5a\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./steps.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/assets/img/black_widow_32x32.png":
/***/ (function(module, exports) {

module.exports = "/images/black_widow_32x32.png?2fb61aed5a0708d85450c285dba0adc3";

/***/ }),

/***/ "./resources/assets/assets/img/down-arrow.png":
/***/ (function(module, exports) {

module.exports = "/images/down-arrow.png?afd9f6fee15e6136539c501c6456bdb2";

/***/ }),

/***/ "./resources/assets/assets/img/gnat_32x32.png":
/***/ (function(module, exports) {

module.exports = "/images/gnat_32x32.png?7bb36c83afb5f5fbedb69bf293dd23e8";

/***/ }),

/***/ "./resources/assets/assets/img/jpg-icon.png":
/***/ (function(module, exports) {

module.exports = "/images/jpg-icon.png?02d417bf68e3ed0a3c80849c3dd39f62";

/***/ }),

/***/ "./resources/assets/assets/img/mp4-icon.png":
/***/ (function(module, exports) {

module.exports = "/images/mp4-icon.png?75fbd58bfab425dab9fe6c627a10b6d4";

/***/ }),

/***/ "./resources/assets/assets/img/pdf-icon.png":
/***/ (function(module, exports) {

module.exports = "/images/pdf-icon.png?0524da1a544c06be796160a67dbfba92";

/***/ }),

/***/ "./resources/assets/assets/img/png-icon.png":
/***/ (function(module, exports) {

module.exports = "/images/png-icon.png?866941ffb652edd5a4746535d58f5cb9";

/***/ }),

/***/ "./resources/assets/assets/img/right-arrow.png":
/***/ (function(module, exports) {

module.exports = "/images/right-arrow.png?2464ea6c943c56469cc6289852f7a3e4";

/***/ }),

/***/ "./resources/assets/assets/img/wasp_32x32.png":
/***/ (function(module, exports) {

module.exports = "/images/wasp_32x32.png?a09bcd0c2d238ce332a3c7abf944212e";

/***/ }),

/***/ "./resources/assets/components/components/device-selector.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/device-selector.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/device-selector.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-0d653902\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/device-selector.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-0d653902"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\device-selector.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0d653902", Component.options)
  } else {
    hotAPI.reload("data-v-0d653902", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/components/project-testing-process-review.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-d754b8aa\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project-testing-process-review.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/project-testing-process-review.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-d754b8aa\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/project-testing-process-review.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-d754b8aa"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\project-testing-process-review.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-d754b8aa", Component.options)
  } else {
    hotAPI.reload("data-v-d754b8aa", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/components/project/rating.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-ed68c038\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project/rating.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/project/rating.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-ed68c038\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/project/rating.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-ed68c038"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\project\\rating.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-ed68c038", Component.options)
  } else {
    hotAPI.reload("data-v-ed68c038", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/components/publisher/file-viewer.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e55f98a2\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/file-viewer.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/publisher/file-viewer.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-e55f98a2\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/publisher/file-viewer.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-e55f98a2"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\publisher\\file-viewer.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e55f98a2", Component.options)
  } else {
    hotAPI.reload("data-v-e55f98a2", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/components/publisher/steps.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-eea29f5a\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/steps.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/publisher/steps.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-eea29f5a\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/publisher/steps.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-eea29f5a"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\publisher\\steps.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-eea29f5a", Component.options)
  } else {
    hotAPI.reload("data-v-eea29f5a", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/components/tester/project-status-test-project.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e009baa8\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/tester/project-status-test-project.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/tester/project-status-test-project.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-e009baa8\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/tester/project-status-test-project.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-e009baa8"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\tester\\project-status-test-project.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e009baa8", Component.options)
  } else {
    hotAPI.reload("data-v-e009baa8", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/components/tester/test-project-details.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-9c7ea048\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/tester/test-project-details.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/tester/test-project-details.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-9c7ea048\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/tester/test-project-details.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-9c7ea048"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\tester\\test-project-details.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-9c7ea048", Component.options)
  } else {
    hotAPI.reload("data-v-9c7ea048", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/pages/tester/project-submission.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-259f8d43\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/project-submission.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/tester/project-submission.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-259f8d43\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/tester/project-submission.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-259f8d43"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\tester\\project-submission.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-259f8d43", Component.options)
  } else {
    hotAPI.reload("data-v-259f8d43", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/services/test-project.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = testProjectStatusWeight;
var TestProjectStatusMap = {
  "new": 1,
  "accepted": 2,
  "submitted": 3,
  "under review": 4,
  "test passed": 5,
  "test failed": 5
};

function testProjectStatusWeight(status) {
  if (!status) {
    return false;
  }
  if (status == "Submitted") {
    status = "under review";
  }
  var statusWeight = TestProjectStatusMap[status.toLowerCase()];
  return statusWeight;
}

/***/ })

});