webpackJsonp([6],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/device/add.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__("./node_modules/vue/dist/vue.common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_form__ = __webpack_require__("./node_modules/vue-form/dist/vue-form.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_form___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_vue_form__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_src_validations_validations_js__ = __webpack_require__("./resources/assets/validations/validations.js");
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




__WEBPACK_IMPORTED_MODULE_0_vue___default.a.use(__WEBPACK_IMPORTED_MODULE_1_vue_form___default.a, __WEBPACK_IMPORTED_MODULE_2_src_validations_validations_js__["a" /* default */]);
var cleanData = {
  name: "",
  status: "Active"
};
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "add_device",
  data: function data() {
    return {
      formstate: {},
      header: this.$route.params.id ? "Edit Device" : "Add Device",
      model: _extends({}, cleanData),
      errors: null,
      old_file: "",
      originalDevice: {},
      iconFile: null,
      previewUrl: "/images/device_no_icon.png"
    };
  },

  methods: {
    checkFile: function checkFile(e) {
      var _this = this;

      var file = e.target.files[0];
      if (file) {
        var ext = file.name.split(".").pop();
        if (["jpg", "jpeg", "png"].indexOf(ext) === -1) {
          return this.$swal("Error", "Only jpg, jpeg, png images are allowed", "error");
        }
      }
      this.iconFile = file;
      var reader = new FileReader();
      reader.onload = function (e) {
        return _this.previewUrl = e.target.result;
      };
      reader.readAsDataURL(file);
    },
    triggerFileClick: function triggerFileClick() {
      this.$refs.icon_file.click();
    },

    onSubmit: function onSubmit() {
      var _this2 = this;

      if (this.formstate.$invalid) {
        return;
      } else {
        var p = void 0;
        var requestData = void 0;
        if (this.iconFile) {
          requestData = new FormData();
          requestData.append("name", this.model.name);
          requestData.append("status", this.model.status);
          requestData.append("icon", this.iconFile);
          if (this.model.id) {
            requestData.append("_method", "PUT");
          }
        } else {
          requestData = _extends({}, this.model);
          if (this.model.id) {
            requestData._method = "PUT";
          }
        }
        if (this.model.id) {
          p = axios.post("/api/device/" + this.model.id, requestData);
        } else {
          p = axios.post("/api/device", requestData);
        }
        p.then(function (response) {
          _this2.$router.push({ name: "admin.device" });
          _this2.$store.commit("set_devices", []); // reset device collection
        }).catch(function (error) {
          return _this2.errors = error.response.data.message;
        });
      }
    }
  },
  mounted: function mounted() {
    var _this3 = this;

    if (this.$route.params.id) {
      axios.get("/api/device/" + this.$route.params.id).then(function (_ref) {
        var data = _ref.data.data;

        _this3.model = data;
        _this3.previewUrl = data.icon;
        _this3.originalDevice = data;
      }).catch(function () {
        return _this3.$router.push({ name: "admin.device" });
      });
    }
  },
  destroyed: function destroyed() {}
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-bd6aa6f6\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/device/add.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.dropzone_wrapper[data-v-bd6aa6f6] {\r\n  width: 100%;\r\n  height: 300px;\n}\n.red-asterisk[data-v-bd6aa6f6] {\r\n  color: red;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-bd6aa6f6\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/device/add.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "row" }, [
    _c(
      "div",
      { staticClass: "col-lg-12" },
      [
        _c(
          "b-card",
          {
            staticClass: "bg-success-card",
            attrs: { header: _vm.header, "header-tag": "h4" }
          },
          [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-lg-12" }, [
                _vm.errors
                  ? _c("div", { staticStyle: { color: "red" } }, [
                      _c(
                        "ul",
                        { attrs: { id: "example-1" } },
                        _vm._l(_vm.errors, function(error) {
                          return _c("li", [
                            _vm._v(
                              "\n                                " +
                                _vm._s(error) +
                                "\n                            "
                            )
                          ])
                        })
                      )
                    ])
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "col-lg-7 col-12 mb-3" },
                [
                  _c(
                    "vue-form",
                    {
                      staticClass: "form-horizontal form-validation",
                      attrs: { state: _vm.formstate },
                      on: {
                        submit: function($event) {
                          $event.preventDefault()
                          _vm.onSubmit($event)
                        }
                      }
                    },
                    [
                      _c("div", { staticClass: "col-lg-12" }, [
                        _c(
                          "div",
                          { staticClass: "form-group" },
                          [
                            _c(
                              "validate",
                              { attrs: { tag: "div" } },
                              [
                                _c("label", { attrs: { for: "name" } }, [
                                  _vm._v(" Device Name")
                                ]),
                                _vm._v(" "),
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.model.name,
                                      expression: "model.name"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    name: "name",
                                    type: "text",
                                    required: "",
                                    autofocus: "",
                                    placeholder: "Device Name"
                                  },
                                  domProps: { value: _vm.model.name },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.model,
                                        "name",
                                        $event.target.value
                                      )
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _c(
                                  "field-messages",
                                  {
                                    staticClass: "text-danger",
                                    attrs: {
                                      name: "name",
                                      show: "$invalid && $submitted"
                                    }
                                  },
                                  [
                                    _c(
                                      "div",
                                      {
                                        attrs: { slot: "required" },
                                        slot: "required"
                                      },
                                      [_vm._v("Please enter device name")]
                                    )
                                  ]
                                )
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "col-lg-12",
                          staticStyle: { "margin-bottom": "15px" }
                        },
                        [
                          _c("label", { attrs: { for: "icon" } }, [
                            _vm._v("Icon")
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "clearfix" }),
                          _vm._v(" "),
                          _c("img", {
                            attrs: {
                              width: "50",
                              height: "50",
                              src: _vm.previewUrl
                                ? _vm.previewUrl
                                : _vm.model.icon
                            }
                          }),
                          _vm._v(" "),
                          _c("div", { staticClass: "clearfix" }),
                          _vm._v(" "),
                          _c("input", {
                            ref: "icon_file",
                            staticStyle: { display: "none" },
                            attrs: { type: "file", name: "icon" },
                            on: { change: _vm.checkFile }
                          }),
                          _vm._v(" "),
                          _c(
                            "a",
                            {
                              attrs: { href: "javascript:;" },
                              on: { click: _vm.triggerFileClick }
                            },
                            [_vm._v("Edit")]
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-12" }, [
                        _c(
                          "div",
                          { staticClass: "form-group" },
                          [
                            _c("validate", { attrs: { tag: "div" } }, [
                              _c("label", { attrs: { for: "status" } }, [
                                _vm._v("Status")
                              ]),
                              _vm._v(" "),
                              _c("br"),
                              _vm._v(" "),
                              _c("label", { staticClass: "radio-inline" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.model.status,
                                      expression: "model.status"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "radio",
                                    name: "status",
                                    value: "Active"
                                  },
                                  domProps: {
                                    checked: _vm._q(_vm.model.status, "Active")
                                  },
                                  on: {
                                    change: function($event) {
                                      _vm.$set(_vm.model, "status", "Active")
                                    }
                                  }
                                }),
                                _vm._v(
                                  "Active\n                                    "
                                )
                              ]),
                              _vm._v(" "),
                              _c("label", { staticClass: "radio-inline" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.model.status,
                                      expression: "model.status"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "radio",
                                    name: "status",
                                    value: "Deactive"
                                  },
                                  domProps: {
                                    checked: _vm._q(
                                      _vm.model.status,
                                      "Deactive"
                                    )
                                  },
                                  on: {
                                    change: function($event) {
                                      _vm.$set(_vm.model, "status", "Deactive")
                                    }
                                  }
                                }),
                                _vm._v(
                                  "Deactive\n                                    "
                                )
                              ])
                            ])
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-md-offset-4 col-md-8 m-t-25" },
                        [
                          _c(
                            "button",
                            {
                              staticClass: "btn btn-primary",
                              attrs: { type: "submit" }
                            },
                            [_vm._v("Submit\n                            ")]
                          ),
                          _vm._v(" "),
                          _c(
                            "router-link",
                            {
                              staticClass: "btn btn-primary",
                              attrs: { to: { name: "admin.device" } }
                            },
                            [_vm._v("Cancel")]
                          )
                        ],
                        1
                      )
                    ]
                  )
                ],
                1
              )
            ])
          ]
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-bd6aa6f6", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-bd6aa6f6\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/device/add.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-bd6aa6f6\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/device/add.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("074d756e", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-bd6aa6f6\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./add.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-bd6aa6f6\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./add.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/components/pages/device/add.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-bd6aa6f6\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/device/add.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/device/add.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-bd6aa6f6\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/device/add.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-bd6aa6f6"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\device\\add.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-bd6aa6f6", Component.options)
  } else {
    hotAPI.reload("data-v-bd6aa6f6", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});