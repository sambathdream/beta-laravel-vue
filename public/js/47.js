webpackJsonp([47],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/tester/projects.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__("./node_modules/vue/dist/vue.common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "projects",
  components: {}
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-20589ffd\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/projects.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.text-green[data-v-20589ffd] {\n  color: #2cac3d;\n}\n.text-red[data-v-20589ffd] {\n  color: #e63423;\n}\n.text-bold[data-v-20589ffd] {\n  font-family: \"BrandonTextBold\" !important;\n}\n.text-medium[data-v-20589ffd] {\n  font-family: \"BrandonTextMedium\";\n}\n.projects-page[data-v-20589ffd] {\n  width: 100%;\n}\n.custom-container[data-v-20589ffd] {\n  max-width: 100%;\n}\n.page-title[data-v-20589ffd] {\n  font-family: \"UniNeueBold\";\n  font-size: 30px;\n  width: 100%;\n  text-align: center;\n  color: #363e48;\n  margin: 5px 0 25px;\n}\n.custom-pagination[data-v-20589ffd] {\n  max-width: 300px;\n  margin: 0 auto;\n}\n.custom-pagination li[data-v-20589ffd] {\n    margin-right: 3px;\n}\n.custom-pagination li .page-link[data-v-20589ffd] {\n      padding: 10px 15px;\n      font-size: 16px;\n      color: #606368;\n      font-family: \"BrandonTextBold\" !important;\n}\n.custom-pagination li:first-child .page-link[data-v-20589ffd] {\n      border-top-left-radius: 30px;\n      border-bottom-left-radius: 30px;\n      line-height: 20px;\n}\n.custom-pagination li:last-child .page-link[data-v-20589ffd] {\n      border-top-right-radius: 30px;\n      border-bottom-right-radius: 30px;\n      line-height: 20px;\n}\n.white-box[data-v-20589ffd] {\n  background-color: #fff;\n  -webkit-box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n          box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n  border-radius: 4px;\n  padding: 20px;\n  margin-bottom: 20px;\n  font-size: 14px;\n  line-height: 22px;\n  color: #606368;\n}\n.green-step-btn[data-v-20589ffd] {\n  width: 100%;\n  max-width: 180px;\n  padding: 7px 10px;\n  border: 2px solid #118921;\n  background-color: #2cac3d;\n  font-size: 14px;\n  font-family: \"BrandonTextMedium\";\n  letter-spacing: 0.05rem;\n  color: #fff;\n  border-radius: 20px;\n  display: inline-block;\n  text-align: center;\n  margin: 10px 15px 30px 0px;\n}\n.green-step-btn[data-v-20589ffd]:hover {\n    background-color: #158f25;\n    color: #fff;\n    outline: none !important;\n}\n@media screen and (max-width: 1281px) {\n.custom-container .projects-page .white-box[data-v-20589ffd] {\n    font-size: 14px;\n}\n.custom-container .projects-page .green-step-btn[data-v-20589ffd] {\n    font-size: 13px;\n}\n}\n@media screen and (max-width: 1200px) {\n.custom-container .page-title[data-v-20589ffd] {\n    font-size: 28px;\n    margin: 0 0 20px;\n}\n.custom-container .projects-page .green-step-btn[data-v-20589ffd] {\n    font-size: 12px;\n    letter-spacing: 0;\n}\n.custom-container .projects-page .white-box[data-v-20589ffd] {\n    font-size: 14px;\n}\n}\n@media screen and (max-width: 991px) {\n.custom-container .projects-page .white-box[data-v-20589ffd] {\n    padding: 15px;\n}\n}\n@media screen and (max-width: 767px) {\n.custom-container .projects-page .green-step-btn[data-v-20589ffd] {\n    margin: 10px 15px 10px 0;\n    max-width: 175px;\n}\n}\n@media screen and (max-width: 575px) {\n.custom-container .page-title[data-v-20589ffd] {\n    font-size: 26px;\n}\n.custom-container .projects-page .white-box[data-v-20589ffd] {\n    font-size: 14px;\n    line-height: 18px;\n}\n.custom-container .green-step-btn[data-v-20589ffd] {\n    margin: 10px 15px 10px 0;\n}\n.custom-container .custom-pagination[data-v-20589ffd] {\n    max-width: 235px;\n}\n.custom-container .custom-pagination li .page-link[data-v-20589ffd] {\n      padding: 5px 10px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-20589ffd\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/tester/projects.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "bg-color" }, [
    _c("div", { staticClass: "container custom-container" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "projects-page" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "white-box" }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-12" }, [
                _c(
                  "div",
                  { staticClass: "tester-projects-tabwrap" },
                  [
                    _c(
                      "b-tabs",
                      [
                        _c("b-tab", {
                          staticClass: "active-tab",
                          attrs: { title: "Active (1)", active: "" }
                        }),
                        _vm._v(" "),
                        _c("b-tab", { attrs: { title: "Open (26)" } }),
                        _vm._v(" "),
                        _c("b-tab", { attrs: { title: "Completed (3)" } }),
                        _vm._v(" "),
                        _c("b-tab", { attrs: { title: "Under Review (2)" } })
                      ],
                      1
                    )
                  ],
                  1
                )
              ])
            ]),
            _vm._v(" "),
            _vm._m(1)
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12" }, [
        _c("h2", { staticClass: "page-title" }, [_vm._v("Projects")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12 text-center" }, [
        _c("nav", { attrs: { "aria-label": "Page navigation example" } }, [
          _c("ul", { staticClass: "pagination custom-pagination" }, [
            _c("li", { staticClass: "page-item" }, [
              _c(
                "a",
                {
                  staticClass: "page-link",
                  attrs: { href: "#", "aria-label": "Previous" }
                },
                [
                  _c("span", { attrs: { "aria-hidden": "true" } }, [
                    _c("img", {
                      staticClass: "ing-fluid",
                      attrs: { src: __webpack_require__("./resources/assets/assets/img/pagination-prev.png") }
                    })
                  ]),
                  _vm._v(" "),
                  _c("span", { staticClass: "sr-only" }, [_vm._v("Previous")])
                ]
              )
            ]),
            _vm._v(" "),
            _c("li", { staticClass: "page-item" }, [
              _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                _vm._v("1")
              ])
            ]),
            _vm._v(" "),
            _c("li", { staticClass: "page-item" }, [
              _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                _vm._v("2")
              ])
            ]),
            _vm._v(" "),
            _c("li", { staticClass: "page-item" }, [
              _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                _vm._v("3")
              ])
            ]),
            _vm._v(" "),
            _c("li", { staticClass: "page-item" }, [
              _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                _vm._v("4")
              ])
            ]),
            _vm._v(" "),
            _c("li", { staticClass: "page-item" }, [
              _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                _vm._v("5")
              ])
            ]),
            _vm._v(" "),
            _c("li", { staticClass: "page-item" }, [
              _c(
                "a",
                {
                  staticClass: "page-link",
                  attrs: { href: "#", "aria-label": "Next" }
                },
                [
                  _c("span", { attrs: { "aria-hidden": "true" } }, [
                    _c("img", {
                      staticClass: "ing-fluid",
                      attrs: { src: __webpack_require__("./resources/assets/assets/img/pagination-next.png") }
                    })
                  ]),
                  _vm._v(" "),
                  _c("span", { staticClass: "sr-only" }, [_vm._v("Next")])
                ]
              )
            ])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-20589ffd", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-20589ffd\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/projects.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-20589ffd\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/projects.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("213be2ce", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-20589ffd\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./projects.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-20589ffd\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./projects.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/assets/img/pagination-next.png":
/***/ (function(module, exports) {

module.exports = "/images/pagination-next.png?bc73e7a892dc37aad89c8745d53e4f4d";

/***/ }),

/***/ "./resources/assets/assets/img/pagination-prev.png":
/***/ (function(module, exports) {

module.exports = "/images/pagination-prev.png?1f39ba43ee6780c5b30c5c8cd34397f2";

/***/ }),

/***/ "./resources/assets/components/pages/tester/projects.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-20589ffd\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/projects.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/tester/projects.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-20589ffd\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/tester/projects.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-20589ffd"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\tester\\projects.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-20589ffd", Component.options)
  } else {
    hotAPI.reload("data-v-20589ffd", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});