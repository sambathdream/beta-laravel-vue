webpackJsonp([82],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/device-selector.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "DeviceSelector",
  props: {
    disabled: {
      type: Boolean,
      defualt: function defualt() {
        return false;
      }
    },
    value: {
      type: Array,
      required: true,
      default: function _default() {
        return [];
      }
    }
  },
  data: function data() {
    return {
      devices: []
    };
  },

  methods: {
    toggleItem: function toggleItem(device) {
      if (this.disabled) {
        return true;
      }
      var index = this.value.indexOf(device.id);
      var newDevices = [];
      if (index >= 0) {
        newDevices = this.value.filter(function (i) {
          return i !== device.id;
        });
      } else {
        newDevices = [].concat(_toConsumableArray(this.value), [device.id]);
      }
      this.$emit("input", newDevices);
      this.$emit("change", newDevices);
    }
  },
  mounted: function mounted() {
    var _this = this;

    if (!this.$store.state.devices.length) {
      axios.get("/api/device").then(function (_ref) {
        var data = _ref.data;

        _this.$store.commit("set_devices", data);
        _this.devices = data;
      });
    } else {
      this.devices = this.$store.state.devices;
    }
  },
  computed: {
    availableDevices: function availableDevices() {
      var _this2 = this;

      if (this.disabled) {
        return this.devices.filter(function (_ref2) {
          var id = _ref2.id;
          return _this2.value.indexOf(id) >= 0;
        });
      }
      return this.devices;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/project-statistics.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "ProjectStatistics",
  props: {
    value: {
      type: Object,
      required: true,
      default: function _default() {
        return {};
      }
    },
    disabled: {
      type: Boolean,
      default: function _default() {
        return false;
      }
    },
    errors: {
      type: Array,
      default: function _default() {
        return null;
      }
    }
  },
  methods: {
    markAsCompleted: function markAsCompleted() {
      var _this = this;

      var requestData = _.pick(this.value, ["cost_per_tester", "allocated_tester", "total_cost", "name"]);
      requestData.status = "COMPLETED";
      axios.put("/api/projects/" + this.value.id, requestData).then(function (_ref) {
        var data = _ref.data.data;

        _this.$emit("onCompleted");
      }).catch(function (error) {
        _this.errors = error.response.data.message;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/publisher/file-viewer.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "FileViewer",
  props: {
    files: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    colorTheme: {
      type: String,
      default: function _default() {
        return 'publisher';
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/publisher/project_view.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_components_components_device_selector__ = __webpack_require__("./resources/assets/components/components/device-selector.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_components_components_device_selector___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_components_components_device_selector__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_components_components_project_statistics__ = __webpack_require__("./resources/assets/components/components/project-statistics.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_components_components_project_statistics___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_components_components_project_statistics__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_components_components_publisher_file_viewer__ = __webpack_require__("./resources/assets/components/components/publisher/file-viewer.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_components_components_publisher_file_viewer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_components_components_publisher_file_viewer__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  name: "view_project",
  components: {
    DeviceSelector: __WEBPACK_IMPORTED_MODULE_0_components_components_device_selector___default.a,
    ProjectStatistics: __WEBPACK_IMPORTED_MODULE_1_components_components_project_statistics___default.a,
    FileViewer: __WEBPACK_IMPORTED_MODULE_2_components_components_publisher_file_viewer___default.a
  },
  data: function data() {
    return {
      testproject: {},
      formstate: {},
      model: {},
      originalUser: {},
      type_name: "",
      errors: null
    };
  },

  mounted: function mounted() {
    var _this = this;

    axios.get("/api/projects/" + this.$route.params.id).then(function (_ref) {
      var data = _ref.data.data;

      _this.testproject = data;
      _this.type_name = _this.testproject.project_type.name;
      _this.testproject.selected_devices = data.devices.map(function (_ref2) {
        var id = _ref2.id;
        return id;
      });
    }).catch(function (error) {});
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/device-selector.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\nimg[data-v-0d653902] {\n  height: 50px;\n  width: 50px;\n  display: inline-block;\n  margin-right: 10px;\n  float: left;\n  opacity: 0.5;\n  cursor: pointer;\n}\nimg.disabled[data-v-0d653902] {\n  cursor: auto;\n}\nimg.selected[data-v-0d653902] {\n  opacity: 1;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-59c40955\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/publisher/project_view.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.text-bold[data-v-59c40955] {\n  font-family: \"BrandonTextBold\" !important;\n}\n.text-medium[data-v-59c40955] {\n  font-family: \"BrandonTextMedium\";\n}\n.text-blue[data-v-59c40955] {\n  color: #0082cc;\n}\n.white-box[data-v-59c40955] {\n  background-color: #fff;\n  -webkit-box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n          box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n  border-radius: 4px;\n  padding: 20px;\n  margin-bottom: 20px;\n  font-size: 14px;\n  line-height: 22px;\n  color: #606368;\n}\n.white-box .block-title[data-v-59c40955] {\n    font-size: 18px;\n    font-family: \"UniNeueBold\";\n    border-bottom: 1px solid #dadada;\n    color: #606368;\n    padding-bottom: 10px;\n    margin-bottom: 10px;\n    width: auto;\n}\n.white-box .block-subtitle[data-v-59c40955] {\n    font-size: 18px;\n    font-family: \"BrandonTextBold\" !important;\n    border-bottom: 1px solid #dadada;\n    color: #606368;\n    padding-bottom: 10px;\n    margin-bottom: 10px;\n    width: auto;\n}\n.white-box .proj-name-wrap .proj-name[data-v-59c40955] {\n    font-family: \"UniNeueBold\";\n    font-size: 24px;\n    line-height: 18px;\n    color: #0082cc;\n}\n.white-box .project-link[data-v-59c40955] {\n    color: #0082cc;\n    text-decoration: underline !important;\n}\n.white-box .proj-title-name[data-v-59c40955] {\n    font-size: 40px;\n    font-family: \"BrandonTextBold\" !important;\n    text-decoration: underline;\n}\n.white-box .proj-name-status[data-v-59c40955] {\n    font-family: \"BrandonTextBold\" !important;\n    font-size: 20px;\n    color: #00aff5;\n    text-decoration: none !important;\n}\n.white-box .proj-detail[data-v-59c40955] {\n    max-width: 45%;\n    line-height: 26px;\n    margin: 20px auto;\n}\n.white-box .proj-status[data-v-59c40955] {\n    padding: 0;\n    padding-top: 8px;\n    overflow: hidden;\n}\n.white-box .proj-status br[data-v-59c40955] {\n      display: none;\n}\n.white-box .proj-status:first-child .proj-progress[data-v-59c40955]::before {\n      width: calc(50% - 30px);\n      left: 15px;\n}\n.white-box .proj-status:last-child .proj-progress[data-v-59c40955]::after {\n      width: calc(50% - 30px);\n      right: 15px;\n}\n.white-box .proj-status .proj-progress[data-v-59c40955] {\n      position: relative;\n      margin-bottom: 5px;\n}\n.white-box .proj-status .proj-progress i[data-v-59c40955] {\n        color: rgba(175, 177, 179, 0.5);\n        font-size: 22px;\n        line-height: 24px;\n}\n.white-box .proj-status .proj-progress[data-v-59c40955]::before {\n        content: \"\";\n        position: absolute;\n        height: 3px;\n        width: calc(50% - 5px);\n        background-color: #ececec;\n        left: -10px;\n        top: 11px;\n}\n.white-box .proj-status .proj-progress[data-v-59c40955]::after {\n        content: \"\";\n        position: absolute;\n        height: 3px;\n        width: calc(50% - 5px);\n        background-color: #ececec;\n        right: -10px;\n        top: 11px;\n}\n.white-box .proj-status.active[data-v-59c40955] {\n    color: #0082cc;\n}\n.white-box .proj-status.active .proj-progress i[data-v-59c40955] {\n      color: #0082cc;\n      font-size: 34px;\n      line-height: 24px;\n      position: relative;\n}\n.white-box .proj-status.active .proj-progress i[data-v-59c40955]::after {\n        height: 15px;\n        width: 15px;\n        background-color: #0082cc;\n        content: \"\";\n        border-radius: 50%;\n        position: absolute;\n        left: 7px;\n        top: 4px;\n}\n.white-box .proj-status.active .proj-progress[data-v-59c40955]::before {\n      background-color: #0082cc;\n}\n.white-box .proj-status.done-step[data-v-59c40955] {\n    color: #0082cc;\n}\n.white-box .proj-status.done-step .proj-progress i[data-v-59c40955] {\n      color: #0082cc;\n      font-size: 34px;\n      line-height: 24px;\n      position: relative;\n}\n.white-box .proj-status.done-step .proj-progress i[data-v-59c40955]::after {\n        height: 15px;\n        width: 15px;\n        background-color: #0082cc;\n        content: \"\";\n        border-radius: 50%;\n        position: absolute;\n        left: 7px;\n        top: 4px;\n}\n.white-box .proj-status.done-step .proj-progress[data-v-59c40955]::before {\n      background-color: #0082cc;\n}\n.white-box .proj-status.done-step .proj-progress[data-v-59c40955]::after {\n      background-color: #0082cc;\n}\n.back-proj-btn[data-v-59c40955] {\n  max-width: 180px;\n}\n.cancel-btn[data-v-59c40955] {\n  max-width: 180px;\n}\n.blue-step-btn[data-v-59c40955] {\n  width: auto;\n  padding: 8px 15px;\n  border: 2px solid #0082cc;\n  background-color: #00aff5;\n  font-size: 14px;\n  font-family: \"BrandonTextMedium\";\n  letter-spacing: 0.05rem;\n  color: #fff;\n  border-radius: 20px;\n  display: block;\n  text-align: center;\n}\n.blue-step-btn[data-v-59c40955]:hover {\n    background-color: #13b9fb;\n}\n.grey-btn[data-v-59c40955] {\n  width: auto;\n  padding: 8px 15px;\n  border: 2px solid #606368;\n  background-color: #898a8c;\n  font-size: 14px;\n  font-family: \"BrandonTextMedium\";\n  letter-spacing: 0.05rem;\n  color: #fff;\n  border-radius: 20px;\n  display: block;\n  text-align: center;\n}\n.grey-btn[data-v-59c40955]:hover {\n    background-color: #95979a;\n}\n.red-btn[data-v-59c40955] {\n  width: auto;\n  padding: 8px 15px;\n  border: 2px solid #940000;\n  background-color: #e80000;\n  font-size: 14px;\n  font-family: \"BrandonTextMedium\";\n  letter-spacing: 0.05rem;\n  color: #fff;\n  border-radius: 20px;\n  display: block;\n  text-align: center;\n}\n@media screen and (max-width: 1281px) {\n.white-box[data-v-59c40955] {\n    font-size: 16px;\n}\n.white-box .proj-title-name[data-v-59c40955] {\n      font-size: 34px;\n}\n.white-box .proj-detail[data-v-59c40955] {\n      max-width: 65%;\n      margin: 20px auto 0;\n}\n}\n@media screen and (max-width: 1200px) {\n.white-box[data-v-59c40955] {\n    font-size: 16px;\n}\n}\n@media screen and (max-width: 991px) {\n.white-box[data-v-59c40955] {\n    padding: 15px;\n    font-size: 14px;\n}\n.white-box .proj-title-name[data-v-59c40955] {\n      font-size: 32px;\n}\n.white-box .proj-name-status[data-v-59c40955] {\n      font-size: 18px;\n}\n}\n@media screen and (max-width: 767px) {\n.white-box .proj-title-name[data-v-59c40955] {\n    font-size: 30px;\n}\n.white-box .proj-name-status[data-v-59c40955] {\n    font-size: 16px;\n}\n.white-box .proj-detail[data-v-59c40955] {\n    line-height: 22px;\n}\n.white-box .proj-status.active span[data-v-59c40955] {\n    display: block !important;\n}\n}\n@media screen and (max-width: 575px) {\n.cancel-btn[data-v-59c40955],\n  .back-proj-btn[data-v-59c40955] {\n    font-size: 12px;\n    padding: 6px;\n}\n.white-box .block-title[data-v-59c40955] {\n    font-size: 18px;\n}\n.white-box .proj-title-name[data-v-59c40955] {\n    font-size: 22px;\n}\n.white-box .proj-name-status[data-v-59c40955] {\n    font-size: 14px;\n}\n.white-box .proj-detail[data-v-59c40955] {\n    max-width: 100%;\n    line-height: 22px;\n    margin: 10px auto 0;\n}\n.white-box .proj-status:first-child .proj-progress[data-v-59c40955]::before {\n    width: calc(50% - 25px);\n}\n.white-box .proj-status:last-child .proj-progress[data-v-59c40955]::after {\n    width: calc(50% - 25px);\n}\n.white-box .proj-status.active .proj-progress i[data-v-59c40955],\n  .white-box .proj-status.done-step .proj-progress i[data-v-59c40955] {\n    font-size: 28px;\n}\n.white-box .proj-status.active .proj-progress i[data-v-59c40955]::after,\n    .white-box .proj-status.done-step .proj-progress i[data-v-59c40955]::after {\n      height: 13px;\n      width: 13px;\n      left: 6px;\n      top: 5px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6ab03afb\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project-statistics.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.text-bold[data-v-6ab03afb] {\n  font-family: \"BrandonTextBold\" !important;\n}\n.text-medium[data-v-6ab03afb] {\n  font-family: \"BrandonTextMedium\";\n}\n.text-blue[data-v-6ab03afb] {\n  color: #0082cc;\n}\n.text-purple[data-v-6ab03afb] {\n  color: #363e48;\n}\n.white-box[data-v-6ab03afb] {\n  background-color: #fff;\n  -webkit-box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n          box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n  border-radius: 4px;\n  padding: 15px;\n  margin-bottom: 20px;\n  font-size: 14px;\n  line-height: 22px;\n  color: #606368;\n}\n.white-box .block-title[data-v-6ab03afb] {\n    font-size: 18px;\n    font-family: \"UniNeueBold\";\n    border-bottom: 1px solid #dadada;\n    color: #0082cc;\n    padding-bottom: 10px;\n    margin-bottom: 10px;\n    width: auto;\n}\n.white-box .block-subtitle[data-v-6ab03afb] {\n    font-size: 20px;\n    font-family: \"BrandonTextBold\" !important;\n    border-bottom: 1px solid #dadada;\n    color: #0082cc;\n    padding-bottom: 10px;\n    margin-bottom: 10px;\n    width: auto;\n}\n.white-box .mark-comple-btn[data-v-6ab03afb] {\n    float: right;\n    margin-top: 15px;\n}\n.blue-btn[data-v-6ab03afb] {\n  width: auto;\n  padding: 8px 15px;\n  border: 2px solid #0082cc;\n  background-color: #00aff5;\n  font-size: 14px;\n  font-family: \"BrandonTextMedium\";\n  letter-spacing: 0.05rem;\n  color: #fff;\n  border-radius: 20px;\n  display: block;\n  text-align: center;\n  color: #fff;\n}\n.blue-btn[data-v-6ab03afb]:hover {\n    background-color: #13b9fb;\n}\n.purple-btn[data-v-6ab03afb] {\n  background-color: #5651b9;\n  border: 2px solid #3e3a94;\n  color: #fff !important;\n  font-size: 15px;\n  line-height: 16px;\n  font-family: \"BrandonTextMedium\";\n  border-radius: 6px;\n  padding: 3px 6px;\n  -webkit-transition: all 0.4s ease;\n  transition: all 0.4s ease;\n  cursor: pointer;\n}\n.purple-btn[data-v-6ab03afb]:hover {\n    outline: none !important;\n    background-color: #6b65e0;\n}\n.purple-btn[data-v-6ab03afb]:focus {\n    outline: none !important;\n}\n@media screen and (max-width: 991px) {\n.white-box[data-v-6ab03afb] {\n    padding: 15px;\n}\n.purple-btn[data-v-6ab03afb] {\n    padding: 3px 5px;\n    font-size: 14px;\n}\n}\n@media screen and (max-width: 991px) {\n.purple-btn[data-v-6ab03afb] {\n    padding: 3px 10px;\n}\n}\n@media screen and (max-width: 575px) {\n.white-box .block-title[data-v-6ab03afb] {\n    font-size: 16px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e55f98a2\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/file-viewer.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.publisher[data-v-e55f98a2] {\n  background: #fff;\n}\n.file[data-v-e55f98a2] {\n  padding: 0px;\n  color: #606368;\n  text-decoration: none;\n  margin-top: 3px;\n  border-radius: 4px;\n  margin-left: 0px;\n}\n.file a[data-v-e55f98a2] {\n    color: #606368;\n    padding: 0;\n}\n.file a[data-v-e55f98a2]:hover {\n      text-decoration: underline !important;\n}\n.tester[data-v-e55f98a2] {\n  background: #fff;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-0d653902\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/device-selector.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    _vm._l(_vm.availableDevices, function(device) {
      return _c("span", { staticClass: "d-inline-block pr-3" }, [
        _c("img", {
          staticClass: "img-fluid",
          class: {
            selected: _vm.value.indexOf(device.id) !== -1,
            disabled: _vm.disabled
          },
          attrs: { title: device.name, src: device.icon },
          on: {
            click: function($event) {
              _vm.toggleItem(device)
            }
          }
        })
      ])
    })
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0d653902", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-59c40955\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/publisher/project_view.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "white-box" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-12 text-center" }, [
          _c("h2", { staticClass: "text-blue text-bold proj-title-name" }, [
            _vm._v(
              "\n           " + _vm._s(_vm.testproject.name) + "\n        "
            )
          ]),
          _vm._v(" "),
          _c("p", { staticClass: "proj-name-status text-bold" }, [
            _vm._v("\n            Waiting for approval\n        ")
          ]),
          _vm._v(" "),
          _c("p", { staticClass: "proj-detail" }, [
            _vm._v(
              "You will be notified when your project is approved for testing. \n        "
            )
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        { staticClass: "col-12 text-right mb-4" },
        [
          _c(
            "router-link",
            {
              staticClass:
                "text-uppercase grey-btn mr-1 mr-sm-3 back-proj-btn d-inline-block",
              attrs: { to: { name: "publisher.projects" } }
            },
            [_vm._v("back to projects")]
          ),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass: "text-uppercase red-btn cancel-btn d-inline-block",
              attrs: { href: "#" }
            },
            [_vm._v("cancel projects")]
          )
        ],
        1
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        { staticClass: "col-md-12" },
        [
          _c("div", { staticClass: "white-box" }, [
            _c("h4", { staticClass: "block-subtitle" }, [
              _vm._v("\n              Basic Information\n        ")
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row pb-3 pb-md-4" }, [
              _c("div", { staticClass: "col-md-5" }, [
                _c("div", { staticClass: "mb-1" }, [
                  _c("span", { staticClass: "text-bold" }, [
                    _vm._v("\n                Project Title :\n              ")
                  ]),
                  _vm._v(" "),
                  _c("span", {}, [
                    _vm._v(
                      "\n                " +
                        _vm._s(_vm.testproject.name) +
                        "\n              "
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "mb-1" }, [
                  _c("span", { staticClass: "text-bold" }, [
                    _vm._v("\n                Project Type :\n              ")
                  ]),
                  _vm._v(" "),
                  _c("span", {}, [
                    _vm._v(
                      "\n                " +
                        _vm._s(_vm.type_name) +
                        "\n              "
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "mb-1" }, [
                  _c("span", { staticClass: "text-bold" }, [
                    _vm._v(
                      "\n                Project Total Amount :\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("span", {}, [
                    _vm._v(
                      "\n                " +
                        _vm._s(_vm.testproject.amount) +
                        "\n              "
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-4" }, [
                _c("div", { staticClass: "mb-1" }, [
                  _c("span", { staticClass: "text-bold" }, [
                    _vm._v("\n                Start Date :\n              ")
                  ]),
                  _vm._v(" "),
                  _c("span", {}, [
                    _vm._v(
                      "\n                " +
                        _vm._s(_vm._f("date")(_vm.testproject.start_date)) +
                        "\n              "
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "mb-1" }, [
                  _c("span", { staticClass: "text-bold" }, [
                    _vm._v("\n                End Date :\n              ")
                  ]),
                  _vm._v(" "),
                  _c("span", {}, [
                    _vm._v(
                      "\n                " +
                        _vm._s(_vm._f("date")(_vm.testproject.end_date)) +
                        "\n              "
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-3" }, [
                _c("div", { staticClass: "mb-1" }, [
                  _c("span", { staticClass: "text-bold" }, [
                    _vm._v(
                      "\n                Time to Test (Approx) :\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("span", {}, [
                    _vm._v(
                      "\n                " +
                        _vm._s(_vm.testproject.estimate_tester_time) +
                        " hrs\n              "
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "mb-1" }, [
                  _c("span", { staticClass: "text-bold" }, [
                    _vm._v("\n                Tester Needed :\n              ")
                  ]),
                  _vm._v(" "),
                  _c("span", [
                    _vm._v(
                      "\n                " +
                        _vm._s(_vm.testproject.tester_needed) +
                        "\n              "
                    )
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("h4", { staticClass: "block-subtitle" }, [
              _vm._v("\n            Project Description\n        ")
            ]),
            _vm._v(" "),
            _c("div", {}, [
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-md-12 pb-3" }, [
                  _c("p", [
                    _vm._v(
                      " " + _vm._s(this.testproject.project_description) + " "
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-md-6 pt-3" }, [
                  _c("div", { staticClass: "form-group" }, [
                    _c("h4", { staticClass: "block-subtitle" }, [
                      _vm._v(
                        "\n                        Links\n                    "
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-md-12 col-12" }, [
                        _c(
                          "ul",
                          { staticClass: "pl-0 mb-2" },
                          _vm._l(_vm.testproject.links, function(link) {
                            return _c("li", { key: link.id }, [
                              _vm._v(
                                "\n                                  " +
                                  _vm._s(link.title) +
                                  ":  "
                              ),
                              _c(
                                "a",
                                {
                                  attrs: { href: link.link, target: "_blank" }
                                },
                                [_vm._v(" " + _vm._s(link.link))]
                              )
                            ])
                          })
                        )
                      ])
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-md-6 pt-3" }, [
                  _c(
                    "div",
                    { staticClass: "form-group" },
                    [
                      _c("h4", { staticClass: "block-subtitle" }, [
                        _vm._v(
                          "\n                      Files\n                  "
                        )
                      ]),
                      _vm._v(" "),
                      _c("file-viewer", {
                        attrs: { files: _vm.testproject.media }
                      })
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-md-6 col-12 pt-3" }, [
                  _c("div", { staticClass: "form-group" }, [
                    _c("h4", { staticClass: "block-subtitle" }, [
                      _vm._v(
                        "\n                        Notes / Instruction\n                    "
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", {}, [
                      _vm._v(
                        "\n                        " +
                          _vm._s(this.testproject.notes_instruction) +
                          "\n                    "
                      )
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-md-6 col-12 pt-3" }, [
                  _c("h4", { staticClass: "block-subtitle" }, [
                    _vm._v(
                      "\n                  Required Devices\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    {},
                    [
                      _c("device-selector", {
                        attrs: { disabled: "" },
                        model: {
                          value: _vm.testproject.selected_devices,
                          callback: function($$v) {
                            _vm.$set(_vm.testproject, "selected_devices", $$v)
                          },
                          expression: "testproject.selected_devices"
                        }
                      })
                    ],
                    1
                  )
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "white-box" }, [
            _c("h4", { staticClass: "block-title" }, [
              _vm._v("\n              Questions\n          ")
            ]),
            _vm._v(" "),
            _c("div", {}, [
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _c("p", [
                    _vm._v(
                      "\n                      Your Questions to Testers which they have to fil up while submitting test Result\n                  "
                    )
                  ]),
                  _vm._v(" "),
                  _vm._l(this.testproject.questions, function(ques, index) {
                    return _c("ul", { staticClass: "pl-0 mb-3" }, [
                      _c("li", [
                        _c("span", { staticClass: "text-bold mr-2" }, [
                          _vm._v(
                            "\n                              Question " +
                              _vm._s(index + 1) +
                              " :\n                          "
                          )
                        ]),
                        _vm._v(
                          "\n                            " +
                            _vm._s(ques.question) +
                            "\n                      "
                        )
                      ])
                    ])
                  })
                ],
                2
              )
            ])
          ]),
          _vm._v(" "),
          _vm.testproject.status &&
          _vm.testproject.status.name != "Pending Approval"
            ? _c("project-statistics", {
                attrs: { errors: _vm.errors },
                model: {
                  value: _vm.testproject,
                  callback: function($$v) {
                    _vm.testproject = $$v
                  },
                  expression: "testproject"
                }
              })
            : _vm._e()
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "white-box" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-12" }, [
          _c("h4", { staticClass: "block-title border-bottom-0" }, [
            _vm._v("\n          Project Status\n        ")
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-12" }, [
          _c("div", { staticClass: "row" }, [
            _c(
              "div",
              { staticClass: "col-3 text-center proj-status done-step" },
              [
                _c("p", { staticClass: "proj-progress" }, [
                  _c("i", { staticClass: "fa fa-circle-thin" })
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "d-none d-md-block text-medium" }, [
                  _vm._v("Submitted")
                ])
              ]
            ),
            _vm._v(" "),
            _c("div", { staticClass: "col-3 text-center proj-status active" }, [
              _c("p", { staticClass: "proj-progress" }, [
                _c("i", { staticClass: "fa fa-circle-thin" })
              ]),
              _vm._v(" "),
              _c("span", { staticClass: "d-none d-md-block text-medium" }, [
                _vm._v("Waiting for Approval")
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-3 text-center proj-status" }, [
              _c("p", { staticClass: "proj-progress" }, [
                _c("i", { staticClass: "fa fa-circle-thin" })
              ]),
              _vm._v(" "),
              _c("span", { staticClass: "d-none d-md-block text-medium" }, [
                _vm._v("Pay")
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-3 text-center proj-status" }, [
              _c("p", { staticClass: "proj-progress" }, [
                _c("i", { staticClass: "fa fa-circle-thin" })
              ]),
              _vm._v(" "),
              _c("span", { staticClass: "d-none d-md-block text-medium" }, [
                _vm._v("Ready to Test")
              ])
            ])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-59c40955", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-6ab03afb\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/project-statistics.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "white-box" }, [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12 col-md-3 statistics-info-wrap" }, [
        _c("p", { staticClass: "mb-2 mb-sm-4" }, [
          _c("span", {}, [_vm._v("\n          Tester Needed :\n        ")]),
          _vm._v(" "),
          _c("span", { staticClass: "text-bold statistics-num" }, [
            _vm._v(
              "\n          " + _vm._s(_vm.value.tester_needed) + "\n        "
            )
          ])
        ]),
        _vm._v(" "),
        _c("p", { staticClass: "mb-2 mb-sm-4" }, [
          _c("span", {}, [_vm._v("\n          Tester Submitted :\n        ")]),
          _vm._v(" "),
          _c("span", { staticClass: "text-bold statistics-num" }, [
            _vm._v(
              "\n          " +
                _vm._s(_vm.value.testers ? _vm.value.testers.length : 0) +
                "\n        "
            )
          ])
        ]),
        _vm._v(" "),
        _vm._m(1),
        _vm._v(" "),
        _vm._m(2),
        _vm._v(" "),
        _vm._m(3)
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-12 col-md-6" }, [
        _c("div", { staticClass: "filter-tester-wrap" }, [
          _c("div", { staticClass: "form-group mb-4" }, [
            _c("label", [_vm._v("Filter by Testers:")]),
            _vm._v(" "),
            _c(
              "select",
              { staticClass: "form-control", attrs: { multiple: "" } },
              _vm._l(_vm.value.testers, function(tester) {
                return _c("option", [
                  _vm._v(
                    _vm._s(tester.first_name) + " " + _vm._s(tester.last_name)
                  )
                ])
              })
            )
          ]),
          _vm._v(" "),
          _c("a", { staticClass: "purple-btn", attrs: { href: "#" } }, [
            _vm._v("\n          Filter\n        ")
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-12 col-md-3" }, [
        _vm._m(4),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c(
            "div",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value:
                    _vm.value.status && _vm.value.status.name != "Completed",
                  expression:
                    "value.status && value.status.name != 'Completed' "
                }
              ],
              staticClass: "col-12"
            },
            [
              _c(
                "a",
                {
                  staticClass: "purple-btn mark-comple-btn",
                  on: { click: _vm.markAsCompleted }
                },
                [_vm._v("Mark as Completed")]
              )
            ]
          )
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12" }, [
        _c("h4", { staticClass: "block-title" }, [_vm._v("Statistics\n      ")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "mb-2 mb-sm-4" }, [
      _c("span", {}, [_vm._v("\n          Ratings/score :\n        ")]),
      _vm._v(" "),
      _c("span", { staticClass: "text-bold statistics-num" }, [
        _vm._v("\n          88\n        ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "mb-2 mb-sm-4" }, [
      _c("span", {}, [_vm._v("\n          Issues :\n        ")]),
      _vm._v(" "),
      _c("span", { staticClass: "text-bold statistics-num" }, [
        _vm._v("\n          11\n        ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "mb-0" }, [
      _c("span", {}, [_vm._v("\n          Attachments :\n        ")]),
      _vm._v(" "),
      _c("span", { staticClass: "text-bold statistics-num" }, [
        _vm._v("\n          10\n        ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12 text-center" }, [
        _c("span", { staticClass: "text-bold float-right" }, [
          _vm._v("\n            Progress : 60%\n          ")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "float-right w-100 mt-2" }, [
          _c("img", {
            staticClass: "img-fluid float-right",
            attrs: { src: __webpack_require__("./resources/assets/assets/img/progress-bar.png") }
          })
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-6ab03afb", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-e55f98a2\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/publisher/file-viewer.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "row" }, [
    _c(
      "div",
      { staticClass: "col-12" },
      _vm._l(_vm.files, function(file) {
        return _c("div", { key: file.id, class: ["file", _vm.colorTheme] }, [
          _c("a", { attrs: { href: file.url, target: "_blank" } }, [
            _vm._v(
              "\n     " +
                _vm._s(file.file_name) +
                " - " +
                _vm._s(_vm._f("fileSize")(file.size)) +
                "\n    "
            )
          ])
        ])
      })
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-e55f98a2", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/device-selector.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/device-selector.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("4b79eeec", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./device-selector.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./device-selector.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-59c40955\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/publisher/project_view.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-59c40955\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/publisher/project_view.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("535ab794", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-59c40955\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project_view.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-59c40955\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project_view.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6ab03afb\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project-statistics.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6ab03afb\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project-statistics.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("70471f82", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6ab03afb\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project-statistics.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6ab03afb\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project-statistics.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e55f98a2\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/file-viewer.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e55f98a2\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/file-viewer.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("a93db18a", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e55f98a2\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./file-viewer.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e55f98a2\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./file-viewer.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/assets/img/progress-bar.png":
/***/ (function(module, exports) {

module.exports = "/images/progress-bar.png?9c04687ac2c36d110989faf38f2c44b0";

/***/ }),

/***/ "./resources/assets/components/components/device-selector.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/device-selector.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/device-selector.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-0d653902\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/device-selector.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-0d653902"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\device-selector.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0d653902", Component.options)
  } else {
    hotAPI.reload("data-v-0d653902", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/components/project-statistics.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6ab03afb\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project-statistics.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/project-statistics.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-6ab03afb\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/project-statistics.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-6ab03afb"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\project-statistics.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6ab03afb", Component.options)
  } else {
    hotAPI.reload("data-v-6ab03afb", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/components/publisher/file-viewer.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e55f98a2\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/file-viewer.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/publisher/file-viewer.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-e55f98a2\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/publisher/file-viewer.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-e55f98a2"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\publisher\\file-viewer.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e55f98a2", Component.options)
  } else {
    hotAPI.reload("data-v-e55f98a2", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/pages/publisher/project_view.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-59c40955\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/publisher/project_view.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/publisher/project_view.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-59c40955\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/publisher/project_view.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-59c40955"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\publisher\\project_view.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-59c40955", Component.options)
  } else {
    hotAPI.reload("data-v-59c40955", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});