webpackJsonp([72],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/test-project/test_project_submission.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "project_view",
  data: function data() {
    return {
      testProject: {},
      originalUser: {}
    };
  },

  mounted: function mounted() {
    this.loadProject();
    this.originalUser = window.USER;
    if (this.originalUser.name == null) {
      this.originalUser.name = this.originalUser.full_name;
    }
  },
  methods: {
    loadProject: function loadProject() {
      var _this = this;

      axios.get("/api/projects/" + this.$route.params.id).then(function (_ref) {
        var data = _ref.data.data;

        _this.testProject = data;
      }).catch(function (error) {});
    },
    deleteTestProject: function deleteTestProject(id) {
      var _this2 = this;

      this.$swal({
        title: "Are you sure?",
        text: "You want to delete this project?",
        icon: "warning",
        buttons: true,
        dangerMode: true
      }).then(function (willDelete) {
        if (willDelete) {
          return axios.delete("/api/projects/" + id + "?test=1").then(function () {
            return _this2.loadProjects();
          }).catch(function (e) {
            return _this2.errors = e.response.message;
          });
        }
      });
    },
    changeStatus: function changeStatus(project, status) {
      var _this3 = this;

      return axios.post("/api/test/" + project.id + "/status", {
        status: status
      }).then(function () {
        return _this3.$router.push({ name: "admin.test_projects" });
      });
    },
    acceptProjectSubmission: function acceptProjectSubmission(project) {
      var _this4 = this;

      this.$swal({
        title: "Are you sure?",
        text: "Are you sure you want to Approve?",
        icon: "warning",
        buttons: true
      }).then(function (willDelete) {
        if (willDelete) {
          return _this4.changeStatus(project, "TEST_PASSED");
        }
      });
    },
    rejectProjectSubmission: function rejectProjectSubmission(project) {
      var _this5 = this;

      this.$swal({
        title: "Are you sure?",
        text: "Are you sure you want to Reject?",
        icon: "warning",
        buttons: true
      }).then(function (willDelete) {
        if (willDelete) {
          return _this5.changeStatus(project, "TEST_FAILED");
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-fa516e2a\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/test-project/test_project_submission.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-md-12 mb-5" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-12" }, [
            _c("div", { staticClass: "card box_shadow pb-5" }, [
              _vm._m(0),
              _vm._v(" "),
              _c("div", { staticClass: "card-body" }, [
                _c("div", { staticClass: "border-row-bottom mb-4" }, [
                  _c("div", { staticClass: "row" }, [
                    _c("div", { staticClass: "col-md-12 pb-3" }, [
                      _c(
                        "span",
                        { staticClass: "font-bold-16 font-title-color" },
                        [_vm._v(_vm._s(_vm.testProject.name))]
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "row" }, [
                    _c("div", { staticClass: "col-md-6 text-left" }, [
                      _c("ul", { staticClass: "pl-0" }, [
                        _c("li", [
                          _c("label", { staticClass: "font-bold-12" }, [
                            _vm._v("Publisher: ")
                          ]),
                          _vm._v(" "),
                          _c("span", { staticClass: "font-12" }, [
                            _vm._v(_vm._s(_vm.originalUser.name))
                          ])
                        ]),
                        _vm._v(" "),
                        _vm._m(1),
                        _vm._v(" "),
                        _c("li", [
                          _c("label", { staticClass: "font-bold-12" }, [
                            _vm._v("Type: ")
                          ]),
                          _vm._v(" "),
                          _c("span", { staticClass: "font-12" }, [
                            _vm._v(
                              _vm._s(
                                _vm.testProject.project_type
                                  ? _vm.testProject.project_type.name
                                  : ""
                              )
                            )
                          ])
                        ])
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-3 text-center" }),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-3 text-right" }, [
                      _c("ul", [
                        _c("li", [
                          _c("label", { staticClass: "font-bold-12" }, [
                            _vm._v("Start Date: ")
                          ]),
                          _vm._v(" "),
                          _c("span", { staticClass: "font-12" }, [
                            _vm._v(
                              _vm._s(_vm._f("date")(_vm.testProject.start_date))
                            )
                          ])
                        ]),
                        _vm._v(" "),
                        _c("li", [
                          _c("label", { staticClass: "font-bold-12" }, [
                            _vm._v("End Date: ")
                          ]),
                          _vm._v(" "),
                          _c("span", { staticClass: "font-12" }, [
                            _vm._v(
                              _vm._s(_vm._f("date")(_vm.testProject.end_date))
                            )
                          ])
                        ])
                      ])
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "row" }, [
                    _c("div", { staticClass: "col-md-12 text-center" }, [
                      _c(
                        "a",
                        {
                          staticClass: "btn btn-success text-white",
                          attrs: { href: "javascript:;" },
                          on: {
                            click: function($event) {
                              _vm.acceptProjectSubmission(_vm.testProject)
                            }
                          }
                        },
                        [
                          _c("i", { staticClass: "fa fa-check" }),
                          _vm._v("Accept\n                                ")
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "a",
                        {
                          staticClass: "btn btn-danger text-white",
                          attrs: { href: "javascript:;" },
                          on: {
                            click: function($event) {
                              $event.preventDefault()
                              _vm.rejectProjectSubmission(_vm.testProject)
                            }
                          }
                        },
                        [
                          _c("i", { staticClass: "fa fa-times" }),
                          _vm._v(" Reject ")
                        ]
                      )
                    ])
                  ])
                ])
              ])
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-header bg-white border-0" }, [
      _c("label", { staticClass: "font-24" }, [_vm._v("Test Projects")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", [
      _c("label", { staticClass: "font-bold-12" }, [_vm._v("Devices: ")]),
      _vm._v(" "),
      _c("span", { staticClass: "font-12" }, [_vm._v("Cardboard, Oculus Rift")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-fa516e2a", module.exports)
  }
}

/***/ }),

/***/ "./resources/assets/components/pages/test-project/test_project_submission.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/test-project/test_project_submission.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-fa516e2a\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/test-project/test_project_submission.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\test-project\\test_project_submission.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-fa516e2a", Component.options)
  } else {
    hotAPI.reload("data-v-fa516e2a", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});