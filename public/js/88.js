webpackJsonp([88],{

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1f5e9b94\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/publisher/project_dashboard.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.custom-container[data-v-1f5e9b94] {\n  max-width: 100%;\n}\n.custom-container .text-blue[data-v-1f5e9b94] {\n    color: #0082cc;\n}\n.custom-container .text-red[data-v-1f5e9b94] {\n    color: #e63423;\n}\n.custom-container .text-bold[data-v-1f5e9b94] {\n    font-family: \"BrandonTextBold\";\n}\n.custom-container .step-page-title[data-v-1f5e9b94] {\n    font-family: \"UniNeueBold\";\n    font-size: 30px;\n    width: 100%;\n    text-align: center;\n    color: #363e48;\n    margin: 5px 0 25px;\n}\n.custom-container .dashboard-page[data-v-1f5e9b94] {\n    width: 100%;\n}\n.custom-container .dashboard-page .white-box[data-v-1f5e9b94] {\n      background-color: #fff;\n      -webkit-box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n              box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n      border-radius: 4px;\n      padding: 20px;\n      margin-bottom: 20px;\n      font-size: 17px;\n      color: #606368;\n}\n.custom-container .dashboard-page .white-box .block-title[data-v-1f5e9b94] {\n        font-size: 20px;\n        font-family: \"UniNeueBold\";\n        border-bottom: 1px solid #dadada;\n        color: #0082cc;\n        padding-bottom: 10px;\n        margin-bottom: 10px;\n        width: auto;\n        min-height: 35px;\n}\n.custom-container .dashboard-page .white-box .proj-status[data-v-1f5e9b94] {\n        font-size: 17px;\n        font-family: \"BrandonTextMedium\";\n        margin-bottom: 0;\n}\n.custom-container .dashboard-page .white-box .proj-count[data-v-1f5e9b94] {\n        font-size: 40px;\n        font-family: \"UniNeueBold\";\n        line-height: 30px;\n        color: #0082cc;\n}\n.custom-container .dashboard-page .white-box .no-proj-desc[data-v-1f5e9b94] {\n        padding: 25px 0 10px;\n        text-align: center;\n}\n.custom-container .dashboard-page .white-box .activity-list ul[data-v-1f5e9b94] {\n        padding-left: 20px;\n        list-style: disc;\n        margin-bottom: 0;\n}\n.custom-container .dashboard-page .white-box .activity-list ul li span[data-v-1f5e9b94] {\n          font-size: 14px;\n}\n.custom-container .dashboard-page .status-bar[data-v-1f5e9b94] {\n      width: 100%;\n      border-radius: 4px;\n      text-align: center;\n      font-size: 17px;\n      font-family: \"BrandonTextMedium\";\n      min-width: 40px;\n      padding: 5px 0;\n      background-color: #fff;\n      margin-bottom: 20px;\n}\n.custom-container .dashboard-page .status-bar.blue-status-bar[data-v-1f5e9b94] {\n      color: #0082cc;\n      border: 2px solid #0082cc;\n}\n.custom-container .dashboard-page .status-bar.green-status-bar[data-v-1f5e9b94] {\n      color: #2cac3d;\n      border: 2px solid #2cac3d;\n      display: none;\n}\n.custom-container .dashboard-page .blue-step-btn[data-v-1f5e9b94] {\n      width: 100%;\n      padding: 2px 5px;\n      border: 2px solid #0082cc;\n      background-color: #00aff5;\n      font-size: 14px;\n      font-family: \"BrandonTextMedium\";\n      letter-spacing: 0.05rem;\n      color: #fff;\n      border-radius: 20px;\n      display: block;\n      text-align: center;\n}\n.custom-container .dashboard-page .blue-step-btn[data-v-1f5e9b94]:hover {\n        background-color: #13b9fb;\n}\n.custom-container .dashboard-page .post-btn[data-v-1f5e9b94] {\n      padding: 7px 10px;\n}\n@media screen and (max-width: 1281px) {\n.custom-container .dashboard-page .white-box[data-v-1f5e9b94] {\n    font-size: 16px;\n}\n.custom-container .dashboard-page .white-box.proj-summary[data-v-1f5e9b94] {\n    padding: 20px 7px;\n}\n.custom-container .dashboard-page .blue-step-btn[data-v-1f5e9b94] {\n    font-size: 13px;\n}\n}\n@media screen and (max-width: 1200px) {\n.custom-container .page-title[data-v-1f5e9b94] {\n    font-size: 28px;\n    margin: 0 0 20px;\n}\n.custom-container .dashboard-page .blue-step-btn[data-v-1f5e9b94] {\n    font-size: 12px;\n    letter-spacing: 0;\n}\n.custom-container .dashboard-page .white-box[data-v-1f5e9b94] {\n    font-size: 14px;\n}\n.custom-container .dashboard-page .white-box .block-title[data-v-1f5e9b94] {\n      font-size: 18px;\n}\n.custom-container .dashboard-page .white-box .proj-count[data-v-1f5e9b94] {\n      font-size: 28px;\n      line-height: 24px;\n}\n.custom-container .dashboard-page .white-box .proj-status[data-v-1f5e9b94] {\n      font-size: 15px;\n}\n}\n@media screen and (max-width: 991px) {\n.custom-container .dashboard-page .white-box[data-v-1f5e9b94] {\n    padding: 15px;\n}\n}\n@media screen and (max-width: 767px) {\n.custom-container .dashboard-page .status-bar[data-v-1f5e9b94] {\n    font-size: 14px;\n}\n}\n@media screen and (max-width: 575px) {\n.custom-container .page-title[data-v-1f5e9b94] {\n    font-size: 26px;\n}\n.custom-container .dashboard-page .blue-step-btn.view-profile-btn[data-v-1f5e9b94] {\n    max-width: 180px;\n    margin: 0 auto 15px;\n}\n.custom-container .dashboard-page .white-box[data-v-1f5e9b94] {\n    font-size: 14px;\n    line-height: 18px;\n}\n.custom-container .dashboard-page .white-box.proj-summary[data-v-1f5e9b94] {\n    padding: 20px 10px;\n    max-height: 100px;\n    min-height: 100px;\n}\n.custom-container .dashboard-page .status-bar[data-v-1f5e9b94] {\n    margin-bottom: 15px;\n}\n.custom-container .dashboard-page .post-btn[data-v-1f5e9b94] {\n    max-width: 150px;\n    float: right;\n    margin-bottom: 15px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-1f5e9b94\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/publisher/project_dashboard.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container custom-container" }, [
    _c("div", { staticClass: "row" }, [
      _c("h1", { staticClass: "step-page-title" }, [
        _vm._v("\n      Dashboard\n    ")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "dashboard-page" }, [
        _vm._m(0),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _vm._m(1),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "col-sm-3 col-12" },
            [
              _c(
                "router-link",
                {
                  staticClass: "text-uppercase blue-step-btn post-btn",
                  attrs: { to: { name: "publisher.project.add" } }
                },
                [_vm._v("post new project")]
              )
            ],
            1
          )
        ]),
        _vm._v(" "),
        _vm._m(2),
        _vm._v(" "),
        _vm._m(3)
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12 col-sm-6 col-lg-3 text-center" }, [
        _c("div", { staticClass: "white-box proj-summary" }, [
          _c("h3", { staticClass: "proj-count" }, [_vm._v("0")]),
          _vm._v(" "),
          _c("p", { staticClass: "proj-status" }, [_vm._v("Running Projects")])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-12 col-sm-6 col-lg-3 text-center" }, [
        _c("div", { staticClass: "white-box proj-summary" }, [
          _c("h3", { staticClass: "proj-count" }, [_vm._v("1")]),
          _vm._v(" "),
          _c("p", { staticClass: "proj-status" }, [_vm._v("Pending Approvals")])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-12 col-sm-6 col-lg-3 text-center" }, [
        _c("div", { staticClass: "white-box proj-summary" }, [
          _c("h3", { staticClass: "proj-count" }, [_vm._v("0")]),
          _vm._v(" "),
          _c("p", { staticClass: "proj-status" }, [_vm._v("Inbox")])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-12 col-sm-6 col-lg-3 text-center" }, [
        _c("div", { staticClass: "white-box proj-summary" }, [
          _c("h3", { staticClass: "proj-count" }, [_vm._v("$0")]),
          _vm._v(" "),
          _c("p", { staticClass: "proj-status" }, [_vm._v("My spending")])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-sm-9 col-12" }, [
      _c("div", { staticClass: "status-bar blue-status-bar" }, [
        _vm._v(
          "\n            Welcome! Press post new project to start posting a project\n          "
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "white-box" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-md-6 col-12" }, [
          _c("h4", { staticClass: "block-title" }, [
            _vm._v("\n              Activities\n            ")
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "activity-list" }, [
            _c("ul", [
              _c("li", [
                _vm._v(
                  "\n                  Welcome you can now post the project. - "
                ),
                _c("span", { staticClass: "text-bold" }, [_vm._v("2 Mins ago")])
              ]),
              _vm._v(" "),
              _c("li", [
                _vm._v("\n                   Sign in - "),
                _c("span", { staticClass: "text-bold" }, [_vm._v("5 Mins ago")])
              ]),
              _vm._v(" "),
              _c("li", [
                _vm._v("\n                  Sample Activity #1 - "),
                _c("span", { staticClass: "text-bold" }, [
                  _vm._v("10 Mins ago")
                ])
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-6 col-12" }, [
          _c("h4", { staticClass: "block-title" }),
          _vm._v(" "),
          _c("div", { staticClass: "activity-list" }, [
            _c("ul", [
              _c("li", [
                _vm._v("\n                  Sample Activity #2 - "),
                _c("span", { staticClass: "text-bold" }, [
                  _vm._v("18 Mins ago")
                ])
              ]),
              _vm._v(" "),
              _c("li", [
                _vm._v("\n                  Sample Activity #3 - "),
                _c("span", { staticClass: "text-bold" }, [
                  _vm._v("23 Mins ago")
                ])
              ]),
              _vm._v(" "),
              _c("li", [
                _vm._v("\n                  Sample Activity #4 - "),
                _c("span", { staticClass: "text-bold" }, [
                  _vm._v("44 Mins ago")
                ])
              ])
            ])
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "white-box" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-12" }, [
          _c("h4", { staticClass: "block-title" }, [
            _vm._v("\n              Running Projects\n            ")
          ]),
          _vm._v(" "),
          _c("p", { staticClass: "no-proj-desc" }, [
            _vm._v(
              " You do not have any running projects. Please click on Post New Project button.\n              "
            )
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-1f5e9b94", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1f5e9b94\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/publisher/project_dashboard.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1f5e9b94\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/publisher/project_dashboard.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("5fea6a3b", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1f5e9b94\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project_dashboard.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1f5e9b94\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project_dashboard.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/components/pages/publisher/project_dashboard.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1f5e9b94\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/publisher/project_dashboard.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = null
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-1f5e9b94\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/publisher/project_dashboard.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-1f5e9b94"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\publisher\\project_dashboard.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1f5e9b94", Component.options)
  } else {
    hotAPI.reload("data-v-1f5e9b94", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});