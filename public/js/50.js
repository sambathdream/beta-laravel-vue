webpackJsonp([50],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/register.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__("./node_modules/vue/dist/vue.common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_form__ = __webpack_require__("./node_modules/vue-form/dist/vue-form.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_form___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_vue_form__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_src_validations_validations_js__ = __webpack_require__("./resources/assets/validations/validations.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




__WEBPACK_IMPORTED_MODULE_0_vue___default.a.use(__WEBPACK_IMPORTED_MODULE_1_vue_form___default.a, __WEBPACK_IMPORTED_MODULE_2_src_validations_validations_js__["a" /* default */]);
/* harmony default export */ __webpack_exports__["default"] = ({
    name: "register",
    data: function data() {
        return {
            formstate: {},
            model: {
                username: "",
                email: "",
                password: '',
                repeatPassword: '',
                terms: false
            }
        };
    },

    methods: {
        onSubmit: function onSubmit() {
            if (this.formstate.$invalid) {
                return;
            } else {
                this.$router.push("/");
            }
        }
    }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0b00358d\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/register.vue":
/***/ (function(module, exports, __webpack_require__) {

var escape = __webpack_require__("./node_modules/css-loader/lib/url/escape.js");
exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.login-content[data-v-0b00358d] {\r\n    margin-top: 6%;\r\n    margin-bottom: 6%;\r\n    padding-bottom: 20px;\r\n    -webkit-box-shadow: 0 0 30px #ccc;\r\n            box-shadow: 0 0 30px #ccc;\r\n    background-size: 100% 100%;\r\n    border-radius: 7px;\n}\n.img_backgrond[data-v-0b00358d]{\r\n    background-image: url(" + escape(__webpack_require__("./resources/assets/assets/img/pages/Login-03-01.png")) + ");\r\n    background-size: cover;\r\n    padding: 75px 15px;\n}\n.text_gray[data-v-0b00358d]{\r\n        color:#625e5e;\n}\nlabel[data-v-0b00358d]{\r\n    font-size: 14px !important;\n}\n[data-v-0b00358d]::-webkit-input-placeholder {\r\n   font-size: 14px;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-0b00358d\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/register.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid img_backgrond" }, [
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        {
          staticClass:
            "col-lg-4 offset-lg-4 col-sm-6 offset-sm-3 col-xs-10 offset-xs-1 login-content mt-5"
        },
        [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "vue-form",
            {
              attrs: { state: _vm.formstate },
              on: {
                submit: function($event) {
                  $event.preventDefault()
                  _vm.onSubmit($event)
                }
              }
            },
            [
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-sm-12 mt-3" }, [
                  _c(
                    "div",
                    { staticClass: "form-group" },
                    [
                      _c(
                        "validate",
                        { attrs: { tag: "div" } },
                        [
                          _c("label", { attrs: { for: "user_name" } }, [
                            _vm._v(" User Name")
                          ]),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.model.username,
                                expression: "model.username"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              id: "user_name",
                              name: "username",
                              type: "text",
                              required: "",
                              autofocus: "",
                              placeholder: "User Name"
                            },
                            domProps: { value: _vm.model.username },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.model,
                                  "username",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c(
                            "field-messages",
                            {
                              staticClass: "text-danger",
                              attrs: {
                                name: "username",
                                show: "$invalid && $submitted"
                              }
                            },
                            [
                              _c(
                                "div",
                                {
                                  attrs: { slot: "required" },
                                  slot: "required"
                                },
                                [_vm._v("Username is a required field")]
                              )
                            ]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-sm-12" }, [
                  _c(
                    "div",
                    { staticClass: "form-group" },
                    [
                      _c(
                        "validate",
                        { attrs: { tag: "div" } },
                        [
                          _c("label", { attrs: { for: "email" } }, [
                            _vm._v(" E-mail")
                          ]),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.model.email,
                                expression: "model.email"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              id: "email",
                              name: "email",
                              type: "email",
                              required: "",
                              placeholder: "E-mail"
                            },
                            domProps: { value: _vm.model.email },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.model,
                                  "email",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c(
                            "field-messages",
                            {
                              staticClass: "text-danger",
                              attrs: {
                                name: "email",
                                show: "$invalid && $submitted"
                              }
                            },
                            [
                              _c(
                                "div",
                                {
                                  attrs: { slot: "required" },
                                  slot: "required"
                                },
                                [_vm._v("Email is a required field")]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { attrs: { slot: "email" }, slot: "email" },
                                [_vm._v("Email is not valid")]
                              )
                            ]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-sm-6" }, [
                  _c(
                    "div",
                    { staticClass: "form-group" },
                    [
                      _c(
                        "validate",
                        { attrs: { tag: "div" } },
                        [
                          _c("label", { attrs: { for: "password" } }, [
                            _vm._v(" Password")
                          ]),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.model.password,
                                expression: "model.password"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              id: "password",
                              name: "password",
                              type: "password",
                              required: "",
                              placeholder: "Password",
                              minlength: "4",
                              maxlength: "10"
                            },
                            domProps: { value: _vm.model.password },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.model,
                                  "password",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c(
                            "field-messages",
                            {
                              staticClass: "text-danger",
                              attrs: {
                                name: "password",
                                show: "$invalid && $submitted"
                              }
                            },
                            [
                              _c(
                                "div",
                                {
                                  attrs: { slot: "required" },
                                  slot: "required"
                                },
                                [_vm._v("Password is required")]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  attrs: { slot: "minlength" },
                                  slot: "minlength"
                                },
                                [
                                  _vm._v(
                                    "Password should be atleast 4 characters long"
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  attrs: { slot: "maxlength" },
                                  slot: "maxlength"
                                },
                                [
                                  _vm._v(
                                    "Password should be atmost 10 characters long"
                                  )
                                ]
                              )
                            ]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-sm-6" }, [
                  _c(
                    "div",
                    { staticClass: "form-group" },
                    [
                      _c(
                        "validate",
                        { attrs: { tag: "div" } },
                        [
                          _c("label", { attrs: { for: "confirm_password" } }, [
                            _vm._v(" Confirm Password")
                          ]),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.model.repeatPassword,
                                expression: "model.repeatPassword"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              name: "repeatpassword",
                              id: "confirm_password",
                              type: "password",
                              required: "",
                              placeholder: "Confirm Password",
                              sameas: _vm.model.password
                            },
                            domProps: { value: _vm.model.repeatPassword },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.model,
                                  "repeatPassword",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c(
                            "field-messages",
                            {
                              staticClass: "text-danger",
                              attrs: {
                                name: "repeatpassword",
                                show: "$invalid && $submitted"
                              }
                            },
                            [
                              _c(
                                "div",
                                {
                                  attrs: { slot: "required" },
                                  slot: "required"
                                },
                                [_vm._v("Password confirmatoin is required")]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { attrs: { slot: "sameas" }, slot: "sameas" },
                                [
                                  _vm._v(
                                    "Password and Confirm password should match"
                                  )
                                ]
                              )
                            ]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "col-sm-12" },
                  [
                    _c(
                      "validate",
                      { attrs: { tag: "label" } },
                      [
                        _c(
                          "label",
                          {
                            staticClass:
                              "form-group custom-control custom-checkbox"
                          },
                          [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.model.terms,
                                  expression: "model.terms"
                                }
                              ],
                              staticClass:
                                "custom-control-input checkbox_label",
                              attrs: {
                                type: "checkbox",
                                name: "terms",
                                id: "terms",
                                checkbox: ""
                              },
                              domProps: {
                                checked: Array.isArray(_vm.model.terms)
                                  ? _vm._i(_vm.model.terms, null) > -1
                                  : _vm.model.terms
                              },
                              on: {
                                change: function($event) {
                                  var $$a = _vm.model.terms,
                                    $$el = $event.target,
                                    $$c = $$el.checked ? true : false
                                  if (Array.isArray($$a)) {
                                    var $$v = null,
                                      $$i = _vm._i($$a, $$v)
                                    if ($$el.checked) {
                                      $$i < 0 &&
                                        (_vm.model.terms = $$a.concat([$$v]))
                                    } else {
                                      $$i > -1 &&
                                        (_vm.model.terms = $$a
                                          .slice(0, $$i)
                                          .concat($$a.slice($$i + 1)))
                                    }
                                  } else {
                                    _vm.$set(_vm.model, "terms", $$c)
                                  }
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("span", {
                              staticClass: "custom-control-indicator"
                            }),
                            _vm._v(" "),
                            _c(
                              "span",
                              {
                                staticClass:
                                  "custom-control-description text_gray"
                              },
                              [
                                _vm._v(
                                  "I agree the\n                            "
                                ),
                                _c(
                                  "span",
                                  [
                                    _c(
                                      "router-link",
                                      {
                                        staticClass: "text-info",
                                        attrs: { to: "/" }
                                      },
                                      [_vm._v("terms & conditions")]
                                    )
                                  ],
                                  1
                                )
                              ]
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "field-messages",
                          {
                            staticClass: "text-danger",
                            attrs: {
                              name: "terms",
                              show: "$invalid && $submitted"
                            }
                          },
                          [
                            _c(
                              "div",
                              { attrs: { slot: "checkbox" }, slot: "checkbox" },
                              [_vm._v("Terms must be accepted")]
                            )
                          ]
                        )
                      ],
                      1
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c("div", { staticClass: "col-sm-12" }, [
                  _c("div", { staticClass: "form-group float-right" }, [
                    _c("input", {
                      staticClass: "btn btn-success",
                      attrs: { type: "submit", value: "Sign Up" }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("div", { staticClass: "col-sm-12 text-center" }, [
                  _c(
                    "div",
                    { staticClass: "form-group" },
                    [
                      _c(
                        "router-link",
                        {
                          staticClass: "btn btn-primary btn-block",
                          attrs: { tag: "a", to: "/login" }
                        },
                        [
                          _vm._v(
                            "Already a member? Sign In\n                            "
                          )
                        ]
                      )
                    ],
                    1
                  )
                ])
              ])
            ]
          )
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-sm-12 mt-3" }, [
        _c("h2", { staticClass: "text-center" }, [
          _c("img", {
            attrs: { src: __webpack_require__("./resources/assets/assets/img/logo_black.png"), alt: "Logo" }
          })
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0b00358d", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0b00358d\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/register.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0b00358d\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/register.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("6a275b95", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0b00358d\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./register.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0b00358d\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./register.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/assets/img/logo_black.png":
/***/ (function(module, exports) {

module.exports = "/images/logo_black.png?117d588be583d4d6e207df3054b95a99";

/***/ }),

/***/ "./resources/assets/assets/img/pages/Login-03-01.png":
/***/ (function(module, exports) {

module.exports = "/images/Login-03-01.png?13d89c25d2f5166aa12b08827d4c9b92";

/***/ }),

/***/ "./resources/assets/components/pages/register.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0b00358d\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/register.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/register.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-0b00358d\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/register.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-0b00358d"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\register.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0b00358d", Component.options)
  } else {
    hotAPI.reload("data-v-0b00358d", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});