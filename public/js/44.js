webpackJsonp([44],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/tester/project-fillup.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "project-fillup"
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-dff5a056\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/project-fillup.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.custom-container[data-v-dff5a056] {\n  max-width: 100%;\n}\n.custom-container .text-green[data-v-dff5a056] {\n    color: #2cac3d;\n}\n.custom-container .text-red[data-v-dff5a056] {\n    color: #e63423;\n}\n.custom-container .text-bold[data-v-dff5a056] {\n    font-family: \"BrandonTextBold\" !important;\n}\n.custom-container .text-medium[data-v-dff5a056] {\n    font-family: \"BrandonTextMedium\";\n}\n.custom-container .test-proj-fillup-page[data-v-dff5a056] {\n    width: 100%;\n}\n.custom-container .test-proj-fillup-page .white-box[data-v-dff5a056] {\n      background-color: #fff;\n      -webkit-box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n              box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n      border-radius: 4px;\n      padding: 20px;\n      margin-bottom: 20px;\n      font-size: 17px;\n      line-height: 22px;\n      color: #606368;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title[data-v-dff5a056] {\n        font-size: 20px;\n        font-family: \"UniNeueBold\";\n        border-bottom: 1px solid #dadada;\n        color: #2cac3d;\n        padding-bottom: 10px;\n        margin-bottom: 10px;\n        width: auto;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title .badge-wrap[data-v-dff5a056] {\n          border: 1px solid #2cac3d;\n          background-color: #2cac3d;\n          color: #fff;\n          font-size: 16px;\n          right: -5px;\n          position: absolute;\n          border-top-left-radius: 16px;\n          border-bottom-left-radius: 16px;\n          padding: 2px 12px;\n          top: -5px;\n          font-family: \"BrandonTextRegular\";\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status[data-v-dff5a056] {\n        padding: 0;\n        padding-top: 8px;\n        overflow: hidden;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status:first-child .proj-progress[data-v-dff5a056]::before {\n          width: calc(50% - 30px);\n          left: 15px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status:last-child .proj-progress[data-v-dff5a056]::after {\n          width: calc(50% - 30px);\n          right: 15px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status .proj-progress[data-v-dff5a056] {\n          position: relative;\n          margin-bottom: 5px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status .proj-progress i[data-v-dff5a056] {\n            color: rgba(175, 177, 179, 0.5);\n            font-size: 22px;\n            line-height: 24px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status .proj-progress[data-v-dff5a056]::before {\n            content: \"\";\n            position: absolute;\n            height: 3px;\n            width: calc(50% - 5px);\n            background-color: #ececec;\n            left: -10px;\n            top: 11px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status .proj-progress[data-v-dff5a056]::after {\n            content: \"\";\n            position: absolute;\n            height: 3px;\n            width: calc(50% - 5px);\n            background-color: #ececec;\n            right: -10px;\n            top: 11px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.active[data-v-dff5a056] {\n        color: #2cac3d;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.active .proj-progress i[data-v-dff5a056] {\n          color: #2cac3d;\n          font-size: 34px;\n          line-height: 24px;\n          position: relative;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.active .proj-progress i[data-v-dff5a056]::after {\n            height: 15px;\n            width: 15px;\n            background-color: #2cac3d;\n            content: \"\";\n            border-radius: 50%;\n            position: absolute;\n            left: 7px;\n            top: 4px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.active .proj-progress[data-v-dff5a056]::before {\n          background-color: #2cac3d;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-desc[data-v-dff5a056] {\n        letter-spacing: 0.01rem;\n        margin-bottom: 5px;\n}\n.custom-container .test-proj-fillup-page .white-box .notes-list[data-v-dff5a056] {\n        padding-left: 15px;\n        margin-bottom: 0px;\n}\n.custom-container .test-proj-fillup-page .white-box .notes-list li[data-v-dff5a056] {\n          position: relative;\n}\n.custom-container .test-proj-fillup-page .white-box .notes-list li[data-v-dff5a056]::before {\n            content: \"-\";\n            position: absolute;\n            left: -15px;\n            top: -1px;\n            color: #606368;\n            font-size: 20px;\n}\n.custom-container .test-proj-fillup-page .white-box .view-more-link[data-v-dff5a056] {\n        font-size: 15px;\n        font-family: \"BrandonTextMedium\";\n        color: #2cac3d;\n        text-decoration: underline !important;\n        text-align: right;\n}\n.custom-container .test-proj-fillup-page .white-box .test-device-list span[data-v-dff5a056] {\n        margin-right: 15px;\n        margin-top: 10px;\n}\n.custom-container .test-proj-fillup-page .green-step-btn[data-v-dff5a056] {\n      width: 100%;\n      max-width: 240px;\n      padding: 7px 10px;\n      border: 2px solid #118921;\n      background-color: #2cac3d;\n      font-size: 14px;\n      font-family: \"BrandonTextMedium\";\n      letter-spacing: 0.05rem;\n      color: #fff;\n      border-radius: 20px;\n      display: inline-block;\n      text-align: center;\n      margin: 10px 15px 30px 0px;\n}\n.custom-container .test-proj-fillup-page .green-step-btn[data-v-dff5a056]:hover {\n        background-color: #158f25;\n}\n@media screen and (max-width: 1281px) {\n.custom-container .test-proj-fillup-page .white-box[data-v-dff5a056] {\n    font-size: 16px;\n}\n.custom-container .test-proj-fillup-page .green-step-btn[data-v-dff5a056] {\n    font-size: 13px;\n}\n}\n@media screen and (max-width: 1200px) {\n.custom-container .test-proj-fillup-page .green-step-btn[data-v-dff5a056] {\n    font-size: 12px;\n    letter-spacing: 0;\n}\n.custom-container .test-proj-fillup-page .white-box[data-v-dff5a056] {\n    font-size: 14px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title[data-v-dff5a056] {\n      font-size: 18px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title .badge-wrap[data-v-dff5a056] {\n        font-size: 15px;\n}\n}\n@media screen and (max-width: 991px) {\n.custom-container .test-proj-fillup-page .white-box[data-v-dff5a056] {\n    padding: 15px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title .badge-wrap[data-v-dff5a056] {\n      right: 0;\n}\n}\n@media screen and (max-width: 767px) {\n.custom-container .test-proj-fillup-page .status-bar[data-v-dff5a056] {\n    font-size: 14px;\n}\n.custom-container .test-proj-fillup-page .green-step-btn[data-v-dff5a056] {\n    margin: 10px 15px 10px 0;\n    max-width: 175px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title .badge-wrap[data-v-dff5a056] {\n    right: 0;\n    top: -3px;\n}\n.custom-container .test-proj-fillup-page .white-box .notes-list[data-v-dff5a056] {\n    margin-bottom: 15px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.active span[data-v-dff5a056] {\n    display: block !important;\n}\n}\n@media screen and (max-width: 575px) {\n.custom-container .step-page-title[data-v-dff5a056] {\n    font-size: 26px;\n}\n.custom-container .test-proj-fillup-page .white-box[data-v-dff5a056] {\n    font-size: 14px;\n    line-height: 18px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title.proj-name-wrap[data-v-dff5a056] {\n      padding-bottom: 35px;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title.proj-name-wrap .proj-name[data-v-dff5a056] {\n        display: block;\n        width: 100%;\n}\n.custom-container .test-proj-fillup-page .white-box .block-title.proj-name-wrap .badge-wrap[data-v-dff5a056] {\n        position: relative;\n        float: right;\n        margin-right: -15px;\n        margin-top: 10px;\n        font-size: 13px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status:first-child .proj-progress[data-v-dff5a056]::before {\n      width: calc(50% - 25px);\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status:last-child .proj-progress[data-v-dff5a056]::after {\n      width: calc(50% - 25px);\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.active .proj-progress i[data-v-dff5a056] {\n      font-size: 28px;\n}\n.custom-container .test-proj-fillup-page .white-box .proj-status.active .proj-progress i[data-v-dff5a056]::after {\n        height: 13px;\n        width: 13px;\n        left: 6px;\n        top: 5px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-dff5a056\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/tester/project-fillup.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bg-color" }, [
      _c("div", { staticClass: "container custom-container" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "test-proj-fillup-page" }, [
            _c("div", { staticClass: "white-box" }, [
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-12" }, [
                  _c("h4", { staticClass: "block-title border-bottom-0" }, [
                    _vm._v("\n                Project Status\n              ")
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-12" }, [
                  _c("div", { staticClass: "row" }, [
                    _c(
                      "div",
                      { staticClass: "col-2 text-center proj-status active" },
                      [
                        _c("p", { staticClass: "proj-progress" }, [
                          _c("i", { staticClass: "fa fa-circle-thin" })
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "d-none d-md-block text-medium" },
                          [_vm._v("New")]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "col-2 text-center proj-status" },
                      [
                        _c("p", { staticClass: "proj-progress" }, [
                          _c("i", { staticClass: "fa fa-circle-thin" })
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "d-none d-md-block text-medium" },
                          [_vm._v("Accepted/Working")]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "col-2 text-center proj-status" },
                      [
                        _c("p", { staticClass: "proj-progress" }, [
                          _c("i", { staticClass: "fa fa-circle-thin" })
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "d-none d-md-block text-medium" },
                          [_vm._v("Submitted")]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "col-2 text-center proj-status" },
                      [
                        _c("p", { staticClass: "proj-progress" }, [
                          _c("i", { staticClass: "fa fa-circle-thin" })
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "d-none d-md-block text-medium" },
                          [_vm._v(" Under Review")]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "col-2 text-center proj-status" },
                      [
                        _c("p", { staticClass: "proj-progress" }, [
                          _c("i", { staticClass: "fa fa-circle-thin" })
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "d-none d-md-block text-medium" },
                          [_vm._v("Completed")]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "col-2 text-center proj-status" },
                      [
                        _c("p", { staticClass: "proj-progress" }, [
                          _c("i", { staticClass: "fa fa-circle-thin" })
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "d-none d-md-block text-medium" },
                          [_vm._v(" Paid")]
                        )
                      ]
                    )
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "white-box" }, [
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-12" }, [
                  _c(
                    "h4",
                    { staticClass: "text-bold block-title proj-name-wrap" },
                    [
                      _c("span", { staticClass: "proj-name" }, [
                        _vm._v("Test")
                      ]),
                      _vm._v(" "),
                      _c("span", { staticClass: "badge-wrap text-right" }, [
                        _c("span", { staticClass: "status-badge" }, [
                          _vm._v(
                            "\n                    Status:Open Spot(Unlimited Candidates)\n                  "
                          )
                        ])
                      ])
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-12 col-sm-6 col-xl-3" }, [
                  _c("p", { staticClass: "mb-1 mt-2 mt-sm-0" }, [
                    _c("span", { staticClass: "text-bold" }, [
                      _vm._v("Devices :")
                    ]),
                    _vm._v(" "),
                    _c("span", {}, [_vm._v("The Beta Plan")])
                  ]),
                  _vm._v(" "),
                  _c("p", { staticClass: "mb-xl-5 mb-3 mt-2 mt-sm-0" }, [
                    _c("span", { staticClass: "text-bold" }, [
                      _vm._v("Type :")
                    ]),
                    _vm._v(" "),
                    _c("span", {}, [_vm._v("Games")])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-12 col-sm-6 col-xl-3" }, [
                  _c("p", { staticClass: "mb-1 mt-2 mt-sm-0" }, [
                    _c("span", { staticClass: "text-bold" }, [
                      _vm._v("Start Date : ")
                    ]),
                    _vm._v(" "),
                    _c("span", [_vm._v("10 January, 2018")])
                  ]),
                  _vm._v(" "),
                  _c("p", { staticClass: "mb-xl-5 mb-3 mt-2 mt-sm-0" }, [
                    _c("span", { staticClass: "text-bold" }, [
                      _vm._v("End Date : ")
                    ]),
                    _vm._v(" "),
                    _c("span", [_vm._v("15 January, 2018")])
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "col-12 col-xl-6 text-xl-right text-left" },
                  [
                    _c("p", { staticClass: "mb-xl-5 mb-3 mt-2 mt-sm-0" }, [
                      _c("span", { staticClass: "text-bold" }, [
                        _vm._v("Estimated Tester Time needed to complete: ")
                      ]),
                      _vm._v(" "),
                      _c("span", { staticClass: "d-block" }, [
                        _vm._v("1 hr 15 Minutes")
                      ])
                    ])
                  ]
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-12" }, [
                  _c("h4", { staticClass: "text-bold block-title" }, [
                    _vm._v("Project Description\n              ")
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-12" }, [
                  _c("p", { staticClass: "proj-desc" }, [
                    _vm._v(
                      "\n                The project description goes here, and this project will have a number of testers, testing based on usability, bug detection and overall fun factor. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the...\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c(
                    "a",
                    {
                      staticClass:
                        "view-more-link mb-xl-5 mb-md-3 md-1 w-100 float-right",
                      attrs: { href: "#" }
                    },
                    [_vm._v("\n                View More\n              ")]
                  )
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-12 col-md-6" }, [
                  _c("h4", { staticClass: "block-title text-bold" }, [
                    _vm._v("Notes/Instruction")
                  ]),
                  _vm._v(" "),
                  _c("p", { staticClass: "mb-1" }, [
                    _vm._v("Notes and Instruction goes here...")
                  ]),
                  _vm._v(" "),
                  _c("ul", { staticClass: "notes-list" }, [
                    _c("li", [_vm._v("point one")]),
                    _vm._v(" "),
                    _c("li", [_vm._v("point two")])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-12 col-md-6" }, [
                  _c("h4", { staticClass: "block-title text-bold" }, [
                    _vm._v("Required Devices")
                  ]),
                  _vm._v(" "),
                  _c("p", { staticClass: "test-device-list" }, [
                    _c("span", { staticClass: "d-inline-block" }, [
                      _c("img", {
                        staticClass: "img-fluid",
                        attrs: { src: __webpack_require__("./resources/assets/assets/img/test-device-1.png") }
                      })
                    ]),
                    _vm._v(" "),
                    _c("span", { staticClass: "d-inline-block" }, [
                      _c("img", {
                        staticClass: "img-fluid",
                        attrs: { src: __webpack_require__("./resources/assets/assets/img/test-device-2.png") }
                      })
                    ]),
                    _vm._v(" "),
                    _c("span", { staticClass: "d-inline-block" }, [
                      _c("img", {
                        staticClass: "img-fluid",
                        attrs: { src: __webpack_require__("./resources/assets/assets/img/test-device-3.png") }
                      })
                    ]),
                    _vm._v(" "),
                    _c("span", { staticClass: "d-inline-block" }, [
                      _c("img", {
                        staticClass: "img-fluid",
                        attrs: { src: __webpack_require__("./resources/assets/assets/img/test-device-4.png") }
                      })
                    ])
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-12 text-center" }, [
              _c(
                "a",
                {
                  staticClass: "btn text-uppercase green-step-btn",
                  attrs: { href: "#" }
                },
                [_vm._v("start working / accept")]
              ),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "btn text-uppercase green-step-btn",
                  attrs: { href: "#" }
                },
                [_vm._v("Back To Dashboard")]
              )
            ])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-dff5a056", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-dff5a056\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/project-fillup.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-dff5a056\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/project-fillup.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("4d716dbe", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-dff5a056\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project-fillup.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-dff5a056\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project-fillup.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/assets/img/test-device-1.png":
/***/ (function(module, exports) {

module.exports = "/images/test-device-1.png?ca006337f2ce3652843331514e07da3f";

/***/ }),

/***/ "./resources/assets/assets/img/test-device-2.png":
/***/ (function(module, exports) {

module.exports = "/images/test-device-2.png?7e85e5afce72acbb94e430440b19b9ff";

/***/ }),

/***/ "./resources/assets/assets/img/test-device-3.png":
/***/ (function(module, exports) {

module.exports = "/images/test-device-3.png?3cdb3e3f4343ea4d714e3b341994cf8c";

/***/ }),

/***/ "./resources/assets/assets/img/test-device-4.png":
/***/ (function(module, exports) {

module.exports = "/images/test-device-4.png?ff2a656e113eb44caef72b06b261b546";

/***/ }),

/***/ "./resources/assets/components/pages/tester/project-fillup.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-dff5a056\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/project-fillup.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/tester/project-fillup.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-dff5a056\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/tester/project-fillup.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-dff5a056"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\tester\\project-fillup.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-dff5a056", Component.options)
  } else {
    hotAPI.reload("data-v-dff5a056", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});