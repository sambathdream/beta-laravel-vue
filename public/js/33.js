webpackJsonp([33],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/star-rating.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "StarRating",
  props: {
    value: {
      default: 0
    },
    disabled: {
      default: false
    },
    small: {
      default: false
    }
  },
  data: function data() {
    return {
      maxStars: 5,
      currentRating: 0,
      totalStars: []
    };
  },
  mounted: function mounted() {
    this.currentRating = this.value;
    for (var i = 1; i <= this.maxStars; i++) {
      this.totalStars.push(i);
    }
  },

  watch: {
    value: function value(newVal, oldVal) {
      this.currentRating = newVal;
    }
  },
  methods: {
    setRating: function setRating(v) {
      if (this.disabled) {
        return true;
      }
      this.currentRating = v;
    },
    selected: function selected(v) {
      if (this.disabled) {
        return true;
      }
      this.setRating(v);
      this.$emit("input", v);
      this.$emit("change", v);
    }
  },
  computed: {
    displayRating: function displayRating() {
      if (this.totalStars.indexOf(this.currentRating) >= 0) {
        return this.currentRating;
      }
      if (/\.5$/.test(this.currentRating.toString())) {
        return this.currentRating;
      }
      return Math.floor(this.currentRating);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/tester/test-active_project_item.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'test_active_project_item',
  props: {
    testerProject: {
      type: Object,
      required: true
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/tester/dashboard.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__("./node_modules/vue/dist/vue.common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment__ = __webpack_require__("./node_modules/moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_components_components_tester_test_active_project_item__ = __webpack_require__("./resources/assets/components/components/tester/test-active_project_item.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_components_components_tester_test_active_project_item___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_components_components_tester_test_active_project_item__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_components_components_star_rating__ = __webpack_require__("./resources/assets/components/components/star-rating.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_components_components_star_rating___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_components_components_star_rating__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  name: "tester-profile",
  components: {
    'moment': __WEBPACK_IMPORTED_MODULE_1_moment___default.a,
    TestActiveProjectItem: __WEBPACK_IMPORTED_MODULE_2_components_components_tester_test_active_project_item___default.a,
    StarRating: __WEBPACK_IMPORTED_MODULE_3_components_components_star_rating___default.a
  },
  data: function data() {
    return {
      rating: window.USER.rating,
      model: {},
      isApprovedTester: 0,
      isApplicationSubmitted: 0,
      aActiveProjects: [],
      aCompletedProjects: [],
      aOpenProjects: [],
      fActiveProjects: [],
      fCompletedProjects: [],
      fOpenProjects: [],
      latestStatus: ""
    };
  },

  mounted: function mounted() {
    var _this = this;

    if (window.USER.latest_project_status) {
      this.latestStatus = window.USER.latest_project_status.name;
    }
    axios.get("/api/tester/dashboard/" + this.$store.state.user.id).then(function (response) {
      var data = response.data;
      _this.isApplicationSubmitted = data.isApplicationSubmitted;
      _this.isApprovedTester = data.isApprovedTester;
      _this.aActiveProjects = data.aAcceptedProjects ? data.aAcceptedProjects : [];
      _this.aCompletedProjects = data.aCompletedProjects ? data.aCompletedProjects : [];
      _this.aOpenProjects = data.aOpenProjects ? data.aOpenProjects : [];
      _this.fOpenProjects = data.aOpenProjects ? data.aOpenProjects.slice(0, 5) : [];
      _this.fActiveProjects = data.aAcceptedProjects ? data.aAcceptedProjects.slice(0, 5) : [];
      _this.fCompletedProjects = data.aCompletedProjects ? data.aCompletedProjects.slice(0, 5) : [];
    });
  },
  methods: {
    viewOpenprojects: function viewOpenprojects() {
      this.fOpenProjects = this.aOpenProjects;
    },
    viewActiveprojects: function viewActiveprojects() {
      this.fActiveProjects = this.aActiveProjects;
    },
    viewCompletedProjects: function viewCompletedProjects() {
      this.fCompletedProjects = this.aCompletedProjects;
    },
    acceptProject: function acceptProject() {
      var _this2 = this;

      axios.post("/api/tester/update-test-status/" + this.$route.params.id, {
        test_status: "ACCEPTED"
      }).then(function (response) {
        if (response.data.success) {
          _this2.$router.push({ name: "tester.project-submission" });
        }
      }).catch(function (error) {});
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-323b1e04\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/star-rating.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\nspan[data-v-323b1e04] {\n  font-size: 12px;\n}\n.starElement[data-v-323b1e04] {\n  font-size: 18px;\n  margin-right: 12px;\n  color: black;\n}\n.starElement.small[data-v-323b1e04] {\n  font-size: 12px;\n  margin-right: 5px;\n}\n.starElement.fa-star[data-v-323b1e04],\n.starElement.fa-star-half-o[data-v-323b1e04] {\n  color: #ffd055;\n}\n.starElement.clickable[data-v-323b1e04] {\n  cursor: pointer;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4ace62be\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/dashboard.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.custom-container[data-v-4ace62be] {\n  max-width: 100%;\n}\n.custom-container .text-green[data-v-4ace62be] {\n    color: #2cac3d;\n}\n.custom-container .text-red[data-v-4ace62be] {\n    color: #e63423;\n}\n.custom-container .text-bold[data-v-4ace62be] {\n    font-family: \"BrandonTextBold\";\n}\n.custom-container .step-page-title[data-v-4ace62be] {\n    font-family: \"UniNeueBold\";\n    font-size: 30px;\n    width: 100%;\n    text-align: center;\n    color: #363e48;\n    margin: 5px 0 25px;\n}\n.custom-container .dashboard-page[data-v-4ace62be] {\n    width: 100%;\n}\n.custom-container .dashboard-page .status-bar[data-v-4ace62be] {\n      width: 100%;\n      border-radius: 4px;\n      text-align: center;\n      font-size: 17px;\n      font-family: \"BrandonTextMedium\";\n      min-width: 40px;\n      padding: 5px 0;\n      background-color: #fff;\n      margin-bottom: 20px;\n}\n.custom-container .dashboard-page .status-bar.disapprove-status[data-v-4ace62be] {\n      border: 2px solid #e63423;\n      color: #e63423;\n}\n.custom-container .dashboard-page .status-bar.approve-status[data-v-4ace62be] {\n      /* display: none; */\n      border: 2px solid #2cac3d;\n      color: #2cac3d;\n}\n.custom-container .dashboard-page .status-bar.underprocess-status[data-v-4ace62be] {\n      /* display: none; */\n      width: 100%;\n      border: 2px solid #ff7800;\n      color: #ff7800;\n      border-radius: 4px;\n      text-align: center;\n      font-size: 17px;\n      font-family: \"BrandonTextMedium\";\n      min-width: 40px;\n      padding: 5px 0;\n      background-color: #fff;\n      margin-bottom: 20px;\n}\n.custom-container .dashboard-page .white-box[data-v-4ace62be] {\n      background-color: #fff;\n      -webkit-box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n              box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n      border-radius: 4px;\n      padding: 20px;\n      margin-bottom: 20px;\n      font-size: 14px;\n      color: #606368;\n      /*  Start developer */\n      /* End  developer */\n}\n.custom-container .dashboard-page .white-box .device-list img[data-v-4ace62be] {\n        max-width: 20px;\n        margin-left: 5px;\n}\n.custom-container .dashboard-page .white-box .block-title[data-v-4ace62be] {\n        font-size: 20px;\n        font-family: \"UniNeueBold\";\n        border-bottom: 1px solid #dadada;\n        color: #2cac3d;\n        padding-bottom: 10px;\n        margin-bottom: 10px;\n        width: auto;\n}\n.custom-container .dashboard-page .white-box .block-title a[data-v-4ace62be] {\n          font-family: \"BrandonTextMedium\";\n          font-size: 17px;\n          text-decoration: underline !important;\n          float: right;\n}\n.custom-container .dashboard-page .white-box .proj-status[data-v-4ace62be] {\n        font-size: 17px;\n        font-family: \"BrandonTextMedium\";\n        margin-bottom: 0;\n}\n.custom-container .dashboard-page .white-box .proj-count[data-v-4ace62be] {\n        font-size: 40px;\n        font-family: \"UniNeueBold\";\n        line-height: 30px;\n}\n.custom-container .dashboard-page .white-box .active-proj-name[data-v-4ace62be] {\n        font-size: 16px;\n        text-transform: uppercase;\n        color: #363e48;\n}\n.custom-container .dashboard-page .white-box .progress-txt[data-v-4ace62be] {\n        font-size: 17px;\n}\n.custom-container .dashboard-page .white-box .open-proj-txt[data-v-4ace62be] {\n        font-size: 17px;\n        padding: 30px 0 10px;\n        color: #9c9c9c;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block[data-v-4ace62be] {\n        width: 100%;\n        border-top: 1px solid #dadada;\n        padding: 20px 0;\n        font-size: 14px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block[data-v-4ace62be]:first-child {\n          border-top: none;\n          padding-top: 10px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block[data-v-4ace62be]:last-child {\n          padding-bottom: 5px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block p[data-v-4ace62be] {\n          margin: 5px 0;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .xl-border-right[data-v-4ace62be] {\n          border-right: 1px solid #dadada;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .btn-wrap[data-v-4ace62be] {\n          position: relative;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .btn-wrap .green-step-btn[data-v-4ace62be] {\n            padding: 3px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .btn-wrap .badge-wrap[data-v-4ace62be] {\n            position: relative;\n            border: 1px solid #2cac3d;\n            border-right: 0;\n            border-top-left-radius: 16px;\n            border-bottom-left-radius: 16px;\n            margin-right: -20px;\n            padding: 3px 40px;\n            line-height: 20px;\n            font-size: 15px;\n            z-index: 1;\n            margin-bottom: 15px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .btn-wrap .pending-amt[data-v-4ace62be] {\n            font-size: 22px;\n            line-height: 26px;\n            text-align: center;\n            font-family: \"BrandonTextMedium\";\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .proj-name[data-v-4ace62be] {\n          color: #363e48;\n          font-size: 16px;\n          text-transform: uppercase;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .proj-price[data-v-4ace62be] {\n          font-size: 22px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .rating-star[data-v-4ace62be] {\n          font-family: \"FontAwesome\";\n          font-size: 16px;\n          color: #ffcc00;\n}\n.custom-container .dashboard-page .white-box hr.seprator[data-v-4ace62be] {\n        border-color: #dadada;\n        float: left;\n        position: relative;\n        width: 100%;\n        margin: 15px;\n}\n.custom-container .dashboard-page .white-box.proj-summary[data-v-4ace62be] {\n      padding: 30px 10px;\n      max-height: 115px;\n      min-height: 115px;\n}\n.custom-container .dashboard-page .white-box.ratings-wrap[data-v-4ace62be] {\n      padding: 15px 10px;\n}\n.custom-container .dashboard-page .white-box.ratings-wrap .proj-count[data-v-4ace62be] {\n        line-height: 20px;\n}\n.custom-container .dashboard-page .white-box.ratings-wrap .ratings-block[data-v-4ace62be] {\n        font-family: \"FontAwesome\";\n        font-size: 14px;\n        color: #2cac3d;\n}\n.custom-container .dashboard-page .white-box.ratings-wrap .ratings-block .rating-star[data-v-4ace62be] {\n          display: block;\n}\n.custom-container .dashboard-page .white-box.ratings-wrap .ratings-block .total-count[data-v-4ace62be] {\n          font-family: \"BrandonTextBold\";\n          font-size: 14px;\n          color: #606368;\n}\n.custom-container .dashboard-page .white-box.ratings-wrap .proj-status[data-v-4ace62be] {\n        line-height: 16px;\n}\n.custom-container .dashboard-page .green-step-btn[data-v-4ace62be] {\n      width: 100%;\n      padding: 7px 10px;\n      border: 2px solid #118921;\n      background-color: #2cac3d;\n      font-size: 14px;\n      font-family: \"BrandonTextMedium\";\n      letter-spacing: 0.05rem;\n      color: #fff;\n      border-radius: 20px;\n      display: block;\n      text-align: center;\n}\n.custom-container .dashboard-page .green-step-btn[data-v-4ace62be]:hover {\n        background-color: #158f25;\n}\n@media screen and (max-width: 1281px) {\n.custom-container .dashboard-page .white-box.proj-summary[data-v-4ace62be] {\n    padding: 30px 7px;\n}\n.custom-container .dashboard-page .white-box.ratings-wrap[data-v-4ace62be] {\n    padding: 15px 7px;\n}\n.custom-container .dashboard-page .green-step-btn[data-v-4ace62be] {\n    font-size: 13px;\n}\n}\n@media screen and (max-width: 1200px) {\n.custom-container .step-page-title[data-v-4ace62be] {\n    font-size: 28px;\n    margin: 0 0 20px;\n}\n.custom-container .dashboard-page .green-step-btn[data-v-4ace62be] {\n    font-size: 12px;\n    letter-spacing: 0;\n}\n.custom-container .dashboard-page .white-box .block-title[data-v-4ace62be] {\n    font-size: 18px;\n}\n.custom-container .dashboard-page .white-box .proj-count[data-v-4ace62be] {\n    font-size: 28px;\n    line-height: 24px;\n}\n.custom-container .dashboard-page .white-box .proj-status[data-v-4ace62be] {\n    font-size: 15px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .btn-wrap .badge-wrap[data-v-4ace62be] {\n    padding: 3px 15px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .btn-wrap .green-step-btn[data-v-4ace62be] {\n    font-size: 14px;\n    padding: 3px 1px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .btn-wrap .pending-amt[data-v-4ace62be] {\n    font-size: 20px;\n    line-height: 22px;\n}\n.custom-container .dashboard-page .white-box.ratings-wrap .ratings-block .total-count[data-v-4ace62be] {\n    font-size: 13px;\n}\n}\n@media screen and (max-width: 991px) {\n.custom-container .dashboard-page .white-box[data-v-4ace62be] {\n    padding: 15px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .btn-wrap .badge-wrap[data-v-4ace62be] {\n      margin-right: -15px;\n}\n}\n@media screen and (max-width: 767px) {\n.custom-container .dashboard-page .status-bar[data-v-4ace62be] {\n    font-size: 14px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .proj-name[data-v-4ace62be] {\n    line-height: 22px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .btn-wrap[data-v-4ace62be] {\n    border-left: 0;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .btn-wrap .green-step-btn[data-v-4ace62be] {\n      margin: 5px 0;\n      max-width: 150px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .btn-wrap .badge-wrap[data-v-4ace62be] {\n      max-width: 180px;\n      padding: 3px 30px;\n      margin-left: -16px;\n      border-right: 1px solid #2cac3d;\n      border-left: 0;\n      border-top-right-radius: 16px;\n      border-bottom-right-radius: 16px;\n      border-top-left-radius: 0px;\n      border-bottom-left-radius: 0px;\n      margin-top: 10px;\n      margin-bottom: 0px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .btn-wrap .pending-amt[data-v-4ace62be] {\n      font-size: 22px;\n      line-height: 26px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .xl-border-right[data-v-4ace62be] {\n    border-right: none;\n}\n}\n@media screen and (max-width: 575px) {\n.custom-container .step-page-title[data-v-4ace62be] {\n    font-size: 26px;\n}\n.custom-container .dashboard-page .green-step-btn.view-profile-btn[data-v-4ace62be] {\n    max-width: 180px;\n    margin: 0 auto 15px;\n}\n.custom-container .dashboard-page .white-box[data-v-4ace62be] {\n    line-height: 18px;\n}\n.custom-container .dashboard-page .white-box .block-title a[data-v-4ace62be] {\n      font-size: 14px;\n}\n.custom-container .dashboard-page .white-box .progress-txt[data-v-4ace62be] {\n      font-size: 14px;\n      display: block;\n      margin-top: 10px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .proj-price[data-v-4ace62be] {\n      font-size: 18px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .btn-wrap .pending-amt[data-v-4ace62be] {\n      font-size: 20px;\n      line-height: 24px;\n}\n.custom-container .dashboard-page .white-box.proj-summary[data-v-4ace62be] {\n    padding: 20px 10px;\n    max-height: 100px;\n    min-height: 100px;\n}\n.custom-container .dashboard-page .white-box.ratings-wrap[data-v-4ace62be] {\n    padding: 12px 10px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-810fff40\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/tester/test-active_project_item.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-323b1e04\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/star-rating.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "row" }, [
    _c(
      "div",
      { staticClass: "col-md-12" },
      [
        _vm._l(_vm.totalStars, function(i) {
          return _c("i", {
            key: i,
            staticClass: "starElement fa",
            class: {
              "fa-star-o": _vm.currentRating < i,
              "fa-star-half-o": _vm.currentRating + 0.5 == i,
              "fa-star": _vm.currentRating >= i,
              clickable: !_vm.disabled,
              small: _vm.small
            },
            on: {
              mouseover: function($event) {
                _vm.setRating(i)
              },
              mouseout: function($event) {
                _vm.setRating(_vm.value)
              },
              click: function($event) {
                _vm.selected(i)
              }
            }
          })
        }),
        _vm._v(" "),
        _vm.small ? _c("span", [_vm._v(_vm._s(_vm.displayRating))]) : _vm._e()
      ],
      2
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-323b1e04", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-4ace62be\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/tester/dashboard.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "bg-color" }, [
    _c("div", { staticClass: "container custom-container" }, [
      _c("div", { staticClass: "row" }, [
        _c("h1", { staticClass: "step-page-title" }, [
          _vm._v("\n        Dashboard\n      ")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "dashboard-page" }, [
          _c(
            "div",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.isApprovedTester,
                  expression: "isApprovedTester"
                }
              ],
              staticClass: "row"
            },
            [
              _vm._m(0),
              _vm._v(" "),
              _vm._m(1),
              _vm._v(" "),
              _vm._m(2),
              _vm._v(" "),
              _vm._m(3),
              _vm._v(" "),
              _vm._m(4),
              _vm._v(" "),
              _vm._m(5)
            ]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-12" }, [
              _c(
                "div",
                {
                  directives: [
                    {
                      name: "show",
                      rawName: "v-show",
                      value:
                        !_vm.isApprovedTester &&
                        !_vm.aActiveProjects.length &&
                        ["Step_1"].includes(_vm.latestStatus),
                      expression:
                        "!isApprovedTester && !aActiveProjects.length && (['Step_1']).includes(latestStatus)"
                    }
                  ],
                  staticClass: "status-bar disapprove-status"
                },
                [
                  _vm._v(
                    "\n              To be approved as a Tester, please pick project from open and Submit!\n            "
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  directives: [
                    {
                      name: "show",
                      rawName: "v-show",
                      value:
                        _vm.aActiveProjects.length &&
                        !_vm.isApprovedTester &&
                        !_vm.isApplicationSubmitted,
                      expression:
                        "aActiveProjects.length && !isApprovedTester && !isApplicationSubmitted"
                    }
                  ],
                  staticClass: "status-bar disapprove-status"
                },
                [
                  _vm._v(
                    "\n              To be approved as a Tester, please complete the active project!\n            "
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  directives: [
                    {
                      name: "show",
                      rawName: "v-show",
                      value:
                        (!_vm.isApprovedTester && _vm.isApplicationSubmitted) ||
                        ["review"].includes(_vm.latestStatus),
                      expression:
                        "!isApprovedTester && isApplicationSubmitted || (['review']).includes(latestStatus)"
                    }
                  ],
                  staticClass: "status-bar underprocess-status"
                },
                [
                  _vm._v(
                    "\n              Your test submission is being reviewed by our team. We'll let you know when you can start testing new VR apps!\n            "
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  directives: [
                    {
                      name: "show",
                      rawName: "v-show",
                      value:
                        _vm.isApprovedTester && !_vm.aActiveProjects.length,
                      expression: "isApprovedTester && !aActiveProjects.length"
                    }
                  ],
                  staticClass: "status-bar approve-status"
                },
                [
                  _vm._v(
                    "\n              Congratulations! You've been approved as a Tester and can now get paid for testing VR apps and games!\n            "
                  )
                ]
              )
            ])
          ]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "white-box" },
            [
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-12" }, [
                  _c("h4", { staticClass: "block-title" }, [
                    _vm._v("\n                Active Projects("),
                    _c("span", [
                      _vm._v(
                        " " +
                          _vm._s(
                            _vm.aActiveProjects.length
                              ? _vm.aActiveProjects.length
                              : 0
                          ) +
                          " "
                      )
                    ]),
                    _vm._v(")\n                "),
                    _c(
                      "a",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value:
                              _vm.isApprovedTester &&
                              _vm.aActiveProjects.length > 5,
                            expression:
                              "isApprovedTester && (aActiveProjects.length > 5) "
                          }
                        ],
                        staticClass: "text-right text-underline text-green",
                        attrs: { href: "#" },
                        on: { click: _vm.viewActiveprojects }
                      },
                      [_vm._v("View All")]
                    )
                  ]),
                  _vm._v(" "),
                  _c(
                    "p",
                    {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.aActiveProjects.length == 0,
                          expression: "aActiveProjects.length == 0"
                        }
                      ],
                      staticClass: "text-center open-proj-txt"
                    },
                    [_vm._v("No Active Projects Available")]
                  )
                ])
              ]),
              _vm._v(" "),
              _vm._l(_vm.fActiveProjects, function(testerProject) {
                return _c("div", { staticClass: "row" }, [
                  _c(
                    "div",
                    { staticClass: "col-12 col-sm-6" },
                    [
                      _c(
                        "router-link",
                        {
                          staticClass: "text-bold active-proj-name",
                          attrs: {
                            tag: "a",
                            to: {
                              name: "tester.project.view",
                              params: { id: testerProject.id }
                            }
                          }
                        },
                        [_vm._v(_vm._s(testerProject.name) + "  ")]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "col-12 col-sm-6 text-sm-right text-left" },
                    [
                      _c("span", { staticClass: "text-bold progress-txt" }, [
                        _vm._v(
                          "\n                Progress : " +
                            _vm._s(
                              (!_vm.isApprovedTester &&
                                _vm.isApplicationSubmitted) ||
                              (_vm.isApprovedTester &&
                                !_vm.aActiveProjects.length)
                                ? "100"
                                : "0"
                            ) +
                            "%\n              "
                        )
                      ]),
                      _vm._v(" "),
                      (!_vm.isApprovedTester && !_vm.aActiveProjects.length) ||
                      (_vm.aActiveProjects.length &&
                        !_vm.isApprovedTester &&
                        !_vm.isApplicationSubmitted)
                        ? _c("img", {
                            staticClass: "img-fluid",
                            attrs: { src: __webpack_require__("./resources/assets/assets/img/progress-bar.png") }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      (!_vm.isApprovedTester && _vm.isApplicationSubmitted) ||
                      (_vm.isApprovedTester && !_vm.aActiveProjects.length)
                        ? _c("img", {
                            staticClass: "img-fluid",
                            attrs: {
                              src: __webpack_require__("./resources/assets/assets/img/green-progress-full.png")
                            }
                          })
                        : _vm._e()
                    ]
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-12 col-sm-6 col-xl-3" }, [
                    _c(
                      "p",
                      { staticClass: "mb-1 mt-2 mt-sm-0" },
                      [
                        _c("span", { staticClass: "text-bold" }, [
                          _vm._v("Device :")
                        ]),
                        _vm._v(" "),
                        _vm._l(testerProject.devices, function(device) {
                          return _c("span", { staticClass: "device-list" }, [
                            _c("img", {
                              directives: [
                                {
                                  name: "b-tooltip",
                                  rawName: "v-b-tooltip.hover",
                                  modifiers: { hover: true }
                                }
                              ],
                              staticClass: "image-responsive device-image",
                              attrs: { title: device.name, src: device.icon }
                            })
                          ])
                        })
                      ],
                      2
                    ),
                    _vm._v(" "),
                    _c("p", { staticClass: "mb-1 mt-2 mt-sm-0" }, [
                      _c("span", { staticClass: "text-bold" }, [
                        _vm._v("Type :")
                      ]),
                      _vm._v(" "),
                      _c("span", {}, [
                        _vm._v(
                          _vm._s(
                            testerProject.project_type
                              ? testerProject.project_type.name
                              : ""
                          )
                        )
                      ])
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-12 col-sm-6 col-xl-3" }),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "col-12 col-xl-6 text-xl-right text-left" },
                    [
                      _c("p", { staticClass: "mb-1 mt-2 mt-sm-0" }),
                      _vm._v(" "),
                      _c("p", { staticClass: "mb-1 mt-2 mt-sm-0" }, [
                        _c("span", { staticClass: "text-bold" }, [
                          _vm._v("Status: ")
                        ]),
                        _vm._v(" "),
                        _c("span", {}, [
                          _vm._v(
                            _vm._s(
                              _vm._f("firstUppercase")(
                                testerProject.status.name
                              )
                            ) +
                              " " +
                              _vm._s(
                                _vm._f("datetime")(
                                  testerProject.status.updated_at
                                )
                              )
                          )
                        ])
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c("hr", { staticClass: "seprator" })
                ])
              })
            ],
            2
          ),
          _vm._v(" "),
          _c("div", { staticClass: "white-box" }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-12" }, [
                _c("h4", { staticClass: "block-title" }, [
                  _vm._v("\n                Open Projects ("),
                  _c("span", [
                    _vm._v(
                      "  " +
                        _vm._s(
                          _vm.aOpenProjects.length
                            ? _vm.aOpenProjects.length
                            : 0
                        ) +
                        " "
                    )
                  ]),
                  _vm._v(")\n                "),
                  _c(
                    "a",
                    {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.aOpenProjects.length > 5,
                          expression: "aOpenProjects.length > 5"
                        }
                      ],
                      staticClass: "text-right text-underline text-green",
                      attrs: { href: "#" },
                      on: { click: _vm.viewOpenprojects }
                    },
                    [_vm._v("View All")]
                  )
                ]),
                _vm._v(" "),
                _c(
                  "p",
                  {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: _vm.aOpenProjects.length == 0,
                        expression: "aOpenProjects.length == 0"
                      }
                    ],
                    staticClass: "text-center open-proj-txt"
                  },
                  [_vm._v("No Open Projects Available")]
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c(
                "div",
                { staticClass: "col-12" },
                _vm._l(_vm.fOpenProjects, function(project) {
                  return _c("div", { staticClass: "proj-desc-block" }, [
                    _c("div", { staticClass: "row align-items-center" }, [
                      _c(
                        "div",
                        {
                          staticClass:
                            "col-12 col-md-9 col-xl-10 xl-border-right"
                        },
                        [
                          _c("div", { staticClass: "row" }, [
                            _c(
                              "div",
                              { staticClass: "col-12" },
                              [
                                _c(
                                  "router-link",
                                  {
                                    staticClass: "text-bold active-proj-name",
                                    attrs: {
                                      tag: "a",
                                      to: {
                                        name: "tester.project.view",
                                        params: { id: project.id }
                                      }
                                    }
                                  },
                                  [_vm._v(_vm._s(project.name) + " ")]
                                )
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-lg-6 col-md-4" }, [
                              _c(
                                "p",
                                [
                                  _c("span", { staticClass: "text-bold" }, [
                                    _vm._v("Device :")
                                  ]),
                                  _vm._v(" "),
                                  _vm._l(project.devices, function(device) {
                                    return _c(
                                      "span",
                                      { staticClass: "device-list" },
                                      [
                                        _c("img", {
                                          directives: [
                                            {
                                              name: "b-tooltip",
                                              rawName: "v-b-tooltip.hover",
                                              modifiers: { hover: true }
                                            }
                                          ],
                                          staticClass:
                                            "image-responsive device-image",
                                          attrs: {
                                            title: device.name,
                                            src: device.icon
                                          }
                                        })
                                      ]
                                    )
                                  })
                                ],
                                2
                              ),
                              _vm._v(" "),
                              _c("p", [
                                _c("span", { staticClass: "text-bold" }, [
                                  _vm._v("Type :")
                                ]),
                                _vm._v(" "),
                                _c("span", {}, [
                                  _vm._v(
                                    _vm._s(
                                      project.project_type
                                        ? project.project_type.name
                                        : ""
                                    )
                                  )
                                ])
                              ]),
                              _vm._v(" "),
                              _vm.isApprovedTester
                                ? _c("p", [
                                    _c("span", { staticClass: "text-bold" }, [
                                      _vm._v(
                                        "Estimated Tester Time needed to complete:"
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("span", {}, [
                                      _vm._v(
                                        _vm._s(
                                          project.estimate_tester_time +
                                            " hours"
                                        )
                                      )
                                    ])
                                  ])
                                : _vm._e()
                            ]),
                            _vm._v(" "),
                            _vm.isApprovedTester
                              ? _c(
                                  "div",
                                  { staticClass: "col-lg-3 col-md-4" },
                                  [
                                    _c("p", [
                                      _c("span", { staticClass: "text-bold" }, [
                                        _vm._v("Total open spot:")
                                      ]),
                                      _vm._v(" "),
                                      _c("span", [
                                        _vm._v(
                                          _vm._s(
                                            project.allocated_tester -
                                              project.testers.length
                                          )
                                        )
                                      ])
                                    ])
                                  ]
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.isApprovedTester
                              ? _c(
                                  "div",
                                  {
                                    staticClass: "col-lg-3 col-md-4 text-right"
                                  },
                                  [
                                    _c("p", [
                                      _c("span", { staticClass: "text-bold" }, [
                                        _vm._v("Amount :")
                                      ]),
                                      _vm._v(" "),
                                      _c("span", [
                                        _vm._v(
                                          _vm._s(
                                            _vm._f("currency")(
                                              project.cost_per_tester
                                            )
                                          )
                                        )
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c("p", [
                                      _c("span", { staticClass: "text-bold" }, [
                                        _vm._v("Start Date :")
                                      ]),
                                      _vm._v(" "),
                                      _c("span", [
                                        _vm._v(
                                          _vm._s(
                                            _vm._f("date")(project.start_date)
                                          )
                                        )
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c("p", [
                                      _c("span", { staticClass: "text-bold" }, [
                                        _vm._v("End Date :")
                                      ]),
                                      _vm._v(" "),
                                      _c("span", [
                                        _vm._v(
                                          _vm._s(
                                            _vm._f("date")(project.end_date)
                                          )
                                        )
                                      ])
                                    ])
                                  ]
                                )
                              : _vm._e()
                          ])
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass:
                            "col-6 col-sm-4 col-md-3 col-xl-2 btn-wrap"
                        },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "green-step-btn",
                              attrs: {
                                tag: "a",
                                to: {
                                  name: "tester.project.view",
                                  params: { id: project.id }
                                }
                              }
                            },
                            [
                              _vm._v(
                                "\n                        Accept Project\n                      "
                              )
                            ]
                          )
                        ],
                        1
                      )
                    ])
                  ])
                })
              )
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "white-box" }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-12" }, [
                _c("h4", { staticClass: "block-title" }, [
                  _vm._v("\n                Completed Projects ("),
                  _c("span", [
                    _vm._v(
                      " " +
                        _vm._s(
                          _vm.aCompletedProjects.length
                            ? _vm.aCompletedProjects.length
                            : 0
                        ) +
                        " "
                    )
                  ]),
                  _vm._v(")\n                "),
                  _c(
                    "a",
                    {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.aCompletedProjects.length > 5,
                          expression: "aCompletedProjects.length > 5"
                        }
                      ],
                      staticClass: "text-right text-underline text-green",
                      attrs: { href: "#" },
                      on: { click: _vm.viewCompletedProjects }
                    },
                    [_vm._v("View All")]
                  )
                ]),
                _vm._v(" "),
                _c(
                  "p",
                  {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: _vm.aCompletedProjects.length == 0,
                        expression: "aCompletedProjects.length == 0"
                      }
                    ],
                    staticClass: "text-center open-proj-txt"
                  },
                  [_vm._v("No Completed Projects Available")]
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c(
                "div",
                { staticClass: "col-12" },
                _vm._l(_vm.fCompletedProjects, function(project) {
                  return _c("div", { staticClass: "proj-desc-block" }, [
                    _c("div", { staticClass: "row align-items-center" }, [
                      _c(
                        "div",
                        {
                          staticClass:
                            "col-12 col-md-9 col-xl-10 xl-border-right"
                        },
                        [
                          _c("div", { staticClass: "row" }, [
                            _c(
                              "div",
                              { staticClass: "col-10 col-xl-11" },
                              [
                                _c(
                                  "router-link",
                                  {
                                    staticClass: "text-bold proj-name",
                                    attrs: {
                                      tag: "a",
                                      to: {
                                        name: "tester.project.view",
                                        params: { id: project.id }
                                      }
                                    }
                                  },
                                  [_vm._v(_vm._s(project.name) + " ")]
                                )
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _vm._m(6, true),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-12 col-sm-6 col-xl-7" },
                              [
                                _c(
                                  "p",
                                  [
                                    _c("span", { staticClass: "text-bold" }, [
                                      _vm._v("Device :")
                                    ]),
                                    _vm._v(" "),
                                    _vm._l(project.devices, function(device) {
                                      return _c(
                                        "span",
                                        {
                                          key: device.id,
                                          staticClass: "device-list"
                                        },
                                        [
                                          _c("img", {
                                            directives: [
                                              {
                                                name: "b-tooltip",
                                                rawName: "v-b-tooltip.hover",
                                                modifiers: { hover: true }
                                              }
                                            ],
                                            staticClass:
                                              "image-responsive device-image",
                                            attrs: {
                                              title: device.name,
                                              src: device.icon
                                            }
                                          })
                                        ]
                                      )
                                    })
                                  ],
                                  2
                                ),
                                _vm._v(" "),
                                _c("p", [
                                  _c("span", { staticClass: "text-bold" }, [
                                    _vm._v("Type :")
                                  ]),
                                  _vm._v(" "),
                                  _c("span", {}, [
                                    _vm._v(
                                      _vm._s(
                                        project.project_type
                                          ? project.project_type.name
                                          : ""
                                      )
                                    )
                                  ])
                                ])
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass:
                                  "col-12 col-xl-5 pl-xl-0 text-xl-right text-left"
                              },
                              [
                                _c("span", { staticClass: "text-bold" }, [
                                  _vm._v("Completed on : ")
                                ]),
                                _vm._v(" "),
                                _c("span", [
                                  _vm._v(
                                    _vm._s(_vm._f("date")(project.created_at))
                                  )
                                ]),
                                _vm._v(" "),
                                _c("star-rating", {
                                  attrs: { disabled: true, small: true },
                                  model: {
                                    value: _vm.rating,
                                    callback: function($$v) {
                                      _vm.rating = $$v
                                    },
                                    expression: "rating"
                                  }
                                })
                              ],
                              1
                            )
                          ])
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass:
                            "col-6 col-sm-4 col-md-3 col-xl-2 btn-wrap"
                        },
                        [
                          _c("p", { staticClass: "badge-wrap" }, [
                            _c(
                              "span",
                              {
                                staticClass:
                                  "text-bold text-green complete-badge"
                              },
                              [
                                _vm._v(
                                  "\n                         " +
                                    _vm._s(
                                      project.status.name == "Test Passed"
                                        ? "Approved"
                                        : project.status.name
                                    ) +
                                    "\n                      "
                                )
                              ]
                            )
                          ]),
                          _vm._v(" "),
                          _c(
                            "p",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: !project.is_test_project,
                                  expression: "!project.is_test_project"
                                }
                              ],
                              staticClass: "pending-amt"
                            },
                            [
                              _c("br"),
                              _vm._v(
                                "\n                      " +
                                  _vm._s(
                                    project.amount ? "$" + project.amount : ""
                                  ) +
                                  "\n                    "
                              )
                            ]
                          )
                        ]
                      )
                    ])
                  ])
                })
              )
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "col-12 col-sm-4 col-lg-4 col-xl-2 text-center" },
      [
        _c("div", { staticClass: "white-box proj-summary" }, [
          _c("h3", { staticClass: "text-green proj-count" }, [_vm._v("0")]),
          _vm._v(" "),
          _c("p", { staticClass: "proj-status" }, [_vm._v("Open Projects")])
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "col-12 col-sm-4 col-lg-4 col-xl-2 text-center" },
      [
        _c("div", { staticClass: "white-box proj-summary" }, [
          _c("h3", { staticClass: "text-green proj-count" }, [_vm._v("1")]),
          _vm._v(" "),
          _c("p", { staticClass: "proj-status" }, [_vm._v("Active Projects")])
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "col-12 col-sm-4 col-lg-4 col-xl-2 text-center" },
      [
        _c("div", { staticClass: "white-box proj-summary" }, [
          _c("h3", { staticClass: "text-green proj-count" }, [_vm._v("0")]),
          _vm._v(" "),
          _c("p", { staticClass: "proj-status" }, [_vm._v("Inbox")])
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "col-12 col-sm-4 col-lg-4 col-xl-2 text-center" },
      [
        _c("div", { staticClass: "white-box proj-summary" }, [
          _c("h3", { staticClass: "text-green proj-count" }, [_vm._v("3")]),
          _vm._v(" "),
          _c("p", { staticClass: "proj-status" }, [_vm._v("My Devices")])
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "col-12 col-sm-4 col-lg-4 col-xl-2 text-center" },
      [
        _c("div", { staticClass: "white-box proj-summary ratings-wrap" }, [
          _c("div", { staticClass: "ratings-block" }, [
            _c("h3", { staticClass: "text-green proj-count" }, [_vm._v("4.0")]),
            _vm._v(" "),
            _c("span", { staticClass: "rating-star" }, [
              _c("i", { staticClass: "fa fa-star" }),
              _vm._v(" "),
              _c("i", { staticClass: "fa fa-star" }),
              _vm._v(" "),
              _c("i", { staticClass: "fa fa-star" }),
              _vm._v(" "),
              _c("i", { staticClass: "fa fa-star" }),
              _vm._v(" "),
              _c("i", { staticClass: "fa fa-star-o" })
            ]),
            _vm._v(" "),
            _c("span", { staticClass: "total-count" }, [
              _vm._v("Total Reviews 14")
            ])
          ]),
          _vm._v(" "),
          _c("p", { staticClass: "proj-status" }, [_vm._v("My Ratings")])
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "col-12 col-sm-4 col-lg-4 col-xl-2 text-center" },
      [
        _c("div", { staticClass: "white-box proj-summary" }, [
          _c("h3", { staticClass: "text-green proj-count" }, [_vm._v("$0")]),
          _vm._v(" "),
          _c("p", { staticClass: "proj-status" }, [_vm._v("Your earning")])
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-2 col-xl-1" }, [
      _c("span", { staticClass: "text-green text-bold proj-price" })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-4ace62be", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-810fff40\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/tester/test-active_project_item.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "row align-items-center" }, [
    _c("div", { staticClass: "col-12 col-md-9 col-xl-10 xl-border-right" }, [
      _c("div", { staticClass: "row" }, [
        _c(
          "div",
          { staticClass: "col-9 col-sm-10 col-xl-11" },
          [
            _c(
              "router-link",
              {
                staticClass: "text-bold active-proj-name",
                attrs: {
                  tag: "a",
                  to: {
                    name: "tester.project.view",
                    params: { id: _vm.testerProject.id }
                  }
                }
              },
              [_vm._v(_vm._s(_vm.testerProject.name) + " ")]
            )
          ],
          1
        ),
        _vm._v(" "),
        _c("div", { staticClass: "col-3 col-sm-2 col-xl-1 text-right" }, [
          _c("span", { staticClass: "text-green text-bold proj-price" }, [
            _vm._v(
              _vm._s(
                _vm.testerProject.amount ? "$" + _vm.testerProject.amount : ""
              )
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-12 col-sm-6 col-md-4 col-xl-3" }, [
          _c(
            "p",
            [
              _c("span", { staticClass: "text-bold" }, [_vm._v("Device :")]),
              _vm._v(" "),
              _vm._l(_vm.testerProject.devices, function(device) {
                return _c("span", { staticClass: "device-list" }, [
                  _c("img", {
                    directives: [
                      {
                        name: "b-tooltip",
                        rawName: "v-b-tooltip.hover",
                        modifiers: { hover: true }
                      }
                    ],
                    staticClass: "image-responsive device-image",
                    attrs: { title: device.name, src: device.icon }
                  })
                ])
              })
            ],
            2
          ),
          _vm._v(" "),
          _c("p", [
            _c("span", { staticClass: "text-bold" }, [_vm._v("Type :")]),
            _vm._v(" "),
            _c("span", {}, [
              _vm._v(
                _vm._s(
                  _vm.testerProject.project_type
                    ? _vm.testerProject.project_type.name
                    : ""
                )
              )
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "col-6 col-sm-4 col-md-3 col-xl-2 btn-wrap" },
      [
        _c(
          "router-link",
          {
            staticClass: "green-step-btn",
            attrs: {
              tag: "a",
              to: {
                name: "tester.project.view",
                params: { id: _vm.testerProject.id }
              }
            }
          },
          [_vm._v("\n      Accept Project\n    ")]
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-810fff40", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-323b1e04\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/star-rating.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-323b1e04\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/star-rating.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("456adb75", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-323b1e04\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./star-rating.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-323b1e04\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./star-rating.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4ace62be\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/dashboard.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4ace62be\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/dashboard.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("97aa4a0e", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4ace62be\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./dashboard.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4ace62be\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./dashboard.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-810fff40\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/tester/test-active_project_item.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-810fff40\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/tester/test-active_project_item.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("37ad7030", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-810fff40\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./test-active_project_item.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-810fff40\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./test-active_project_item.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/assets/img/green-progress-full.png":
/***/ (function(module, exports) {

module.exports = "/images/green-progress-full.png?135f0c23cd693b9fd3631d389f69549b";

/***/ }),

/***/ "./resources/assets/assets/img/progress-bar.png":
/***/ (function(module, exports) {

module.exports = "/images/progress-bar.png?9c04687ac2c36d110989faf38f2c44b0";

/***/ }),

/***/ "./resources/assets/components/components/star-rating.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-323b1e04\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/star-rating.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/star-rating.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-323b1e04\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/star-rating.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-323b1e04"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\star-rating.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-323b1e04", Component.options)
  } else {
    hotAPI.reload("data-v-323b1e04", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/components/tester/test-active_project_item.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-810fff40\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/tester/test-active_project_item.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/tester/test-active_project_item.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-810fff40\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/tester/test-active_project_item.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-810fff40"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\tester\\test-active_project_item.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-810fff40", Component.options)
  } else {
    hotAPI.reload("data-v-810fff40", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/pages/tester/dashboard.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4ace62be\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/dashboard.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/tester/dashboard.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-4ace62be\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/tester/dashboard.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-4ace62be"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\tester\\dashboard.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4ace62be", Component.options)
  } else {
    hotAPI.reload("data-v-4ace62be", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});