webpackJsonp([83],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/device-selector.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "DeviceSelector",
  props: {
    disabled: {
      type: Boolean,
      defualt: function defualt() {
        return false;
      }
    },
    value: {
      type: Array,
      required: true,
      default: function _default() {
        return [];
      }
    }
  },
  data: function data() {
    return {
      devices: []
    };
  },

  methods: {
    toggleItem: function toggleItem(device) {
      if (this.disabled) {
        return true;
      }
      var index = this.value.indexOf(device.id);
      var newDevices = [];
      if (index >= 0) {
        newDevices = this.value.filter(function (i) {
          return i !== device.id;
        });
      } else {
        newDevices = [].concat(_toConsumableArray(this.value), [device.id]);
      }
      this.$emit("input", newDevices);
      this.$emit("change", newDevices);
    }
  },
  mounted: function mounted() {
    var _this = this;

    if (!this.$store.state.devices.length) {
      axios.get("/api/device").then(function (_ref) {
        var data = _ref.data;

        _this.$store.commit("set_devices", data);
        _this.devices = data;
      });
    } else {
      this.devices = this.$store.state.devices;
    }
  },
  computed: {
    availableDevices: function availableDevices() {
      var _this2 = this;

      if (this.disabled) {
        return this.devices.filter(function (_ref2) {
          var id = _ref2.id;
          return _this2.value.indexOf(id) >= 0;
        });
      }
      return this.devices;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/publisher/project_item.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_components_components_device_selector__ = __webpack_require__("./resources/assets/components/components/device-selector.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_components_components_device_selector___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_components_components_device_selector__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_src_services_project__ = __webpack_require__("./resources/assets/services/project.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "ProjectItem",
  components: {
    DeviceSelector: __WEBPACK_IMPORTED_MODULE_0_components_components_device_selector___default.a
  },
  props: {
    project: {
      type: Object,
      required: true
    }
  },
  data: function data() {
    return {
      originalUser: window.USER
    };
  },

  computed: {
    selected_devices: function selected_devices() {
      return (this.project.devices || []).map(function (_ref) {
        var id = _ref.id;
        return id;
      });
    },
    project_approved: function project_approved() {
      return Object(__WEBPACK_IMPORTED_MODULE_1_src_services_project__["a" /* isProjectApproved */])(this.project.status.name);
    }
  },
  methods: {
    onDelete: function onDelete(id) {
      this.$emit("delete", id);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/publisher/project_list.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_components_components_publisher_project_item__ = __webpack_require__("./resources/assets/components/components/publisher/project_item.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_components_components_publisher_project_item___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_components_components_publisher_project_item__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "projects_list",
  components: {
    ProjectItem: __WEBPACK_IMPORTED_MODULE_0_components_components_publisher_project_item___default.a
  },
  data: function data() {
    return {
      aPendingApprovalProjects: [],
      aRunningProjects: [],
      aPendingPaymentProjects: [],
      aCompletedProjects: [],
      formstate: {},
      model: {},
      originalUser: {},
      aDevices: {},
      additional_devices: [],
      project_types: {},
      running_project: ""
    };
  },

  mounted: function mounted() {
    var _this = this;

    this.loadProjects();
    axios.get("/api/devices").then(function (response) {
      _this.aDevices = response.data;
    }).then(function (_ref) {
      var data = _ref.data.data;
      return _this.assignData(data);
    }).catch(function (error) {});
    axios.get("/api/tester/" + this.$store.state.user.id).then(function (_ref2) {
      var data = _ref2.data.data;

      _this.model = data;
    }).catch(function (error) {});
    axios.get("/api/project-types").then(function (response) {
      _this.project_types = response.data;
    }).catch(function (error) {});
  },
  methods: {
    loadProjects: function loadProjects() {
      var _this2 = this;

      axios.get("/api/publisher-projects?publisher_id=" + this.$store.state.user.id).then(function (response) {
        var data = response.data;
        _this2.aPendingApprovalProjects = data.aPendingApprovalProjects ? data.aPendingApprovalProjects : [];
        _this2.aRunningProjects = data.aRunningProjects ? data.aRunningProjects : [];
        _this2.aPendingPaymentProjects = data.aPendingPaymentProjects ? data.aPendingPaymentProjects : [];
        _this2.aCompletedProjects = data.aCompletedProjects ? data.aCompletedProjects : [];
      }).catch(function (error) {});
    },
    assignData: function assignData(data) {
      this.model = data;
      if (this.model.aDevices.id == null) {
        this.model.aDevices_id = 0; //
      }
    },
    deleteProject: function deleteProject(id) {
      var _this3 = this;

      this.$swal({
        title: "Are you sure?",
        text: "You want to delete this project?",
        icon: "warning",
        buttons: true,
        dangerMode: true
      }).then(function (willDelete) {
        if (willDelete) {
          return axios.delete("/api/projects/" + id + "?test=0").then(function () {
            return _this3.loadProjects();
          }).catch(function (e) {
            return _this3.errors = e.response.message;
          });
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/device-selector.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\nimg[data-v-0d653902] {\n  height: 50px;\n  width: 50px;\n  display: inline-block;\n  margin-right: 10px;\n  float: left;\n  opacity: 0.5;\n  cursor: pointer;\n}\nimg.disabled[data-v-0d653902] {\n  cursor: auto;\n}\nimg.selected[data-v-0d653902] {\n  opacity: 1;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-62b1dace\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/publisher/project_list.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-62b1dace\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=1!./resources/assets/components/pages/publisher/project_list.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.custom-container[data-v-62b1dace] {\n  max-width: 100%;\n  padding: 0;\n}\n.custom-container .text-blue[data-v-62b1dace] {\n    color: #0082cc;\n}\n.custom-container .text-red[data-v-62b1dace] {\n    color: #e63423;\n}\n.custom-container .text-green[data-v-62b1dace] {\n    color: #2cac3d;\n}\n.custom-container .text-bold[data-v-62b1dace] {\n    font-family: \"BrandonTextBold\";\n}\n.custom-container .page-title[data-v-62b1dace] {\n    font-family: \"UniNeueBold\";\n    font-size: 30px;\n    width: 100%;\n    text-align: center;\n    color: #363e48;\n    margin: 5px 0 25px;\n}\n.custom-container .blue-btn[data-v-62b1dace] {\n    width: 100%;\n    padding: 2px 4px;\n    border: 2px solid #0082cc;\n    background-color: #00aff5;\n    font-size: 14px;\n    font-family: \"BrandonTextMedium\";\n    letter-spacing: 0.05rem;\n    color: #fff;\n    border-radius: 20px;\n    display: block;\n    text-align: center;\n}\n.custom-container .blue-btn[data-v-62b1dace]:hover {\n      background-color: #13b9fb;\n}\n.custom-container .new-proj-btn[data-v-62b1dace] {\n    margin-top: 15px;\n}\n.custom-container .white-box[data-v-62b1dace] {\n    background-color: #fff;\n    -webkit-box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n            box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n    border-radius: 4px;\n    padding: 20px;\n    margin-bottom: 20px;\n    font-size: 17px;\n    color: #606368;\n}\n@media screen and (max-width: 991px) {\n.custom-container[data-v-62b1dace] {\n    max-width: 100%;\n    padding: 0px;\n}\n.custom-container .white-box[data-v-62b1dace] {\n      padding: 15px;\n}\n}\n@media screen and (max-width: 767px) {\n.custom-container .page-title[data-v-62b1dace] {\n    font-size: 24px;\n    margin: 0 0 20px;\n}\n.custom-container .new-proj-btn[data-v-62b1dace] {\n    margin-top: 5px;\n    padding: 2px;\n}\n}\n@media screen and (max-width: 575px) {\n.custom-container .new-proj-btn[data-v-62b1dace] {\n    margin: 0 auto 15px;\n    max-width: 150px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-630d69fd\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/project_item.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.text-blue[data-v-630d69fd] {\n  color: #0082cc;\n}\n.text-red[data-v-630d69fd] {\n  color: #e63423;\n}\n.text-green[data-v-630d69fd] {\n  color: #2cac3d;\n}\n.text-bold[data-v-630d69fd] {\n  font-family: \"BrandonTextBold\";\n}\n.text-medium[data-v-630d69fd] {\n  font-family: \"BrandonTextMedium\";\n}\n.blue-btn[data-v-630d69fd] {\n  width: 100%;\n  padding: 2px 4px;\n  border: 2px solid #0082cc;\n  background-color: #00aff5;\n  font-size: 14px;\n  font-family: \"BrandonTextMedium\";\n  letter-spacing: 0.05rem;\n  color: #fff;\n  border-radius: 20px;\n  display: block;\n  text-align: center;\n}\n.blue-btn[data-v-630d69fd]:hover {\n    background-color: #13b9fb;\n}\n.proj-block-wrap[data-v-630d69fd] {\n  padding: 15px 0;\n  font-family: \"BrandonTextRegular\";\n  font-size: 14px;\n  color: #606368;\n  position: relative;\n  border-bottom: 1px solid #dadada;\n}\n.proj-block-wrap span[data-v-630d69fd] {\n    vertical-align: top;\n}\n.proj-block-wrap .border-xl-right[data-v-630d69fd] {\n    border-right: 1px solid #dadada;\n}\n.proj-block-wrap .proj-title[data-v-630d69fd] {\n    font-family: \"BrandonTextBold\";\n    font-size: 16px;\n    color: #363e48;\n}\n@media screen and (max-width: 1200px) {\n.blue-btn[data-v-630d69fd] {\n    float: right;\n    max-width: 150px;\n}\n.proj-block-wrap .border-xl-right[data-v-630d69fd] {\n    border-right: none;\n}\n}\n@media screen and (max-width: 991px) {\n.blue-btn[data-v-630d69fd] {\n    float: left;\n    margin-top: 10px;\n}\n}\n@media screen and (max-width: 767px) {\n.proj-block-wrap[data-v-630d69fd] {\n    padding: 10px 0 15px;\n}\n.proj-block-wrap .proj-title[data-v-630d69fd] {\n      font-size: 20px;\n}\n}\n@media screen and (max-width: 575px) {\n.proj-block-wrap .proj-title[data-v-630d69fd] {\n    font-size: 18px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-0d653902\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/device-selector.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    _vm._l(_vm.availableDevices, function(device) {
      return _c("span", { staticClass: "d-inline-block pr-3" }, [
        _c("img", {
          staticClass: "img-fluid",
          class: {
            selected: _vm.value.indexOf(device.id) !== -1,
            disabled: _vm.disabled
          },
          attrs: { title: device.name, src: device.icon },
          on: {
            click: function($event) {
              _vm.toggleItem(device)
            }
          }
        })
      ])
    })
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0d653902", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-62b1dace\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/publisher/project_list.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "container custom-container" }, [
      _c("div", { staticClass: "row" }, [
        _vm._m(0),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "col-xl-2 col-md-3 col-sm-3" },
          [
            _c(
              "router-link",
              {
                staticClass: "blue-btn new-proj-btn",
                attrs: { to: { name: "publisher.project.add" } }
              },
              [_vm._v("Add New Project")]
            )
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-md-12" }, [
          _c("div", { ref: "testprojectList", staticClass: "white-box" }, [
            _c(
              "div",
              { staticClass: "publisher-project-tabwrap" },
              [
                _c(
                  "vue-tabs",
                  { attrs: { type: "pills" } },
                  [
                    _c(
                      "v-tab",
                      {
                        attrs: {
                          title:
                            "Running ( " + _vm.aRunningProjects.length + " )"
                        }
                      },
                      _vm._l(_vm.aRunningProjects, function(testproject) {
                        return _c("project-item", {
                          key: testproject.id,
                          attrs: { project: testproject },
                          on: { delete: _vm.deleteProject }
                        })
                      })
                    ),
                    _vm._v(" "),
                    _c(
                      "v-tab",
                      {
                        attrs: {
                          title:
                            "Waiting for Approval ( " +
                            _vm.aPendingApprovalProjects.length +
                            " )"
                        }
                      },
                      _vm._l(_vm.aPendingApprovalProjects, function(
                        testproject
                      ) {
                        return _c("project-item", {
                          key: testproject.id,
                          attrs: { project: testproject },
                          on: { delete: _vm.deleteProject }
                        })
                      })
                    ),
                    _vm._v(" "),
                    _c(
                      "v-tab",
                      {
                        attrs: {
                          title:
                            "Pending Payment ( " +
                            _vm.aPendingPaymentProjects.length +
                            " )"
                        }
                      },
                      _vm._l(_vm.aPendingPaymentProjects, function(
                        testproject
                      ) {
                        return _c("project-item", {
                          key: testproject.id,
                          attrs: { project: testproject },
                          on: { delete: _vm.deleteProject }
                        })
                      })
                    ),
                    _vm._v(" "),
                    _c(
                      "v-tab",
                      {
                        attrs: {
                          title:
                            "Completed ( " +
                            _vm.aCompletedProjects.length +
                            " )"
                        }
                      },
                      _vm._l(_vm.aCompletedProjects, function(testproject) {
                        return _c("project-item", {
                          key: testproject.id,
                          attrs: { project: testproject },
                          on: { delete: _vm.deleteProject }
                        })
                      })
                    )
                  ],
                  1
                )
              ],
              1
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "col-xl-10 col-md-9 col-sm-9 text-center" },
      [
        _c("h1", { staticClass: "page-title" }, [
          _vm._v("\n          My Projects\n        ")
        ])
      ]
    )
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-62b1dace", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-630d69fd\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/publisher/project_item.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "proj-block-wrap" }, [
    _c("div", { staticClass: "row align-items-center" }, [
      _c("div", { staticClass: "col-md-12 col-xl-10 border-xl-right" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12 mb-2" }, [
            _c("span", { staticClass: "proj-title text-uppercase" }, [
              _vm._v(_vm._s(_vm.project.name))
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-xl-4 col-lg-6 text-left" }, [
            _c("div", { staticClass: "mb-2" }, [
              _c(
                "span",
                { staticClass: "text-bold mr-1 d-block d-sm-inline-block" },
                [_vm._v("Devices : ")]
              ),
              _vm._v(" "),
              _c(
                "span",
                { staticClass: "d-block d-sm-inline-block" },
                [
                  _c("device-selector", {
                    attrs: { disabled: "" },
                    model: {
                      value: _vm.selected_devices,
                      callback: function($$v) {
                        _vm.selected_devices = $$v
                      },
                      expression: "selected_devices"
                    }
                  })
                ],
                1
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "mb-2" }, [
              _c(
                "span",
                { staticClass: "text-bold mr-1 d-block d-sm-inline-block" },
                [_vm._v("Type : ")]
              ),
              _vm._v(" "),
              _c("span", { staticClass: "d-block d-sm-inline-block" }, [
                _vm._v(_vm._s(_vm.project.project_type.name))
              ])
            ]),
            _vm._v(" "),
            _vm.project.status.name == "Running"
              ? _c("div", { staticClass: "mb-2" }, [
                  _c("span", { staticClass: "d-block d-sm-inline-block" }, [
                    _vm._v(
                      _vm._s(_vm.project.testersSubmitted.length) +
                        " Testers Submitted, "
                    )
                  ]),
                  _vm._v(" "),
                  _c("span", { staticClass: "d-block d-sm-inline-block" }, [
                    _vm._v(
                      _vm._s(
                        _vm.project.tester_needed -
                          _vm.project.testersSubmitted.length
                      ) + " Pending"
                    )
                  ])
                ])
              : _vm.project.status.name == "Pending Approval"
                ? _c("div", { staticClass: "mb-2" }, [
                    _c("span", { staticClass: "d-block d-sm-inline-block" }, [
                      _vm._v("Testers needed : ")
                    ]),
                    _vm._v(" "),
                    _c("span", { staticClass: "d-block d-sm-inline-block" }, [
                      _vm._v(_vm._s(_vm.project.tester_needed) + " ")
                    ])
                  ])
                : _vm._e()
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-xl-4 col-lg-6 text-left" }, [
            _c("div", { staticClass: "mb-2" }, [
              _c(
                "span",
                { staticClass: "text-bold mr-1 d-block d-sm-inline-block" },
                [
                  _vm._v(
                    "\n              Estimated Tester Time needed to complete :\n            "
                  )
                ]
              ),
              _vm._v(" "),
              _c("span", { staticClass: "d-block d-sm-inline-block" }, [
                _vm._v(
                  " " + _vm._s(_vm.project.estimate_tester_time) + " Hours"
                )
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-xl-4 col-lg-6 text-xl-right" }, [
            _c("div", { staticClass: "mb-2" }, [
              _c(
                "span",
                { staticClass: "text-bold mr-1 d-block d-sm-inline-block" },
                [_vm._v("Start Date : ")]
              ),
              _vm._v(" "),
              _c("span", { staticClass: "d-block d-sm-inline-block" }, [
                _vm._v(_vm._s(_vm._f("date")(_vm.project.start_date)))
              ])
            ]),
            _vm._v(" "),
            _c("div", [
              _c(
                "span",
                { staticClass: "text-bold mr-1 d-block d-sm-inline-block" },
                [_vm._v("End Date : ")]
              ),
              _vm._v(" "),
              _c("span", { staticClass: "d-block d-sm-inline-block" }, [
                _vm._v(_vm._s(_vm._f("date")(_vm.project.end_date)))
              ])
            ]),
            _vm._v(" "),
            _vm.project.status.name != "Pending Approval"
              ? _c("div", { staticClass: "mb-2" }, [
                  _c(
                    "span",
                    {
                      staticClass:
                        "text-bold mr-1 d-block d-sm-inline-block pl-xl-0"
                    },
                    [_vm._v("\n              Progress :\n            ")]
                  ),
                  _vm._v(" "),
                  _vm._m(0)
                ])
              : _vm._e(),
            _vm._v(" "),
            _c("div", { staticClass: "mb-2" }, [
              this.project.status.name == "Running"
                ? _c(
                    "span",
                    {
                      staticClass:
                        "text-bold mr-1 d-block d-sm-inline-block pl-xl-0"
                    },
                    [_vm._v("\n              Started On:\n            ")]
                  )
                : _vm._e(),
              _vm._v(" "),
              this.project.status.name == "Pending Approval"
                ? _c(
                    "span",
                    {
                      staticClass:
                        "text-bold mr-1 d-block d-sm-inline-block pl-xl-0"
                    },
                    [
                      _vm._v(
                        "\n              Request for approval sent on:\n            "
                      )
                    ]
                  )
                : _vm._e(),
              _vm._v(" "),
              _c("span", { staticClass: "mb-2 d-block d-sm-inline-block" }, [
                _vm._v(
                  "\n              " +
                    _vm._s(_vm._f("datetime")(_vm.project.created_at)) +
                    "\n            "
                )
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "col-md-12 col-xl-2 btn-wrap" },
        [
          _c(
            "router-link",
            {
              staticClass: "blue-btn",
              attrs: {
                to: {
                  name:
                    "publisher.project." +
                    (_vm.project_approved ? "invoice" : "view"),
                  params: { id: _vm.project.id }
                }
              }
            },
            [_vm._v("View Project")]
          ),
          _vm._v(" "),
          _c(
            "router-link",
            {
              staticClass: "text-center mt-5 m-5",
              attrs: {
                tag: "a",
                to: {
                  name: "publisher.project.edit",
                  params: { id: _vm.project.id }
                }
              }
            },
            [_vm._v("\n        Edit project\n      ")]
          )
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "mb-2 d-block d-sm-inline-block" }, [
      _c("span", { staticClass: "text-bold mr-2" }, [_vm._v("100%")]),
      _vm._v(" "),
      _c("img", {
        staticClass: "img-fluid",
        attrs: { src: __webpack_require__("./resources/assets/assets/img/green-progress-full.png") }
      })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-630d69fd", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/device-selector.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/device-selector.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("4b79eeec", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./device-selector.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./device-selector.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-62b1dace\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/publisher/project_list.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-62b1dace\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/publisher/project_list.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("80b5d6b6", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-62b1dace\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project_list.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-62b1dace\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project_list.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-62b1dace\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=1!./resources/assets/components/pages/publisher/project_list.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-62b1dace\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=1!./resources/assets/components/pages/publisher/project_list.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("186c712b", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-62b1dace\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=1!./project_list.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-62b1dace\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=1!./project_list.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-630d69fd\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/project_item.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-630d69fd\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/project_item.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("063fe55e", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-630d69fd\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project_item.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-630d69fd\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project_item.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/assets/img/green-progress-full.png":
/***/ (function(module, exports) {

module.exports = "/images/green-progress-full.png?135f0c23cd693b9fd3631d389f69549b";

/***/ }),

/***/ "./resources/assets/components/components/device-selector.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/device-selector.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/device-selector.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-0d653902\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/device-selector.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-0d653902"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\device-selector.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0d653902", Component.options)
  } else {
    hotAPI.reload("data-v-0d653902", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/components/publisher/project_item.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-630d69fd\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/project_item.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/publisher/project_item.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-630d69fd\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/publisher/project_item.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-630d69fd"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\publisher\\project_item.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-630d69fd", Component.options)
  } else {
    hotAPI.reload("data-v-630d69fd", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/pages/publisher/project_list.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-62b1dace\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/publisher/project_list.vue")
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-62b1dace\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=1!./resources/assets/components/pages/publisher/project_list.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/publisher/project_list.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-62b1dace\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/publisher/project_list.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-62b1dace"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\publisher\\project_list.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-62b1dace", Component.options)
  } else {
    hotAPI.reload("data-v-62b1dace", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/services/project.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = isProjectApproved;
/* harmony export (immutable) */ __webpack_exports__["c"] = isProjectRunning;
/* harmony export (immutable) */ __webpack_exports__["b"] = isProjectCompleted;
var StatusMap = {
  "pending approval": 1,
  "pending payment": 2,
  running: 3,
  completed: 4
};
function isProjectApproved(status) {
  if (!status) {
    return false;
  }
  var statusWeight = StatusMap[status.toLowerCase()];
  return statusWeight && statusWeight > 1;
}

function isProjectRunning(status) {
  if (!status) {
    return false;
  }
  var statusWeight = StatusMap[status.toLowerCase()];
  return statusWeight && statusWeight > 2;
}

function isProjectCompleted(status) {
  if (!status) {
    return false;
  }
  var statusWeight = StatusMap[status.toLowerCase()];
  return statusWeight && statusWeight > 3;
}

/***/ })

});