webpackJsonp([15],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/notification.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__("./node_modules/vue/dist/vue.common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "notification",
  props: {
    thumb_icon: {
      type: Array,
      default: function _default() {
        return [];
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/layouts/header/fixed-header.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_screenfull__ = __webpack_require__("./node_modules/screenfull/dist/screenfull.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_screenfull___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_screenfull__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_components_components_notification_vue__ = __webpack_require__("./resources/assets/components/components/notification.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_components_components_notification_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_components_components_notification_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_src_services_auth__ = __webpack_require__("./resources/assets/services/auth.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  name: "vueadmin_header",
  components: {
    notifications: __WEBPACK_IMPORTED_MODULE_1_components_components_notification_vue___default.a
  },
  data: function data() {
    return {
      userClass: 'tester-topbar',
      hasTesterRole: Object(__WEBPACK_IMPORTED_MODULE_2_src_services_auth__["b" /* hasTesterRole */])(),
      hasPublisherRole: Object(__WEBPACK_IMPORTED_MODULE_2_src_services_auth__["a" /* hasPublisherRole */])()
    };
  },

  mounted: function mounted() {
    if (Object(__WEBPACK_IMPORTED_MODULE_2_src_services_auth__["a" /* hasPublisherRole */])()) {
      this.userClass = 'publisher-topbar';
    }
  },
  methods: {
    toggle_menu: function toggle_menu() {
      this.$store.commit("left_menu", "toggle");
    },
    fullscreen: function fullscreen() {
      if (__WEBPACK_IMPORTED_MODULE_0_screenfull___default.a.enabled) {
        __WEBPACK_IMPORTED_MODULE_0_screenfull___default.a.toggle();
      }
    },
    logout: function logout() {
      Object(__WEBPACK_IMPORTED_MODULE_2_src_services_auth__["h" /* logout */])();
      var name = "/login";
      if (this.$route.path.indexOf("/admin") >= 0) {
        name = "/admin/login";
      }
      window.location = '/logout?next=/portal' + name;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/layouts/left-side/default/menu/MenuItem.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["link", "icon", "src", "activeIcon"],
  data: function data() {
    return {
      currentIcon: this.src
    };
  },

  methods: {
    onMouseEnter: function onMouseEnter() {
      this.currentIcon = this.activeIcon || this.src;
    },
    onMouseExit: function onMouseExit() {
      this.currentIcon = this.src;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/layouts/left-side/default/menu/subMenu.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_animejs__ = __webpack_require__("./node_modules/animejs/anime.min.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_animejs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_animejs__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    props: {
        selected: Boolean,
        icon: String,
        title: {
            type: String,
            required: true
        }
    },
    data: function data() {
        return {
            isActived: this.selected
        };
    },

    computed: {
        index: function index() {
            return this.$parent.$collapseItems.indexOf(this);
        }
    },
    created: function created() {
        this._isCollapseItem = true;
    },
    mounted: function mounted() {
        var _this = this;

        function activate(self) {
            if (self.$refs.box) {
                self.isActived = self.$refs.box.querySelectorAll("a.active").length >= 1 ? true : false;
            }
        }
        //change when route changes
        this.$store.subscribe(function (mutation, state) {
            if (mutation.type == "routeChange" && mutation.payload == "end") {
                setTimeout(function () {
                    activate(_this);
                }, 0);
            }
        });
        activate(this);
    },

    methods: {
        toggle: function toggle() {
            this.$parent.$emit('closeall', this.index);
            this.isActived = !this.isActived;
        },
        cancel: function cancel() {
            this.anime.pause();
        },
        before: function before(targets) {
            targets.removeAttribute('style');
        },
        enter: function enter(targets, done) {
            var height = targets.scrollHeight;
            targets.style.height = 0;
            targets.style.opacity = 0;
            __WEBPACK_IMPORTED_MODULE_0_animejs___default()({
                targets: targets,
                duration: 377,
                easing: 'easeOutExpo',
                opacity: [0, 1],
                height: height,
                complete: function complete() {
                    targets.removeAttribute('style');
                    done();
                }
            });
        },
        leave: function leave(targets, complete) {
            __WEBPACK_IMPORTED_MODULE_0_animejs___default()({
                targets: targets,
                duration: 377,
                easing: 'easeOutExpo',
                opacity: [1, 0],
                height: 0
            });
        }
    }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/layouts/left-side/default/menu/vueMenu.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    computed: {
        $collapseItems: function $collapseItems() {
            return this.$children.filter(function (child) {
                return child._isCollapseItem;
            });
        }
    },
    methods: {
        openByIndex: function openByIndex(index) {
            this.$collapseItems.forEach(function (item, i) {
                if (i !== index) {
                    item.isActived = false;
                }
            });
        }
    },
    mounted: function mounted() {
        var _this = this;

        this.$on("closeall", function (index) {
            _this.openByIndex(index);
        });
    }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/layouts/left-side/publisher/left-side.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__default_menu__ = __webpack_require__("./resources/assets/components/layouts/left-side/default/menu/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__default_menu___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__default_menu__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__publisher_menu_js__ = __webpack_require__("./resources/assets/publisher_menu.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "left-side",
  components: {
    vmenu: __WEBPACK_IMPORTED_MODULE_0__default_menu__["vmenu"],
    vsubMenu: __WEBPACK_IMPORTED_MODULE_0__default_menu__["vsubMenu"],
    vmenuItem: __WEBPACK_IMPORTED_MODULE_0__default_menu__["vmenuItem"]
  },
  data: function data() {
    return {
      menuitems: __WEBPACK_IMPORTED_MODULE_1__publisher_menu_js__["a" /* default */]
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/layouts/right-side.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "right-side",
  methods: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/publisher_layout.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_components_layouts_preloader_preloader__ = __webpack_require__("./resources/assets/components/layouts/preloader/preloader.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_components_layouts_preloader_preloader___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_components_layouts_preloader_preloader__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_components_layouts_right_side__ = __webpack_require__("./resources/assets/components/layouts/right-side.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_components_layouts_right_side___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_components_layouts_right_side__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_components_layouts_left_side_publisher_left_side__ = __webpack_require__("./resources/assets/components/layouts/left-side/publisher/left-side.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_components_layouts_left_side_publisher_left_side___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_components_layouts_left_side_publisher_left_side__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_components_layouts_header_fixed_header__ = __webpack_require__("./resources/assets/components/layouts/header/fixed-header.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_components_layouts_header_fixed_header___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_components_layouts_header_fixed_header__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_assets_sass_custom_scss__ = __webpack_require__("./resources/assets/assets/sass/custom.scss");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_assets_sass_custom_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_assets_sass_custom_scss__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_components_layouts_css_fixed_menu_scss__ = __webpack_require__("./resources/assets/components/layouts/css/fixed-menu.scss");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_components_layouts_css_fixed_menu_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_components_layouts_css_fixed_menu_scss__);
//
//
//
//
//
//
//
//
//
//
//
//

/**
 * These are the files that enable you to change layouts and other options
 */

/**
 * import preloader
 * choose from preloader and bounce
 */


/**
 * The right side content
 */


/**
 * import left-side from default or horizontal-menu
 * eg: import left_side from 'components/layouts/left-side/horizontal-menu/left-side'
 */


/**
 * import from header or fixed-header or no-header
 */


/**
 * Styles
 */

/**
 * Main stylesheet for the layout
 */


/**
 * Style required for a fixed-menu layout
 */


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "layout",
  components: {
    preloader: __WEBPACK_IMPORTED_MODULE_0_components_layouts_preloader_preloader___default.a,
    vueadmin_header: __WEBPACK_IMPORTED_MODULE_3_components_layouts_header_fixed_header___default.a,
    left_side: __WEBPACK_IMPORTED_MODULE_2_components_layouts_left_side_publisher_left_side___default.a,
    right_side: __WEBPACK_IMPORTED_MODULE_1_components_layouts_right_side___default.a
  },
  data: function data() {
    return {
      showtopbtn: false
    };
  },
  mounted: function mounted() {
    if (window.innerWidth <= 992) {
      this.$store.commit("left_menu", "close");
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./resources/assets/assets/sass/custom.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*\r\nCreated by: Jyostna Designs\r\n\r\n[TABLE OF CONTENTS]\r\n\r\n1.  RESET STYLES\r\n2.  HEADER STYLES\r\n    2.1 HEADER LEFT\r\n    2.2 HEADER RIGHT SIDE DROPDOWNS\r\n3. LEFT SIDEBAR\r\n4. MAIN WRAPPER STYLES\r\n5. LEFT MENU COLLAPSE STYLES\r\n6. CUSTOM STYLES\r\n7. MEDIA QUERIES\r\n\r\n*/\n/*===import bootstrap variables===*/\n.badge {\n  color: #fff; }\n\n/*****  CUSTOM FONTS CSS  *****/\n@font-face {\n  font-family: \"FontAwesome\";\n  src: local(\"FontAwesome\"), local(\"FontAwesome\"), url(\"/fonts/fontawesome-webfont.woff2\") format(\"woff2\"); }\n\n@font-face {\n  font-family: \"BrandonTextRegular\";\n  src: local(\"BrandonTextRegular\"), local(\"BrandonTextRegular\"), url(\"/fonts/BrandonTextRegular.woff2\") format(\"woff2\"); }\n\n@font-face {\n  font-family: \"BrandonTextBold\";\n  src: local(\"BrandonTextBold\"), local(\"BrandonTextBold\"), url(\"/fonts/BrandonTextBold.woff2\") format(\"woff2\"); }\n\n@font-face {\n  font-family: \"BrandonTextMedium\";\n  src: local(\"BrandonTextMedium\"), local(\"BrandonTextMedium\"), url(\"/fonts/BrandonTextMedium.woff2\") format(\"woff2\"); }\n\n@font-face {\n  font-family: \"BrandonTextBlack\";\n  src: local(\"BrandonTextBlack\"), local(\"BrandonTextBlack\"), url(\"/fonts/BrandonTextBlack.woff2\") format(\"woff2\"); }\n\n@font-face {\n  font-family: \"UniNeueBold\";\n  src: local(\"UniNeueBold\"), local(\"UniNeueBold\"), url(\"/fonts/UniNeueBold.woff2\") format(\"woff2\"); }\n\n@font-face {\n  font-family: \"UniNeueRegular\";\n  src: local(\"UniNeueRegular\"), local(\"UniNeueRegular\"), url(\"/fonts/UniNeueRegular.woff2\") format(\"woff2\"); }\n\n@font-face {\n  font-family: \"UniNeueHeavy\";\n  src: local(\"FontfabricUniNeueHeavy\"), local(\"FontfabricUniNeueHeavy\"), url(\"/fonts/FontfabricUniNeueHeavy.woff2\") format(\"woff2\"); }\n\n/*****  1.RESET STYLES  *****/\nhtml {\n  background: none repeat scroll 0 0 #fff;\n  overflow-x: hidden;\n  transition: all 0.25s ease-out;\n  font-size: small; }\n\nhtml,\nbutton,\ninput,\noptgroup,\nselect,\ntextarea {\n  font-family: \"BrandonTextRegular\"; }\n\nbody {\n  background: none repeat scroll 0 0 #fff;\n  overflow-x: hidden;\n  transition: all 0.25s ease-out;\n  font-size: small;\n  letter-spacing: 0.5px;\n  width: 100%;\n  margin: 0 auto !important;\n  font-family: \"BrandonTextRegular\"; }\n\nbody,\n#app,\n#app > div,\n#app div.wrapper {\n  min-height: calc(100vh - 70px); }\n\nul {\n  list-style: none; }\n\nlabel {\n  font-weight: 400;\n  font-size: 14px;\n  letter-spacing: 1px; }\n\na:hover,\na {\n  text-decoration: none !important; }\n\n.table > thead > tr > th,\n.table > thead > tr > td,\n.table > tbody > tr > th,\n.table > tbody > tr > td,\n.table > tfoot > tr > th,\n.table > tfoot > tr > td {\n  vertical-align: middle; }\n\n.input-required {\n  color: #fd7570; }\n\n/**Dropdown menus**/\n.dropdown-menu {\n  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.1);\n  z-index: 2300;\n  margin-top: 0;\n  border: none;\n  outline: none; }\n\n/*********6. CUSTOM STYLES *********/\n.submenu-content .name {\n  vertical-align: text-top !important; }\n\n/*text-area resize*/\n.resize_vertical {\n  resize: vertical; }\n\nh4.card-header {\n  font-size: 16px; }\n\n.card {\n  margin-bottom: 25px; }\n\n/**************card bg colors*******************/\n.bg-primary-card {\n  border: 1px solid #337ab7; }\n  .bg-primary-card > .card-header {\n    background-color: #337ab7;\n    color: #fff; }\n\n.bg-info-card {\n  border: 1px solid #4fc1e9; }\n  .bg-info-card > .card-header {\n    background-color: #4fc1e9;\n    color: #fff; }\n\n.bg-warning-card {\n  border: 1px solid #fea115; }\n  .bg-warning-card > .card-header {\n    background-color: #fea115;\n    color: #fff; }\n\n.bg-danger-card {\n  border: 1px solid #fd7570; }\n  .bg-danger-card > .card-header {\n    background-color: #fd7570;\n    color: #fff; }\n\n.bg-success-card {\n  border: 1px solid #16af81; }\n  .bg-success-card > .card-header {\n    background-color: #16af81;\n    color: #fff; }\n\n.bg-primary-card:hover,\n.bg-info-card:hover,\n.bg-danger-card:hover,\n.bg-warning-card:hover,\n.bg-success-card:hover,\n.card:hover {\n  box-shadow: 1px 1px 38px -8px #8e8c8e; }\n\n.btn-link,\n.btn {\n  cursor: pointer; }\n\n/*=======hide unwanted content in print======*/\n@media print {\n  header.header,\n  aside.left-aside,\n  section.content-header,\n  #scroll {\n    display: none !important; }\n  .table-responsive {\n    display: inline-table;\n    width: 100%; } }\n\n/*=======hide unwanted content in print======*/\n/*============header styles=======*/\nheader .navbar-right .dropdown-menu {\n  padding: 0; }\n\nheader .navbar-right .dropdownheader .nav.nav-tabs,\nheader .navbar-right .dropdown-footer,\nheader .navbar-right .dropdownheader:hover .nav.nav-tabs,\nheader .navbar-right .dropdown-footer:hover {\n  background: #fff;\n  color: #000;\n  border: 1px solid #ccc; }\n\nheader .navbar-right .show > .btn-secondary.dropdown-toggle {\n  background-color: #EEEEEE;\n  color: #000;\n  border-color: #EEEEEE; }\n\nheader .navbar-right .btn-link:focus,\nheader .navbar-right .btn-link:hover,\nheader .navbar-right .btn-link:active {\n  text-decoration: none; }\n\nheader .navbar-right .btn .btn:active {\n  border: none; }\n\nheader .navbar-right .btn-group > button {\n  vertical-align: top;\n  color: #000; }\n  header .navbar-right .btn-group > button:hover {\n    background-color: #ededed;\n    color: #000; }\n\n@media (max-width: 560px) {\n  .notifications-menu .dropdown-menu.dropdown-menu-right {\n    right: -123px; } }\n\n.notifications-menu .dropdown-menu {\n  width: 300px; }\n\n.dropdownheader .nav-tabs .nav-link {\n  color: #000; }\n\n.dropdown-menu .nav-tabs .nav-link:focus,\n.dropdown-menu .nav-tabs .nav-link:hover {\n  border-color: transparent; }\n\n.dropdown-menu .nav-tabs .nav-link.active,\n.dropdown-menu .nav-tabs .nav-item.show .nav-link {\n  color: #000;\n  background-color: #eee;\n  border-left: 1px transparent;\n  border-right: 1px transparent;\n  border-top: 1px transparent; }\n\n.nav-tabs > li > a {\n  color: #000; }\n\n.drpodowtext {\n  color: #333;\n  transition: 300ms; }\n\n.drpodowtext:hover {\n  margin-left: 12px;\n  transition: 300ms; }\n\n.dropdown_content a {\n  width: 100%;\n  display: block;\n  padding: 10px 15px; }\n\n.dropdown-item img {\n  height: 50px;\n  width: 50px; }\n\n.dropdown-item.active,\n.dropdown-item:active {\n  color: #111;\n  text-decoration: none;\n  background-color: transparent; }\n\n.btn-secondary:focus,\n.btn-secondary.focus {\n  box-shadow: none; }\n\n.dropdown-toggle::after {\n  display: none; }\n\n/*============header styles=======*/\n/*====================form editors Start============*/\n.ql-container .ql-editor {\n  min-height: 20em;\n  padding-bottom: 1em;\n  max-height: 25em; }\n\n.nav-tabs:focus {\n  outline: none; }\n\n/*====================form editors end============*/\n/*=========calendar========*/\n.full-calendar-body .dates .dates-events .events-week .events-day {\n  min-height: 100px !important; }\n\n.full-calendar-header > div.header-center {\n  font-size: 18px; }\n\n/*=========calendar========*/\n/*======chartist=====*/\n.ct-series-a .ct-line,\n.ct-series-a .ct-point {\n  stroke: #8dcee4 !important; }\n\n.ct-series-b .ct-line,\n.ct-series-b .ct-point {\n  stroke: #4fc1e9 !important; }\n\n.ct-series-c .ct-line,\n.ct-series-c .ct-point {\n  stroke: #16af81 !important; }\n\n.ct-series-d .ct-line,\n.ct-series-d .ct-point {\n  stroke: #8dcee4 !important; }\n\n/*======chartist bars=====*/\n.ct-series-a .ct-bar {\n  /* Colour of your bars */\n  stroke: #97d9ef !important; }\n\n.ct-series-b .ct-bar {\n  /* Colour of your bars */\n  stroke: #4fc1e9 !important; }\n\n.ct-label {\n  font-size: 10px;\n  color: #000; }\n\n/*======pie chart=====*/\n.ct-series-a .ct-slice-pie {\n  fill: #7faff7 !important; }\n\n.ct-series-b .ct-slice-pie {\n  fill: #6f8dd5 !important; }\n\n.ct-series-c .ct-slice-pie {\n  fill: #11bca9 !important; }\n\n.v-chartist-container {\n  height: 300px; }\n\n/*====== donut chart=====*/\n.ct-series-a .ct-slice-donut {\n  stroke: #11bca9 !important; }\n\n.ct-series-a .ct-slice-donut {\n  stroke: #6f8dd5 !important; }\n\n.ct-series-a .ct-slice-donut {\n  stroke: #7faff7 !important; }\n\n.ct-series-c .ct-slice-donut {\n  stroke: #11bca9 !important; }\n\n.ct-series-b .ct-slice-donut {\n  stroke: #6f8dd5 !important; }\n\n/*=======form-elements======*/\n.drp_align .dropdown-menu {\n  left: -100%; }\n\n.form_elemntsdropdown .dropdown-item {\n  padding: 10px 15px; }\n\n/*=======form-elements======*/\n/*========breadcrumbs====*/\n.breadcrumb1 > .breadcrumb-item + .breadcrumb-item::before {\n  content: \"\\BB\"; }\n\n.breadcrumb2 > .breadcrumb-item + .breadcrumb-item::before {\n  content: \"\\203A\" !important; }\n\n.breadcrumb3 > .breadcrumb-item + .breadcrumb-item::before {\n  content: \"\\2013   \"; }\n\n/*========breadcrumbs====*/\n/*==========datepickers=========*/\n.vdp-datepicker input,\n.vdp-datepicker select {\n  font-size: 100%;\n  font-size: 100%;\n  border: 1px solid #dcdccc;\n  width: 100%; }\n\n@media (max-width: 768px) {\n  .vdp-datepicker__calendar {\n    width: 100% !important; } }\n\n/*==========datepickers=========*/\n.pull-right {\n  float: right; }\n\n.car-header:first-child {\n  border: none !important; }\n\n/* custom component css */\n/* For Tester Sidebar */\n.tester-leftsidebar .menu-wrap {\n  margin-right: 28px !important; }\n  .tester-leftsidebar .menu-wrap span.step-text {\n    color: #fff; }\n  .tester-leftsidebar .menu-wrap a {\n    padding: 10px 7px !important; }\n  .tester-leftsidebar .menu-wrap .listbrdr {\n    border-bottom: 1px solid rgba(66, 68, 77, 0.5); }\n    .tester-leftsidebar .menu-wrap .listbrdr .menu-item {\n      color: rgba(255, 255, 255, 0.5) !important; }\n      .tester-leftsidebar .menu-wrap .listbrdr .menu-item:hover {\n        color: #2cac3d !important; }\n      .tester-leftsidebar .menu-wrap .listbrdr .menu-item:focus {\n        color: #2cac3d !important; }\n    .tester-leftsidebar .menu-wrap .listbrdr .menu-item.active {\n      color: #2cac3d !important;\n      background-color: transparent; }\n  .tester-leftsidebar .menu-wrap a.active {\n    color: #2cac3d !important;\n    background-color: transparent; }\n\n.testing-process-tabwrap,\n.tester-projects-tabwrap {\n  letter-spacing: 0; }\n  .testing-process-tabwrap .nav,\n  .tester-projects-tabwrap .nav {\n    border-bottom: none;\n    margin-bottom: 30px; }\n    .testing-process-tabwrap .nav .nav-item,\n    .tester-projects-tabwrap .nav .nav-item {\n      margin-right: 25px;\n      min-width: 110px;\n      text-align: left;\n      margin-bottom: 0px; }\n      .testing-process-tabwrap .nav .nav-item .nav-link,\n      .tester-projects-tabwrap .nav .nav-item .nav-link {\n        border-bottom: 1px solid #cdcdcd;\n        font-family: \"BrandonTextMedium\";\n        font-size: 16px;\n        color: #606368;\n        padding-left: 0; }\n        .testing-process-tabwrap .nav .nav-item .nav-link:hover,\n        .tester-projects-tabwrap .nav .nav-item .nav-link:hover {\n          border-color: transparent transparent #cdcdcd transparent; }\n      .testing-process-tabwrap .nav .nav-item .nav-link.active,\n      .tester-projects-tabwrap .nav .nav-item .nav-link.active {\n        color: #2cac3d;\n        border: none;\n        border-bottom: 2px solid #2cac3d;\n        font-family: \"BrandonTextBold\"; }\n\n.tester-payment-table .table {\n  border: none;\n  margin-bottom: 0; }\n  .tester-payment-table .table tr {\n    border: none; }\n    .tester-payment-table .table tr th {\n      border: none;\n      padding: 5px 0;\n      text-align: left; }\n    .tester-payment-table .table tr td {\n      border: none;\n      padding: 5px 0;\n      text-align: left; }\n\n.payment-page .vdp-datepicker .form-control[readonly] {\n  background-color: transparent !important; }\n\n/* for publisher sidebar  */\n.custom-leftsidebar .menu-wrap {\n  margin-right: 28px !important; }\n  .custom-leftsidebar .menu-wrap span.step-text {\n    color: #fff; }\n  .custom-leftsidebar .menu-wrap a {\n    padding: 10px 7px !important; }\n  .custom-leftsidebar .menu-wrap .listbrdr {\n    border-bottom: 1px solid rgba(66, 68, 77, 0.5); }\n    .custom-leftsidebar .menu-wrap .listbrdr .menu-item {\n      color: rgba(255, 255, 255, 0.5) !important; }\n      .custom-leftsidebar .menu-wrap .listbrdr .menu-item:hover {\n        color: #00aff3 !important; }\n      .custom-leftsidebar .menu-wrap .listbrdr .menu-item:focus {\n        color: #00aff3 !important; }\n    .custom-leftsidebar .menu-wrap .listbrdr .menu-item.active {\n      color: #00aff3 !important;\n      background-color: transparent; }\n  .custom-leftsidebar .menu-wrap a.active {\n    background-color: transparent; }\n\n/* For Admin Sidebar */\n.admin-sidebar .sidebar .navigation {\n  width: 100%; }\n  .admin-sidebar .sidebar .navigation .listbrdr {\n    display: block; }\n    .admin-sidebar .sidebar .navigation .listbrdr .menu-item {\n      color: rgba(255, 255, 255, 0.5);\n      text-transform: uppercase;\n      font-family: \"UniNeueRegular\";\n      padding: 10px 0px; }\n      .admin-sidebar .sidebar .navigation .listbrdr .menu-item span {\n        margin-left: 5px; }\n      .admin-sidebar .sidebar .navigation .listbrdr .menu-item:hover {\n        color: rgba(20, 193, 191, 0.93); }\n      .admin-sidebar .sidebar .navigation .listbrdr .menu-item:focus {\n        color: rgba(20, 193, 191, 0.93); }\n    .admin-sidebar .sidebar .navigation .listbrdr .menu-item.active {\n      background-color: transparent !important;\n      color: rgba(20, 193, 191, 0.93); }\n    .admin-sidebar .sidebar .navigation .listbrdr a.active {\n      background-color: transparent !important;\n      color: rgba(20, 193, 191, 0.93); }\n\n.admin-tab-wrap .nav {\n  border-bottom: none;\n  margin-bottom: 20px; }\n  .admin-tab-wrap .nav .nav-item {\n    margin-right: 25px;\n    min-width: 130px;\n    text-align: left;\n    margin-bottom: 0px; }\n    .admin-tab-wrap .nav .nav-item .nav-link {\n      border-bottom: 1px solid #cdcdcd;\n      font-size: 20px;\n      color: #606368;\n      padding-left: 0;\n      font-family: \"UniNeueRegular\"; }\n      .admin-tab-wrap .nav .nav-item .nav-link:hover {\n        border-color: transparent transparent #cdcdcd transparent; }\n    .admin-tab-wrap .nav .nav-item .nav-link.active {\n      color: #3e3a94;\n      font-family: \"UniNeueBold\";\n      border: none;\n      border-bottom: 2px solid #3e3a94; }\n\n.admin-project-tabwrap .vue-tab .nav-tabs-navigation {\n  padding-bottom: 20px !important; }\n\n.admin-project-tabwrap .nav {\n  margin-bottom: 20px; }\n  .admin-project-tabwrap .nav li .tabs__link {\n    border-bottom: 1px solid #ccc;\n    -webkit-border-radius: 0 !important;\n    -moz-border-radius: 0 !important;\n    border-radius: 0 !important;\n    margin-right: 30px;\n    padding: 0 0 10px;\n    min-width: 160px;\n    font-size: 17px;\n    font-family: \"BrandonTextMedium\";\n    color: #606368; }\n    .admin-project-tabwrap .nav li .tabs__link span.title {\n      justify-content: left; }\n    .admin-project-tabwrap .nav li .tabs__link:hover {\n      background-color: transparent;\n      outline: none !important; }\n    .admin-project-tabwrap .nav li .tabs__link:focus {\n      outline: none !important; }\n  .admin-project-tabwrap .nav li .active_tab {\n    font-family: \"BrandonTextBold\";\n    color: #3e3a94 !important;\n    background-color: transparent !important;\n    border-bottom: 1px solid #3e3a94 !important;\n    /*border-radius: 0 !important;*/ }\n\n.admin-project-tabwrap .card-body {\n  padding: 15px 0; }\n\n.publisher-project-tabwrap .vue-tab .nav-tabs-navigation {\n  padding-bottom: 20px !important; }\n\n.publisher-project-tabwrap .nav li .tabs__link {\n  border-bottom: 1px solid #ccc;\n  -webkit-border-radius: 0 !important;\n  -moz-border-radius: 0 !important;\n  border-radius: 0 !important;\n  margin-right: 30px;\n  padding: 0 0 10px;\n  min-width: 160px;\n  font-size: 17px;\n  font-family: \"BrandonTextMedium\";\n  color: #606368; }\n  .publisher-project-tabwrap .nav li .tabs__link span.title {\n    justify-content: left; }\n  .publisher-project-tabwrap .nav li .tabs__link:hover {\n    background-color: transparent;\n    outline: none !important; }\n  .publisher-project-tabwrap .nav li .tabs__link:focus {\n    outline: none !important; }\n\n.publisher-project-tabwrap .nav li .active_tab {\n  font-family: \"BrandonTextBold\";\n  color: #0082cc !important;\n  background-color: transparent !important;\n  border-bottom: 1px solid #00aff3 !important;\n  /*border-radius: 0 !important;*/ }\n\n.publisher-project-tabwrap .card-body {\n  padding: 15px 0; }\n\n@media screen and (max-width: 1200px) {\n  .publisher-project-tabwrap .nav li .tabs__link {\n    min-width: 105px;\n    font-size: 15px;\n    margin-right: 20px; } }\n\n@media screen and (max-width: 1112px) {\n  .admin-tab-wrap .nav .nav-item .nav-link {\n    font-size: 18px;\n    padding: 0; } }\n\n@media screen and (max-width: 991px) {\n  .admin-project-tabwrap .nav {\n    margin-bottom: 5px; }\n    .admin-project-tabwrap .nav li .tabs__link {\n      font-size: 14px;\n      padding: 0 0 5px;\n      margin-bottom: 10px;\n      margin-right: 15px;\n      min-width: 105px; } }\n\n@media screen and (max-width: 767px) {\n  .testing-process-tabwrap .nav .nav-item,\n  .tester-projects-tabwrap .nav .nav-item {\n    margin-right: 15px;\n    min-width: 70px; }\n  .tester-payment-table .table tr td {\n    min-width: 120px; }\n    .tester-payment-table .table tr td:nth-child(2) {\n      min-width: 250px; }\n  .admin-tab-wrap .nav .nav-item .nav-link {\n    font-size: 16px;\n    padding: 0; }\n  .publisher-project-tabwrap .nav li .tabs__link {\n    margin-bottom: 10px;\n    padding: 0 0 5px; } }\n\n@media screen and (max-width: 576px) {\n  .admin-tab-wrap .nav .nav-item .nav-link {\n    margin-bottom: 10px;\n    font-size: 14px; }\n  .testing-process-tabwrap,\n  .tester-projects-tabwrap .nav {\n    margin-bottom: 20px; } }\n\n@media screen and (max-width: 360px) {\n  .testing-process-tabwrap,\n  .tester-projects-tabwrap .nav {\n    margin-bottom: 20px; } }\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./resources/assets/components/layouts/css/fixed-menu.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "aside.left-aside {\n  position: fixed;\n  top: 70px;\n  left: 0;\n  max-height: 100%;\n  height: calc(100% - 70px);\n  overflow-y: auto; }\n  @media screen and (max-width: 560px) {\n    aside.left-aside {\n      top: 100px;\n      height: calc(100% - 70px); } }\n\naside.right-aside {\n  display: block !important;\n  width: auto !important;\n  margin-left: 250px; }\n\nbody.left-hidden aside.right-aside {\n  margin-left: 0; }\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0f664e31\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/publisher_layout.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.wrapper[data-v-0f664e31]:before,\n.wrapper[data-v-0f664e31]:after {\n  display: table;\n  content: \" \";\n}\n.wrapper[data-v-0f664e31]:after {\n  clear: both;\n}\n.wrapper[data-v-0f664e31] {\n  display: table;\n  overflow-x: hidden;\n  width: 100%;\n  max-width: 100%;\n  table-layout: fixed;\n}\n.right-aside[data-v-0f664e31],\n.left-aside[data-v-0f664e31] {\n  padding: 0;\n  display: table-cell;\n  vertical-align: top;\n}\n.right-aside[data-v-0f664e31] {\n  background-color: #ebf2f6 !important;\n}\n@media (max-width: 992px) {\n.wrapper > .right-aside[data-v-0f664e31] {\n    width: 100vw;\n    min-width: 100vw;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-25d0ed80\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=1!./resources/assets/components/layouts/header/fixed-header.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.wrapper {\n  margin-top: 70px;\n}\n@media screen and (max-width: 560px) {\n.wrapper {\n      margin-top: 100px;\n}\n}\n.sidebar-toggle {\n  margin-left: 10px;\n}\n.bell_bg button.btn-secondary {\n  background-color: #fff;\n  border: none;\n  border-radius: 0;\n  -webkit-box-shadow: none !important;\n          box-shadow: none !important;\n}\n.bell_bg button.btn-secondary:hover {\n    background-color: #ededed !important;\n}\n.bell_bg button.btn-secondary:active {\n    color: #000 !important;\n}\n.bell_bg.show button {\n  background-color: #ededed !important;\n}\n.bell_bg.user_btn .dropdown-toggle {\n  padding: 7px 9px;\n}\n.tester-topbar .navbar-right .user button.btn-secondary,\n.tester-topbar .navbar-right .bell_bg button.btn-secondary {\n  background-color: transparent !important;\n  outline: none !important;\n  text-transform: capitalize;\n  color: #fff;\n  font-size: 13px;\n  font-weight: 600;\n  position: relative;\n}\n.tester-topbar .navbar-right .user button.btn-secondary:hover,\n.tester-topbar .navbar-right .user button.btn-secondary:focus,\n.tester-topbar .navbar-right .bell_bg button.btn-secondary:hover,\n.tester-topbar .navbar-right .bell_bg button.btn-secondary:focus {\n  background-color: transparent !important;\n  border: 0px !important;\n  -webkit-box-shadow: none !important;\n          box-shadow: none !important;\n  outline: none !important;\n}\n.tester-topbar .navbar-right .user button.btn-secondary {\n  padding-right: 15px;\n}\n.tester-topbar .navbar-right .user button.btn-secondary:before {\n  color: #fff;\n  position: absolute;\n  right: 0;\n  top: 15px;\n  content: '';\n  border-top: .3em solid;\n  border-right: .3em solid transparent;\n  border-bottom: 0;\n  border-left: .3em solid transparent;\n}\n.tabs_cont,\n.event_date {\n  background-color: #fff !important;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-25d0ed80\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/layouts/header/fixed-header.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.header[data-v-25d0ed80] {\n  z-index: 1030;\n}\n.header nav[data-v-25d0ed80] {\n    margin-bottom: 0;\n    height: 100px;\n    background: #fff;\n    background-size: 100% 100%;\n    -webkit-box-shadow: 0px 0px 10px #ccc;\n            box-shadow: 0px 0px 10px #ccc;\n}\n.header .navbar-right[data-v-25d0ed80] {\n    float: right;\n    margin-right: 15px;\n}\n.header .logo[data-v-25d0ed80] {\n    display: block;\n    float: left;\n    height: 50px;\n    line-height: 41px;\n    padding: 3px 10px;\n    text-align: center;\n    width: 250px;\n    background: #fff;\n}\n.header .logo img[data-v-25d0ed80] {\n      width: 125px;\n}\n.header .navbar-right .dropdown-item[data-v-25d0ed80] {\n    padding: 0;\n    width: 100%;\n    outline: none;\n}\n.header .navbar-right div.dropdown > a[data-v-25d0ed80] {\n    color: #000;\n}\n.header .navbar-right div.dropdown .dropdown-menu > button[data-v-25d0ed80] {\n    padding: 10px 15px;\n}\n.header .navbar-right div.dropdown.notifications-menu[data-v-25d0ed80] {\n    height: 50px;\n}\n.header .navbar-right div.dropdown.notifications-menu .noti-icon[data-v-25d0ed80] {\n      font-size: 18px;\n}\n.header .navbar-right div.dropdown > a > i[data-v-25d0ed80] {\n    font-size: 23px;\n}\n.header .navbar-right div.dropdown > a[data-v-25d0ed80] {\n    display: block;\n    padding: 12px;\n}\n.header .navbar-right div.dropdown:hover > a[data-v-25d0ed80] {\n    background-color: #ededed;\n    color: #000;\n}\n.header .navbar-right div.dropdown > a.padding-user[data-v-25d0ed80] {\n    padding-top: 8px;\n    padding-bottom: 6px;\n}\n.header nav .sidebar-toggle[data-v-25d0ed80] {\n    float: left;\n    color: #000;\n    font-size: 19px;\n    padding-top: 10px;\n}\n\n/* .tester-topbar{\r\n    nav {\r\n        background-color: #2cac3d;\r\n    }\r\n} */\n.tester-topbar[data-v-25d0ed80] {\n  color: #ffffff;\n  font-family: \"BrandonTextRegular\";\n}\n.tester-topbar nav[data-v-25d0ed80] {\n    height: 70px;\n    -webkit-box-shadow: none;\n    background-color: #2cac3d;\n    box-shadow: none;\n    font-size: 14px;\n}\n.tester-topbar .logo[data-v-25d0ed80] {\n    background: transparent;\n    margin-top: 10px;\n    text-align: left;\n    padding-left: 25px;\n}\n.tester-topbar .toggle-menu-btn[data-v-25d0ed80] {\n    display: none;\n}\n.tester-topbar .navbar-right[data-v-25d0ed80] {\n    margin-top: 15px;\n}\n.tester-topbar .navbar-right div.dropdown.notifications-menu .noti-icon[data-v-25d0ed80] {\n      color: #fff;\n      font-size: 20px;\n}\n.publisher-topbar nav[data-v-25d0ed80] {\n  background-color: #0081CC;\n}\n@media screen and (max-width: 991px) {\n.tester-topbar .logo[data-v-25d0ed80] {\n    text-align: left;\n    padding-left: 0px;\n}\n.tester-topbar .toggle-menu-btn[data-v-25d0ed80] {\n    display: Block;\n    margin: 8px 15px;\n}\n.tester-topbar .toggle-menu-btn .sidebar-toggle[data-v-25d0ed80] {\n      color: #fff;\n      font-size: 24px;\n      padding: 10px;\n      margin: 0;\n}\n}\n@media screen and (max-width: 560px) {\n.tester-topbar .logo[data-v-25d0ed80] {\n    width: auto !important;\n    margin-top: 5px;\n}\n.tester-topbar .toggle-menu-btn[data-v-25d0ed80] {\n    margin: 5px;\n}\n.tester-topbar .toggle-menu-btn .sidebar-toggle[data-v-25d0ed80] {\n      padding-bottom: 0px;\n}\n.tester-topbar .navbar-right[data-v-25d0ed80] {\n    margin: 5px 0px 0px;\n    width: 100%;\n    padding: 0 5px;\n}\n.tester-topbar .navbar-right .notifications-menu[data-v-25d0ed80] {\n      height: auto !important;\n      float: left;\n}\n.tester-topbar .user_btn[data-v-25d0ed80] {\n    float: right;\n}\n}\n.user_name_max[data-v-25d0ed80] {\n  display: inline-block;\n  max-width: 180px;\n  white-space: nowrap;\n  overflow: hidden !important;\n  text-overflow: ellipsis;\n  margin: 0 0 -4px;\n}\n.noti_msg[data-v-25d0ed80] {\n  font-size: 16px;\n  padding: 10px;\n  border: 1px solid #4f90c1;\n  border-radius: 50px;\n  margin-top: 10px;\n}\n.user.user-menu > button img[data-v-25d0ed80] {\n  width: 35px;\n  height: 35px;\n}\n\n/**** END HEADER RIGHT SIDE DROPDOWNS ****/\n@media screen and (max-width: 767px) {\n.dropdown.open .dropdown-menu[data-v-25d0ed80] {\n    position: absolute;\n}\n.navbar-right > li > a[data-v-25d0ed80] {\n    padding: 10px 12px;\n}\n}\n\n/* Fix menu positions on xs screens to appear correctly and fully */\n@media (max-width: 560px) {\nbody .header .logo[data-v-25d0ed80],\n  body .header nav[data-v-25d0ed80] {\n    width: 100%;\n}\nbody .header nav .sidebar-toggle[data-v-25d0ed80] {\n    padding-left: 15px;\n}\nnav[data-v-25d0ed80] {\n    height: 100px !important;\n}\n}\n.notifications_badge_top[data-v-25d0ed80] {\n  margin-top: -28px;\n  margin-left: 10px;\n  position: absolute;\n}\n.notifications_badge_top span[data-v-25d0ed80] {\n    top: 1px;\n    left: 2px;\n    border-radius: 50%;\n}\n.notifications-menu .dropdown-item[data-v-25d0ed80] {\n  width: 100%;\n  display: block;\n}\n.dropdown-footer[data-v-25d0ed80] {\n  padding: 10px !important;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2e1cda8f\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/notification.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.notification-tabwrap[data-v-2e1cda8f]:focus {\n  outline: none !important;\n}\n.notification-tabwrap .dropdown-item[data-v-2e1cda8f] {\n  padding: 0;\n}\n.notification-tabwrap .dropdown-item[data-v-2e1cda8f]:focus {\n    outline: none !important;\n}\n.notification-tabwrap .dropdown-item .img-wrap img[data-v-2e1cda8f] {\n    height: 32px;\n    width: 32px;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6f3c6c5b\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/layouts/preloader/preloader.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.cssload-aim[data-v-6f3c6c5b] {\n  position: fixed;\n  z-index: 1500;\n  left: calc(100% - 42px);\n  top: 60px;\n  border-radius: 20px;\n  background-color: transparent;\n  border-width: 15px;\n  border-style: double;\n  border-color: transparent #428bca;\n  -webkit-animation: cssload-anim-data-v-6f3c6c5b 0.7s linear infinite;\n          animation: cssload-anim-data-v-6f3c6c5b 0.7s linear infinite;\n}\n@media screen and (max-width: 560px) {\n.cssload-aim[data-v-6f3c6c5b] {\n      top: 106px;\n}\n}\n@-webkit-keyframes cssload-anim-data-v-6f3c6c5b {\n0% {\n    -webkit-transform: rotate(0deg);\n            transform: rotate(0deg);\n}\n100% {\n    -webkit-transform: rotate(360deg);\n            transform: rotate(360deg);\n}\n}\n@keyframes cssload-anim-data-v-6f3c6c5b {\n0% {\n    -webkit-transform: rotate(0deg);\n            transform: rotate(0deg);\n}\n100% {\n    -webkit-transform: rotate(360deg);\n            transform: rotate(360deg);\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-710cf4a1\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/layouts/left-side/default/menu/subMenu.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.collapse-item .submenu-header[data-v-710cf4a1] {\n  cursor: pointer;\n  color: #3e6174;\n  padding: 11px 10px 11px 30px;\n}\n.collapse-item .submenu-header[data-v-710cf4a1]:hover {\n    color: #5e7b85;\n}\n.collapse-item .submenu-header .submenu-header-title[data-v-710cf4a1] {\n    vertical-align: text-bottom;\n}\n.collapse-item .submenu-header .active .submenu-header-title[data-v-710cf4a1] {\n    font-weight: 600;\n}\n.collapse-item .submenu-content[data-v-710cf4a1] {\n  background-color: #F8F7F6;\n  overflow-y: hidden;\n  position: relative;\n}\n.collapse-item .submenu-content-box[data-v-710cf4a1]:active {\n  color: #000;\n}\n.collapse-item .submenu_icon[data-v-710cf4a1] {\n  -webkit-transition: all 0.3s;\n  transition: all 0.3s;\n  font-size: 16px;\n  margin-top: -2px;\n  color: #3e6174;\n}\n.collapse-item.active > .submenu-header[data-v-710cf4a1] {\n  background-color: #e6e6e6;\n  color: #3e6174;\n  font-weight: 500;\n}\n.collapse-item.active > .submenu-header > .submenu_icon[data-v-710cf4a1] {\n    -webkit-transform: rotate(90deg);\n            transform: rotate(90deg);\n}\n.collapse-item.active > .submenu-header i[data-v-710cf4a1] {\n    color: #3e6174 !important;\n}\n.leftmenu_icon[data-v-710cf4a1] {\n  margin-right: 6px;\n  line-height: 20px;\n  height: 22px;\n  width: 20px;\n  text-align: center;\n  font-size: 16px;\n  color: #3e6174;\n}\n.leftmenu_icon:hover .submenu-header-title[data-v-710cf4a1], .leftmenu_icon:hover .submenu_icon[data-v-710cf4a1] {\n  padding-left: 50px;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-71e9fa6d\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/layouts/right-side.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.right-aside[data-v-71e9fa6d] {\n  padding: 20px 20px 10px 20px;\n  width: 100%;\n  max-width: 100%;\n  max-height: 100%;\n  min-height: calc(100vh - 70px);\n  position: relative;\n}\n.right-aside .content-header > h1[data-v-71e9fa6d] {\n    margin: 2px;\n    padding-left: 13px;\n    padding-top: 12px;\n    font-size: 20px;\n    line-height: 1.5;\n}\n.right-aside .content-header[data-v-71e9fa6d] {\n    margin: -2px -20px 25px -20px;\n    height: 55px;\n    background: #f9fafb;\n    -webkit-box-shadow: 3px 1px 5px #ccc;\n            box-shadow: 3px 1px 5px #ccc;\n}\n@media screen and (max-width: 991px) {\n.right-aside .welcome-page h1[data-v-71e9fa6d] {\n    font-size: 34px;\n}\n.right-aside .welcome-page p[data-v-71e9fa6d] {\n    font-size: 16px;\n}\n}\n@media screen and (max-width: 576px) {\n.right-aside .welcome-page h1[data-v-71e9fa6d] {\n    font-size: 28px;\n}\n.right-aside .welcome-page p[data-v-71e9fa6d] {\n    font-size: 14px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7d95a8ee\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/layouts/left-side/publisher/left-side.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.name {\n    font-size: 14px;\n    vertical-align: middle;\n    padding-left: 20px !important;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7d95a8ee\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=1!./resources/assets/components/layouts/left-side/publisher/left-side.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.text-white[data-v-7d95a8ee] {\n  color: #fff;\n}\n.left-aside[data-v-7d95a8ee] {\n  width: 250px;\n  background: #fff;\n  background-repeat: repeat-y;\n}\n.navigation[data-v-7d95a8ee] {\n  padding: 0;\n}\n.divider[data-v-7d95a8ee] {\n  margin-top: 10px;\n  list-style-type: none;\n  border-bottom: 1px solid #ececec;\n  padding-bottom: 6px;\n}\n.divider span[data-v-7d95a8ee] {\n  font-size: 15px;\n  font-weight: 700;\n  color: #fff;\n  margin: 20px 20px -15px 20px;\n}\n.sidebar[data-v-7d95a8ee] {\n  display: block;\n  font-size: 14px;\n  letter-spacing: 1px;\n}\n.content[data-v-7d95a8ee] {\n  display: block;\n  width: auto;\n  overflow-x: hidden;\n  padding: 0 15px;\n}\n.custom-leftsidebar[data-v-7d95a8ee] {\n  background: #1f2027;\n}\n.custom-leftsidebar .profile-wrap[data-v-7d95a8ee] {\n    margin: 40px 0px 0px;\n}\n.custom-leftsidebar .profile-wrap .user-avatar[data-v-7d95a8ee] {\n      padding-bottom: 57px;\n      border-bottom: 1px solid #31323a;\n      margin: 0 28px;\n}\n.custom-leftsidebar .profile-wrap .user-avatar img[data-v-7d95a8ee] {\n        max-width: 121px;\n        border-radius: 50%;\n}\n.custom-leftsidebar .profile-wrap .user-avatar .user-name[data-v-7d95a8ee] {\n        font-size: 16px;\n        margin: 14px;\n        text-transform: capitalize;\n        color: #fff;\n        font-family: \"BrandonTextMedium\";\n}\n.custom-leftsidebar .menu-wrap[data-v-7d95a8ee] {\n    color: #fff;\n    font-size: 13px;\n    font-family: \"UniNeueRegular\";\n    margin: 0 5px 0px 28px;\n}\n.custom-leftsidebar .menu-wrap span.step-text[data-v-7d95a8ee] {\n      color: #fff;\n}\n.custom-leftsidebar .menu-wrap a.active[data-v-7d95a8ee] {\n      background-color: transparent;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b64edb80\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/layouts/left-side/default/menu/MenuItem.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\na[data-v-b64edb80] {\n  color: #3e6174;\n  position: relative;\n  display: block;\n  line-height: 21px;\n  padding: 10px 30px;\n}\na[data-v-b64edb80]:hover {\n    color: #5e7b85;\n}\na.active[data-v-b64edb80] {\n    color: #3e6174;\n    background-color: transparent;\n}\na.active i[data-v-b64edb80] {\n      color: #3e6174 !important;\n}\n\n/*.submenu-content-box .listbrdr::after{\r\n      content: \"\";\r\n     display: block;\r\n     position: absolute;\r\n     width: 11px;\r\n     left: 25px;\r\n     margin-top: -21px;\r\n     border-top: 2px solid #888585;\r\n     }*/\n.submenu-content-box .listbrdr[data-v-b64edb80]:active {\n  color: #000;\n}\n.submenu-content a[data-v-b64edb80] {\n  padding: 11px 20px 10px 45px;\n  position: relative;\n}\n.submenu-content .leftmenu_icon[data-v-b64edb80] {\n  font-size: 14px;\n  color: inherit;\n}\n.name[data-v-b64edb80] {\n  vertical-align: middle;\n  padding-left: 15px;\n}\n.collapse-item .card-content .card-content-box div a[data-v-b64edb80] {\n  padding-left: 43px;\n}\n.leftmenu_icon[data-v-b64edb80] {\n  margin-right: 1px;\n  line-height: 22px;\n  height: 23px;\n  width: 20px;\n  text-align: center;\n  font-size: 16px;\n  color: #3e6174;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/screenfull/dist/screenfull.js":
/***/ (function(module, exports) {

/*!
* screenfull
* v3.3.2 - 2017-10-27
* (c) Sindre Sorhus; MIT License
*/
(function () {
	'use strict';

	var document = typeof window !== 'undefined' && typeof window.document !== 'undefined' ? window.document : {};
	var isCommonjs = typeof module !== 'undefined' && module.exports;
	var keyboardAllowed = typeof Element !== 'undefined' && 'ALLOW_KEYBOARD_INPUT' in Element;

	var fn = (function () {
		var val;

		var fnMap = [
			[
				'requestFullscreen',
				'exitFullscreen',
				'fullscreenElement',
				'fullscreenEnabled',
				'fullscreenchange',
				'fullscreenerror'
			],
			// New WebKit
			[
				'webkitRequestFullscreen',
				'webkitExitFullscreen',
				'webkitFullscreenElement',
				'webkitFullscreenEnabled',
				'webkitfullscreenchange',
				'webkitfullscreenerror'

			],
			// Old WebKit (Safari 5.1)
			[
				'webkitRequestFullScreen',
				'webkitCancelFullScreen',
				'webkitCurrentFullScreenElement',
				'webkitCancelFullScreen',
				'webkitfullscreenchange',
				'webkitfullscreenerror'

			],
			[
				'mozRequestFullScreen',
				'mozCancelFullScreen',
				'mozFullScreenElement',
				'mozFullScreenEnabled',
				'mozfullscreenchange',
				'mozfullscreenerror'
			],
			[
				'msRequestFullscreen',
				'msExitFullscreen',
				'msFullscreenElement',
				'msFullscreenEnabled',
				'MSFullscreenChange',
				'MSFullscreenError'
			]
		];

		var i = 0;
		var l = fnMap.length;
		var ret = {};

		for (; i < l; i++) {
			val = fnMap[i];
			if (val && val[1] in document) {
				for (i = 0; i < val.length; i++) {
					ret[fnMap[0][i]] = val[i];
				}
				return ret;
			}
		}

		return false;
	})();

	var eventNameMap = {
		change: fn.fullscreenchange,
		error: fn.fullscreenerror
	};

	var screenfull = {
		request: function (elem) {
			var request = fn.requestFullscreen;

			elem = elem || document.documentElement;

			// Work around Safari 5.1 bug: reports support for
			// keyboard in fullscreen even though it doesn't.
			// Browser sniffing, since the alternative with
			// setTimeout is even worse.
			if (/ Version\/5\.1(?:\.\d+)? Safari\//.test(navigator.userAgent)) {
				elem[request]();
			} else {
				elem[request](keyboardAllowed && Element.ALLOW_KEYBOARD_INPUT);
			}
		},
		exit: function () {
			document[fn.exitFullscreen]();
		},
		toggle: function (elem) {
			if (this.isFullscreen) {
				this.exit();
			} else {
				this.request(elem);
			}
		},
		onchange: function (callback) {
			this.on('change', callback);
		},
		onerror: function (callback) {
			this.on('error', callback);
		},
		on: function (event, callback) {
			var eventName = eventNameMap[event];
			if (eventName) {
				document.addEventListener(eventName, callback, false);
			}
		},
		off: function (event, callback) {
			var eventName = eventNameMap[event];
			if (eventName) {
				document.removeEventListener(eventName, callback, false);
			}
		},
		raw: fn
	};

	if (!fn) {
		if (isCommonjs) {
			module.exports = false;
		} else {
			window.screenfull = false;
		}

		return;
	}

	Object.defineProperties(screenfull, {
		isFullscreen: {
			get: function () {
				return Boolean(document[fn.fullscreenElement]);
			}
		},
		element: {
			enumerable: true,
			get: function () {
				return document[fn.fullscreenElement];
			}
		},
		enabled: {
			enumerable: true,
			get: function () {
				// Coerce to boolean in case of old WebKit
				return Boolean(document[fn.fullscreenEnabled]);
			}
		}
	});

	if (isCommonjs) {
		module.exports = screenfull;
	} else {
		window.screenfull = screenfull;
	}
})();


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-0f664e31\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/publisher_layout.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("preloader", {
        directives: [
          {
            name: "show",
            rawName: "v-show",
            value: this.$store.state.preloader,
            expression: "this.$store.state.preloader"
          }
        ]
      }),
      _vm._v(" "),
      _c("vueadmin_header"),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "wrapper row-offcanvas" },
        [
          _c("left_side", {
            directives: [
              {
                name: "show",
                rawName: "v-show",
                value: this.$store.state.left_open,
                expression: "this.$store.state.left_open"
              }
            ]
          }),
          _vm._v(" "),
          _c("right_side", [_c("router-view")], 1)
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0f664e31", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-25d0ed80\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/layouts/header/fixed-header.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "header",
    { class: ["header fixed-top tester-topbar", _vm.userClass] },
    [
      _c(
        "nav",
        [
          _c("div", { staticClass: "float-left toggle-menu-btn" }, [
            _c(
              "a",
              {
                staticClass: "sidebar-toggle",
                attrs: { href: "javascript:void(0)" },
                on: { click: _vm.toggle_menu }
              },
              [_c("i", { staticClass: "fa fa-bars" })]
            )
          ]),
          _vm._v(" "),
          _c("router-link", { staticClass: "logo", attrs: { to: "/" } }, [
            _c("a", { attrs: { href: "#" } }, [
              _c("img", {
                attrs: { src: __webpack_require__("./resources/assets/assets/img/logo.png"), alt: "The Beta Plan" }
              })
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "navbar-right" }, [
            _c(
              "div",
              [
                _c(
                  "b-dropdown",
                  {
                    staticClass: "notifications-menu bell_bg",
                    attrs: { right: "", link: "" }
                  },
                  [
                    _c("span", { attrs: { slot: "text" }, slot: "text" }, [
                      _c("i", { staticClass: "fa fa-bell noti-icon" }),
                      _vm._v(" "),
                      _c("div", { staticClass: "notifications_badge_top" }, [
                        _c("span", { staticClass: "badge badge-danger" }, [
                          _vm._v("4\n                            ")
                        ])
                      ])
                    ]),
                    _vm._v(" "),
                    _vm.hasTesterRole
                      ? _c("notifications", [
                          _c(
                            "div",
                            {
                              attrs: { slot: "thumb-icon" },
                              slot: "thumb-icon"
                            },
                            [
                              _c("img", {
                                staticClass: "img-fluid",
                                attrs: {
                                  src: __webpack_require__("./resources/assets/assets/img/tester_calendar_32x32.png")
                                }
                              })
                            ]
                          )
                        ])
                      : _vm.hasPublisherRole
                        ? _c("notifications", [
                            _c(
                              "div",
                              {
                                attrs: { slot: "thumb-icon" },
                                slot: "thumb-icon"
                              },
                              [
                                _c("img", {
                                  staticClass: "img-fluid",
                                  attrs: {
                                    src: __webpack_require__("./resources/assets/assets/img/publisher_calendar_32x32.png")
                                  }
                                })
                              ]
                            )
                          ])
                        : _vm._e(),
                    _vm._v(" "),
                    _vm.hasTesterRole
                      ? _c("notifications", [
                          _c(
                            "div",
                            {
                              attrs: { slot: "thumb-icon" },
                              slot: "thumb-icon"
                            },
                            [
                              _c("img", {
                                staticClass: "img-fluid",
                                attrs: {
                                  src: __webpack_require__("./resources/assets/assets/img/tester_message_32x32.png")
                                }
                              })
                            ]
                          )
                        ])
                      : _vm.hasPublisherRole
                        ? _c("notifications", [
                            _c(
                              "div",
                              {
                                attrs: { slot: "thumb-icon" },
                                slot: "thumb-icon"
                              },
                              [
                                _c("img", {
                                  staticClass: "img-fluid",
                                  attrs: {
                                    src: __webpack_require__("./resources/assets/assets/img/publisher_message_32x32.png")
                                  }
                                })
                              ]
                            )
                          ])
                        : _vm._e(),
                    _vm._v(" "),
                    _vm.hasTesterRole
                      ? _c("notifications", [
                          _c(
                            "div",
                            {
                              attrs: { slot: "thumb-icon" },
                              slot: "thumb-icon"
                            },
                            [
                              _c("img", {
                                staticClass: "img-fluid",
                                attrs: {
                                  src: __webpack_require__("./resources/assets/assets/img/tester_payment_32x32.png")
                                }
                              })
                            ]
                          )
                        ])
                      : _vm.hasPublisherRole
                        ? _c("notifications", [
                            _c(
                              "div",
                              {
                                attrs: { slot: "thumb-icon" },
                                slot: "thumb-icon"
                              },
                              [
                                _c("img", {
                                  staticClass: "img-fluid",
                                  attrs: {
                                    src: __webpack_require__("./resources/assets/assets/img/publisher_payment_32x32.png")
                                  }
                                })
                              ]
                            )
                          ])
                        : _vm._e(),
                    _vm._v(" "),
                    _vm.hasTesterRole
                      ? _c("notifications", [
                          _c(
                            "div",
                            {
                              attrs: { slot: "thumb-icon" },
                              slot: "thumb-icon"
                            },
                            [
                              _c("img", {
                                staticClass: "img-fluid",
                                attrs: {
                                  src: __webpack_require__("./resources/assets/assets/img/tester_profile_32x32.png")
                                }
                              })
                            ]
                          )
                        ])
                      : _vm.hasPublisherRole
                        ? _c("notifications", [
                            _c(
                              "div",
                              {
                                attrs: { slot: "thumb-icon" },
                                slot: "thumb-icon"
                              },
                              [
                                _c("img", {
                                  staticClass: "img-fluid",
                                  attrs: {
                                    src: __webpack_require__("./resources/assets/assets/img/publisher_profile_32x32.png")
                                  }
                                })
                              ]
                            )
                          ])
                        : _vm._e(),
                    _vm._v(" "),
                    _vm.hasTesterRole
                      ? _c("notifications", [
                          _c(
                            "div",
                            {
                              attrs: { slot: "thumb-icon" },
                              slot: "thumb-icon"
                            },
                            [
                              _c("img", {
                                staticClass: "img-fluid",
                                attrs: {
                                  src: __webpack_require__("./resources/assets/assets/img/tester_project_32x32.png")
                                }
                              })
                            ]
                          )
                        ])
                      : _vm.hasPublisherRole
                        ? _c("notifications", [
                            _c(
                              "div",
                              {
                                attrs: { slot: "thumb-icon" },
                                slot: "thumb-icon"
                              },
                              [
                                _c("img", {
                                  staticClass: "img-fluid",
                                  attrs: {
                                    src: __webpack_require__("./resources/assets/assets/img/publisher_project_32x32.png")
                                  }
                                })
                              ]
                            )
                          ])
                        : _vm._e()
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "b-dropdown",
                  {
                    staticClass: "user user-menu bell_bg user_btn",
                    attrs: { right: "", link: "" }
                  },
                  [
                    _c("span", { attrs: { slot: "text" }, slot: "text" }, [
                      _c("p", { staticClass: "user_name_max" }, [
                        _vm._v(
                          _vm._s(this.$store.state.user.first_name) +
                            "\n                        "
                        )
                      ])
                    ]),
                    _vm._v(" "),
                    _c(
                      "b-dropdown-item",
                      { staticClass: "dropdown_content", attrs: { exact: "" } },
                      [
                        _vm.hasTesterRole
                          ? _c(
                              "router-link",
                              {
                                staticClass: "drpodowtext",
                                attrs: {
                                  tag: "a",
                                  to: { name: "tester.tester-profile" },
                                  exact: ""
                                }
                              },
                              [
                                _c("i", { staticClass: "fa fa-user-o" }),
                                _vm._v(" Profile\n                        ")
                              ]
                            )
                          : _vm.hasPublisherRole
                            ? _c(
                                "router-link",
                                {
                                  staticClass: "drpodowtext",
                                  attrs: {
                                    tag: "a",
                                    to: { name: "publisher.profile" },
                                    exact: ""
                                  }
                                },
                                [
                                  _c("i", { staticClass: "fa fa-cog" }),
                                  _vm._v(" Profile\n                        ")
                                ]
                              )
                            : _vm._e()
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "b-dropdown-item",
                      { staticClass: "dropdown_content", attrs: { exact: "" } },
                      [
                        _vm.hasTesterRole
                          ? _c(
                              "router-link",
                              {
                                staticClass: "drpodowtext",
                                attrs: {
                                  tag: "a",
                                  to: { name: "tester.settings" },
                                  exact: ""
                                }
                              },
                              [
                                _c("i", { staticClass: "fa fa-cog" }),
                                _vm._v(" Settings\n                        ")
                              ]
                            )
                          : _vm.hasPublisherRole
                            ? _c(
                                "router-link",
                                {
                                  staticClass: "drpodowtext",
                                  attrs: {
                                    tag: "a",
                                    to: { name: "publisher.settings" },
                                    exact: ""
                                  }
                                },
                                [
                                  _c("i", { staticClass: "fa fa-cog" }),
                                  _vm._v(" Settings\n                        ")
                                ]
                              )
                            : _vm._e()
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "b-dropdown-item",
                      { staticClass: "dropdown_content", attrs: { exact: "" } },
                      [
                        _c(
                          "a",
                          {
                            staticClass: "drpodowtext",
                            on: {
                              click: function($event) {
                                _vm.logout()
                              }
                            }
                          },
                          [
                            _c("i", { staticClass: "fa fa-sign-out" }),
                            _vm._v(" Logout\n                        ")
                          ]
                        )
                      ]
                    )
                  ],
                  1
                )
              ],
              1
            )
          ])
        ],
        1
      )
    ]
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-25d0ed80", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-26ef4c32\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/layouts/left-side/default/menu/vueMenu.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "vuemenu navigation" },
    [_vm._t("default")],
    2
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-26ef4c32", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-2e1cda8f\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/notification.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "notification-tabwrap" },
    [
      _c("b-dropdown-item", [
        _c("div", { staticClass: "row m-0" }, [
          _c("div", { staticClass: "col-2 mt-2 pr-0" }, [
            _c(
              "div",
              { staticClass: "img-wrap rounded-circle" },
              [_vm._t("thumb-icon")],
              2
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-9 mt-2" }, [
            _c("p", [
              _vm._v("  Lorem ipsum dolor sit amet, elit.\n                  "),
              _c("br"),
              _vm._v(" "),
              _c("small", { staticClass: "ml-1 text-info" }, [_vm._v("Today ")])
            ])
          ])
        ])
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-2e1cda8f", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-6f3c6c5b\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/layouts/preloader/preloader.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "cssload-aim" })
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-6f3c6c5b", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-710cf4a1\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/layouts/left-side/default/menu/subMenu.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "submenu collapse-item", class: { active: _vm.isActived } },
    [
      _c(
        "div",
        {
          staticClass: "submenu-header touchable",
          attrs: {
            role: "tab",
            "aria-expanded": _vm.selected ? "true" : "fase"
          },
          on: { click: _vm.toggle }
        },
        [
          _c("i", { staticClass: "leftmenu_icon\n", class: _vm.icon }),
          _vm._v(" "),
          _c("span", { staticClass: "submenu-header-title" }, [
            _vm._v(_vm._s(_vm.title))
          ]),
          _vm._v(" "),
          _vm._m(0)
        ]
      ),
      _vm._v(" "),
      _c(
        "transition",
        {
          attrs: { name: "collapsed-fade", css: false },
          on: {
            "before-appear": _vm.before,
            appear: _vm.enter,
            "appear-cancel": _vm.cancel,
            "before-enter": _vm.before,
            enter: _vm.enter,
            "enter-cancel": _vm.cancel,
            leave: _vm.leave,
            "leave-cancel": _vm.cancel
          }
        },
        [
          _c(
            "div",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.isActived,
                  expression: "isActived"
                }
              ],
              staticClass: "submenu-content"
            },
            [
              _c(
                "div",
                { ref: "box", staticClass: "submenu-content-box" },
                [_vm._t("default")],
                2
              )
            ]
          )
        ]
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "submenu_icon float-right" }, [
      _c("i", { staticClass: "fa fa-angle-right" })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-710cf4a1", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-71e9fa6d\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/layouts/right-side.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("aside", { staticClass: "right-aside" }, [
    _c("section", { staticClass: "content" }, [_vm._t("default")], 2)
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-71e9fa6d", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-7d95a8ee\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/layouts/left-side/publisher/left-side.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "aside",
    { staticClass: "left-aside sidebar-offcanvas custom-leftsidebar" },
    [
      _c("section", { staticClass: "sidebar" }, [
        _c("div", { attrs: { id: "menu", role: "navigation" } }, [
          _c("div", { staticClass: "nav_profile profile-wrap" }, [
            _c("div", { staticClass: "text-center user-avatar" }, [
              _c("img", {
                attrs: {
                  src: this.$store.state.user.profile,
                  alt: "user-avatar"
                }
              }),
              _vm._v(" "),
              _c("p", { staticClass: "user-name mb-2" }, [
                _vm._v(
                  "\n                        " +
                    _vm._s(this.$store.state.user.first_name) +
                    "  " +
                    _vm._s(this.$store.state.user.last_name) +
                    "\n                    "
                )
              ])
            ])
          ]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "menu-wrap" },
            [
              _c(
                "vmenu",
                [
                  _vm._l(_vm.menuitems, function(item) {
                    return [
                      item.link
                        ? _c(
                            "vmenu-item",
                            {
                              attrs: {
                                link: item.link,
                                activeIcon: item.activeIcon,
                                src: item.src
                              }
                            },
                            [
                              _vm._v(
                                _vm._s(item.name) + "\n                        "
                              )
                            ]
                          )
                        : _vm._e()
                    ]
                  })
                ],
                2
              )
            ],
            1
          )
        ])
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-7d95a8ee", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-b64edb80\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/layouts/left-side/default/menu/MenuItem.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "listbrdr",
      on: { mouseover: _vm.onMouseEnter, mouseout: _vm.onMouseExit }
    },
    [
      _c(
        "router-link",
        { staticClass: "menu-item", attrs: { to: _vm.link, exact: "" } },
        [
          _c("i", { staticClass: "leftmenu_icon", class: _vm.icon }),
          _vm._v(" "),
          _vm.link == _vm.$route.path
            ? _c("img", { attrs: { src: _vm.activeIcon } })
            : _c("img", { attrs: { src: _vm.currentIcon } }),
          _vm._v(" "),
          _c("span", { staticClass: "name" }, [_vm._t("default")], 2)
        ]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-b64edb80", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0f664e31\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/publisher_layout.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0f664e31\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/publisher_layout.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("272661ed", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0f664e31\",\"scoped\":true,\"hasInlineConfig\":true}!../../node_modules/sass-loader/lib/loader.js!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./publisher_layout.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0f664e31\",\"scoped\":true,\"hasInlineConfig\":true}!../../node_modules/sass-loader/lib/loader.js!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./publisher_layout.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-25d0ed80\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=1!./resources/assets/components/layouts/header/fixed-header.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-25d0ed80\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=1!./resources/assets/components/layouts/header/fixed-header.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("8e8e35d8", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-25d0ed80\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=1!./fixed-header.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-25d0ed80\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=1!./fixed-header.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-25d0ed80\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/layouts/header/fixed-header.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-25d0ed80\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/layouts/header/fixed-header.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("54fb3f62", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-25d0ed80\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./fixed-header.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-25d0ed80\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./fixed-header.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2e1cda8f\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/notification.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2e1cda8f\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/notification.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("1b4b7ad8", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2e1cda8f\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./notification.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2e1cda8f\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./notification.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6f3c6c5b\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/layouts/preloader/preloader.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6f3c6c5b\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/layouts/preloader/preloader.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("4ad70b8e", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6f3c6c5b\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./preloader.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6f3c6c5b\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./preloader.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-710cf4a1\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/layouts/left-side/default/menu/subMenu.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-710cf4a1\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/layouts/left-side/default/menu/subMenu.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("12fb026b", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../../node_modules/css-loader/index.js!../../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-710cf4a1\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./subMenu.vue", function() {
     var newContent = require("!!../../../../../../../node_modules/css-loader/index.js!../../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-710cf4a1\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./subMenu.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-71e9fa6d\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/layouts/right-side.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-71e9fa6d\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/layouts/right-side.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("ae9b1cf8", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-71e9fa6d\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./right-side.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-71e9fa6d\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./right-side.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7d95a8ee\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/layouts/left-side/publisher/left-side.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7d95a8ee\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/layouts/left-side/publisher/left-side.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("c67fb0f0", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7d95a8ee\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./left-side.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7d95a8ee\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./left-side.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7d95a8ee\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=1!./resources/assets/components/layouts/left-side/publisher/left-side.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7d95a8ee\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=1!./resources/assets/components/layouts/left-side/publisher/left-side.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("3a82e955", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7d95a8ee\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=1!./left-side.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7d95a8ee\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=1!./left-side.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b64edb80\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/layouts/left-side/default/menu/MenuItem.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b64edb80\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/layouts/left-side/default/menu/MenuItem.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("551260e3", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../../node_modules/css-loader/index.js!../../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b64edb80\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./MenuItem.vue", function() {
     var newContent = require("!!../../../../../../../node_modules/css-loader/index.js!../../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b64edb80\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./MenuItem.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/assets/img/logo.png":
/***/ (function(module, exports) {

module.exports = "/images/logo.png?1a2086faf6b06b086b9f10c5cc50eae2";

/***/ }),

/***/ "./resources/assets/assets/img/publisher-dashboard-icon-active.png":
/***/ (function(module, exports) {

module.exports = "/images/publisher-dashboard-icon-active.png?ac013845fd46f6479ffe1e5a3df2ecc7";

/***/ }),

/***/ "./resources/assets/assets/img/publisher-dashboard-icon.png":
/***/ (function(module, exports) {

module.exports = "/images/publisher-dashboard-icon.png?cee87a20d96f9742f0d987e9487c065a";

/***/ }),

/***/ "./resources/assets/assets/img/publisher-finance-icon-active.png":
/***/ (function(module, exports) {

module.exports = "/images/publisher-finance-icon-active.png?76ba1408c42210197305caaac3afdae3";

/***/ }),

/***/ "./resources/assets/assets/img/publisher-finance-icon.png":
/***/ (function(module, exports) {

module.exports = "/images/publisher-finance-icon.png?55b74056d732568e7930ce1b2890c9ef";

/***/ }),

/***/ "./resources/assets/assets/img/publisher-help-icon-active.png":
/***/ (function(module, exports) {

module.exports = "/images/publisher-help-icon-active.png?417a9751d458e35ca73648240676b900";

/***/ }),

/***/ "./resources/assets/assets/img/publisher-help-icon.png":
/***/ (function(module, exports) {

module.exports = "/images/publisher-help-icon.png?f79bcb5bde5b113cbf1d243a96908138";

/***/ }),

/***/ "./resources/assets/assets/img/publisher-inbox-icon-active.png":
/***/ (function(module, exports) {

module.exports = "/images/publisher-inbox-icon-active.png?0572ab12ad036b256cfda6417b448c83";

/***/ }),

/***/ "./resources/assets/assets/img/publisher-inbox-icon.png":
/***/ (function(module, exports) {

module.exports = "/images/publisher-inbox-icon.png?9ef9e6b4b83a55223231c6dd190459e6";

/***/ }),

/***/ "./resources/assets/assets/img/publisher-members-icon-active.png":
/***/ (function(module, exports) {

module.exports = "/images/publisher-members-icon-active.png?ad2097fceaf212c66fea1570e9e9b460";

/***/ }),

/***/ "./resources/assets/assets/img/publisher-members-icon.png":
/***/ (function(module, exports) {

module.exports = "/images/publisher-members-icon.png?d4e0ca1f76e6b7ad8da6af2824076ca7";

/***/ }),

/***/ "./resources/assets/assets/img/publisher-profile-icon-active.png":
/***/ (function(module, exports) {

module.exports = "/images/publisher-profile-icon-active.png?0142e27aeedd445f801b5b7493efb5c4";

/***/ }),

/***/ "./resources/assets/assets/img/publisher-profile-icon.png":
/***/ (function(module, exports) {

module.exports = "/images/publisher-profile-icon.png?c8eb7fef0eba0b5c58925d8d35841341";

/***/ }),

/***/ "./resources/assets/assets/img/publisher-projects-icon-active.png":
/***/ (function(module, exports) {

module.exports = "/images/publisher-projects-icon-active.png?43a0cdff0803a7e03e73294b41b71066";

/***/ }),

/***/ "./resources/assets/assets/img/publisher-projects-icon.png":
/***/ (function(module, exports) {

module.exports = "/images/publisher-projects-icon.png?6360204bac9909de0e292c52c8408456";

/***/ }),

/***/ "./resources/assets/assets/img/publisher-test-project-icon-active.png":
/***/ (function(module, exports) {

module.exports = "/images/publisher-test-project-icon-active.png?f06927086926c783ab72c76522520417";

/***/ }),

/***/ "./resources/assets/assets/img/publisher-test-project-icon.png":
/***/ (function(module, exports) {

module.exports = "/images/publisher-test-project-icon.png?2b7b9110cadc226f56ab5dbc379fe640";

/***/ }),

/***/ "./resources/assets/assets/img/publisher_calendar_32x32.png":
/***/ (function(module, exports) {

module.exports = "/images/publisher_calendar_32x32.png?9f307baa347362191d5dcc5aabef12ce";

/***/ }),

/***/ "./resources/assets/assets/img/publisher_message_32x32.png":
/***/ (function(module, exports) {

module.exports = "/images/publisher_message_32x32.png?c58ac3eaa92990a0c82ee13c7ae5ffef";

/***/ }),

/***/ "./resources/assets/assets/img/publisher_payment_32x32.png":
/***/ (function(module, exports) {

module.exports = "/images/publisher_payment_32x32.png?d532fce302dcd5c1a36dffcddb6e84e5";

/***/ }),

/***/ "./resources/assets/assets/img/publisher_profile_32x32.png":
/***/ (function(module, exports) {

module.exports = "/images/publisher_profile_32x32.png?748dbfbb5603ab3978d458a53d7026dc";

/***/ }),

/***/ "./resources/assets/assets/img/publisher_project_32x32.png":
/***/ (function(module, exports) {

module.exports = "/images/publisher_project_32x32.png?21e7039e177fe7a81f4f81552e7ee50d";

/***/ }),

/***/ "./resources/assets/assets/img/tester_calendar_32x32.png":
/***/ (function(module, exports) {

module.exports = "/images/tester_calendar_32x32.png?4a91770e43d5c793e94dccf1e0c22925";

/***/ }),

/***/ "./resources/assets/assets/img/tester_message_32x32.png":
/***/ (function(module, exports) {

module.exports = "/images/tester_message_32x32.png?9d57f743e930db3c6ff0e29edae40fa9";

/***/ }),

/***/ "./resources/assets/assets/img/tester_payment_32x32.png":
/***/ (function(module, exports) {

module.exports = "/images/tester_payment_32x32.png?c48bf6c53f0280fbd7693d419a3136e1";

/***/ }),

/***/ "./resources/assets/assets/img/tester_profile_32x32.png":
/***/ (function(module, exports) {

module.exports = "/images/tester_profile_32x32.png?d82305d6daf38c0fe829a2ce4c9257f8";

/***/ }),

/***/ "./resources/assets/assets/img/tester_project_32x32.png":
/***/ (function(module, exports) {

module.exports = "/images/tester_project_32x32.png?8f099b9bc7b8e13ab190bbb68dc1ab6d";

/***/ }),

/***/ "./resources/assets/assets/sass/custom.scss":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./resources/assets/assets/sass/custom.scss");
if(typeof content === 'string') content = [[module.i, content, '']];
// Prepare cssTransformation
var transform;

var options = {}
options.transform = transform
// add the styles to the DOM
var update = __webpack_require__("./node_modules/style-loader/lib/addStyles.js")(content, options);
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/sass-loader/lib/loader.js!./custom.scss", function() {
			var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/sass-loader/lib/loader.js!./custom.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/components/components/notification.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2e1cda8f\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/notification.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/notification.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-2e1cda8f\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/notification.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-2e1cda8f"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\notification.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-2e1cda8f", Component.options)
  } else {
    hotAPI.reload("data-v-2e1cda8f", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/layouts/css/fixed-menu.scss":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./resources/assets/components/layouts/css/fixed-menu.scss");
if(typeof content === 'string') content = [[module.i, content, '']];
// Prepare cssTransformation
var transform;

var options = {}
options.transform = transform
// add the styles to the DOM
var update = __webpack_require__("./node_modules/style-loader/lib/addStyles.js")(content, options);
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/sass-loader/lib/loader.js!./fixed-menu.scss", function() {
			var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/sass-loader/lib/loader.js!./fixed-menu.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/components/layouts/header/fixed-header.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-25d0ed80\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/layouts/header/fixed-header.vue")
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-25d0ed80\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=1!./resources/assets/components/layouts/header/fixed-header.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/layouts/header/fixed-header.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-25d0ed80\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/layouts/header/fixed-header.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-25d0ed80"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\layouts\\header\\fixed-header.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-25d0ed80", Component.options)
  } else {
    hotAPI.reload("data-v-25d0ed80", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/layouts/left-side/default/menu/MenuItem.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b64edb80\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/layouts/left-side/default/menu/MenuItem.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/layouts/left-side/default/menu/MenuItem.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-b64edb80\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/layouts/left-side/default/menu/MenuItem.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-b64edb80"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\layouts\\left-side\\default\\menu\\MenuItem.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-b64edb80", Component.options)
  } else {
    hotAPI.reload("data-v-b64edb80", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/layouts/left-side/default/menu/index.js":
/***/ (function(module, exports, __webpack_require__) {

exports.vmenu = __webpack_require__("./resources/assets/components/layouts/left-side/default/menu/vueMenu.vue");
exports.vsubMenu = __webpack_require__("./resources/assets/components/layouts/left-side/default/menu/subMenu.vue");
exports.vmenuItem = __webpack_require__("./resources/assets/components/layouts/left-side/default/menu/MenuItem.vue");

/***/ }),

/***/ "./resources/assets/components/layouts/left-side/default/menu/subMenu.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-710cf4a1\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/layouts/left-side/default/menu/subMenu.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/layouts/left-side/default/menu/subMenu.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-710cf4a1\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/layouts/left-side/default/menu/subMenu.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-710cf4a1"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\layouts\\left-side\\default\\menu\\subMenu.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-710cf4a1", Component.options)
  } else {
    hotAPI.reload("data-v-710cf4a1", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/layouts/left-side/default/menu/vueMenu.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/layouts/left-side/default/menu/vueMenu.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-26ef4c32\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/layouts/left-side/default/menu/vueMenu.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\layouts\\left-side\\default\\menu\\vueMenu.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-26ef4c32", Component.options)
  } else {
    hotAPI.reload("data-v-26ef4c32", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/layouts/left-side/publisher/left-side.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7d95a8ee\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/layouts/left-side/publisher/left-side.vue")
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7d95a8ee\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=1!./resources/assets/components/layouts/left-side/publisher/left-side.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/layouts/left-side/publisher/left-side.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-7d95a8ee\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/layouts/left-side/publisher/left-side.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-7d95a8ee"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\layouts\\left-side\\publisher\\left-side.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7d95a8ee", Component.options)
  } else {
    hotAPI.reload("data-v-7d95a8ee", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/layouts/preloader/preloader.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6f3c6c5b\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/layouts/preloader/preloader.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = null
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-6f3c6c5b\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/layouts/preloader/preloader.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-6f3c6c5b"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\layouts\\preloader\\preloader.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6f3c6c5b", Component.options)
  } else {
    hotAPI.reload("data-v-6f3c6c5b", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/layouts/right-side.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-71e9fa6d\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/layouts/right-side.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/layouts/right-side.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-71e9fa6d\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/layouts/right-side.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-71e9fa6d"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\layouts\\right-side.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-71e9fa6d", Component.options)
  } else {
    hotAPI.reload("data-v-71e9fa6d", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/publisher_layout.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0f664e31\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/publisher_layout.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/publisher_layout.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-0f664e31\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/publisher_layout.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-0f664e31"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\publisher_layout.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0f664e31", Component.options)
  } else {
    hotAPI.reload("data-v-0f664e31", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/publisher_menu.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_src_router_router_prefix__ = __webpack_require__("./resources/assets/router/router-prefix.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_src_assets_img_publisher_dashboard_icon_active_png__ = __webpack_require__("./resources/assets/assets/img/publisher-dashboard-icon-active.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_src_assets_img_publisher_dashboard_icon_active_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_src_assets_img_publisher_dashboard_icon_active_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_src_assets_img_publisher_dashboard_icon_png__ = __webpack_require__("./resources/assets/assets/img/publisher-dashboard-icon.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_src_assets_img_publisher_dashboard_icon_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_src_assets_img_publisher_dashboard_icon_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_src_assets_img_publisher_finance_icon_png__ = __webpack_require__("./resources/assets/assets/img/publisher-finance-icon.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_src_assets_img_publisher_finance_icon_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_src_assets_img_publisher_finance_icon_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_src_assets_img_publisher_finance_icon_active_png__ = __webpack_require__("./resources/assets/assets/img/publisher-finance-icon-active.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_src_assets_img_publisher_finance_icon_active_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_src_assets_img_publisher_finance_icon_active_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_src_assets_img_publisher_help_icon_png__ = __webpack_require__("./resources/assets/assets/img/publisher-help-icon.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_src_assets_img_publisher_help_icon_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_src_assets_img_publisher_help_icon_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_src_assets_img_publisher_help_icon_active_png__ = __webpack_require__("./resources/assets/assets/img/publisher-help-icon-active.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_src_assets_img_publisher_help_icon_active_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_src_assets_img_publisher_help_icon_active_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_src_assets_img_publisher_members_icon_png__ = __webpack_require__("./resources/assets/assets/img/publisher-members-icon.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_src_assets_img_publisher_members_icon_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_src_assets_img_publisher_members_icon_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_src_assets_img_publisher_members_icon_active_png__ = __webpack_require__("./resources/assets/assets/img/publisher-members-icon-active.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_src_assets_img_publisher_members_icon_active_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_src_assets_img_publisher_members_icon_active_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_src_assets_img_publisher_test_project_icon_png__ = __webpack_require__("./resources/assets/assets/img/publisher-test-project-icon.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_src_assets_img_publisher_test_project_icon_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_src_assets_img_publisher_test_project_icon_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_src_assets_img_publisher_test_project_icon_active_png__ = __webpack_require__("./resources/assets/assets/img/publisher-test-project-icon-active.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_src_assets_img_publisher_test_project_icon_active_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_src_assets_img_publisher_test_project_icon_active_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_src_assets_img_publisher_inbox_icon_png__ = __webpack_require__("./resources/assets/assets/img/publisher-inbox-icon.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_src_assets_img_publisher_inbox_icon_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_src_assets_img_publisher_inbox_icon_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_src_assets_img_publisher_inbox_icon_active_png__ = __webpack_require__("./resources/assets/assets/img/publisher-inbox-icon-active.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_src_assets_img_publisher_inbox_icon_active_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_src_assets_img_publisher_inbox_icon_active_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_src_assets_img_publisher_profile_icon_png__ = __webpack_require__("./resources/assets/assets/img/publisher-profile-icon.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_src_assets_img_publisher_profile_icon_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_src_assets_img_publisher_profile_icon_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_src_assets_img_publisher_profile_icon_active_png__ = __webpack_require__("./resources/assets/assets/img/publisher-profile-icon-active.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_src_assets_img_publisher_profile_icon_active_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_14_src_assets_img_publisher_profile_icon_active_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_src_assets_img_publisher_projects_icon_png__ = __webpack_require__("./resources/assets/assets/img/publisher-projects-icon.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_src_assets_img_publisher_projects_icon_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_src_assets_img_publisher_projects_icon_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_src_assets_img_publisher_projects_icon_active_png__ = __webpack_require__("./resources/assets/assets/img/publisher-projects-icon-active.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_src_assets_img_publisher_projects_icon_active_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_16_src_assets_img_publisher_projects_icon_active_png__);


















var menu_items = [{
    name: "DASHBOARD",
    link: __WEBPACK_IMPORTED_MODULE_0_src_router_router_prefix__["a" /* ROUTE_PREFIX */] + "/publisher/dashboard",
    icon: "",
    src: __WEBPACK_IMPORTED_MODULE_2_src_assets_img_publisher_dashboard_icon_png___default.a,
    activeIcon: __WEBPACK_IMPORTED_MODULE_1_src_assets_img_publisher_dashboard_icon_active_png___default.a
}, {
    name: "PROJECTS",
    link: __WEBPACK_IMPORTED_MODULE_0_src_router_router_prefix__["a" /* ROUTE_PREFIX */] + "/publisher/projects",
    icon: "",
    src: __WEBPACK_IMPORTED_MODULE_15_src_assets_img_publisher_projects_icon_png___default.a,
    activeIcon: __WEBPACK_IMPORTED_MODULE_16_src_assets_img_publisher_projects_icon_active_png___default.a
}, {
    name: "FINANCES",
    link: __WEBPACK_IMPORTED_MODULE_0_src_router_router_prefix__["a" /* ROUTE_PREFIX */] + "/publisher/finance",
    icon: "",
    src: __WEBPACK_IMPORTED_MODULE_3_src_assets_img_publisher_finance_icon_png___default.a,
    activeIcon: __WEBPACK_IMPORTED_MODULE_4_src_assets_img_publisher_finance_icon_active_png___default.a
}, {
    name: "MEMBERS",
    link: __WEBPACK_IMPORTED_MODULE_0_src_router_router_prefix__["a" /* ROUTE_PREFIX */] + "/publisher/members",
    icon: "",
    src: __WEBPACK_IMPORTED_MODULE_7_src_assets_img_publisher_members_icon_png___default.a,
    activeIcon: __WEBPACK_IMPORTED_MODULE_8_src_assets_img_publisher_members_icon_active_png___default.a
}, {
    name: "INBOX",
    link: __WEBPACK_IMPORTED_MODULE_0_src_router_router_prefix__["a" /* ROUTE_PREFIX */] + "/publisher/inbox",
    icon: "",
    src: __WEBPACK_IMPORTED_MODULE_11_src_assets_img_publisher_inbox_icon_png___default.a,
    activeIcon: __WEBPACK_IMPORTED_MODULE_12_src_assets_img_publisher_inbox_icon_active_png___default.a
}, {
    name: "HELP",
    link: __WEBPACK_IMPORTED_MODULE_0_src_router_router_prefix__["a" /* ROUTE_PREFIX */] + "/publisher/help",
    icon: "",
    src: __WEBPACK_IMPORTED_MODULE_5_src_assets_img_publisher_help_icon_png___default.a,
    activeIcon: __WEBPACK_IMPORTED_MODULE_6_src_assets_img_publisher_help_icon_active_png___default.a
}];
/* harmony default export */ __webpack_exports__["a"] = (menu_items);

/***/ })

});