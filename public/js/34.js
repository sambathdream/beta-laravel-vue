webpackJsonp([34],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/admin/testproject_item.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_components_components_device_selector__ = __webpack_require__("./resources/assets/components/components/device-selector.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_components_components_device_selector___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_components_components_device_selector__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "ProjectItem",
  components: {
    DeviceSelector: __WEBPACK_IMPORTED_MODULE_0_components_components_device_selector___default.a
  },
  props: {
    project: {
      type: Object,
      required: true
    }
  },
  data: function data() {
    return {
      originalUser: window.USER
    };
  },

  computed: {
    selected_devices: function selected_devices() {
      return (this.project.devices || []).map(function (_ref) {
        var id = _ref.id;
        return id;
      });
    }
  },
  methods: {
    deleteTestProject: function deleteTestProject(id) {
      var _this = this;

      this.$swal({
        title: "Are you sure?",
        text: "You want to delete this project?",
        icon: "warning",
        buttons: true,
        dangerMode: true
      }).then(function (willDelete) {
        if (willDelete) {
          return axios.delete("/api/projects/" + id + "?test=1").then(function () {
            return _this.loadProjects();
          }).catch(function (e) {
            return _this.errors = e.response.message;
          });
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/device-selector.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "DeviceSelector",
  props: {
    disabled: {
      type: Boolean,
      defualt: function defualt() {
        return false;
      }
    },
    value: {
      type: Array,
      required: true,
      default: function _default() {
        return [];
      }
    }
  },
  data: function data() {
    return {
      devices: []
    };
  },

  methods: {
    toggleItem: function toggleItem(device) {
      if (this.disabled) {
        return true;
      }
      var index = this.value.indexOf(device.id);
      var newDevices = [];
      if (index >= 0) {
        newDevices = this.value.filter(function (i) {
          return i !== device.id;
        });
      } else {
        newDevices = [].concat(_toConsumableArray(this.value), [device.id]);
      }
      this.$emit("input", newDevices);
      this.$emit("change", newDevices);
    }
  },
  mounted: function mounted() {
    var _this = this;

    if (!this.$store.state.devices.length) {
      axios.get("/api/device").then(function (_ref) {
        var data = _ref.data;

        _this.$store.commit("set_devices", data);
        _this.devices = data;
      });
    } else {
      this.devices = this.$store.state.devices;
    }
  },
  computed: {
    availableDevices: function availableDevices() {
      var _this2 = this;

      if (this.disabled) {
        return this.devices.filter(function (_ref2) {
          var id = _ref2.id;
          return _this2.value.indexOf(id) >= 0;
        });
      }
      return this.devices;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/test-project/test_project_list.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_components_components_admin_testproject_item__ = __webpack_require__("./resources/assets/components/components/admin/testproject_item.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_components_components_admin_testproject_item___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_components_components_admin_testproject_item__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    name: "projects_list",
    components: {
        TestProjectItem: __WEBPACK_IMPORTED_MODULE_0_components_components_admin_testproject_item___default.a
    },
    data: function data() {
        return {
            testprojects: {},
            formstate: {},
            model: {},
            originalUser: {},
            aDevices: {},
            additional_devices: [],
            project_types: {}
        };
    },

    mounted: function mounted() {
        var _this = this;

        this.loadProjects();
        axios.get("/api/devices").then(function (response) {
            _this.aDevices = response.data;
        }).then(function (_ref) {
            var data = _ref.data.data;
            return _this.assignData(data);
        }).catch(function (error) {});
        axios.get("/api/tester/" + this.$store.state.user.id).then(function (_ref2) {
            var data = _ref2.data.data;

            _this.model = data;
        }).catch(function (error) {});
        axios.get("/api/project-types").then(function (response) {
            _this.project_types = response.data;
        }).catch(function (error) {});
        this.originalUser = window.USER;
        if (this.originalUser.name == null) {
            this.originalUser.name = this.originalUser.full_name;
        }
    },
    computed: {
        selected_devices: function selected_devices(index) {
            return (this.project.devices || []).map(function (_ref3) {
                var id = _ref3.id;
                return id;
            });
        }
    },
    methods: {
        loadProjects: function loadProjects() {
            var _this2 = this;

            axios.get("/api/projects?test=1").then(function (response) {
                _this2.testprojects = response.data;
            }).catch(function (error) {});
        },
        assignData: function assignData(data) {
            this.model = data;
            if (this.model.aDevices.id == null) {
                this.model.aDevices_id = 0; //
            }
        },
        deleteTestProject: function deleteTestProject(id) {
            var _this3 = this;

            this.$swal({
                title: "Are you sure?",
                text: "You want to delete this project?",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then(function (willDelete) {
                if (willDelete) {
                    return axios.delete("/api/projects/" + id + "?test=1").then(function () {
                        return _this3.loadProjects();
                    }).catch(function (e) {
                        return _this3.errors = e.response.message;
                    });
                }
            });
        }
    }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/device-selector.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\nimg[data-v-0d653902] {\n  height: 50px;\n  width: 50px;\n  display: inline-block;\n  margin-right: 10px;\n  float: left;\n  opacity: 0.5;\n  cursor: pointer;\n}\nimg.disabled[data-v-0d653902] {\n  cursor: auto;\n}\nimg.selected[data-v-0d653902] {\n  opacity: 1;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-248c1b1c\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/admin/testproject_item.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.page-title[data-v-248c1b1c] {\n  font-size: 30px;\n  color: #363e48;\n  font-family: \"UniNeueBold\";\n}\n.text-yellow[data-v-248c1b1c] {\n  color: #ffcc00;\n}\n.text-orange[data-v-248c1b1c] {\n  color: #ff8a0e;\n}\n.text-green[data-v-248c1b1c] {\n  color: #02c505;\n}\n.text-darkgreen[data-v-248c1b1c] {\n  color: #2cac3d;\n}\n.text-red[data-v-248c1b1c] {\n  color: #f83636;\n}\n.text-bold[data-v-248c1b1c] {\n  font-family: \"BrandonTextBold\";\n}\n.text-medium[data-v-248c1b1c] {\n  font-family: \"BrandonTextMedium\";\n}\n.purple-btn[data-v-248c1b1c] {\n  background-color: #5651b9;\n  border: 2px solid #3e3a94;\n  color: #fff;\n  font-size: 15px;\n  line-height: 16px;\n  font-family: \"BrandonTextMedium\";\n  border-radius: 25px;\n  padding: 4px 6px;\n  -webkit-transition: all 0.4s ease;\n  transition: all 0.4s ease;\n}\n.purple-btn[data-v-248c1b1c]:hover {\n    outline: none !important;\n    text-decoration: underline !important;\n}\n.purple-btn[data-v-248c1b1c]:focus {\n    outline: none !important;\n}\n.white-box[data-v-248c1b1c] {\n  background-color: #fff;\n  -webkit-box-shadow: 1px 1px 1px 1px rgba(0, 0, 0, 0.1);\n          box-shadow: 1px 1px 1px 1px rgba(0, 0, 0, 0.1);\n  border-radius: 4px;\n  font-family: \"BrandonTextMedium\";\n  font-size: 14px;\n  color: #606368;\n  padding: 15px;\n}\n.white-box .proj-block-wrap[data-v-248c1b1c] {\n    padding: 15px 0;\n    font-family: \"BrandonTextRegular\";\n    font-size: 14px;\n    color: #606368;\n    position: relative;\n    border-bottom: 1px solid #dadada;\n}\n.white-box .proj-block-wrap[data-v-248c1b1c]:first-child {\n      padding-top: 0;\n}\n.white-box .proj-block-wrap[data-v-248c1b1c]:last-child {\n      padding-bottom: 0;\n      border: none;\n}\n.white-box .proj-block-wrap span[data-v-248c1b1c] {\n      vertical-align: top;\n}\n.white-box .proj-block-wrap .border-xl-right[data-v-248c1b1c] {\n      border-right: 1px solid #dadada;\n}\n.white-box .proj-block-wrap .proj-title[data-v-248c1b1c] {\n      font-family: \"BrandonTextBold\";\n      font-size: 16px;\n      color: #363e48;\n}\n.white-box .proj-block-wrap .edit-icon[data-v-248c1b1c] {\n      color: #3e3a94;\n      font-size: 24px;\n      padding: 5px 0;\n      background-color: transparent;\n      border: none;\n}\n.white-box .proj-block-wrap .edit-icon[data-v-248c1b1c]:hover {\n        border: none;\n        outline: none;\n}\n.white-box .proj-block-wrap .edit-icon[data-v-248c1b1c]:focus {\n        border: none;\n        outline: none;\n}\n.white-box .proj-block-wrap .delete-icon[data-v-248c1b1c] {\n      color: #f83636;\n      font-size: 24px;\n      padding: 5px 0;\n      background-color: transparent;\n      border: none;\n}\n.white-box .proj-block-wrap .delete-icon[data-v-248c1b1c]:hover {\n        border: none;\n        outline: none;\n}\n.white-box .proj-block-wrap .delete-icon[data-v-248c1b1c]:focus {\n        border: none;\n        outline: none;\n}\n@media screen and (max-width: 1200px) {\n.view-proj-btn[data-v-248c1b1c] {\n    float: left;\n    max-width: 150px;\n}\n.white-box .proj-block-wrap .border-xl-right[data-v-248c1b1c] {\n    border-right: none;\n}\n.white-box .proj-block-wrap .edit-icon[data-v-248c1b1c], .white-box .proj-block-wrap .delete-icon[data-v-248c1b1c] {\n    line-height: 14px;\n    padding: 0;\n}\n}\n@media screen and (max-width: 767px) {\n.page-title[data-v-248c1b1c] {\n    font-size: 26px;\n}\n.proj-block-wrap[data-v-248c1b1c] {\n    padding: 10px 0 15px;\n}\n.proj-block-wrap .proj-title[data-v-248c1b1c] {\n      font-size: 20px;\n}\n}\n@media screen and (max-width: 575px) {\n.page-title[data-v-248c1b1c] {\n    font-size: 24px;\n    margin-bottom: 15px;\n}\n.proj-block-wrap .proj-title[data-v-248c1b1c] {\n    font-size: 18px;\n}\n.purple-btn[data-v-248c1b1c] {\n    width: 100%;\n    max-width: 185px;\n    font-size: 14px;\n    padding: 3px 6px;\n    vertical-align: top;\n}\n.view-proj-btn[data-v-248c1b1c] {\n    max-width: 150px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-df105306\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/test-project/test_project_list.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.page-title[data-v-df105306] {\n  font-size: 30px;\n  color: #363e48;\n  font-family: \"UniNeueBold\";\n}\n.text-yellow[data-v-df105306] {\n  color: #ffcc00;\n}\n.text-orange[data-v-df105306] {\n  color: #ff8a0e;\n}\n.text-green[data-v-df105306] {\n  color: #02c505;\n}\n.text-darkgreen[data-v-df105306] {\n  color: #2cac3d;\n}\n.text-red[data-v-df105306] {\n  color: #f83636;\n}\n.text-bold[data-v-df105306] {\n  font-family: \"BrandonTextBold\";\n}\n.text-medium[data-v-df105306] {\n  font-family: \"BrandonTextMedium\";\n}\n.purple-btn[data-v-df105306] {\n  background-color: #5651b9;\n  border: 2px solid #3e3a94;\n  color: #fff;\n  font-size: 15px;\n  line-height: 16px;\n  font-family: \"BrandonTextMedium\";\n  border-radius: 25px;\n  padding: 4px 6px;\n  -webkit-transition: all 0.4s ease;\n  transition: all 0.4s ease;\n}\n.purple-btn[data-v-df105306]:hover {\n    outline: none !important;\n    text-decoration: underline !important;\n}\n.purple-btn[data-v-df105306]:focus {\n    outline: none !important;\n}\n.white-box[data-v-df105306] {\n  background-color: #fff;\n  -webkit-box-shadow: 1px 1px 1px 1px rgba(0, 0, 0, 0.1);\n          box-shadow: 1px 1px 1px 1px rgba(0, 0, 0, 0.1);\n  border-radius: 4px;\n  font-family: \"BrandonTextMedium\";\n  font-size: 14px;\n  color: #606368;\n  padding: 15px;\n}\n.white-box .proj-block-wrap[data-v-df105306] {\n    padding: 15px 0;\n    font-family: \"BrandonTextRegular\";\n    font-size: 14px;\n    color: #606368;\n    position: relative;\n    border-bottom: 1px solid #dadada;\n}\n.white-box .proj-block-wrap[data-v-df105306]:first-child {\n      padding-top: 0;\n}\n.white-box .proj-block-wrap[data-v-df105306]:last-child {\n      padding-bottom: 0;\n      border: none;\n}\n.white-box .proj-block-wrap span[data-v-df105306] {\n      vertical-align: top;\n}\n.white-box .proj-block-wrap .border-xl-right[data-v-df105306] {\n      border-right: 1px solid #dadada;\n}\n.white-box .proj-block-wrap .proj-title[data-v-df105306] {\n      font-family: \"BrandonTextBold\";\n      font-size: 16px;\n      color: #363e48;\n}\n.white-box .proj-block-wrap .edit-icon[data-v-df105306] {\n      color: #3e3a94;\n      font-size: 24px;\n      padding: 5px 0;\n      background-color: transparent;\n      border: none;\n}\n.white-box .proj-block-wrap .edit-icon[data-v-df105306]:hover {\n        border: none;\n        outline: none;\n}\n.white-box .proj-block-wrap .edit-icon[data-v-df105306]:focus {\n        border: none;\n        outline: none;\n}\n.white-box .proj-block-wrap .delete-icon[data-v-df105306] {\n      color: #f83636;\n      font-size: 24px;\n      padding: 5px 0;\n      background-color: transparent;\n      border: none;\n}\n.white-box .proj-block-wrap .delete-icon[data-v-df105306]:hover {\n        border: none;\n        outline: none;\n}\n.white-box .proj-block-wrap .delete-icon[data-v-df105306]:focus {\n        border: none;\n        outline: none;\n}\n@media screen and (max-width: 1200px) {\n.view-proj-btn[data-v-df105306] {\n    float: left;\n    max-width: 150px;\n}\n.white-box .proj-block-wrap .border-xl-right[data-v-df105306] {\n    border-right: none;\n}\n.white-box .proj-block-wrap .edit-icon[data-v-df105306], .white-box .proj-block-wrap .delete-icon[data-v-df105306] {\n    line-height: 14px;\n    padding: 0;\n}\n}\n@media screen and (max-width: 767px) {\n.page-title[data-v-df105306] {\n    font-size: 26px;\n}\n.proj-block-wrap[data-v-df105306] {\n    padding: 10px 0 15px;\n}\n.proj-block-wrap .proj-title[data-v-df105306] {\n      font-size: 20px;\n}\n}\n@media screen and (max-width: 575px) {\n.page-title[data-v-df105306] {\n    font-size: 24px;\n    margin-bottom: 15px;\n}\n.proj-block-wrap .proj-title[data-v-df105306] {\n    font-size: 18px;\n}\n.purple-btn[data-v-df105306] {\n    width: 100%;\n    max-width: 185px;\n    font-size: 14px;\n    padding: 3px 6px;\n    vertical-align: top;\n}\n.view-proj-btn[data-v-df105306] {\n    max-width: 150px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-0d653902\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/device-selector.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    _vm._l(_vm.availableDevices, function(device) {
      return _c("span", { staticClass: "d-inline-block pr-3" }, [
        _c("img", {
          staticClass: "img-fluid",
          class: {
            selected: _vm.value.indexOf(device.id) !== -1,
            disabled: _vm.disabled
          },
          attrs: { title: device.name, src: device.icon },
          on: {
            click: function($event) {
              _vm.toggleItem(device)
            }
          }
        })
      ])
    })
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0d653902", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-248c1b1c\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/admin/testproject_item.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "proj-block-wrap" }, [
    _c("div", { staticClass: "row align-items-center" }, [
      _c("div", { staticClass: "col-md-12 col-xl-10 border-xl-right" }, [
        _c("div", { staticClass: "row" }, [
          _c(
            "div",
            { staticClass: "col-12 mb-2" },
            [
              _c(
                "router-link",
                {
                  staticClass: "proj-title text-uppercase",
                  attrs: {
                    to: {
                      name: "admin.project.view",
                      params: { id: _vm.project.id }
                    }
                  }
                },
                [_vm._v(_vm._s(_vm.project.name))]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-6 text-left" }, [
            _c("div", { staticClass: "mb-2" }, [
              _c(
                "span",
                { staticClass: "text-bold mr-1 d-block d-sm-inline-block" },
                [_vm._v("Devices : ")]
              ),
              _vm._v(" "),
              _c(
                "span",
                { staticClass: "d-block d-sm-inline-block" },
                [
                  _c("device-selector", {
                    attrs: { disabled: "" },
                    model: {
                      value: _vm.selected_devices,
                      callback: function($$v) {
                        _vm.selected_devices = $$v
                      },
                      expression: "selected_devices"
                    }
                  })
                ],
                1
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "mb-2" }, [
              _c(
                "span",
                { staticClass: "text-bold mr-1 d-block d-sm-inline-block" },
                [_vm._v("Type : ")]
              ),
              _vm._v(" "),
              _c("span", { staticClass: "d-block d-sm-inline-block" }, [
                _vm._v(
                  "\n                        " +
                    _vm._s(_vm.project.project_type.name) +
                    "\n                    "
                )
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-6 text-md-right text-left" }, [
            _c("div", { staticClass: "mb-2" }, [
              _c(
                "span",
                { staticClass: "text-bold mr-1 d-block d-sm-inline-block" },
                [_vm._v("Created on : ")]
              ),
              _vm._v(" "),
              _c("span", { staticClass: "d-block d-sm-inline-block" }, [
                _vm._v(_vm._s(_vm._f("date")(_vm.project.created_at)))
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "col-md-12 col-xl-2 btn-wrap" },
        [
          _c(
            "router-link",
            {
              staticClass: "btn purple-btn d-xl-block view-proj-btn",
              attrs: {
                to: {
                  name: "admin.test_project.view",
                  params: { id: _vm.project.id }
                }
              }
            },
            [_vm._v("View Project")]
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "d-xl-block text-xl-center text-right" },
            [
              _c(
                "router-link",
                {
                  staticClass: "btn edit-icon mr-xl-0 mr-1",
                  attrs: {
                    to: {
                      name: "admin.test_project.edit",
                      params: { id: _vm.project.id }
                    }
                  }
                },
                [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: __webpack_require__("./resources/assets/assets/img/edit_18x18.png") }
                  })
                ]
              ),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "btn delete-icon",
                  attrs: { href: "javascript:;" },
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      _vm.deleteTestProject(_vm.project.id)
                    }
                  }
                },
                [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: __webpack_require__("./resources/assets/assets/img/bin_18x18.png") }
                  })
                ]
              )
            ],
            1
          )
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-248c1b1c", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-df105306\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/test-project/test_project_list.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-md-12 mb-5" }, [
        _c("div", { staticClass: "row pb-3" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "col-xl-5 col-lg-6 col-12 pl-md-0" }, [
            _c(
              "div",
              {
                staticClass:
                  "pull-md-right float-none mt-0 mt-sm-2 text-center text-md-right"
              },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "btn purple-btn mr-0 mr-sm-2 mb-sm-0 mb-3",
                    attrs: { to: { name: "admin.test_project.moderate" } }
                  },
                  [_vm._v(" View New Tester Submissions ")]
                ),
                _vm._v(" "),
                _c(
                  "router-link",
                  {
                    staticClass: "btn purple-btn",
                    attrs: { to: { name: "admin.test_project.add" } }
                  },
                  [_vm._v(" Create New Test Project ")]
                )
              ],
              1
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12" }, [
            _c(
              "div",
              { staticClass: "white-box" },
              _vm._l(_vm.testprojects, function(testproject) {
                return _c("test-project-item", {
                  key: testproject.id,
                  attrs: { project: testproject },
                  on: { delete: _vm.deleteTestProject }
                })
              })
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-xl-7 col-lg-6 col-12" }, [
      _c("h1", { staticClass: "page-title" }, [_vm._v(" Test Projects")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-df105306", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/device-selector.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/device-selector.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("4b79eeec", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./device-selector.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./device-selector.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-248c1b1c\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/admin/testproject_item.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-248c1b1c\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/admin/testproject_item.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("957d0626", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-248c1b1c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./testproject_item.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-248c1b1c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./testproject_item.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-df105306\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/test-project/test_project_list.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-df105306\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/test-project/test_project_list.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("8e46fca2", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-df105306\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./test_project_list.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-df105306\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./test_project_list.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/assets/img/bin_18x18.png":
/***/ (function(module, exports) {

module.exports = "/images/bin_18x18.png?ab62865331bd6d9e0406f4e24ef6627b";

/***/ }),

/***/ "./resources/assets/assets/img/edit_18x18.png":
/***/ (function(module, exports) {

module.exports = "/images/edit_18x18.png?a324553d85fa702694c7317bc177aef4";

/***/ }),

/***/ "./resources/assets/components/components/admin/testproject_item.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-248c1b1c\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/admin/testproject_item.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/admin/testproject_item.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-248c1b1c\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/admin/testproject_item.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-248c1b1c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\admin\\testproject_item.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-248c1b1c", Component.options)
  } else {
    hotAPI.reload("data-v-248c1b1c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/components/device-selector.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/device-selector.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/device-selector.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-0d653902\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/device-selector.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-0d653902"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\device-selector.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0d653902", Component.options)
  } else {
    hotAPI.reload("data-v-0d653902", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/pages/test-project/test_project_list.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-df105306\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/test-project/test_project_list.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/test-project/test_project_list.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-df105306\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/test-project/test_project_list.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-df105306"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\test-project\\test_project_list.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-df105306", Component.options)
  } else {
    hotAPI.reload("data-v-df105306", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});