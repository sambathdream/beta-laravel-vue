webpackJsonp([66],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/forgotpassword.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__("./node_modules/vue/dist/vue.common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_form__ = __webpack_require__("./node_modules/vue-form/dist/vue-form.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_form___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_vue_form__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_src_validations_validations_js__ = __webpack_require__("./resources/assets/validations/validations.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




__WEBPACK_IMPORTED_MODULE_0_vue___default.a.use(__WEBPACK_IMPORTED_MODULE_1_vue_form___default.a, __WEBPACK_IMPORTED_MODULE_2_src_validations_validations_js__["a" /* default */]);
/* harmony default export */ __webpack_exports__["default"] = ({
    name: "forgetpassword",
    data: function data() {
        return {
            formstate: {},
            model: {
                email: ""
            }
        };
    },

    methods: {
        onSubmit: function onSubmit() {
            if (this.formstate.$invalid) {
                return;
            } else {
                this.$router.push("/reset_password");
            }
        }
    },
    mounted: function mounted() {},
    destroyed: function destroyed() {}
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-334726a8\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/forgotpassword.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.login_bg[data-v-334726a8] {\n  background-color: #eff9fe;\n  background-size: 100%;\n  min-height: 100vh !important;\n}\n.login_bg .row.align-items-center[data-v-334726a8] {\n    height: 100vh;\n}\n.login_bg .login-top-part[data-v-334726a8] {\n    padding: 5px 50px 0px;\n}\n.login_bg .login-content[data-v-334726a8] {\n    margin: 100px auto;\n    -webkit-box-shadow: 0px 0px 5px 5px rgba(0, 0, 0, 0.03);\n            box-shadow: 0px 0px 5px 5px rgba(0, 0, 0, 0.03);\n    background-size: 100% 100%;\n    border-radius: 7px;\n    background-color: #fff;\n}\n.login_bg .login-content .login-logo[data-v-334726a8] {\n      max-width: 170px;\n      margin-bottom: 20px;\n}\n.login_bg .login-content a[data-v-334726a8] {\n      color: #2cac3d;\n      text-decoration: underline !important;\n      margin-bottom: 40px;\n      font-size: 15px;\n}\n.login_bg .user-message[data-v-334726a8] {\n    padding: 15px 0;\n    font-size: 14px;\n    color: #777;\n    text-align: center;\n}\n.login_bg .form-control[data-v-334726a8] {\n    border-radius: 0px !important;\n}\n.login_bg .custom-form-group[data-v-334726a8] {\n    position: relative;\n    margin-bottom: 25px;\n}\n.login_bg .custom-form-group .custom-form-control[data-v-334726a8] {\n      background-color: transparent !important;\n      border: none;\n      border-radius: none !important;\n      border-bottom: 1px solid #c9c9c9;\n      font-size: 18px;\n      color: #b7b7b7;\n      width: 100%;\n      padding-right: 35px;\n      position: relative;\n}\n.login_bg .custom-form-group .custom-form-control[data-v-334726a8]:hover {\n        outline: none !important;\n}\n.login_bg .custom-form-group .custom-form-control[data-v-334726a8]:focus {\n        outline: none !important;\n}\n.login_bg .custom-form-group .input-group-text[data-v-334726a8] {\n      padding: 5px;\n      position: absolute;\n      right: 0px;\n      top: 3px;\n}\n.login_bg .green-btn[data-v-334726a8] {\n    width: 100%;\n    padding: 7px 10px;\n    border: 2px solid #2cac3d;\n    background-color: #2cac3d;\n    font-size: 15px;\n    font-family: \"BrandonTextMedium\";\n    letter-spacing: 0px;\n    color: #fff;\n    border-radius: 20px;\n    display: block;\n    text-align: center;\n    margin: 20px auto;\n    max-width: 230px;\n}\n.login_bg .green-btn[data-v-334726a8]:hover {\n      background-color: #158f25;\n}\n@media screen and (max-width: 1200px) {\n.login_bg .login-top-part[data-v-334726a8] {\n    padding: 15px 35px 20px;\n}\n.login_bg .green-btn[data-v-334726a8] {\n    margin: 5px auto;\n}\n.login_bg .custom-form-group .custom-form-control[data-v-334726a8] {\n    font-size: 16px;\n}\n.login_bg .custom-form-control[data-v-334726a8]::-webkit-input-placeholder {\n    /* Chrome/Opera/Safari */\n    font-size: 16px;\n}\n.login_bg .custom-form-control[data-v-334726a8]::-moz-placeholder {\n    /* Firefox 19+ */\n    font-size: 16px;\n}\n.login_bg .custom-form-control[data-v-334726a8]:-ms-input-placeholder {\n    /* IE 10+ */\n    font-size: 16px;\n}\n.login_bg .custom-form-control[data-v-334726a8]:-moz-placeholder {\n    /* Firefox 18- */\n    font-size: 16px;\n}\n}\n@media screen and (max-width: 767px) {\n.login_bg .login-content[data-v-334726a8] {\n    margin: 50px auto;\n}\n.login_bg .login-content .login-logo[data-v-334726a8] {\n      margin-bottom: 0px;\n}\n.login_bg .user-message[data-v-334726a8] {\n    padding: 0 0 15px;\n}\n.login_bg .login-top-part[data-v-334726a8] {\n    padding: 0 15px 15px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-334726a8\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/forgotpassword.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid login_bg" }, [
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        {
          staticClass:
            "col-lg-4 offset-lg-4 col-sm-6 offset-sm-3 col-10 offset-1 p-0 login-content"
        },
        [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "vue-form",
            {
              attrs: { state: _vm.formstate },
              on: {
                submit: function($event) {
                  $event.preventDefault()
                  _vm.onSubmit($event)
                }
              }
            },
            [
              _c("div", { staticClass: "col-lg-12" }, [
                _c("p", { staticClass: "user-message " }, [
                  _vm._v(
                    " Please enter the email you are registered with. An email message will be sent to the email address with further instructions."
                  )
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "form-group custom-form-group" },
                  [
                    _c(
                      "validate",
                      { attrs: { tag: "div" } },
                      [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.model.email,
                              expression: "model.email"
                            }
                          ],
                          staticClass: "form-control custom-form-control",
                          attrs: {
                            name: "email",
                            type: "email",
                            required: "",
                            placeholder: "E-mail"
                          },
                          domProps: { value: _vm.model.email },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(_vm.model, "email", $event.target.value)
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "field-messages",
                          {
                            staticClass: "text-danger",
                            attrs: {
                              name: "email",
                              show: "$invalid && $submitted"
                            }
                          },
                          [
                            _c(
                              "div",
                              { attrs: { slot: "required" }, slot: "required" },
                              [_vm._v("Email is a required field")]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { attrs: { slot: "email" }, slot: "email" },
                              [_vm._v("Email is not valid")]
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c("div", { staticClass: "input-group-text" }, [
                          _c("img", {
                            staticClass: "img-fluid",
                            attrs: { src: __webpack_require__("./resources/assets/assets/img/msg-icon.png") }
                          })
                        ])
                      ],
                      1
                    )
                  ],
                  1
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-12 mt-4" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c("input", {
                    staticClass: "btn green-btn text-uppercase",
                    attrs: { type: "submit", value: "Send Confirmation Mail" }
                  })
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "form-group text-center mb-4" },
                  [
                    _c(
                      "router-link",
                      { attrs: { tag: "a", to: { name: "login" } } },
                      [_vm._v("Back to login")]
                    )
                  ],
                  1
                )
              ])
            ]
          )
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "login-top-part" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-sm-12 mt-3" }, [
          _c("h2", { staticClass: "text-center" }, [
            _c("img", {
              staticClass: "img-fluid login-logo",
              attrs: { src: __webpack_require__("./resources/assets/assets/img/home_logo.png"), alt: "The Beta Plan" }
            })
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-334726a8", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-334726a8\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/forgotpassword.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-334726a8\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/forgotpassword.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("5e6149a2", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-334726a8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./forgotpassword.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-334726a8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./forgotpassword.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/components/pages/forgotpassword.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-334726a8\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/forgotpassword.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/forgotpassword.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-334726a8\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/forgotpassword.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-334726a8"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\forgotpassword.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-334726a8", Component.options)
  } else {
    hotAPI.reload("data-v-334726a8", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});