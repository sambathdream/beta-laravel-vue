webpackJsonp([39,56],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/tester/nda-policy.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__("./node_modules/vue/dist/vue.common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_src_services_auth_js__ = __webpack_require__("./resources/assets/services/auth.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



var VueScrollTo = __webpack_require__("./node_modules/vue-scrollto/vue-scrollto.js");
__WEBPACK_IMPORTED_MODULE_0_vue___default.a.use(VueScrollTo, {
  container: "body",
  duration: 500,
  easing: "ease"
});

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "nda-policy",
  data: function data() {
    return {
      isApprovedTester: Object(__WEBPACK_IMPORTED_MODULE_1_src_services_auth_js__["d" /* isApprovedTester */])()
    };
  },

  methods: {
    onAgree: function onAgree() {
      localStorage.setItem("nda-agree", 1);
      this.$emit("done");
      if (this.isApprovedTester) this.$router.push({ name: "tester.tester-profile" });else this.$router.push({ name: "tester.step1" });
    },
    ondisAgree: function ondisAgree() {
      localStorage.setItem("nda-agree", 0);
      this.$emit("done");
      this.$router.push({ name: "tester.step1" });
    }
  },
  mounted: function mounted() {
    VueScrollTo.scrollTo('body');
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/tester_profile.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__("./node_modules/vue/dist/vue.common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_form__ = __webpack_require__("./node_modules/vue-form/dist/vue-form.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_form___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_vue_form__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_src_validations_validations_js__ = __webpack_require__("./resources/assets/validations/validations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_src_services_tester__ = __webpack_require__("./resources/assets/services/tester.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_vue_js_toggle_button__ = __webpack_require__("./node_modules/vue-js-toggle-button/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_vue_js_toggle_button___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_vue_js_toggle_button__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_pages_tester_nda_policy__ = __webpack_require__("./resources/assets/components/pages/tester/nda-policy.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_pages_tester_nda_policy___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_pages_tester_nda_policy__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//







__WEBPACK_IMPORTED_MODULE_0_vue___default.a.use(__WEBPACK_IMPORTED_MODULE_1_vue_form___default.a, __WEBPACK_IMPORTED_MODULE_2_src_validations_validations_js__["a" /* default */]);
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "tester_profile",
  data: function data() {
    return {
      header: "Profile",
      user: {},
      showNda: false,
      formstate: {},
      model: {},
      aDevices: {},
      aCountries: {},
      additional_devices: [],
      device_map: {},
      policy_agree: false,
      userdevice_flag: false,
      previewUrl: "/images/device_no_icon.png"
    };
  },
  mounted: function mounted() {
    var _this = this;

    if (this.$route.params.id) {
      axios.get("/api/tester/" + this.$route.params.id).then(function (_ref) {
        var data = _ref.data.data;

        _this.user = data;
        _this.header = _this.user.first_name + "'s Profile";
      }).catch(function () {
        return _this.$router.push({ name: "admin.tester" });
      });
      axios.get("/api/devices").then(function (response) {
        _this.aDevices = response.data;
        _this.aDevices.forEach(function (d) {
          return _this.device_map[d.id] = false;
        });
        _this.user.devices.forEach(function (f) {
          return _this.device_map[f.id] = true;
        });
      }).then(function (_ref2) {
        var data = _ref2.data.data;
        return _this.assignData(data);
      }).catch(function (error) {});
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-34658397\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/nda-policy.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.custom-container[data-v-34658397] {\n  max-width: 100%;\n}\n.custom-container .step-page-title[data-v-34658397] {\n    font-family: \"UniNeueBold\";\n    font-size: 30px;\n    width: 100%;\n    text-align: center;\n    color: #363e48;\n    margin: 5px 0 25px;\n}\n.custom-container .nda-policy-page[data-v-34658397] {\n    width: 100%;\n}\n.custom-container .nda-policy-page .white-box[data-v-34658397] {\n      background-color: #fff;\n      -webkit-box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n              box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n      border-radius: 4px;\n      padding: 20px;\n      font-size: 17px;\n      line-height: 20px;\n      color: #606368;\n}\n.custom-container .nda-policy-page .white-box .agree-btn-group[data-v-34658397] {\n        width: 100%;\n        border-top: 1px solid #dadada;\n}\n.custom-container .nda-policy-page .white-box .agree-btn-group .green-btn[data-v-34658397] {\n          min-width: 130px;\n          padding: 7px 10px;\n          border: 2px solid #118921;\n          background-color: #2cac3d;\n          font-size: 14px;\n          font-family: \"BrandonTextMedium\";\n          letter-spacing: 0.05rem;\n          color: #fff;\n          margin: 15px auto 0;\n          border-radius: 20px;\n          display: inline-block;\n          margin-right: 10px;\n}\n.custom-container .nda-policy-page .white-box .agree-btn-group .green-btn[data-v-34658397]:hover {\n            background-color: #158f25;\n}\n.custom-container .nda-policy-page .white-box .agree-btn-group .red-btn[data-v-34658397] {\n          min-width: 130px;\n          padding: 7px 10px;\n          border: 2px solid #940000;\n          background-color: #e80000;\n          font-size: 14px;\n          font-family: \"BrandonTextMedium\";\n          letter-spacing: 0.05rem;\n          color: #fff;\n          margin: 15px auto 0;\n          border-radius: 20px;\n          display: inline-block;\n}\n.custom-container .nda-policy-page .white-box .agree-btn-group .red-btn[data-v-34658397]:hover {\n            background-color: #d41111;\n}\n@media screen and (max-width: 1281px) {\n.custom-container .nda-policy-page .white-box[data-v-34658397] {\n    font-size: 16px;\n}\n}\n@media screen and (max-width: 1200px) {\n.custom-container .step-page-title[data-v-34658397] {\n    font-size: 28px;\n    margin: 0 0 20px;\n}\n.custom-container .nda-policy-page .white-box[data-v-34658397] {\n    font-size: 15px;\n}\n}\n@media screen and (max-width: 991px) {\n.custom-container .nda-policy-page .white-box[data-v-34658397] {\n    padding: 15px;\n}\n}\n@media screen and (max-width: 575px) {\n.custom-container .step-page-title[data-v-34658397] {\n    font-size: 26px;\n}\n.custom-container .nda-policy-page .white-box[data-v-34658397] {\n    font-size: 14px;\n    line-height: 18px;\n}\n.custom-container .nda-policy-page .white-box .agree-btn-group .green-btn[data-v-34658397],\n    .custom-container .nda-policy-page .white-box .agree-btn-group .red-btn[data-v-34658397] {\n      min-width: 105px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-525c2493\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester_profile.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.vue-js-switch[data-v-525c2493] {\n  float: right;\n}\n.user_image[data-v-525c2493] {\n  font-size: 10px;\n}\n.input[data-v-525c2493] {\n  width: 100% !important;\n}\n.custom-county-block[data-v-525c2493] {\n  width: 25%;\n}\n.text-bold[data-v-525c2493] {\n  font-family: \"BrandonTextBold\";\n}\n.custom-container[data-v-525c2493] {\n  max-width: 100%;\n}\n.custom-container .step-page-title[data-v-525c2493] {\n    font-family: \"UniNeueBold\";\n    font-size: 30px;\n    width: 100%;\n    text-align: center;\n    color: #363e48;\n    margin: 5px 0 25px;\n}\n.custom-container .tester-profile-page[data-v-525c2493] {\n    width: 100%;\n}\n.custom-container .tester-profile-page .other-input[data-v-525c2493] {\n      min-width: 150px;\n}\n.custom-container .tester-profile-page .toggle-btn-group[data-v-525c2493] {\n      width: calc(100% - 115px);\n}\n.custom-container .tester-profile-page .load-proj-wrap[data-v-525c2493] {\n      background-color: #2cac3d;\n      color: #fff;\n      font-family: \"BrandonTextBold\";\n      border-radius: 4px;\n      width: 100%;\n      text-align: center;\n      padding: 4px 8px;\n      margin-bottom: 20px;\n      font-size: 16px;\n      border: 2px solid #118921;\n}\n.custom-container .tester-profile-page .white-box[data-v-525c2493] {\n      background-color: #fff;\n      -webkit-box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n              box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n      border-radius: 4px;\n      padding: 20px;\n      margin-bottom: 20px;\n      font-size: 17px;\n      color: #606368;\n      letter-spacing: 0;\n}\n.custom-container .tester-profile-page .white-box textarea[data-v-525c2493] {\n        font-size: 15px;\n        resize: none;\n}\n.custom-container .tester-profile-page .white-box .block-title[data-v-525c2493] {\n        font-size: 20px;\n        font-family: \"UniNeueBold\";\n        border-bottom: 1px solid #dadada;\n        color: #2cac3d;\n        padding-bottom: 10px;\n        margin-bottom: 10px;\n}\n.custom-container .tester-profile-page .white-box .img-wrap[data-v-525c2493] {\n        height: 120px;\n        width: 120px;\n        border-radius: 50%;\n        border: 3px solid #1f8e28;\n        background-color: #f0f5f9;\n        vertical-align: middle;\n        margin-right: 15px;\n        padding: 18px 25px;\n        position: relative;\n}\n.custom-container .tester-profile-page .white-box .img-wrap h2[data-v-525c2493] {\n          font-size: 14px;\n          vertical-align: middle;\n          margin-top: 25px;\n          text-align: center;\n}\n.custom-container .tester-profile-page .white-box .img-wrap .choose-img-btn[data-v-525c2493] {\n          opacity: 0;\n          position: absolute;\n          left: 0;\n          top: 0;\n          height: 100%;\n          cursor: pointer;\n}\n.custom-container .tester-profile-page .white-box .content-wrap[data-v-525c2493] {\n        vertical-align: middle;\n}\n.custom-container .tester-profile-page .white-box .content-wrap .username[data-v-525c2493] {\n          font-family: \"UniNeueRegular\";\n          font-size: 25px;\n          color: #363e48;\n          margin-bottom: 0px;\n}\n.custom-container .tester-profile-page .white-box .content-wrap .usermail[data-v-525c2493] {\n          font-size: 15px;\n          color: #73767b;\n          font-family: \"BrandonTextRegular\";\n          margin-bottom: 0;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form[data-v-525c2493] {\n        padding-top: 35px;\n        float: right;\n        width: 70%;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group[data-v-525c2493] {\n          min-width: 80px;\n          margin-right: 20px;\n          vertical-align: top;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group[data-v-525c2493]:last-child {\n            margin-right: 0;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group .form-control[data-v-525c2493]:hover, .custom-container .tester-profile-page .white-box .user-profile-form .form-group .form-control[data-v-525c2493]:focus {\n            outline: none !important;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group label[data-v-525c2493] {\n            display: block;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group .form-check[data-v-525c2493] {\n            display: inline-block;\n            margin-right: 15px;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group .form-check .form-check-input[data-v-525c2493] {\n              margin-left: 0;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group .form-check .form-check-label[data-v-525c2493] {\n              padding-left: 20px;\n}\n.custom-container .tester-profile-page .white-box .detail-form[data-v-525c2493] {\n        width: 100%;\n}\n.custom-container .tester-profile-page .white-box .detail-form span[data-v-525c2493] {\n          max-width: 210px;\n          vertical-align: middle;\n          font-size: 16px;\n          color: #606368;\n          line-height: 20px;\n          margin-right: 15px;\n}\n.custom-container .tester-profile-page .white-box .detail-form .form-control[data-v-525c2493] {\n          width: calc(100% - 90px);\n          float: right;\n          margin-top: 5px;\n}\n.custom-container .tester-profile-page .white-box .detail-form .form-control[data-v-525c2493]:hover {\n            outline: none !important;\n}\n.custom-container .tester-profile-page .white-box .detail-form .form-control[data-v-525c2493]:focus {\n            outline: none !important;\n}\n.custom-container .tester-profile-page .white-box .detail-form .toggle-btn-group[data-v-525c2493] {\n          display: inline-block;\n          vertical-align: inherit;\n          margin-top: 5px;\n          margin-bottom: 0;\n}\n.custom-container .tester-profile-page .white-box .device-name[data-v-525c2493] {\n        font-size: 17px;\n        color: #606368;\n        min-width: 110px;\n        vertical-align: middle;\n        display: inline-block;\n        min-width: 75%;\n}\n.custom-container .tester-profile-page .white-box .selected-txt[data-v-525c2493] {\n        display: inline-block;\n        vertical-align: middle;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group[data-v-525c2493] {\n        display: inline-block;\n        vertical-align: middle;\n        margin-bottom: 10px;\n        margin-top: 10px;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .green-btn[data-v-525c2493] {\n          background-color: #2cac3d;\n          font-size: 17px;\n          font-family: \"BrandonTextMedium\";\n          letter-spacing: 0.05rem;\n          color: #fff;\n          border-radius: 4px;\n          padding: 0px 10px;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .green-btn[data-v-525c2493]:hover {\n            background-color: #158f25;\n            outline: none !important;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .green-btn[data-v-525c2493]:focus {\n            outline: none !important;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .white-btn[data-v-525c2493] {\n          background-color: #fff;\n          border: 1px solid #e4e4e4;\n          font-size: 17px;\n          font-family: \"BrandonTextRegular\";\n          letter-spacing: 0.05rem;\n          color: #606368;\n          border-radius: 4px;\n          padding: 0px 10px;\n          margin-left: 5px;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .white-btn[data-v-525c2493]:hover {\n            background-color: #e4e4e4;\n            outline: none !important;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .white-btn[data-v-525c2493]:focus {\n            outline: none !important;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .form-control[data-v-525c2493] {\n          vertical-align: middle;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .form-control[data-v-525c2493]:hover, .custom-container .tester-profile-page .white-box .toggle-btn-group .form-control[data-v-525c2493]:focus {\n            outline: none !important;\n}\n.custom-container .tester-profile-page .white-box .policy-text[data-v-525c2493] {\n        margin-bottom: 5px;\n}\n.custom-container .tester-profile-page .white-box .policy-text a[data-v-525c2493] {\n          color: #2cac3d;\n          font-family: \"BrandonTextBold\";\n          font-size: 15px;\n          text-decoration: underline !important;\n}\n.custom-container .tester-profile-page .white-box .agreed-txt[data-v-525c2493] {\n        font-size: 15px;\n        color: #2cac3d;\n}\n.custom-container .tester-profile-page .green-step-btn[data-v-525c2493] {\n      width: 100%;\n      max-width: 130px;\n      padding: 7px 10px;\n      border: 2px solid #118921;\n      background-color: #2cac3d;\n      font-size: 14px;\n      font-family: \"BrandonTextMedium\";\n      letter-spacing: 0.05rem;\n      color: #fff;\n      margin: 15px auto;\n      border-radius: 20px;\n      display: block;\n}\n.custom-container .tester-profile-page .green-step-btn[data-v-525c2493]:hover {\n        background-color: #158f25;\n}\n@media screen and (max-width: 1281px) {\n.custom-container .tester-profile-page .white-box[data-v-525c2493] {\n    font-size: 16px;\n}\n.custom-container .tester-profile-page .white-box textarea[data-v-525c2493] {\n      font-size: 14px;\n}\n.custom-container .tester-profile-page .white-box .img-wrap[data-v-525c2493] {\n      height: 100px;\n      width: 100px;\n      padding: 15px 20px;\n      margin-right: 10px;\n}\n.custom-container .tester-profile-page .white-box .content-wrap .username[data-v-525c2493] {\n      font-size: 22px;\n}\n.custom-container .tester-profile-page .white-box .detail-form span[data-v-525c2493] {\n      max-width: 160px;\n      font-size: 14px;\n      line-height: 18px;\n}\n.custom-container .tester-profile-page .white-box .device-name[data-v-525c2493] {\n      font-size: 16px;\n}\n}\n@media screen and (max-width: 1200px) {\n.custom-container .step-page-title[data-v-525c2493] {\n    font-size: 28px;\n    margin: 0 0 20px;\n}\n.custom-container .tester-profile-page .white-box[data-v-525c2493] {\n    font-size: 15px;\n}\n.custom-container .tester-profile-page .white-box .block-title[data-v-525c2493] {\n      font-size: 18px;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form[data-v-525c2493] {\n      padding-top: 20px;\n      float: left;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group .form-check[data-v-525c2493] {\n        margin-right: 0;\n}\n.custom-container .tester-profile-page .white-box .img-wrap[data-v-525c2493] {\n      height: 80px;\n      width: 80px;\n      padding: 10px 15px;\n}\n.custom-container .tester-profile-page .white-box .detail-form .form-control[data-v-525c2493] {\n      float: left;\n}\n.custom-container .tester-profile-page .white-box .device-name[data-v-525c2493] {\n      min-width: 100%;\n      display: block;\n      font-size: 15px;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group[data-v-525c2493] {\n      display: block;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .green-btn[data-v-525c2493] {\n        font-size: 15px;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .white-btn[data-v-525c2493] {\n        font-size: 15px;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .form-control[data-v-525c2493] {\n        max-width: 100px;\n}\n.custom-container .tester-profile-page .white-box .policy-text a[data-v-525c2493] {\n      font-size: 16px;\n}\n}\n@media screen and (max-width: 991px) {\n.custom-container .tester-profile-page .white-box[data-v-525c2493] {\n    padding: 15px;\n}\n.custom-container .tester-profile-page .white-box .detail-form .form-control[data-v-525c2493] {\n      max-width: 120px;\n      float: none;\n      margin-bottom: 15px;\n      margin-left: 15px;\n}\n.custom-container .tester-profile-page .white-box .detail-form span[data-v-525c2493] {\n      min-height: auto;\n      max-width: 100%;\n}\n.custom-container .tester-profile-page .white-box .detail-form span br[data-v-525c2493] {\n        display: none;\n}\n}\n@media screen and (max-width: 575px) {\n.custom-container .step-page-title[data-v-525c2493] {\n    font-size: 26px;\n}\n.custom-container .tester-profile-page .white-box[data-v-525c2493] {\n    font-size: 14px;\n    line-height: 18px;\n}\n.custom-container .tester-profile-page .white-box .img-wrap[data-v-525c2493] {\n      margin-bottom: 10px;\n}\n.custom-container .tester-profile-page .white-box .detail-form .toggle-btn-group[data-v-525c2493] {\n      margin-bottom: 15px;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form[data-v-525c2493] {\n      width: 100%;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group[data-v-525c2493] {\n        min-width: 100%;\n        margin-right: 0;\n}\n.custom-container .tester-profile-page .white-box .policy-text[data-v-525c2493] {\n      font-size: 14px;\n}\n.custom-container .tester-profile-page .white-box .agree-text[data-v-525c2493] {\n      font-size: 13px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-34658397\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/tester/nda-policy.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "bg-color" }, [
    _c("div", { staticClass: "container custom-container" }, [
      _c("div", { staticClass: "row" }, [
        _c("h1", { staticClass: "step-page-title" }, [
          _vm._v("\n        Our NDA Policy\n      ")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "nda-policy-page" }, [
          _c("div", { staticClass: "white-box" }, [
            _vm._m(0),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-12 text-center" }, [
                _vm.isApprovedTester
                  ? _c("div", { staticClass: "agree-btn-group" }, [
                      _c(
                        "a",
                        {
                          staticClass: "btn text-uppercase green-btn",
                          attrs: { href: "javascript:;" },
                          on: { click: _vm.onAgree }
                        },
                        [_vm._v("Back to Profile")]
                      )
                    ])
                  : _c("div", { staticClass: "agree-btn-group" }, [
                      _c(
                        "a",
                        {
                          staticClass: "btn text-uppercase green-btn",
                          attrs: { href: "javascript:;" },
                          on: { click: _vm.onAgree }
                        },
                        [_vm._v("I Agree")]
                      ),
                      _vm._v(" "),
                      _c(
                        "a",
                        {
                          staticClass: "btn text-uppercase red-btn",
                          attrs: { href: "javascript:;" },
                          on: { click: _vm.ondisAgree }
                        },
                        [_vm._v("I Disagree")]
                      )
                    ])
              ])
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12" }, [
        _c("p", [_vm._v("Confidential/Nondisclosure Agreement")]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            '1. Definition of Confidential Information. For purposes of this Agreement, "Confidential Information" shall include all information or material that has or could have commercial value or other utility in the business in which Disclosing Party is engaged.'
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "2. Exclusions from Confidential Information. Receiving Party's obligations under this Agreement do not extend to information that is: (a) publicly known at the time of disclosure or subsequently becomes publicly known through no fault of the Receiving Party; (b) discovered or created by the Receiving Party before disclosure by Disclosing Party; (c) learned by the Receiving Party through legitimate means other than from the Disclosing Party or Disclosing Party's representatives; or (d) is disclosed by Receiving Party with Disclosing Party's prior written approval."
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "3. Obligations of Receiving Party. Receiving Party shall hold and maintain the Confidential Information in strictest confidence for the sole and exclusive benefit of the Disclosing Party. Receiving Party shall not, without prior written approval of Disclosing Party, use for Receiving Party's own benefit, publish, copy, or otherwise disclose to others, or permit the use by others for their benefit or to the detriment of Disclosing Party, any Confidential Information. Receiving Party shall return to Disclosing Party any and all records, notes, and other written, printed, or tangible materials in its possession pertaining to Confidential Information."
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "4. Time Periods. The nondisclosure provisions of this Agreement shall survive the termination of this Agreement and Receiving Party's duty to hold Confidential Information in confidence shall remain in effect until the Confidential Information no longer qualifies as a trade secret or until Disclosing Party sends Receiving Party written notice releasing Receiving Party from this Agreement, whichever occurs first."
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "5. Relationships. Nothing contained in this Agreement shall be deemed to constitute either party a partner, joint venture or employee of the other party for any purpose."
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "6. Integration. This Agreement expresses the complete understanding of the parties with respect to the subject matter and supersedes all prior proposals, agreements, representations, and understandings. This Agreement may not be amended except in a writing signed by both parties."
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "8. Waiver. The failure to exercise any right provided in this Agreement shall not be a waiver of prior or subsequent rights.\n              This Agreement and each party's obligations shall be binding on the representatives, assigns, and successors of such party."
          )
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-34658397", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-525c2493\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/tester_profile.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "bg-color" }, [
      _c("div", { staticClass: "container custom-container" }, [
        _c("div", { staticClass: "row" }, [
          _c("h1", { staticClass: "step-page-title" }, [
            _vm._v("\n                    Profile\n                ")
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "tester-profile-page" }, [
            _c("div", { staticClass: "white-box" }, [
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-xl-5 col-12" }, [
                  _c(
                    "div",
                    { staticClass: "img-wrap d-block d-sm-inline-block" },
                    [
                      _c("img", {
                        staticClass: "img-fluid",
                        attrs: {
                          src: _vm.user.profile
                            ? _vm.user.profile
                            : _vm.previewUrl
                        }
                      })
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "content-wrap d-block d-sm-inline-block" },
                    [
                      _c("h4", { staticClass: "username" }, [
                        _vm._v(
                          _vm._s(_vm.user.first_name) +
                            " " +
                            _vm._s(_vm.user.last_name)
                        )
                      ]),
                      _vm._v(" "),
                      _c("p", { staticClass: "usermail" }, [
                        _vm._v(_vm._s(_vm.user.email))
                      ])
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-xl-7 col-12" }, [
                  _c("div", { staticClass: "user-profile-form" }, [
                    _c(
                      "div",
                      {
                        staticClass:
                          "form-group d-inline-block custom-county-block"
                      },
                      [
                        _c("label", [_vm._v("Country")]),
                        _vm._v(" "),
                        _c("span", { staticClass: "d-block text-bold" }, [
                          _vm._v(
                            "\n                                            " +
                              _vm._s(
                                _vm.user.country ? _vm.user.country.name : ""
                              ) +
                              "\n                                        "
                          )
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group d-inline-block" }, [
                      _c("label", [_vm._v("Gender")]),
                      _vm._v(" "),
                      _c("span", { staticClass: "d-block text-bold" }, [
                        _vm._v(
                          "\n                                          " +
                            _vm._s(_vm.user.gender) +
                            "\n                                        "
                        )
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group d-inline-block" }, [
                      _c("label", [_vm._v("Birthdate")]),
                      _vm._v(" "),
                      _c("span", { staticClass: "d-block text-bold" }, [
                        _vm._v(
                          "\n                                            " +
                            _vm._s(_vm._f("date")(_vm.user.date_of_birth)) +
                            "\n                                        "
                        )
                      ])
                    ])
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "white-box" }, [
              _c("h4", { staticClass: "block-title" }, [
                _vm._v(
                  "\n                            Basic Information\n                        "
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "detail-form" }, [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-12" }, [
                    _vm._m(0),
                    _vm._v(" "),
                    _c("span", { staticClass: "text-bold d-inline-block" }, [
                      _vm._v(
                        "\n                                        " +
                          _vm._s(_vm.user.mobile_no) +
                          "\n                                    "
                      )
                    ])
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "white-box pb-3" }, [
              _c("h4", { staticClass: "block-title" }, [
                _vm._v(
                  "\n                            Devices you have to test on\n                        "
                )
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "row" },
                _vm._l(_vm.aDevices, function(device, index) {
                  return _c(
                    "div",
                    { staticClass: "col-md-4 col-sm-6 col-12 mb-3" },
                    [
                      _c(
                        "span",
                        {
                          staticClass: "device-name",
                          attrs: { color: "#2cac3d" }
                        },
                        [
                          _vm._v(
                            "\n                                  " +
                              _vm._s(index + 1) +
                              ". " +
                              _vm._s(device.name) +
                              "\n                                "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c("span", { staticClass: "text-bold selected-txt" }, [
                        _vm._v(
                          " " +
                            _vm._s(_vm.device_map[index + 1] ? "yes" : "no") +
                            " "
                        )
                      ])
                    ]
                  )
                })
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "white-box" }, [
              _c("h4", { staticClass: "block-title" }, [
                _vm._v(
                  "\n                            Other Information\n                        "
                )
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "mb-0" }, [
                _vm._v(
                  "\n                            " +
                    _vm._s(_vm.user.additional_information) +
                    "\n                        "
                )
              ])
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-lg-12" }, [
        _c(
          "div",
          { staticClass: "col-md-offset-4 col-md-8 m-t-25" },
          [
            _c(
              "router-link",
              {
                staticClass: "btn btn-primary",
                attrs: { to: { name: "admin.tester" } }
              },
              [_vm._v("Back to Testers")]
            )
          ],
          1
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "d-inline-block " }, [
      _vm._v("\n                                      Cell "),
      _c("br"),
      _vm._v(" Number :\n                                    ")
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-525c2493", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-scrollto/vue-scrollto.js":
/***/ (function(module, exports, __webpack_require__) {

(function (global, factory) {
	 true ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(global['vue-scrollto'] = factory());
}(this, (function () { 'use strict';

/**
 * https://github.com/gre/bezier-easing
 * BezierEasing - use bezier curve for transition easing function
 * by Gaëtan Renaudeau 2014 - 2015 – MIT License
 */

// These values are established by empiricism with tests (tradeoff: performance VS precision)
var NEWTON_ITERATIONS = 4;
var NEWTON_MIN_SLOPE = 0.001;
var SUBDIVISION_PRECISION = 0.0000001;
var SUBDIVISION_MAX_ITERATIONS = 10;

var kSplineTableSize = 11;
var kSampleStepSize = 1.0 / (kSplineTableSize - 1.0);

var float32ArraySupported = typeof Float32Array === 'function';

function A (aA1, aA2) { return 1.0 - 3.0 * aA2 + 3.0 * aA1; }
function B (aA1, aA2) { return 3.0 * aA2 - 6.0 * aA1; }
function C (aA1)      { return 3.0 * aA1; }

// Returns x(t) given t, x1, and x2, or y(t) given t, y1, and y2.
function calcBezier (aT, aA1, aA2) { return ((A(aA1, aA2) * aT + B(aA1, aA2)) * aT + C(aA1)) * aT; }

// Returns dx/dt given t, x1, and x2, or dy/dt given t, y1, and y2.
function getSlope (aT, aA1, aA2) { return 3.0 * A(aA1, aA2) * aT * aT + 2.0 * B(aA1, aA2) * aT + C(aA1); }

function binarySubdivide (aX, aA, aB, mX1, mX2) {
  var currentX, currentT, i = 0;
  do {
    currentT = aA + (aB - aA) / 2.0;
    currentX = calcBezier(currentT, mX1, mX2) - aX;
    if (currentX > 0.0) {
      aB = currentT;
    } else {
      aA = currentT;
    }
  } while (Math.abs(currentX) > SUBDIVISION_PRECISION && ++i < SUBDIVISION_MAX_ITERATIONS);
  return currentT;
}

function newtonRaphsonIterate (aX, aGuessT, mX1, mX2) {
 for (var i = 0; i < NEWTON_ITERATIONS; ++i) {
   var currentSlope = getSlope(aGuessT, mX1, mX2);
   if (currentSlope === 0.0) {
     return aGuessT;
   }
   var currentX = calcBezier(aGuessT, mX1, mX2) - aX;
   aGuessT -= currentX / currentSlope;
 }
 return aGuessT;
}

var src = function bezier (mX1, mY1, mX2, mY2) {
  if (!(0 <= mX1 && mX1 <= 1 && 0 <= mX2 && mX2 <= 1)) {
    throw new Error('bezier x values must be in [0, 1] range');
  }

  // Precompute samples table
  var sampleValues = float32ArraySupported ? new Float32Array(kSplineTableSize) : new Array(kSplineTableSize);
  if (mX1 !== mY1 || mX2 !== mY2) {
    for (var i = 0; i < kSplineTableSize; ++i) {
      sampleValues[i] = calcBezier(i * kSampleStepSize, mX1, mX2);
    }
  }

  function getTForX (aX) {
    var intervalStart = 0.0;
    var currentSample = 1;
    var lastSample = kSplineTableSize - 1;

    for (; currentSample !== lastSample && sampleValues[currentSample] <= aX; ++currentSample) {
      intervalStart += kSampleStepSize;
    }
    --currentSample;

    // Interpolate to provide an initial guess for t
    var dist = (aX - sampleValues[currentSample]) / (sampleValues[currentSample + 1] - sampleValues[currentSample]);
    var guessForT = intervalStart + dist * kSampleStepSize;

    var initialSlope = getSlope(guessForT, mX1, mX2);
    if (initialSlope >= NEWTON_MIN_SLOPE) {
      return newtonRaphsonIterate(aX, guessForT, mX1, mX2);
    } else if (initialSlope === 0.0) {
      return guessForT;
    } else {
      return binarySubdivide(aX, intervalStart, intervalStart + kSampleStepSize, mX1, mX2);
    }
  }

  return function BezierEasing (x) {
    if (mX1 === mY1 && mX2 === mY2) {
      return x; // linear
    }
    // Because JavaScript number are imprecise, we should guarantee the extremes are right.
    if (x === 0) {
      return 0;
    }
    if (x === 1) {
      return 1;
    }
    return calcBezier(getTForX(x), mY1, mY2);
  };
};

var easings = {
    ease: [0.25, 0.1, 0.25, 1.0],
    linear: [0.00, 0.0, 1.00, 1.0],
    "ease-in": [0.42, 0.0, 1.00, 1.0],
    "ease-out": [0.00, 0.0, 0.58, 1.0],
    "ease-in-out": [0.42, 0.0, 0.58, 1.0]
};

// https://github.com/WICG/EventListenerOptions/blob/gh-pages/explainer.md#feature-detection
var supportsPassive = false;
try {
    var opts = Object.defineProperty({}, "passive", {
        get: function get() {
            supportsPassive = true;
        }
    });
    window.addEventListener("test", null, opts);
} catch (e) {}

var _ = {
    $: function $(selector) {
        if (typeof selector !== "string") {
            return selector;
        }
        return document.querySelector(selector);
    },
    on: function on(element, events, handler) {
        var opts = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : { passive: false };

        if (!(events instanceof Array)) {
            events = [events];
        }
        for (var i = 0; i < events.length; i++) {
            element.addEventListener(events[i], handler, supportsPassive ? opts : false);
        }
    },
    off: function off(element, events, handler) {
        if (!(events instanceof Array)) {
            events = [events];
        }
        for (var i = 0; i < events.length; i++) {
            element.removeEventListener(events[i], handler);
        }
    },
    cumulativeOffset: function cumulativeOffset(element) {
        var top = 0;
        var left = 0;

        do {
            top += element.offsetTop || 0;
            left += element.offsetLeft || 0;
            element = element.offsetParent;
        } while (element);

        return {
            top: top,
            left: left
        };
    }
};

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
  return typeof obj;
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
};





















var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var abortEvents = ["mousedown", "wheel", "DOMMouseScroll", "mousewheel", "keyup", "touchmove"];

var defaults$$1 = {
    container: "body",
    duration: 500,
    easing: "ease",
    offset: 0,
    cancelable: true,
    onDone: false,
    onCancel: false,
    x: false,
    y: true
};

function setDefaults(options) {
    defaults$$1 = _extends({}, defaults$$1, options);
}

var scroller = function scroller() {
    var element = void 0; // element to scroll to
    var container = void 0; // container to scroll
    var duration = void 0; // duration of the scrolling
    var easing = void 0; // easing to be used when scrolling
    var offset = void 0; // offset to be added (subtracted)
    var cancelable = void 0; // indicates if user can cancel the scroll or not.
    var onDone = void 0; // callback when scrolling is done
    var onCancel = void 0; // callback when scrolling is canceled / aborted
    var x = void 0; // scroll on x axis
    var y = void 0; // scroll on y axis

    var initialX = void 0; // initial X of container
    var targetX = void 0; // target X of container
    var initialY = void 0; // initial Y of container
    var targetY = void 0; // target Y of container
    var diffX = void 0; // difference
    var diffY = void 0; // difference

    var abort = void 0; // is scrolling aborted

    var abortEv = void 0; // event that aborted scrolling
    var abortFn = function abortFn(e) {
        if (!cancelable) return;
        abortEv = e;
        abort = true;
    };
    var easingFn = void 0;

    var timeStart = void 0; // time when scrolling started
    var timeElapsed = void 0; // time elapsed since scrolling started

    var progress = void 0; // progress

    function scrollTop(container) {
        var scrollTop = container.scrollTop;

        if (container.tagName.toLowerCase() === "body") {
            // in firefox body.scrollTop always returns 0
            // thus if we are trying to get scrollTop on a body tag
            // we need to get it from the documentElement
            scrollTop = scrollTop || document.documentElement.scrollTop;
        }

        return scrollTop;
    }

    function scrollLeft(container) {
        var scrollLeft = container.scrollLeft;

        if (container.tagName.toLowerCase() === "body") {
            // in firefox body.scrollLeft always returns 0
            // thus if we are trying to get scrollLeft on a body tag
            // we need to get it from the documentElement
            scrollLeft = scrollLeft || document.documentElement.scrollLeft;
        }

        return scrollLeft;
    }

    function step(timestamp) {
        if (abort) return done();
        if (!timeStart) timeStart = timestamp;

        timeElapsed = timestamp - timeStart;

        progress = Math.min(timeElapsed / duration, 1);
        progress = easingFn(progress);

        topLeft(container, initialY + diffY * progress, initialX + diffX * progress);

        timeElapsed < duration ? window.requestAnimationFrame(step) : done();
    }

    function done() {
        if (!abort) topLeft(container, targetY, targetX);
        timeStart = false;

        _.off(container, abortEvents, abortFn);
        if (abort && onCancel) onCancel(abortEv);
        if (!abort && onDone) onDone();
    }

    function topLeft(element, top, left) {
        if (y) element.scrollTop = top;
        if (x) element.scrollLeft = left;
        if (element.tagName.toLowerCase() === "body") {
            // in firefox body.scrollTop doesn't scroll the page
            // thus if we are trying to scrollTop on a body tag
            // we need to scroll on the documentElement
            if (y) document.documentElement.scrollTop = top;
            if (x) document.documentElement.scrollLeft = left;
        }
    }

    function scrollTo(target, _duration) {
        var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

        if ((typeof _duration === "undefined" ? "undefined" : _typeof(_duration)) === "object") {
            options = _duration;
        } else if (typeof _duration === "number") {
            options.duration = _duration;
        }

        element = _.$(target);

        if (!element) {
            return console.warn("[vue-scrollto warn]: Trying to scroll to an element that is not on the page: " + target);
        }

        container = _.$(options.container || defaults$$1.container);
        duration = options.duration || defaults$$1.duration;
        easing = options.easing || defaults$$1.easing;
        offset = options.offset || defaults$$1.offset;
        cancelable = options.hasOwnProperty("cancelable") ? options.cancelable !== false : defaults$$1.cancelable;
        onDone = options.onDone || defaults$$1.onDone;
        onCancel = options.onCancel || defaults$$1.onCancel;
        x = options.x === undefined ? defaults$$1.x : options.x;
        y = options.y === undefined ? defaults$$1.y : options.y;

        var cumulativeOffsetContainer = _.cumulativeOffset(container);
        var cumulativeOffsetElement = _.cumulativeOffset(element);

        if (typeof offset === "function") {
            offset = offset();
        }

        initialY = scrollTop(container);
        targetY = cumulativeOffsetElement.top - cumulativeOffsetContainer.top + offset;

        initialX = scrollLeft(container);
        targetX = cumulativeOffsetElement.left - cumulativeOffsetContainer.left + offset;

        abort = false;

        diffY = targetY - initialY;
        diffX = targetX - initialX;

        if (typeof easing === "string") {
            easing = easings[easing] || easings["ease"];
        }

        easingFn = src.apply(src, easing);

        if (!diffY && !diffX) return;

        _.on(container, abortEvents, abortFn, { passive: true });

        window.requestAnimationFrame(step);

        return function () {
            abortEv = null;
            abort = true;
        };
    }

    return scrollTo;
};

var _scroller = scroller();

var bindings = []; // store binding data

function deleteBinding(el) {
    for (var i = 0; i < bindings.length; ++i) {
        if (bindings[i].el === el) {
            bindings.splice(i, 1);
            return true;
        }
    }
    return false;
}

function findBinding(el) {
    for (var i = 0; i < bindings.length; ++i) {
        if (bindings[i].el === el) {
            return bindings[i];
        }
    }
}

function getBinding(el) {
    var binding = findBinding(el);

    if (binding) {
        return binding;
    }

    bindings.push(binding = {
        el: el,
        binding: {}
    });

    return binding;
}

function handleClick(e) {
    e.preventDefault();
    var ctx = getBinding(this).binding;

    if (typeof ctx.value === "string") {
        return _scroller(ctx.value);
    }
    _scroller(ctx.value.el || ctx.value.element, ctx.value);
}

var VueScrollTo$1 = {
    bind: function bind(el, binding) {
        getBinding(el).binding = binding;
        _.on(el, "click", handleClick);
    },
    unbind: function unbind(el) {
        deleteBinding(el);
        _.off(el, "click", handleClick);
    },
    update: function update(el, binding) {
        getBinding(el).binding = binding;
    },

    scrollTo: _scroller,
    bindings: bindings
};

var install = function install(Vue, options) {
    if (options) setDefaults(options);
    Vue.directive("scroll-to", VueScrollTo$1);
    Vue.prototype.$scrollTo = VueScrollTo$1.scrollTo;
};

if (typeof window !== "undefined" && window.Vue) {
    window.VueScrollTo = VueScrollTo$1;
    window.VueScrollTo.setDefaults = setDefaults;
    Vue.use(install);
}

VueScrollTo$1.install = install;

return VueScrollTo$1;

})));


/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-34658397\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/nda-policy.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-34658397\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/nda-policy.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("5f79779a", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-34658397\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./nda-policy.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-34658397\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./nda-policy.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-525c2493\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester_profile.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-525c2493\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester_profile.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("70f9a88e", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-525c2493\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./tester_profile.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-525c2493\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./tester_profile.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/components/pages/tester/nda-policy.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-34658397\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/nda-policy.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/tester/nda-policy.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-34658397\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/tester/nda-policy.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-34658397"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\tester\\nda-policy.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-34658397", Component.options)
  } else {
    hotAPI.reload("data-v-34658397", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/pages/tester_profile.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-525c2493\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester_profile.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/tester_profile.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-525c2493\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/tester_profile.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-525c2493"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\tester_profile.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-525c2493", Component.options)
  } else {
    hotAPI.reload("data-v-525c2493", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});