webpackJsonp([67],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/change-password.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__("./node_modules/vue/dist/vue.common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_form__ = __webpack_require__("./node_modules/vue-form/dist/vue-form.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_form___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_vue_form__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "change-password"
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2a87089e\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/change-password.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.text-bold[data-v-2a87089e] {\n  font-family: \"BrandonTextBold\";\n}\n.custom-container[data-v-2a87089e] {\n  max-width: 100%;\n}\n.custom-container .page-title[data-v-2a87089e] {\n    font-family: \"UniNeueBold\";\n    font-size: 30px;\n    width: 100%;\n    text-align: center;\n    color: #363e48;\n    margin: 5px 0 25px;\n}\n.custom-container .change-password-page[data-v-2a87089e] {\n    width: 100%;\n}\n.custom-container .change-password-page .white-box[data-v-2a87089e] {\n      background-color: #fff;\n      -webkit-box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n              box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n      border-radius: 4px;\n      padding: 20px;\n      margin-bottom: 20px;\n      font-size: 17px;\n      color: #606368;\n      letter-spacing: 0;\n}\n.custom-container .change-password-page .white-box .block-title[data-v-2a87089e] {\n        font-size: 20px;\n        font-family: \"UniNeueBold\";\n        border-bottom: 1px solid #dadada;\n        color: #2cac3d;\n        padding-bottom: 10px;\n        margin-bottom: 10px;\n}\n.custom-container .change-password-page .purple-btn[data-v-2a87089e] {\n      background-color: #5651b9;\n      border: 2px solid #3e3a94;\n      color: #fff;\n      font-size: 15px;\n      line-height: 16px;\n      font-family: \"BrandonTextMedium\";\n      border-radius: 25px;\n      padding: 8px 10px;\n      -webkit-transition: all 0.4s ease;\n      transition: all 0.4s ease;\n      margin: 0 auto;\n      display: block;\n      max-width: 200px;\n}\n.custom-container .change-password-page .purple-btn[data-v-2a87089e]:hover {\n        outline: none !important;\n        background-color: #3e3a94;\n}\n.custom-container .change-password-page .purple-btn[data-v-2a87089e]:focus {\n        outline: none !important;\n}\n@media screen and (max-width: 1281px) {\n.custom-container .change-password-page .white-box[data-v-2a87089e] {\n    font-size: 14px;\n}\n}\n@media screen and (max-width: 1200px) {\n.custom-container .page-title[data-v-2a87089e] {\n    font-size: 28px;\n    margin: 0 0 20px;\n}\n.custom-container .change-password-page .white-box[data-v-2a87089e] {\n    font-size: 14px;\n}\n.custom-container .change-password-page .white-box .block-title[data-v-2a87089e] {\n      font-size: 18px;\n}\n}\n@media screen and (max-width: 991px) {\n.custom-container .change-password-page .white-box[data-v-2a87089e] {\n    padding: 15px;\n}\n}\n@media screen and (max-width: 575px) {\n.custom-container .page-title[data-v-2a87089e] {\n    font-size: 26px;\n}\n.custom-container .change-password-page .white-box[data-v-2a87089e] {\n    font-size: 14px;\n    line-height: 18px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-2a87089e\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/change-password.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("div", { staticClass: "container custom-container" }, [
        _c("div", { staticClass: "row" }, [
          _c("h1", { staticClass: "page-title" }, [
            _vm._v("\n        Change Password\n      ")
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "change-password-page" }, [
            _c("div", { staticClass: "white-box" }, [
              _c("div", { staticClass: "row" }, [
                _c(
                  "div",
                  {
                    staticClass:
                      "col-md-6 offset-md-3 col-sm-8 offset-sm-2 col-12"
                  },
                  [
                    _c("div", { staticClass: "form-group custom-form-group" }, [
                      _c("span", { staticClass: "text-bold" }, [
                        _vm._v("Current password")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        staticClass: "form-control custom-form-control mt-1",
                        attrs: { type: "text", placeholder: "Current password" }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group custom-form-group" }, [
                      _c("span", { staticClass: "text-bold" }, [
                        _vm._v("Old password")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        staticClass: "form-control custom-form-control mt-1",
                        attrs: { type: "text", placeholder: "Old password" }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group custom-form-group" }, [
                      _c("span", { staticClass: "text-bold" }, [
                        _vm._v("New Password")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        staticClass: "form-control custom-form-control mt-1",
                        attrs: { type: "text", placeholder: "New password" }
                      })
                    ])
                  ]
                )
              ])
            ]),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "btn text-uppercase text-center purple-btn",
                attrs: { href: "#" }
              },
              [_vm._v("change password")]
            )
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-2a87089e", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2a87089e\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/change-password.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2a87089e\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/change-password.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("6f9fa7a2", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2a87089e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./change-password.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2a87089e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./change-password.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/components/pages/change-password.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2a87089e\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/change-password.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/change-password.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-2a87089e\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/change-password.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-2a87089e"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\change-password.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-2a87089e", Component.options)
  } else {
    hotAPI.reload("data-v-2a87089e", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});