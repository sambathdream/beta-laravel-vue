webpackJsonp([62],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/test-project/moderate_test_projects.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "projects_list",
  data: function data() {
    return {
      testprojects: [],
      aDevices: [],
      model: {},
      fetched: false
    };
  },

  mounted: function mounted() {
    var _this = this;

    this.loadModerateProjects();
    axios.get("/api/devices").then(function (response) {
      _this.aDevices = response.data;
    }).catch(function (error) {});
  },
  methods: {
    loadModerateProjects: function loadModerateProjects() {
      var _this2 = this;

      this.fetched = false;
      axios.get("/api/moderate-projects").then(function (response) {
        _this2.testprojects = response.data;
        _this2.fetched = true;
      }).catch(function (error) {
        _this2.fetched = true;
      });
    },
    changeStatus: function changeStatus(project, status) {
      var _this3 = this;

      return axios.post("/api/test/" + project.id + "/status", {
        status: status
      }).then(function () {
        return _this3.loadModerateProjects();
      });
    },
    acceptProjectSubmission: function acceptProjectSubmission(project) {
      return this.changeStatus(project, "TEST_PASSED");
    },
    rejectProjectSubmission: function rejectProjectSubmission(project) {
      var _this4 = this;

      this.$swal({
        title: "Are you sure?",
        text: "You want to reject this test?",
        icon: "warning",
        buttons: true
      }).then(function (willDelete) {
        if (willDelete) {
          return _this4.changeStatus(project, "TEST_FAILED");
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-62c3a50f\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/test-project/moderate_test_projects.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.page-title[data-v-62c3a50f] {\n  font-size: 30px;\n  color: #363e48;\n  font-family: \"UniNeueBold\";\n}\n.text-red[data-v-62c3a50f] {\n  color: #f83636;\n}\n.text-bold[data-v-62c3a50f] {\n  font-family: \"BrandonTextBold\";\n}\n.text-medium[data-v-62c3a50f] {\n  font-family: \"BrandonTextMedium\";\n}\n.purple-btn[data-v-62c3a50f] {\n  background-color: #5651b9;\n  border: 2px solid #3e3a94;\n  color: #fff;\n  font-size: 15px;\n  line-height: 16px;\n  font-family: \"BrandonTextMedium\";\n  border-radius: 25px;\n  padding: 4px 6px;\n  -webkit-transition: all 0.4s ease;\n  transition: all 0.4s ease;\n  width: 100%;\n}\n.purple-btn[data-v-62c3a50f]:hover {\n    outline: none !important;\n    text-decoration: underline !important;\n}\n.purple-btn[data-v-62c3a50f]:focus {\n    outline: none !important;\n}\n.back-proj-btn[data-v-62c3a50f] {\n  max-width: 200px;\n}\n.white-box[data-v-62c3a50f] {\n  background-color: #fff;\n  -webkit-box-shadow: 1px 1px 1px 1px rgba(0, 0, 0, 0.1);\n          box-shadow: 1px 1px 1px 1px rgba(0, 0, 0, 0.1);\n  border-radius: 4px;\n  font-family: \"BrandonTextMedium\";\n  font-size: 14px;\n  color: #606368;\n  padding: 15px;\n}\n.white-box .proj-block-wrap[data-v-62c3a50f] {\n    padding: 15px 0;\n    font-family: \"BrandonTextRegular\";\n    font-size: 14px;\n    color: #606368;\n    position: relative;\n    border-bottom: 1px solid #dadada;\n}\n.white-box .proj-block-wrap[data-v-62c3a50f]:first-child {\n      padding-top: 0;\n}\n.white-box .proj-block-wrap[data-v-62c3a50f]:last-child {\n      padding-bottom: 0;\n      border: none;\n}\n.white-box .proj-block-wrap span[data-v-62c3a50f] {\n      vertical-align: top;\n}\n.white-box .proj-block-wrap .border-xl-right[data-v-62c3a50f] {\n      border-right: 1px solid #dadada;\n}\n.white-box .proj-block-wrap .proj-title[data-v-62c3a50f] {\n      font-family: \"BrandonTextBold\";\n      font-size: 16px;\n      color: #363e48;\n}\n.white-box .proj-block-wrap .icon-wrap[data-v-62c3a50f] {\n      text-align: center;\n      width: 100%;\n}\n.white-box .proj-block-wrap .accept-icon[data-v-62c3a50f] {\n      color: #3e3a94;\n      font-size: 24px;\n      padding: 0;\n      background-color: transparent;\n      border: none;\n}\n.white-box .proj-block-wrap .accept-icon[data-v-62c3a50f]:hover {\n        border: none;\n        outline: none;\n}\n.white-box .proj-block-wrap .accept-icon[data-v-62c3a50f]:focus {\n        border: none;\n        outline: none;\n}\n.white-box .proj-block-wrap .reject-icon[data-v-62c3a50f] {\n      color: #f83636;\n      font-size: 24px;\n      padding: 0;\n      background-color: transparent;\n      border: none;\n}\n.white-box .proj-block-wrap .reject-icon[data-v-62c3a50f]:hover {\n        border: none;\n        outline: none;\n}\n.white-box .proj-block-wrap .reject-icon[data-v-62c3a50f]:focus {\n        border: none;\n        outline: none;\n}\n@media screen and (max-width: 1200px) {\n.view-proj-btn[data-v-62c3a50f] {\n    float: left;\n    max-width: 150px;\n}\n.white-box .proj-block-wrap[data-v-62c3a50f] {\n    padding: 15px 0 10px;\n}\n.white-box .proj-block-wrap .border-xl-right[data-v-62c3a50f] {\n      border-right: none;\n}\n.white-box .proj-block-wrap .icon-wrap[data-v-62c3a50f] {\n      text-align: right;\n      width: auto;\n      float: right;\n}\n.white-box .proj-block-wrap .btn-wrap .accept-icon[data-v-62c3a50f], .white-box .proj-block-wrap .btn-wrap .reject-icon[data-v-62c3a50f] {\n      line-height: 14px;\n      padding: 0;\n}\n}\n@media screen and (max-width: 767px) {\n.page-title[data-v-62c3a50f] {\n    font-size: 26px;\n    text-align: center;\n}\n.proj-block-wrap[data-v-62c3a50f] {\n    padding: 10px 0 15px;\n}\n.proj-block-wrap .proj-title[data-v-62c3a50f] {\n      font-size: 20px;\n}\n}\n@media screen and (max-width: 575px) {\n.page-title[data-v-62c3a50f] {\n    font-size: 24px;\n    margin-bottom: 15px;\n}\n.proj-block-wrap .proj-title[data-v-62c3a50f] {\n    font-size: 18px;\n}\n.purple-btn[data-v-62c3a50f] {\n    width: 100%;\n    max-width: 185px;\n    font-size: 14px;\n    padding: 3px 6px;\n    vertical-align: top;\n}\n.view-proj-btn[data-v-62c3a50f] {\n    max-width: 150px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-62c3a50f\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/test-project/moderate_test_projects.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "row pb-3" }, [
      _vm._m(0),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-4 col-12 pl-md-0" }, [
        _c(
          "div",
          {
            staticClass:
              "pull-md-right float-none mt-0 mt-sm-2 text-md-right text-center"
          },
          [
            _c(
              "router-link",
              {
                staticClass: "btn purple-btn back-proj-btn",
                attrs: { to: { name: "admin.test_projects" } }
              },
              [_vm._v(" Back To Test Project ")]
            )
          ],
          1
        )
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-md-12 mb-5" }, [
        _c(
          "div",
          { ref: "testprojectList", staticClass: "white-box" },
          [
            _vm.fetched && _vm.testprojects.length === 0
              ? _c("div", [_vm._m(1)])
              : _vm._l(_vm.testprojects, function(testproject) {
                  return _c("div", { staticClass: "proj-block-wrap" }, [
                    _c("div", { staticClass: "row align-items-center" }, [
                      _c(
                        "div",
                        { staticClass: "col-md-12 col-xl-10 border-xl-right" },
                        [
                          _c("div", { staticClass: "row" }, [
                            _c("div", { staticClass: "col-12 mb-2" }, [
                              _c(
                                "span",
                                { staticClass: "proj-title text-uppercase" },
                                [
                                  _vm._v(
                                    "\n                                         " +
                                      _vm._s(testproject.project.name) +
                                      "\n                                     "
                                  )
                                ]
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-6 text-left" }, [
                              _c("div", { staticClass: "mb-2" }, [
                                _c(
                                  "span",
                                  {
                                    staticClass:
                                      "text-bold mr-1 d-block d-sm-inline-block"
                                  },
                                  [_vm._v("Tester Name : ")]
                                ),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  { staticClass: "d-block d-sm-inline-block" },
                                  [
                                    _vm._v(
                                      "\n                                            " +
                                        _vm._s(testproject.tester.first_name) +
                                        " " +
                                        _vm._s(testproject.tester.last_name) +
                                        "\n                                        "
                                    )
                                  ]
                                )
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "mb-2" }, [
                                _c(
                                  "span",
                                  {
                                    staticClass:
                                      "text-bold mr-1 d-block d-sm-inline-block"
                                  },
                                  [_vm._v("Tester Email : ")]
                                ),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  { staticClass: "d-block d-sm-inline-block" },
                                  [
                                    _vm._v(
                                      "\n                                            " +
                                        _vm._s(testproject.tester.email) +
                                        "\n                                        "
                                    )
                                  ]
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass: "col-md-6 text-md-right text-left"
                              },
                              [
                                _c("div", { staticClass: "mb-2" }, [
                                  _c(
                                    "span",
                                    {
                                      staticClass:
                                        "text-bold mr-1 d-block d-sm-inline-block"
                                    },
                                    [_vm._v("Type : ")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    {
                                      staticClass: "d-block d-sm-inline-block"
                                    },
                                    [
                                      _vm._v(
                                        "\n                                           " +
                                          _vm._s(
                                            testproject.project.project_type
                                              .name
                                          ) +
                                          "\n                                        "
                                      )
                                    ]
                                  )
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "mb-2" }, [
                                  _c(
                                    "span",
                                    {
                                      staticClass:
                                        "text-bold mr-1 d-block d-sm-inline-block"
                                    },
                                    [_vm._v("Submission Date : ")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    {
                                      staticClass: "d-block d-sm-inline-block"
                                    },
                                    [
                                      _vm._v(
                                        "\n                                           " +
                                          _vm._s(
                                            _vm._f("date")(
                                              testproject.updated_at
                                            )
                                          ) +
                                          "\n                                        "
                                      )
                                    ]
                                  )
                                ])
                              ]
                            )
                          ])
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-md-12 col-xl-2 btn-wrap" },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass:
                                "btn purple-btn d-inline-block view-proj-btn",
                              attrs: {
                                to: {
                                  name: "admin.test_project.view-submission",
                                  params: { id: testproject.id }
                                }
                              }
                            },
                            [_vm._v("View Project")]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "icon-wrap d-inline-block" },
                            [
                              _c(
                                "a",
                                {
                                  staticClass: "btn accept-icon",
                                  attrs: { href: "javascript:;" },
                                  on: {
                                    click: function($event) {
                                      _vm.acceptProjectSubmission(testproject)
                                    }
                                  }
                                },
                                [_c("i", { staticClass: "fa fa-check" })]
                              ),
                              _vm._v(" "),
                              _c(
                                "a",
                                {
                                  staticClass: "btn reject-icon",
                                  attrs: { href: "javascript:;" },
                                  on: {
                                    click: function($event) {
                                      $event.preventDefault()
                                      _vm.rejectProjectSubmission(testproject)
                                    }
                                  }
                                },
                                [_c("i", { staticClass: "fa fa-times" })]
                              )
                            ]
                          )
                        ],
                        1
                      )
                    ])
                  ])
                })
          ],
          2
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-8 col-12" }, [
      _c("h1", { staticClass: "page-title" }, [
        _vm._v(" Moderate Test Projects")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-md-12 pb-3" }, [
        _c("h3", [_vm._v("No test have been submitted")])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-62c3a50f", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-62c3a50f\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/test-project/moderate_test_projects.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-62c3a50f\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/test-project/moderate_test_projects.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("d2e68734", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-62c3a50f\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./moderate_test_projects.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-62c3a50f\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./moderate_test_projects.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/components/pages/test-project/moderate_test_projects.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-62c3a50f\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/test-project/moderate_test_projects.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/test-project/moderate_test_projects.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-62c3a50f\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/test-project/moderate_test_projects.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-62c3a50f"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\test-project\\moderate_test_projects.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-62c3a50f", Component.options)
  } else {
    hotAPI.reload("data-v-62c3a50f", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});