webpackJsonp([73,56],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./node_modules/vue-tagsinput/src/input.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vuept__ = __webpack_require__("./node_modules/vuept/dist/vuept.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vuept___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vuept__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__lib__ = __webpack_require__("./node_modules/vue-tagsinput/src/lib.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    /**
     * Array<{
     *   text: String,
     *   readOnly: Boolean,
     *   invalid: Boolean
     * }>
     */
    tags: __WEBPACK_IMPORTED_MODULE_0_vuept__["arr"].required,
    placeholder: String,
    klass: __WEBPACK_IMPORTED_MODULE_0_vuept__["obj"].default(function () {
      return __WEBPACK_IMPORTED_MODULE_1__lib__["b" /* klass */];
    })
  },
  data: function data() {
    return {
      typingIndex: -1
    };
  },

  computed: {
    normalizeTagItems: function normalizeTagItems() {
      return this.tags.map(function (item) {
        return typeof item === 'string' ? { text: item } : item;
      }).concat(null);
    },
    showPlaceholder: function showPlaceholder() {
      return this.placeholder && this.typingIndex < 0;
    },
    length: function length() {
      return this.tags.length;
    }
  },
  methods: {
    focus: function focus(index) {
      if (this.typingIndex === -1) {
        this.$emit('focus', index);
      }
      this.typingIndex = index;
    },
    blur: function blur(index) {
      // it will be false when caused by keyPress events
      if (index === this.typingIndex) {
        this.$emit('blur', index);
        this.typingIndex = -1;
      }
    },
    removeTag: function removeTag(index) {
      if (index >= 0 && !this.tags[index].readOnly) {
        this.$emit('tags-change', index, undefined);
      }
    },
    insertTag: function insertTag(index, text) {
      this.$emit('tags-change', index, text);
    },
    activeOther: function activeOther(index) {
      if (index >= 0 && index <= this.length) {
        this.typingIndex = index;
      }
    },
    getRemoveHandle: function getRemoveHandle(item, index) {
      var _this = this;

      return item.readOnly ? null : function () {
        return _this.removeTag(index);
      };
    }
  },
  components: {
    tag: __webpack_require__("./node_modules/vue-tagsinput/src/tag.vue"),
    typing: __webpack_require__("./node_modules/vue-tagsinput/src/typing.vue")
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./node_modules/vue-tagsinput/src/tag.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vuept__ = __webpack_require__("./node_modules/vuept/dist/vuept.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vuept___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vuept__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



var CROSS_ICON = __webpack_require__("./node_modules/vue-tagsinput/src/assets/ic_close_black_16px.svg");

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    text: __WEBPACK_IMPORTED_MODULE_0_vuept__["str"].required,
    remove: [Function, null],
    invalid: __WEBPACK_IMPORTED_MODULE_0_vuept__["bool"].default(false)
  },
  data: function data() {
    return { crossIcon: CROSS_ICON };
  },

  computed: {
    klass: function klass() {
      return this.$parent.klass;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./node_modules/vue-tagsinput/src/typing.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vuept__ = __webpack_require__("./node_modules/vuept/dist/vuept.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vuept___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vuept__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__lib__ = __webpack_require__("./node_modules/vue-tagsinput/src/lib.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    index: __WEBPACK_IMPORTED_MODULE_0_vuept__["num"].required,
    typing: __WEBPACK_IMPORTED_MODULE_0_vuept__["bool"].required,
    handleInsert: __WEBPACK_IMPORTED_MODULE_0_vuept__["func"].required,
    handleRemove: __WEBPACK_IMPORTED_MODULE_0_vuept__["func"].required,
    activeOther: __WEBPACK_IMPORTED_MODULE_0_vuept__["func"].required
  },
  data: function data() {
    return {
      text: ''
    };
  },

  computed: {
    baseWidth: function baseWidth() {
      return this.typing ? 2 : 0;
    },
    klass: function klass() {
      return this.$parent.klass;
    },
    trimText: function trimText() {
      return this.text.trim();
    }
  },
  watch: {
    typing: function typing(val) {
      var _this = this;

      val && this.$nextTick(function () {
        var $el = _this.$refs.input;
        $el.focus();
      });
    }
  },
  methods: {
    preventNativeActive: function preventNativeActive(e) {
      if (!this.typing) e.preventDefault();
    },
    finishEditing: function finishEditing() {
      this.addTag();
      this.$emit('blur', this.index);
    },
    addTag: function addTag() {
      var trimText = this.trimText;

      if (trimText) {
        this.handleInsert(this.index, trimText);
        this.text = '';
        return trimText;
      }
    },
    charLen: function charLen(str) {
      var charNum = 0;
      for (var i = 0; i < str.length; ++i) {
        charNum += str.charCodeAt(i) > 127 ? 2 : 1;
      }
      return charNum;
    },
    keyPress: function keyPress(e) {
      var _this2 = this;

      var $input = this.$refs.input;
      var cursor = $input.selectionStart;
      var valLen = $input.value.length;
      var key = e.keyCode;
      var native = false;

      if (key === __WEBPACK_IMPORTED_MODULE_1__lib__["a" /* KEY_CODE */].RIGHT && valLen === 0) {
        this.activeOther(this.index + 1);
      } else if (key === __WEBPACK_IMPORTED_MODULE_1__lib__["a" /* KEY_CODE */].LEFT && valLen === 0) {
        this.activeOther(this.index - 1);
      } else if (key === __WEBPACK_IMPORTED_MODULE_1__lib__["a" /* KEY_CODE */].BACKSPACE && cursor === 0) {
        var index = this.index - 1;
        this.handleRemove(index);
        this.activeOther(index);
      } else if (key === __WEBPACK_IMPORTED_MODULE_1__lib__["a" /* KEY_CODE */].TAB) {
        var _index = this.index + 1;
        this.addTag() && this.$nextTick(function () {
          return _this2.activeOther(_index);
        });
      } else native = true;

      !native && e.preventDefault();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/publisher/profile.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

// import Vue from "vue";
// Vue.use(VueForm, options);
/* harmony default export */ __webpack_exports__["default"] = ({
    name: "ComponentsProfile",
    data: function data() {
        return {
            showNda: false,
            formstate: {},
            model: {},
            aDevices: {},
            aCountries: {},
            additional_devices: [],
            device_map: {},
            policy_agree: false,
            previewUrl: "/images/device_no_icon.png"
        };
    },

    mounted: function mounted() {
        var _this = this;

        axios.get("/api/devices").then(function (response) {
            _this.aDevices = response.data;
            _this.aDevices.forEach(function (d) {
                return _this.device_map[d.id] = false;
            });
            return axios.get("/api/tester/" + _this.$store.state.user.id);
        }).then(function (_ref) {
            var data = _ref.data.data;
            return _this.assignData(data);
        }).catch(function (error) {});

        axios.get("/api/getCountries").then(function (response) {
            _this.aCountries = response.data;
        }).catch(function (error) {});
    },
    methods: {
        showNDA: function showNDA() {
            this.showNda = true;
        },
        hideNDA: function hideNDA() {
            if (localStorage.getItem("nda-agree")) {
                this.model.policy_agree = localStorage.getItem("nda-agree") == "1" ? true : false;
                localStorage.removeItem("nda-agree");
            }
            this.showNda = false;
        },
        assignData: function assignData(data) {
            var _this2 = this;

            this.model = _extends({}, this.model, data);
            this.previewUrl = data.profile;
            if (this.model.country_id == null) {
                this.model.country_id = 231; // 'United states' will be selected as default
            }
            data.devices.forEach(function (d) {
                return _this2.device_map[d.id] = true;
            });
            if (hasClearedStep1(data.status.name)) {
                this.model.policy_agree = true;
            }
        },

        /** Start - user image  */
        onFileChange: function onFileChange(e) {
            var _this3 = this;

            var file = e.target.files[0];
            var ext = file.name.split(".").pop();
            if (["jpg", "jpeg", "png"].indexOf(ext) === -1) {
                return this.$swal("Error", "Only jpg, jpeg, png images are allowed", "error");
            }
            this.iconFile = file;
            var reader = new FileReader();
            reader.onload = function (e) {
                return _this3.previewUrl = e.target.result;
            };
            reader.readAsDataURL(file);
            var formData = new FormData();
            formData.append("profile", file);
            formData.append("_method", "PUT");
            formData.append("email", this.model.email);
            axios.post("/api/tester/" + this.model.id, formData).then(function (_ref2) {
                var data = _ref2.data.data;

                _this3.assignData(data);
                _this3.$store.commit("set_user", data);
            }).catch(function (error) {
                return _this3.error = error;
            });
        },

        onSubmit: function onSubmit() {
            var _this4 = this;

            if (this.formstate.$invalid) {
                return;
            } else {
                var p = void 0;
                this.errors = null;
                if (!this.model.policy_agree) {
                    return this.$swal("Error", "Please read and agree to the policy", "error");
                }
                if (this.model.id) {
                    this.model.additional_devices = this.additional_devices;
                    var devices = [];
                    Object.keys(this.device_map).forEach(function (id) {
                        if (_this4.device_map[id]) {
                            devices.push(parseInt(id));
                        }
                    });
                    this.model.image = this.image;
                    var requestData = _extends({}, this.model, { devices: devices });
                    if (requestData.date_of_birth && typeof requestData.date_of_birth !== "string") {
                        requestData.date_of_birth = requestData.date_of_birth.toMysql();
                    }
                    requestData.status = "Step_1";
                    p = axios.put("/api/tester/" + this.model.id, requestData);
                    p.then(function (response) {
                        _this4.$store.commit("set_user", response.data.data);
                        _this4.$router.push({ name: "tester.step2" });
                    }).catch(function (error) {
                        return _this4.errors = error.response.data.message;
                    });
                }
            }
        },
        handleChange: function handleChange(index, text) {
            if (text) {
                this.additional_devices.splice(index, 0, text);
            } else {
                this.additional_devices.splice(index, 1);
            }
        }
    },
    components: {
        "tags-input": __webpack_require__("./node_modules/vue-tagsinput/src/input.vue"),
        Policy: Policy
    }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/publisher/profile.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_form__ = __webpack_require__("./node_modules/vue-form/dist/vue-form.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_form___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue_form__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_src_validations_validations_js__ = __webpack_require__("./resources/assets/validations/validations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_src_services_tester__ = __webpack_require__("./resources/assets/services/tester.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_vue_js_toggle_button__ = __webpack_require__("./node_modules/vue-js-toggle-button/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_vue_js_toggle_button___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_vue_js_toggle_button__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_pages_tester_nda_policy__ = __webpack_require__("./resources/assets/components/pages/tester/nda-policy.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_pages_tester_nda_policy___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_pages_tester_nda_policy__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_components_components_publisher_profile__ = __webpack_require__("./resources/assets/components/components/publisher/profile.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_components_components_publisher_profile___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_components_components_publisher_profile__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

// import Vue from "vue";






// Vue.use(VueForm, options);
/* harmony default export */ __webpack_exports__["default"] = ({
    name: "tester-profile",
    component: { ComponentsProfile: __WEBPACK_IMPORTED_MODULE_5_components_components_publisher_profile__["ComponentsProfile"] }

});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/tester/nda-policy.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__("./node_modules/vue/dist/vue.common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_src_services_auth_js__ = __webpack_require__("./resources/assets/services/auth.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



var VueScrollTo = __webpack_require__("./node_modules/vue-scrollto/vue-scrollto.js");
__WEBPACK_IMPORTED_MODULE_0_vue___default.a.use(VueScrollTo, {
  container: "body",
  duration: 500,
  easing: "ease"
});

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "nda-policy",
  data: function data() {
    return {
      isApprovedTester: Object(__WEBPACK_IMPORTED_MODULE_1_src_services_auth_js__["d" /* isApprovedTester */])()
    };
  },

  methods: {
    onAgree: function onAgree() {
      localStorage.setItem("nda-agree", 1);
      this.$emit("done");
      if (this.isApprovedTester) this.$router.push({ name: "tester.tester-profile" });else this.$router.push({ name: "tester.step1" });
    },
    ondisAgree: function ondisAgree() {
      localStorage.setItem("nda-agree", 0);
      this.$emit("done");
      this.$router.push({ name: "tester.step1" });
    }
  },
  mounted: function mounted() {
    VueScrollTo.scrollTo('body');
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1b27a788\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./node_modules/vue-tagsinput/src/input.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.tags-input[data-v-1b27a788] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -webkit-box-shadow: 0 0 0.15rem rgba(0, 0, 0, 0.3);\n          box-shadow: 0 0 0.15rem rgba(0, 0, 0, 0.3);\n  font-size: 0.75rem;\n  padding: 0.1rem 0\n}\n.placeholder[data-v-1b27a788] {\n  display: inline-block;\n  color: #A9A9A9;\n  line-height: 2em;\n  white-space: nowrap;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-34658397\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/nda-policy.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.custom-container[data-v-34658397] {\n  max-width: 100%;\n}\n.custom-container .step-page-title[data-v-34658397] {\n    font-family: \"UniNeueBold\";\n    font-size: 30px;\n    width: 100%;\n    text-align: center;\n    color: #363e48;\n    margin: 5px 0 25px;\n}\n.custom-container .nda-policy-page[data-v-34658397] {\n    width: 100%;\n}\n.custom-container .nda-policy-page .white-box[data-v-34658397] {\n      background-color: #fff;\n      -webkit-box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n              box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n      border-radius: 4px;\n      padding: 20px;\n      font-size: 17px;\n      line-height: 20px;\n      color: #606368;\n}\n.custom-container .nda-policy-page .white-box .agree-btn-group[data-v-34658397] {\n        width: 100%;\n        border-top: 1px solid #dadada;\n}\n.custom-container .nda-policy-page .white-box .agree-btn-group .green-btn[data-v-34658397] {\n          min-width: 130px;\n          padding: 7px 10px;\n          border: 2px solid #118921;\n          background-color: #2cac3d;\n          font-size: 14px;\n          font-family: \"BrandonTextMedium\";\n          letter-spacing: 0.05rem;\n          color: #fff;\n          margin: 15px auto 0;\n          border-radius: 20px;\n          display: inline-block;\n          margin-right: 10px;\n}\n.custom-container .nda-policy-page .white-box .agree-btn-group .green-btn[data-v-34658397]:hover {\n            background-color: #158f25;\n}\n.custom-container .nda-policy-page .white-box .agree-btn-group .red-btn[data-v-34658397] {\n          min-width: 130px;\n          padding: 7px 10px;\n          border: 2px solid #940000;\n          background-color: #e80000;\n          font-size: 14px;\n          font-family: \"BrandonTextMedium\";\n          letter-spacing: 0.05rem;\n          color: #fff;\n          margin: 15px auto 0;\n          border-radius: 20px;\n          display: inline-block;\n}\n.custom-container .nda-policy-page .white-box .agree-btn-group .red-btn[data-v-34658397]:hover {\n            background-color: #d41111;\n}\n@media screen and (max-width: 1281px) {\n.custom-container .nda-policy-page .white-box[data-v-34658397] {\n    font-size: 16px;\n}\n}\n@media screen and (max-width: 1200px) {\n.custom-container .step-page-title[data-v-34658397] {\n    font-size: 28px;\n    margin: 0 0 20px;\n}\n.custom-container .nda-policy-page .white-box[data-v-34658397] {\n    font-size: 15px;\n}\n}\n@media screen and (max-width: 991px) {\n.custom-container .nda-policy-page .white-box[data-v-34658397] {\n    padding: 15px;\n}\n}\n@media screen and (max-width: 575px) {\n.custom-container .step-page-title[data-v-34658397] {\n    font-size: 26px;\n}\n.custom-container .nda-policy-page .white-box[data-v-34658397] {\n    font-size: 14px;\n    line-height: 18px;\n}\n.custom-container .nda-policy-page .white-box .agree-btn-group .green-btn[data-v-34658397],\n    .custom-container .nda-policy-page .white-box .agree-btn-group .red-btn[data-v-34658397] {\n      min-width: 105px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5f061a8c\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./node_modules/vue-tagsinput/src/tag.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.tag[data-v-5f061a8c] {\n  border: 1px solid #e0e0e0;\n  border-radius: 3px;\n  color: #858585;\n  font-weight: normal;\n  font-size: 1.1em;\n  padding: 0 0.5ch;\n  vertical-align: middle\n}\n.tag[invalid][data-v-5f061a8c] {\n  border: 1px solid red;\n}\n.hl-click[data-v-5f061a8c] {\n  width: 10px;\n  cursor: pointer;\n}\n.hl-click[data-v-5f061a8c]:hover:active {\n  -webkit-box-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125);\n          box-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125);\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-91a6ae16\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./node_modules/vue-tagsinput/src/typing.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\ninput.input[data-v-91a6ae16] {\n  outline: none;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  border: none;\n  background-color: transparent;\n  font-family: monospace;\n  padding: 0 0.5ch;\n  line-height: 2em;\n}\n.gap[data-v-91a6ae16]:last-child {\n  -webkit-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-f7bbbe56\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/profile.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.vue-js-switch[data-v-f7bbbe56] {\n  float: right;\n}\n.user_image[data-v-f7bbbe56] {\n  font-size: 10px;\n}\n.input[data-v-f7bbbe56] {\n  width: 100% !important;\n}\n.custom-county-block[data-v-f7bbbe56] {\n  width: 25%;\n}\n.text-bold[data-v-f7bbbe56] {\n  font-family: \"BrandonTextBold\";\n}\n.custom-container[data-v-f7bbbe56] {\n  max-width: 100%;\n}\n.custom-container .step-page-title[data-v-f7bbbe56] {\n    font-family: \"UniNeueBold\";\n    font-size: 30px;\n    width: 100%;\n    text-align: center;\n    color: #363e48;\n    margin: 5px 0 25px;\n}\n.custom-container .tester-profile-page[data-v-f7bbbe56] {\n    width: 100%;\n}\n.custom-container .tester-profile-page .other-input[data-v-f7bbbe56] {\n      min-width: 150px;\n}\n.custom-container .tester-profile-page .toggle-btn-group[data-v-f7bbbe56] {\n      width: calc(100% - 115px);\n}\n.custom-container .tester-profile-page .load-proj-wrap[data-v-f7bbbe56] {\n      background-color: #2cac3d;\n      color: #fff;\n      font-family: \"BrandonTextBold\";\n      border-radius: 4px;\n      width: 100%;\n      text-align: center;\n      padding: 4px 8px;\n      margin-bottom: 20px;\n      font-size: 16px;\n      border: 2px solid #118921;\n}\n.custom-container .tester-profile-page .white-box[data-v-f7bbbe56] {\n      background-color: #fff;\n      -webkit-box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n              box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n      border-radius: 4px;\n      padding: 20px;\n      margin-bottom: 20px;\n      font-size: 17px;\n      color: #606368;\n      letter-spacing: 0;\n}\n.custom-container .tester-profile-page .white-box textarea[data-v-f7bbbe56] {\n        font-size: 15px;\n        resize: none;\n}\n.custom-container .tester-profile-page .white-box .block-title[data-v-f7bbbe56] {\n        font-size: 20px;\n        font-family: \"UniNeueBold\";\n        border-bottom: 1px solid #dadada;\n        color: #2cac3d;\n        padding-bottom: 10px;\n        margin-bottom: 10px;\n}\n.custom-container .tester-profile-page .white-box .img-wrap[data-v-f7bbbe56] {\n        height: 120px;\n        width: 120px;\n        border-radius: 50%;\n        border: 3px solid #1f8e28;\n        background-color: #f0f5f9;\n        vertical-align: middle;\n        margin-right: 15px;\n        padding: 18px 25px;\n        position: relative;\n}\n.custom-container .tester-profile-page .white-box .img-wrap h2[data-v-f7bbbe56] {\n          font-size: 14px;\n          vertical-align: middle;\n          margin-top: 25px;\n          text-align: center;\n}\n.custom-container .tester-profile-page .white-box .img-wrap .choose-img-btn[data-v-f7bbbe56] {\n          opacity: 0;\n          position: absolute;\n          left: 0;\n          top: 0;\n          height: 100%;\n          cursor: pointer;\n}\n.custom-container .tester-profile-page .white-box .content-wrap[data-v-f7bbbe56] {\n        vertical-align: middle;\n}\n.custom-container .tester-profile-page .white-box .content-wrap .username[data-v-f7bbbe56] {\n          font-family: \"UniNeueRegular\";\n          font-size: 25px;\n          color: #363e48;\n          margin-bottom: 0px;\n}\n.custom-container .tester-profile-page .white-box .content-wrap .usermail[data-v-f7bbbe56] {\n          font-size: 15px;\n          color: #73767b;\n          font-family: \"BrandonTextRegular\";\n          margin-bottom: 0;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form[data-v-f7bbbe56] {\n        padding-top: 35px;\n        float: right;\n        width: 70%;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group[data-v-f7bbbe56] {\n          min-width: 80px;\n          margin-right: 20px;\n          vertical-align: top;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group[data-v-f7bbbe56]:last-child {\n            margin-right: 0;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group .form-control[data-v-f7bbbe56]:hover, .custom-container .tester-profile-page .white-box .user-profile-form .form-group .form-control[data-v-f7bbbe56]:focus {\n            outline: none !important;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group label[data-v-f7bbbe56] {\n            display: block;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group .form-check[data-v-f7bbbe56] {\n            display: inline-block;\n            margin-right: 15px;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group .form-check .form-check-input[data-v-f7bbbe56] {\n              margin-left: 0;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group .form-check .form-check-label[data-v-f7bbbe56] {\n              padding-left: 20px;\n}\n.custom-container .tester-profile-page .white-box .detail-form[data-v-f7bbbe56] {\n        width: 100%;\n}\n.custom-container .tester-profile-page .white-box .detail-form span[data-v-f7bbbe56] {\n          max-width: 210px;\n          vertical-align: middle;\n          font-size: 16px;\n          color: #606368;\n          line-height: 20px;\n          margin-right: 15px;\n}\n.custom-container .tester-profile-page .white-box .detail-form .form-control[data-v-f7bbbe56] {\n          width: calc(100% - 90px);\n          float: right;\n          margin-top: 5px;\n}\n.custom-container .tester-profile-page .white-box .detail-form .form-control[data-v-f7bbbe56]:hover {\n            outline: none !important;\n}\n.custom-container .tester-profile-page .white-box .detail-form .form-control[data-v-f7bbbe56]:focus {\n            outline: none !important;\n}\n.custom-container .tester-profile-page .white-box .detail-form .toggle-btn-group[data-v-f7bbbe56] {\n          display: inline-block;\n          vertical-align: inherit;\n          margin-top: 5px;\n          margin-bottom: 0;\n}\n.custom-container .tester-profile-page .white-box .device-name[data-v-f7bbbe56] {\n        font-size: 17px;\n        color: #606368;\n        min-width: 110px;\n        vertical-align: middle;\n        display: inline-block;\n        min-width: 75%;\n}\n.custom-container .tester-profile-page .white-box .selected-txt[data-v-f7bbbe56] {\n        display: inline-block;\n        vertical-align: middle;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group[data-v-f7bbbe56] {\n        display: inline-block;\n        vertical-align: middle;\n        margin-bottom: 10px;\n        margin-top: 10px;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .green-btn[data-v-f7bbbe56] {\n          background-color: #2cac3d;\n          font-size: 17px;\n          font-family: \"BrandonTextMedium\";\n          letter-spacing: 0.05rem;\n          color: #fff;\n          border-radius: 4px;\n          padding: 0px 10px;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .green-btn[data-v-f7bbbe56]:hover {\n            background-color: #158f25;\n            outline: none !important;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .green-btn[data-v-f7bbbe56]:focus {\n            outline: none !important;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .white-btn[data-v-f7bbbe56] {\n          background-color: #fff;\n          border: 1px solid #e4e4e4;\n          font-size: 17px;\n          font-family: \"BrandonTextRegular\";\n          letter-spacing: 0.05rem;\n          color: #606368;\n          border-radius: 4px;\n          padding: 0px 10px;\n          margin-left: 5px;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .white-btn[data-v-f7bbbe56]:hover {\n            background-color: #e4e4e4;\n            outline: none !important;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .white-btn[data-v-f7bbbe56]:focus {\n            outline: none !important;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .form-control[data-v-f7bbbe56] {\n          vertical-align: middle;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .form-control[data-v-f7bbbe56]:hover, .custom-container .tester-profile-page .white-box .toggle-btn-group .form-control[data-v-f7bbbe56]:focus {\n            outline: none !important;\n}\n.custom-container .tester-profile-page .white-box .policy-text[data-v-f7bbbe56] {\n        margin-bottom: 5px;\n}\n.custom-container .tester-profile-page .white-box .policy-text a[data-v-f7bbbe56] {\n          color: #2cac3d;\n          font-family: \"BrandonTextBold\";\n          font-size: 15px;\n          text-decoration: underline !important;\n}\n.custom-container .tester-profile-page .white-box .agreed-txt[data-v-f7bbbe56] {\n        font-size: 15px;\n        color: #2cac3d;\n}\n.custom-container .tester-profile-page .green-step-btn[data-v-f7bbbe56] {\n      width: 100%;\n      max-width: 130px;\n      padding: 7px 10px;\n      border: 2px solid #118921;\n      background-color: #2cac3d;\n      font-size: 14px;\n      font-family: \"BrandonTextMedium\";\n      letter-spacing: 0.05rem;\n      color: #fff;\n      margin: 15px auto;\n      border-radius: 20px;\n      display: block;\n}\n.custom-container .tester-profile-page .green-step-btn[data-v-f7bbbe56]:hover {\n        background-color: #158f25;\n}\n@media screen and (max-width: 1281px) {\n.custom-container .tester-profile-page .white-box[data-v-f7bbbe56] {\n    font-size: 16px;\n}\n.custom-container .tester-profile-page .white-box textarea[data-v-f7bbbe56] {\n      font-size: 14px;\n}\n.custom-container .tester-profile-page .white-box .img-wrap[data-v-f7bbbe56] {\n      height: 100px;\n      width: 100px;\n      padding: 15px 20px;\n      margin-right: 10px;\n}\n.custom-container .tester-profile-page .white-box .content-wrap .username[data-v-f7bbbe56] {\n      font-size: 22px;\n}\n.custom-container .tester-profile-page .white-box .detail-form span[data-v-f7bbbe56] {\n      max-width: 160px;\n      font-size: 14px;\n      line-height: 18px;\n}\n.custom-container .tester-profile-page .white-box .device-name[data-v-f7bbbe56] {\n      font-size: 16px;\n}\n}\n@media screen and (max-width: 1200px) {\n.custom-container .step-page-title[data-v-f7bbbe56] {\n    font-size: 28px;\n    margin: 0 0 20px;\n}\n.custom-container .tester-profile-page .white-box[data-v-f7bbbe56] {\n    font-size: 15px;\n}\n.custom-container .tester-profile-page .white-box .block-title[data-v-f7bbbe56] {\n      font-size: 18px;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form[data-v-f7bbbe56] {\n      padding-top: 20px;\n      float: left;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group .form-check[data-v-f7bbbe56] {\n        margin-right: 0;\n}\n.custom-container .tester-profile-page .white-box .img-wrap[data-v-f7bbbe56] {\n      height: 80px;\n      width: 80px;\n      padding: 10px 15px;\n}\n.custom-container .tester-profile-page .white-box .detail-form .form-control[data-v-f7bbbe56] {\n      float: left;\n}\n.custom-container .tester-profile-page .white-box .device-name[data-v-f7bbbe56] {\n      min-width: 100%;\n      display: block;\n      font-size: 15px;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group[data-v-f7bbbe56] {\n      display: block;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .green-btn[data-v-f7bbbe56] {\n        font-size: 15px;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .white-btn[data-v-f7bbbe56] {\n        font-size: 15px;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .form-control[data-v-f7bbbe56] {\n        max-width: 100px;\n}\n.custom-container .tester-profile-page .white-box .policy-text a[data-v-f7bbbe56] {\n      font-size: 16px;\n}\n}\n@media screen and (max-width: 991px) {\n.custom-container .tester-profile-page .white-box[data-v-f7bbbe56] {\n    padding: 15px;\n}\n.custom-container .tester-profile-page .white-box .detail-form .form-control[data-v-f7bbbe56] {\n      max-width: 120px;\n      float: none;\n      margin-bottom: 15px;\n      margin-left: 15px;\n}\n.custom-container .tester-profile-page .white-box .detail-form span[data-v-f7bbbe56] {\n      min-height: auto;\n      max-width: 100%;\n}\n.custom-container .tester-profile-page .white-box .detail-form span br[data-v-f7bbbe56] {\n        display: none;\n}\n}\n@media screen and (max-width: 575px) {\n.custom-container .step-page-title[data-v-f7bbbe56] {\n    font-size: 26px;\n}\n.custom-container .tester-profile-page .white-box[data-v-f7bbbe56] {\n    font-size: 14px;\n    line-height: 18px;\n}\n.custom-container .tester-profile-page .white-box .img-wrap[data-v-f7bbbe56] {\n      margin-bottom: 10px;\n}\n.custom-container .tester-profile-page .white-box .detail-form .toggle-btn-group[data-v-f7bbbe56] {\n      margin-bottom: 15px;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form[data-v-f7bbbe56] {\n      width: 100%;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group[data-v-f7bbbe56] {\n        min-width: 100%;\n        margin-right: 0;\n}\n.custom-container .tester-profile-page .white-box .policy-text[data-v-f7bbbe56] {\n      font-size: 14px;\n}\n.custom-container .tester-profile-page .white-box .agree-text[data-v-f7bbbe56] {\n      font-size: 13px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-1b27a788\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./node_modules/vue-tagsinput/src/input.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { class: _vm.klass.container },
    [
      _vm._l(_vm.normalizeTagItems, function(item, index) {
        return [
          _c(
            "typing",
            {
              attrs: {
                index: index,
                typing: index === _vm.typingIndex,
                "handle-insert": _vm.insertTag,
                "handle-remove": _vm.removeTag,
                "active-other": _vm.activeOther
              },
              on: {
                click: function($event) {
                  _vm.focus(index)
                },
                blur: function($event) {
                  _vm.blur(index)
                }
              }
            },
            [
              _c(
                "span",
                {
                  directives: [
                    {
                      name: "show",
                      rawName: "v-show",
                      value: index === _vm.length && _vm.showPlaceholder,
                      expression: "index === length && showPlaceholder"
                    }
                  ],
                  class: _vm.klass.placeholder
                },
                [_vm._v(_vm._s(_vm.placeholder))]
              )
            ]
          ),
          _vm._v(" "),
          index !== _vm.length
            ? _c("tag", {
                attrs: {
                  text: item.text,
                  remove: _vm.getRemoveHandle(item, index),
                  invalid: item.invalid
                }
              })
            : _vm._e()
        ]
      })
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-1b27a788", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-34658397\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/tester/nda-policy.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "bg-color" }, [
    _c("div", { staticClass: "container custom-container" }, [
      _c("div", { staticClass: "row" }, [
        _c("h1", { staticClass: "step-page-title" }, [
          _vm._v("\n        Our NDA Policy\n      ")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "nda-policy-page" }, [
          _c("div", { staticClass: "white-box" }, [
            _vm._m(0),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-12 text-center" }, [
                _vm.isApprovedTester
                  ? _c("div", { staticClass: "agree-btn-group" }, [
                      _c(
                        "a",
                        {
                          staticClass: "btn text-uppercase green-btn",
                          attrs: { href: "javascript:;" },
                          on: { click: _vm.onAgree }
                        },
                        [_vm._v("Back to Profile")]
                      )
                    ])
                  : _c("div", { staticClass: "agree-btn-group" }, [
                      _c(
                        "a",
                        {
                          staticClass: "btn text-uppercase green-btn",
                          attrs: { href: "javascript:;" },
                          on: { click: _vm.onAgree }
                        },
                        [_vm._v("I Agree")]
                      ),
                      _vm._v(" "),
                      _c(
                        "a",
                        {
                          staticClass: "btn text-uppercase red-btn",
                          attrs: { href: "javascript:;" },
                          on: { click: _vm.ondisAgree }
                        },
                        [_vm._v("I Disagree")]
                      )
                    ])
              ])
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12" }, [
        _c("p", [_vm._v("Confidential/Nondisclosure Agreement")]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            '1. Definition of Confidential Information. For purposes of this Agreement, "Confidential Information" shall include all information or material that has or could have commercial value or other utility in the business in which Disclosing Party is engaged.'
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "2. Exclusions from Confidential Information. Receiving Party's obligations under this Agreement do not extend to information that is: (a) publicly known at the time of disclosure or subsequently becomes publicly known through no fault of the Receiving Party; (b) discovered or created by the Receiving Party before disclosure by Disclosing Party; (c) learned by the Receiving Party through legitimate means other than from the Disclosing Party or Disclosing Party's representatives; or (d) is disclosed by Receiving Party with Disclosing Party's prior written approval."
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "3. Obligations of Receiving Party. Receiving Party shall hold and maintain the Confidential Information in strictest confidence for the sole and exclusive benefit of the Disclosing Party. Receiving Party shall not, without prior written approval of Disclosing Party, use for Receiving Party's own benefit, publish, copy, or otherwise disclose to others, or permit the use by others for their benefit or to the detriment of Disclosing Party, any Confidential Information. Receiving Party shall return to Disclosing Party any and all records, notes, and other written, printed, or tangible materials in its possession pertaining to Confidential Information."
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "4. Time Periods. The nondisclosure provisions of this Agreement shall survive the termination of this Agreement and Receiving Party's duty to hold Confidential Information in confidence shall remain in effect until the Confidential Information no longer qualifies as a trade secret or until Disclosing Party sends Receiving Party written notice releasing Receiving Party from this Agreement, whichever occurs first."
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "5. Relationships. Nothing contained in this Agreement shall be deemed to constitute either party a partner, joint venture or employee of the other party for any purpose."
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "6. Integration. This Agreement expresses the complete understanding of the parties with respect to the subject matter and supersedes all prior proposals, agreements, representations, and understandings. This Agreement may not be amended except in a writing signed by both parties."
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "8. Waiver. The failure to exercise any right provided in this Agreement shall not be a waiver of prior or subsequent rights.\n              This Agreement and each party's obligations shall be binding on the representatives, assigns, and successors of such party."
          )
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-34658397", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-5f061a8c\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./node_modules/vue-tagsinput/src/tag.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("span", { class: _vm.klass.tag, attrs: { invalid: _vm.invalid } }, [
    _vm._v("\n  " + _vm._s(_vm.text) + "\n  "),
    _vm.remove
      ? _c("img", {
          staticClass: "hl-click",
          attrs: { src: _vm.crossIcon, alt: "close" },
          on: { click: _vm.remove }
        })
      : _vm._e()
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-5f061a8c", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-7d10444f\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/publisher/profile.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _vm.showNda
        ? _c("policy", { on: { done: _vm.hideNDA } })
        : _c("div", { staticClass: "bg-color" }, [
            _c("div", { staticClass: "container custom-container" }, [
              _c(
                "div",
                { staticClass: "row" },
                [
                  _c("h1", { staticClass: "step-page-title" }, [
                    _vm._v("\n                    Profile\n                ")
                  ]),
                  _vm._v(" "),
                  _c("Profile")
                ],
                1
              )
            ])
          ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-7d10444f", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-91a6ae16\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./node_modules/vue-tagsinput/src/typing.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "span",
    {
      class: _vm.klass.gap,
      on: {
        click: function($event) {
          _vm.$emit("click")
        }
      }
    },
    [
      _c("input", {
        directives: [
          {
            name: "model",
            rawName: "v-model",
            value: _vm.text,
            expression: "text"
          }
        ],
        ref: "input",
        class: _vm.klass.input,
        style: { width: _vm.baseWidth + _vm.charLen(_vm.text) + "ch" },
        attrs: { type: "text" },
        domProps: { value: _vm.text },
        on: {
          mousedown: _vm.preventNativeActive,
          blur: _vm.finishEditing,
          keydown: _vm.keyPress,
          input: function($event) {
            if ($event.target.composing) {
              return
            }
            _vm.text = $event.target.value
          }
        }
      }),
      _vm._v(" "),
      !_vm.typing ? _vm._t("default") : _vm._e()
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-91a6ae16", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-f7bbbe56\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/publisher/profile.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "tester-profile-page" }, [
    _c("div", { staticClass: "white-box" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-xl-5 col-12" }, [
          _c("div", { staticClass: "img-wrap d-block d-sm-inline-block" }, [
            _c("input", {
              staticClass: "form-control choose-img-btn",
              attrs: { type: "file" },
              on: { change: _vm.onFileChange }
            }),
            _vm._v(" "),
            _c("img", {
              staticClass: "img-fluid",
              attrs: {
                src: _vm.previewUrl ? _vm.previewUrl : _vm.model.profile
              }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "content-wrap d-block d-sm-inline-block" }, [
            _c("h4", { staticClass: "username" }, [
              _vm._v(
                _vm._s(_vm.model.first_name) + " " + _vm._s(_vm.model.last_name)
              )
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "usermail" }, [
              _vm._v(_vm._s(_vm.model.email))
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-xl-7 col-12" }, [
          _c("div", { staticClass: "user-profile-form" }, [
            _c(
              "div",
              { staticClass: "form-group d-inline-block custom-county-block" },
              [
                _c("label", [_vm._v("Country")]),
                _vm._v(" "),
                _c("span", { staticClass: "d-block text-bold" }, [
                  _vm._v(
                    "\n                        " +
                      _vm._s(_vm.model.country ? _vm.model.country.name : "") +
                      "\n                      "
                  )
                ])
              ]
            ),
            _vm._v(" "),
            _c("div", { staticClass: "form-group d-inline-block" }, [
              _c("label", [_vm._v("Gender")]),
              _vm._v(" "),
              _c("span", { staticClass: "d-block text-bold" }, [
                _vm._v(
                  "\n                      " +
                    _vm._s(_vm.model.gender) +
                    "\n                      "
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group d-inline-block" }, [
              _c("label", [_vm._v("Birthdate")]),
              _vm._v(" "),
              _c("span", { staticClass: "d-block text-bold" }, [
                _vm._v(
                  "\n                        " +
                    _vm._s(_vm._f("date")(_vm.model.date_of_birth)) +
                    "\n                      "
                )
              ])
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "white-box" }, [
      _c("h4", { staticClass: "block-title" }, [
        _vm._v("\n            Basic Information\n        ")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "detail-form" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12" }, [
            _vm._m(0),
            _vm._v(" "),
            _c("span", { staticClass: "text-bold d-inline-block" }, [
              _vm._v(
                "\n                        " +
                  _vm._s(_vm.model.mobile_no) +
                  "\n                      "
              )
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "white-box" }, [
      _c("h4", { staticClass: "block-title" }, [
        _vm._v("\n            Other Information\n        ")
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "mb-0" }, [
        _vm._v(
          "\n            " +
            _vm._s(_vm.model.additional_information) +
            "\n        "
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "d-inline-block " }, [
      _vm._v("\n                      Cell "),
      _c("br"),
      _vm._v(" Number :\n                    ")
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-f7bbbe56", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-scrollto/vue-scrollto.js":
/***/ (function(module, exports, __webpack_require__) {

(function (global, factory) {
	 true ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(global['vue-scrollto'] = factory());
}(this, (function () { 'use strict';

/**
 * https://github.com/gre/bezier-easing
 * BezierEasing - use bezier curve for transition easing function
 * by Gaëtan Renaudeau 2014 - 2015 – MIT License
 */

// These values are established by empiricism with tests (tradeoff: performance VS precision)
var NEWTON_ITERATIONS = 4;
var NEWTON_MIN_SLOPE = 0.001;
var SUBDIVISION_PRECISION = 0.0000001;
var SUBDIVISION_MAX_ITERATIONS = 10;

var kSplineTableSize = 11;
var kSampleStepSize = 1.0 / (kSplineTableSize - 1.0);

var float32ArraySupported = typeof Float32Array === 'function';

function A (aA1, aA2) { return 1.0 - 3.0 * aA2 + 3.0 * aA1; }
function B (aA1, aA2) { return 3.0 * aA2 - 6.0 * aA1; }
function C (aA1)      { return 3.0 * aA1; }

// Returns x(t) given t, x1, and x2, or y(t) given t, y1, and y2.
function calcBezier (aT, aA1, aA2) { return ((A(aA1, aA2) * aT + B(aA1, aA2)) * aT + C(aA1)) * aT; }

// Returns dx/dt given t, x1, and x2, or dy/dt given t, y1, and y2.
function getSlope (aT, aA1, aA2) { return 3.0 * A(aA1, aA2) * aT * aT + 2.0 * B(aA1, aA2) * aT + C(aA1); }

function binarySubdivide (aX, aA, aB, mX1, mX2) {
  var currentX, currentT, i = 0;
  do {
    currentT = aA + (aB - aA) / 2.0;
    currentX = calcBezier(currentT, mX1, mX2) - aX;
    if (currentX > 0.0) {
      aB = currentT;
    } else {
      aA = currentT;
    }
  } while (Math.abs(currentX) > SUBDIVISION_PRECISION && ++i < SUBDIVISION_MAX_ITERATIONS);
  return currentT;
}

function newtonRaphsonIterate (aX, aGuessT, mX1, mX2) {
 for (var i = 0; i < NEWTON_ITERATIONS; ++i) {
   var currentSlope = getSlope(aGuessT, mX1, mX2);
   if (currentSlope === 0.0) {
     return aGuessT;
   }
   var currentX = calcBezier(aGuessT, mX1, mX2) - aX;
   aGuessT -= currentX / currentSlope;
 }
 return aGuessT;
}

var src = function bezier (mX1, mY1, mX2, mY2) {
  if (!(0 <= mX1 && mX1 <= 1 && 0 <= mX2 && mX2 <= 1)) {
    throw new Error('bezier x values must be in [0, 1] range');
  }

  // Precompute samples table
  var sampleValues = float32ArraySupported ? new Float32Array(kSplineTableSize) : new Array(kSplineTableSize);
  if (mX1 !== mY1 || mX2 !== mY2) {
    for (var i = 0; i < kSplineTableSize; ++i) {
      sampleValues[i] = calcBezier(i * kSampleStepSize, mX1, mX2);
    }
  }

  function getTForX (aX) {
    var intervalStart = 0.0;
    var currentSample = 1;
    var lastSample = kSplineTableSize - 1;

    for (; currentSample !== lastSample && sampleValues[currentSample] <= aX; ++currentSample) {
      intervalStart += kSampleStepSize;
    }
    --currentSample;

    // Interpolate to provide an initial guess for t
    var dist = (aX - sampleValues[currentSample]) / (sampleValues[currentSample + 1] - sampleValues[currentSample]);
    var guessForT = intervalStart + dist * kSampleStepSize;

    var initialSlope = getSlope(guessForT, mX1, mX2);
    if (initialSlope >= NEWTON_MIN_SLOPE) {
      return newtonRaphsonIterate(aX, guessForT, mX1, mX2);
    } else if (initialSlope === 0.0) {
      return guessForT;
    } else {
      return binarySubdivide(aX, intervalStart, intervalStart + kSampleStepSize, mX1, mX2);
    }
  }

  return function BezierEasing (x) {
    if (mX1 === mY1 && mX2 === mY2) {
      return x; // linear
    }
    // Because JavaScript number are imprecise, we should guarantee the extremes are right.
    if (x === 0) {
      return 0;
    }
    if (x === 1) {
      return 1;
    }
    return calcBezier(getTForX(x), mY1, mY2);
  };
};

var easings = {
    ease: [0.25, 0.1, 0.25, 1.0],
    linear: [0.00, 0.0, 1.00, 1.0],
    "ease-in": [0.42, 0.0, 1.00, 1.0],
    "ease-out": [0.00, 0.0, 0.58, 1.0],
    "ease-in-out": [0.42, 0.0, 0.58, 1.0]
};

// https://github.com/WICG/EventListenerOptions/blob/gh-pages/explainer.md#feature-detection
var supportsPassive = false;
try {
    var opts = Object.defineProperty({}, "passive", {
        get: function get() {
            supportsPassive = true;
        }
    });
    window.addEventListener("test", null, opts);
} catch (e) {}

var _ = {
    $: function $(selector) {
        if (typeof selector !== "string") {
            return selector;
        }
        return document.querySelector(selector);
    },
    on: function on(element, events, handler) {
        var opts = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : { passive: false };

        if (!(events instanceof Array)) {
            events = [events];
        }
        for (var i = 0; i < events.length; i++) {
            element.addEventListener(events[i], handler, supportsPassive ? opts : false);
        }
    },
    off: function off(element, events, handler) {
        if (!(events instanceof Array)) {
            events = [events];
        }
        for (var i = 0; i < events.length; i++) {
            element.removeEventListener(events[i], handler);
        }
    },
    cumulativeOffset: function cumulativeOffset(element) {
        var top = 0;
        var left = 0;

        do {
            top += element.offsetTop || 0;
            left += element.offsetLeft || 0;
            element = element.offsetParent;
        } while (element);

        return {
            top: top,
            left: left
        };
    }
};

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
  return typeof obj;
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
};





















var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var abortEvents = ["mousedown", "wheel", "DOMMouseScroll", "mousewheel", "keyup", "touchmove"];

var defaults$$1 = {
    container: "body",
    duration: 500,
    easing: "ease",
    offset: 0,
    cancelable: true,
    onDone: false,
    onCancel: false,
    x: false,
    y: true
};

function setDefaults(options) {
    defaults$$1 = _extends({}, defaults$$1, options);
}

var scroller = function scroller() {
    var element = void 0; // element to scroll to
    var container = void 0; // container to scroll
    var duration = void 0; // duration of the scrolling
    var easing = void 0; // easing to be used when scrolling
    var offset = void 0; // offset to be added (subtracted)
    var cancelable = void 0; // indicates if user can cancel the scroll or not.
    var onDone = void 0; // callback when scrolling is done
    var onCancel = void 0; // callback when scrolling is canceled / aborted
    var x = void 0; // scroll on x axis
    var y = void 0; // scroll on y axis

    var initialX = void 0; // initial X of container
    var targetX = void 0; // target X of container
    var initialY = void 0; // initial Y of container
    var targetY = void 0; // target Y of container
    var diffX = void 0; // difference
    var diffY = void 0; // difference

    var abort = void 0; // is scrolling aborted

    var abortEv = void 0; // event that aborted scrolling
    var abortFn = function abortFn(e) {
        if (!cancelable) return;
        abortEv = e;
        abort = true;
    };
    var easingFn = void 0;

    var timeStart = void 0; // time when scrolling started
    var timeElapsed = void 0; // time elapsed since scrolling started

    var progress = void 0; // progress

    function scrollTop(container) {
        var scrollTop = container.scrollTop;

        if (container.tagName.toLowerCase() === "body") {
            // in firefox body.scrollTop always returns 0
            // thus if we are trying to get scrollTop on a body tag
            // we need to get it from the documentElement
            scrollTop = scrollTop || document.documentElement.scrollTop;
        }

        return scrollTop;
    }

    function scrollLeft(container) {
        var scrollLeft = container.scrollLeft;

        if (container.tagName.toLowerCase() === "body") {
            // in firefox body.scrollLeft always returns 0
            // thus if we are trying to get scrollLeft on a body tag
            // we need to get it from the documentElement
            scrollLeft = scrollLeft || document.documentElement.scrollLeft;
        }

        return scrollLeft;
    }

    function step(timestamp) {
        if (abort) return done();
        if (!timeStart) timeStart = timestamp;

        timeElapsed = timestamp - timeStart;

        progress = Math.min(timeElapsed / duration, 1);
        progress = easingFn(progress);

        topLeft(container, initialY + diffY * progress, initialX + diffX * progress);

        timeElapsed < duration ? window.requestAnimationFrame(step) : done();
    }

    function done() {
        if (!abort) topLeft(container, targetY, targetX);
        timeStart = false;

        _.off(container, abortEvents, abortFn);
        if (abort && onCancel) onCancel(abortEv);
        if (!abort && onDone) onDone();
    }

    function topLeft(element, top, left) {
        if (y) element.scrollTop = top;
        if (x) element.scrollLeft = left;
        if (element.tagName.toLowerCase() === "body") {
            // in firefox body.scrollTop doesn't scroll the page
            // thus if we are trying to scrollTop on a body tag
            // we need to scroll on the documentElement
            if (y) document.documentElement.scrollTop = top;
            if (x) document.documentElement.scrollLeft = left;
        }
    }

    function scrollTo(target, _duration) {
        var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

        if ((typeof _duration === "undefined" ? "undefined" : _typeof(_duration)) === "object") {
            options = _duration;
        } else if (typeof _duration === "number") {
            options.duration = _duration;
        }

        element = _.$(target);

        if (!element) {
            return console.warn("[vue-scrollto warn]: Trying to scroll to an element that is not on the page: " + target);
        }

        container = _.$(options.container || defaults$$1.container);
        duration = options.duration || defaults$$1.duration;
        easing = options.easing || defaults$$1.easing;
        offset = options.offset || defaults$$1.offset;
        cancelable = options.hasOwnProperty("cancelable") ? options.cancelable !== false : defaults$$1.cancelable;
        onDone = options.onDone || defaults$$1.onDone;
        onCancel = options.onCancel || defaults$$1.onCancel;
        x = options.x === undefined ? defaults$$1.x : options.x;
        y = options.y === undefined ? defaults$$1.y : options.y;

        var cumulativeOffsetContainer = _.cumulativeOffset(container);
        var cumulativeOffsetElement = _.cumulativeOffset(element);

        if (typeof offset === "function") {
            offset = offset();
        }

        initialY = scrollTop(container);
        targetY = cumulativeOffsetElement.top - cumulativeOffsetContainer.top + offset;

        initialX = scrollLeft(container);
        targetX = cumulativeOffsetElement.left - cumulativeOffsetContainer.left + offset;

        abort = false;

        diffY = targetY - initialY;
        diffX = targetX - initialX;

        if (typeof easing === "string") {
            easing = easings[easing] || easings["ease"];
        }

        easingFn = src.apply(src, easing);

        if (!diffY && !diffX) return;

        _.on(container, abortEvents, abortFn, { passive: true });

        window.requestAnimationFrame(step);

        return function () {
            abortEv = null;
            abort = true;
        };
    }

    return scrollTo;
};

var _scroller = scroller();

var bindings = []; // store binding data

function deleteBinding(el) {
    for (var i = 0; i < bindings.length; ++i) {
        if (bindings[i].el === el) {
            bindings.splice(i, 1);
            return true;
        }
    }
    return false;
}

function findBinding(el) {
    for (var i = 0; i < bindings.length; ++i) {
        if (bindings[i].el === el) {
            return bindings[i];
        }
    }
}

function getBinding(el) {
    var binding = findBinding(el);

    if (binding) {
        return binding;
    }

    bindings.push(binding = {
        el: el,
        binding: {}
    });

    return binding;
}

function handleClick(e) {
    e.preventDefault();
    var ctx = getBinding(this).binding;

    if (typeof ctx.value === "string") {
        return _scroller(ctx.value);
    }
    _scroller(ctx.value.el || ctx.value.element, ctx.value);
}

var VueScrollTo$1 = {
    bind: function bind(el, binding) {
        getBinding(el).binding = binding;
        _.on(el, "click", handleClick);
    },
    unbind: function unbind(el) {
        deleteBinding(el);
        _.off(el, "click", handleClick);
    },
    update: function update(el, binding) {
        getBinding(el).binding = binding;
    },

    scrollTo: _scroller,
    bindings: bindings
};

var install = function install(Vue, options) {
    if (options) setDefaults(options);
    Vue.directive("scroll-to", VueScrollTo$1);
    Vue.prototype.$scrollTo = VueScrollTo$1.scrollTo;
};

if (typeof window !== "undefined" && window.Vue) {
    window.VueScrollTo = VueScrollTo$1;
    window.VueScrollTo.setDefaults = setDefaults;
    Vue.use(install);
}

VueScrollTo$1.install = install;

return VueScrollTo$1;

})));


/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1b27a788\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./node_modules/vue-tagsinput/src/input.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1b27a788\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./node_modules/vue-tagsinput/src/input.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("27b2958e", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../css-loader/index.js!../../vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1b27a788\",\"scoped\":true,\"hasInlineConfig\":true}!../../vue-loader/lib/selector.js?type=styles&index=0!./input.vue", function() {
     var newContent = require("!!../../css-loader/index.js!../../vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1b27a788\",\"scoped\":true,\"hasInlineConfig\":true}!../../vue-loader/lib/selector.js?type=styles&index=0!./input.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-34658397\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/nda-policy.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-34658397\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/nda-policy.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("5f79779a", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-34658397\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./nda-policy.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-34658397\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./nda-policy.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5f061a8c\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./node_modules/vue-tagsinput/src/tag.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5f061a8c\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./node_modules/vue-tagsinput/src/tag.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("4ff50cd4", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../css-loader/index.js!../../vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5f061a8c\",\"scoped\":true,\"hasInlineConfig\":true}!../../vue-loader/lib/selector.js?type=styles&index=0!./tag.vue", function() {
     var newContent = require("!!../../css-loader/index.js!../../vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5f061a8c\",\"scoped\":true,\"hasInlineConfig\":true}!../../vue-loader/lib/selector.js?type=styles&index=0!./tag.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-91a6ae16\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./node_modules/vue-tagsinput/src/typing.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-91a6ae16\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./node_modules/vue-tagsinput/src/typing.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("44baf17e", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../css-loader/index.js!../../vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-91a6ae16\",\"scoped\":true,\"hasInlineConfig\":true}!../../vue-loader/lib/selector.js?type=styles&index=0!./typing.vue", function() {
     var newContent = require("!!../../css-loader/index.js!../../vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-91a6ae16\",\"scoped\":true,\"hasInlineConfig\":true}!../../vue-loader/lib/selector.js?type=styles&index=0!./typing.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-f7bbbe56\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/profile.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-f7bbbe56\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/profile.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("2c728c1f", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-f7bbbe56\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./profile.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-f7bbbe56\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./profile.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-tagsinput/src/assets/ic_close_black_16px.svg":
/***/ (function(module, exports) {

module.exports = "/fonts/vendor/vue-tagsinput/src/ic_close_black_16px.svg?3d22f7bbb73ec35cad207c6b1c638257";

/***/ }),

/***/ "./node_modules/vue-tagsinput/src/input.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1b27a788\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./node_modules/vue-tagsinput/src/input.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./node_modules/vue-tagsinput/src/input.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-1b27a788\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./node_modules/vue-tagsinput/src/input.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-1b27a788"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "node_modules\\vue-tagsinput\\src\\input.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1b27a788", Component.options)
  } else {
    hotAPI.reload("data-v-1b27a788", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./node_modules/vue-tagsinput/src/lib.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
const KEY_CODE = {
  LEFT: 37,
  RIGHT: 39,
  TAB: 9,
  BACKSPACE: 8
}
/* harmony export (immutable) */ __webpack_exports__["a"] = KEY_CODE;


const klass = {
  container: 'tags-input',
  input: 'input',
  gap: 'gap',
  tag: 'tag',
  placeholder: 'placeholder'
}
/* harmony export (immutable) */ __webpack_exports__["b"] = klass;



/***/ }),

/***/ "./node_modules/vue-tagsinput/src/tag.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5f061a8c\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./node_modules/vue-tagsinput/src/tag.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./node_modules/vue-tagsinput/src/tag.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-5f061a8c\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./node_modules/vue-tagsinput/src/tag.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-5f061a8c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "node_modules\\vue-tagsinput\\src\\tag.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5f061a8c", Component.options)
  } else {
    hotAPI.reload("data-v-5f061a8c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./node_modules/vue-tagsinput/src/typing.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-91a6ae16\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./node_modules/vue-tagsinput/src/typing.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./node_modules/vue-tagsinput/src/typing.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-91a6ae16\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./node_modules/vue-tagsinput/src/typing.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-91a6ae16"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "node_modules\\vue-tagsinput\\src\\typing.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-91a6ae16", Component.options)
  } else {
    hotAPI.reload("data-v-91a6ae16", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./node_modules/vuept/dist/vuept.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var types = {
  num: Number,
  str: String,
  bool: Boolean,
  func: Function,
  obj: Object,
  arr: Array,
  any: null
};

var props = {
  validator: function validator(func) {
    return { validator: func };
  }
};
var proto = {
  get required() {
    return {
      type: this._type,
      required: true
    };
  },
  default: function _default(_default2) {
    return {
      type: this._type,
      default: _default2
    };
  }
};

Object.keys(types).forEach(function (t) {
  props[t] = Object.create(proto, {
    _type: {
      value: types[t]
    }
  });
});

module.exports = props;


/***/ }),

/***/ "./resources/assets/components/components/publisher/profile.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-f7bbbe56\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/profile.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/publisher/profile.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-f7bbbe56\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/publisher/profile.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-f7bbbe56"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\publisher\\profile.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-f7bbbe56", Component.options)
  } else {
    hotAPI.reload("data-v-f7bbbe56", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/pages/publisher/profile.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/publisher/profile.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-7d10444f\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/publisher/profile.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\publisher\\profile.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7d10444f", Component.options)
  } else {
    hotAPI.reload("data-v-7d10444f", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/pages/tester/nda-policy.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-34658397\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/nda-policy.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/tester/nda-policy.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-34658397\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/tester/nda-policy.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-34658397"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\tester\\nda-policy.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-34658397", Component.options)
  } else {
    hotAPI.reload("data-v-34658397", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});