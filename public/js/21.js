webpackJsonp([21],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/device-selector.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "DeviceSelector",
  props: {
    disabled: {
      type: Boolean,
      defualt: function defualt() {
        return false;
      }
    },
    value: {
      type: Array,
      required: true,
      default: function _default() {
        return [];
      }
    }
  },
  data: function data() {
    return {
      devices: []
    };
  },

  methods: {
    toggleItem: function toggleItem(device) {
      if (this.disabled) {
        return true;
      }
      var index = this.value.indexOf(device.id);
      var newDevices = [];
      if (index >= 0) {
        newDevices = this.value.filter(function (i) {
          return i !== device.id;
        });
      } else {
        newDevices = [].concat(_toConsumableArray(this.value), [device.id]);
      }
      this.$emit("input", newDevices);
      this.$emit("change", newDevices);
    }
  },
  mounted: function mounted() {
    var _this = this;

    if (!this.$store.state.devices.length) {
      axios.get("/api/device").then(function (_ref) {
        var data = _ref.data;

        _this.$store.commit("set_devices", data);
        _this.devices = data;
      });
    } else {
      this.devices = this.$store.state.devices;
    }
  },
  computed: {
    availableDevices: function availableDevices() {
      var _this2 = this;

      if (this.disabled) {
        return this.devices.filter(function (_ref2) {
          var id = _ref2.id;
          return _this2.value.indexOf(id) >= 0;
        });
      }
      return this.devices;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/project-cost.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "ProjectCost",
  props: {
    value: {
      type: Object,
      required: true,
      default: function _default() {
        return {};
      }
    },
    disabled: {
      type: Boolean,
      default: function _default() {
        return false;
      }
    },
    errors: {
      type: Array,
      default: function _default() {
        return null;
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/project-statistics.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "ProjectStatistics",
  props: {
    value: {
      type: Object,
      required: true,
      default: function _default() {
        return {};
      }
    },
    disabled: {
      type: Boolean,
      default: function _default() {
        return false;
      }
    },
    errors: {
      type: Array,
      default: function _default() {
        return null;
      }
    }
  },
  methods: {
    markAsCompleted: function markAsCompleted() {
      var _this = this;

      var requestData = _.pick(this.value, ["cost_per_tester", "allocated_tester", "total_cost", "name"]);
      requestData.status = "COMPLETED";
      axios.put("/api/projects/" + this.value.id, requestData).then(function (_ref) {
        var data = _ref.data.data;

        _this.$emit("onCompleted");
      }).catch(function (error) {
        _this.errors = error.response.data.message;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/project-tester-ratings.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_src_services_auth__ = __webpack_require__("./resources/assets/services/auth.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__star_rating__ = __webpack_require__("./resources/assets/components/components/star-rating.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__star_rating___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__star_rating__);
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



var defaultRating = {
  speed: 0,
  quality: 0,
  professionalism: 0
};
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "projectTesterRatings",
  components: {
    StarRating: __WEBPACK_IMPORTED_MODULE_1__star_rating___default.a
  },
  props: {
    project: {
      type: Object,
      required: true
    },
    exclude: {
      type: Array,
      default: function _default() {
        return [];
      }
    }
  },
  data: function data() {
    return {
      currentRatingTester: null,
      ratings: _extends({}, defaultRating),
      alreadyRatedUsers: [],
      alreadyPaidTesters: [],
      paymentProcessingTesterId: null,
      isUserAdmin: false
    };
  },

  methods: {
    giveRating: function giveRating(tester) {
      this.currentRatingTester = tester;
      return this.$refs.rating_modal.show();
    },
    resetRatings: function resetRatings() {
      this.ratings = _extends({}, defaultRating);
    },
    handleRateSubmit: function handleRateSubmit() {
      var _this = this;

      axios.post("/api/projects/" + this.project.id + "/rating/" + this.currentRatingTester.id, { ratings: this.avgRating }).then(function (_ref) {
        var data = _ref.data.data;

        _this.alreadyRatedUsers = [].concat(_toConsumableArray(_this.alreadyRatedUsers), [_extends({}, data, { tester_id: _this.currentRatingTester.id })]);
        _this.$emit("onRating", _extends({}, _this.currentRatingTester, {
          ratings: _this.avgRating
        }));
        _this.currentRatingTester = null;
        _this.resetRatings();
      });
      this.$refs.rating_modal.hide();
    },
    confirmAndPayToTester: function confirmAndPayToTester(tester) {
      var _this2 = this;

      return this.$swal({
        title: "Are you sure?",
        text: "You want to send $" + this.project.cost_per_tester + " to " + tester.first_name + "?",
        icon: "info",
        buttons: true,
        dangerMode: true
      }).then(function (willDelete) {
        if (willDelete) {
          _this2.payTester(tester);
        }
      });
    },
    payTester: function payTester(tester) {
      var _this3 = this;

      axios.post("/api/tester-earnings", {
        amount: this.project.cost_per_tester,
        note: "" + this.project.name,
        is_bonus: false, // later ask for bonus if required
        tester_id: tester.id,
        project_id: this.project.id
      }).then(function () {
        _this3.alreadyPaidTesters = [].concat(_toConsumableArray(_this3.alreadyPaidTesters), [tester.id]);
        _this3.paymentProcessingTesterId = null;
        _this3.$swal({
          title: "Success!",
          text: tester.first_name + "'s account has credited with $" + _this3.project.cost_per_tester,
          icon: "success"
        });
      }).catch(function (error) {
        _this3.paymentProcessingTesterId = null;
        _this3.$swal({
          title: "Fail!",
          text: error.response.data.message,
          icon: "error"
        });
      });
    },
    getTesterRating: function getTesterRating(tester) {
      var i = this.alreadyRatedUsers.filter(function (_ref2) {
        var tester_id = _ref2.tester_id;
        return tester_id === tester.id;
      }).pop();
      if (i) {
        return i.score;
      }
      return 0;
    }
  },
  computed: {
    rateModalTitle: function rateModalTitle() {
      var currentRatingTester = this.currentRatingTester || {};
      return "Rate Tester " + currentRatingTester.first_name + " " + currentRatingTester.last_name;
    },
    avgRating: function avgRating() {
      var _this4 = this;

      return Object.keys(this.ratings).reduce(function (t, k) {
        return t + _this4.ratings[k];
      }, 0) / Object.keys(this.ratings).length;
    },
    excludedUsers: function excludedUsers() {
      return (this.alreadyRatedUsers || []).map(function (_ref3) {
        var tester_id = _ref3.tester_id;
        return tester_id;
      }).map(function (d) {
        return d;
      });
    }
  },
  mounted: function mounted() {
    var _this5 = this;

    this.isUserAdmin = Object(__WEBPACK_IMPORTED_MODULE_0_src_services_auth__["c" /* isAdmin */])();
    axios.get("/api/projects/" + this.project.id + "/rated").then(function (_ref4) {
      var data = _ref4.data;
      return _this5.alreadyRatedUsers = data;
    });
    axios.get("/api/projects/" + this.project.id + "/paid").then(function (_ref5) {
      var data = _ref5.data;
      return _this5.alreadyPaidTesters = data;
    });
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/publisher/file-viewer.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "FileViewer",
  props: {
    files: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    colorTheme: {
      type: String,
      default: function _default() {
        return 'publisher';
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/star-rating.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "StarRating",
  props: {
    value: {
      default: 0
    },
    disabled: {
      default: false
    },
    small: {
      default: false
    }
  },
  data: function data() {
    return {
      maxStars: 5,
      currentRating: 0,
      totalStars: []
    };
  },
  mounted: function mounted() {
    this.currentRating = this.value;
    for (var i = 1; i <= this.maxStars; i++) {
      this.totalStars.push(i);
    }
  },

  watch: {
    value: function value(newVal, oldVal) {
      this.currentRating = newVal;
    }
  },
  methods: {
    setRating: function setRating(v) {
      if (this.disabled) {
        return true;
      }
      this.currentRating = v;
    },
    selected: function selected(v) {
      if (this.disabled) {
        return true;
      }
      this.setRating(v);
      this.$emit("input", v);
      this.$emit("change", v);
    }
  },
  computed: {
    displayRating: function displayRating() {
      if (this.totalStars.indexOf(this.currentRating) >= 0) {
        return this.currentRating;
      }
      if (/\.5$/.test(this.currentRating.toString())) {
        return this.currentRating;
      }
      return Math.floor(this.currentRating);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/projects/view.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_components_components_device_selector__ = __webpack_require__("./resources/assets/components/components/device-selector.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_components_components_device_selector___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_components_components_device_selector__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_components_components_project_cost__ = __webpack_require__("./resources/assets/components/components/project-cost.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_components_components_project_cost___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_components_components_project_cost__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_components_components_project_statistics__ = __webpack_require__("./resources/assets/components/components/project-statistics.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_components_components_project_statistics___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_components_components_project_statistics__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_components_components_project_tester_ratings__ = __webpack_require__("./resources/assets/components/components/project-tester-ratings.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_components_components_project_tester_ratings___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_components_components_project_tester_ratings__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_components_components_publisher_file_viewer__ = __webpack_require__("./resources/assets/components/components/publisher/file-viewer.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_components_components_publisher_file_viewer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_components_components_publisher_file_viewer__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_src_services_project__ = __webpack_require__("./resources/assets/services/project.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//








/* harmony default export */ __webpack_exports__["default"] = ({
  name: "view_project",
  components: {
    DeviceSelector: __WEBPACK_IMPORTED_MODULE_0_components_components_device_selector___default.a,
    ProjectCost: __WEBPACK_IMPORTED_MODULE_1_components_components_project_cost___default.a,
    ProjectStatistics: __WEBPACK_IMPORTED_MODULE_2_components_components_project_statistics___default.a,
    ProjectTesterRatings: __WEBPACK_IMPORTED_MODULE_3_components_components_project_tester_ratings___default.a,
    FileViewer: __WEBPACK_IMPORTED_MODULE_4_components_components_publisher_file_viewer___default.a
  },
  data: function data() {
    return {
      testproject: {},
      formstate: {},
      model: {},
      originalUser: {},
      publisher_name: "",
      type_name: "",
      isProjectApproved: false,
      isProjectCompleted: false,
      errors: null,
      ratedUsers: []
    };
  },

  methods: {
    projectCompleted: function projectCompleted() {
      this.getProject();
    },
    approveProject: function approveProject() {
      var _this = this;

      var requestData = _.pick(this.testproject, ["cost_per_tester", "allocated_tester", "total_cost", "name"]);
      requestData.status = "PENDING_PAYMENT";
      this.errors = null;
      return axios.put("/api/projects/" + this.testproject.id, requestData).then(function (_ref) {
        var data = _ref.data.data;

        _this.testproject = data;
        _this.isProjectApproved = true;
        _this.$router.push({
          name: "admin.project.invoice",
          params: { id: _this.testproject.id }
        });
      }).catch(function (error) {
        if (Array.isArray(error.response.data.message)) {
          _this.errors = error.response.data.message;
        } else {
          _this.errors = [error.response.data.message];
        }
      });
    },
    getProject: function getProject() {
      var _this2 = this;

      axios.get("/api/projects/" + this.$route.params.id).then(function (_ref2) {
        var data = _ref2.data.data;

        _this2.testproject = data;
        _this2.isProjectApproved = Object(__WEBPACK_IMPORTED_MODULE_5_src_services_project__["a" /* isProjectApproved */])(data.status.name);
        _this2.isProjectCompleted = Object(__WEBPACK_IMPORTED_MODULE_5_src_services_project__["b" /* isProjectCompleted */])(data.status.name);
        _this2.type_name = _this2.testproject.project_type.name;
        _this2.publisher_name = _this2.testproject.publisher.name ? _this2.testproject.publisher.name : _this2.testproject.publisher.first_name + " " + _this2.testproject.publisher.last_name;
        _this2.testproject.selected_devices = data.devices.map(function (_ref3) {
          var id = _ref3.id;
          return id;
        });
      }).catch(function (error) {});
    }
  },
  mounted: function mounted() {
    this.getProject();
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0a203d4d\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/projects/view.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.text-bold[data-v-0a203d4d] {\n  font-family: \"BrandonTextBold\" !important;\n}\n.text-medium[data-v-0a203d4d] {\n  font-family: \"BrandonTextMedium\";\n}\n.text-blue[data-v-0a203d4d] {\n  color: #0082cc;\n}\n.orange-info[data-v-0a203d4d] {\n  color: #fff;\n  background-color: #ff7802;\n  border-radius: 4px;\n  padding: 10px;\n  font-size: 16px;\n}\n.orange-outline-info[data-v-0a203d4d] {\n  background-color: #fff;\n  color: #ff7802;\n  border: 2px solid #ff7802;\n  border-radius: 4px;\n  padding: 10px;\n  font-size: 16px;\n}\n.green-info[data-v-0a203d4d] {\n  color: #fff;\n  background-color: #2cac3d;\n  border-radius: 4px;\n  padding: 10px;\n  font-size: 16px;\n}\n.white-box[data-v-0a203d4d] {\n  background-color: #fff;\n  -webkit-box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n          box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n  border-radius: 4px;\n  padding: 20px;\n  margin-bottom: 20px;\n  font-size: 14px;\n  line-height: 22px;\n  color: #606368;\n}\n.white-box .block-title[data-v-0a203d4d] {\n    font-size: 18px;\n    font-family: \"UniNeueBold\";\n    border-bottom: 1px solid #dadada;\n    color: #3e3a94;\n    padding-bottom: 10px;\n    margin-bottom: 10px;\n    width: auto;\n}\n.white-box .project-link[data-v-0a203d4d] {\n    color: #0082cc;\n    text-decoration: underline !important;\n}\n.back-proj-btn[data-v-0a203d4d] {\n  max-width: 160px;\n  min-width: 160px;\n  min-height: 37px;\n  margin-right: 15px;\n}\n.approve-btn[data-v-0a203d4d] {\n  max-width: 160px;\n  min-height: 35px;\n  min-width: 160px;\n}\n.purple-btn[data-v-0a203d4d] {\n  background-color: #5651b9;\n  border: 2px solid #3e3a94;\n  color: #fff;\n  font-size: 15px;\n  line-height: 16px;\n  font-family: \"BrandonTextMedium\";\n  border-radius: 6px;\n  padding: 8px 10px;\n  -webkit-transition: all 0.4s ease;\n  transition: all 0.4s ease;\n  min-height: 25px;\n  margin-right: 5px;\n}\n.purple-btn[data-v-0a203d4d]:hover {\n    outline: none !important;\n    background-color: #3e3a94;\n}\n.purple-btn[data-v-0a203d4d]:focus {\n    outline: none !important;\n}\n.grey-btn[data-v-0a203d4d] {\n  margin-right: 5px;\n  width: auto;\n  padding: 5px 10px;\n  border: 2px solid #606368;\n  background-color: #898a8c;\n  font-size: 14px;\n  font-family: \"BrandonTextMedium\";\n  letter-spacing: 0.05rem;\n  color: #fff !important;\n  border-radius: 6px;\n  text-align: center;\n}\n.grey-btn[data-v-0a203d4d]:hover {\n    background-color: #95979a;\n}\n@media screen and (max-width: 991px) {\n.white-box[data-v-0a203d4d] {\n    padding: 15px;\n}\n}\n@media screen and (max-width: 767px) {\n.orange-info[data-v-0a203d4d], .orange-outline-info[data-v-0a203d4d], .green-info[data-v-0a203d4d] {\n    padding: 5px;\n    font-size: 14px;\n    line-height: 18px;\n}\n}\n@media screen and (max-width: 575px) {\n.white-box .block-title[data-v-0a203d4d] {\n    font-size: 16px;\n}\n.grey-btn[data-v-0a203d4d], .purple-btn[data-v-0a203d4d] {\n    padding: 4px 5px;\n    display: block;\n    max-width: 120px;\n    margin: 10px auto;\n    min-height: 30px;\n    max-height: 30px;\n}\n.back-proj-btn[data-v-0a203d4d] {\n    max-width: 125px;\n    min-width: 125px;\n}\n.approve-btn[data-v-0a203d4d] {\n    max-width: 125px;\n    min-width: 125px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/device-selector.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\nimg[data-v-0d653902] {\n  height: 50px;\n  width: 50px;\n  display: inline-block;\n  margin-right: 10px;\n  float: left;\n  opacity: 0.5;\n  cursor: pointer;\n}\nimg.disabled[data-v-0d653902] {\n  cursor: auto;\n}\nimg.selected[data-v-0d653902] {\n  opacity: 1;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-323b1e04\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/star-rating.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\nspan[data-v-323b1e04] {\n  font-size: 12px;\n}\n.starElement[data-v-323b1e04] {\n  font-size: 18px;\n  margin-right: 12px;\n  color: black;\n}\n.starElement.small[data-v-323b1e04] {\n  font-size: 12px;\n  margin-right: 5px;\n}\n.starElement.fa-star[data-v-323b1e04],\n.starElement.fa-star-half-o[data-v-323b1e04] {\n  color: #ffd055;\n}\n.starElement.clickable[data-v-323b1e04] {\n  cursor: pointer;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3adc2ac0\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project-tester-ratings.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.purple-btn[data-v-3adc2ac0] {\n  background-color: #5651b9;\n  border: 2px solid #3e3a94;\n  color: #fff !important;\n  font-size: 15px;\n  line-height: 16px;\n  font-family: \"BrandonTextMedium\";\n  border-radius: 6px;\n  padding: 3px 6px;\n  -webkit-transition: all 0.4s ease;\n  transition: all 0.4s ease;\n  cursor: pointer;\n}\n.purple-btn[data-v-3adc2ac0]:hover {\n    outline: none !important;\n    background-color: #6b65e0;\n}\n.purple-btn[data-v-3adc2ac0]:focus {\n    outline: none !important;\n}\n.statistics-info-wrap[data-v-3adc2ac0] {\n  border-right: 1px solid #cecece;\n}\n.text-bold[data-v-3adc2ac0] {\n  font-family: \"BrandonTextBold\" !important;\n}\n.text-medium[data-v-3adc2ac0] {\n  font-family: \"BrandonTextMedium\";\n}\n.text-blue[data-v-3adc2ac0] {\n  color: #0082cc;\n}\n.text-purple[data-v-3adc2ac0] {\n  color: #363e48;\n}\n.white-box[data-v-3adc2ac0] {\n  background-color: #fff;\n  -webkit-box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n          box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n  border-radius: 4px;\n  padding: 20px;\n  margin-bottom: 20px;\n  font-size: 14px;\n  line-height: 22px;\n  color: #606368;\n}\n.white-box .block-title[data-v-3adc2ac0] {\n    font-size: 18px;\n    font-family: \"UniNeueBold\";\n    border-bottom: 1px solid #dadada;\n    color: #0082cc;\n    padding-bottom: 10px;\n    margin-bottom: 10px;\n    width: auto;\n}\n.white-box .block-subtitle[data-v-3adc2ac0] {\n    font-size: 20px;\n    font-family: \"BrandonTextBold\" !important;\n    border-bottom: 1px solid #dadada;\n    color: #0082cc;\n    padding-bottom: 10px;\n    margin-bottom: 10px;\n    width: auto;\n}\n.white-box .mark-comple-btn[data-v-3adc2ac0] {\n    float: right;\n    margin-top: 15px;\n}\n.blue-btn[data-v-3adc2ac0] {\n  width: auto;\n  padding: 8px 15px;\n  border: 2px solid #0082cc;\n  background-color: #00aff5;\n  font-size: 14px;\n  font-family: \"BrandonTextMedium\";\n  letter-spacing: 0.05rem;\n  color: #fff;\n  border-radius: 20px;\n  display: block;\n  text-align: center;\n  color: #fff;\n}\n.blue-btn[data-v-3adc2ac0]:hover {\n    background-color: #13b9fb;\n}\n.purple-btn[data-v-3adc2ac0] {\n  background-color: #5651b9;\n  border: 2px solid #3e3a94;\n  color: #fff !important;\n  font-size: 15px;\n  line-height: 16px;\n  font-family: \"BrandonTextMedium\";\n  border-radius: 25px;\n  padding: 5px 10px;\n  -webkit-transition: all 0.4s ease;\n  transition: all 0.4s ease;\n  cursor: pointer;\n}\n.purple-btn[data-v-3adc2ac0]:hover {\n    outline: none !important;\n    text-decoration: underline !important;\n    background-color: #6b65e0;\n}\n.purple-btn[data-v-3adc2ac0]:focus {\n    outline: none !important;\n}\n@media screen and (max-width: 991px) {\n.white-box[data-v-3adc2ac0] {\n    padding: 15px;\n}\n}\n@media screen and (max-width: 767px) {\n.statistics-info-wrap[data-v-3adc2ac0] {\n    border-right: none;\n}\n}\n@media screen and (max-width: 575px) {\n.white-box .block-title[data-v-3adc2ac0] {\n    font-size: 16px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6ab03afb\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project-statistics.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.text-bold[data-v-6ab03afb] {\n  font-family: \"BrandonTextBold\" !important;\n}\n.text-medium[data-v-6ab03afb] {\n  font-family: \"BrandonTextMedium\";\n}\n.text-blue[data-v-6ab03afb] {\n  color: #0082cc;\n}\n.text-purple[data-v-6ab03afb] {\n  color: #363e48;\n}\n.white-box[data-v-6ab03afb] {\n  background-color: #fff;\n  -webkit-box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n          box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n  border-radius: 4px;\n  padding: 15px;\n  margin-bottom: 20px;\n  font-size: 14px;\n  line-height: 22px;\n  color: #606368;\n}\n.white-box .block-title[data-v-6ab03afb] {\n    font-size: 18px;\n    font-family: \"UniNeueBold\";\n    border-bottom: 1px solid #dadada;\n    color: #0082cc;\n    padding-bottom: 10px;\n    margin-bottom: 10px;\n    width: auto;\n}\n.white-box .block-subtitle[data-v-6ab03afb] {\n    font-size: 20px;\n    font-family: \"BrandonTextBold\" !important;\n    border-bottom: 1px solid #dadada;\n    color: #0082cc;\n    padding-bottom: 10px;\n    margin-bottom: 10px;\n    width: auto;\n}\n.white-box .mark-comple-btn[data-v-6ab03afb] {\n    float: right;\n    margin-top: 15px;\n}\n.blue-btn[data-v-6ab03afb] {\n  width: auto;\n  padding: 8px 15px;\n  border: 2px solid #0082cc;\n  background-color: #00aff5;\n  font-size: 14px;\n  font-family: \"BrandonTextMedium\";\n  letter-spacing: 0.05rem;\n  color: #fff;\n  border-radius: 20px;\n  display: block;\n  text-align: center;\n  color: #fff;\n}\n.blue-btn[data-v-6ab03afb]:hover {\n    background-color: #13b9fb;\n}\n.purple-btn[data-v-6ab03afb] {\n  background-color: #5651b9;\n  border: 2px solid #3e3a94;\n  color: #fff !important;\n  font-size: 15px;\n  line-height: 16px;\n  font-family: \"BrandonTextMedium\";\n  border-radius: 6px;\n  padding: 3px 6px;\n  -webkit-transition: all 0.4s ease;\n  transition: all 0.4s ease;\n  cursor: pointer;\n}\n.purple-btn[data-v-6ab03afb]:hover {\n    outline: none !important;\n    background-color: #6b65e0;\n}\n.purple-btn[data-v-6ab03afb]:focus {\n    outline: none !important;\n}\n@media screen and (max-width: 991px) {\n.white-box[data-v-6ab03afb] {\n    padding: 15px;\n}\n.purple-btn[data-v-6ab03afb] {\n    padding: 3px 5px;\n    font-size: 14px;\n}\n}\n@media screen and (max-width: 991px) {\n.purple-btn[data-v-6ab03afb] {\n    padding: 3px 10px;\n}\n}\n@media screen and (max-width: 575px) {\n.white-box .block-title[data-v-6ab03afb] {\n    font-size: 16px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-788664a5\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project-cost.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.text-yellow[data-v-788664a5] {\n  color: #ffcc00;\n}\n.text-orange[data-v-788664a5] {\n  color: #ff8a0e;\n}\n.text-green[data-v-788664a5] {\n  color: #02c505;\n}\n.text-darkgreen[data-v-788664a5] {\n  color: #2cac3d;\n}\n.text-red[data-v-788664a5] {\n  color: #f83636;\n}\n.text-bold[data-v-788664a5] {\n  font-family: \"BrandonTextBold\";\n}\n.white-box .block-title[data-v-788664a5] {\n  font-size: 18px;\n  font-family: \"UniNeueBold\";\n  border-bottom: 1px solid #dadada;\n  color: #3e3a94;\n  padding-bottom: 10px;\n  margin-bottom: 10px;\n  width: auto;\n}\n.white-box .project-block-wrap[data-v-788664a5] {\n  margin-top: 5px;\n  font-family: \"BrandonTextRegular\";\n  font-size: 16px;\n  color: #606368;\n  position: relative;\n}\n.white-box .project-block-wrap p[data-v-788664a5] {\n    margin-bottom: 25px;\n}\n.white-box .project-block-wrap a[data-v-788664a5] {\n    color: #14c1bf;\n    text-decoration: underline !important;\n}\n.white-box .project-block-wrap .proj-amt[data-v-788664a5] {\n    font-size: 25px;\n    font-family: \"BrandonTextBlack\";\n}\n.white-box .project-block-wrap .tester-amt[data-v-788664a5] {\n    font-family: \"BrandonTextBlack\";\n    font-size: 50px;\n}\n.white-box .project-block-wrap .form-group[data-v-788664a5] {\n    max-width: 80%;\n    margin: 0 auto;\n    display: block;\n}\n.white-box .project-block-wrap .amt-block-wrap[data-v-788664a5] {\n    position: relative;\n    border-right: 1px solid #dadada;\n}\n@media screen and (max-width: 1199px) {\n.white-box .project-block-wrap p[data-v-788664a5] {\n    margin-bottom: 20px;\n}\n.white-box .project-block-wrap .proj-amt[data-v-788664a5] {\n    font-size: 22px;\n}\n.white-box .project-block-wrap .form-group[data-v-788664a5] {\n    margin: 0 auto;\n    max-width: 80%;\n}\n}\n@media screen and (max-width: 1112px) {\n.white-box .project-block-wrap[data-v-788664a5] {\n    font-size: 14px;\n    margin-top: 10px;\n}\n.white-box .project-block-wrap .proj-title[data-v-788664a5] {\n      font-size: 18px;\n      margin-bottom: 5px;\n}\n}\n@media screen and (max-width: 767px) {\n.white-box .project-block-wrap p[data-v-788664a5] {\n    margin-bottom: 5px;\n}\n.white-box .project-block-wrap .amt-block-wrap[data-v-788664a5] {\n    border: none;\n}\n.white-box .project-block-wrap .tester-amt[data-v-788664a5] {\n    font-size: 32px;\n    line-height: 20px;\n}\n.white-box .project-block-wrap .form-group[data-v-788664a5] {\n    margin: 0;\n    max-width: 50%;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e55f98a2\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/file-viewer.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.publisher[data-v-e55f98a2] {\n  background: #fff;\n}\n.file[data-v-e55f98a2] {\n  padding: 0px;\n  color: #606368;\n  text-decoration: none;\n  margin-top: 3px;\n  border-radius: 4px;\n  margin-left: 0px;\n}\n.file a[data-v-e55f98a2] {\n    color: #606368;\n    padding: 0;\n}\n.file a[data-v-e55f98a2]:hover {\n      text-decoration: underline !important;\n}\n.tester[data-v-e55f98a2] {\n  background: #fff;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-0a203d4d\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/projects/view.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        { staticClass: "col-md-12" },
        [
          _vm.isProjectApproved
            ? _c(
                "div",
                { staticClass: "text-medium text-center orange-info mb-3" },
                [
                  _vm._v(
                    " \n        Payment received. project is running since " +
                      _vm._s(_vm._f("date")(_vm.testproject.start_date)) +
                      "\n      "
                  )
                ]
              )
            : _vm._e(),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass:
                "text-medium text-center orange-outline-info mb-3 d-none"
            },
            [_vm._v("\n        status = waiting for approval\n      ")]
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "text-medium text-center green-info mb-3 d-none" },
            [
              _vm._v(
                "\n        Project is completed on " +
                  _vm._s(_vm._f("date")(_vm.testproject.end_date)) +
                  "\n      "
              )
            ]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "white-box" }, [
            _vm._m(0),
            _vm._v(" "),
            _c("div", { staticClass: "row pb-3 pb-md-4" }, [
              _c("div", { staticClass: "col-md-5" }, [
                _c("div", { staticClass: "mb-1" }, [
                  _c(
                    "span",
                    {
                      staticClass:
                        "d-block d-sm-inline-block align-middle text-bold"
                    },
                    [
                      _vm._v(
                        "\n                Publisher Name :\n              "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "span",
                    { staticClass: "d-block d-sm-inline-block align-middle" },
                    [
                      _vm._v(
                        "\n                " +
                          _vm._s(_vm.publisher_name) +
                          "\n              "
                      )
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "mb-1" }, [
                  _c(
                    "span",
                    {
                      staticClass:
                        "d-block d-sm-inline-block align-middle text-bold"
                    },
                    [
                      _vm._v(
                        "\n                Project Title : \n              "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "span",
                    { staticClass: "d-block d-sm-inline-block align-middle" },
                    [
                      _vm._v(
                        "\n                " +
                          _vm._s(this.testproject.name) +
                          "\n              "
                      )
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "mb-1" }, [
                  _c(
                    "span",
                    {
                      staticClass:
                        "d-block d-sm-inline-block align-middle text-bold"
                    },
                    [_vm._v("\n                Project Type :\n              ")]
                  ),
                  _vm._v(" "),
                  _c(
                    "span",
                    { staticClass: "d-block d-sm-inline-block align-middle" },
                    [
                      _vm._v(
                        "\n                " +
                          _vm._s(_vm.type_name) +
                          "\n              "
                      )
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "mb-1" }, [
                  _c(
                    "span",
                    {
                      staticClass:
                        "d-block d-sm-inline-block align-middle text-bold"
                    },
                    [
                      _vm._v(
                        "\n                Project Total Amount :\n              "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "span",
                    { staticClass: "d-block d-sm-inline-block align-middle" },
                    [
                      _vm._v(
                        "\n                " +
                          _vm._s(
                            _vm.testproject.total_cost
                              ? "$" + _vm.testproject.total_cost
                              : "-"
                          ) +
                          "\n              "
                      )
                    ]
                  )
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-4" }, [
                _c("div", { staticClass: "mb-1" }, [
                  _c(
                    "span",
                    {
                      staticClass:
                        "d-block d-sm-inline-block align-middle text-bold"
                    },
                    [_vm._v("\n                Start Date :\n              ")]
                  ),
                  _vm._v(" "),
                  _c(
                    "span",
                    { staticClass: "d-block d-sm-inline-block align-middle" },
                    [
                      _vm._v(
                        "\n                " +
                          _vm._s(_vm._f("date")(_vm.testproject.start_date)) +
                          "\n              "
                      )
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "mb-1" }, [
                  _c(
                    "span",
                    {
                      staticClass:
                        "d-block d-sm-inline-block align-middle text-bold"
                    },
                    [_vm._v("\n                End Date :\n              ")]
                  ),
                  _vm._v(" "),
                  _c(
                    "span",
                    { staticClass: "d-block d-sm-inline-block align-middle" },
                    [
                      _vm._v(
                        "\n                " +
                          _vm._s(_vm._f("date")(_vm.testproject.end_date)) +
                          "\n              "
                      )
                    ]
                  )
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-3" }, [
                _c("div", { staticClass: "mb-1" }, [
                  _c(
                    "span",
                    {
                      staticClass:
                        "d-block d-sm-inline-block align-middle text-bold"
                    },
                    [
                      _vm._v(
                        "\n                Time to Test (Approx) :\n              "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "span",
                    { staticClass: "d-block d-sm-inline-block align-middle" },
                    [
                      _vm._v(
                        "\n                " +
                          _vm._s(_vm.testproject.estimate_tester_time) +
                          " hrs\n              "
                      )
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "mb-1" }, [
                  _c(
                    "span",
                    {
                      staticClass:
                        "d-block d-sm-inline-block align-middle text-bold"
                    },
                    [
                      _vm._v(
                        "\n                Tester Needed :\n              "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "span",
                    { staticClass: "d-block d-sm-inline-block align-middle" },
                    [
                      _vm._v(
                        "\n                " +
                          _vm._s(
                            _vm.testproject.allocated_tester ||
                              _vm.testproject.tester_needed
                          ) +
                          "\n              "
                      )
                    ]
                  )
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row pb-3 pb-md-4" }, [
              _c("div", { staticClass: "col-12" }, [
                _c("h4", { staticClass: "block-title" }, [
                  _vm._v("\n              Project Description\n            ")
                ]),
                _vm._v(" "),
                _c("p", [
                  _vm._v(
                    "\n              " +
                      _vm._s(this.testproject.project_description) +
                      " \n            "
                  )
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row pb-3 pb-md-4" }, [
              _c(
                "div",
                { staticClass: "col-md-6 pb-md-0 pb-3" },
                [
                  _c("h4", { staticClass: "block-title" }, [
                    _vm._v("\n                Links\n            ")
                  ]),
                  _vm._v(" "),
                  _vm._l(_vm.testproject.links, function(val) {
                    return _c("div", { staticClass: "mb-1" }, [
                      _c(
                        "span",
                        {
                          staticClass:
                            "d-block d-sm-inline-block align-middle text-bold"
                        },
                        [
                          _vm._v(
                            "\n                " +
                              _vm._s(val.title) +
                              " :\n              "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "span",
                        {
                          staticClass: "d-block d-sm-inline-block align-middle"
                        },
                        [
                          _c(
                            "a",
                            { attrs: { href: val.link, target: "_blank" } },
                            [_vm._v(_vm._s(val.link))]
                          )
                        ]
                      )
                    ])
                  })
                ],
                2
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "col-md-6" },
                [
                  _c("h4", { staticClass: "block-title" }, [
                    _vm._v("\n                Files\n            ")
                  ]),
                  _vm._v(" "),
                  _c("file-viewer", { attrs: { files: _vm.testproject.media } })
                ],
                1
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-md-6 pb-md-0 pb-3" }, [
                _c("h4", { staticClass: "block-title" }, [
                  _vm._v("\n                Notes / Instruction\n            ")
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "mb-1" }, [
                  _vm._v(
                    "\n              " +
                      _vm._s(this.testproject.notes_instruction) +
                      "\n            "
                  )
                ])
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "col-md-6" },
                [
                  _c("h4", { staticClass: "block-title" }, [
                    _vm._v("\n                Required Devices\n            ")
                  ]),
                  _vm._v(" "),
                  _c("device-selector", {
                    attrs: { disabled: "" },
                    model: {
                      value: _vm.testproject.selected_devices,
                      callback: function($$v) {
                        _vm.$set(_vm.testproject, "selected_devices", $$v)
                      },
                      expression: "testproject.selected_devices"
                    }
                  })
                ],
                1
              )
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "white-box" }, [
            _c("div", { staticClass: "row" }, [
              _c(
                "div",
                { staticClass: "col-12" },
                [
                  _c("h4", { staticClass: "block-title" }, [
                    _vm._v("\n                Questions\n            ")
                  ]),
                  _vm._v(" "),
                  _c("p", [
                    _vm._v(
                      "\n              Your Questions to Testers which they have to fill up while submitting test Result\n            "
                    )
                  ]),
                  _vm._v(" "),
                  _vm._l(_vm.testproject.questions, function(ques, index) {
                    return _c("div", { key: index, staticClass: "mb-1" }, [
                      _c("span", { staticClass: "text-bold" }, [
                        _vm._v("Question " + _vm._s(index + 1) + " : ")
                      ]),
                      _vm._v(" "),
                      _c("span", { staticClass: "text-medium" }, [
                        _vm._v(" " + _vm._s(ques.question))
                      ])
                    ])
                  })
                ],
                2
              )
            ])
          ]),
          _vm._v(" "),
          _c("project-cost", {
            attrs: { errors: _vm.errors, disabled: _vm.isProjectApproved },
            model: {
              value: _vm.testproject,
              callback: function($$v) {
                _vm.testproject = $$v
              },
              expression: "testproject"
            }
          }),
          _vm._v(" "),
          this.isProjectApproved
            ? _c("project-statistics", {
                attrs: { errors: _vm.errors },
                on: { onCompleted: _vm.projectCompleted },
                model: {
                  value: _vm.testproject,
                  callback: function($$v) {
                    _vm.testproject = $$v
                  },
                  expression: "testproject"
                }
              })
            : _vm._e(),
          _vm._v(" "),
          _vm.isProjectCompleted
            ? _c("project-tester-ratings", {
                attrs: { exclude: _vm.ratedUsers, project: _vm.testproject }
              })
            : _vm._e(),
          _vm._v(" "),
          _c("div", { staticClass: "col-12 text-center p-0" }, [
            _vm.isProjectApproved
              ? _c(
                  "div",
                  [
                    _c(
                      "router-link",
                      {
                        staticClass: "btn grey-btn",
                        attrs: { to: { name: "admin.projects" } }
                      },
                      [_vm._v("Download PDF")]
                    ),
                    _vm._v(" "),
                    _c(
                      "router-link",
                      {
                        staticClass: "btn purple-btn",
                        attrs: {
                          to: {
                            name: "admin.project.invoice",
                            params: { id: _vm.testproject.id }
                          }
                        }
                      },
                      [_vm._v("View Invoice")]
                    ),
                    _vm._v(" "),
                    _c(
                      "a",
                      { staticClass: "btn grey-btn", attrs: { href: "#" } },
                      [_vm._v("View Testers")]
                    ),
                    _vm._v(" "),
                    _c(
                      "router-link",
                      {
                        staticClass: "btn grey-btn",
                        attrs: { to: { name: "admin.projects" } }
                      },
                      [_vm._v("View Activites")]
                    )
                  ],
                  1
                )
              : _c(
                  "div",
                  [
                    _c("p", [
                      _vm._v(
                        "Publisher will notify for the pending payment. We will inform you once publisher will pay to start project."
                      )
                    ]),
                    _vm._v(" "),
                    _c(
                      "router-link",
                      {
                        staticClass: "btn grey-btn back-proj-btn",
                        attrs: { to: { name: "admin.projects" } }
                      },
                      [_vm._v("Back To Project")]
                    ),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass: "btn purple-btn approve-btn",
                        class: { disable: _vm.isProjectApproved },
                        attrs: { type: "button" },
                        on: { click: _vm.approveProject }
                      },
                      [_vm._v("Approve")]
                    )
                  ],
                  1
                )
          ])
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12" }, [
        _c("h4", { staticClass: "block-title" }, [
          _vm._v("\n              Basic Information\n            ")
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0a203d4d", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-0d653902\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/device-selector.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    _vm._l(_vm.availableDevices, function(device) {
      return _c("span", { staticClass: "d-inline-block pr-3" }, [
        _c("img", {
          staticClass: "img-fluid",
          class: {
            selected: _vm.value.indexOf(device.id) !== -1,
            disabled: _vm.disabled
          },
          attrs: { title: device.name, src: device.icon },
          on: {
            click: function($event) {
              _vm.toggleItem(device)
            }
          }
        })
      ])
    })
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0d653902", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-323b1e04\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/star-rating.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "row" }, [
    _c(
      "div",
      { staticClass: "col-md-12" },
      [
        _vm._l(_vm.totalStars, function(i) {
          return _c("i", {
            key: i,
            staticClass: "starElement fa",
            class: {
              "fa-star-o": _vm.currentRating < i,
              "fa-star-half-o": _vm.currentRating + 0.5 == i,
              "fa-star": _vm.currentRating >= i,
              clickable: !_vm.disabled,
              small: _vm.small
            },
            on: {
              mouseover: function($event) {
                _vm.setRating(i)
              },
              mouseout: function($event) {
                _vm.setRating(_vm.value)
              },
              click: function($event) {
                _vm.selected(i)
              }
            }
          })
        }),
        _vm._v(" "),
        _vm.small ? _c("span", [_vm._v(_vm._s(_vm.displayRating))]) : _vm._e()
      ],
      2
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-323b1e04", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-3adc2ac0\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/project-tester-ratings.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "white-box" },
    [
      _vm._m(0),
      _vm._v(" "),
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-12 col-md-3 statistics-info-wrap" }, [
          _c("p", { staticClass: "mb-2 mb-sm-4" }, [
            _c("span", {}, [
              _vm._v("\n          Project Total Amount :\n        ")
            ]),
            _vm._v(" "),
            _c("span", { staticClass: "text-bold statistics-num" }, [
              _vm._v(
                "\n          $" + _vm._s(_vm.project.total_cost) + "\n        "
              )
            ])
          ]),
          _vm._v(" "),
          _c("p", { staticClass: "mb-2 mb-sm-4" }, [
            _c("span", {}, [_vm._v("\n          Total Testers :\n        ")]),
            _vm._v(" "),
            _c("span", { staticClass: "text-bold statistics-num" }, [
              _vm._v(
                "\n          " +
                  _vm._s(_vm.project.allocated_tester) +
                  "\n        "
              )
            ])
          ]),
          _vm._v(" "),
          _c("p", { staticClass: "mb-2 mb-sm-4" }, [
            _c("span", {}, [
              _vm._v("\n          Amount for each Tester:\n        ")
            ]),
            _vm._v(" "),
            _c("span", { staticClass: "text-bold statistics-num" }, [
              _vm._v(
                "\n          $" +
                  _vm._s(_vm.project.cost_per_tester) +
                  "\n        "
              )
            ])
          ]),
          _vm._v(" "),
          _vm._m(1),
          _vm._v(" "),
          _vm._m(2)
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-12 col-md-9" }, [
          _c(
            "table",
            { staticClass: "table" },
            [
              _c("thead", [
                _c("tr", [
                  _c("th", [_vm._v("Tester")]),
                  _vm._v(" "),
                  _c("th", [_vm._v("Given Rating")]),
                  _vm._v(" "),
                  _c("th", [_vm._v("Amount")]),
                  _vm._v(" "),
                  _vm.isUserAdmin ? _c("th", [_vm._v("Payment")]) : _vm._e()
                ])
              ]),
              _vm._v(" "),
              _vm._l(_vm.project.testers, function(tester) {
                return _c("tr", { key: tester.id }, [
                  _c(
                    "td",
                    [
                      _vm._v(
                        "\n            " +
                          _vm._s(tester.first_name) +
                          " " +
                          _vm._s(tester.last_name) +
                          "\n            "
                      ),
                      tester.rating
                        ? _c("star-rating", {
                            attrs: { disabled: true, small: true },
                            model: {
                              value: tester.rating,
                              callback: function($$v) {
                                _vm.$set(tester, "rating", $$v)
                              },
                              expression: "tester.rating"
                            }
                          })
                        : _vm._e()
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "td",
                    [
                      _vm.excludedUsers.indexOf(tester.id) === -1
                        ? _c(
                            "button",
                            {
                              staticClass: "purple-btn",
                              attrs: { type: "button" },
                              on: {
                                click: function($event) {
                                  _vm.giveRating(tester)
                                }
                              }
                            },
                            [_vm._v("Give Ratings")]
                          )
                        : _c("star-rating", {
                            attrs: {
                              disabled: true,
                              value: _vm.getTesterRating(tester),
                              small: true
                            }
                          })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("td", [_vm._v("$" + _vm._s(_vm.project.cost_per_tester))]),
                  _vm._v(" "),
                  _vm.isUserAdmin
                    ? _c("td", [
                        _vm.alreadyPaidTesters.indexOf(tester.id) === -1
                          ? _c(
                              "button",
                              {
                                staticClass: "purple-btn",
                                attrs: {
                                  type: "button",
                                  disabled:
                                    _vm.paymentProcessingTesterId === tester.id
                                },
                                on: {
                                  click: function($event) {
                                    _vm.confirmAndPayToTester(tester)
                                  }
                                }
                              },
                              [_vm._v("Pay To Tester")]
                            )
                          : _c(
                              "label",
                              { staticClass: "badge badge-success" },
                              [_vm._v("PAID")]
                            )
                      ])
                    : _vm._e()
                ])
              })
            ],
            2
          )
        ])
      ]),
      _vm._v(" "),
      _c(
        "b-modal",
        {
          ref: "rating_modal",
          attrs: { "cancel-disabled": true, title: _vm.rateModalTitle },
          on: { shown: _vm.resetRatings }
        },
        [
          _c(
            "div",
            { staticClass: "col-md-12 mb-2" },
            [
              _vm._v("\n      Completeness / Followed instructions\n      "),
              _c("star-rating", {
                model: {
                  value: _vm.ratings.speed,
                  callback: function($$v) {
                    _vm.$set(_vm.ratings, "speed", $$v)
                  },
                  expression: "ratings.speed"
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "col-md-12 mb-2" },
            [
              _vm._v("\n      Quality / Usefulness of feedback\n      "),
              _c("star-rating", {
                model: {
                  value: _vm.ratings.quality,
                  callback: function($$v) {
                    _vm.$set(_vm.ratings, "quality", $$v)
                  },
                  expression: "ratings.quality"
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "col-md-12 mb-2" },
            [
              _vm._v("\n      Professionalism\n      "),
              _c("star-rating", {
                model: {
                  value: _vm.ratings.professionalism,
                  callback: function($$v) {
                    _vm.$set(_vm.ratings, "professionalism", $$v)
                  },
                  expression: "ratings.professionalism"
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "col-md-12 mb-4 text-center" },
            [
              _vm._v("\n      Overall ratings\n      "),
              _c("star-rating", {
                attrs: { disabled: "" },
                model: {
                  value: _vm.avgRating,
                  callback: function($$v) {
                    _vm.avgRating = $$v
                  },
                  expression: "avgRating"
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c("div", { attrs: { slot: "modal-footer" }, slot: "modal-footer" }, [
            _c(
              "button",
              {
                staticClass: "purple-btn",
                attrs: { type: "button" },
                on: { click: _vm.handleRateSubmit }
              },
              [_vm._v("Submit")]
            )
          ])
        ]
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12" }, [
        _c("h4", { staticClass: "block-title" }, [
          _vm._v("Payment Details of Testers\n      ")
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "mb-2 mb-sm-4" }, [
      _c("span", {}, [_vm._v("\n          Total Payable Amount :\n        ")]),
      _vm._v(" "),
      _c("span", { staticClass: "text-bold statistics-num" }, [
        _vm._v("\n          $40\n        ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "mb-0" }, [
      _c("span", {}, [_vm._v("\n          Unpaid Amount :\n        ")]),
      _vm._v(" "),
      _c("span", { staticClass: "text-bold statistics-num" }, [
        _vm._v("\n          $10\n        ")
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-3adc2ac0", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-6ab03afb\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/project-statistics.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "white-box" }, [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12 col-md-3 statistics-info-wrap" }, [
        _c("p", { staticClass: "mb-2 mb-sm-4" }, [
          _c("span", {}, [_vm._v("\n          Tester Needed :\n        ")]),
          _vm._v(" "),
          _c("span", { staticClass: "text-bold statistics-num" }, [
            _vm._v(
              "\n          " + _vm._s(_vm.value.tester_needed) + "\n        "
            )
          ])
        ]),
        _vm._v(" "),
        _c("p", { staticClass: "mb-2 mb-sm-4" }, [
          _c("span", {}, [_vm._v("\n          Tester Submitted :\n        ")]),
          _vm._v(" "),
          _c("span", { staticClass: "text-bold statistics-num" }, [
            _vm._v(
              "\n          " +
                _vm._s(_vm.value.testers ? _vm.value.testers.length : 0) +
                "\n        "
            )
          ])
        ]),
        _vm._v(" "),
        _vm._m(1),
        _vm._v(" "),
        _vm._m(2),
        _vm._v(" "),
        _vm._m(3)
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-12 col-md-6" }, [
        _c("div", { staticClass: "filter-tester-wrap" }, [
          _c("div", { staticClass: "form-group mb-4" }, [
            _c("label", [_vm._v("Filter by Testers:")]),
            _vm._v(" "),
            _c(
              "select",
              { staticClass: "form-control", attrs: { multiple: "" } },
              _vm._l(_vm.value.testers, function(tester) {
                return _c("option", [
                  _vm._v(
                    _vm._s(tester.first_name) + " " + _vm._s(tester.last_name)
                  )
                ])
              })
            )
          ]),
          _vm._v(" "),
          _c("a", { staticClass: "purple-btn", attrs: { href: "#" } }, [
            _vm._v("\n          Filter\n        ")
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-12 col-md-3" }, [
        _vm._m(4),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c(
            "div",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value:
                    _vm.value.status && _vm.value.status.name != "Completed",
                  expression:
                    "value.status && value.status.name != 'Completed' "
                }
              ],
              staticClass: "col-12"
            },
            [
              _c(
                "a",
                {
                  staticClass: "purple-btn mark-comple-btn",
                  on: { click: _vm.markAsCompleted }
                },
                [_vm._v("Mark as Completed")]
              )
            ]
          )
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12" }, [
        _c("h4", { staticClass: "block-title" }, [_vm._v("Statistics\n      ")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "mb-2 mb-sm-4" }, [
      _c("span", {}, [_vm._v("\n          Ratings/score :\n        ")]),
      _vm._v(" "),
      _c("span", { staticClass: "text-bold statistics-num" }, [
        _vm._v("\n          88\n        ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "mb-2 mb-sm-4" }, [
      _c("span", {}, [_vm._v("\n          Issues :\n        ")]),
      _vm._v(" "),
      _c("span", { staticClass: "text-bold statistics-num" }, [
        _vm._v("\n          11\n        ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "mb-0" }, [
      _c("span", {}, [_vm._v("\n          Attachments :\n        ")]),
      _vm._v(" "),
      _c("span", { staticClass: "text-bold statistics-num" }, [
        _vm._v("\n          10\n        ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12 text-center" }, [
        _c("span", { staticClass: "text-bold float-right" }, [
          _vm._v("\n            Progress : 60%\n          ")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "float-right w-100 mt-2" }, [
          _c("img", {
            staticClass: "img-fluid float-right",
            attrs: { src: __webpack_require__("./resources/assets/assets/img/progress-bar.png") }
          })
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-6ab03afb", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-788664a5\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/project-cost.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "white-box" }, [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12" }, [
        _vm.errors
          ? _c("div", { staticStyle: { color: "red" } }, [
              _c(
                "ul",
                { staticClass: "pl-0", attrs: { id: "example-1" } },
                _vm._l(_vm.errors, function(error) {
                  return _c("li", [
                    _vm._v("\n          " + _vm._s(error) + "\n          ")
                  ])
                })
              )
            ])
          : _vm._e()
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "project-block-wrap" }, [
      _c("div", { staticClass: "row align-items-center" }, [
        _c(
          "div",
          {
            staticClass: "col-md-4 col-12 text-left amt-block-wrap mb-2 mb-md-0"
          },
          [
            _c("div", { staticClass: "mb-3" }, [
              _c(
                "span",
                {
                  staticClass:
                    "text-bold mr-2 d-block d-sm-inline-block align-middle"
                },
                [_vm._v("\n              Project cost :\n            ")]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "d-block d-sm-inline-block align-middle" },
                [
                  !_vm.disabled
                    ? _c("div", { staticClass: "input-group" }, [
                        _c("span", { staticClass: "input-group-addon" }, [
                          _vm._v("$")
                        ]),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.value.total_cost,
                              expression: "value.total_cost"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "number" },
                          domProps: { value: _vm.value.total_cost },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.value,
                                "total_cost",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ])
                    : _c("span", { staticClass: "text-bold proj-amt" }, [
                        _vm._v(
                          "\n                $" +
                            _vm._s(_vm.value.total_cost) +
                            "\n              "
                        )
                      ])
                ]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "mb-3" }, [
              _c(
                "span",
                {
                  staticClass:
                    "text-bold mr-2 d-block d-sm-inline-block align-middle"
                },
                [_vm._v("\n              Tester requested :\n            ")]
              ),
              _vm._v(" "),
              _c(
                "span",
                {
                  staticClass:
                    "text-bold  proj-amt d-block d-sm-inline-block align-middle"
                },
                [
                  _vm._v(
                    "\n             " +
                      _vm._s(_vm.value.tester_needed) +
                      "\n            "
                  )
                ]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "mb-0" }, [
              _c(
                "span",
                {
                  staticClass:
                    "text-bold d-inline-block mr-2 d-block d-sm-inline-block align-middle"
                },
                [_vm._v("\n              Tester assign : \n            ")]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "d-block d-sm-inline-block align-middle" },
                [
                  !_vm.disabled
                    ? _c("div", [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.value.allocated_tester,
                              expression: "value.allocated_tester"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "number" },
                          domProps: { value: _vm.value.allocated_tester },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.value,
                                "allocated_tester",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ])
                    : _c("span", { staticClass: "text-bold proj-amt" }, [
                        _vm._v(
                          "\n                " +
                            _vm._s(_vm.value.allocated_tester) +
                            "\n              "
                        )
                      ])
                ]
              )
            ])
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass:
              "col-md-4 col-12 text-md-center text-left amt-block-wrap"
          },
          [
            _c("p", { staticClass: "pt-md-3 mb-md-2 text-bold" }, [
              _vm._v("\n            Amount for each Tester :\n          ")
            ]),
            _vm._v(" "),
            !_vm.disabled
              ? _c(
                  "div",
                  { staticClass: "mb-md-5 mb-4 form-group text-center" },
                  [
                    _c("div", { staticClass: "input-group" }, [
                      _c("span", { staticClass: "input-group-addon" }, [
                        _vm._v("$")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.value.cost_per_tester,
                            expression: "value.cost_per_tester"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "number" },
                        domProps: { value: _vm.value.cost_per_tester },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.value,
                              "cost_per_tester",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ])
                  ]
                )
              : _c(
                  "span",
                  {
                    staticClass:
                      "text-purple tester-amt d-block mt-md-3 pb-md-4 pb-3"
                  },
                  [
                    _vm._v(
                      "\n              $" +
                        _vm._s(_vm.value.cost_per_tester) +
                        "\n            "
                    )
                  ]
                )
          ]
        ),
        _vm._v(" "),
        _vm._m(1)
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12" }, [
        _c("h4", { staticClass: "block-title" }, [
          _vm._v("\n          Amount for Testers\n      ")
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "col-md-4 col-12 text-left mb-2 mb-md-0" },
      [
        _c("div", { staticClass: "mb-3" }, [
          _c(
            "span",
            { staticClass: "text-bold d-block d-sm-inline-block align-middle" },
            [
              _vm._v(
                "\n              Total allocated for Testers :\n            "
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "span",
            {
              staticClass:
                "text-red proj-amt d-block d-sm-inline-block align-middle"
            },
            [_vm._v("\n              $50\n            ")]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "mb-0" }, [
          _c(
            "span",
            { staticClass: "text-bold d-block d-sm-inline-block align-middle" },
            [_vm._v("\n              Balance owed to Testers :\n            ")]
          ),
          _vm._v(" "),
          _c(
            "span",
            {
              staticClass:
                "text-darkgreen proj-amt d-block d-sm-inline-block align-middle"
            },
            [_vm._v("\n              $148\n            ")]
          )
        ])
      ]
    )
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-788664a5", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-e55f98a2\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/publisher/file-viewer.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "row" }, [
    _c(
      "div",
      { staticClass: "col-12" },
      _vm._l(_vm.files, function(file) {
        return _c("div", { key: file.id, class: ["file", _vm.colorTheme] }, [
          _c("a", { attrs: { href: file.url, target: "_blank" } }, [
            _vm._v(
              "\n     " +
                _vm._s(file.file_name) +
                " - " +
                _vm._s(_vm._f("fileSize")(file.size)) +
                "\n    "
            )
          ])
        ])
      })
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-e55f98a2", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0a203d4d\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/projects/view.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0a203d4d\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/projects/view.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("1fed6450", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0a203d4d\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./view.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0a203d4d\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./view.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/device-selector.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/device-selector.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("4b79eeec", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./device-selector.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./device-selector.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-323b1e04\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/star-rating.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-323b1e04\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/star-rating.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("456adb75", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-323b1e04\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./star-rating.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-323b1e04\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./star-rating.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3adc2ac0\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project-tester-ratings.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3adc2ac0\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project-tester-ratings.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("27cf89af", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3adc2ac0\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project-tester-ratings.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3adc2ac0\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project-tester-ratings.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6ab03afb\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project-statistics.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6ab03afb\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project-statistics.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("70471f82", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6ab03afb\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project-statistics.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6ab03afb\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project-statistics.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-788664a5\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project-cost.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-788664a5\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project-cost.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("fef68ebe", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-788664a5\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project-cost.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-788664a5\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project-cost.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e55f98a2\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/file-viewer.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e55f98a2\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/file-viewer.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("a93db18a", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e55f98a2\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./file-viewer.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e55f98a2\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./file-viewer.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/assets/img/progress-bar.png":
/***/ (function(module, exports) {

module.exports = "/images/progress-bar.png?9c04687ac2c36d110989faf38f2c44b0";

/***/ }),

/***/ "./resources/assets/components/components/device-selector.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/device-selector.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/device-selector.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-0d653902\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/device-selector.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-0d653902"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\device-selector.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0d653902", Component.options)
  } else {
    hotAPI.reload("data-v-0d653902", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/components/project-cost.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-788664a5\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project-cost.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/project-cost.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-788664a5\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/project-cost.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-788664a5"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\project-cost.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-788664a5", Component.options)
  } else {
    hotAPI.reload("data-v-788664a5", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/components/project-statistics.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6ab03afb\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project-statistics.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/project-statistics.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-6ab03afb\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/project-statistics.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-6ab03afb"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\project-statistics.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6ab03afb", Component.options)
  } else {
    hotAPI.reload("data-v-6ab03afb", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/components/project-tester-ratings.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3adc2ac0\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/project-tester-ratings.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/project-tester-ratings.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-3adc2ac0\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/project-tester-ratings.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-3adc2ac0"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\project-tester-ratings.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3adc2ac0", Component.options)
  } else {
    hotAPI.reload("data-v-3adc2ac0", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/components/publisher/file-viewer.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e55f98a2\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/file-viewer.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/publisher/file-viewer.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-e55f98a2\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/publisher/file-viewer.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-e55f98a2"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\publisher\\file-viewer.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e55f98a2", Component.options)
  } else {
    hotAPI.reload("data-v-e55f98a2", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/components/star-rating.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-323b1e04\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/star-rating.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/star-rating.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-323b1e04\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/star-rating.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-323b1e04"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\star-rating.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-323b1e04", Component.options)
  } else {
    hotAPI.reload("data-v-323b1e04", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/pages/projects/view.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0a203d4d\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/projects/view.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/projects/view.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-0a203d4d\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/projects/view.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-0a203d4d"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\projects\\view.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0a203d4d", Component.options)
  } else {
    hotAPI.reload("data-v-0a203d4d", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/services/project.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = isProjectApproved;
/* harmony export (immutable) */ __webpack_exports__["c"] = isProjectRunning;
/* harmony export (immutable) */ __webpack_exports__["b"] = isProjectCompleted;
var StatusMap = {
  "pending approval": 1,
  "pending payment": 2,
  running: 3,
  completed: 4
};
function isProjectApproved(status) {
  if (!status) {
    return false;
  }
  var statusWeight = StatusMap[status.toLowerCase()];
  return statusWeight && statusWeight > 1;
}

function isProjectRunning(status) {
  if (!status) {
    return false;
  }
  var statusWeight = StatusMap[status.toLowerCase()];
  return statusWeight && statusWeight > 2;
}

function isProjectCompleted(status) {
  if (!status) {
    return false;
  }
  var statusWeight = StatusMap[status.toLowerCase()];
  return statusWeight && statusWeight > 3;
}

/***/ })

});