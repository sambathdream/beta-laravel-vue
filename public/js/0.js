webpackJsonp([0],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/device-selector.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "DeviceSelector",
  props: {
    disabled: {
      type: Boolean,
      defualt: function defualt() {
        return false;
      }
    },
    value: {
      type: Array,
      required: true,
      default: function _default() {
        return [];
      }
    }
  },
  data: function data() {
    return {
      devices: []
    };
  },

  methods: {
    toggleItem: function toggleItem(device) {
      if (this.disabled) {
        return true;
      }
      var index = this.value.indexOf(device.id);
      var newDevices = [];
      if (index >= 0) {
        newDevices = this.value.filter(function (i) {
          return i !== device.id;
        });
      } else {
        newDevices = [].concat(_toConsumableArray(this.value), [device.id]);
      }
      this.$emit("input", newDevices);
      this.$emit("change", newDevices);
    }
  },
  mounted: function mounted() {
    var _this = this;

    if (!this.$store.state.devices.length) {
      axios.get("/api/device").then(function (_ref) {
        var data = _ref.data;

        _this.$store.commit("set_devices", data);
        _this.devices = data;
      });
    } else {
      this.devices = this.$store.state.devices;
    }
  },
  computed: {
    availableDevices: function availableDevices() {
      var _this2 = this;

      if (this.disabled) {
        return this.devices.filter(function (_ref2) {
          var id = _ref2.id;
          return _this2.value.indexOf(id) >= 0;
        });
      }
      return this.devices;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/publisher/app-uploader.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "AppUploader",
  props: {
    file: {
      type: Object,
      default: function _default() {
        return {
          file: {
            name: "",
            description: "",
            uploadedFile: null
          }
        };
      }
    },
    index: {
      type: Number,
      default: function _default() {
        return 0;
      }
    },
    canRemove: {
      default: function _default() {
        return true;
      }
    }
  },
  data: function data() {
    return {
      activeFile: this.file
    };
  },

  methods: {
    triggerFileClick: function triggerFileClick() {
      this.$refs.app_file.value = null;
      this.$refs.app_file.click();
    },
    deleteFile: function deleteFile() {
      this.$emit("delete");
    },
    updateModel: function updateModel() {
      this.$emit("change", this.activeFile, this.index);
    },
    checkFile: function checkFile(e) {
      var file = e.target.files[0];
      if (file) {
        var ext = file.name.split(".").pop();
        if (["ipa", "apk", "xap", "appx", "zip", "rar", "7zip", "xlsx", "doc", "docs", "pdf", "jpg", "png"].indexOf(ext) === -1) {
          return this.$swal("Error", "Only app files are allowed", "error");
        }
        this.activeFile = _extends({}, this.file, this.activeFile, {
          uploadedFile: file,
          name: file.name
        });
        this.$emit("change", this.activeFile, this.index);
      }
    }
  },
  watch: {
    file: function file(newValue) {
      this.activeFile = _extends({}, newValue);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/test-project/test_project_add.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__("./node_modules/vue/dist/vue.common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_form__ = __webpack_require__("./node_modules/vue-form/dist/vue-form.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_form___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_vue_form__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_src_validations_validations_js__ = __webpack_require__("./resources/assets/validations/validations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_components_components_device_selector__ = __webpack_require__("./resources/assets/components/components/device-selector.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_components_components_device_selector___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_components_components_device_selector__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_components_components_publisher_app_uploader__ = __webpack_require__("./resources/assets/components/components/publisher/app-uploader.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_components_components_publisher_app_uploader___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_components_components_publisher_app_uploader__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_src_services_utils__ = __webpack_require__("./resources/assets/services/utils.js");
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

__webpack_require__("./node_modules/vue2-animate/dist/vue2-animate.min.css");






__WEBPACK_IMPORTED_MODULE_0_vue___default.a.use(__WEBPACK_IMPORTED_MODULE_1_vue_form___default.a, __WEBPACK_IMPORTED_MODULE_2_src_validations_validations_js__["a" /* default */]);
// validation jump
var VueScrollTo = __webpack_require__("./node_modules/vue-scrollto/vue-scrollto.js");
__WEBPACK_IMPORTED_MODULE_0_vue___default.a.use(VueScrollTo, {
  container: "body",
  duration: 500,
  easing: "ease",
  offset: -100,
  cancelable: true,
  onDone: false,
  onCancel: false,
  x: false,
  y: true
});

var cleanData = {
  name: "",
  project_type_id: "",
  estimate_tester_time: 1.5,
  project_description: "",
  notes_instruction: "",
  devices: [],
  questions: [],
  initQuestion: "",
  questionEmpty: false,
  initValue: false
};
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "test_project_add",
  components: {
    DeviceSelector: __WEBPACK_IMPORTED_MODULE_3_components_components_device_selector___default.a,
    AppUploader: __WEBPACK_IMPORTED_MODULE_4_components_components_publisher_app_uploader___default.a
  },
  data: function data() {
    return {
      formstate: {},
      header: this.$route.params.id ? "Edit Project" : "Add Test Project",
      action_name: this.$route.params.id ? "Edit Project" : "Post Project",
      model: _extends({}, cleanData),
      errors: null,
      old_file: "",
      originalTestProjects: {},
      projectTypes: {},
      input: "",
      effect: "fadeUp",
      activeFile: {},
      fileEmpty: false,
      attachments: [],
      files: [],
      initTitle: "",
      initLink: "",
      titleEmpty: false
    };
  },

  methods: {
    onSubmit: function onSubmit() {
      var _this = this;

      this.model.initValue = true;
      this.addQuestion(false);
      if (this.formstate.$invalid || this.model.questionEmpty) {
        VueScrollTo.scrollTo("#scroll-top");
        return;
      } else {
        var p = void 0;
        this.errors = null;
        var requestData = _extends({}, this.model, {
          files: undefined,
          attachments: undefined
        });
        requestData.is_test_project = 1;
        if (this.model.id) {
          requestData._method = "PUT";
        }
        delete requestData.attachments;
        delete requestData.files;
        var formData = Object(__WEBPACK_IMPORTED_MODULE_5_src_services_utils__["a" /* objectToFormData */])(requestData);
        if (this.files) {
          this.files.filter(function (f) {
            return f.uploadedFile;
          }).forEach(function (f) {
            formData.append("app[]", f.uploadedFile);
            formData.append("app_description[]", f.description);
          });
          formData.append("existing_app", []);
          this.files.forEach(function (f) {
            formData.append("existing_app[]", f.name);
          });
          if (this.activeFile) {
            formData.append("app[]", this.activeFile.uploadedFile);
            formData.append("app_description[]", this.activeFile.description);
          }
        }
        (this.attachments || []).forEach(function (a, index) {
          formData.append("attachments[" + index + "][title]", a.title);
          formData.append("attachments[" + index + "][link]", a.link);
        });
        if (this.initTitle != "" && this.initLink != "") {
          formData.append("attachments[" + this.attachments.length + "][title]", this.initTitle);
          formData.append("attachments[" + this.attachments.length + "][link]", this.initLink);
        }
        this.$swal({
          title: "Are you sure?",
          text: "You want to submit this test project?",
          icon: "success",
          buttons: ["No", "Yes"],
          dangerMode: true
        }).then(function (isConfirmed) {
          if (isConfirmed) {
            if (_this.model.id) {
              p = axios.post("/api/projects/" + _this.model.id, formData);
            } else {
              p = axios.post("/api/projects", formData);
            }
            p.then(function (response) {
              _this.$router.push({ name: "admin.test_projects" });
              _this.$store.commit("set_devices", []); // reset device collection
            }).catch(function (error) {
              return _this.errors = error.response.data.message;
            });
          }
        });
      }
    },
    addAttachment: function addAttachment() {
      if (this.initTitle == "" || this.initLink == "") {
        this.titleEmpty = true;
        return false;
      } else {
        this.titleEmpty = false;
        this.attachments.push({});
        var key = this.attachments.length - 1;
        this.attachments[key] = {
          title: this.initTitle,
          link: this.initLink
        };
        this.initTitle = "";
        this.initLink = "";
      }
    },
    deleteAttachment: function deleteAttachment(index) {
      this.attachments.splice(index, 1);
    },
    updateActiveFile: function updateActiveFile(file) {
      this.activeFile = _extends({}, file);
    },

    addFile: function addFile(file) {
      if (!this.activeFile.name) {
        this.fileEmpty = true;
        return false;
      }
      this.fileEmpty = false;
      this.files.push(this.activeFile);
      this.activeFile = {};
    },
    updateFileInCollection: function updateFileInCollection(file, index) {
      this.files = [].concat(_toConsumableArray(this.files.slice(0, index)), [_extends({}, this.files[index], file)], _toConsumableArray(this.files.slice(index + 1)));
    },

    deleteFile: function deleteFile(index) {
      this.files = [].concat(_toConsumableArray(this.files.slice(0, index)), _toConsumableArray(this.files.slice(index + 1)));
    },
    addQuestion: function addQuestion(flag) {
      var key = this.model.questions.length - 1;
      if (this.model.questions[key] === "") {
        this.model.questionEmpty = true;
        return false;
      } else {
        this.model.questionEmpty = false;
        if (flag) {
          this.model.questions.push("");
        }
      }
    },
    deleteQuestion: function deleteQuestion(index) {
      this.model.questions.splice(index, 1);
    }
  },
  mounted: function mounted() {
    var _this2 = this;

    if (this.$route.params.id) {
      axios.get("/api/projects/" + this.$route.params.id).then(function (_ref) {
        var data = _ref.data.data;

        _this2.model = data;
        _this2.files = data.media.map(function (file) {
          return { 'name': file.file_name, 'uploadedFile': '' };
        });
        _this2.attachments = data.links.map(function (l) {
          return { 'link': l.link, 'title': l.title };
        });
        _this2.model.questions = data.questions.map(function (q) {
          return q.question;
        });
        _this2.initLink = "";
        _this2.initQuestion = "";
        _this2.initTitle = "";
        _this2.titleEmpty = false;
        _this2.model.devices = data.devices.map(function (_ref2) {
          var id = _ref2.id;
          return id;
        });
        _this2.originalTestProjects = data;
      }).catch(function () {
        return _this2.$router.push({ name: "admin.test_project" });
      });
    }
    axios.get("/api/project-types").then(function (response) {
      _this2.projectTypes = response.data;
    }).catch(function (error) {});
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/device-selector.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\nimg[data-v-0d653902] {\n  height: 50px;\n  width: 50px;\n  display: inline-block;\n  margin-right: 10px;\n  float: left;\n  opacity: 0.5;\n  cursor: pointer;\n}\nimg.disabled[data-v-0d653902] {\n  cursor: auto;\n}\nimg.selected[data-v-0d653902] {\n  opacity: 1;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-956bc8dc\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=1!./resources/assets/components/pages/test-project/test_project_add.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.custom-container[data-v-956bc8dc] {\n  max-width: 100%;\n  padding: 0;\n}\n.text-blue[data-v-956bc8dc] {\n  color: #0082cc;\n}\n.text-red[data-v-956bc8dc] {\n  color: #e63423;\n}\n.text-green[data-v-956bc8dc] {\n  color: #2cac3d;\n}\n.text-purple[data-v-956bc8dc] {\n  color: #3e3a94;\n}\n.text-bold[data-v-956bc8dc] {\n  font-family: \"BrandonTextBold\";\n}\n.text-medium[data-v-956bc8dc] {\n  font-family: \"BrandonTextMedium\";\n}\n.step-page-title[data-v-956bc8dc] {\n  font-family: \"UniNeueBold\";\n  font-size: 30px;\n  width: 100%;\n  text-align: center;\n  color: #363e48;\n  margin: 5px 0 25px;\n}\n.add-project-page[data-v-956bc8dc] {\n  width: 100%;\n}\n.add-project-page .white-box[data-v-956bc8dc] {\n    background-color: #fff;\n    -webkit-box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n            box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n    border-radius: 4px;\n    padding: 20px;\n    margin-bottom: 20px;\n    font-size: 17px;\n    color: #606368;\n}\n.add-project-page .white-box .block-title[data-v-956bc8dc] {\n      font-size: 20px;\n      font-family: \"UniNeueBold\";\n      border-bottom: 1px solid #dadada;\n      color: #3e3a94;\n      padding-bottom: 10px;\n      margin-bottom: 10px;\n      width: auto;\n      min-height: 35px;\n}\n.add-project-page .white-box .block-title a[data-v-956bc8dc] {\n        font-family: \"BrandonTextMedium\";\n        font-size: 17px;\n        text-decoration: underline !important;\n        float: right;\n}\n.add-project-page .white-box .block-subtitle[data-v-956bc8dc] {\n      font-family: \"BrandonTextBold\";\n      font-size: 20px;\n      color: #3e3a94;\n      border-bottom: 1px solid #dadada;\n      padding-bottom: 10px;\n      margin-bottom: 15px;\n}\n.add-project-page .white-box .custom-form-control[data-v-956bc8dc] {\n      font-size: 17px;\n      border-radius: 4px;\n      padding: 2px 8px;\n}\n.add-project-page .white-box .custom-form-control[data-v-956bc8dc]:hover {\n        outline: none !important;\n}\n.add-project-page .white-box .custom-form-control[data-v-956bc8dc]:focus {\n        outline: none !important;\n}\n.add-project-page .blue-step-btn[data-v-956bc8dc] {\n    width: auto;\n    padding: 2px 4px;\n    border: 2px solid #0082cc;\n    background-color: #00aff5;\n    font-size: 14px;\n    font-family: \"BrandonTextMedium\";\n    letter-spacing: 0.05rem;\n    color: #fff;\n    border-radius: 20px;\n    display: block;\n    text-align: center;\n}\n.add-project-page .blue-step-btn[data-v-956bc8dc]:hover {\n      background-color: #13b9fb;\n}\n.add-project-page .grey-btn[data-v-956bc8dc] {\n    width: auto;\n    padding: 2px 4px;\n    border: 2px solid #606368;\n    background-color: #898a8c;\n    font-size: 14px;\n    font-family: \"BrandonTextMedium\";\n    letter-spacing: 0.05rem;\n    color: #fff;\n    border-radius: 20px;\n    display: block;\n    text-align: center;\n}\n.add-project-page .grey-btn[data-v-956bc8dc]:hover {\n      background-color: #95979a;\n}\n.add-project-page .purple-btn[data-v-956bc8dc] {\n    cursor: pointer;\n    width: auto;\n    background-color: #5651b9;\n    border: 2px solid #3e3a94;\n    color: #fff;\n    font-size: 15px;\n    line-height: 16px;\n    font-family: \"BrandonTextMedium\";\n    border-radius: 20px !important;\n    padding: 6px 20px;\n    -webkit-transition: all 0.4s ease;\n    transition: all 0.4s ease;\n    display: block;\n    text-align: center;\n}\n.add-project-page .purple-btn[data-v-956bc8dc]:hover {\n      outline: none !important;\n      text-decoration: underline !important;\n      color: #fff;\n}\n.add-project-page .purple-btn[data-v-956bc8dc]:focus {\n      outline: none !important;\n      color: #fff;\n}\n.add-project-page .post-poj-btn[data-v-956bc8dc] {\n    padding: 10px 15px;\n    max-width: 200px;\n    font-size: 16px;\n    vertical-align: top;\n}\n.add-project-page .back-proj-btn[data-v-956bc8dc] {\n    padding: 6px 15px;\n    max-width: 200px;\n    font-size: 16px;\n    vertical-align: top;\n}\n.add-project-page .question-label[data-v-956bc8dc] {\n    font-size: 17px;\n}\n@media screen and (max-width: 1281px) {\n.add-project-page .white-box[data-v-956bc8dc] {\n    font-size: 16px;\n}\n.add-project-page .white-box .icon-34[data-v-956bc8dc] {\n      font-size: 24px;\n      margin-top: 4px;\n}\n.add-project-page .purple-btn[data-v-956bc8dc] {\n    padding: 6px;\n}\n.add-project-page .post-poj-btn[data-v-956bc8dc] {\n    padding: 9px 15px;\n    font-size: 14px;\n}\n.add-project-page .back-proj-btn[data-v-956bc8dc] {\n    font-size: 14px;\n}\n}\n@media screen and (max-width: 1200px) {\n.page-title[data-v-956bc8dc] {\n    font-size: 28px;\n    margin: 0 0 20px;\n}\n.add-project-page .blue-step-btn[data-v-956bc8dc] {\n    letter-spacing: 0;\n}\n.add-project-page .white-box[data-v-956bc8dc] {\n    font-size: 14px;\n}\n.add-project-page .white-box .block-title[data-v-956bc8dc] {\n      font-size: 18px;\n}\n}\n@media screen and (max-width: 991px) {\n.custom-container[data-v-956bc8dc] {\n    max-width: 100%;\n}\n.custom-container .add-project-page .white-box[data-v-956bc8dc] {\n      padding: 15px;\n}\n}\n@media screen and (max-width: 767px) {\n.add-project-page .status-bar[data-v-956bc8dc] {\n    font-size: 14px;\n}\n}\n@media screen and (max-width: 575px) {\n.custom-container[data-v-956bc8dc] {\n    padding: 0;\n}\n.custom-container .step-page-title[data-v-956bc8dc] {\n      font-size: 26px;\n      margin: 0 0 20px;\n}\n.custom-container .add-project-page .blue-step-btn[data-v-956bc8dc] {\n      max-width: 180px;\n      font-size: 14px;\n      margin: 10px 0 0;\n}\n.custom-container .add-project-page .grey-btn[data-v-956bc8dc] {\n      font-size: 14px;\n}\n.custom-container .add-project-page .purple-btn[data-v-956bc8dc] {\n      margin-top: 10px;\n}\n.custom-container .add-project-page .white-box[data-v-956bc8dc] {\n      font-size: 14px;\n      line-height: 18px;\n}\n.custom-container .add-project-page .back-proj-btn[data-v-956bc8dc] {\n      font-size: 14px;\n      margin: 0 auto !important;\n}\n.custom-container .add-project-page .post-poj-btn[data-v-956bc8dc] {\n      margin: 10px auto 0 !important;\n}\n.custom-container .add-project-page .question-label[data-v-956bc8dc] {\n      margin-bottom: 10px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-956bc8dc\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/test-project/test_project_add.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.dropzone_wrapper[data-v-956bc8dc] {\r\n  width: 100%;\r\n  height: 300px;\n}\n.red-asterisk[data-v-956bc8dc] {\r\n  color: red;\n}\n@media only screen and (min-width: 768px) {\n.border-right[data-v-956bc8dc] {\r\n    border-right: 1px solid #ccc;\n}\n}\n.border-bottom[data-v-956bc8dc] {\r\n  border-bottom: 1px solid #ccc;\n}\n.devices-icon[data-v-956bc8dc] {\r\n  font-size: 50px;\r\n  padding-right: 15px;\n}\nselect[data-v-956bc8dc] {\r\n  height: 34px;\n}\n.label-title[data-v-956bc8dc] {\r\n  font-size: 14px;\r\n  color: #656599;\r\n  font-weight: 700;\n}\n.icon-34[data-v-956bc8dc] {\r\n  font-size: 34px;\n}\n.btn[data-v-956bc8dc] {\r\n  border-radius: 0 !important;\n}\n.active[data-v-956bc8dc] {\r\n  opacity: 0.5;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b80804c4\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/app-uploader.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.icon-34[data-v-b80804c4] {\n  font-size: 34px;\n}\n.custom-form-control[data-v-b80804c4] {\n  font-size: 14px;\n  border-radius: 4px;\n  padding: 6px 8px;\n}\n.custom-form-control[data-v-b80804c4]:hover {\n    outline: none !important;\n}\n.custom-form-control[data-v-b80804c4]:focus {\n    outline: none !important;\n}\n.blue-step-btn[data-v-b80804c4] {\n  cursor: pointer;\n  width: auto;\n  padding: 2px 4px;\n  border: 2px solid #0082cc;\n  background-color: #00aff5;\n  font-size: 14px;\n  font-family: \"BrandonTextMedium\";\n  letter-spacing: 0.05rem;\n  color: #fff;\n  border-radius: 20px;\n  display: block;\n  text-align: center;\n}\n.blue-step-btn[data-v-b80804c4]:hover {\n    background-color: #13b9fb;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue2-animate/dist/vue2-animate.min.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@charset \"UTF-8\";/*!\n * vue2-animate v1.0.4\n * (c) 2016 Simon Asika\n * Released under the MIT License.\n * Documentation: https://github.com/asika32764/vue2-animate\n */.bounce-enter-active,.bounce-leave-active,.bounceDown-enter-active,.bounceDown-leave-active,.bounceIn,.bounceInDown,.bounceInLeft,.bounceInRight,.bounceInUp,.bounceLeft-enter-active,.bounceLeft-leave-active,.bounceOut,.bounceOutDown,.bounceOutLeft,.bounceOutRight,.bounceOutUp,.bounceRight-enter-active,.bounceRight-leave-active,.bounceUp-enter-active,.bounceUp-leave-active,.fade-enter-active,.fade-leave-active,.fadeDown-enter-active,.fadeDown-leave-active,.fadeDownBig-enter-active,.fadeDownBig-leave-active,.fadeIn,.fadeInDown,.fadeInDownBig,.fadeInLeft,.fadeInLeftBig,.fadeInRight,.fadeInRightBig,.fadeInUp,.fadeInUpBig,.fadeLeft-enter-active,.fadeLeft-leave-active,.fadeLeftBig-enter-active,.fadeLeftBig-leave-active,.fadeOut,.fadeOutDown,.fadeOutDownBig,.fadeOutLeft,.fadeOutLeftBig,.fadeOutRight,.fadeOutRightBig,.fadeOutUp,.fadeOutUpBig,.fadeRight-enter-active,.fadeRight-leave-active,.fadeRightBig-enter-active,.fadeRightBig-leave-active,.fadeUp-enter-active,.fadeUp-leave-active,.fadeUpBig-enter-active,.fadeUpBig-leave-active,.rotateDownLeft-enter-active,.rotateDownLeft-leave-active,.rotateDownRight-enter-active,.rotateDownRight-leave-active,.rotateInDownLeft,.rotateInDownRight,.rotateInUpLeft,.rotateInUpRight,.rotateOutDownLeft,.rotateOutDownRight,.rotateOutUpLeft,.rotateOutUpRight,.rotateUpLeft-enter-active,.rotateUpLeft-leave-active,.rotateUpRight-enter-active,.rotateUpRight-leave-active,.slide-enter-active,.slide-leave-active,.slideDown-enter-active,.slideDown-leave-active,.slideIn,.slideInDown,.slideInLeft,.slideInRight,.slideInUp,.slideLeft-enter-active,.slideLeft-leave-active,.slideOut,.slideOutDown,.slideOutLeft,.slideOutRight,.slideOutUp,.slideRight-enter-active,.slideRight-leave-active,.slideUp-enter-active,.slideUp-leave-active,.zoom-enter-active,.zoom-leave-active,.zoomDown-enter-active,.zoomDown-leave-active,.zoomIn,.zoomInDown,.zoomInLeft,.zoomInRight,.zoomInUp,.zoomLeft-enter-active,.zoomLeft-leave-active,.zoomOut,.zoomOutDown,.zoomOutLeft,.zoomOutRight,.zoomOutUp,.zoomRight-enter-active,.zoomRight-leave-active,.zoomUp-enter-active,.zoomUp-leave-active{animation-duration:1s;animation-fill-mode:both}@keyframes bounceIn{20%,40%,60%,80%,from,to{animation-timing-function:cubic-bezier(.215,.61,.355,1)}0%{opacity:0;transform:scale3d(.3,.3,.3)}20%{transform:scale3d(1.1,1.1,1.1)}40%{transform:scale3d(.9,.9,.9)}60%{opacity:1;transform:scale3d(1.03,1.03,1.03)}80%{transform:scale3d(.97,.97,.97)}to{opacity:1;transform:scale3d(1,1,1)}}@keyframes bounceOut{20%{transform:scale3d(.9,.9,.9)}50%,55%{opacity:1;transform:scale3d(1.1,1.1,1.1)}to{opacity:0;transform:scale3d(.3,.3,.3)}}@keyframes bounceInDown{60%,75%,90%,from,to{animation-timing-function:cubic-bezier(.215,.61,.355,1)}0%{opacity:0;transform:translate3d(0,-3000px,0)}60%{opacity:1;transform:translate3d(0,25px,0)}75%{transform:translate3d(0,-10px,0)}90%{transform:translate3d(0,5px,0)}to{transform:none}}@keyframes bounceOutDown{20%{transform:translate3d(0,10px,0)}40%,45%{opacity:1;transform:translate3d(0,-20px,0)}to{opacity:0;transform:translate3d(0,2000px,0)}}@keyframes bounceInLeft{60%,75%,90%,from,to{animation-timing-function:cubic-bezier(.215,.61,.355,1)}0%{opacity:0;transform:translate3d(-3000px,0,0)}60%{opacity:1;transform:translate3d(25px,0,0)}75%{transform:translate3d(-10px,0,0)}90%{transform:translate3d(5px,0,0)}to{transform:none}}@keyframes bounceOutLeft{20%{opacity:1;transform:translate3d(20px,0,0)}to{opacity:0;transform:translate3d(-2000px,0,0)}}@keyframes bounceInRight{60%,75%,90%,from,to{animation-timing-function:cubic-bezier(.215,.61,.355,1)}from{opacity:0;transform:translate3d(3000px,0,0)}60%{opacity:1;transform:translate3d(-25px,0,0)}75%{transform:translate3d(10px,0,0)}90%{transform:translate3d(-5px,0,0)}to{transform:none}}@keyframes bounceOutRight{20%{opacity:1;transform:translate3d(-20px,0,0)}to{opacity:0;transform:translate3d(2000px,0,0)}}@keyframes bounceInUp{60%,75%,90%,from,to{animation-timing-function:cubic-bezier(.215,.61,.355,1)}from{opacity:0;transform:translate3d(0,3000px,0)}60%{opacity:1;transform:translate3d(0,-20px,0)}75%{transform:translate3d(0,10px,0)}90%{transform:translate3d(0,-5px,0)}to{transform:translate3d(0,0,0)}}@keyframes bounceOutUp{20%{transform:translate3d(0,-10px,0)}40%,45%{opacity:1;transform:translate3d(0,20px,0)}to{opacity:0;transform:translate3d(0,-2000px,0)}}.bounce-enter-active,.bounceIn{animation-name:bounceIn}.bounce-leave-active,.bounceOut{animation-name:bounceOut}.bounceInUp,.bounceUp-enter-active{animation-name:bounceInUp}.bounceOutUp,.bounceUp-leave-active{animation-name:bounceOutUp}.bounceInRight,.bounceRight-enter-active{animation-name:bounceInRight}.bounceOutRight,.bounceRight-leave-active{animation-name:bounceOutRight}.bounceInLeft,.bounceLeft-enter-active{animation-name:bounceInLeft}.bounceLeft-leave-active,.bounceOutLeft{animation-name:bounceOutLeft}.bounceDown-enter-active,.bounceInDown{animation-name:bounceInDown}.bounceDown-leave-active,.bounceOutDown{animation-name:bounceOutDown}@keyframes fadeIn{from{opacity:0}to{opacity:1}}@keyframes fadeOut{from{opacity:1}to{opacity:0}}@keyframes fadeInDown{from{opacity:0;transform:translate3d(0,-100%,0)}to{opacity:1;transform:none}}@keyframes fadeOutDown{from{opacity:1}to{opacity:0;transform:translate3d(0,100%,0)}}@keyframes fadeInDownBig{from{opacity:0;transform:translate3d(0,-2000px,0)}to{opacity:1;transform:none}}@keyframes fadeOutDownBig{from{opacity:1}to{opacity:0;transform:translate3d(0,2000px,0)}}@keyframes fadeInLeft{from{opacity:0;transform:translate3d(-100%,0,0)}to{opacity:1;transform:none}}@keyframes fadeOutLeft{from{opacity:1}to{opacity:0;transform:translate3d(-100%,0,0)}}@keyframes fadeInLeftBig{from{opacity:0;transform:translate3d(-2000px,0,0)}to{opacity:1;transform:none}}@keyframes fadeOutLeftBig{from{opacity:1}to{opacity:0;transform:translate3d(-2000px,0,0)}}@keyframes fadeInRight{from{opacity:0;transform:translate3d(100%,0,0)}to{opacity:1;transform:none}}@keyframes fadeOutRight{from{opacity:1}to{opacity:0;transform:translate3d(100%,0,0)}}@keyframes fadeInRightBig{from{opacity:0;transform:translate3d(2000px,0,0)}to{opacity:1;transform:none}}@keyframes fadeOutRightBig{from{opacity:1}to{opacity:0;transform:translate3d(2000px,0,0)}}@keyframes fadeInUp{from{opacity:0;transform:translate3d(0,100%,0)}to{opacity:1;transform:none}}@keyframes fadeInUpBig{from{opacity:0;transform:translate3d(0,2000px,0)}to{opacity:1;transform:none}}@keyframes fadeOutUp{from{opacity:1}to{opacity:0;transform:translate3d(0,-100%,0)}}.fade-enter-active,.fadeIn{animation-name:fadeIn}.fade-leave-active,.fadeOut{animation-name:fadeOut}.fadeInUpBig,.fadeUpBig-enter-active{animation-name:fadeInUpBig}.fadeOutUpBig,.fadeUpBig-leave-active{animation-name:fadeOutUpBig}.fadeInUp,.fadeUp-enter-active{animation-name:fadeInUp}.fadeOutUp,.fadeUp-leave-active{animation-name:fadeOutUp}.fadeInRightBig,.fadeRightBig-enter-active{animation-name:fadeInRightBig}.fadeOutRightBig,.fadeRightBig-leave-active{animation-name:fadeOutRightBig}.fadeInRight,.fadeRight-enter-active{animation-name:fadeInRight}.fadeOutRight,.fadeRight-leave-active{animation-name:fadeOutRight}.fadeInLeftBig,.fadeLeftBig-enter-active{animation-name:fadeInLeftBig}.fadeLeftBig-leave-active,.fadeOutLeftBig{animation-name:fadeOutLeftBig}.fadeInLeft,.fadeLeft-enter-active{animation-name:fadeInLeft}.fadeLeft-leave-active,.fadeOutLeft{animation-name:fadeOutLeft}.fadeDownBig-enter-active,.fadeInDownBig{animation-name:fadeInDownBig}.fadeDownBig-leave-active,.fadeOutDownBig{animation-name:fadeOutDownBig}.fadeDown-enter-active,.fadeInDown{animation-name:fadeInDown}.fadeDown-leave-active,.fadeOutDown{animation-name:fadeOutDown}@keyframes rotateIn{from{transform-origin:center;transform:rotate3d(0,0,1,-200deg);opacity:0}to{transform-origin:center;transform:none;opacity:1}}@keyframes rotateOut{from{transform-origin:center;opacity:1}to{transform-origin:center;transform:rotate3d(0,0,1,200deg);opacity:0}}@keyframes rotateInDownLeft{from{transform-origin:left bottom;transform:rotate3d(0,0,1,-45deg);opacity:0}to{transform-origin:left bottom;transform:none;opacity:1}}@keyframes rotateOutDownLeft{from{transform-origin:left bottom;opacity:1}to{transform-origin:left bottom;transform:rotate3d(0,0,1,45deg);opacity:0}}@keyframes rotateInDownRight{from{transform-origin:right bottom;transform:rotate3d(0,0,1,45deg);opacity:0}to{transform-origin:right bottom;transform:none;opacity:1}}@keyframes rotateOutDownRight{from{transform-origin:right bottom;opacity:1}to{transform-origin:right bottom;transform:rotate3d(0,0,1,-45deg);opacity:0}}@keyframes rotateInUpLeft{from{transform-origin:left bottom;transform:rotate3d(0,0,1,45deg);opacity:0}to{transform-origin:left bottom;transform:none;opacity:1}}@keyframes rotateOutUpLeft{from{transform-origin:left bottom;opacity:1}to{transform-origin:left bottom;transform:rotate3d(0,0,1,-45deg);opacity:0}}@keyframes rotateInUpRight{from{transform-origin:right bottom;transform:rotate3d(0,0,1,-90deg);opacity:0}to{transform-origin:right bottom;transform:none;opacity:1}}@keyframes rotateOutUpRight{from{transform-origin:right bottom;opacity:1}to{transform-origin:right bottom;transform:rotate3d(0,0,1,90deg);opacity:0}}.rotate-enter-active,.rotate-leave-active,.rotateIn,.rotateOut{animation-duration:1s;animation-fill-mode:both}.rotate-enter-active,.rotateIn{animation-name:rotateIn}.rotate-leave-active,.rotateOut{animation-name:rotateOut}.rotateInUpRight,.rotateUpRight-enter-active{animation-name:rotateInUpRight}.rotateOutUpRight,.rotateUpRight-leave-active{animation-name:rotateOutUpRight}.rotateInUpLeft,.rotateUpLeft-enter-active{animation-name:rotateInUpLeft}.rotateOutUpLeft,.rotateUpLeft-leave-active{animation-name:rotateOutUpLeft}.rotateDownRight-enter-active,.rotateInDownRight{animation-name:rotateInDownRight}.rotateDownRight-leave-active,.rotateOutDownRight{animation-name:rotateOutDownRight}.rotateDownLeft-enter-active,.rotateInDownLeft{animation-name:rotateInDownLeft}.rotateDownLeft-leave-active,.rotateOutDownLeft{animation-name:rotateOutDownLeft}@keyframes slideInDown{from{transform:translate3d(0,-100%,0);visibility:visible}to{transform:translate3d(0,0,0)}}@keyframes slideOutDown{from{transform:translate3d(0,0,0)}to{visibility:hidden;transform:translate3d(0,100%,0)}}@keyframes slideInLeft{from{transform:translate3d(-100%,0,0);visibility:visible}to{transform:translate3d(0,0,0)}}@keyframes slideOutLeft{from{transform:translate3d(0,0,0)}to{visibility:hidden;transform:translate3d(-100%,0,0)}}@keyframes slideInRight{from{transform:translate3d(100%,0,0);visibility:visible}to{transform:translate3d(0,0,0)}}@keyframes slideOutRight{from{transform:translate3d(0,0,0)}to{visibility:hidden;transform:translate3d(100%,0,0)}}@keyframes slideInUp{from{transform:translate3d(0,100%,0);visibility:visible}to{transform:translate3d(0,0,0)}}@keyframes slideOutUp{from{transform:translate3d(0,0,0)}to{visibility:hidden;transform:translate3d(0,-100%,0)}}.slide-enter-active,.slideIn{animation-name:slideIn}.slide-leave-active,.slideOut{animation-name:slideOut}.slideInUp,.slideUp-enter-active{animation-name:slideInUp}.slideOutUp,.slideUp-leave-active{animation-name:slideOutUp}.slideInRight,.slideRight-enter-active{animation-name:slideInRight}.slideOutRight,.slideRight-leave-active{animation-name:slideOutRight}.slideInLeft,.slideLeft-enter-active{animation-name:slideInLeft}.slideLeft-leave-active,.slideOutLeft{animation-name:slideOutLeft}.slideDown-enter-active,.slideInDown{animation-name:slideInDown}.slideDown-leave-active,.slideOutDown{animation-name:slideOutDown}@keyframes zoomIn{from{opacity:0;transform:scale3d(.3,.3,.3)}50%{opacity:1}}@keyframes zoomOut{from{opacity:1}50%{opacity:0;transform:scale3d(.3,.3,.3)}to{opacity:0}}@keyframes zoomInDown{from{opacity:0;transform:scale3d(.1,.1,.1) translate3d(0,-1000px,0);animation-timing-function:cubic-bezier(.55,.055,.675,.19)}60%{opacity:1;transform:scale3d(.475,.475,.475) translate3d(0,60px,0);animation-timing-function:cubic-bezier(.175,.885,.32,1)}}@keyframes zoomOutDown{40%{opacity:1;transform:scale3d(.475,.475,.475) translate3d(0,-60px,0);animation-timing-function:cubic-bezier(.55,.055,.675,.19)}to{opacity:0;transform:scale3d(.1,.1,.1) translate3d(0,2000px,0);transform-origin:center bottom;animation-timing-function:cubic-bezier(.175,.885,.32,1)}}@keyframes zoomInLeft{from{opacity:0;transform:scale3d(.1,.1,.1) translate3d(-1000px,0,0);animation-timing-function:cubic-bezier(.55,.055,.675,.19)}60%{opacity:1;transform:scale3d(.475,.475,.475) translate3d(10px,0,0);animation-timing-function:cubic-bezier(.175,.885,.32,1)}}@keyframes zoomOutLeft{40%{opacity:1;transform:scale3d(.475,.475,.475) translate3d(42px,0,0)}to{opacity:0;transform:scale(.1) translate3d(-2000px,0,0);transform-origin:left center}}@keyframes zoomInRight{from{opacity:0;transform:scale3d(.1,.1,.1) translate3d(1000px,0,0);animation-timing-function:cubic-bezier(.55,.055,.675,.19)}60%{opacity:1;transform:scale3d(.475,.475,.475) translate3d(-10px,0,0);animation-timing-function:cubic-bezier(.175,.885,.32,1)}}@keyframes zoomOutRight{40%{opacity:1;transform:scale3d(.475,.475,.475) translate3d(-42px,0,0)}to{opacity:0;transform:scale(.1) translate3d(2000px,0,0);transform-origin:right center}}@keyframes zoomInUp{from{opacity:0;transform:scale3d(.1,.1,.1) translate3d(0,1000px,0);animation-timing-function:cubic-bezier(.55,.055,.675,.19)}60%{opacity:1;transform:scale3d(.475,.475,.475) translate3d(0,-60px,0);animation-timing-function:cubic-bezier(.175,.885,.32,1)}}@keyframes zoomOutUp{40%{opacity:1;transform:scale3d(.475,.475,.475) translate3d(0,60px,0);animation-timing-function:cubic-bezier(.55,.055,.675,.19)}to{opacity:0;transform:scale3d(.1,.1,.1) translate3d(0,-2000px,0);transform-origin:center bottom;animation-timing-function:cubic-bezier(.175,.885,.32,1)}}.zoom-enter-active,.zoomIn{animation-name:zoomIn}.zoom-leave-active,.zoomOut{animation-name:zoomOut}.zoomInUp,.zoomUp-enter-active{animation-name:zoomInUp}.zoomOutUp,.zoomUp-leave-active{animation-name:zoomOutUp}.zoomInRight,.zoomRight-enter-active{animation-name:zoomInRight}.zoomOutRight,.zoomRight-leave-active{animation-name:zoomOutRight}.zoomInLeft,.zoomLeft-enter-active{animation-name:zoomInLeft}.zoomLeft-leave-active,.zoomOutLeft{animation-name:zoomOutLeft}.zoomDown-enter-active,.zoomInDown{animation-name:zoomInDown}.zoomDown-leave-active,.zoomOutDown{animation-name:zoomOutDown}", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-0d653902\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/device-selector.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    _vm._l(_vm.availableDevices, function(device) {
      return _c("span", { staticClass: "d-inline-block pr-3" }, [
        _c("img", {
          staticClass: "img-fluid",
          class: {
            selected: _vm.value.indexOf(device.id) !== -1,
            disabled: _vm.disabled
          },
          attrs: { title: device.name, src: device.icon },
          on: {
            click: function($event) {
              _vm.toggleItem(device)
            }
          }
        })
      ])
    })
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0d653902", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-956bc8dc\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/test-project/test_project_add.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { attrs: { id: "scroll-top" } }, [
    _c("div", { staticClass: "container custom-container" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-12" }, [
          _c("h1", { staticClass: "step-page-title" }, [
            _vm._v("\n               " + _vm._s(_vm.header) + "\n            ")
          ])
        ])
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "add-project-page" },
        [
          _c(
            "vue-form",
            {
              staticClass: "form-horizontal form-validation",
              attrs: { state: _vm.formstate, enctype: "multipart/form-data" },
              on: {
                submit: function($event) {
                  $event.preventDefault()
                  _vm.onSubmit($event)
                }
              }
            },
            [
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-md-12" }, [
                  _c("div", { staticClass: "white-box" }, [
                    _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-12" }, [
                        _c("h4", { staticClass: "block-subtitle" }, [
                          _vm._v(
                            "\n                                    Project Details\n                                    "
                          )
                        ])
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-lg-12" }, [
                      _vm.errors
                        ? _c("div", {
                            staticStyle: { color: "red" },
                            domProps: { textContent: _vm._s(_vm.errors) }
                          })
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "row" }, [
                      _c(
                        "div",
                        { staticClass: "col-md-6 border-right" },
                        [
                          _c("validate", { attrs: { tag: "div" } }, [
                            _c("div", { staticClass: "row pb-3" }, [
                              _c(
                                "div",
                                {
                                  staticClass: "col-md-4 pr-md-0 mb-2 mb-sm-0"
                                },
                                [_c("span", [_vm._v("Project Title :")])]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-md-8" },
                                [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.model.name,
                                        expression: "model.name"
                                      }
                                    ],
                                    staticClass:
                                      "form-control custom-form-control ",
                                    attrs: {
                                      name: "project_title",
                                      type: "text",
                                      required: "",
                                      autofocus: "",
                                      placeholder: "Enter Project Title"
                                    },
                                    domProps: { value: _vm.model.name },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          _vm.model,
                                          "name",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "field-messages",
                                    {
                                      staticClass: "text-danger",
                                      attrs: {
                                        name: "project_title",
                                        show: "$invalid && $submitted"
                                      }
                                    },
                                    [
                                      _c(
                                        "div",
                                        {
                                          attrs: { slot: "required" },
                                          slot: "required"
                                        },
                                        [
                                          _vm._v(
                                            "Project Title is a required field"
                                          )
                                        ]
                                      )
                                    ]
                                  )
                                ],
                                1
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c("validate", { attrs: { tag: "div" } }, [
                            _c("div", { staticClass: "row pb-3" }, [
                              _c(
                                "div",
                                {
                                  staticClass: "col-md-4 pr-md-0 mb-2 mb-sm-0"
                                },
                                [_c("span", [_vm._v("Project Type :")])]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-md-8" },
                                [
                                  _c(
                                    "select",
                                    {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.model.project_type_id,
                                          expression: "model.project_type_id"
                                        }
                                      ],
                                      staticClass:
                                        "form-control custom-form-control",
                                      attrs: {
                                        id: "project_type",
                                        name: "project_type",
                                        size: "1",
                                        required: "",
                                        checkbox: ""
                                      },
                                      on: {
                                        change: function($event) {
                                          var $$selectedVal = Array.prototype.filter
                                            .call(
                                              $event.target.options,
                                              function(o) {
                                                return o.selected
                                              }
                                            )
                                            .map(function(o) {
                                              var val =
                                                "_value" in o
                                                  ? o._value
                                                  : o.value
                                              return val
                                            })
                                          _vm.$set(
                                            _vm.model,
                                            "project_type_id",
                                            $event.target.multiple
                                              ? $$selectedVal
                                              : $$selectedVal[0]
                                          )
                                        }
                                      }
                                    },
                                    [
                                      _c(
                                        "option",
                                        {
                                          attrs: {
                                            value: "-1",
                                            selected: "",
                                            disabled: ""
                                          }
                                        },
                                        [
                                          _vm._v(
                                            "\n                                                        Select Type of Project\n                                                    "
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _vm._l(_vm.projectTypes, function(
                                        projectType
                                      ) {
                                        return _c(
                                          "option",
                                          {
                                            domProps: { value: projectType.id }
                                          },
                                          [
                                            _vm._v(
                                              " " +
                                                _vm._s(projectType.name) +
                                                " "
                                            )
                                          ]
                                        )
                                      })
                                    ],
                                    2
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "field-messages",
                                    {
                                      staticClass: "text-danger",
                                      attrs: {
                                        name: "project_type",
                                        show: "$invalid && $submitted"
                                      }
                                    },
                                    [
                                      _c(
                                        "div",
                                        {
                                          attrs: { slot: "required" },
                                          slot: "required"
                                        },
                                        [
                                          _vm._v(
                                            "Project Type is a required field"
                                          )
                                        ]
                                      )
                                    ]
                                  )
                                ],
                                1
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c("validate", { attrs: { tag: "div" } }, [
                            _c("div", { staticClass: "row pb-3" }, [
                              _c(
                                "div",
                                {
                                  staticClass: "col-md-4 pr-md-0 mb-2 mb-sm-0"
                                },
                                [
                                  _c("span", [
                                    _vm._v("Time to Test (Approx Hrs.) :")
                                  ])
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-md-8" },
                                [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.model.estimate_tester_time,
                                        expression: "model.estimate_tester_time"
                                      }
                                    ],
                                    staticClass:
                                      "form-control custom-form-control",
                                    attrs: {
                                      type: "number",
                                      step: 0.5,
                                      min: 0,
                                      name: "estimate_tester_time",
                                      required: ""
                                    },
                                    domProps: {
                                      value: _vm.model.estimate_tester_time
                                    },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          _vm.model,
                                          "estimate_tester_time",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "field-messages",
                                    {
                                      staticClass: "text-danger",
                                      attrs: {
                                        name: "estimate_tester_time",
                                        show: "$invalid && $submitted"
                                      }
                                    },
                                    [
                                      _c(
                                        "div",
                                        {
                                          attrs: { slot: "required" },
                                          slot: "required"
                                        },
                                        [_vm._v("This is a required field")]
                                      )
                                    ]
                                  )
                                ],
                                1
                              )
                            ])
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-md-6 pr-0" },
                        [
                          _c("validate", [
                            _c("div", { staticClass: "row pb-3" }, [
                              _c("div", { staticClass: "col-md-12" }, [
                                _vm._v("Devices to test on :")
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-md-8 pt-4" },
                                [
                                  _c("device-selector", {
                                    attrs: {
                                      name: "device-selector",
                                      required: ""
                                    },
                                    model: {
                                      value: _vm.model.devices,
                                      callback: function($$v) {
                                        _vm.$set(_vm.model, "devices", $$v)
                                      },
                                      expression: "model.devices"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "field-messages",
                                    {
                                      staticClass: "text-danger",
                                      attrs: {
                                        name: "device-selector",
                                        show: "$invalid && $submitted"
                                      }
                                    },
                                    [
                                      _c(
                                        "div",
                                        {
                                          attrs: { slot: "required" },
                                          slot: "required"
                                        },
                                        [_vm._v("Please select device")]
                                      )
                                    ]
                                  )
                                ],
                                1
                              )
                            ])
                          ])
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "row pt-3" }, [
                      _c("div", { staticClass: "col-12" }, [
                        _c("h4", { staticClass: "block-subtitle" }, [
                          _vm._v(
                            "\n                                    Project Description\n                                    "
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-md-12" },
                        [
                          _c(
                            "validate",
                            { attrs: { tag: "div" } },
                            [
                              _c("textarea", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.model.project_description,
                                    expression: "model.project_description"
                                  }
                                ],
                                staticClass:
                                  "form-control custom-form-control resize_vertical",
                                attrs: {
                                  rows: "5",
                                  name: "project_description",
                                  placeholder: "Enter Project Description Here",
                                  required: ""
                                },
                                domProps: {
                                  value: _vm.model.project_description
                                },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.model,
                                      "project_description",
                                      $event.target.value
                                    )
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "field-messages",
                                {
                                  staticClass: "text-danger",
                                  attrs: {
                                    name: "project_description",
                                    show: "$invalid && $submitted"
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      attrs: { slot: "required" },
                                      slot: "required"
                                    },
                                    [_vm._v("This is a required field")]
                                  )
                                ]
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-md-12" }, [
                  _c("div", { staticClass: "white-box" }, [
                    _c("div", { staticClass: "row pb-3" }, [
                      _c(
                        "div",
                        { staticClass: "col-md-6" },
                        [
                          _c("h4", { staticClass: "block-subtitle mb-4" }, [
                            _vm._v(
                              "\n                                  Links of App :\n                                "
                            )
                          ]),
                          _vm._v(" "),
                          _c(
                            "transition-group",
                            {
                              staticClass: "list-group",
                              attrs: { name: _vm.effect, tag: "ul" }
                            },
                            _vm._l(_vm.attachments, function(attachment, i) {
                              return _c("li", { key: i }, [
                                _c("div", { staticClass: "row pb-3" }, [
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "col-md-4 col-sm-3 col-12 pr-md-0 pb-3 pb-sm-0"
                                    },
                                    [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: attachment.title,
                                            expression: "attachment.title"
                                          }
                                        ],
                                        staticClass:
                                          "form-control custom-form-control",
                                        attrs: {
                                          type: "text",
                                          placeholder: "Title of Link"
                                        },
                                        domProps: { value: attachment.title },
                                        on: {
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              attachment,
                                              "title",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    { staticClass: "col-md-7 col-sm-8 col-10" },
                                    [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: attachment.link,
                                            expression: "attachment.link"
                                          }
                                        ],
                                        staticClass:
                                          "form-control custom-form-control",
                                        attrs: {
                                          type: "text",
                                          placeholder: "https://"
                                        },
                                        domProps: { value: attachment.link },
                                        on: {
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              attachment,
                                              "link",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    {
                                      staticClass: "col-md-1 col-sm-1 col-2 p-0"
                                    },
                                    [
                                      _c("i", {
                                        staticClass:
                                          "fa fa-minus-circle icon-34",
                                        on: {
                                          click: function($event) {
                                            _vm.deleteAttachment(i)
                                          }
                                        }
                                      })
                                    ]
                                  )
                                ])
                              ])
                            })
                          ),
                          _vm._v(" "),
                          _c("div", { staticClass: "row pb-3" }, [
                            _c(
                              "div",
                              {
                                staticClass:
                                  "col-md-4 col-sm-3 col-12 pr-md-0 pb-3 pb-sm-0"
                              },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.initTitle,
                                      expression: "initTitle"
                                    }
                                  ],
                                  staticClass:
                                    "form-control custom-form-control",
                                  attrs: {
                                    name: "title_of_link",
                                    type: "text",
                                    placeholder: "Title of Link"
                                  },
                                  domProps: { value: _vm.initTitle },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.initTitle = $event.target.value
                                    }
                                  }
                                })
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-md-7 col-sm-8 col-10" },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.initLink,
                                      expression: "initLink"
                                    }
                                  ],
                                  staticClass:
                                    "form-control custom-form-control",
                                  attrs: {
                                    type: "text",
                                    placeholder: "https://"
                                  },
                                  domProps: { value: _vm.initLink },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.initLink = $event.target.value
                                    }
                                  }
                                })
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-md-1 col-sm-1 p-0 col-2" },
                              [
                                _c("i", {
                                  staticClass: "fa fa-plus-circle icon-34",
                                  on: { click: _vm.addAttachment }
                                })
                              ]
                            ),
                            _vm._v(" "),
                            _vm.titleEmpty
                              ? _c("div", { staticClass: "pl-3" }, [
                                  _c("span", { staticClass: "text-danger" }, [
                                    _vm._v("This is a required field.")
                                  ])
                                ])
                              : _vm._e()
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-md-6" },
                        [
                          _c("h4", { staticClass: "block-subtitle mb-4" }, [
                            _vm._v(
                              "\n                                    Upload App :\n                                  "
                            )
                          ]),
                          _vm._v(" "),
                          _c(
                            "transition-group",
                            {
                              staticClass: "list-group",
                              attrs: { name: _vm.effect, tag: "ul" }
                            },
                            _vm._l(_vm.files, function(file, i) {
                              return _c(
                                "li",
                                { key: i },
                                [
                                  _c("app-uploader", {
                                    staticClass: "row pb-2",
                                    attrs: {
                                      canRemove: "true",
                                      index: i,
                                      file: file
                                    },
                                    on: {
                                      change: _vm.updateFileInCollection,
                                      delete: function($event) {
                                        _vm.deleteFile(i)
                                      }
                                    }
                                  })
                                ],
                                1
                              )
                            })
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            [
                              _c(
                                "app-uploader",
                                {
                                  staticClass: "row pb-sm-2 pb-4",
                                  attrs: {
                                    canRemove: false,
                                    file: _vm.activeFile
                                  },
                                  on: { change: _vm.updateActiveFile }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "col-md-1 col-sm-1 col-2 p-0 pt-2 pt-sm-0",
                                      attrs: { slot: "add-icon" },
                                      slot: "add-icon"
                                    },
                                    [
                                      _c("i", {
                                        staticClass:
                                          "fa fa-plus-circle icon-34",
                                        on: { click: _vm.addFile }
                                      })
                                    ]
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _vm.fileEmpty
                                ? _c("div", { staticClass: "pl-3" }, [
                                    _c("span", { staticClass: "text-danger" }, [
                                      _vm._v("This is a required field.")
                                    ])
                                  ])
                                : _vm._e()
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-md-12" }, [
                  _c("div", { staticClass: "white-box" }, [
                    _c("div", { staticClass: "row pb-4" }, [
                      _c("div", { staticClass: "col-md-12" }, [
                        _c("h4", { staticClass: "block-subtitle" }, [
                          _vm._v(
                            "\n                                        Notes/Instruction :\n                                    "
                          )
                        ]),
                        _vm._v(" "),
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.model.notes_instruction,
                              expression: "model.notes_instruction"
                            }
                          ],
                          staticClass:
                            "form-control custom-form-control resize_vertical mb-3",
                          attrs: {
                            rows: "5",
                            name: "notes_instruction",
                            placeholder:
                              "Enter any notes for the Tester here including installation instructions and any needed codes"
                          },
                          domProps: { value: _vm.model.notes_instruction },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.model,
                                "notes_instruction",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "row pb-3 pt-4" }, [
                      _c(
                        "div",
                        { staticClass: "col-md-12" },
                        [
                          _c("h4", { staticClass: "block-subtitle" }, [
                            _vm._v(
                              "\n                                        Questions\n                                    "
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "row pb-4" }, [
                            _c("span", { staticClass: "col-12" }, [
                              _vm._v(
                                "Add any specific questioins for the Testers"
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c(
                            "transition-group",
                            {
                              staticClass: "list-group",
                              attrs: { name: _vm.effect, tag: "ul" }
                            },
                            _vm._l(_vm.model.questions, function(question, i) {
                              return _c("li", { key: i }, [
                                _c("div", { staticClass: "form-group pb-3" }, [
                                  _c("div", { staticClass: "row" }, [
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "col-md-2 pr-md-0 mb-2 mb-sm-0"
                                      },
                                      [
                                        _c(
                                          "span",
                                          {
                                            staticClass:
                                              "text-bold question-label"
                                          },
                                          [
                                            _vm._v(
                                              "Question " + _vm._s(i + 1) + " :"
                                            )
                                          ]
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      { staticClass: "col-md-8 col-sm-9" },
                                      [
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: _vm.model.questions[i],
                                              expression: "model.questions[i]"
                                            }
                                          ],
                                          staticClass:
                                            "form-control custom-form-control",
                                          attrs: {
                                            type: "text",
                                            name: "questions",
                                            placeholder: "Enter question"
                                          },
                                          domProps: {
                                            value: _vm.model.questions[i]
                                          },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                _vm.model.questions,
                                                i,
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      { staticClass: "col-md-2 col-sm-3" },
                                      [
                                        _c(
                                          "button",
                                          {
                                            staticClass: "purple-btn",
                                            attrs: { type: "button" },
                                            on: {
                                              click: function($event) {
                                                _vm.deleteQuestion(i)
                                              }
                                            }
                                          },
                                          [_vm._v("Remove")]
                                        )
                                      ]
                                    )
                                  ])
                                ])
                              ])
                            })
                          ),
                          _vm._v(" "),
                          _vm.model.questionEmpty
                            ? _c("div", [
                                _c("span", { staticClass: "text-danger" }, [
                                  _vm._v("Please write question.")
                                ])
                              ])
                            : _vm._e(),
                          _vm._v(" "),
                          _c("div", { staticClass: "row pb-4" }, [
                            _c("div", { staticClass: "col-xl-3 col-md-6" }, [
                              _c(
                                "button",
                                {
                                  staticClass: "purple-btn",
                                  attrs: { type: "button" },
                                  on: {
                                    click: function($event) {
                                      _vm.addQuestion(true)
                                    }
                                  }
                                },
                                [_vm._v("Add more Question")]
                              )
                            ])
                          ])
                        ],
                        1
                      )
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "row mb-5" }, [
                _c(
                  "div",
                  { staticClass: "col-md-12 text-center" },
                  [
                    _c(
                      "router-link",
                      {
                        staticClass:
                          "d-block d-sm-inline-block mr-sm-2 mr-0 text-uppercase grey-btn back-proj-btn",
                        attrs: { to: { name: "admin.test_projects" } }
                      },
                      [_vm._v(" Back to Projects ")]
                    ),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass:
                          "d-block d-sm-inline-block text-uppercase purple-btn post-poj-btn",
                        attrs: { type: "submit" }
                      },
                      [_vm._v("Post Test Project")]
                    )
                  ],
                  1
                )
              ])
            ]
          )
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-956bc8dc", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-b80804c4\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/publisher/app-uploader.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "row" },
    [
      _c("div", { staticClass: "col-md-8 col-sm-8 col-12" }, [
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.activeFile.name,
              expression: "activeFile.name"
            }
          ],
          staticClass: "form-control custom-form-control",
          attrs: { type: "text", placeholder: "Title of File", disabled: "" },
          domProps: { value: _vm.activeFile.name },
          on: {
            change: _vm.updateModel,
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.activeFile, "name", $event.target.value)
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-3 col-sm-3 col-10 pl-sm-0" }, [
        _c("input", {
          ref: "app_file",
          staticStyle: { display: "none" },
          attrs: { type: "file" },
          on: { change: _vm.checkFile }
        }),
        _vm._v(" "),
        _c(
          "a",
          {
            staticClass: "btn-primary blue-step-btn",
            attrs: { href: "javascript:;" },
            on: { click: _vm.triggerFileClick }
          },
          [_vm._v("Browse")]
        )
      ]),
      _vm._v(" "),
      _vm.canRemove && _vm.file.name
        ? _c(
            "div",
            { staticClass: "col-md-1 col-sm-1 col-2 p-0 pt-2 pt-sm-0" },
            [
              _c("i", {
                staticClass: "fa fa-minus-circle icon-34",
                on: {
                  click: function($event) {
                    _vm.deleteFile()
                  }
                }
              })
            ]
          )
        : _vm._e(),
      _vm._v(" "),
      _vm._t("add-icon")
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-b80804c4", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-scrollto/vue-scrollto.js":
/***/ (function(module, exports, __webpack_require__) {

(function (global, factory) {
	 true ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(global['vue-scrollto'] = factory());
}(this, (function () { 'use strict';

/**
 * https://github.com/gre/bezier-easing
 * BezierEasing - use bezier curve for transition easing function
 * by Gaëtan Renaudeau 2014 - 2015 – MIT License
 */

// These values are established by empiricism with tests (tradeoff: performance VS precision)
var NEWTON_ITERATIONS = 4;
var NEWTON_MIN_SLOPE = 0.001;
var SUBDIVISION_PRECISION = 0.0000001;
var SUBDIVISION_MAX_ITERATIONS = 10;

var kSplineTableSize = 11;
var kSampleStepSize = 1.0 / (kSplineTableSize - 1.0);

var float32ArraySupported = typeof Float32Array === 'function';

function A (aA1, aA2) { return 1.0 - 3.0 * aA2 + 3.0 * aA1; }
function B (aA1, aA2) { return 3.0 * aA2 - 6.0 * aA1; }
function C (aA1)      { return 3.0 * aA1; }

// Returns x(t) given t, x1, and x2, or y(t) given t, y1, and y2.
function calcBezier (aT, aA1, aA2) { return ((A(aA1, aA2) * aT + B(aA1, aA2)) * aT + C(aA1)) * aT; }

// Returns dx/dt given t, x1, and x2, or dy/dt given t, y1, and y2.
function getSlope (aT, aA1, aA2) { return 3.0 * A(aA1, aA2) * aT * aT + 2.0 * B(aA1, aA2) * aT + C(aA1); }

function binarySubdivide (aX, aA, aB, mX1, mX2) {
  var currentX, currentT, i = 0;
  do {
    currentT = aA + (aB - aA) / 2.0;
    currentX = calcBezier(currentT, mX1, mX2) - aX;
    if (currentX > 0.0) {
      aB = currentT;
    } else {
      aA = currentT;
    }
  } while (Math.abs(currentX) > SUBDIVISION_PRECISION && ++i < SUBDIVISION_MAX_ITERATIONS);
  return currentT;
}

function newtonRaphsonIterate (aX, aGuessT, mX1, mX2) {
 for (var i = 0; i < NEWTON_ITERATIONS; ++i) {
   var currentSlope = getSlope(aGuessT, mX1, mX2);
   if (currentSlope === 0.0) {
     return aGuessT;
   }
   var currentX = calcBezier(aGuessT, mX1, mX2) - aX;
   aGuessT -= currentX / currentSlope;
 }
 return aGuessT;
}

var src = function bezier (mX1, mY1, mX2, mY2) {
  if (!(0 <= mX1 && mX1 <= 1 && 0 <= mX2 && mX2 <= 1)) {
    throw new Error('bezier x values must be in [0, 1] range');
  }

  // Precompute samples table
  var sampleValues = float32ArraySupported ? new Float32Array(kSplineTableSize) : new Array(kSplineTableSize);
  if (mX1 !== mY1 || mX2 !== mY2) {
    for (var i = 0; i < kSplineTableSize; ++i) {
      sampleValues[i] = calcBezier(i * kSampleStepSize, mX1, mX2);
    }
  }

  function getTForX (aX) {
    var intervalStart = 0.0;
    var currentSample = 1;
    var lastSample = kSplineTableSize - 1;

    for (; currentSample !== lastSample && sampleValues[currentSample] <= aX; ++currentSample) {
      intervalStart += kSampleStepSize;
    }
    --currentSample;

    // Interpolate to provide an initial guess for t
    var dist = (aX - sampleValues[currentSample]) / (sampleValues[currentSample + 1] - sampleValues[currentSample]);
    var guessForT = intervalStart + dist * kSampleStepSize;

    var initialSlope = getSlope(guessForT, mX1, mX2);
    if (initialSlope >= NEWTON_MIN_SLOPE) {
      return newtonRaphsonIterate(aX, guessForT, mX1, mX2);
    } else if (initialSlope === 0.0) {
      return guessForT;
    } else {
      return binarySubdivide(aX, intervalStart, intervalStart + kSampleStepSize, mX1, mX2);
    }
  }

  return function BezierEasing (x) {
    if (mX1 === mY1 && mX2 === mY2) {
      return x; // linear
    }
    // Because JavaScript number are imprecise, we should guarantee the extremes are right.
    if (x === 0) {
      return 0;
    }
    if (x === 1) {
      return 1;
    }
    return calcBezier(getTForX(x), mY1, mY2);
  };
};

var easings = {
    ease: [0.25, 0.1, 0.25, 1.0],
    linear: [0.00, 0.0, 1.00, 1.0],
    "ease-in": [0.42, 0.0, 1.00, 1.0],
    "ease-out": [0.00, 0.0, 0.58, 1.0],
    "ease-in-out": [0.42, 0.0, 0.58, 1.0]
};

// https://github.com/WICG/EventListenerOptions/blob/gh-pages/explainer.md#feature-detection
var supportsPassive = false;
try {
    var opts = Object.defineProperty({}, "passive", {
        get: function get() {
            supportsPassive = true;
        }
    });
    window.addEventListener("test", null, opts);
} catch (e) {}

var _ = {
    $: function $(selector) {
        if (typeof selector !== "string") {
            return selector;
        }
        return document.querySelector(selector);
    },
    on: function on(element, events, handler) {
        var opts = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : { passive: false };

        if (!(events instanceof Array)) {
            events = [events];
        }
        for (var i = 0; i < events.length; i++) {
            element.addEventListener(events[i], handler, supportsPassive ? opts : false);
        }
    },
    off: function off(element, events, handler) {
        if (!(events instanceof Array)) {
            events = [events];
        }
        for (var i = 0; i < events.length; i++) {
            element.removeEventListener(events[i], handler);
        }
    },
    cumulativeOffset: function cumulativeOffset(element) {
        var top = 0;
        var left = 0;

        do {
            top += element.offsetTop || 0;
            left += element.offsetLeft || 0;
            element = element.offsetParent;
        } while (element);

        return {
            top: top,
            left: left
        };
    }
};

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
  return typeof obj;
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
};





















var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var abortEvents = ["mousedown", "wheel", "DOMMouseScroll", "mousewheel", "keyup", "touchmove"];

var defaults$$1 = {
    container: "body",
    duration: 500,
    easing: "ease",
    offset: 0,
    cancelable: true,
    onDone: false,
    onCancel: false,
    x: false,
    y: true
};

function setDefaults(options) {
    defaults$$1 = _extends({}, defaults$$1, options);
}

var scroller = function scroller() {
    var element = void 0; // element to scroll to
    var container = void 0; // container to scroll
    var duration = void 0; // duration of the scrolling
    var easing = void 0; // easing to be used when scrolling
    var offset = void 0; // offset to be added (subtracted)
    var cancelable = void 0; // indicates if user can cancel the scroll or not.
    var onDone = void 0; // callback when scrolling is done
    var onCancel = void 0; // callback when scrolling is canceled / aborted
    var x = void 0; // scroll on x axis
    var y = void 0; // scroll on y axis

    var initialX = void 0; // initial X of container
    var targetX = void 0; // target X of container
    var initialY = void 0; // initial Y of container
    var targetY = void 0; // target Y of container
    var diffX = void 0; // difference
    var diffY = void 0; // difference

    var abort = void 0; // is scrolling aborted

    var abortEv = void 0; // event that aborted scrolling
    var abortFn = function abortFn(e) {
        if (!cancelable) return;
        abortEv = e;
        abort = true;
    };
    var easingFn = void 0;

    var timeStart = void 0; // time when scrolling started
    var timeElapsed = void 0; // time elapsed since scrolling started

    var progress = void 0; // progress

    function scrollTop(container) {
        var scrollTop = container.scrollTop;

        if (container.tagName.toLowerCase() === "body") {
            // in firefox body.scrollTop always returns 0
            // thus if we are trying to get scrollTop on a body tag
            // we need to get it from the documentElement
            scrollTop = scrollTop || document.documentElement.scrollTop;
        }

        return scrollTop;
    }

    function scrollLeft(container) {
        var scrollLeft = container.scrollLeft;

        if (container.tagName.toLowerCase() === "body") {
            // in firefox body.scrollLeft always returns 0
            // thus if we are trying to get scrollLeft on a body tag
            // we need to get it from the documentElement
            scrollLeft = scrollLeft || document.documentElement.scrollLeft;
        }

        return scrollLeft;
    }

    function step(timestamp) {
        if (abort) return done();
        if (!timeStart) timeStart = timestamp;

        timeElapsed = timestamp - timeStart;

        progress = Math.min(timeElapsed / duration, 1);
        progress = easingFn(progress);

        topLeft(container, initialY + diffY * progress, initialX + diffX * progress);

        timeElapsed < duration ? window.requestAnimationFrame(step) : done();
    }

    function done() {
        if (!abort) topLeft(container, targetY, targetX);
        timeStart = false;

        _.off(container, abortEvents, abortFn);
        if (abort && onCancel) onCancel(abortEv);
        if (!abort && onDone) onDone();
    }

    function topLeft(element, top, left) {
        if (y) element.scrollTop = top;
        if (x) element.scrollLeft = left;
        if (element.tagName.toLowerCase() === "body") {
            // in firefox body.scrollTop doesn't scroll the page
            // thus if we are trying to scrollTop on a body tag
            // we need to scroll on the documentElement
            if (y) document.documentElement.scrollTop = top;
            if (x) document.documentElement.scrollLeft = left;
        }
    }

    function scrollTo(target, _duration) {
        var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

        if ((typeof _duration === "undefined" ? "undefined" : _typeof(_duration)) === "object") {
            options = _duration;
        } else if (typeof _duration === "number") {
            options.duration = _duration;
        }

        element = _.$(target);

        if (!element) {
            return console.warn("[vue-scrollto warn]: Trying to scroll to an element that is not on the page: " + target);
        }

        container = _.$(options.container || defaults$$1.container);
        duration = options.duration || defaults$$1.duration;
        easing = options.easing || defaults$$1.easing;
        offset = options.offset || defaults$$1.offset;
        cancelable = options.hasOwnProperty("cancelable") ? options.cancelable !== false : defaults$$1.cancelable;
        onDone = options.onDone || defaults$$1.onDone;
        onCancel = options.onCancel || defaults$$1.onCancel;
        x = options.x === undefined ? defaults$$1.x : options.x;
        y = options.y === undefined ? defaults$$1.y : options.y;

        var cumulativeOffsetContainer = _.cumulativeOffset(container);
        var cumulativeOffsetElement = _.cumulativeOffset(element);

        if (typeof offset === "function") {
            offset = offset();
        }

        initialY = scrollTop(container);
        targetY = cumulativeOffsetElement.top - cumulativeOffsetContainer.top + offset;

        initialX = scrollLeft(container);
        targetX = cumulativeOffsetElement.left - cumulativeOffsetContainer.left + offset;

        abort = false;

        diffY = targetY - initialY;
        diffX = targetX - initialX;

        if (typeof easing === "string") {
            easing = easings[easing] || easings["ease"];
        }

        easingFn = src.apply(src, easing);

        if (!diffY && !diffX) return;

        _.on(container, abortEvents, abortFn, { passive: true });

        window.requestAnimationFrame(step);

        return function () {
            abortEv = null;
            abort = true;
        };
    }

    return scrollTo;
};

var _scroller = scroller();

var bindings = []; // store binding data

function deleteBinding(el) {
    for (var i = 0; i < bindings.length; ++i) {
        if (bindings[i].el === el) {
            bindings.splice(i, 1);
            return true;
        }
    }
    return false;
}

function findBinding(el) {
    for (var i = 0; i < bindings.length; ++i) {
        if (bindings[i].el === el) {
            return bindings[i];
        }
    }
}

function getBinding(el) {
    var binding = findBinding(el);

    if (binding) {
        return binding;
    }

    bindings.push(binding = {
        el: el,
        binding: {}
    });

    return binding;
}

function handleClick(e) {
    e.preventDefault();
    var ctx = getBinding(this).binding;

    if (typeof ctx.value === "string") {
        return _scroller(ctx.value);
    }
    _scroller(ctx.value.el || ctx.value.element, ctx.value);
}

var VueScrollTo$1 = {
    bind: function bind(el, binding) {
        getBinding(el).binding = binding;
        _.on(el, "click", handleClick);
    },
    unbind: function unbind(el) {
        deleteBinding(el);
        _.off(el, "click", handleClick);
    },
    update: function update(el, binding) {
        getBinding(el).binding = binding;
    },

    scrollTo: _scroller,
    bindings: bindings
};

var install = function install(Vue, options) {
    if (options) setDefaults(options);
    Vue.directive("scroll-to", VueScrollTo$1);
    Vue.prototype.$scrollTo = VueScrollTo$1.scrollTo;
};

if (typeof window !== "undefined" && window.Vue) {
    window.VueScrollTo = VueScrollTo$1;
    window.VueScrollTo.setDefaults = setDefaults;
    Vue.use(install);
}

VueScrollTo$1.install = install;

return VueScrollTo$1;

})));


/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/device-selector.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/device-selector.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("4b79eeec", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./device-selector.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./device-selector.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-956bc8dc\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=1!./resources/assets/components/pages/test-project/test_project_add.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-956bc8dc\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=1!./resources/assets/components/pages/test-project/test_project_add.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("298f221e", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-956bc8dc\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=1!./test_project_add.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-956bc8dc\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=1!./test_project_add.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-956bc8dc\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/test-project/test_project_add.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-956bc8dc\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/test-project/test_project_add.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("92836628", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-956bc8dc\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./test_project_add.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-956bc8dc\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./test_project_add.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b80804c4\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/app-uploader.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b80804c4\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/app-uploader.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("4fc3c981", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b80804c4\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./app-uploader.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b80804c4\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./app-uploader.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue2-animate/dist/vue2-animate.min.css":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue2-animate/dist/vue2-animate.min.css");
if(typeof content === 'string') content = [[module.i, content, '']];
// Prepare cssTransformation
var transform;

var options = {}
options.transform = transform
// add the styles to the DOM
var update = __webpack_require__("./node_modules/style-loader/lib/addStyles.js")(content, options);
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../css-loader/index.js!./vue2-animate.min.css", function() {
			var newContent = require("!!../../css-loader/index.js!./vue2-animate.min.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/components/components/device-selector.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0d653902\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/device-selector.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/device-selector.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-0d653902\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/device-selector.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-0d653902"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\device-selector.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0d653902", Component.options)
  } else {
    hotAPI.reload("data-v-0d653902", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/components/publisher/app-uploader.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b80804c4\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/publisher/app-uploader.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/publisher/app-uploader.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-b80804c4\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/publisher/app-uploader.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-b80804c4"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\publisher\\app-uploader.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-b80804c4", Component.options)
  } else {
    hotAPI.reload("data-v-b80804c4", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/pages/test-project/test_project_add.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-956bc8dc\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/test-project/test_project_add.vue")
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-956bc8dc\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=1!./resources/assets/components/pages/test-project/test_project_add.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/test-project/test_project_add.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-956bc8dc\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/test-project/test_project_add.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-956bc8dc"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\test-project\\test_project_add.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-956bc8dc", Component.options)
  } else {
    hotAPI.reload("data-v-956bc8dc", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/services/utils.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return objectToFormData; });
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

// takes a {} object and returns a FormData object
var objectToFormData = function objectToFormData(obj, form, namespace) {
  var fd = form || new FormData();
  var formKey;

  for (var property in obj) {
    if (obj.hasOwnProperty(property)) {
      if (namespace) {
        formKey = namespace + "[" + property + "]";
      } else {
        formKey = property;
      }

      // if the property is an object, but not a File,
      // use recursivity.
      if (_typeof(obj[property]) === "object" && !(obj[property] instanceof File)) {
        objectToFormData(obj[property], fd, property);
      } else {
        // if it's a string or a File object
        fd.append(formKey, obj[property]);
      }
    }
  }

  return fd;
};

/***/ })

});