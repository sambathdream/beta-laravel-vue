webpackJsonp([79],{

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3cd927c0\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/publisher/dashboard-standard.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.custom-container[data-v-3cd927c0] {\n  max-width: 100%;\n}\n.custom-container .text-blue[data-v-3cd927c0] {\n    color: #0082cc;\n}\n.custom-container .text-red[data-v-3cd927c0] {\n    color: #e63423;\n}\n.custom-container .text-green[data-v-3cd927c0] {\n    color: #2cac3d;\n}\n.custom-container .text-bold[data-v-3cd927c0] {\n    font-family: \"BrandonTextBold\";\n}\n.custom-container .step-page-title[data-v-3cd927c0] {\n    font-family: \"UniNeueBold\";\n    font-size: 30px;\n    width: 100%;\n    text-align: center;\n    color: #363e48;\n    margin: 5px 0 25px;\n}\n.custom-container .dashboard-page[data-v-3cd927c0] {\n    width: 100%;\n}\n.custom-container .dashboard-page .white-box[data-v-3cd927c0] {\n      background-color: #fff;\n      -webkit-box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n              box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n      border-radius: 4px;\n      padding: 20px;\n      margin-bottom: 20px;\n      font-size: 17px;\n      color: #606368;\n}\n.custom-container .dashboard-page .white-box .block-title[data-v-3cd927c0] {\n        font-size: 20px;\n        font-family: \"UniNeueBold\";\n        border-bottom: 1px solid #dadada;\n        color: #0082cc;\n        padding-bottom: 10px;\n        margin-bottom: 10px;\n        width: auto;\n        min-height: 35px;\n}\n.custom-container .dashboard-page .white-box .block-title a[data-v-3cd927c0] {\n          font-family: \"BrandonTextMedium\";\n          font-size: 17px;\n          text-decoration: underline !important;\n          float: right;\n}\n.custom-container .dashboard-page .white-box .proj-status[data-v-3cd927c0] {\n        font-size: 17px;\n        font-family: \"BrandonTextMedium\";\n        margin-bottom: 0;\n}\n.custom-container .dashboard-page .white-box .proj-count[data-v-3cd927c0] {\n        font-size: 40px;\n        font-family: \"UniNeueBold\";\n        line-height: 30px;\n        color: #0082cc;\n}\n.custom-container .dashboard-page .white-box .no-proj-desc[data-v-3cd927c0] {\n        padding: 25px 0 10px;\n        text-align: center;\n}\n.custom-container .dashboard-page .white-box .activity-list ul[data-v-3cd927c0] {\n        padding-left: 20px;\n        list-style: disc;\n        margin-bottom: 0;\n}\n.custom-container .dashboard-page .white-box .activity-list ul li span[data-v-3cd927c0] {\n          font-size: 14px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block[data-v-3cd927c0] {\n        width: 100%;\n        border-top: 1px solid #dadada;\n        padding: 20px 0;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block[data-v-3cd927c0]:first-child {\n          border-top: none;\n          padding-top: 10px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block[data-v-3cd927c0]:last-child {\n          padding-bottom: 5px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block p[data-v-3cd927c0] {\n          margin: 5px 0;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .details-wrap[data-v-3cd927c0] {\n          border-right: 1px solid #dadada;\n          position: relative;\n          width: auto;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .btn-wrap[data-v-3cd927c0] {\n          position: relative;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .btn-wrap .green-step-btn[data-v-3cd927c0] {\n            margin: 35px 0 20px;\n            padding: 3px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .badge-wrap[data-v-3cd927c0] {\n          position: relative;\n          border: 1px solid #2cac3d;\n          border-right: 0;\n          border-top-left-radius: 30px;\n          border-bottom-left-radius: 30px;\n          margin-right: -20px;\n          padding: 3px 15px;\n          line-height: 20px;\n          font-size: 15px;\n          z-index: 1;\n          line-height: 18px;\n          float: right;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .pending-amt .b-right[data-v-3cd927c0] {\n          border-right: 1px solid #434343;\n          padding-right: 5px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .proj-name[data-v-3cd927c0] {\n          color: #363e48;\n          font-size: 22px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .proj-price[data-v-3cd927c0] {\n          font-size: 22px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .rating-star[data-v-3cd927c0] {\n          font-family: \"FontAwesome\";\n          font-size: 16px;\n          color: #ffcc00;\n}\n.custom-container .dashboard-page .status-bar[data-v-3cd927c0] {\n      width: 100%;\n      border-radius: 4px;\n      text-align: center;\n      font-size: 17px;\n      font-family: \"BrandonTextMedium\";\n      min-width: 40px;\n      padding: 5px 0;\n      background-color: #fff;\n      margin-bottom: 20px;\n}\n.custom-container .dashboard-page .status-bar.blue-status-bar[data-v-3cd927c0] {\n      color: #0082cc;\n      border: 2px solid #0082cc;\n      display: none;\n}\n.custom-container .dashboard-page .status-bar.green-status-bar[data-v-3cd927c0] {\n      color: #2cac3d;\n      border: 2px solid #2cac3d;\n}\n.custom-container .dashboard-page .blue-step-btn[data-v-3cd927c0] {\n      width: 100%;\n      padding: 2px 4px;\n      border: 2px solid #0082cc;\n      background-color: #00aff5;\n      font-size: 14px;\n      font-family: \"BrandonTextMedium\";\n      letter-spacing: 0.05rem;\n      color: #fff;\n      border-radius: 20px;\n      display: block;\n      text-align: center;\n}\n.custom-container .dashboard-page .blue-step-btn[data-v-3cd927c0]:hover {\n        background-color: #13b9fb;\n}\n.custom-container .dashboard-page .post-btn[data-v-3cd927c0] {\n      padding: 7px 10px;\n}\n@media screen and (max-width: 1281px) {\n.custom-container .dashboard-page .white-box[data-v-3cd927c0] {\n    font-size: 16px;\n}\n.custom-container .dashboard-page .white-box.proj-summary[data-v-3cd927c0] {\n    padding: 20px 7px;\n}\n.custom-container .dashboard-page .blue-step-btn[data-v-3cd927c0] {\n    font-size: 13px;\n}\n}\n@media screen and (max-width: 1200px) {\n.custom-container .page-title[data-v-3cd927c0] {\n    font-size: 28px;\n    margin: 0 0 20px;\n}\n.custom-container .dashboard-page .blue-step-btn[data-v-3cd927c0] {\n    font-size: 12px;\n    letter-spacing: 0;\n}\n.custom-container .dashboard-page .white-box[data-v-3cd927c0] {\n    font-size: 14px;\n}\n.custom-container .dashboard-page .white-box .block-title[data-v-3cd927c0] {\n      font-size: 18px;\n}\n.custom-container .dashboard-page .white-box .proj-count[data-v-3cd927c0] {\n      font-size: 28px;\n      line-height: 24px;\n}\n.custom-container .dashboard-page .white-box .proj-status[data-v-3cd927c0] {\n      font-size: 15px;\n}\n}\n@media screen and (max-width: 991px) {\n.custom-container .dashboard-page .white-box[data-v-3cd927c0] {\n    padding: 15px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .badge-wrap[data-v-3cd927c0] {\n      margin-right: -15px;\n}\n}\n@media screen and (max-width: 767px) {\n.custom-container .dashboard-page .status-bar[data-v-3cd927c0] {\n    font-size: 14px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .details-wrap[data-v-3cd927c0] {\n    border-right: none;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .proj-name[data-v-3cd927c0] {\n    font-size: 20px;\n}\n.custom-container .dashboard-page .white-box .proj-desc-block .badge-wrap[data-v-3cd927c0] {\n    margin-top: 10px;\n    border-left: 0px;\n    margin-left: -15px;\n    margin-right: 0;\n    border-right: 1px solid;\n    border-top-right-radius: 30px;\n    border-bottom-right-radius: 30px;\n    border-top-left-radius: 0;\n    border-bottom-left-radius: 0;\n    float: left;\n    padding: 3px 10px;\n}\n}\n@media screen and (max-width: 575px) {\n.custom-container .page-title[data-v-3cd927c0] {\n    font-size: 26px;\n}\n.custom-container .dashboard-page .blue-step-btn[data-v-3cd927c0] {\n    max-width: 180px;\n    margin: 15px 0 0;\n}\n.custom-container .dashboard-page .white-box[data-v-3cd927c0] {\n    font-size: 14px;\n    line-height: 18px;\n}\n.custom-container .dashboard-page .white-box .proj-name[data-v-3cd927c0] {\n      font-size: 16px;\n}\n.custom-container .dashboard-page .white-box.proj-summary[data-v-3cd927c0] {\n    padding: 20px 10px;\n    max-height: 100px;\n    min-height: 100px;\n}\n.custom-container .dashboard-page .status-bar[data-v-3cd927c0] {\n    margin-bottom: 15px;\n    padding: 5px;\n}\n.custom-container .dashboard-page .post-btn[data-v-3cd927c0] {\n    max-width: 150px;\n    float: right;\n    margin: 0 0 15px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-3cd927c0\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/publisher/dashboard-standard.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "container custom-container" }, [
      _c("div", { staticClass: "row" }, [
        _c("h1", { staticClass: "step-page-title" }, [
          _vm._v("\n      Dashboard\n    ")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "dashboard-page" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-12 col-sm-6 col-lg-3 text-center" }, [
              _c("div", { staticClass: "white-box proj-summary" }, [
                _c("h3", { staticClass: "proj-count" }, [_vm._v("3")]),
                _vm._v(" "),
                _c("p", { staticClass: "proj-status" }, [
                  _vm._v("Running Projects")
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-12 col-sm-6 col-lg-3 text-center" }, [
              _c("div", { staticClass: "white-box proj-summary" }, [
                _c("h3", { staticClass: "proj-count" }, [_vm._v("2")]),
                _vm._v(" "),
                _c("p", { staticClass: "proj-status" }, [
                  _vm._v("Pending Approvals")
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-12 col-sm-6 col-lg-3 text-center" }, [
              _c("div", { staticClass: "white-box proj-summary" }, [
                _c("h3", { staticClass: "proj-count" }, [_vm._v("1")]),
                _vm._v(" "),
                _c("p", { staticClass: "proj-status" }, [_vm._v("Inbox")])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-12 col-sm-6 col-lg-3 text-center" }, [
              _c("div", { staticClass: "white-box proj-summary" }, [
                _c("h3", { staticClass: "proj-count" }, [_vm._v("$0")]),
                _vm._v(" "),
                _c("p", { staticClass: "proj-status" }, [_vm._v("My spending")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-sm-9 col-12" }, [
              _c("div", { staticClass: "status-bar green-status-bar" }, [
                _vm._v(
                  "\n            Project name 1.2 is ready for review. Click here to see review\n          "
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-sm-3 col-12" }, [
              _c(
                "a",
                {
                  staticClass: "text-uppercase blue-step-btn post-btn",
                  attrs: { href: "#" }
                },
                [_vm._v("post new project")]
              )
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6 col-12" }, [
              _c("div", { staticClass: "white-box" }, [
                _c("h4", { staticClass: "block-title" }, [
                  _vm._v("\n              Payment Summary\n            ")
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "activity-list" }, [
                  _c("ul", [
                    _c("li", [
                      _vm._v(
                        "\n                  Waiting for Approval\n                "
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", [
                      _vm._v(
                        "\n                  The Beta Plan sent paypal invoice for Project name 1.1 on 10th Jan 2018\n                "
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", [
                      _vm._v(
                        "\n                  Sample Activity #1\n                "
                      )
                    ])
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-6 col-12" }, [
              _c("div", { staticClass: "white-box" }, [
                _c("h4", { staticClass: "block-title" }, [
                  _vm._v("\n              Activities\n            ")
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "activity-list" }, [
                  _c("ul", [
                    _c("li", [
                      _vm._v(
                        "\n                  Project Name 0.1 is submitted for approval on 10th Jan 2018\n                "
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", [
                      _vm._v(
                        "\n                  Project Name 1.2 is ready for review\n                "
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", [
                      _vm._v(
                        "\n                  Sample Activity #1 \n                "
                      )
                    ])
                  ])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "white-box" }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-12" }, [
                _c("h4", { staticClass: "block-title" }, [
                  _vm._v("\n              New Project\n            ")
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-12" }, [
                _c("div", { staticClass: "proj-desc-block" }, [
                  _c("div", { staticClass: "row align-items-center" }, [
                    _c(
                      "div",
                      { staticClass: "col-12 col-md-9 col-xl-10 details-wrap" },
                      [
                        _c("div", { staticClass: "row" }, [
                          _c("div", { staticClass: "col-10 col-xl-12" }, [
                            _c("span", { staticClass: "text-bold proj-name" }, [
                              _vm._v("Project Name 1.1")
                            ])
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "col-12 col-sm-6 col-xl-3" },
                            [
                              _c("p", [
                                _c("span", { staticClass: "text-bold" }, [
                                  _vm._v("Devices :")
                                ]),
                                _vm._v(" "),
                                _c("span", [_vm._v("Rift")])
                              ]),
                              _vm._v(" "),
                              _c("p", [
                                _c("span", { staticClass: "text-bold" }, [
                                  _vm._v("Type :")
                                ]),
                                _vm._v(" "),
                                _c("span", {}, [_vm._v("VR Apps")])
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "col-12 col-sm-6 col-xl-4" },
                            [
                              _c("p", [
                                _c("span", { staticClass: "text-bold" }, [
                                  _vm._v("Tester Needed :")
                                ]),
                                _vm._v(" "),
                                _c("span", {}, [_vm._v("5")])
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "col-12 col-sm-6 col-xl-5 pl-xl-0 text-xl-right text-left"
                            },
                            [
                              _c(
                                "p",
                                { staticClass: "text-xl-right text-left" },
                                [
                                  _c("span", { staticClass: "text-bold" }, [
                                    _vm._v("Start Date : ")
                                  ]),
                                  _vm._v(" "),
                                  _c("span", [_vm._v("1 January,2018")])
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "p",
                                { staticClass: "text-xl-right text-left" },
                                [
                                  _c("span", { staticClass: "text-bold" }, [
                                    _vm._v("End Date : ")
                                  ]),
                                  _vm._v(" "),
                                  _c("span", [_vm._v("15 January,2018")])
                                ]
                              )
                            ]
                          )
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "col-12 col-sm-4 col-md-3 col-xl-2" },
                      [
                        _c(
                          "span",
                          {
                            staticClass:
                              "badge-wrap text-center text-green text-bold"
                          },
                          [_vm._v("Waiting for Approval")]
                        )
                      ]
                    )
                  ])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "white-box" }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-12" }, [
                _c("h4", { staticClass: "block-title" }, [
                  _vm._v("\n              Running Projects\n              "),
                  _c(
                    "a",
                    {
                      staticClass: "text-right text-underline text-blue",
                      attrs: { href: "#" }
                    },
                    [_vm._v("View All")]
                  )
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-12" }, [
                _c("div", { staticClass: "proj-desc-block" }, [
                  _c("div", { staticClass: "row align-items-center" }, [
                    _c(
                      "div",
                      { staticClass: "col-12 col-md-9 col-xl-10 details-wrap" },
                      [
                        _c("div", { staticClass: "row" }, [
                          _c("div", { staticClass: "col-10 col-xl-12" }, [
                            _c("span", { staticClass: "text-bold proj-name" }, [
                              _vm._v("Project Name 1.1")
                            ])
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "col-12 col-sm-6 col-xl-3" },
                            [
                              _c("p", [
                                _c("span", { staticClass: "text-bold" }, [
                                  _vm._v("Device :")
                                ]),
                                _vm._v(" "),
                                _c("span", [_vm._v("iPhone")])
                              ]),
                              _vm._v(" "),
                              _c("p", [
                                _c("span", { staticClass: "text-bold" }, [
                                  _vm._v("Type :")
                                ]),
                                _vm._v(" "),
                                _c("span", {}, [_vm._v("AR Apps")])
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "col-12 col-sm-6 col-xl-4" },
                            [
                              _c("p", [
                                _c("span", { staticClass: "text-bold" }, [
                                  _vm._v("Tester Needed :")
                                ]),
                                _vm._v(" "),
                                _c("span", {}, [_vm._v("5")])
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "col-12 col-sm-6 col-xl-5 pl-xl-0 text-xl-right text-left"
                            },
                            [
                              _c(
                                "p",
                                { staticClass: "text-xl-right text-left" },
                                [
                                  _c("span", { staticClass: "text-bold" }, [
                                    _vm._v("Start Date : ")
                                  ]),
                                  _vm._v(" "),
                                  _c("span", [_vm._v("1 January,2018")])
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "p",
                                { staticClass: "text-xl-right text-left" },
                                [
                                  _c("span", { staticClass: "text-bold" }, [
                                    _vm._v("End Date : ")
                                  ]),
                                  _vm._v(" "),
                                  _c("span", [_vm._v("15 January,2018")])
                                ]
                              )
                            ]
                          )
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "col-6 col-sm-4 col-md-3 col-xl-2 btn-wrap"
                      },
                      [
                        _c(
                          "a",
                          {
                            staticClass: "blue-step-btn",
                            attrs: { href: "#" }
                          },
                          [_vm._v("View Results")]
                        )
                      ]
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "proj-desc-block" }, [
                  _c("div", { staticClass: "row align-items-center" }, [
                    _c(
                      "div",
                      { staticClass: "col-12 col-md-9 col-xl-10 details-wrap" },
                      [
                        _c("div", { staticClass: "row" }, [
                          _c("div", { staticClass: "col-10 col-xl-12" }, [
                            _c("span", { staticClass: "text-bold proj-name" }, [
                              _vm._v("Project Name 1.2")
                            ])
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "col-12 col-sm-6 col-xl-3" },
                            [
                              _c("p", [
                                _c("span", { staticClass: "text-bold" }, [
                                  _vm._v("Device :")
                                ]),
                                _vm._v(" "),
                                _c("span", [_vm._v("Rift")])
                              ]),
                              _vm._v(" "),
                              _c("p", [
                                _c("span", { staticClass: "text-bold" }, [
                                  _vm._v("Type :")
                                ]),
                                _vm._v(" "),
                                _c("span", {}, [_vm._v("AR Apps")])
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "col-12 col-sm-6 col-xl-4" },
                            [
                              _c("p", [
                                _c("span", { staticClass: "text-bold" }, [
                                  _vm._v("Tester Needed :")
                                ]),
                                _vm._v(" "),
                                _c("span", {}, [_vm._v("5")])
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "col-12 col-sm-6 col-xl-5 pl-xl-0 text-xl-right text-left"
                            },
                            [
                              _c(
                                "p",
                                { staticClass: "text-xl-right text-left" },
                                [
                                  _c("span", { staticClass: "text-bold" }, [
                                    _vm._v("Start Date : ")
                                  ]),
                                  _vm._v(" "),
                                  _c("span", [_vm._v("1 January,2018")])
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "p",
                                { staticClass: "text-xl-right text-left" },
                                [
                                  _c("span", { staticClass: "text-bold" }, [
                                    _vm._v("End Date : ")
                                  ]),
                                  _vm._v(" "),
                                  _c("span", [_vm._v("15 January,2018")])
                                ]
                              )
                            ]
                          )
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "col-6 col-sm-4 col-md-3 col-xl-2 btn-wrap"
                      },
                      [
                        _c(
                          "a",
                          {
                            staticClass: "blue-step-btn",
                            attrs: { href: "#" }
                          },
                          [_vm._v("View Results")]
                        )
                      ]
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "proj-desc-block" }, [
                  _c("div", { staticClass: "row align-items-center" }, [
                    _c(
                      "div",
                      { staticClass: "col-12 col-md-9 col-xl-10 details-wrap" },
                      [
                        _c("div", { staticClass: "row" }, [
                          _c("div", { staticClass: "col-10 col-xl-12" }, [
                            _c("span", { staticClass: "text-bold proj-name" }, [
                              _vm._v("Project Name 1.3")
                            ])
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "col-12 col-sm-6 col-xl-3" },
                            [
                              _c("p", [
                                _c("span", { staticClass: "text-bold" }, [
                                  _vm._v("Device :")
                                ]),
                                _vm._v(" "),
                                _c("span", [_vm._v("Rift,Windows")])
                              ]),
                              _vm._v(" "),
                              _c("p", [
                                _c("span", { staticClass: "text-bold" }, [
                                  _vm._v("Type :")
                                ]),
                                _vm._v(" "),
                                _c("span", {}, [_vm._v("AR/MR Apps")])
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "col-12 col-sm-6 col-xl-4" },
                            [
                              _c("p", [
                                _c("span", { staticClass: "text-bold" }, [
                                  _vm._v("Tester Needed :")
                                ]),
                                _vm._v(" "),
                                _c("span", {}, [_vm._v("5")])
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "col-12 col-sm-6 col-xl-5 pl-xl-0 text-xl-right text-left"
                            },
                            [
                              _c(
                                "p",
                                { staticClass: "text-xl-right text-left" },
                                [
                                  _c("span", { staticClass: "text-bold" }, [
                                    _vm._v("Start Date : ")
                                  ]),
                                  _vm._v(" "),
                                  _c("span", [_vm._v("1 January,2018")])
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "p",
                                { staticClass: "text-xl-right text-left" },
                                [
                                  _c("span", { staticClass: "text-bold" }, [
                                    _vm._v("End Date : ")
                                  ]),
                                  _vm._v(" "),
                                  _c("span", [_vm._v("15 January,2018")])
                                ]
                              )
                            ]
                          )
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "col-6 col-sm-4 col-md-3 col-xl-2 btn-wrap"
                      },
                      [
                        _c(
                          "a",
                          {
                            staticClass: "blue-step-btn",
                            attrs: { href: "#" }
                          },
                          [_vm._v("View Results")]
                        )
                      ]
                    )
                  ])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "white-box" }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-12" }, [
                _c("h4", { staticClass: "block-title" }, [
                  _vm._v("\n              Completed Projects\n            ")
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-12" }, [
                _c("div", { staticClass: "proj-desc-block" }, [
                  _c("div", { staticClass: "row" }, [
                    _c("div", { staticClass: "col-12 col-md-6 col-xl-6" }, [
                      _c("span", { staticClass: "text-bold proj-name" }, [
                        _vm._v("Project Name 0.1")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "row" }, [
                        _c("div", { staticClass: "col-12 col-sm-6 col-xl-6" }, [
                          _c("p", [
                            _c("span", { staticClass: "text-bold" }, [
                              _vm._v("Device :")
                            ]),
                            _vm._v(" "),
                            _c("span", [_vm._v(" iPhone ")])
                          ]),
                          _vm._v(" "),
                          _c("p", [
                            _c("span", { staticClass: "text-bold" }, [
                              _vm._v("Type :")
                            ]),
                            _vm._v(" "),
                            _c("span", {}, [_vm._v("Games")])
                          ]),
                          _vm._v(" "),
                          _c("p", [
                            _c("span", { staticClass: "text-bold" }, [
                              _vm._v("Testers :")
                            ]),
                            _vm._v(" "),
                            _c("span", {}, [_vm._v("5")])
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "col-12 col-sm-6 col-xl-6" }, [
                          _c("p", { staticClass: "text-left" }, [
                            _c("span", { staticClass: "text-bold" }, [
                              _vm._v("Start Date : ")
                            ]),
                            _vm._v(" "),
                            _c("span", [_vm._v("10 January, 2018")])
                          ]),
                          _vm._v(" "),
                          _c("p", { staticClass: "text-left" }, [
                            _c("span", { staticClass: "text-bold" }, [
                              _vm._v("End Date : ")
                            ]),
                            _vm._v(" "),
                            _c("span", [_vm._v("15 January, 2018")])
                          ])
                        ])
                      ])
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass:
                          "col-12 col-md-6 col-xl-6 pl-xl-0 text-lg-right text-left"
                      },
                      [
                        _c("div", { staticClass: "w-100 float-left" }, [
                          _c("p", { staticClass: "badge-wrap" }, [
                            _c(
                              "span",
                              {
                                staticClass:
                                  "text-bold text-green complete-badge"
                              },
                              [_vm._v("Completed")]
                            )
                          ])
                        ]),
                        _vm._v(" "),
                        _c("p", [
                          _vm._v(
                            "\n                    Average Ratings given to testers\n                    "
                          ),
                          _c(
                            "span",
                            {
                              staticClass:
                                "rating-star d-block d-sm-inline-block"
                            },
                            [
                              _c("i", { staticClass: "fa fa-star" }),
                              _vm._v(" "),
                              _c("i", { staticClass: "fa fa-star" }),
                              _vm._v(" "),
                              _c("i", { staticClass: "fa fa-star" }),
                              _vm._v(" "),
                              _c("i", { staticClass: "fa fa-star" }),
                              _vm._v(" "),
                              _c("i", { staticClass: "fa fa-star-o" })
                            ]
                          ),
                          _vm._v(" "),
                          _c("span", { staticClass: "text-bold" }, [
                            _vm._v("4.0")
                          ])
                        ]),
                        _vm._v(" "),
                        _c("p", [
                          _c("span", {}, [
                            _vm._v("Completed on 5 January, 2018")
                          ])
                        ]),
                        _vm._v(" "),
                        _c("p", { staticClass: "pending-amt" }, [
                          _c("span", { staticClass: "b-right" }, [
                            _vm._v("Total Amount : "),
                            _c("span", { staticClass: "text-bold" }, [
                              _vm._v("$100")
                            ])
                          ]),
                          _vm._v(" "),
                          _c("span", {}, [
                            _vm._v("Pending : "),
                            _c("span", { staticClass: "text-bold" }, [
                              _vm._v("$0")
                            ])
                          ])
                        ])
                      ]
                    )
                  ])
                ])
              ])
            ])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-3cd927c0", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3cd927c0\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/publisher/dashboard-standard.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3cd927c0\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/publisher/dashboard-standard.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("e4e64946", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3cd927c0\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./dashboard-standard.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3cd927c0\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./dashboard-standard.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/components/pages/publisher/dashboard-standard.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3cd927c0\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/publisher/dashboard-standard.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = null
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-3cd927c0\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/publisher/dashboard-standard.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-3cd927c0"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\publisher\\dashboard-standard.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3cd927c0", Component.options)
  } else {
    hotAPI.reload("data-v-3cd927c0", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});