webpackJsonp([38],{

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1c5d10da\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/publisher/add-project.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.custom-container[data-v-1c5d10da] {\n  max-width: 100%;\n}\n.custom-container .text-blue[data-v-1c5d10da] {\n    color: #0082cc;\n}\n.custom-container .text-red[data-v-1c5d10da] {\n    color: #e63423;\n}\n.custom-container .text-green[data-v-1c5d10da] {\n    color: #2cac3d;\n}\n.custom-container .text-bold[data-v-1c5d10da] {\n    font-family: \"BrandonTextBold\";\n}\n.custom-container .step-page-title[data-v-1c5d10da] {\n    font-family: \"UniNeueBold\";\n    font-size: 30px;\n    width: 100%;\n    text-align: center;\n    color: #363e48;\n    margin: 5px 0 25px;\n}\n.custom-container .add-project-page[data-v-1c5d10da] {\n    width: 100%;\n}\n.custom-container .add-project-page .white-box[data-v-1c5d10da] {\n      background-color: #fff;\n      -webkit-box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n              box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n      border-radius: 4px;\n      padding: 20px;\n      margin-bottom: 20px;\n      font-size: 17px;\n      color: #606368;\n}\n.custom-container .add-project-page .white-box .block-title[data-v-1c5d10da] {\n        font-size: 20px;\n        font-family: \"UniNeueBold\";\n        border-bottom: 1px solid #dadada;\n        color: #0082cc;\n        padding-bottom: 10px;\n        margin-bottom: 10px;\n        width: auto;\n        min-height: 35px;\n}\n.custom-container .add-project-page .white-box .block-title a[data-v-1c5d10da] {\n          font-family: \"BrandonTextMedium\";\n          font-size: 17px;\n          text-decoration: underline !important;\n          float: right;\n}\n.custom-container .add-project-page .white-box .block-subtitle[data-v-1c5d10da] {\n        font-family: \"BrandonTextBold\";\n        font-size: 20px;\n        color: #0082cc;\n        border-bottom: 1px solid #dadada;\n        padding-bottom: 10px;\n        margin-bottom: 15px;\n}\n.custom-container .add-project-page .white-box .proj-detail-form label[data-v-1c5d10da] {\n        min-width: 200px;\n        font-size: 17px;\n        vertical-align: middle;\n}\n.custom-container .add-project-page .white-box .proj-detail-form .custom-form-control[data-v-1c5d10da] {\n        width: calc(100% - 205px);\n        vertical-align: middle;\n        margin-bottom: 15px;\n        font-size: 17px;\n}\n.custom-container .add-project-page .white-box .proj-detail-form .test-devices-wrap[data-v-1c5d10da] {\n        width: calc(100% - 205px);\n        vertical-align: top;\n}\n.custom-container .add-project-page .blue-step-btn[data-v-1c5d10da] {\n      width: 100%;\n      padding: 2px 4px;\n      border: 2px solid #0082cc;\n      background-color: #00aff5;\n      font-size: 14px;\n      font-family: \"BrandonTextMedium\";\n      letter-spacing: 0.05rem;\n      color: #fff;\n      border-radius: 20px;\n      display: block;\n      text-align: center;\n}\n.custom-container .add-project-page .blue-step-btn[data-v-1c5d10da]:hover {\n        background-color: #13b9fb;\n}\n.custom-container .add-project-page .post-btn[data-v-1c5d10da] {\n      padding: 7px 10px;\n}\n@media screen and (max-width: 1281px) {\n.custom-container .add-project-page .white-box[data-v-1c5d10da] {\n    font-size: 16px;\n}\n.custom-container .add-project-page .blue-step-btn[data-v-1c5d10da] {\n    font-size: 13px;\n}\n}\n@media screen and (max-width: 1200px) {\n.custom-container .page-title[data-v-1c5d10da] {\n    font-size: 28px;\n    margin: 0 0 20px;\n}\n.custom-container .add-project-page .blue-step-btn[data-v-1c5d10da] {\n    font-size: 12px;\n    letter-spacing: 0;\n}\n.custom-container .add-project-page .white-box[data-v-1c5d10da] {\n    font-size: 14px;\n}\n.custom-container .add-project-page .white-box .block-title[data-v-1c5d10da] {\n      font-size: 18px;\n}\n}\n@media screen and (max-width: 991px) {\n.custom-container .add-project-page .white-box[data-v-1c5d10da] {\n    padding: 15px;\n}\n}\n@media screen and (max-width: 767px) {\n.custom-container .add-project-page .status-bar[data-v-1c5d10da] {\n    font-size: 14px;\n}\n}\n@media screen and (max-width: 575px) {\n.custom-container .page-title[data-v-1c5d10da] {\n    font-size: 26px;\n}\n.custom-container .add-project-page .blue-step-btn[data-v-1c5d10da] {\n    max-width: 180px;\n    margin: 15px 0 0;\n}\n.custom-container .add-project-page .white-box[data-v-1c5d10da] {\n    font-size: 14px;\n    line-height: 18px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-1c5d10da\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/publisher/add-project.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "container custom-container" }, [
      _c("div", { staticClass: "row" }, [
        _c("h1", { staticClass: "step-page-title" }, [
          _vm._v("\n      New Project\n    ")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "add-project-page" }, [
          _c("div", { staticClass: "white-box" }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-12" }, [
                _c("h4", { staticClass: "block-subtitle" }, [
                  _vm._v("\n              Project Details\n            ")
                ])
              ])
            ]),
            _vm._v(" "),
            _c("form", { staticClass: "proj-detail-form" }, [
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-12 col-md-6" }, [
                  _c(
                    "div",
                    { staticClass: "form-group mb-2 custom-form-group" },
                    [
                      _c(
                        "label",
                        { staticClass: "d-block d-md-inline-block" },
                        [_vm._v("Project Title :")]
                      ),
                      _vm._v(" "),
                      _c("input", {
                        staticClass:
                          "form-control custom-form-control d-block d-md-inline-block",
                        attrs: {
                          type: "text",
                          placeholder: "Enter Project Title"
                        }
                      })
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "form-group mb-2 custom-form-group" },
                    [
                      _c(
                        "label",
                        { staticClass: "d-block d-md-inline-block" },
                        [_vm._v("Type :")]
                      ),
                      _vm._v(" "),
                      _c("input", {
                        staticClass:
                          "form-control custom-form-control d-block d-md-inline-block",
                        attrs: {
                          type: "text",
                          placeholder: "Enter Project Title"
                        }
                      })
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "form-group mb-2 custom-form-group" },
                    [
                      _c(
                        "label",
                        { staticClass: "d-block d-md-inline-block" },
                        [_vm._v("Start Date :")]
                      ),
                      _vm._v(" "),
                      _c("input", {
                        staticClass:
                          "form-control custom-form-control d-block d-md-inline-block",
                        attrs: { type: "text", placeholder: "mm/dd/yyyy" }
                      })
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "form-group mb-2 custom-form-group" },
                    [
                      _c(
                        "label",
                        { staticClass: "d-block d-md-inline-block" },
                        [_vm._v("End Date :")]
                      ),
                      _vm._v(" "),
                      _c("input", {
                        staticClass:
                          "form-control custom-form-control d-block d-md-inline-block",
                        attrs: { type: "text", placeholder: "mm/dd/yyyy" }
                      })
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-12 col-md-6" }, [
                  _c("div", { staticClass: "form-group mb-2" }, [
                    _c("label", { staticClass: "d-block d-md-inline-block" }, [
                      _vm._v("Time to Test (Approx) :")
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      staticClass:
                        "form-control custom-form-control d-block d-md-inline-block",
                      attrs: { type: "text", placeholder: "--:--:--" }
                    })
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-group mb-2" }, [
                    _c("label", { staticClass: "d-block d-md-inline-block" }, [
                      _vm._v("Tester Needed :")
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      staticClass:
                        "form-control custom-form-control d-block d-md-inline-block",
                      attrs: { type: "text", placeholder: "" }
                    })
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-group" }, [
                    _c("label", { staticClass: "d-block d-md-inline-block" }, [
                      _vm._v(
                        "\n                    Devices to test on :\n                "
                      )
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass:
                          "test-devices-wrap d-block d-md-inline-block"
                      },
                      [
                        _c("img", {
                          staticClass: "img-fluid d-inline-block mr-3 mb-3",
                          attrs: { src: __webpack_require__("./resources/assets/assets/img/test-device-1.png") }
                        }),
                        _vm._v(" "),
                        _c("img", {
                          staticClass: "img-fluid d-inline-block mr-3 mb-3",
                          attrs: { src: __webpack_require__("./resources/assets/assets/img/test-device-2.png") }
                        }),
                        _vm._v(" "),
                        _c("img", {
                          staticClass: "img-fluid d-inline-block mr-3 mb-3",
                          attrs: { src: __webpack_require__("./resources/assets/assets/img/test-device-3.png") }
                        }),
                        _vm._v(" "),
                        _c("img", {
                          staticClass: "img-fluid d-inline-block mr-3 mb-3",
                          attrs: { src: __webpack_require__("./resources/assets/assets/img/test-device-4.png") }
                        }),
                        _vm._v(" "),
                        _c("img", {
                          staticClass: "img-fluid d-inline-block mr-3 mb-3",
                          attrs: { src: __webpack_require__("./resources/assets/assets/img/test-device-5.png") }
                        }),
                        _vm._v(" "),
                        _c("img", {
                          staticClass: "img-fluid d-inline-block mr-3",
                          attrs: { src: __webpack_require__("./resources/assets/assets/img/test-device-6.png") }
                        }),
                        _vm._v(" "),
                        _c("img", {
                          staticClass: "img-fluid d-inline-block mr-3",
                          attrs: { src: __webpack_require__("./resources/assets/assets/img/test-device-7.png") }
                        })
                      ]
                    )
                  ])
                ])
              ])
            ])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-1c5d10da", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1c5d10da\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/publisher/add-project.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1c5d10da\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/publisher/add-project.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("0fabec05", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1c5d10da\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./add-project.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1c5d10da\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./add-project.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/assets/img/test-device-1.png":
/***/ (function(module, exports) {

module.exports = "/images/test-device-1.png?ca006337f2ce3652843331514e07da3f";

/***/ }),

/***/ "./resources/assets/assets/img/test-device-2.png":
/***/ (function(module, exports) {

module.exports = "/images/test-device-2.png?7e85e5afce72acbb94e430440b19b9ff";

/***/ }),

/***/ "./resources/assets/assets/img/test-device-3.png":
/***/ (function(module, exports) {

module.exports = "/images/test-device-3.png?3cdb3e3f4343ea4d714e3b341994cf8c";

/***/ }),

/***/ "./resources/assets/assets/img/test-device-4.png":
/***/ (function(module, exports) {

module.exports = "/images/test-device-4.png?ff2a656e113eb44caef72b06b261b546";

/***/ }),

/***/ "./resources/assets/assets/img/test-device-5.png":
/***/ (function(module, exports) {

module.exports = "/images/test-device-5.png?88a9acdad5ece9b36a3524c2cfbc30b2";

/***/ }),

/***/ "./resources/assets/assets/img/test-device-6.png":
/***/ (function(module, exports) {

module.exports = "/images/test-device-6.png?4f92c9921fc3922e19875b74407e738d";

/***/ }),

/***/ "./resources/assets/assets/img/test-device-7.png":
/***/ (function(module, exports) {

module.exports = "/images/test-device-7.png?6c3249f311ba46a1fc7001a9128c53f9";

/***/ }),

/***/ "./resources/assets/components/pages/publisher/add-project.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1c5d10da\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/publisher/add-project.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = null
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-1c5d10da\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/publisher/add-project.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-1c5d10da"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\publisher\\add-project.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1c5d10da", Component.options)
  } else {
    hotAPI.reload("data-v-1c5d10da", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});