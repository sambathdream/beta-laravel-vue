webpackJsonp([59],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/tester/step2.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "step-2",
  methods: {
    goToDashboard: function goToDashboard() {
      var _this = this;

      var requestData = {
        'status': 'Step_2',
        'email': this.$store.state.user.email
      };
      axios.put('/api/tester/' + this.$store.state.user.id, requestData).then(function (response) {
        _this.$router.push({ name: "tester.dashboard" });
      }).catch(function (error) {
        return _this.errors = error.response.data.message;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-58024073\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/step2.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.vue-js-switch[data-v-58024073] {\n  float: right;\n}\n.user_image[data-v-58024073] {\n  font-size: 10px;\n}\n.input[data-v-58024073] {\n  width: 100% !important;\n}\n.custom-county-block[data-v-58024073] {\n  width: 30%;\n}\n.custom-container[data-v-58024073] {\n  max-width: 100%;\n}\n.custom-container .step-page-title[data-v-58024073] {\n    font-family: \"UniNeueBold\";\n    font-size: 30px;\n    width: 100%;\n    text-align: center;\n    color: #363e48;\n    margin: 5px 0 25px;\n}\n.custom-container .tester-profile-page[data-v-58024073] {\n    width: 100%;\n}\n.custom-container .tester-profile-page .other-input[data-v-58024073] {\n      min-width: 150px;\n}\n.custom-container .tester-profile-page .toggle-btn-group[data-v-58024073] {\n      width: calc(100% - 115px);\n}\n.custom-container .tester-profile-page .white-box[data-v-58024073] {\n      background-color: #fff;\n      -webkit-box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n              box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n      border-radius: 4px;\n      padding: 20px;\n      margin-bottom: 20px;\n      font-size: 17px;\n      color: #606368;\n}\n.custom-container .tester-profile-page .white-box textarea[data-v-58024073] {\n        font-size: 15px;\n        resize: none;\n}\n.custom-container .tester-profile-page .white-box .block-title[data-v-58024073] {\n        font-size: 20px;\n        font-family: \"UniNeueBold\";\n        border-bottom: 1px solid #dadada;\n        color: #2cac3d;\n        padding-bottom: 10px;\n        margin-bottom: 10px;\n}\n.custom-container .tester-profile-page .white-box .img-wrap[data-v-58024073] {\n        height: 120px;\n        width: 120px;\n        border-radius: 50%;\n        border: 3px solid #1f8e28;\n        background-color: #f0f5f9;\n        vertical-align: middle;\n        margin-right: 15px;\n        padding: 18px 25px;\n        position: relative;\n}\n.custom-container .tester-profile-page .white-box .img-wrap h2[data-v-58024073] {\n          font-size: 14px;\n          vertical-align: middle;\n          margin-top: 25px;\n          text-align: center;\n}\n.custom-container .tester-profile-page .white-box .img-wrap .choose-img-btn[data-v-58024073] {\n          opacity: 0;\n          position: absolute;\n          left: 0;\n          top: 0;\n          height: 100%;\n          cursor: pointer;\n}\n.custom-container .tester-profile-page .white-box .content-wrap[data-v-58024073] {\n        vertical-align: middle;\n}\n.custom-container .tester-profile-page .white-box .content-wrap .username[data-v-58024073] {\n          font-family: \"UniNeueRegular\";\n          font-size: 25px;\n          color: #363e48;\n          margin-bottom: 0px;\n}\n.custom-container .tester-profile-page .white-box .content-wrap .usermail[data-v-58024073] {\n          font-size: 15px;\n          color: #73767b;\n          font-family: \"BrandonTextRegular\";\n          margin-bottom: 0;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form[data-v-58024073] {\n        padding-top: 25px;\n        float: right;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group[data-v-58024073] {\n          min-width: 120px;\n          margin-right: 20px;\n          vertical-align: top;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group[data-v-58024073]:last-child {\n            margin-right: 0;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group .form-control[data-v-58024073]:hover, .custom-container .tester-profile-page .white-box .user-profile-form .form-group .form-control[data-v-58024073]:focus {\n            outline: none !important;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group label[data-v-58024073] {\n            display: block;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group .form-check[data-v-58024073] {\n            display: inline-block;\n            margin-right: 15px;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group .form-check .form-check-input[data-v-58024073] {\n              margin-left: 0;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group .form-check .form-check-label[data-v-58024073] {\n              padding-left: 20px;\n}\n.custom-container .tester-profile-page .white-box .detail-form[data-v-58024073] {\n        width: 100%;\n}\n.custom-container .tester-profile-page .white-box .detail-form span[data-v-58024073] {\n          max-width: 210px;\n          vertical-align: middle;\n          font-size: 16px;\n          color: #606368;\n          line-height: 20px;\n}\n.custom-container .tester-profile-page .white-box .detail-form .form-control[data-v-58024073] {\n          width: calc(100% - 90px);\n          float: right;\n          margin-top: 5px;\n}\n.custom-container .tester-profile-page .white-box .detail-form .form-control[data-v-58024073]:hover {\n            outline: none !important;\n}\n.custom-container .tester-profile-page .white-box .detail-form .form-control[data-v-58024073]:focus {\n            outline: none !important;\n}\n.custom-container .tester-profile-page .white-box .detail-form .toggle-btn-group[data-v-58024073] {\n          display: inline-block;\n          vertical-align: inherit;\n          margin-top: 5px;\n          margin-bottom: 0;\n}\n.custom-container .tester-profile-page .white-box .device-name[data-v-58024073] {\n        font-size: 17px;\n        color: #606368;\n        min-width: 110px;\n        vertical-align: middle;\n        display: inline-block;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group[data-v-58024073] {\n        display: inline-block;\n        vertical-align: middle;\n        margin-bottom: 10px;\n        margin-top: 10px;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .green-btn[data-v-58024073] {\n          background-color: #2cac3d;\n          font-size: 17px;\n          font-family: \"BrandonTextMedium\";\n          letter-spacing: 0.05rem;\n          color: #fff;\n          border-radius: 4px;\n          padding: 0px 10px;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .green-btn[data-v-58024073]:hover {\n            background-color: #158f25;\n            outline: none !important;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .green-btn[data-v-58024073]:focus {\n            outline: none !important;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .white-btn[data-v-58024073] {\n          background-color: #fff;\n          border: 1px solid #e4e4e4;\n          font-size: 17px;\n          font-family: \"BrandonTextRegular\";\n          letter-spacing: 0.05rem;\n          color: #606368;\n          border-radius: 4px;\n          padding: 0px 10px;\n          margin-left: 5px;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .white-btn[data-v-58024073]:hover {\n            background-color: #e4e4e4;\n            outline: none !important;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .white-btn[data-v-58024073]:focus {\n            outline: none !important;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .form-control[data-v-58024073] {\n          vertical-align: middle;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .form-control[data-v-58024073]:hover, .custom-container .tester-profile-page .white-box .toggle-btn-group .form-control[data-v-58024073]:focus {\n            outline: none !important;\n}\n.custom-container .tester-profile-page .white-box .policy-text[data-v-58024073] {\n        margin-bottom: 5px;\n}\n.custom-container .tester-profile-page .white-box .policy-text a[data-v-58024073] {\n          color: #2cac3d;\n          font-family: \"BrandonTextBold\";\n          font-size: 17px;\n          text-decoration: underline !important;\n}\n.custom-container .tester-profile-page .white-box .agree-text[data-v-58024073] {\n        font-size: 15px;\n        color: #ff0000;\n}\n.custom-container .tester-profile-page .green-step-btn[data-v-58024073] {\n      width: 100%;\n      max-width: 260px;\n      padding: 7px 10px;\n      border: 2px solid #118921;\n      background-color: #2cac3d;\n      font-size: 14px;\n      font-family: \"BrandonTextMedium\";\n      letter-spacing: 0.05rem;\n      color: #fff;\n      margin: 15px auto;\n      border-radius: 20px;\n      display: block;\n}\n.custom-container .tester-profile-page .green-step-btn[data-v-58024073]:hover {\n        background-color: #158f25;\n}\n@media screen and (max-width: 1281px) {\n.custom-container .tester-profile-page .white-box[data-v-58024073] {\n    font-size: 16px;\n}\n.custom-container .tester-profile-page .white-box textarea[data-v-58024073] {\n      font-size: 14px;\n}\n.custom-container .tester-profile-page .white-box .img-wrap[data-v-58024073] {\n      height: 100px;\n      width: 100px;\n      padding: 15px 20px;\n      margin-right: 10px;\n}\n.custom-container .tester-profile-page .white-box .content-wrap .username[data-v-58024073] {\n      font-size: 22px;\n}\n.custom-container .tester-profile-page .white-box .detail-form span[data-v-58024073] {\n      max-width: 160px;\n      font-size: 14px;\n      line-height: 18px;\n}\n.custom-container .tester-profile-page .white-box .device-name[data-v-58024073] {\n      font-size: 16px;\n}\n}\n@media screen and (max-width: 1200px) {\n.custom-container .step-page-title[data-v-58024073] {\n    font-size: 28px;\n    margin: 0 0 20px;\n}\n.custom-container .tester-profile-page .white-box[data-v-58024073] {\n    font-size: 15px;\n}\n.custom-container .tester-profile-page .white-box .block-title[data-v-58024073] {\n      font-size: 18px;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form[data-v-58024073] {\n      padding-top: 20px;\n      float: left;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group .form-check[data-v-58024073] {\n        margin-right: 0;\n}\n.custom-container .tester-profile-page .white-box .img-wrap[data-v-58024073] {\n      height: 80px;\n      width: 80px;\n      padding: 10px 15px;\n}\n.custom-container .tester-profile-page .white-box .detail-form .form-control[data-v-58024073] {\n      float: left;\n}\n.custom-container .tester-profile-page .white-box .detail-form span[data-v-58024073] {\n      min-height: 55px;\n}\n.custom-container .tester-profile-page .white-box .device-name[data-v-58024073] {\n      min-width: 85px;\n      font-size: 15px;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group[data-v-58024073] {\n      display: block;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .green-btn[data-v-58024073] {\n        font-size: 15px;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .white-btn[data-v-58024073] {\n        font-size: 15px;\n}\n.custom-container .tester-profile-page .white-box .toggle-btn-group .form-control[data-v-58024073] {\n        max-width: 100px;\n}\n.custom-container .tester-profile-page .white-box .policy-text a[data-v-58024073] {\n      font-size: 16px;\n}\n}\n@media screen and (max-width: 991px) {\n.custom-container .tester-profile-page .white-box[data-v-58024073] {\n    padding: 15px;\n}\n.custom-container .tester-profile-page .white-box .detail-form .form-control[data-v-58024073] {\n      max-width: 120px;\n      float: none;\n      margin-bottom: 15px;\n      margin-left: 15px;\n}\n.custom-container .tester-profile-page .white-box .detail-form span[data-v-58024073] {\n      min-height: auto;\n      max-width: 100%;\n}\n.custom-container .tester-profile-page .white-box .detail-form span br[data-v-58024073] {\n        display: none;\n}\n}\n@media screen and (max-width: 575px) {\n.custom-container .step-page-title[data-v-58024073] {\n    font-size: 26px;\n}\n.custom-container .tester-profile-page .white-box[data-v-58024073] {\n    font-size: 14px;\n    line-height: 18px;\n}\n.custom-container .tester-profile-page .white-box .img-wrap[data-v-58024073] {\n      margin-bottom: 10px;\n}\n.custom-container .tester-profile-page .white-box .detail-form .toggle-btn-group[data-v-58024073] {\n      margin-bottom: 15px;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form[data-v-58024073] {\n      width: 100%;\n}\n.custom-container .tester-profile-page .white-box .user-profile-form .form-group[data-v-58024073] {\n        min-width: 100%;\n        margin-right: 0;\n}\n.custom-container .tester-profile-page .white-box .policy-text[data-v-58024073] {\n      font-size: 14px;\n}\n.custom-container .tester-profile-page .white-box .agree-text[data-v-58024073] {\n      font-size: 13px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-58024073\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/tester/step2.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "bg-color" }, [
    _c("div", { staticClass: "container custom-container" }, [
      _c("div", { staticClass: "row" }, [
        _c("h1", { staticClass: "step-page-title" }, [
          _vm._v("\n        Step 2 - Submission Process\n      ")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "tester-profile-page" }, [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass: "btn text-uppercase text-center green-step-btn",
              on: { click: _vm.goToDashboard }
            },
            [_vm._v("go to dashboard")]
          )
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "white-box" }, [
      _c("h4", { staticClass: "block-title" }, [
        _vm._v(
          "\n            How to Accept and Submit Project Review\n          "
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "detail-form" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12" }, [
            _c(
              "div",
              { staticClass: "embed-responsive embed-responsive-16by9" },
              [
                _c("iframe", {
                  staticClass: "embed-responsive-item",
                  attrs: {
                    src: "https://www.youtube.com/embed/xcJtL7QggTI",
                    frameborder: "0"
                  }
                })
              ]
            )
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-58024073", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-58024073\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/step2.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-58024073\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/step2.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("08d44130", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-58024073\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./step2.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-58024073\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./step2.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/components/pages/tester/step2.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-58024073\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/step2.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/tester/step2.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-58024073\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/tester/step2.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-58024073"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\tester\\step2.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-58024073", Component.options)
  } else {
    hotAPI.reload("data-v-58024073", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});