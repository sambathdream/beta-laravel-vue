webpackJsonp([46],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/invoice.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__("./node_modules/vue/dist/vue.common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


__WEBPACK_IMPORTED_MODULE_0_vue___default.a.filter('phone', function (phone) {
  return phone.replace(/[^0-9]/g, '').replace(/(\d{1})(\d{3})(\d{3})(\d{4})/, '(+$1) $2-$3-$4');
});
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "ProjectInvoice",
  props: {
    invoice: {
      required: true,
      default: function _default() {
        return {};
      }
    },
    project: {
      required: true,
      default: function _default() {
        return { publisher: {} };
      }
    }
  },
  computed: {
    totalAmount: function totalAmount() {
      return this.invoice.amount + this.invoice.tax;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/projects/invoice.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_components_components_invoice__ = __webpack_require__("./resources/assets/components/components/invoice.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_components_components_invoice___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_components_components_invoice__);
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "ProjectInvoice",
  components: {
    invoice: __WEBPACK_IMPORTED_MODULE_0_components_components_invoice___default.a
  },
  props: ["invoice", "project"],
  data: function data() {
    return {
      invoice: {},
      project: {
        publisher: {}
      }
    };
  },
  mounted: function mounted() {
    var _this = this;

    axios.get("/api/projects/" + this.$route.params.id).then(function (_ref) {
      var data = _ref.data.data;
      return _this.project = data;
    });
    axios.get("/api/projects/" + this.$route.params.id + "/invoice").then(function (_ref2) {
      var data = _ref2.data.data;
      return _this.invoice = data;
    });
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1762e339\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/invoice.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.payment-info[data-v-1762e339] {\n  padding: 15px;\n  border: 1px solid #cecece;\n  border-radius: 4px;\n  text-align: center;\n}\n.payment-info.pending[data-v-1762e339] {\n  max-width: 250px;\n  border-color: red;\n  color: red;\n}\n.payment-info.paid[data-v-1762e339] {\n  color: green;\n  border-color: green;\n}\n.invoice-note-wrap[data-v-1762e339] {\n  margin: 0 auto;\n  display: block;\n  width: 100%;\n}\n.invoice-note-wrap .invoice-note[data-v-1762e339] {\n  margin: 0 auto;\n  background: #fff;\n  position: relative;\n  padding: 15px;\n}\n.invoice-note-wrap .invoice-note[data-v-1762e339]:before {\n  content: \"\";\n  position: absolute;\n  top: 0;\n  right: 0;\n  border-width: 0 50px 50px 0;\n  /* This trick side-steps a webkit bug */\n  border-style: solid;\n  border-color: white #ebf2f6 white white;\n  /* A bit more verbose to work with .rounded too */\n  background: #fff;\n  /* For when also applying a border-radius */\n  display: block;\n  width: 0;\n  /* Only for Firefox 3.0 damage limitation */\n  /* Optional: shadow */\n  -webkit-box-shadow: -3px 4px 7px rgba(0, 0, 0, 0.1), -1px 1px 0px rgba(0, 0, 0, 0.2);\n  box-shadow: -3px 4px 7px rgba(0, 0, 0, 0.1), -1px 1px 0px rgba(0, 0, 0, 0.2);\n}\n.btn.btn-grey[data-v-1762e339] {\n  background: #898a8c;\n  color: white;\n}\n.amount-box[data-v-1762e339] {\n  padding: 15px;\n  border: 1px solid #cecece;\n  border-radius: 4px;\n  text-align: center;\n  max-width: 250px;\n}\n.amount-box b[data-v-1762e339] {\n  font-size: 16px;\n}\n.description-box[data-v-1762e339] {\n  background: #f9f9f9;\n  padding: 5px 10px;\n}\n.description-box div[data-v-1762e339] {\n  font-weight: 700;\n  font-size: 14px;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-1762e339\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/invoice.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "row" },
    [
      _c(
        "div",
        { staticClass: "col-xl-8 col-lg-10 col-12 invoice-note-wrap" },
        [
          _c("div", { staticClass: "invoice-note" }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-md-6" }, [
                _c("img", {
                  staticClass: "img-fluid",
                  attrs: { src: "/images/home_logo.png" }
                }),
                _vm._v(" "),
                _c("div", { staticClass: "row mt-5" }, [
                  _vm._m(0),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-12 mt-3" }, [
                    _vm._v(
                      "\n              Phone: " +
                        _vm._s(
                          _vm._f("phone")("1" + _vm.project.publisher.mobile_no)
                        )
                    ),
                    _c("br"),
                    _vm._v("\n              info@thebetaplan.com "),
                    _c("br"),
                    _vm._v(" "),
                    _c("a", { attrs: { href: "http://thebetaplan.com" } }, [
                      _vm._v("thebetaplan.com")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-6" }, [
                _c("h2", [_vm._v("Invoice")]),
                _vm._v(" "),
                _c("div", { staticClass: "row mt-5" }, [
                  _c("div", { staticClass: "col-md-12" }, [
                    _vm._v(
                      "\n              invoice # " +
                        _vm._s(_vm.invoice.invoice_number) +
                        " "
                    ),
                    _c("br"),
                    _vm._v(
                      "\n              invoice Date: " +
                        _vm._s(_vm._f("date")(_vm.invoice.created_at)) +
                        " "
                    ),
                    _c("br"),
                    _vm._v(
                      "\n              Due Date: " +
                        _vm._s(_vm._f("date")(_vm.invoice.due_on)) +
                        "\n            "
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-12 mt-4" }, [
                    _c("div", { staticClass: "amount-box" }, [
                      _vm._v("\n                Amount due "),
                      _c("br"),
                      _vm._v(" "),
                      _c("b", [_vm._v("$ " + _vm._s(_vm.totalAmount))])
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "payment-info mt-2",
                        class: {
                          pending: !_vm.invoice.is_paid,
                          paid: _vm.invoice.is_paid
                        }
                      },
                      [
                        _vm.invoice.is_paid
                          ? _c("div", { staticClass: "payment-done-info" }, [
                              _vm._v(
                                "\n                  PAID on " +
                                  _vm._s(_vm._f("date")(_vm.invoice.paid_on)) +
                                  "."
                              ),
                              _c("br"),
                              _vm._v(
                                "\n                  Transaction id " +
                                  _vm._s(_vm.invoice.transaction_id) +
                                  "\n                "
                              )
                            ])
                          : _c("div", [
                              _vm._v(
                                "\n                  Pending\n                "
                              )
                            ])
                      ]
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("hr"),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-12 mt-3" }, [
                _c("b", [_vm._v("Bill To")]),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _vm._v(
                  "\n          " +
                    _vm._s(_vm.project.publisher.first_name) +
                    " " +
                    _vm._s(_vm.project.publisher.last_name)
                ),
                _c("br"),
                _vm._v(" "),
                _c("div", {
                  domProps: {
                    innerHTML: _vm._s(
                      (_vm.project.publisher.address || "")
                        .split("\n")
                        .join("<br />")
                    )
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-12 mt-3" }, [
                _vm._m(1),
                _vm._v(" "),
                _c("div", { staticClass: "mt-3 row" }, [
                  _c("div", { staticClass: "col-md-6" }, [
                    _vm._v(
                      "\n            " +
                        _vm._s(_vm.project.name) +
                        "\n            "
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-6 text-right" }, [
                    _vm._v(
                      "\n              $ " +
                        _vm._s(_vm.totalAmount) +
                        "\n            "
                    )
                  ])
                ])
              ])
            ])
          ])
        ]
      ),
      _vm._v(" "),
      _vm._t("actions", [
        _c(
          "div",
          { staticClass: "col-md-12 mt-5 text-center" },
          [
            _c(
              "router-link",
              {
                staticClass: "btn btn-grey",
                attrs: {
                  to: {
                    name: "admin.project.view",
                    params: { id: _vm.project.id }
                  }
                }
              },
              [_vm._v("Back To Project")]
            )
          ],
          1
        )
      ])
    ],
    2
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-12" }, [
      _c("b", [_vm._v("The Beta Plan")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "description-box row" }, [
      _c("div", { staticClass: "col-md-6" }, [_vm._v("Description")]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-6 text-right" }, [_vm._v("Amount")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-1762e339", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-3cbca2b5\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/projects/invoice.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("invoice", {
    attrs: { invoice: _vm.invoice, project: _vm.project }
  })
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-3cbca2b5", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1762e339\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/invoice.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1762e339\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/invoice.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("06008245", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1762e339\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./invoice.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1762e339\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./invoice.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/components/components/invoice.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1762e339\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/less-loader/dist/cjs.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/invoice.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/invoice.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-1762e339\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/invoice.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-1762e339"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\invoice.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1762e339", Component.options)
  } else {
    hotAPI.reload("data-v-1762e339", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/pages/projects/invoice.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/projects/invoice.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-3cbca2b5\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/projects/invoice.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\projects\\invoice.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3cbca2b5", Component.options)
  } else {
    hotAPI.reload("data-v-3cbca2b5", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});