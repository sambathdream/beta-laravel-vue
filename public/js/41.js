webpackJsonp([41],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/tester/payments.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__("./node_modules/vue/dist/vue.common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "payment",
  data: function data() {
    return {
      wallet: {},
      transactionHistories: [],
      withdrawal_amount: 0,
      amount_error: null,
      withdrawal_inProgress: false,
      pendingWithdrawRequest: null
    };
  },

  methods: {
    openWithdrawModal: function openWithdrawModal() {
      this.$refs.withdrawal_model.show();
    },
    resetAmount: function resetAmount() {
      this.withdrawal_amount = 0;
      this.amount_error = null;
      this.withdrawal_inProgress = false;
    },
    handleWithdrawSubmit: function handleWithdrawSubmit() {
      var _this = this;

      this.amount_error = null;
      if (!this.withdrawal_amount) {
        this.amount_error = "Amount is required";
      }
      if (isNaN(this.withdrawal_amount)) {
        this.amount_error = "Amount must be numeric";
      }
      if (this.amount_error) {
        return;
      }
      this.withdrawal_inProgress = true;
      axios.post("/api/withdraw", { amount: this.withdrawal_amount }).then(function () {
        _this.$refs.withdrawal_model.hide();
        _this.withdrawal_inProgress = false;
        _this.getHistories();
        _this.getWallet();
        _this.getLatestPendingRequest();
      }).catch(function (error) {
        _this.withdrawal_inProgress = false;
        if (Array.isArray(error.response.data.message)) {
          _this.amount_error = error.response.data.message[0];
        } else {
          _this.amount_error = error.response.data.message;
        }
      });
    },
    getHistories: function getHistories() {
      var _this2 = this;

      return axios.get("/api/transaction-histories").then(function (_ref) {
        var data = _ref.data;
        return _this2.transactionHistories = data;
      });
    },
    getWallet: function getWallet() {
      var _this3 = this;

      return axios.get("/api/wallet").then(function (_ref2) {
        var data = _ref2.data.data;
        return _this3.wallet = data;
      });
    },
    getLatestPendingRequest: function getLatestPendingRequest() {
      var _this4 = this;

      return axios.get("/api/transaction-histories/pending-withdrawal").then(function (_ref3) {
        var data = _ref3.data.data;
        return _this4.pendingWithdrawRequest = data;
      });
    }
  },
  mounted: function mounted() {
    this.getWallet();
    this.getHistories();
    this.getLatestPendingRequest();
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/tester/payments.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__("./node_modules/vue/dist/vue.common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_components_components_tester_payments_vue__ = __webpack_require__("./resources/assets/components/components/tester/payments.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_components_components_tester_payments_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_components_components_tester_payments_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  name: "tester-payments",
  components: {
    payments: __WEBPACK_IMPORTED_MODULE_1_components_components_tester_payments_vue___default.a
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-50f76994\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/tester/payments.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.text-green[data-v-50f76994] {\n  color: #2cac3d;\n}\n.text-red[data-v-50f76994] {\n  color: #e63423;\n}\n.text-blue[data-v-50f76994] {\n  color: #00aff3;\n}\n.text-bold[data-v-50f76994] {\n  font-family: \"BrandonTextBold\" !important;\n}\n.text-medium[data-v-50f76994] {\n  font-family: \"BrandonTextMedium\";\n}\n.info-wrap[data-v-50f76994] {\n  background-color: #fcb3b4;\n  border-radius: 6px;\n  padding: 8px;\n  margin-bottom: 20px;\n}\n.white-box[data-v-50f76994] {\n  background-color: #fff;\n  -webkit-box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n          box-shadow: 0px 0px 3px 2px rgba(3, 3, 3, 0.05);\n  border-radius: 4px;\n  padding: 20px;\n  margin-bottom: 20px;\n  font-size: 14px;\n  color: #606368;\n  letter-spacing: 0;\n}\n.white-box a[data-v-50f76994] {\n    font-size: 14px;\n    color: #2cac3d;\n    font-family: \"BrandonTextRegular\";\n    text-decoration: underline !important;\n}\n.white-box .block-title[data-v-50f76994] {\n    font-size: 20px;\n    font-family: \"UniNeueBold\";\n    border-bottom: 1px solid #dadada;\n    color: #2cac3d;\n    padding-bottom: 10px;\n    margin-bottom: 10px;\n}\n.white-box .content-wrap[data-v-50f76994] {\n    vertical-align: middle;\n}\n.white-box .content-wrap .username[data-v-50f76994] {\n      font-family: \"UniNeueRegular\";\n      font-size: 25px;\n      color: #363e48;\n      margin-bottom: 0px;\n}\n.white-box .content-wrap .usermail[data-v-50f76994] {\n      font-size: 15px;\n      color: #73767b;\n      font-family: \"BrandonTextRegular\";\n      margin-bottom: 0;\n}\n.white-box .custom-form-group .custom-form-control[data-v-50f76994] {\n    background-color: transparent !important;\n}\n.green-btn[data-v-50f76994] {\n  width: 100%;\n  max-width: 130px;\n  padding: 7px 10px;\n  border: 2px solid #118921;\n  background-color: #2cac3d;\n  font-size: 14px;\n  font-family: \"BrandonTextMedium\";\n  letter-spacing: 0.05rem;\n  color: #fff;\n  margin: 15px auto;\n  border-radius: 20px;\n  display: block;\n}\n.green-btn[data-v-50f76994]:hover {\n    background-color: #158f25;\n}\n.blue-btn[data-v-50f76994] {\n  width: 100%;\n  max-width: 130px;\n  padding: 7px 10px;\n  border: 2px solid #0082cc;\n  background-color: #00aff5;\n  font-size: 14px;\n  font-family: \"BrandonTextMedium\";\n  letter-spacing: 0.05rem;\n  color: #fff;\n  margin: 15px auto;\n  border-radius: 20px;\n  display: block;\n}\n.blue-btn[data-v-50f76994]:hover {\n    background-color: #13b9fb;\n}\n@media screen and (max-width: 1281px) {\n.white-box .content-wrap .username[data-v-50f76994] {\n    font-size: 22px;\n}\n}\n@media screen and (max-width: 1200px) {\n.white-box .block-title[data-v-50f76994] {\n    font-size: 18px;\n}\n}\n@media screen and (max-width: 991px) {\n.white-box[data-v-50f76994] {\n    padding: 15px;\n}\n}\n@media screen and (max-width: 767px) {\n.white-box .content-wrap[data-v-50f76994] {\n    margin-bottom: 5px;\n}\n.white-box .content-wrap .username[data-v-50f76994] {\n      font-size: 18px;\n      margin-bottom: 5px;\n}\n}\n@media screen and (max-width: 575px) {\n.white-box[data-v-50f76994] {\n    line-height: 18px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6a8df5a0\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/payments.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.text-bold[data-v-6a8df5a0] {\n  font-family: \"BrandonTextBold\";\n}\n.custom-container[data-v-6a8df5a0] {\n  max-width: 100%;\n}\n.custom-container .page-title[data-v-6a8df5a0] {\n    font-family: \"UniNeueBold\";\n    font-size: 30px;\n    width: 100%;\n    text-align: center;\n    color: #363e48;\n    margin: 5px 0 25px;\n}\n.custom-container .payment-page[data-v-6a8df5a0] {\n    width: 100%;\n}\n@media screen and (max-width: 1200px) {\n.custom-container .page-title[data-v-6a8df5a0] {\n    font-size: 28px;\n    margin: 0 0 20px;\n}\n}\n@media screen and (max-width: 575px) {\n.custom-container .page-title[data-v-6a8df5a0] {\n    font-size: 26px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-50f76994\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/tester/payments.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "testing-process-tabwrap" },
    [
      _vm.pendingWithdrawRequest
        ? _c("div", { staticClass: "info-wrap" }, [
            _vm._v("\n    Your request of "),
            _c("span", { staticClass: "text-bold" }, [
              _vm._v("$" + _vm._s(_vm.pendingWithdrawRequest.amount))
            ]),
            _vm._v(
              " on " +
                _vm._s(_vm._f("date")(_vm.pendingWithdrawRequest.created_at)) +
                " is still pending.\n  "
            )
          ])
        : _vm._e(),
      _vm._v(" "),
      _c("div", { staticClass: "white-box" }, [
        _c("div", { staticClass: "row" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-4 col-12 text-md-right" }, [
            _c("span", { staticClass: "d-block" }, [_vm._v("Current Balance")]),
            _vm._v(" "),
            _c("span", { staticClass: "d-block text-bold" }, [
              _vm._v("$" + _vm._s(_vm.wallet.available_earnings || 0))
            ]),
            _vm._v(" "),
            _vm.wallet.available_earnings && !_vm.pendingWithdrawRequest
              ? _c(
                  "a",
                  {
                    attrs: { href: "#" },
                    on: {
                      click: function($event) {
                        $event.preventDefault()
                        _vm.openWithdrawModal($event)
                      }
                    }
                  },
                  [_vm._v("Withdraw")]
                )
              : _vm._e()
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "white-box" }, [
        _c("h4", { staticClass: "block-title" }, [
          _vm._v("\n      Transaction History\n    ")
        ]),
        _vm._v(" "),
        _c("form", [
          _c("div", { staticClass: "row" }, [
            _vm._m(1),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-4 col-12" }, [
              _c(
                "div",
                { staticClass: "form-group custom-form-group" },
                [
                  _c("span", { staticClass: "text-bold" }, [
                    _vm._v("From Date : ")
                  ]),
                  _vm._v(" "),
                  _c("datepicker", {
                    attrs: {
                      "input-class": "form-control custom-form-control",
                      format: "dd MMM yyyy"
                    }
                  })
                ],
                1
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-4 col-12" }, [
              _c(
                "div",
                { staticClass: "form-group custom-form-group" },
                [
                  _c("span", { staticClass: "text-bold" }, [
                    _vm._v("To Date : ")
                  ]),
                  _vm._v(" "),
                  _c("datepicker", {
                    attrs: {
                      "input-class": "form-control custom-form-control",
                      format: "dd MMM yyyy"
                    }
                  })
                ],
                1
              )
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "white-box" }, [
        _c("h4", { staticClass: "block-title" }, [
          _vm._v("\n      Transactions\n    ")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "table-responsive tester-payment-table" }, [
          _c("table", { staticClass: "table" }, [
            _vm._m(2),
            _vm._v(" "),
            _c(
              "tbody",
              _vm._l(_vm.transactionHistories, function(t) {
                return _c("tr", { key: t.id }, [
                  _c("td", { attrs: { scope: "row" } }, [
                    _vm._v(_vm._s(_vm._f("date")(t.created_at)))
                  ]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(t.note))]),
                  _vm._v(" "),
                  _c("td", { staticClass: "text-right" }, [
                    _vm._v(
                      _vm._s(t.note.indexOf("Withdrawal") >= 0 ? "-" : "") +
                        "$" +
                        _vm._s(t.amount)
                    )
                  ]),
                  _vm._v(" "),
                  _c("td", { staticClass: "text-right" }, [
                    _vm._v("$" + _vm._s(t.balance_after))
                  ]),
                  _vm._v(" "),
                  _c("td", { staticClass: "text-right" }, [
                    _c("a", { attrs: { href: "#" } }, [_vm._v(_vm._s(t.id))])
                  ])
                ])
              })
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _vm.wallet.available_earnings && !_vm.pendingWithdrawRequest
        ? _c("p", [
            _vm._v(" Your current balance is : "),
            _c("span", { staticClass: "text-bold" }, [
              _vm._v("$" + _vm._s(_vm.wallet.available_earnings || 0))
            ]),
            _vm._v(". Would you like to withdraw balance amount ?\n  ")
          ])
        : _vm._e(),
      _vm._v(" "),
      _vm.wallet.available_earnings && !_vm.pendingWithdrawRequest
        ? _c(
            "button",
            {
              staticClass: "btn text-uppercase text-center green-btn",
              attrs: { type: "button" },
              on: {
                click: function($event) {
                  $event.preventDefault()
                  _vm.openWithdrawModal($event)
                }
              }
            },
            [_vm._v("withdraw")]
          )
        : _vm._e(),
      _vm._v(" "),
      _c(
        "b-modal",
        {
          ref: "withdrawal_model",
          attrs: { "cancel-disabled": true, title: "Withdraw Amount" },
          on: { shown: _vm.resetAmount }
        },
        [
          _c("div", { staticClass: "col-md-12 mb-2" }, [
            _c("div", { attrs: { className: "form-group" } }, [
              _c("label", [_vm._v("Amount To Withdraw")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.withdrawal_amount,
                    expression: "withdrawal_amount"
                  }
                ],
                staticClass: "form-control",
                attrs: { type: "text", required: "" },
                domProps: { value: _vm.withdrawal_amount },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.withdrawal_amount = $event.target.value
                  }
                }
              }),
              _vm._v(" "),
              _vm.amount_error
                ? _c("div", { staticStyle: { color: "red" } }, [
                    _vm._v(_vm._s(_vm.amount_error))
                  ])
                : _vm._e()
            ])
          ]),
          _vm._v(" "),
          _c("div", { attrs: { slot: "modal-footer" }, slot: "modal-footer" }, [
            _c(
              "button",
              {
                staticClass: "btn text-uppercase text-center green-btn",
                staticStyle: { "max-width": "100%" },
                attrs: { disabled: _vm.withdrawal_inProgress, type: "button" },
                on: { click: _vm.handleWithdrawSubmit }
              },
              [_vm._v("Submit Withdraw Request")]
            )
          ])
        ]
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-8 col-12" }, [
      _c("div", { staticClass: "content-wrap d-block d-sm-inline-block" }, [
        _c("h4", { staticClass: "username" }, [
          _vm._v("Your default Paypal account")
        ]),
        _vm._v(" "),
        _c("p", { staticClass: "usermail" }, [_vm._v("ndtester1@yopmail.com")]),
        _vm._v(" "),
        _c("a", { attrs: { href: "#" } }, [_vm._v("Change")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-4 col-12" }, [
      _c("div", { staticClass: "form-group custom-form-group" }, [
        _c("span", { staticClass: "text-bold" }, [_vm._v("Records : ")]),
        _vm._v(" "),
        _c("select", { staticClass: "form-control custom-form-control" }, [
          _c("option", [_vm._v("Last 1 Month")]),
          _vm._v(" "),
          _c("option", [_vm._v("Lat 3 Months")]),
          _vm._v(" "),
          _c("option", [_vm._v("Last 6 Months")]),
          _vm._v(" "),
          _c("option", [_vm._v("Custom")])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Payment Date")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Project Name")]),
        _vm._v(" "),
        _c("th", { staticClass: "text-right", attrs: { scope: "col" } }, [
          _vm._v("Amount")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "text-right", attrs: { scope: "col" } }, [
          _vm._v("Balance")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "text-right", attrs: { scope: "col" } }, [
          _vm._v("Ref. Id")
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-50f76994", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-6a8df5a0\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/tester/payments.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "bg-color" }, [
      _c("div", { staticClass: "container custom-container" }, [
        _c("div", { staticClass: "row" }, [
          _c("h1", { staticClass: "page-title" }, [
            _vm._v("\n        Payments\n      ")
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "payment-page" }, [_c("payments")], 1)
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-6a8df5a0", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-50f76994\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/tester/payments.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-50f76994\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/tester/payments.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("92ab4bae", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-50f76994\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./payments.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-50f76994\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./payments.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6a8df5a0\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/payments.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6a8df5a0\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/payments.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("1f038668", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6a8df5a0\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./payments.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6a8df5a0\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./payments.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/assets/components/components/tester/payments.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-50f76994\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/components/tester/payments.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/components/tester/payments.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-50f76994\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/components/tester/payments.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-50f76994"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\components\\tester\\payments.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-50f76994", Component.options)
  } else {
    hotAPI.reload("data-v-50f76994", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/components/pages/tester/payments.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6a8df5a0\",\"scoped\":true,\"hasInlineConfig\":true}!./node_modules/sass-loader/lib/loader.js!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/components/pages/tester/payments.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/components/pages/tester/payments.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-6a8df5a0\",\"hasScoped\":true,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/components/pages/tester/payments.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-6a8df5a0"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\components\\pages\\tester\\payments.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6a8df5a0", Component.options)
  } else {
    hotAPI.reload("data-v-6a8df5a0", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});