<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::post('public/payments/stripebill/{id}', 'ApiUserInvoiceController@stripeHookPost');

Route::get('/testsummary', 'HomeController@emailSummaryTest');
Route::get('/retstripecust', 'HomeController@retStripeCustomers'); //retrieve stripe customers
Route::get('/retstripeinv', 'HomeController@retStripeInv'); //retrieve stripe invoices
Route::get('/retstripecharge', 'HomeController@retStripeCharge'); //retrieve stripe invoices

//Route::post('/razorpost', 'HomeController@razorPost');
//Route::get('/yodlee', 'HomeController@yodlee');
//Route::get('/chart', 'HomeController@show');
//Route::get('/country', 'HomeController@country');
//Route::get('/pdftest', 'PublicController@pdftest');
//Route::get('/report', 'HomeController@report');
//Route::get('/viewinvoice', 'HomeController@invoiceView');
//Route::get('/template', 'HomeController@invoiceTemplateTest');

Route::get('/', 'WelcomeController@show');

Route::get('/home', 'DashboardController@index');

Route::get('invoices/design', 'UserInvoiceController@designInvoice');

Route::resource('dashboard', 'DashboardController');
Route::resource('contacts', 'ContactController');

Route::resource('invoices/recurring', 'RecInvoiceController');
Route::resource('invoices', 'UserInvoiceController');


Route::resource('gateways', 'GatewayController');
Route::resource('payments', 'PaymentController');   //?? needed?
Route::resource('inventory', 'InventoryController');
Route::resource('transactions', 'TransactionController');
Route::resource('glaccounts', 'GLaccountController');
Route::resource('config', 'ConfigController');
Route::resource('quotations', 'QuotationController');
Route::resource('creditnotes', 'CreditNoteController');
Route::resource('reports', 'ReportController');

Route::get('/public/invoices/{id}', 'PublicController@showInvoice');
Route::get('/public/quotations/{id}', 'PublicController@showQuote');
Route::get('/public/creditnotes/{id}', 'PublicController@showCreditNote');

Route::get('/public/invoices/{id}/download', 'PublicController@downloadInvoice');
Route::get('/public/quotations/{id}/download', 'PublicController@downloadQuote');
Route::get('/public/creditnotes/{id}/download', 'PublicController@downloadCreditNote');

Route::get('/public/invoices/{id}/payment', 'PublicController@createPayment');
Route::post('/public/invoices/{id}/payment', 'PublicController@storePayment');
Route::post('/public/invoices/{id}/transaction', 'PublicController@postTransaction');

Route::post('/public/payments/{id}/webhook', 'PublicController@stripeWebHook');
//no CSRF protection- modified VerifyCsrfToken.php in the middleware

//Route::get('/recordpayment', function (){ return view('user.payments.record_payment'); } );
