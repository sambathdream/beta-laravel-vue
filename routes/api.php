<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register the API routes for your application as
| the routes are automatically authenticated using the API guard and
| loaded automatically by this application's RouteServiceProvider.
|
*/

Route::group([
    'middleware' => 'auth:api'
], function () {
    //

    Route::get('contacts/list', 'ApiContactController@listContacts');
    Route::get('invoices/list', 'ApiUserInvoiceController@listInvoices');
    Route::get('inventory/list', 'ApiInventoryController@listItems');
    Route::get('quotations/list', 'ApiQuotationController@listQuotes');
    Route::get('gsthsnsac/list', 'ApiGstController@listGSTHsnSac');
    
    Route::post('payments/convert', 'ApiPaymentController@convertCurrency');

    Route::post('invoices/stripebillconvert/{id}', 'ApiUserInvoiceController@stripeBillConvert');
    
    Route::post('invoices/{id}/send', 'ApiUserInvoiceController@sendInvoice');
    Route::get('invoices/previous', 'ApiUserInvoiceController@previousInvManID');
    Route::get('quotations/previous', 'ApiQuotationController@previousQuoteManID');
    Route::get('creditnotes/previous', 'ApiCreditNoteController@previousCreditNoteManID');
    
    Route::get('invoices/previous/{id}', 'ApiUserInvoiceController@validateInvID');
    Route::get('quotations/previous/{id}', 'ApiQuotationController@validateQuoteID');
    Route::get('creditnotes/previous/{id}', 'ApiCreditNoteController@validateCreditNoteID');

    Route::post('quotations/{id}/send', 'ApiQuotationController@sendQuote');
    Route::post('creditnotes/{id}/send', 'ApiCreditNoteController@sendCreditNote');
    Route::post('quotations/convert', 'ApiQuotationController@convertToInvoice');
    //
    Route::get('searchlocation', 'ApiConfigController@findLocation');

    Route::resource('contacts', 'ApiContactController');
    Route::resource('invoices/recurring', 'ApiRecInvoiceController');
    //Route::resource('recurring', 'ApiRecInvoiceController');
    Route::resource('invoices', 'ApiUserInvoiceController');
    Route::resource('inventory', 'ApiInventoryController');
    Route::resource('gateways', 'ApiGatewayController');
    Route::resource('transactions', 'ApiTransactionController');
    Route::resource('glaccounts', 'ApiGLaccountController');
    Route::resource('config', 'ApiConfigController');
    Route::resource('payments', 'ApiPaymentController');
    Route::resource('quotations', 'ApiQuotationController');
    Route::resource('creditnotes', 'ApiCreditNoteController');
    Route::resource('refunds', 'ApiRefundController');
    Route::resource('boottours', 'ApiTourController');

    Route::post('displayreport', 'ApiReportController@displayReport');
    //Route::get('displayreport', 'ApiReportController@displayReport');
    Route::resource('reports', 'ApiReportController');

});


Route::post('authtok', 'PublicController@authenticate');

